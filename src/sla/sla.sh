#!/bin/sh

CUR_PID=`ps -ef | grep SLARunner | awk -F' ' '{print $2}'`
IFS=" "
OLD_PIDS=`cat /opt/log/sla.proc`

for OLD_PID in ${OLD_PIDS}; do
  if [ "${OLD_PID}" = "${CUR_PID}" ]; then
     exit
  fi
done

rm /opt/log/sla.proc
sleep 1s

SLA_PROPS_FILE="/opt/dashboard/special/sla.properties"
DASHBOARD_PATH=`grep "DASHBOARD_PATH" ${SLA_PROPS_FILE} | cut -d "=" -f2`

WEB_INF_PATH=`grep "WEB_INF_PATH" ${SLA_PROPS_FILE} | cut -d "=" -f2`
TOMCAT_LIB_PATH=`grep "TOMCAT_LIB_PATH" ${SLA_PROPS_FILE} | cut -d "=" -f2`
DASHBOARD_CLASSES_PATH=${DASHBOARD_PATH}/${WEB_INF_PATH}/`grep "CLASSES_PATH" ${SLA_PROPS_FILE} | cut -d "=" -f2`
java -cp ${DASHBOARD_CLASSES_PATH}:${TOMCAT_LIB_PATH}/* net.juniper.dash.sla.SLARunner &

echo $! > /opt/log/sla.proc
