"""

Sanny Mulyono, smulyono@juniper.net, 2010-06-25

Copyright (c) 2010, Juniper Networks, Inc.
All rights reserved.

"""
from django.conf import settings
from django.http import HttpResponse, HttpResponseRedirect, \
                        HttpResponseNotFound
from django.template import loader
from django.shortcuts import render_to_response
from django.template.context import RequestContext

import csv
import cjson
from cStringIO import StringIO
import re
import utils
import dash

def default_page(request):
    
    return render_to_response('pr_date/view.html', {}, RequestContext(request, {
        'title':'PR Deploy Date',
        'page_version' : "BETA"
    }))
    

def find_pr(request):
    pr_list = request.POST.get('pr_number', None)
    # construct an array from the comma separated PR
    if pr_list != None:
        pr_list = re.compile("[\s,]").split(pr_list)
    return render_to_response('pr_date/result_pr.html', {}, RequestContext(request, {
        'title':'PR Deploy FIND RESULT',
        'pr_list' : " ,".join(pr_list),
    }))
    
def add_row(request):
    pr_list = request.POST.get('pr_number', None)
    result = None
    if pr_list != None:
        pr_list = re.compile("[\s,]").split(pr_list)
        result = utils.find_pr(pr_list)
        if len(result) == 0 :
            result = None
    return render_to_response('pr_date/result_pr_row.html', {}, RequestContext(request, {
        'pr_number' : "".join(pr_list),
        'result' : result,
        'host' : dash.DataSource.all['Gnats'].host,
        'path' : dash.DataSource.all['Gnats'].path,
        'database' : 'default'
    }))

def create_csv(request):
    params = request.GET
    rows = params.get("csv_data")
    obj_data = cjson.decode(rows)
    
    output = StringIO()
    writer = csv.writer(output, csv.QUOTE_NONNUMERIC)

    head_col = obj_data.get("header", [])
    data_rows = obj_data.get("rows", [])
    writer.writerow(head_col)
    for i in data_rows:
        if len(i) > 0:
            # only write that actually has data
            writer.writerow(i)
    result = output.getvalue()
    output.close()
    response = HttpResponse(result, mimetype='text/csv')
    response['Content-Disposition'] = 'attachment; filename=result.csv' 
    return response 