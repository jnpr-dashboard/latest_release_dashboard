'''
Created on May 27, 2010

@author: smulyono
'''
import os

from django.conf import settings
from django.conf.urls import *

urlpatterns = patterns('dash.pr_date.views',
    url(r'^/?$',
        'default_page',
        name='pr_date_default'),
    url(r'^/find_pr',
        'find_pr',
        name='find_pr'),
    url(r'^/add_row',
        'add_row',
        name='add_row'),
    url(r'^/create_csv',
        'create_csv',
        name='create_csv'),
)
