#!env python26

'''
GNATS connection database

This module will let you connect and do some calculation
to get the closest deploy date.
 
There is dependency on the R1 deploy date which coming from 
  dash
  
Created on May 27, 2010

@author: smulyono
'''
import dash
import gnats
import pprint
import re
import sys
import time


from optparse import OptionParser

release_pattern = re.compile("(?P<release>(?P<major>[1-9][0-9]?).(?P<minor>[1-9]?[0-9]))(?P<release_type>(?P<type>.*)[1-9][0-9]?)")
pr_pattern      = re.compile('(?P<pr_number>(\d)+)(-(?P<scope>(\d)*))?')
scope_pattern   = re.compile("(?P<field>.*){(?P<scope_num>(\d)+)}")
# our identifer
NACR = "Not actual customer release"
NDY  = "Not deployed yet"

class PR_Deploy(object):
    """ Container of the PRs along with their new deploy dates """
    
    def __init__(self, non_default=[], num_prs=''):
        self.hostname           = getattr(non_default, "hostname", "gnats.juniper.net")  
        self.dbname             = getattr(non_default, "dname", "default")
        self.port               = getattr(non_default, "port", "1529")
        self.expr               = getattr(non_default, "expr", None)
        self.timestamp_expr     = getattr(non_default, "timestamp_expr", None)
        self.columns            = getattr(non_default, "columns", "number").split(",")
        self.timestamp_columns  = getattr(non_default, "timestamp_columns", "number").split(",")
        self.table_col          = getattr(non_default, "t_columns", {'change-log': ['field','to', 'datetime']})
        self.result_sort        = getattr(non_default, "sort", None)
        self.username           = "dashboard"
        self.password           = "*"
        self.gnats_db           = None
        self.raw_results = []
        self.q_results = [] # shows the native results from GNATS
        self.pr_results = [] # reformat the structure to meet our needs here...
        
        # Some static configuration, which might be handy if there are changes in the future
        self.change_log_table = 'change-log'
        self.table_release_field = ['committed-release','conf-committed-release']
        self.other_timestamp = ['target']
        self.table_field_index = 0
        self.table_value_index = 1
        self.table_datetime_index = 2
        
        self.table_value_field= ['to']
        self.table_process_column = ['field','to','datetime']
        
        # for pr number arguments
        self.prs = None
        if num_prs : 
            self.prs = re.split('[\s,;]+',num_prs[0])
        
    
    def create_connection_to_gnats(self):
        temp_db = gnats.get_database(self.hostname, self.dbname ,self.port)
        self.gnats_db = temp_db.get_handle(self.username, self.password)
    
    def run_query(self):
        if not self.gnats_db:
            self.create_connection_to_gnats()
        try:
            # Run the first query for getting the first initial data & scopes data
            db_results = self.gnats_db.query(self.expr, self.columns , sort=self.result_sort, table_cols = self.table_col, pr_list=self.prs)
            self.raw_results = db_results
            self.reformat_query()

            # remove the PR scope from the list
            new_unique_pr = []
            for pr in self.prs:
                m = re.match(pr_pattern,pr)
                if (m.group('pr_number')!= None):
                    new_unique_pr.append(m.group('pr_number'))
            db_results = self.gnats_db.query(self.timestamp_expr, self.timestamp_columns , sort=self.result_sort, table_cols = self.table_col, pr_list=new_unique_pr)
            self.raw_results = db_results
            self.make_data()
            return "success %d rows" % (len(self.q_results))
        except gnats.GnatsException, err:
            return "Error in query: %s" % err.message
    
    def reformat_query(self):
        ''' 
        Pre-processing results
        '''
        data_results = {}
        for row in self.raw_results:
            temp = {}
            # dict that will store the each scope information temporarily
            col_index = 0
            # Iterates on every fields/column of the rows
            pr_number = ""
            for column in self.columns:
                if self.table_col and column in self.table_col.keys():
                    # Iterates the table data
                    temp[column] = []
                    for tbl_col_value in row[col_index]:
                        # iterating the column on the table results
                        # XXXNOTICE... we are assuming that on each rows of table-record
                        #     there will be only 1 column that specified the scopes identifier
                        tbl_col_index = 0
                        temp_col_value = {}
                        # We iterate the columns based on the parameter in table_col
                        for tbl_field in self.table_col.get(column):
                            temp_col_value[tbl_field] = tbl_col_value[tbl_col_index]
                            tbl_col_index += 1
                        # temp_col_value will consist of the table-field and their value in 
                        #   the specific row
                        # temp_col_value['<field-name>'] = '<value>'
                        temp[column].append(temp_col_value)
                else:
                    # If the fields are not Table fields, then it should go here.
                    temp[column] = row[col_index]
                    if column == "number":
                        pr_number = temp[column]
                        data_results[temp[column]] = {}
                col_index+=1
            data_results[pr_number] = temp
        self.q_results = data_results

    def make_data(self):
        fmt_results = {}
        """ (A)
            Construct data into fmt_results 
            Find data either CR/CCR
        """ 
        for key, value in self.q_results.iteritems():
            value['deploy_date'] = ''
            value['deploy_epoch'] = 0
            value['deploy_time'] = 0
            value['deploy_recnum'] = ""
            value['deploy_link'] = ""
            value['deploy_release'] = NDY
            # Determine the correct actual_release
            time_to_compare = None
            value['field'] = "None"
            value['value'] = "-"
            value['value_deployed_time'] = 0
            value['value_recnum'] =""
            value['value_link'] = ""
            for fld in self.table_release_field:
                if len(value[fld]) > 0 : # if Release value not empty
                    try:
                        m = re.match(release_pattern, value[fld])
                        if m.group("type").lower() in ['s','r'] :
                            # we only care on R and S releases
                            (date, epoch_time, rec_num) = find_deploy_date(m.group("release"), m.group("release_type"))
                            if (time_to_compare > epoch_time or \
                                time_to_compare == None):
                                value["deploy_time"] = date
                                value["deploy_epoch"] = epoch_time
                                value["deploy_release"] = value[fld]
                                value["deploy_recnum"] = rec_num
                                value["field"] = fld
                                value["value"] = value[fld]
                                value["value_deployed_time"] = date
                                value["value_recnum"] = rec_num

                                if rec_num != 0 :
                                    value["deploy_link"] = "https://{{ Releases.host }}{{ Releases.path }}/do/showView?tableName=Milestones&record_number=%s" % rec_num
                                    value["value_link"] = "https://{{ Releases.host }}{{ Releases.path }}/do/showView?tableName=Milestones&record_number=%s" % rec_num
                                else:
                                    value["deploy_link"] = ""
                                    value["value_link"] = ""
                                    
                                time_to_compare = epoch_time
                    except KeyError:
                        pass
            pr = re.match(pr_pattern, key)
            if pr.group("pr_number") in fmt_results:
                fmt_results[pr.group("pr_number")][pr.group("scope")] = value
            else:
                fmt_results[pr.group("pr_number")] = {pr.group("scope"):value}

        # Get CR/CCR Value first from q_results
        """ (B)"""
        for row in self.raw_results:
            col_index = 0
            pr_number = 0
            for column in self.timestamp_columns:
                if column == "number":
                    try:
                        m = re.match(pr_pattern, row[col_index])
                        pr_number = m.group("pr_number")
                    except KeyError:
                        pass
                else:
                    # the other column will be timestamp or tables
                    tbl_named_data = {}
                    if column in self.table_col.keys():
                        # make <field><scope>:value
                        for tbl_data in row[col_index]:
                            try:
                                m = re.match(scope_pattern, tbl_data[self.table_field_index])
                                field_name = m.group("field").lower() 
                                if field_name in self.table_release_field or \
                                   field_name in self.other_timestamp :
                                    if m.group("scope_num") in tbl_named_data:
                                        if field_name in tbl_named_data[m.group("scope_num")]:
                                            old_data = tbl_named_data[m.group("scope_num")][field_name]
                                            time_compare = get_epoch(old_data[self.table_datetime_index])
                                            if time_compare < get_epoch(tbl_data[self.table_datetime_index]):
                                                tbl_named_data[m.group("scope_num")][field_name] = tbl_data
                                        else:
                                            tbl_named_data[m.group("scope_num")][field_name]= tbl_data
                                    else:
                                        tbl_named_data[m.group("scope_num")] = {}
                                        tbl_named_data[m.group("scope_num")][field_name]= tbl_data
                            except KeyError:
                                pass
                    # Check for Target
                    for scope,field_data in tbl_named_data.iteritems():
                        if "target" not in field_data.keys():
                            # No Target, then we will use CR/CCR
                            if not scope in fmt_results[pr_number]: continue
                            fld = fmt_results[pr_number][scope]['field']
                            for fld_keys in field_data.keys():
                                if fld_keys in self.table_release_field:
                                    if fld_keys != fld.lower():
                                        del tbl_named_data[scope][fld_keys]

                                else:
                                    for fld_keys in field_data.keys():
                                    # No last commit time can be obtained?!!!
                                        del tbl_named_data[scope][fld_keys]
                        else:
                            for items in field_data.keys():
                                if items not in ['target'] :
                                    del tbl_named_data[scope][items]
                    # Compare the data we just have into fmt_results
                    for scope, field_data in tbl_named_data.iteritems():
                        for fld in field_data:
                            grab_data = tbl_named_data[scope][fld]
                            if scope in fmt_results[pr_number] :
                                fmt_results[pr_number][scope]['last_commit_field'] = fld 
                                fmt_results[pr_number][scope]['last_commit_time'] = grab_data[self.table_datetime_index] 
                                fmt_results[pr_number][scope]['last_commit_epoch'] = get_epoch(grab_data[self.table_datetime_index]) 
                col_index += 1

        """ (C) """
        for scope_data in fmt_results.values():
            for scope_num, value_data in scope_data.iteritems():
                if 'last_commit_epoch' not in value_data.keys():
                    value_data['last_commit_epoch'] = 0
                    value_data['last_commit_time'] = ""
                    value_data['last_commit_field'] = "NA"
                value_data = find_missed_release(value_data)
        self.pr_results = fmt_results
        
    def show_results(self):
        print " Captured by PR Deploy as "
        print """
   Database = %s
   Host     = %s
   Port     = %s
   Columns  = %s
   T-column = %s
   Expr     = %s
   PRs      = %s
        """ % (self.dbname, self.hostname, self.port,self.columns, self.table_col,self.expr, self.prs)
        if self.pr_results:
            for temp in self.pr_results.iterkeys():
                print "PR Number   = %s " % temp
                print "Total Scope = %d " % len(self.pr_results[temp])
                print " ========================= "
                for scope, field_tbl in self.pr_results[temp].iteritems():
                    print "   Scope - %s " % scope
                    for datum in field_tbl:
                        for key, val in datum.iteritems():
                            print "    * %s -> %s " % (key, val)
                        print " ******************* "

    # Simple format display
    def display_normal_result(self):
        # display columns;
        for row in self.q_results:
            for column in self.columns:
                if column in self.table_col.keys():
                    for tb_row in row[column]:
                        for tbl_col in self.table_col.get(column):
                            print "         %s => %s " % (tbl_col,tb_row[tbl_col])
                        print "====================================="
                else :
                    print "%s : %s" % (column, row[column])
                    

    
    def run_test(self):
        # Test Scenario 
        #  Using PR #481366,521509  749126 751228 438746
        self.prs = ['438746']
        #  Using expression with changelog
        self.hostname = 'gnats.juniper.net'
        self.timestamp_expr = 'product[group]=="junos" & \
                    ((change-log.field~"^(conf-)?committed-release" ) | (change-log.field~"target" )) & \
                    (change-log.username ~ "builder|gnats" )'
        self.expr = 'product[group]=="junos"'
        
        self.columns = ['number','committed-release','conf-committed-release','target']
        self.timestamp_columns = ['number','change-log']
        self.table_col = {'change-log': ['field','to', 'datetime']}
        
        self.result_sort = None #(('change-log', 'desc'),)
        self.run_query()
        pprint.pprint(self.pr_results)
#        self.display_normal_result()
#        self.show_results()

    def run_report(self, pr_number):
        # Test Scenario 
        #  Using PR #481366,521509  749126 751228 438746
        self.prs = pr_number
        #  Using expression with changelog
        self.hostname = 'gnats.juniper.net'
        self.timestamp_expr = 'product[group]=="junos" & \
                    ((change-log.field~"^(conf-)?committed-release" ) | (change-log.field~"target" )) & \
                    (change-log.username ~ "builder|gnats" )'
        self.expr = 'product[group]=="junos"'
        
        self.columns = ['number','committed-release','conf-committed-release','target']
        self.timestamp_columns = ['number','change-log']
        self.table_col = {'change-log': ['field','to', 'datetime']}
        
        self.result_sort = None #(('change-log', 'desc'),)
        self.run_query()
#        time.sleep(5)
        return self.pr_results

def get_epoch(dtime):
    return dash.get_epoch(dtime)

def find_deploy_date(release, release_type):
    """
        Function to lookup the release and release_type actual deploy date from JSON 
    """
    retval = ("0000-00-00",0,0) 
    try:
        find_rel = "%s%s"% (release,release_type.lower())
        if find_rel in dash.Records.deployed_releases:
            retval = (dash.Records.release[release]["%s_deploy_string"%release_type.lower()],dash.Records.release[release]["%s_deploy_epoch"%release_type.lower()],dash.Records.release[release]["%s_record_num"%release_type.lower()])
    except KeyError:
        pass
    return retval

def find_missed_release(data):
    """
    find any release that is go out the door from the same throttle branch
    data = {
        'field' : <CR or CCR>,
        'value' : <value from the field>,
        'deploy_release' : <actual deploy release>,
        'deploy_time' : <actual deploy date>,
        'deploy_epoch' : <actual deploy time in epoch>,
        'last_commit_time' : <datetime>,
        'last_commit_epoch' : <datetime in epoch>
        }
        
    The new deploy date will update the 'deploy_time', 'deploy_epoch' and 'deploy_release'. As of now, 
    the deploy_time and deploy_release are picked from CCR or CR and their information
    are available in our dash.Records.release
    """
    if data['deploy_epoch'] == 0 and \
        data['last_commit_epoch'] == 0:
        data['deploy_release'] = NDY
        data['confidence_level'] = "HIGH"
    else:
        # Find the current throttle
        current_MN = None
        try:
            m = re.match(release_pattern, data['value'])
            current_MN = m.group("release")
        except:
            # no throttle value can be obtained, so the value is not Release
            data['confidence_leve'] =  "HIGH"
            return data
        other_rel_candidate = []
        point1 = data['last_commit_epoch']
        point2 = data['deploy_epoch']
        if point1 > point2:
            point2 = point1
        
        for dtm in dash.Records.sort_deployed_releases:
            # add the current release in CR/CCR
            if ("".join([dtm.values()[0]['relname'],dtm.values()[0]['type']])).lower() == data['value'].lower():
                other_rel_candidate.append(dtm)
                continue
            # just check the same throttle/release MN
            if (dtm.values()[0]['relname']) != current_MN:
                continue
            if dtm.values()[0]['epoch']>point1 and dtm.values()[0]['epoch']<=point2:
                # check if the throttle has change
                if (dtm.values()[0]['release_branch_type'] == 'actual'):
                    if (dtm.values()[0]['release_branch_epoch'] > point1): 
                        other_rel_candidate.append(dtm)
                else:
                    # not actual changed yet, so it should be okay
                    other_rel_candidate.append(dtm)
            # to reduce unnecessary iteration
            if dtm.values()[0]['epoch']>point2:
                break
        
        if len(other_rel_candidate) == 0 :
            # just use what we already get
            if data['deploy_epoch'] == 0:
                data['deploy_release'] = NDY
            data['confidence_level'] = "HIGH"
        else :
            # let's look the other candidate and sorted them based on the earliest epoch
            other_rel_candidate.sort(lambda x,y : cmp(x.values()[0]['epoch'], y.values()[0]['epoch']))
            
            # thing to update
            data['deploy_release'] = other_rel_candidate[0].keys()[0].upper()
            relname = other_rel_candidate[0].values()[0]['relname']
            type = other_rel_candidate[0].values()[0]['type']
            data['deploy_recnum'] = dash.Records.release[relname]['%s_record_num'%(type)]
            data['deploy_time'] = dash.Records.release[relname]['%s_deploy_string'%(type)]
            if data['deploy_recnum'] != 0:
                data['deploy_link'] = "https://{{ Releases.host }}{{ Releases.path }}/do/showView?tableName=Milestones&record_number=%s" % data['deploy_recnum']
            else:
                data['deploy_link'] = ""
            data['confidence_level'] = "MEDIUM"
            data['other_alternate_releases'] = other_rel_candidate
    return data


def run_app():
    report = PR_Deploy()  
    print "=============================================="
    report.run_test()
    print "=============================================="

def find_pr(pr_number):
    report = PR_Deploy()
    return report.run_report(pr_number)
     
if __name__ == '__main__':

    parser = OptionParser()
    # parameter for setting Server Connection
    parser.add_option('-H','--host', dest="hostname",
                      help='Host of the GNATS server to connect (default : gnats.juniper.net)',
                      default='gnats.juniper.net')
    parser.add_option('-D','--database', dest="dbname",
                      help='Database information to connect in GNATS (default : default) ',
                      default='default')
    parser.add_option('-P','--port', dest="port",
                      help='Port of the GNATS server to connect (default : 1529) ',
                      default='1529')

    # parameter for Query
    parser.add_option('-c','--columns', dest="columns",
                      help='Columns to be queried upon',
                      default="number,synopsis")
    parser.add_option('-x','--timestamp-columns', dest="timestamp_columns",
                      help='Columns to be queried upon',
                      default="number,synopsis")
    parser.add_option('-t','--table-columns', dest="t_columns",
                      help='Table Columns',
                      default=None)
    parser.add_option('-e','--expressions', dest="expr",
                      help='what kind of expressions that query will be working on',
                      default=None)
    parser.add_option('-r','--timestamp-expressions', dest="timestamp_expr",
                      help='what kind of expressions that query will be working on',
                      default=None)
    # The real reason for this standalone scripts
    parser.add_option('-T','--testing', dest="test",
                      help='Enable Unit testing',
                      default=False)

    (options, args) = parser.parse_args()
    
    print """
CONNECTING TO GNATS USING THIS PARAMETER:
   Database      = %s
   Host          = %s
   Port          = %s
   Columns       = %s
   Table Columns = %s
   Expr          = %s
    """ % (options.dbname, options.hostname, options.port, options.columns, options.t_columns,options.expr)
  
    report = PR_Deploy(options, args)  
    print "=============================================="
    report.run_test()
    print "=============================================="
    

