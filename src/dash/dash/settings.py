# Django settings for dash project.
import logging
import os
import sys
import cx_Oracle

from datetime import timedelta

# FIXME: Automate populating the following two variables
VERSION = '2013.11R3'
RELEASE_DATE = '2013-11-19'

# set to True when you want to run in debug mode
DEBUG = False

TEMPLATE_DEBUG = DEBUG

# configuration for tableau tag 
TABLEAU_SERVER = 'https://tableau.juniper.net/'

# what mode should the server run in.
# default: "normal" for use when using the development server
# instance1: run as instance1
# instance2: run as instance2
# proxy  : proxy requests based on loading status of instances 
MODE = os.environ.get('DASH_MODE','default')

if MODE == 'proxy':
    SKIP_JSON_LOAD = True

EDW_MTTR_URL_BASE = 'http://127.0.0.1:8180/dashws/mttr/'

lock_location = '/opt/dashboard/special'

ALLOWED_HOSTS = ['*']

# SPECIAL_DIR points to the folder which contains bg-list.properties file.
# This is being used in Client-Side MTTR Code.
SPECIAL_DIR = '/opt/dashboard/special'

# all instance information for keeping one instance in a non-loading state
INSTANCE_INFO = {'instance1': {
                               'lock_file': os.path.join(lock_location, 'instance1.lock'),
                               #
                               'host': ('127.0.0.1', 8081)
                               },
                 'instance2': {
                               'lock_file' : os.path.join(lock_location, 'instance2.lock'),
                               'host'      : ('127.0.0.1', 8082)
                               }
                }

# lock file will be deleted by proxy after 3 hours
MAX_LOADING_TIME = timedelta(0, 60*60*3)

# this is only needed when running the development server.  when going through Apache, a REMOTE_USER will
# always be set by Apache based on the LDAP authentication
# this is needed for Tableau to work correctly
DEFAULT_REMOTE_USER = 'dashboard'

# set to DEBUG when you want debug output in the log file
LOGGING_LEVEL = logging.DEBUG

LOGGING_FORMAT = "[%%(levelname)s][%s] %%(message)s" % MODE
LOGGING_DATEFMT = "%m-%d %H:%M:%S"
LOGGING_LOGFILE = '/tmp/dashboard.log'

# this will log to apache error log or to the screen when using runserver
logging.basicConfig(level=LOGGING_LEVEL,
                    format=LOGGING_FORMAT,
                    datefmt=LOGGING_DATEFMT,
                    stream=sys.stdout
                   )

LOG_FILE = '/opt/dashboard/logs/ui.log'

# This allows us to have multiple copies running under mod_wsgi
DASH_URL_BASE = 'dashboard'
JSON_PATH = '/opt/dashboard/json'

_conn_file = open(os.path.join(lock_location, 'dash-db.properties'))
CONN_PROPS = {}
for line in _conn_file:
     name, value = line.split('=')
     CONN_PROPS[name] = value.strip()

DATABASES = {
    'default': {
        'ENGINE' : 'django.db.backends.oracle',
        'NAME' : CONN_PROPS['tns_entry'],
        'USER' : CONN_PROPS['user'],
        'PASSWORD': CONN_PROPS['password']
    }
}

ADMINS = (
    ('mbasha', 'mbasha@juniper.net'),
)
MANAGERS = ADMINS

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# If running in a Windows environment this must be set to the same as your
# system time zone.
TIME_ZONE = 'America/Los_Angeles'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'en-us'

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# Absolute path to the "document root", in which you can find css, js, etc.
DOC_ROOT = os.path.join(os.path.dirname(__file__), 'ui')

# Absolute path to the directory that holds media.
# Example: "/home/media/media.lawrence.com/"
MEDIA_ROOT = os.path.normpath(os.path.join(DOC_ROOT, "inc"))

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash if there is a path component (optional in other cases).
# Examples: "http://media.lawrence.com", "http://example.com/media/"
MEDIA_URL = '/' + DASH_URL_BASE + '/inc/'

# URL prefix for admin media -- CSS, JavaScript and images. Make sure to use a
# trailing slash.
# Examples: "http://foo.com/media/", "/media/".
ADMIN_MEDIA_PREFIX = '/media/'

# Make this unique, and don't share it with anybody.
SECRET_KEY = 'wxf-ah9xkqw%j$iw^36n7*+jd))!7gu=6anjjd=r6t!1^@&l^w'

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    #'django.template.loaders.filesystem.load_template_source',
    #'django.template.loaders.app_directories.load_template_source',
    #'django.template.loaders.eggs.load_template_source',
    # For django >= 1.2
    'django.template.loaders.app_directories.Loader',
    'django.template.loaders.eggs.Loader',
)

MIDDLEWARE_CLASSES = (
   'django.middleware.common.CommonMiddleware',
   'django.contrib.sessions.middleware.SessionMiddleware',
   'django.contrib.auth.middleware.AuthenticationMiddleware',
   'dash.DashProxyRemoteUserMiddleWare',
#    'django.contrib.auth.middleware.RemoteUserMiddleware',
   'dash.ui.DashExceptionMiddleware',
   'dash.DashLoadCheckMiddleware',
)

AUTHENTICATION_BACKENDS = (
   'django.contrib.auth.backends.RemoteUserBackend',
)

ROOT_URLCONF = 'dash.urls'

TEMPLATE_DIRS = (
    # Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
)

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.admin',
    'compress',
    'dash.ui',
    'dash.pr_date',
    'dash.junos_ops',
)

TEMPLATE_CONTEXT_PROCESSORS = (
    # For django < 1.2
    # "django.core.context_processors.auth",
    # For Django >= 1.2
    "django.contrib.auth.context_processors.auth",
    "django.core.context_processors.debug",
    "django.core.context_processors.media",
    "django.core.context_processors.request",
    'dash.ui.context_processor'
)

# css and js munging, courtesy of django-compress
# http://code.google.com/p/django-compress/
COMPRESS_CSS = {
    'junosops' : {
        'source_filenames': (
                'styles/jquery.treeview.css',
                'styles/dash.css',
                'styles/jquery.autocomplete.css',
                'styles/superfish.css',
                '../../junos_ops/inc/css/datepicker.min.css',
                '../../junos_ops/inc/css/custom-bootstrap.min.css',
                '../../junos_ops/inc/css/tabdrop.css',
                '../../junos_ops/inc/css/custom-bootstrap-responsive.min.css',
                '../../junos_ops/inc/css/junosops.css'
                ),
        'output_filename': 'compressed/junosops-styles-%s.?.css' % MODE,
        'extra_context': {
            'media': 'screen,projection',
        },
    },
    'edw': {
        'source_filenames': (
                             'edw/css/edw.css',
                             ),
        'output_filename': 'compressed/edwstyles-%s.?.css' % MODE,
        'extra_context': {
            'media': 'screen,projection',
        },
    },
    'main': {
        'source_filenames': (
                             'styles/ui.core.css',
                             'styles/ui.theme.css',
                             'styles/ui.dialog.css',
                             'styles/ui.progressbar.css',
                             'styles/ui.tabs.css',
                             'styles/jquery.autocomplete.css',
                             'styles/jquery.treeview.css',
                             'styles/dash.css',
                             'styles/pr_date.css',
                             'styles/superfish.css',
                             'styles/visualize.jQuery.css',
                             'styles/component.css',
                             'styles/qir.css',
                             'styles/rating.css'
                
                             ),
        'output_filename': 'compressed/styles-%s.?.css' % MODE,
        'extra_context': {
            'media': 'screen,projection',
        },
    },
    'print': {
        'source_filenames': ('styles/print.css',
                             'styles/visualize.jQuery.css',
                             ),
        'output_filename': 'compressed/print-%s.?.css' % MODE,
        'extra_context': {
            'media': 'print',
        },
    },
    'ie': {
        'source_filenames': ('styles/ie.css',),
        'output_filename': 'compressed/ie-%s.?.css' % MODE,
    },
}

COMPRESS_JS = {
   'edw-libs': {
        'source_filenames': (
                             'edw/lib/jquery.min.js',
                             'edw/lib/jquery.iecors.js',
                             'edw/lib/json3.min.js',
                             'edw/lib/underscore.min.js',
                             'edw/dash/global.js'
                             ),
        'output_filename': 'compressed/edw-libs-scripts-%s.?.js' % MODE,
    },        
    'edw': {
        'source_filenames': (
                             'edw/dash/rule.comp.js',
                             'edw/rule-box.js',
                             'edw/dash/rule_total.comp.js',
                             'edw/rule-total-box.js'
                             ),
        'output_filename': 'compressed/edwscripts-%s.?.js' % MODE,
    },                         
    'junosops' : {
        'source_filenames': (
                             '../../junos_ops/inc/js/jquery-1.9.1.min.js',
                             'jquery/jquery.treeview.js',
                             'jquery/jquery.cookie.js',
                             'jquery/tableauUtil.js',
                             'jquery/jquery.hoverIntent.js',
                             'jquery/jquery.autocomplete.js',
                             'jquery/jquery.qtip.js',
                             'jquery/jquery.bgiframe.js',
                             'jquery/superfish.js',
                             '../../junos_ops/inc/js/bootstrap.min.js',
                             '../../junos_ops/inc/js/jquery-migrate-1.2.1.min.js',
                             '../../junos_ops/inc/js/bootstrap-tabdrop.js',
                             '../../junos_ops/inc/js/date.min.js',
                             '../../junos_ops/inc/js/bootstrap-datepicker.min.js',
                             '../../junos_ops/inc/js/junosops.js',
                             '../../junos_ops/inc/js/highcharts/highstock.js',
                             '../../junos-ops/inc/js/highcharts/highcharts-exporting.js'
                             ),
        'output_filename': 'compressed/junosops-scripts-%s.?.js' % MODE,
    },
    'ie8' : {
        'source_filenames': (
                             '../../junos_ops/inc/js/html5.js',
                             '../../junos_ops/inc/js/respond.js'
                            ),
        'output_filename': 'compressed/ie8-%s.?.js' % MODE,
    },
    'main': {
        'source_filenames': (
                             'jquery/jquery.js',
                             'jquery/jquery.json-2.2.js',
                             'jquery/ui.js',
                             'jquery/jquery.autocomplete.js',
                             'jquery/jquery.bgiframe.js',
                             'jquery/jquery.hoverIntent.js',
                             'jquery/superfish.js',
                             'jquery/jquery.qtip.js',
                             'jquery/jquery.cookie.js',
                             'jquery/jquery.treeview.js',
                             'jquery/jquery.tablesorter.js',
                             'jquery/visualize.jQuery.js',
                             'jquery/qir.js',
                             'jquery/component.js',
                             'jquery/tableauUtil.js',
                             'dash.js',
                             'rating.js',
                             ),
        'output_filename': 'compressed/scripts-%s.?.js' % MODE,
    },
    'ie': {
        'source_filenames': ('jquery/excanvas.compiled.js',),
        'output_filename': 'compressed/ie-%s.?.js' % MODE,
    },
}
# Add a version-number string to the output filename (in place of the "?")
COMPRESS_VERSION = True
COMPRESS = True
# Use the bundled Python iplementation of csstidy
COMPRESS_CSS_FILTERS = ('dash.ui.cssmin.CssMiniFilter',)
COMPRESS_CSSMINIFILTER_URL_PREFIX = '../styles'

# Override values with local settings
import sys
sys.path.append(lock_location)
try:
    from dash_local_settings import *
    adjust_settings(globals())
except Exception, e:
    print e

SLA_PROPS = {}

sla_prop_file = open(os.path.join(lock_location, 'sla.properties'))

for line in sla_prop_file:
    if line.startswith('ORACLE_DASH_') or line.startswith('GNATS_SEARCH_LINK_') or line.startswith('QUERY_DASH') or line.startswith('PRODUCT') or line.startswith('SLA_VERSION') or line.startswith('PAGE_SUBJECT') or line.startswith('SLAJOB_') or line.startswith('TABLEAU_'):
        idx = line.find('=')
        SLA_PROPS[line[0:idx]] = line[idx+1:].strip()

ORACLE_DASH_POOL = cx_Oracle.SessionPool(
    user = SLA_PROPS['ORACLE_DASH_USER'],
    password = SLA_PROPS['ORACLE_DASH_PASSWD'],
    dsn = SLA_PROPS['ORACLE_DASH_SERVICE_NAME'],
    min = int(SLA_PROPS['ORACLE_DASH_POOL_MIN_CONNECT']),
    max = int(SLA_PROPS['ORACLE_DASH_POOL_MAX_CONNECT']),
    increment = 1,
    connectiontype = cx_Oracle.Connection,
    threaded = True,
    getmode = cx_Oracle.SPOOL_ATTRVAL_NOWAIT,
    homogeneous = True)

ORACLE_DASH_POOL.timeout = int(SLA_PROPS['ORACLE_DASH_POOL_CONNECT_TIMEOUT'])

#CRS
crs_props_file = open(os.path.join(lock_location, 'crs.properties'))
CRS_PROPS = {}
for line in crs_props_file:
    if '=' in line and not line.startswith('#'):
       name, value = line.split('=')
       CRS_PROPS[name] = value.strip()
