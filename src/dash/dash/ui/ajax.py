"""
AJAX Views for Dashboard

Dirk Bergstrom, dirk@juniper.net, 2009-05-04

Copyright (c) 2009, Juniper Networks, Inc.
All rights reserved.
"""
import cjson
import datetime
import logging
# standard urllib library to make http request
import urllib, urllib2

from django.conf import settings
from django.http import HttpResponse

import dash

def user(request, ac_type='1'):
    """ Autocomplete for usernames.

    If ac_type == 1, return output suitable for the simple autocomplete plugin,
    else return output for the tokenized multiple autocomplete plugin.
    TODO remove no longer needed tokenized output style.
    """
    if ac_type == '1':
        empty = ''
        mimetype = 'text/plain'
    else:
        empty = []
        mimetype = 'application/javascript'
    q = request.GET.get('q', '')
    limit = request.GET.get('limit', '')
    q = q.strip()
    if not q:
        return HttpResponse(empty, mimetype=mimetype)
    unames = [u for u in dash.User.all.keys() if u.find(q) > -1]
    unames.sort()
    if limit:
        try:
            lim = int(limit)
            unames = unames[:lim]
        except ValueError:
            pass
    if ac_type == '1':
        out = '\n'.join(unames)
    else:
        out = cjson.encode([{'id': u, 'name': u} for u in unames])
    return HttpResponse(out, mimetype=mimetype)

def generate_tableau(request, workbook, reportname,
                     parameter = None,
                     server_name = settings.TABLEAU_SERVER):
    # must be http or https
    if not server_name.startswith('http') :
        server_name = "https://" + server_name
    
    trusted_ticket = -1
    try :
        trusted_ticket = __generate_tableau_ticket(request.user.username, server_name)
    except KeyError:
        logging.debug(">>>>> no username found!")
        pass
    
    if str(trusted_ticket) != '-1':
        server_url = server_name + '/trusted/' + str(trusted_ticket) + '/views/' + workbook + '/' + reportname
    else:
        server_url = server_name + '/views/' + workbook + '/' + reportname
        
    # this parameter is considered standard for now
    server_url += '?:embed=yes&:comments=no'
    if parameter != None :
        server_url += '&' +parameter

    return HttpResponse(server_url, 'text/plain')

def __generate_tableau_ticket(uid, server_name):
    ticket = -1
    try :
        # send POST request to tableau server ONLY
        values = {'username' : uid,
              }
        ticket = -1
        try:
            data = urllib.urlencode(values)
            req = urllib2.Request(server_name + '/trusted' , data)
            response = urllib2.urlopen(req)
            ticket = response.read()
        except Exception, detail:
            logging.error(detail)
    except KeyError:
        logging.debug(">>> cannot find username for generating tableau ticket!")
        pass
    return ticket
