#models.py
from datetime import datetime
from django.db import models
from django.utils.translation import gettext_lazy as _

class MotdMessageManager(models.Manager):
    def active_messages(self):
        dtnow = datetime.utcnow()
        return super(MotdMessageManager, self).get_query_set().filter(
            enabled=True, start_time__lte=dtnow, end_time__gte=dtnow)

class MotdMessage(models.Model):
    enabled = models.BooleanField(default=True)
    start_time = models.DateTimeField()
    end_time = models.DateTimeField()
    message = models.CharField(max_length=400)
    body = models.TextField(max_length=2000, blank=True,
         help_text="""Optional body, will be displayed once per user, click
         to expand thereafter.""")

    objects = MotdMessageManager()

    class Meta:
        ordering = ['start_time']

    def __unicode__(self):
        return u'%s' % self.message

class HardeningReport(models.Model):
    rel_type = models.CharField(max_length=5)
    product = models.CharField(max_length=500)
    source = models.CharField(max_length=500)
    planned_rel = models.CharField(max_length=100)
    customer = models.CharField(max_length=500)
    technology = models.CharField(max_length=500)
    pr_numbers = models.TextField(max_length=3000, blank=True)
    keywords = models.TextField(max_length=500)
    counts = models.IntegerField(default=0)
    rule_type = models.CharField(max_length=10)
    modified_on = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return u'%s' % self.rel_type

class Rating(models.Model):
    username = models.CharField(max_length=200)
    rating = models.IntegerField(max_length=1)
    topopportunity= models.CharField(max_length=200)
    votedatentime=models.DateTimeField()
    activevote=models.BooleanField()
    
