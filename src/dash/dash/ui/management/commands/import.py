from django.core.management import BaseCommand, CommandError
from optparse import make_option

class Command(BaseCommand):
    help = ('import specified module in application context')
    args = '[module]'
    
    requires_model_validation = True
    can_import_settings = True
    
    def handle(self, *args, **options):
        if len(args) != 1:
            return self.handle_error('import takes exactly one argument, the module to import')
        
        module = args[0]
        try:
            __import__(module, globals(), locals())
        except ImportError, e:
            if str(e) == 'No module named %s' % module:
                return self.handle_error('module %s was not found' % module)
            raise
        
        