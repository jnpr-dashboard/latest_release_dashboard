# ----
# override the built in runserver so that we can force the JSON files to load
# at start time.  See how this is done at the bottom of dash.__init__

import os

from django.conf import settings
from django.core.management.commands.runserver import Command as RunserverCommand

import dash

class Command(RunserverCommand):
    def handle(self, addrport='', *args, **options):
        print '*** AUTO RELOAD IS OFF.  You must use CONTROL-C and rerun to see code changes. ***'
        print 'This is done because of JSON load overhead'
        print 'Template changes do not require a restart'
        # do initial data load
        dash.async_load_all(settings.JSON_PATH)
        # do normal runserver stuff
        # force --noreload option because of JSON data load times
        print 'Running mode: %s' % settings.MODE
        options['use_reloader'] = False
        super(Command, self).handle(addrport=addrport, *args, **options)
