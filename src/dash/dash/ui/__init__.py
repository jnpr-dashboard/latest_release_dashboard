"""
Base functionality for Dashboard.

Dirk Bergstrom, dirk@juniper.net, 2009-03-31

Copyright (c) 2009, Juniper Networks, Inc.
All rights reserved.
"""
import os
import sys

from django.conf import settings
from django.shortcuts import render_to_response
from django.template.context import RequestContext
from django.core.urlresolvers import reverse, NoReverseMatch

import dash
from dash.ui.models import MotdMessage
from dash import DashError

# Exceptions to pay attention to
FATAL_EXCEPTIONS = (SystemExit, KeyboardInterrupt, MemoryError)

def context_processor(request):
    """ Adds commonly used information to the request context. """
    try:
        user = dash.User.all[request.user.username]
    except KeyError:
        user = dash.User.JUNOS

    messages = MotdMessage.objects.active_messages()

    context = \
        {
         'version': settings.VERSION,
         'release_date': settings.RELEASE_DATE,
         'active_releases': dash.Records.active_releases,
         'in_flight_releases': dash.Records.in_flight_releases,
         'pr_fix_releases': dash.Records.pr_fix_releases,
         'grounded_releases': dash.Records.grounded_releases,
         'schedule': dash.Records.schedule[:20],
         'is_mozilla': request.META.get('HTTP_USER_AGENT', '').find('Gecko/') > 0,
         'the_user': user,
         'junos': dash.User.JUNOS,
         'messages': messages,
         'summary_count': 5, # Number of summaries shown in broken-rule.html
        }
    context.update(dash.DataSource.all)
    return context


class DashExceptionMiddleware(object):
    """ Handle various custom exceptions gracefully. """

    def process_exception(self, request, exception):
        if type(exception) == dash.DashError:
            return render_to_response('ui/error.html', {}, RequestContext(request, {
                'title':'Error',
                'error_message':exception.args[0],
                'details': '',
            }))
        else:
            return None

class DashUtil(object):
    """ Make a url using the Django reverse method.

    pattern_name is the name of the url pattern in urls.py.
    args is a list of params to the url pattern.
    """
    @staticmethod
    def make_url(pattern_name, args=None):
        url = ""
        if args is None:
            args = []
        try:
            url = reverse(pattern_name, args=args)
        except NoReverseMatch:
            try:
                url = reverse(settings.DASH_URL_BASE + '.' + pattern_name, args=args)
            except NoReverseMatch:
                msg = "Can't reverse resolve URL pattern \"%s\"." % pattern_name
                raise DashError(msg)
        return url
    
    @staticmethod
    def isBranchExist(branch_name):
        exist = True
        try: 
            rel = dash.User.all['junos'].statuses[branch_name]
        except KeyError:
            exist = False
        if branch_name == '':
            exist = False
        return exist
