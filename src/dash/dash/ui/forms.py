"""
Forms for Dashboard UI.

Dirk Bergstrom, dirk@juniper.net, 2009-04-16

Copyright (c) 2009, Juniper Networks, Inc.
All rights reserved.
"""
from itertools import chain

from django import forms
from django.forms.widgets import SelectMultiple, Select
from django.utils.html import escape, conditional_escape
from django.utils.encoding import force_unicode
from django.forms.util import flatatt
from django.utils.safestring import mark_safe

import dash
from dash.ui import DashUtil

class FakeSelect(Select):
    """ The render() method returns just the text value, for use in complex
    templates.
    """

    def render(self, name, value, attrs=None, choices=()):
        if value is None or not value:
            value = u''
        return value


class ListSelect(SelectMultiple):
    """ Render an html UL element that will be used as a select.

    Unpacks comma/space separated values in selected_choices during render.
    Will choke spectacularly if there are optgroups in the choices...
    """

    def render(self, name, value, attrs=None, choices=()):
        if value is None: value = []
        final_attrs = self.build_attrs(attrs, name=name)
        output = [u'<ul%s>' % flatatt(final_attrs)]
        options = self.render_options(choices, value)
        if options:
            output.append(options)
        output.append('</ul>')
        return mark_safe(u'\n'.join(output))

    def render_options(self, choices, selected_choices):
        def render_option(option_value, option_label):
            option_value = force_unicode(option_value)
            return u'<li val="%s"><span>&nbsp;X&nbsp;</span>%s</li>' % (
                escape(option_value),
                conditional_escape(force_unicode(option_label)))
        # If there's only one selected_choice, split it on comma/space.
        if selected_choices and len(selected_choices) == 1:
            selected_choices = selected_choices[0].replace(',', ' ').split()
        # Normalize to strings.
        selected_choices = [force_unicode(v) for v in selected_choices]
        choice_dict = dict([(value, label) for value, label in
                            chain(self.choices, choices)])
        output = []
        for opt in selected_choices:
            if opt in choice_dict:
                output.append(render_option(opt, choice_dict[opt]))
        return u'\n'.join(output)


class SelectMultipleCommaSepd(SelectMultiple):
    """ Handles comma-separated values. """

    def render(self, name, value, attrs=None, choices=()):
        return super(self.__class__, self).\
            render(name, _parse_comma_sepd(value), attrs=attrs, choices=choices)


class MultipleChoiceFieldAcceptsCommaSepd(forms.MultipleChoiceField):
    """ A MultipleChoiceField that can accept a comma-separated string of
    valid values.
    """
    def clean(self, value):
        """ If value is a comma or space separated string, split it into a list
        before validating.
        """
        return super(self.__class__, self).clean(_parse_comma_sepd(value))

def _parse_comma_sepd(value):
    """ Given a string or a one-element list, returns a list split on commas,
    otherwise returns the original value. """
    if isinstance(value, basestring):
        value = value.replace(',', ' ').split()
    elif isinstance(value, (list, tuple)) and len(value) == 1:
        value = value[0].replace(',', ' ').split()
    return value


class MatrixForm(forms.Form):
    """ Django Form for the "build a matrix" input.

    FIXME need to make releases & rules more dynamic.
    """

    def __init__(self, *args, **kwargs):
        super(MatrixForm, self).__init__(*args, **kwargs)
        # Need these values to be dynamic at runtime.
        choice_rel = []
        # FIXME later for better dynamic values
        for r in dash.Records.active_releases:
            n = dash.findNumericRelease(r)
            choice_rel.append((r,r))
            if (float(n) >= dash.cdm_release_cutoff):
                for v_cdm in dash.Records.release[r]['childBranch_array']:
                    if (DashUtil.isBranchExist(str(v_cdm))):
                        choice_rel.append((str(v_cdm),"..."+str(v_cdm)))

        # for single_release
        choice_rel.insert(0,('',''))
        self.fields['release_id'].choices = choice_rel 

        # for multiple
        choice_rel.remove(('',''))
        choice_rel.insert(0,('all','all'))
        self.fields['releases'].choices= choice_rel
                    
        self.fields['rules'].choices = \
            [(r.id, r.name) for r in dash.Rule.all.values()]

    users = forms.RegexField(required=False,
        regex=r'^[\w\-]+([, ]+[\w\-]+)*[, ]*$',
        widget=forms.TextInput(attrs={'size': 70}))
    reports = forms.BooleanField(required=False)
    orient = forms.ChoiceField(required=False,
        widget=FakeSelect(),
        initial='uxm',
        choices=(('uxm', 'users across, metrics down'),
                 ('mxu', 'metrics across, users down')))
    release_id = forms.ChoiceField(
        choices=[], # Set in __init__
        initial='',
        required=False)
    feedback = forms.ChoiceField(
        choices=(('yes', 'include feedback PRs'), ('all', 'include monitored, suspended, feedback PRs'),('no', 'exclude monitored, suspended and feedback PRs'),
                 ('only', 'feedback PRs only'),('monitored', 'monitored PRs only'),('suspended', 'suspended PRs only'),
                 ('inreport', 'Reported-in PRs Only'),('backlog', 'show NOT Reported-in PR')
                 ),
        initial='yes',
        required=False)
    found_during = forms.ChoiceField(
        choices = (('all', 'All'),
                   ('fdvals', 'Found during *(see found duing values below)')),
        initial='all',
        required=False)
    releases = MultipleChoiceFieldAcceptsCommaSepd(
        widget=SelectMultipleCommaSepd(attrs={'size': 9}),
        choices=[], # Set in __init__
        initial=['all'],
        required=False)
    title = forms.CharField(max_length=100, required=False,
        widget=forms.TextInput(attrs={'size': 70}))
    rules = MultipleChoiceFieldAcceptsCommaSepd(
        widget=ListSelect(attrs={'size': 10}),
        choices=[], # Set in __init__
        required=False)
