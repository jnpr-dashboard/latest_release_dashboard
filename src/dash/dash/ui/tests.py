"""
Unit tests for Dashboard

Dirk Bergstrom, dirk@juniper.net, 2009-05-15

Copyright (c) 2009, Juniper Networks, Inc.
All rights reserved.
"""
import os
from urllib import unquote_plus

from django.test import TestCase as DjangoTestCase

os.environ['TESTING_MODE'] = 'fred!'

import dash

# Pull in the tests from the dash package
from dash.tests import *

class DashTestCase(DjangoTestCase):

    fixtures = ['auth.json']

    def setUp(self):
        clear_dash_caches()
        dash.load_all(os.path.join(os.path.dirname(__file__), 'tests'))

    def login(self, uid):
        login = self.client.login(username=uid, password='secret')
        self.failUnless(login, 'Could not log in as "%s"' % uid)

    def get_page(self, path, code=200):
        # The params stuff can be removed when the next (1.1?) version of
        # Django ships, since it will automatically parse query strings.
        params = {}
        if path.find('?') > -1:
            path, qs = path.split('?')
            params = dict([kv.split('=') for kv in unquote_plus(qs).split('&')])
        response = self.client.get('/dashboard/' + path, params)
        self.failUnlessEqual(response.status_code, code)
        return response

    def post_page(self, path, params):
        response = self.client.post('/dashboard/' + path, params)
        self.failUnlessEqual(response.status_code, 200)
        return response


class T01_Views(DashTestCase):

    def test_01_context_processor(self):
        """ Check the objects from the context processor middleware. """
        self.login('ina')
        response = self.get_page('help')
        self.assertTemplateUsed(response, 'docs/help.html')
        self.assertEquals(response.context[0]['the_user'].id, 'ina')
        self.assertEquals(response.context[0]['junos'], dash.User.JUNOS)
        self.assertEquals(response.context[0]['active_releases'][0], '10.4')
        self.assertEquals(response.context[0]['in_flight_releases'][0], '10.1')
        self.assertEquals(response.context[0]['grounded_releases'][-1], '8.1')
        self.assertEquals(response.context[0]['schedule'][0].event, 'B2 Respin')
        self.assertFalse(response.context[0]['is_mozilla'])

    def test_02_help(self):
        """ Help page. """
        self.login('ina')
        response = self.get_page('help')
        self.assertTemplateUsed(response, 'docs/help.html')

    def test_03_error(self):
        """ Error page. """
        self.login('ina')
        response = self.get_page('rule/notarule')
        self.assertTemplateUsed(response, 'ui/error.html')
        self.assertEquals(response.context[0]['error_message'],
                          'Rule "notarule" not found')

    def test_04_json_ready_admin_only(self):
        """ JSON loading. """
        self.login('ina')
        response = self.get_page('json-ready')
        self.assertEquals(response.context[0]['error_message'],
                          'Admistrator-only function')

    def test_04_json_ready(self):
        """ JSON loading. """
        x = []
        old_load_all = dash.load_all
        def nla(path):
            x.append(path)
        try:
            dash.load_all = nla
            self.login('admin')
            response = self.get_page('json-ready')
            self.assertEqual(response.content, 'ok')
            self.assertTrue(x)
        finally:
            dash.load_all = old_load_all

    def test_05_npis(self):
        """ NPIs page. """
        response = self.get_page('npis')
        self.assertTemplateUsed(response, 'ui/npis.html')
        self.assertEqual(len(response.context[0]['npis']), 1)
        self.assertEqual(response.context[0]['npis'][0].id, 'npi-540')

    def test_05_rule_list(self):
        """ List of rules. """
        response = self.get_page('rules')
        self.assertTemplateUsed(response, 'ui/rule.html')
        self.assertEqual(len(response.context[0]['rules']), 70)
        self.assertEqual(response.context[0]['rules'][0].id, 'prs_for_npi')

    def test_06_rule(self):
        """ View a rule. """
        response = self.get_page('rule/at_risk')
        self.assertTemplateUsed(response, 'ui/rule.html')
        self.assertEqual(len(response.context[0]['rules']), 70)
        self.assertEqual(response.context[0]['rule'].id, 'at_risk')

    def test_07_bsc(self):
        """ BSC page for release 9.6. """
        self.login('ina')
        self.fail("Not yet implemented")
        # FIXME need to get BU and BG users into the test data.  Maybe just
        # as stubs?
        #self.assertTemplateUsed(response, 'docs/bsc-help.html')
        #response = self.get_page('bsc-help/9.6')
        #self.assertEqual(response.context[0]['data']['eabu'], 'foo')

    def test_08_bad_release(self):
        """ Nonexistent release. """
        self.login('ina')
        response = self.get_page('release/9.9/ina')
        self.assertTemplateUsed(response, 'ui/error.html')
        self.assertEquals(response.context[0]['error_message'],
                          'Release "9.9" not found')

    def test_09_release(self):
        """ Release page. """
        self.login('ina')
        response = self.get_page('release/9.6/ina')
        self.assertTemplateUsed(response, 'ui/release.html')
        self.assertEqual(response.context[0]['usr'].id, 'ina')
        self.assertEqual(response.context[0]['status'].release_id, '9.6')
        self.assertEqual(response.context[0]['status'].total_score, 224)
        self.assertEqual(len(response.context[0]['status'].scores), 17)
        self.assertEqual(response.context[0]['release_id'], '9.6')
        self.assertEqual(response.context[0]['release']['st_responsible'], 'jshaw')
        self.assertEqual(response.context[0]['show_count_only'], False)
        self.assertEqual(response.context[0]['broken_rule_displays_release'], False)

    def test_10_agenda(self):
        """ User agenda. """
        self.login('steven')
        response = self.get_page('agenda')
        self.assertTemplateUsed(response, 'ui/agenda.html')
        self.assertEqual(response.context[0]['usr'].id, 'steven')
        self.assertEqual(response.context[0]['release_id'], False)
        self.assertEqual(response.context[0]['show_count_only'], False)
        self.assertEqual(response.context[0]['broken_rule_displays_release'], True)

    def test_11_user_agenda(self):
        """ Agenda for other user. """
        self.login('steven')
        response = self.get_page('agenda/ina')
        self.assertTemplateUsed(response, 'ui/agenda.html')
        self.assertEqual(response.context[0]['usr'].id, 'ina')

    def test_12_dwim_user_agenda(self):
        """ DWIM for agenda. """
        self.login('steven')
        response = self.get_page('dwim?s=ina')
        self.assertTemplateUsed(response, 'ui/agenda.html')
        self.assertEqual(response.context[0]['usr'].id, 'ina')

    def test_13_dwim_release(self):
        """ DWIM for release for logged in user. """
        self.login('steven')
        response = self.get_page('dwim?s=9.6')
        self.assertTemplateUsed(response, 'ui/release.html')
        self.assertEqual(response.context[0]['usr'].id, 'steven')

    def test_14_dwim_release_user(self):
        """ DWIM for release for other user. """
        self.login('steven')
        response = self.get_page('dwim?s=9.6%20ina')
        self.assertTemplateUsed(response, 'ui/release.html')
        self.assertEqual(response.context[0]['usr'].id, 'ina')
        response = self.get_page('dwim?s=ina%209.6')
        self.assertTemplateUsed(response, 'ui/release.html')
        self.assertEqual(response.context[0]['usr'].id, 'ina')

    def test_15_dwim_prs(self):
        """ DWIM for pr matrix for logged in user. """
        self.login('steven')
        response = self.get_page('dwim?s=prs')
        self.assertTemplateUsed(response, 'ui/matrix.html')
        self.assertEqual(response.context[0]['users'][0].id, 'steven')
        self.assertEqual(response.context[0]['users'][1].id, 'ina')

    def test_16_dwim_prs_user(self):
        """ DWIM for pr matrix for other user. """
        self.login('steven')
        response = self.get_page('dwim?s=ina%20prs')
        self.assertTemplateUsed(response, 'ui/matrix.html')
        self.assertEqual(response.context[0]['users'][0].id, 'ina')
        response = self.get_page('dwim?s=prs%20ina')
        self.assertTemplateUsed(response, 'ui/matrix.html')
        self.assertEqual(response.context[0]['users'][0].id, 'ina')

    def test_17_dwim_pr(self):
        """ DWIM for PR number. """
        response = self.get_page('dwim?s=pr%201234', code=302)
        self.failUnless(response['Location'].
                        find('%s/%s' % (dash.ui.views.GNATS_BASE_URL, 1234)) > -1)

    def test_18_dwim_prs(self):
        """ DWIM for multiple PR numbers. """
        response = self.get_page('dwim?s=pr%201234,5432', code=302)
        self.failUnless(response['Location'].find('do-query?prs=1234,5432') > -1)

    def test_19_dwim_rli(self):
        """ DWIM for RLI number. """
        response = self.get_page('dwim?s=rli%201234', code=302)
        self.failUnless(response['Location'].find('record_number=1234') > -1)
        self.failUnless(response['Location'].find('showView') > -1)

    def test_20_dwim_rlis(self):
        """ DWIM for multiple RLI numbers. """
        response = self.get_page('dwim?s=rli%201234,5432', code=302)
        self.failUnless(response['Location'].find('record_number=1234,5432') > -1)
        self.failUnless(response['Location'].find('/query') > -1)

    def test_21_dwim_error(self):
        """ Unparseable DWIM request. """
        response = self.get_page('dwim?s=your%20mama')
        self.assertTemplateUsed(response, 'ui/error.html')
        self.assertTrue(response.context[0]['error_message'].
                          find('Sorry') > -1)


class T02_BasicMatrix(DashTestCase):

    def test_01_matrix_form(self):
        """ Matrix form. """
        response = self.get_page('matrix-form/')
        self.assertTemplateUsed(response, 'ui/matrix-form.html')
        self.assertEqual(len(response.context[0]['rules']), 69)
        self.assertEqual(response.context[0]['rules'][0].id, 'prs_for_npi')
        from dash.ui.forms import MatrixForm
        self.assertTrue(isinstance(response.context[0]['form'], MatrixForm))

    matrix_params = {
        'rules': 'at_risk,critical_prs,beta_blocker_prs',
        'users': 'steven,ina',
        'reports': 'no',
        'orient': 'uxm',
        'release_id': '9.6',
        'releases': 'all',
        'feedback': 'yes',
        'title': 'whatisthematrix',
    }

    def test_02_matrix_no_rules(self):
        """ Matrix with no rules selected. """
        params = dict(self.matrix_params, rules='')
        response = self.post_page('matrix/', params)
        self.assertTemplateUsed(response, 'ui/matrix-form.html')
        self.assertEqual(response.context[0]['form'].errors['rules'],
                         "No rules supplied.")

    def test_03_matrix_bad_rule(self):
        """ Matrix with non-existent rule. """
        params = dict(self.matrix_params, rules='XXX')
        response = self.post_page('matrix/', params)
        self.assertTemplateUsed(response, 'ui/matrix-form.html')
        self.assertTrue(response.context[0]['form'].errors['rules'][0].find('XXX') > 0)

    def test_04_matrix_bad_user(self):
        """ Matrix with bad username. """
        params = dict(self.matrix_params, users='XXX')
        response = self.post_page('matrix/', params)
        self.assertTemplateUsed(response, 'ui/matrix-form.html')
        self.assertTrue(response.context[0]['form'].errors['users'].find('XXX') > 0)

    def test_05_matrix_bad_release_id(self):
        """ Matrix with bad release_id. """
        params = dict(self.matrix_params, release_id='XXX')
        response = self.post_page('matrix/', params)
        self.assertTemplateUsed(response, 'ui/matrix-form.html')
        self.assertTrue(response.context[0]['form'].errors['release_id'][0].find('XXX') > 0)

    def test_07_matrix_bad_release(self):
        """ Matrix with bad release. """
        params = dict(self.matrix_params, releases='9.6,XXX')
        response = self.post_page('matrix/', params)
        self.assertTemplateUsed(response, 'ui/matrix-form.html')
        self.assertTrue(response.context[0]['form'].errors['releases'][0].find('XXX') > 0)

    def test_10_pr_matrix(self):
        """ PR Matrix. """
        self.login('steven')
        response = self.get_page('prs/')
        self.assertTemplateUsed(response, 'ui/matrix.html')
        self.assertEqual(response.context[0]['usr'].id, 'steven')
        self.assertEqual(len(response.context[0]['users']), 15)
        self.assertEqual(response.context[0]['reports'], '1')
        self.assertEqual(response.context[0]['show_count_only'], True)
        self.assertEqual(len(response.context[0]['rules']), 11)
        self.assertEqual(response.context[0]['rule_ids'][0], 'critical_prs')
        self.assertEqual(response.context[0]['vn'], 'prs')
        self.assertEqual(response.context[0]['feedback'], 'yes')
        self.assertEqual(response.context[0]['has_pr_rules'], True)
        self.assertEqual(response.context[0]['one_rule'], False)
        self.assertEqual(response.context[0]['has_release_based_rules'], True)
        self.assertEqual(response.context[0]['releases'][0], '10.4')
        self.assertEqual(response.context[0]['generated_title'], True)
        self.assertEqual(response.context[0]['title'], 'PR Matrix :: steven')

    def test_11_pr_matrix_user(self):
        """ PR Matrix. """
        self.login('ina')
        response = self.get_page('prs/steven')
        self.assertTemplateUsed(response, 'ui/matrix.html')
        self.assertEqual(response.context[0]['usr'].id, 'steven')
        self.assertEqual(len(response.context[0]['users']), 15)
        self.assertEqual(response.context[0]['title'], 'PR Matrix :: steven')


class T02_SingleReleaseMatrix(DashTestCase):

    matrix_params = {
        'rules': 'at_risk,critical_prs,beta_blocker_prs',
        'users': 'steven,ina',
        'reports': '',
        'orient': 'uxm',
        'release_id': '9.6',
        'feedback': 'yes',
        'title': 'whatisthematrix',
    }

    def test_01_matrix_1_rule(self):
        """ Matrix with 1 rule. """
        self.login('ina')
        params = dict(self.matrix_params, rules='at_risk')
        response = self.post_page('matrix/', params)
        self.assertTemplateUsed(response, 'ui/release-matrix.html')
        self.assertEqual(response.context[0]['usr'].id, 'steven')
        self.assertEqual(len(response.context[0]['users']), 2)
        self.assertEqual(response.context[0]['reports'], '')
        self.assertEqual(len(response.context[0]['rules']), 1)
        self.assertEqual(response.context[0]['rule_ids'][0], 'at_risk')
        self.assertEqual(response.context[0]['vn'], 'matrix')
        self.assertEqual(response.context[0]['feedback'], 'yes')
        self.assertEqual(response.context[0]['has_pr_rules'], False)
        self.assertEqual(response.context[0]['orientation'], 'uxm')
        self.assertEqual(response.context[0]['generated_title'], False)
        self.assertEqual(response.context[0]['title'], 'whatisthematrix')

    def test_02_matrix_3_rules(self):
        """ Matrix with 3 rules. """
        self.login('ina')
        response = self.post_page('matrix/', self.matrix_params)
        self.assertTemplateUsed(response, 'ui/release-matrix.html')
        self.assertEqual(response.context[0]['usr'].id, 'steven')
        self.assertEqual(len(response.context[0]['users']), 2)
        self.assertEqual(response.context[0]['reports'], '')
        self.assertEqual(len(response.context[0]['rules']), 3)
        self.assertEqual(response.context[0]['rule_ids'][1], 'critical_prs')
        self.assertEqual(response.context[0]['vn'], 'matrix')
        self.assertEqual(response.context[0]['feedback'], 'yes')
        self.assertEqual(response.context[0]['has_pr_rules'], True)
        self.assertEqual(response.context[0]['orientation'], 'uxm')
        self.assertEqual(response.context[0]['generated_title'], False)
        self.assertEqual(response.context[0]['title'], 'whatisthematrix')

    def test_03_matrix_mxu(self):
        """ Matrix with mxu orientation. """
        self.login('ina')
        params = dict(self.matrix_params, orient='mxu')
        response = self.post_page('matrix/', params)
        self.assertTemplateUsed(response, 'ui/release-matrix.html')
        self.assertEqual(response.context[0]['usr'].id, 'steven')
        self.assertEqual(len(response.context[0]['users']), 2)
        self.assertEqual(response.context[0]['reports'], '')
        self.assertEqual(len(response.context[0]['rules']), 3)
        self.assertEqual(response.context[0]['rule_ids'][1], 'critical_prs')
        self.assertEqual(response.context[0]['vn'], 'matrix')
        self.assertEqual(response.context[0]['feedback'], 'yes')
        self.assertEqual(response.context[0]['has_pr_rules'], True)
        self.assertEqual(response.context[0]['orientation'], 'mxu')
        self.assertEqual(response.context[0]['generated_title'], False)
        self.assertEqual(response.context[0]['title'], 'whatisthematrix')

    def test_04_matrix_no_feedback(self):
        """ Matrix with feedback=no. """
        self.login('ina')
        params = dict(self.matrix_params, feedback='no')
        response = self.post_page('matrix/', params)
        self.assertTemplateUsed(response, 'ui/release-matrix.html')
        self.assertEqual(response.context[0]['usr'].id, 'steven')
        self.assertEqual(len(response.context[0]['users']), 2)
        self.assertEqual(response.context[0]['reports'], '')
        self.assertEqual(len(response.context[0]['rules']), 3)
        self.assertEqual(response.context[0]['rule_ids'][1], 'critical_prs')
        self.assertEqual(response.context[0]['vn'], 'matrix')
        self.assertEqual(response.context[0]['feedback'], 'no')
        self.assertEqual(response.context[0]['has_pr_rules'], True)
        self.assertEqual(response.context[0]['orientation'], 'uxm')
        self.assertEqual(response.context[0]['generated_title'], False)
        self.assertEqual(response.context[0]['title'], 'whatisthematrix')

    def test_05_matrix_only_feedback(self):
        """ Matrix with feedback=only. """
        self.login('ina')
        params = dict(self.matrix_params, feedback='only')
        response = self.post_page('matrix/', params)
        self.assertTemplateUsed(response, 'ui/release-matrix.html')
        self.assertEqual(response.context[0]['usr'].id, 'steven')
        self.assertEqual(len(response.context[0]['users']), 2)
        self.assertEqual(response.context[0]['reports'], '')
        self.assertEqual(len(response.context[0]['rules']), 3)
        self.assertEqual(response.context[0]['rule_ids'][1], 'critical_prs')
        self.assertEqual(response.context[0]['vn'], 'matrix')
        self.assertEqual(response.context[0]['feedback'], 'only')
        self.assertEqual(response.context[0]['has_pr_rules'], True)
        self.assertEqual(response.context[0]['orientation'], 'uxm')
        self.assertEqual(response.context[0]['generated_title'], False)
        self.assertEqual(response.context[0]['title'], 'whatisthematrix')

    def test_06_matrix_generated_title(self):
        """ Matrix with generated title and multiple users. """
        self.login('ina')
        params = dict(self.matrix_params, title='')
        response = self.post_page('matrix/', params)
        self.assertEqual(response.context[0]['generated_title'], True)
        self.assertEqual(response.context[0]['title'], '9.6 Matrix')

    def test_07_matrix_generated_title_1_user(self):
        """ Matrix with generated title and 1 user. """
        self.login('ina')
        params = dict(self.matrix_params, title='', users='ina')
        response = self.post_page('matrix/', params)
        self.assertEqual(response.context[0]['generated_title'], True)
        self.assertEqual(response.context[0]['title'], '9.6 Matrix :: ina')

    def test_08_matrix_only_feedback(self):
        """ Matrix with feedback=only. """
        self.login('ina')
        params = dict(self.matrix_params, feedback='only')
        response = self.post_page('matrix/', params)
        self.assertTemplateUsed(response, 'ui/release-matrix.html')
        self.assertEqual(response.context[0]['usr'].id, 'steven')
        self.assertEqual(len(response.context[0]['users']), 2)
        self.assertEqual(response.context[0]['reports'], '')
        self.assertEqual(len(response.context[0]['rules']), 3)
        self.assertEqual(response.context[0]['rule_ids'][1], 'critical_prs')
        self.assertEqual(response.context[0]['vn'], 'matrix')
        self.assertEqual(response.context[0]['feedback'], 'only')
        self.assertEqual(response.context[0]['has_pr_rules'], True)
        self.assertEqual(response.context[0]['orientation'], 'uxm')
        self.assertEqual(response.context[0]['generated_title'], False)
        self.assertEqual(response.context[0]['title'], 'whatisthematrix')


class T03_MultiReleaseMatrix(DashTestCase):

    matrix_params = {
        'rules': 'at_risk,critical_prs,beta_blocker_prs',
        'users': 'steven,ina',
        'reports': '',
        'orient': 'uxm',
        'releases': ['all'],
        'feedback': 'yes',
    }

    def test_01_matrix_1_rule(self):
        """ Matrix with 1 rule. """
        self.login('ina')
        params = dict(self.matrix_params, rules='at_risk')
        response = self.post_page('matrix/', params)
        self.assertTemplateUsed(response, 'ui/matrix.html')
        self.assertEqual(len(response.context[0]['rules']), 1)
        self.assertEqual(response.context[0]['one_rule'], True)
        self.assertEqual(response.context[0]['generated_title'], True)
        self.assertEqual(response.context[0]['title'], 'Matrix :: RLIs At Risk')

    def test_02_matrix_3_rules(self):
        """ Matrix with 3 rules. """
        self.login('ina')
        response = self.post_page('matrix/', self.matrix_params)
        self.assertTemplateUsed(response, 'ui/matrix.html')
        self.assertEqual(len(response.context[0]['rules']), 3)
        self.assertEqual(response.context[0]['one_rule'], False)
        self.assertEqual(response.context[0]['title'], 'Matrix')

    def test_03_matrix_release_based_rules(self):
        """ Matrix with release_based rules. """
        self.login('ina')
        response = self.post_page('matrix/', self.matrix_params)
        self.assertTemplateUsed(response, 'ui/matrix.html')
        self.assertEqual(response.context[0]['has_release_based_rules'], True)

    def test_04_matrix_no_release_based_rules(self):
        """ Matrix with no release_based rules. """
        self.login('ina')
        params = dict(self.matrix_params, rules='tot_blocker_prs')
        response = self.post_page('matrix/', params)
        self.assertTemplateUsed(response, 'ui/matrix.html')
        self.assertEqual(response.context[0]['has_release_based_rules'], False)

    def test_05_matrix_2_releases(self):
        """ Matrix with 2 releases. """
        self.login('ina')
        params = dict(self.matrix_params, releases=['9.6', '10.1'])
        response = self.post_page('matrix/', params)
        self.assertTemplateUsed(response, 'ui/matrix.html')
        self.assertEqual(len(response.context[0]['releases']), 2)
        self.assertEqual(response.context[0]['releases'][1], '10.1')
        self.assertEqual(response.context[0]['rel_list_for_edit'], '9.6,10.1')

    def test_06_matrix_all_releases(self):
        """ Matrix with all releases. """
        self.login('ina')
        response = self.post_page('matrix/', self.matrix_params)
        self.assertTemplateUsed(response, 'ui/matrix.html')
        self.assertEqual(len(response.context[0]['releases']), 15)
        self.assertEqual(response.context[0]['releases'][1], '10.3')
        self.assertEqual(response.context[0]['rel_list_for_edit'], "all")
