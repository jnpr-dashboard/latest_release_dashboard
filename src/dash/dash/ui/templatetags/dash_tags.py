"""
Custom tags.

Dirk Bergstrom, dirk@juniper.net, 2009-03-31

Copyright (c) 2009, Juniper Networks, Inc.
All rights reserved.
"""
import time
import urllib
import random

from django import template
from django.utils.safestring import mark_safe

# need to always import dash, for objects that will changed by loading
# and then they must always be accessed as dash.Class like dash.DataSource
import dash
from dash.ui import DashUtil
from datetime import datetime

NO_DATA = mark_safe('<span class="no-data">&lt;no data&gt;</span>')
NONE_VAL = mark_safe('<span class="no-data">none</span>')

register = template.Library()

@register.filter
def formatify(value, format):
    return format % value

@register.filter
def reports_in_release(user, release_id):
    """ Return the statuses of the user's reports in the given release."""
    return user.top_reports_in_release(release_id)

@register.filter
def shorter_than(value, length):
    """ Return True if the given value's length is less than length. """
    if hasattr(value, '__len__'):
        return len(value) < int(length)
    else:
        return False

@register.filter
def longer_than(value, length):
    """ Return True if the given value's length is more than length. """
    if hasattr(value, '__len__'):
        return len(value) > int(length)
    else:
        return False

@register.filter
def subtract(val1, val2):
    """ Return val1 - val2. """
    return int(val1) - int(val2)

@register.filter
def in_list(value, strings):
    """ Return True if the given string is in the given list of comma-separated
    strings. """
    return value in strings.split(',')

@register.filter
def getHash(h,key):
    return h[key]

@register.tag(name="iter_score_summaries")
def do_iter_score_summaries(parser, token):
    try:
        tag_name, score_var, count_var = token.contents.split(None, 2)
    except ValueError:
        raise template.TemplateSyntaxError, "%r tag requires arguments" % \
            token.contents.split()[0]
    nodelist = parser.parse(('end_iter_score_summaries',))
    parser.delete_first_token()
    return IterScoreSummariesNode(nodelist, score_var, count_var)

class IterScoreSummariesNode(template.Node):
    def __init__(self, nodelist, score_var, count_var):
        self.nodelist = nodelist
        self.score_var = template.Variable(score_var)
        self.count_var = template.Variable(count_var)

    def render(self, context):
        try:
            score = self.score_var.resolve(context)
            count = int(self.count_var.resolve(context))
        except template.VariableDoesNotExist:
            return 'ERROR!' # FIXME
        except ValueError:
            return 'Non-integer count!' # FIXME

        rule_counts = score.rule.counts
        record_numbers = score.record_numbers
        out = []
        for recnum in record_numbers[:count]:
            context['sum_rec_num'] = recnum
            if rule_counts.lower() != "closedprs" :
                summary = dash.Records.get_summary(rule_counts, recnum)
                if not summary:
                    context['sum_synopsis'] = NO_DATA
                    context['sum_last_modified'] = ''
                else:
                    context['sum_synopsis'] = summary[0]
                    context['sum_last_modified'] = summary[1]
            else :
                    context['sum_synopsis'] = "CLOSED PR"
                    context['sum_last_modified'] = ''
            out.append(self.nodelist.render(context))
        return ''.join(out)


@register.tag(name="score_sections")
def do_score_sections(parser, token):
    try:
        tag_name, scores_var, rule_types_var, invert_types_var = \
            token.contents.split(None, 3)
    except ValueError:
        raise template.TemplateSyntaxError, "%r tag requires arguments" % \
            token.contents.split()[0]
    nodelist = parser.parse(('end_score_sections',))
    parser.delete_first_token()
    return ScoreSectionsNode(nodelist, scores_var, rule_types_var,
                             invert_types_var)

class ScoreSectionsNode(template.Node):
    def __init__(self, nodelist, scores_var, rule_types_var, invert_types_var):
        self.nodelist = nodelist
        self.scores_var = template.Variable(scores_var)
        self.rule_types_var = template.Variable(rule_types_var)
        self.invert_types_var = template.Variable(invert_types_var)

    def render(self, context):
        try:
            scores = self.scores_var.resolve(context)
            rule_types = self.rule_types_var.resolve(context)
            invert_types = self.invert_types_var.resolve(context)
            rule_types = rule_types.split(',')
        except template.VariableDoesNotExist:
            return '' # FIXME
        chunk_config = ((-2, 'Overdue', 'more than 2 days ago'),
                        (3, 'Now', 'in the next 3 days'),
                        (14, 'Soon', 'in the next two weeks'),
                        (999999999, 'Later', 'more than two weeks from now'))
        chunks = {}
        # bucket the scores into chunks based on days_left, excluding scores
        # with no score value, or whose rule weights are outside the thresholds
        for score in scores:
            if not score.score or not score.rule.weight or score.rule.counts=="CLOSEDPRS" or \
                    '_reference' in score.rule.id:
                # Don't display zero score or rules with zero weight or reference rules
                continue
            if (invert_types and score.rule.counts in rule_types) or \
                    (not invert_types and score.rule.counts not in rule_types):
                # Display only rules that don't count the undesired types, or
                # do count the desired types
                continue
            if score.rule.milestone.big_window:
                chunks.setdefault('Always', []).append(score)
                continue
            for days, name, _ in chunk_config:
                if score.days_left < days:
                    chunks.setdefault(name, []).append(score)
                    break
        output = []
        if 'Always' in chunks:
            output.append((chunks['Always'], 'Always Important', ''))
        for _, name, desc in chunk_config:
            if name in chunks:
                output.append((chunks[name], name, desc))
        if output:
            context['sections'] = output
            return self.nodelist.render(context)
        else:
            return 'None'

def can_use_get(url, is_mozilla):
    """ If the supplied URL is longer than the browser or the web server can
    handle, fall back to a POST form.

    *) The maximum URL that apache/tomcat will accept is ~7700 characters.
    *) The maximum URL that IE (through IE8) will handle is ~2048 characters
       (IE is pathetic)
    *) Far as anyone can tell, Mozilla browsers have no limit.
    *) Changed the URL character limit from 7700 to 5000 to send the 
       request as "POST" method.
    """
    return len(url) < 5000 and (is_mozilla or len(url) < 2000)

def deepthought_link(record_numbers, columns, user, is_mozilla, link_context):
    if len(record_numbers) == 1:
        # Render a link to the one record (handle list or set)
        link_context['recnum'] = iter(record_numbers).next()
        full_url = "https://%(host)s%(path)s/do/showView?tableName=%(table)s&record_number=%(recnum)s" % \
            link_context
    else:
        if not user.pseudo_user:
            link_context['restitle'] = user.id + '%27s%20' + link_context['restitle']
        link_context['records'] = ','.join(record_numbers)
        link_context['columns'] = '&columns='.join(columns)
        link_context['url_base'] = \
            "https://%(host)s%(path)s/do/query?tableName=%(table)s&" % \
            link_context
        link_context['url_tail'] = \
            "&columns=%(columns)s&resultsTitle=%(restitle)s&%(sortfields)s" % \
            link_context
        full_url = "%(url_base)srecord_number=%(records)s%(url_tail)s" % \
            link_context
    if can_use_get(full_url, is_mozilla):
        link_context['url'] = full_url
        return """<a href="%(url)s" id="%(link_id)s"
class="%(url_class)s">%(linktext)s</a>""" % link_context
    else:
        # Big and IE or just plain huge
        return """
<a href="#" id="%(link_id)s" class="%(url_class)s urlform">%(linktext)s</a>
<form method="post" action="%(url_base)s%(url_tail)s" class="viewform" id="f_%(link_id)s">
<input type="hidden" name="record_number" value="%(records)s"/>
</form>""" % link_context

def make_tooltip_link(recs, record_numbers, div_id, title):
    if record_numbers:
        return_html = "<div style='display:none;'  id='"+div_id+"' title='"+title+"' stay_on='true'>"
        if len(record_numbers) > 9:
            return_html += "<div style='height:250px;overflow:auto;'>"
        if len(record_numbers) > 0:
            link_context = {}
            link_context['host'] = dash.DataSource.all['Gnats'].host
            link_context['path'] = dash.DataSource.all['Gnats'].path
            link_context['database'] = 'default'
            return_html +=  "<table id='tooltip_table' class='tablesorter'>"
            return_html += "<thead><tr>"
            return_html += "<th class='small_head_prfix'>PR Number</th>"
            return_html += "<th class='small_head_prfix'> Average MTTR / PR </th>"
            return_html += "<th class='small_head_prfix'> No of Cases / PR </th>"
            return_html += "<th class='small_head_prfix'> Dev-Owner </th>"
            return_html += "<th class='small_head_prfix'> Category</th>"
            return_html += "</tr>"
            return_html += "</thead>"
            for each_rec in recs:
                link_context['defectno'] = str(each_rec['defect_id'])
                link_context['scopenr'] = str(each_rec['scope_nr'])
                
                link_context['url'] = "https://%(host)s%(path)s/%(database)s/%(defectno)s-%(scopenr)s" % link_context
                return_html += "<tr class='{% cycle \'even\' \'odd\' %}' >"
                return_html += "<td><a href='%(url)s'>%(defectno)s-%(scopenr)s</a></td>" % link_context
                return_html += "<td>"+str(each_rec['mttr_val'])+"</td>"
                return_html += "<td>"+str(each_rec['case_cnt'])+"</td>"
                return_html += "<td>"+each_rec['dev_owner']+"</td>"
                return_html += "<td>"+each_rec['category_cd']+"</td>"
                return_html += "</tr>"            
        return_html += "</table>"
        if len(record_numbers) > 9:
            return_html += "</div>"            
        return_html += "</div>"
            
        return  return_html

def gnats_link(record_numbers, columns, user, is_mozilla, link_context):
    link_context['host'] = dash.DataSource.all['Gnats'].host
    link_context['path'] = dash.DataSource.all['Gnats'].path
    link_context['database'] = 'default'
    if len(record_numbers) == 1:
        # Render a link to the one record (handle list or set)
        link_context['recnum'] = iter(record_numbers).next()
        full_url = "https://%(host)s%(path)s/%(database)s/%(recnum)s" % link_context
    else:
        if not type(user) is str:
            if not user.pseudo_user:
                link_context['restitle'] = user.id + '\'s ' + link_context['restitle']
        else:
            link_context['restitle'] = user + '\'s '  + link_context['restitle'] 
        link_context['records'] = ','.join(record_numbers)
        link_context['columns'] = ','.join(columns)
        link_context['url_base'] = \
            "https://%(host)s%(path)s/%(database)s/do-query?" % link_context
            
        link_context['get_restitle'] = urllib.urlencode({"queryname": link_context['restitle']})
        link_context['url_tail'] = \
            "&columns=%(columns)s&%(get_restitle)s&sortfld1dir=asc&sortfld1=%(sortby)s" % \
            link_context
        # GNATSweb parameter for unique record    
        if "colType" in link_context:
            link_context['url_tail'] = "%(url_tail)s&colType=%(colType)s" % link_context
        full_url = "%(url_base)sprs=%(records)s%(url_tail)s" % link_context

    if can_use_get(full_url, is_mozilla):
        link_context['url'] = full_url
        return """<a href="%(url)s" id="%(link_id)s"
class="%(url_class)s">%(linktext)s</a>""" % link_context
    else:
        link_context['random_number'] = random.random()
        link_context['random_number'] = str(link_context['random_number']).replace('.','')
        # Big and IE or just plain huge
        retval = ["""<a href="#" id="%(link_id)s" onclick="javascript: document.a%(random_number)s.submit();" class="%(url_class)s urlform">%(linktext)s</a>
<form method="post" action="%(url_base)s" class="viewform" name="a%(random_number)s">
<input type="hidden" name="prs" value="%(records)s"/>
<input type="hidden" name="sortfld1" value="%(sortby)s"/>
<input type="hidden" name="sortfld1dir" value="asc"/>
<input type="hidden" name="queryname" value="%(restitle)s"/>
<input type="hidden" name="columns" value="%(columns)s"/>""" % link_context]
        if "colType" in link_context:
            retval.append("""<input type="hidden" name="colType" value="%(colType)s"/>""" % link_context)
        retval.append("</form>")
        return '\n'.join(retval)

def review_link(record_numbers, columns, user, is_mozilla, link_context):
    link_context['host'] = dash.DataSource.all['ReviewTracker'].host
    link_context['path'] = dash.DataSource.all['ReviewTracker'].path
    if len(record_numbers) == 1:
        # Render a link to the one record (handle list or set)
        link_context['recnum'] = iter(record_numbers).next()
        full_url = "http://%(host)s%(path)s?database=review&cmd=view%%20audit-trail&id=%(recnum)s" % \
            link_context
    else:
        link_context['sortby'] = 'number'
        link_context['records'] = ','.join(record_numbers)
        link_context['columns'] = '&columns='.join(columns)
        link_context['url_base'] = "http://%(host)s%(path)s?" % link_context
        link_context['url_tail'] = \
            "&columns=%(columns)s&database=review&cmd=submit%%20query&" \
            "order=ascending&sortby=%(sortby)s" % link_context
        full_url = "%(url_base)sprs=%(records)s%(url_tail)s" % link_context
    if can_use_get(full_url, is_mozilla):
        link_context['url'] = full_url
        return """<a href="%(url)s" id="%(link_id)s"
class="%(url_class)s">%(linktext)s</a>""" % link_context
    else:
        # Big and IE or just plain huge
        retval = ["""
<a href="#" id="%(link_id)s" class="%(url_class)s urlform">%(linktext)s</a>
<form method="post" action="%(url_base)s" class="viewform" id="f_%(link_id)s">
<input type="hidden" name="cmd" value="submit query"/>
<input type="hidden" name="sortby" value="%(sortby)s"/>
<input type="hidden" name="order" value="ascending"/>
<input type="hidden" name="prs" value="%(records)s"/>
<input type="hidden" name="database" value="review"/>""" % link_context]
        for col in columns:
            retval.append("""<input type="hidden" name="columns" value="%s"/>""" % col)
        retval.append("</form>")
        return '\n'.join(retval)
        
def crs_link(record_numbers, is_mozilla, link_context):
    link_context['host'] = dash.DataSource.all['CRSTracker'].host
    link_context['path'] = dash.DataSource.all['CRSTracker'].path
    if len(record_numbers) == 1:
        # Render a link to the one record (handle list or set)
        link_context['recnum'] = iter(record_numbers).next()
        full_url = "http://%(host)s%(path)s?crs=%(recnum)s" % \
            link_context
    else:
        link_context['records'] = ','.join(record_numbers)
        link_context['url_base'] = "http://%(host)s%(path)s?" % link_context
        full_url = "%(url_base)scrs=%(records)s" % link_context
    if can_use_get(full_url, is_mozilla):
        link_context['url'] = full_url
        return """<a href="%(url)s" id="%(link_id)s"
class="%(url_class)s">%(linktext)s</a>""" % link_context
    else:
        retval = ["""
<a href="#" id="%(link_id)s" class="%(url_class)s urlform">%(linktext)s</a>
<form method="post" action="%(url_base)s" class="viewform" id="f_%(link_id)s">
<input type="hidden" name="cmd" value="submit query"/>
<input type="hidden" name="crs" value="%(records)s"/>""" % link_context]
        retval.append("</form>")
        return '\n'.join(retval)
            
def rli_link(record_numbers, columns, user, is_mozilla, link_context):
    link_context['host'] = dash.DataSource.all['RLI'].host
    link_context['path'] = dash.DataSource.all['RLI'].path
    link_context['table'] = "RLI"
    link_context['sortfields'] = 'sortField1=release_target&sortDir1=asc&' \
        'sortField2=responsible&sortDir2=asc&sortField3=state&sortDir3=asc'
    return deepthought_link(record_numbers, columns, user, is_mozilla, link_context)

def bt_link(record_numbers, columns, user, is_mozilla, link_context):
    link_context['host'] = dash.DataSource.all['BranchTracker'].host
    link_context['path'] = dash.DataSource.all['BranchTracker'].path
    link_context['table'] = "branch"
    link_context['sortfields'] = 'sortField1=release_target&sortDir1=asc&' \
        'sortField2=responsible&sortDir2=asc&sortField3=state&sortDir3=asc'
    return deepthought_link(record_numbers, columns, user, is_mozilla, link_context)

def req_link(record_numbers, columns, user, is_mozilla, link_context):
    link_context['host'] = dash.DataSource.all['Requirements'].host
    link_context['path'] = dash.DataSource.all['Requirements'].path
    link_context['table'] = "REQUIREMENTS"
    link_context['sortfields'] = 'sortField1=release_target&sortDir1=asc&' \
        'sortField2=plm_driver&sortDir2=asc&sortField3=status&sortDir3=asc'
    return deepthought_link(record_numbers, columns, user, is_mozilla, link_context)


def npi_link(record_numbers, columns, user, is_mozilla, link_context):
    link_context['host'] = dash.DataSource.all['NPI'].host
    link_context['path'] = dash.DataSource.all['NPI'].path
    link_context['table'] = "npi"
    link_context['sortfields'] = 'sortField1=phase&sortDir1=asc'
    return deepthought_link(record_numbers, columns, user, is_mozilla, link_context)

@register.simple_tag
def totals_rule_link(rule_id, user, is_mozilla, release_id=None, label=None):
    """ 
    Render a link to a named rule with a given title.
    If you want to get a part of the score, use a . in the rule_id
    for example prs_user.only will get the score from
    score.count_for_type('only')
    """
    feedback = None
    if '.' in rule_id:
        rule_id, feedback = rule_id.split('.')
    score = score_for_rule(rule_id, user, release_id)
    title = dash.Rule.all[rule_id].name
    if label:
        title = '%s (%s)' % (title, label)
    zero_val = "no %s" % title
    if not score:
        # The rule isn't in the user's list, the score is zero...
        return zero_val
    return rule_link(score, user, is_mozilla, feedback=feedback,
                     show_count_only=True, link_text=title,
                     url_class='', zero_val=zero_val)

@register.simple_tag
def named_rule_link(rule_id, user, is_mozilla, release_id=None, feedback='',
                    found_during=''):
    """ Render a link to a named rule.

    If release_id is supplied, and append_release is True, append "_release"
    to the rule_id.  Allows the tag to do both per-release and all-release pages.
    """
    score = score_for_rule(rule_id, user, release_id)
    # for CDM release, the TOT release will have to accumulate the IBx
    # will make (#totalcount)count on TOT
    # FIXME ... little hack to create the total count link
    #      
    str_total_count = ""
    n = dash.findNumericRelease(release_id)
    divider = ("","")
    try:
            rule = dash.Rule.all[rule_id]
    except:
            # should not come into here unless it is not score
            return "&nbsp"
    # calculate for cdm release and release_based
    if (n >= dash.cdm_release_cutoff) and rule.release_based:
            total_count = 0
            total_score = None
            if score:
                total_score = score.copy_as_totals_score()
            for v_IB in dash.Records.release[release_id]['childBranch_array']:
                try:
                    temp_score = user.statuses[v_IB].scores_by_rule_id[rule_id]
                except KeyError:
                    continue
                total_count  += temp_score.count_for_type(feedback)
                if not total_score:
                    total_score = temp_score.copy_as_totals_score()
                else :
                    # add the record numbers here
                    total_score.add_score(temp_score)
            if (total_count > 0):
                str_total_count = rule_link(total_score, user, is_mozilla,
                                        feedback=feedback,
                                        found_during=found_during,
                                        show_count_only=True, url_class='')
                divider = ("  <em>(",")</em>")
        
    if not score :
        # The rule isn't in the user's list, the score is zero...
        #     with cdm, we need to check the IBx too
        if str_total_count != "" :
            return divider[0] + str_total_count + divider[1]
        else:
            return "&nbsp;"
    
    ib_rel = "&nbsp;"
    if str_total_count != '&nbsp;': ib_rel = divider[0] + str_total_count + divider[1] 

    # FIXME need to munge restitle
    return  rule_link(score, user, is_mozilla, feedback=feedback,
                     found_during=found_during,
                     show_count_only=True, url_class='') + ib_rel 

@register.simple_tag
def matrix_total_rule_link(totals, rule_id, user, is_mozilla, feedback='',
                            found_during=''):
    """ Render a link to a named rule out of the supplied totals dict. """
    score = totals[user.id][rule_id]
    if not score:
        # The rule isn't in the user's list, the score is zero...
        return "&nbsp;"
    # FIXME need to munge restitle
    return rule_link(score, user, is_mozilla, feedback=feedback,
                     found_during=found_during, show_count_only=True,
                     url_class='')

def score_for_rule(rule_id, user, release_id):
    try:
        rule = dash.Rule.all[rule_id]
    except KeyError:
        raise dash.DashError("No rule matching '%s'" % rule_id)
    try:
        if release_id and rule.release_based:
            score = user.statuses[release_id].scores_by_rule_id[rule_id]
        else:
            score = user.scores_by_rule_id[rule_id]
        return score
    except KeyError:
        return None

@register.simple_tag
def broken_rule_link(score, user, is_mozilla):
    """ Render a link to a broken rule.

    This is here so that we don't have to mess with the tag when we add args
    to rule_link().
    """
    return rule_link(score, user, is_mozilla)

def rule_link(score, user, is_mozilla, show_count_only=False, feedback='',
              found_during='all', url_class="ruletooltip", zero_val='&nbsp;',
              link_text=None):
    """ Render a link to a rule, subject to various options. """
    if (score.rule_id in dash.PL_FILTERS or score.rule.counts.lower() == 'prs')\
        and found_during == 'fdvals':
        count = score.count_for_fd(feedback)
    else:
        count = score.count_for_type(feedback)
    if count == 0:
        # We only render the zero if show_count_only is false.
        if show_count_only:
            return zero_val
        else:
            return """0 <span id="rule-target-%s">%s</span>""" % \
                (score.id_hash, score.rule.name)

    link_context = {}
    link_context['link_id'] = "%s_tip" % score.id_hash
    link_context['url_class'] = url_class
    if link_text or not show_count_only:
        link_context['linktext'] = \
            '<strong>%s</strong> %s' % (count, link_text or score.rule.name)
    else:
        link_context['linktext'] = count
    if score.rule.counts == 'RLIS':
        if score.release_id:
            link_context['restitle'] = 'Release%20' + score.release_id + \
                '%20RLIs%20for%20metric%20%27' + score.rule.name + '%27'
        else:
            link_context['restitle'] = 'RLIs%20for%20metric%20%27' + \
                   score.rule.name + '%27'
        return rli_link(score.record_numbers, score.rule.columns, user,
                        is_mozilla, link_context)

    elif score.rule.counts == 'BTS':
        if score.release_id:
            link_context['restitle'] = 'Release%20' + score.release_id + \
                '%20Branch Trackers%20for%20metric%20%27' + score.rule.name + '%27'
        else:
            link_context['restitle'] = 'Branch Trackers%20for%20metric%20%27' + \
                   score.rule.name + '%27'
        return bt_link(score.record_numbers, score.rule.columns, user,
                        is_mozilla, link_context)
        
    elif score.rule.counts == 'REQS':
        if score.release_id:
            link_context['restitle'] = 'Release%20' + score.release_id + \
                '%20Requirements%20for%20metric%20%27' + score.rule.name + '%27'
        else:
            link_context['restitle'] = 'Requirement%20for%20metric%20%27' + \
                   score.rule.name + '%27'
        return req_link(score.record_numbers, score.rule.columns, user,
                        is_mozilla, link_context)

    elif score.rule.counts == 'NPIS':
        if score.release_id:
            link_context['restitle'] = 'Release%20' + score.release_id + \
                   '%20NPIs%20for%20metric%20%27' + score.rule.name + '%27'
        else:
            link_context['restitle'] = 'NPIs%20for%20metric%20%27' + \
                   score.rule.name + '%27'
        return npi_link(score.record_numbers, score.rule.columns, user,
                        is_mozilla, link_context)

    elif score.rule.counts == 'PRS' or score.rule.counts == 'CLOSEDPRS':
        if score.release_id:
            link_context['restitle'] = 'Release ' + score.release_id + \
                   ' PRs for metric \'' + score.rule.name + '\''
        else:
            link_context['restitle'] = 'PRs for metric \'' + \
                   score.rule.name + '\' '
        link_context['sortby'] = ''
        cols = score.rule.columns
        if link_context['sortby'] not in cols:
            # gnatsweb can't sort on a column unless it's in the display list.
            cols = list(cols)
            cols.append(link_context['sortby'])
        
        if (score.rule_id in dash.PL_FILTERS or \
            score.rule.counts.lower() == 'prs') and found_during == 'fdvals':
            return gnats_link(score.records_for_fd(feedback), cols, user,
                          is_mozilla, link_context)
        else: 
            return gnats_link(score.records_for_type(feedback), cols, user,
                          is_mozilla, link_context)

    elif score.rule.counts == 'REVIEWS':
        return review_link(score.record_numbers, score.rule.columns, user,
                           is_mozilla, link_context)
                           
    elif score.rule.counts == 'CRSS':
        return crs_link(score.record_numbers, is_mozilla, link_context)                           
                           

@register.simple_tag
def sunset_or_effective(rule, sunset, release_id=None):
    """ Render the sunset or effectivity of the Rule. """
    if sunset:
        horizon = rule.sunset_horizon
        milestone = rule.sunset_milestone
    else:
        horizon = rule.horizon
        milestone = rule.milestone
    if milestone.big_window:
        if sunset:
            return 'Never'
        else:
            return milestone.name
    else:
        if horizon == 0:
            retval = 'At'
        elif horizon > 0:
            retval = '%d day%s before' % (horizon, horizon > 1 and 's' or '')
        else:
            retval = '%d day%s after' % (0 - horizon, horizon < -1 and 's' or '')
        if rule.release_based and release_id:
            rel_info = "%s " % release_id
        else:
            rel_info = ''
        return '%s %s%s' % (retval, rel_info, milestone.name)
    
@register.simple_tag
def multiplier(rule):
    """ Render the type of multiplier used for this rule """
    if rule.milestone.name == 'Always Active':
        retval = 'N/A'
    else:
        if rule.horizon >= 0:
            retval = 'Positive'
        else:
            retval = 'Negative'
    return retval
            
@register.simple_tag
def release_score(user, release_id):
    """ Show the score for the user in a given release """
    if user.stub:
        return 0
    try:
        return user.statuses[release_id].total_score
    except KeyError:
        return 0

@register.simple_tag
def updated_time(user):
    """ Render the last update time for the user. """
    if not user.last_updated:
        return "Never updated"
    last_update_date = user.last_updated
    #last_update_date = datetime.datetime(2010,1,8,20,45,0)
    retval = ["""Updated %s<span class="noprint"> """ %
              last_update_date.strftime('%Y-%m-%d %H:%M')]
    minutes_ago = (dash.now() - time.mktime(last_update_date.timetuple())) / 60
    if minutes_ago < 1:
        retval.append('(mere seconds ago)')
    elif minutes_ago < 2:
        retval.append('(just a minute ago)')
    else:
        retval.append('(%d minutes ago)' % minutes_ago)
    retval.append('</span>')
    return ''.join(retval)

@register.simple_tag
def mttr_updated_time(edw_updated_time):
    """ Render the last update time for the MTTR. """

    dt = edw_updated_time.split('.')[0]
    last_update_date = datetime.strptime(dt, '%Y-%m-%d %H:%M:%S')
    retval = [""" %s<span class="noprint"> """ %
              last_update_date.strftime('%Y-%m-%d %H:%M')]
    minutes_ago = (dash.now() - time.mktime(last_update_date.timetuple())) / 60
    if minutes_ago < 1:
        retval.append('(mere seconds ago)')
    elif minutes_ago < 2:
        retval.append('(just a minute ago)')
    else:
        retval.append('(%d minutes ago)' % minutes_ago)
    retval.append('</span>')
    return ''.join(retval)

@register.tag(name="show_user_nav")
def do_show_user_nav(parser, token):
    try:
        tag_name = token.contents.split()
    except ValueError:
        raise template.TemplateSyntaxError, "%r tag takes no arguments" % \
            token.contents.split()[0]
    nodelist = parser.parse(('end_show_user_nav',))
    parser.delete_first_token()
    return ShowUserNavNode(nodelist)

class ShowUserNavNode(template.Node):
    """ Render the nodes between the tags if the two users are different,
    and neither of them is 'junos'. """
    def __init__(self, nodelist):
        self.nodelist = nodelist

    def render(self, context):
        if not 'usr' in context and 'the_user' in context:
            # One or both the vars isn't there
            return ''
        usr = context.get('usr', None)
        the_user = context.get('the_user', None)
        if usr and usr.id != the_user.id and usr.id != 'junos' and the_user.id != 'junos':
            return self.nodelist.render(context)
        else:
            return ''

"""
    This is the link produced for the menu + top menu bar
"""
@register.simple_tag
def show_cdm_branch(url_type, release, user_id=''):
    return_html = "</a>"
    user_obj = dash.User.all[user_id]
    if (float(release)>= dash.cdm_release_cutoff):
        if url_type in ("pr_fix", "newlyin_pr_fix"):
            user_id=""
        return_html = "&raquo;</a> <ul>";
        try:
            release_obj = dash.Records.release[release]
            children_branch = release_obj['childBranch_array']
            for child in children_branch:
                if DashUtil.isBranchExist(child):
                    if len(user_id) > 0 :
                        link = DashUtil.make_url(url_type,args=[child,str(user_id)])
                    else :
                        link = DashUtil.make_url(url_type,args=[child])
                    score = release_score(user_obj, child)
                    return_html += "<li class='wide_size'><a href='%s'>%s :: %d </a></li>" % (link,child, score)
        except KeyError:
            pass
        return_html +=  "</ul>"
    return return_html

"""
    This is the menu produced for the agenda page
"""
@register.simple_tag
def show_cdm_branch_menu(release, user_id):
    return_html = "</a>"
    url_type = "release";
    user_obj = dash.User.all[user_id]
    if (float(release)>= dash.cdm_release_cutoff):
        return_html += "<ul class='sf-menu sub-menu'>";
        try:
            release_obj = dash.Records.release[release]
            children_branch = release_obj['childBranch_array']
            for child in children_branch:
                if DashUtil.isBranchExist(child):
                    link = DashUtil.make_url(url_type,args=[child,str(user_id)])
                    score = release_score(user_obj, child)
                    return_html += "<li class='wide_size'><a href='%s'>%s :: %d </a></li>" % (link,child, score)
        except KeyError:
            pass
        return_html +=  "</ul> <br clear='all'>"
    return return_html

@register.tag(name="reorder_rule")
def do_reorder_rule(parser, token):
    try:
        tag_name, score_list = token.contents.split()
    except ValueError:
        raise template.TemplateSyntaxError, "%r tag takes no arguments" % \
            token.contents.split()[0]
    nodelist = parser.parse(('end_reorder_rule',))
    parser.delete_first_token()
    return ReorderRule(nodelist, score_list)

class ReorderRule(template.Node):
    """ Render the nodes to re-order the score list """
    def __init__(self, nodelist, score_list):
        self.nodelist = nodelist
        self.scorelist_var= template.Variable(score_list)

    def render(self, context):
        """ added few flags to draw the hierarchial rule in cdm releases
        """
        try:
            scorelist = self.scorelist_var.resolve(context)
        except template.VariableDoesNotExist:
            return '' # FIXME
        output = []
        output_release = {}
        for score in scorelist:
            # this is the new flag for any release based rul
            score.release_group = False
            # there are some rules which is not meant to be shown in agenda and release, since they are
            # pure only reference, or only for metrics calculation
            if score.rule.agenda_group != "no_show" :
                if score.release_id:
                    score.release_group = True
                    # check if this is cdm_release
                    mDotN = dash.findNumericRelease(score.release_id)
                    # basic initialization for not breaking 
                    if score.rule_id not in output_release:
                        output_release[score.rule_id] = {}
                    if mDotN not in output_release[score.rule_id]:
                        output_release[score.rule_id][mDotN] = {}
                        output_release[score.rule_id][mDotN]["scores"] = []
                        output_release[score.rule_id][mDotN]["total_scores"] = 0
                        output_release[score.rule_id][mDotN]["combined_records"] = []
                        # need this variable to hold the total grouping scores and count (resp) 
                        output_release[score.rule_id][mDotN]["cdm_release"] = False
                        output_release[score.rule_id][mDotN]["multi_line"] = False
                        output_release[score.rule_id][mDotN]["rule_name"] = score.rule.name
                        
                        
                    # cdm release check
                    if (float(mDotN) >= dash.cdm_release_cutoff):
                        # we will need to calculate the total score
                        score.display_type = True
                        output_release[score.rule_id][mDotN]["scores"].append(score)
                        if score.rule.counts == 'RLIS':
                            output_release[score.rule_id][mDotN]["combined_records"].extend(score.record_numbers)
                            output_release[score.rule_id][mDotN]["total_scores"] = len(set(output_release[score.rule_id][mDotN]["combined_records"])) * int(score.rule.weight)
                        else:
                            output_release[score.rule_id][mDotN]["total_scores"] += score.score

                        if not output_release[score.rule_id][mDotN]["cdm_release"]:
                            output_release[score.rule_id][mDotN]["cdm_release"] = True
                    else:
                        score.display_type = False
                        output_release[score.rule_id][mDotN]["scores"].append(score)
                        output_release[score.rule_id][mDotN]["total_scores"] = score.score
                    if len(output_release[score.rule_id][mDotN]["scores"]) > 1:
                        # do the sort based on their  release_type
                        output_release[score.rule_id][mDotN]["scores"].sort(key=lambda x:(x.release_type))
                        output_release[score.rule_id][mDotN]["multi_line"]  = True
                else:
                    output.append(score)
        
        
        context["score_list"] = output
        context["group_score"] = output_release
        return self.nodelist.render(context)
    

