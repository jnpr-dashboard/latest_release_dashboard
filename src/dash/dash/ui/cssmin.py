"""
Simple CSS minifier.

Dirk Bergstrom, dirk@juniper.net, 2009-06-09

Copyright (c) 2009, Juniper Networks, Inc.
All rights reserved.
"""
import re

from django.conf import settings
from compress.filter_base import FilterBase

class CssMiniFilter(FilterBase):
    def filter_css(self, css):
        """ Mildly minify css, optionally munging the path to images.

        Removes blank lines, one-line comments, leading and trailing whitespace.
        Doesn't try to do anything fancy like optimize hex colors, etc.
        """
        url_prefix = settings.COMPRESS_CSSMINIFILTER_URL_PREFIX
        out = []
        for line in css.splitlines():
            # Strip leading/trailing whitespace
            line = line.strip()
            if not line: continue

            # Remove comments
            line = re.sub(r'/\*.*\*/', '', line)
            if not line: continue

            if url_prefix:
                # Adjust relative paths to images, etc. if the compressed file
                # will be on a different path
                # url('mumble') => url('styles/mumble')
                line = re.sub(r'(url\([\'"]?)(?![/\.])',
                              r'\1' + url_prefix + '/',
                              line)
            out.append(line)
        return '\n'.join(out)

