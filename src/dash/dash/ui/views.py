"""
Views for Dashboard

Dirk Bergstrom, dirk@juniper.net, 2009-03-31

Copyright (c) 2009, Juniper Networks, Inc.
All rights reserved.
"""
import copy
import datetime
import forms
import logging
import math
import random
import re
import os
import urllib2, time
from pprint import pformat
from urllib import urlencode

# do not use json or simplejson, they are slower than cjson
import cjson
import json
from django.conf import settings
from django.http import HttpResponse, HttpResponseRedirect, \
                        HttpResponseNotFound
from django.shortcuts import render_to_response
from django.template import loader
from django.template.context import RequestContext
import dash
from dash import db
from dash.ui import FATAL_EXCEPTIONS
from dash.ui import DashUtil
from dash.ui.templatetags import dash_tags
from dash.ui.snapshot import type_check, SnapshotReporter
from cookielib import logger
from dash.ui.models import HardeningReport,Rating
from django.db.models import Count,Sum


# FIXME put these somewhere sensible.
GNATS_BASE_URL = 'https://gnats.juniper.net/web/default'
GNATS_DEFAULT_COLUMNS = 'synopsis,state,responsible,category'

RLI_BASE_URL = 'https://deepthought.juniper.net/app/do'
RLI_DEFAULT_COLUMNS = 'synopsis&columns=state&columns=release_target&columns=responsible'

MATRIX_BACKLOG_COUNT_TYPE = [
                       {'uid':'all','displayName':'Include monitored,suspended and feedback','count_type':'all'},
                       {'uid':'yes','displayName':'Include feedback only','count_type':'yes'},
                       {'uid':'only','displayName':'Feedback PRs only','count_type':'only'},
                       {'uid':'monitored','displayName':'Monitored PRs only','count_type':'monitored'},
                       {'uid':'suspended','displayName':'Suspended PRs only','count_type':'suspended'},
                       {'uid':'no','displayName':'Exclude Feedback','count_type':'no'},
                       ]

def dwim(request, s=None):
    """ Attempt to do what the user means. """
    if not s:
        s = request.GET.get('s', None)
    if not s:
        return agenda(request)
    s = s.strip()

    # Now, let's see if we can figure out what the user wants
    if re.match(r"^(prs(\s+[\w\-]+)?|[\w\-]+\s+prs)$", s):
        # "prs" or "prs username" or "username prs" -- PR matrix
        uid = s.replace('prs', '').strip() or request.user.username
        return matrix(request, uid=uid, view_type='prs')

    elif re.match(r'^[\w\-]+$', s):
        # A username, show that user's agenda
        return agenda(request, s)

    elif re.match(r'^(([\w\-]+)\s+)?\d\d?\.\d\d?(\s+([\w\-]+))?$', s):
        # A release number, preceeded or followed by an optional username;
        # show the agenda for that release for the given user, or the remote
        # user.
        uid = None
        groups = re.match(r'^(?:([\w\-]+)\s+)?(\d\d?\.\d\d?)(?:\s+([\w\-]+))?$',
                           s).groups()
        for g in groups:
            if g:
                if g[0].isdigit():
                    release_id = g
                else:
                    uid = g
        uid = uid or request.user.username
        return release(request, relname=release_id, uid=uid)

    elif re.match(r'^pr\s*\d+[\d\-,\s]*$', s, flags=re.I):
        # View PR or PRs
        prnums = re.search(r'(\d+[\d\-,\s]*)', s).group(1)
        if prnums.find(',') > -1 or prnums.find(' ') > -1:
            # Multiple PRs
            prnums = ','.join(prnums.replace(',', ' ').split())
            url = '%s/do-query?prs=%s&columns=%s' % \
                (GNATS_BASE_URL, prnums, GNATS_DEFAULT_COLUMNS)
        else:
            url = '%s/%s' % (GNATS_BASE_URL, prnums)
        return HttpResponseRedirect(url)

    elif re.match(r'^rli\s*\d+[\d,\s]*$', s, flags=re.I):
        # View RLI or RLIs
        rlis = re.search(r'(\d+[\d\-,\s]*)', s).group(1)
        if rlis.find(',') > -1 or rlis.find(' ') > -1:
            # Multiple RLIs
            rlis = ','.join(rlis.replace(',', ' ').split())
            url = '%s/query?tableName=RLI&record_number=%s&columns=%s' % \
                (RLI_BASE_URL, rlis, RLI_DEFAULT_COLUMNS)
        else:
            url = '%s/showView?tableName=RLI&record_number=%s' % \
                (RLI_BASE_URL, rlis)
        return HttpResponseRedirect(url)

    else:
        return error(request,
            message='Sorry, I was unable to guess what you meant by "%s".' % s)


def error(request, message, details=None):
    """ Error page. """
    return render_to_response('ui/error.html', {}, RequestContext(request, {
        'title':'Error',
        'error_message':message,
        'details': details,
    }))

def static(request, template):
    """ A slightly smarter generic view. """
    return render_to_response('ui/%s.html' % template, {},
        RequestContext(request, { }))

def help(request, template):
    """ Generic view for Help page. """
    return render_to_response('docs/%s.html' % template, {},
        RequestContext(request, {
            'rules': dash.Rule.all.values(),
            'users': dash.User.all.values(),
            'objectives': dash.Objective.ordered,
            'milestones': dash.Milestone.ordered,
            'misc': dash.Misc,
    }))

def handler404(request):
    """ Handle 404 errors. """
    t = loader.get_template("404.html")
    return HttpResponseNotFound(t.render(RequestContext(request, {
        'request_path': request.path,
    })))

def json_ready(request):
    """ The Java layer says that update JSON data is ready to be processed. """
    if request.user and request.user.username and not request.user.is_superuser:
        return error(request, "Admistrator-only function",
            "Go away, or I will replace you with a very small shell script.")
    try:
        dash.async_load_all(settings.JSON_PATH)
        retval = 'ok'
    except FATAL_EXCEPTIONS:
        raise
    except Exception, e:
        retval = "error: %s" % e
    return HttpResponse(retval, mimetype='text/plain')



def dev_login(request, username):
    """ Used to fake logins for the Django development server. """
    from django.contrib.auth import authenticate, login
    user = authenticate(username=username, password='foobar')
    if user is None:
        from django.contrib.auth.models import User
        user = User.objects.create_user(username, username + '@example.com',
                                        password='foo')
        user = authenticate(username=username, password='foo')
    login(request, user)
    return HttpResponseRedirect('/%s/' % settings.DASH_URL_BASE)

def npis(request):
    """ List the NPI programs. """
    return render_to_response('ui/npis.html', {}, RequestContext(request, {
        'title': 'Active NPI Programs',
        'npis': dash.NPIUser.sorted_programs,
    }))

def rule(request, id=None, template=None):
    """ Display a rule, or the list of rules. """
    title = 'Dashboard Rules'
    rule = None
    # For the PR fix rules, we need to modify the PR types
    rule_list = []
    for r in dash.Rule.all.values():
        if r.active:
            tempr = copy.deepcopy(r)
            if tempr.id in ['regression_prs_fix','major_prs_fix','blocker_prs_fix','critical_prs_fix']:
                tempr.counts = "PRfix"
            rule_list.append(tempr)
    rule_list.sort()
    if id:
        try:
            rule = dash.Rule.all[id]
            title = 'Rule "%s"' % rule.name
        except KeyError:
            return error(request, 'Rule "%s" not found' % id)
    if template:
        title = 'Summary of all Rules'
    else:
        template = 'rule'

    real_types = dash.Rule.types
    # we are going to modified the list on the copy, so it wont change the ORIGINAL list
    types = copy.deepcopy(real_types)
    # add modified types for *_fix -- to be shown in different block
    types.append(('PRfix','Rules modified for PR Bug Fix Report'))

    return render_to_response('ui/%s.html' % template, {},
        RequestContext(request, {
        'title': title,
        'rule': rule,
        'rules': rule_list,
        'rule_types': types,
    }))

def __construct_hierarchy_display (data, data_id, link_name):
    mgr_data = []
    if data_id.id != "junos":
        # some display for user non JUNOS
        data = []
        temp_ = {}
        temp_['uid'] = data_id.id
        # Display Manager Manager's information
        mgr_data = __get_manager_name(data, data_id, link_name)
        display_str = data_id.id
        if (not data_id.is_BG) :
            display_str = data_id.bus_memberships[0].upper() + " / " +  display_str
        temp_['displayName'] = display_str
        link_str = DashUtil.make_url(link_name,  args=[data_id.id])
        temp_['linkDisplayName'] = link_str
        data.append(temp_)
    for direct_re in data_id.direct_reports:
        temp_ = {}
        temp_['uid'] = direct_re.id
        display_str = direct_re.id
        if (not direct_re.is_BG ) :
            display_str = direct_re.bus_memberships[0].upper() + " / " +  display_str
        elif (direct_re.is_BG):
            display_str = display_str.upper()
        temp_['displayName'] = display_str
        link_str = DashUtil.make_url(link_name, args=[direct_re.id])
        temp_['linkDisplayName'] = link_str
        data.append(temp_)
    # sort it, but keep the first entry as number one
    top = data.pop(0)
    data.sort(cmp=lambda a, b: cmp(a['linkDisplayName'], b['linkDisplayName']))
    data.insert(0, top)
    # Keep Manager's information as number one
    if mgr_data:
        mgr_info = mgr_data.pop(0)
        data.insert(0, mgr_info)
    return data

def __get_manager_name(data, data_id, link_name):

    data = []
    mgr_temp_ = {}
    # Display Manager Manager's information
    if not data_id.is_BG and not data_id.is_BU and data_id.manager.id != "junos":
        mgr_temp_['uid'] = data_id.manager.id
        mgr_temp_['displayName'] = data_id.manager.bus_memberships[0].upper() + " / " + data_id.manager.id
        link_str_mgr = DashUtil.make_url(link_name, args=[data_id.manager.id])
        mgr_temp_['linkDisplayName'] = link_str_mgr
        data.append(mgr_temp_)
    return data

def __calc_fields_from_rule(request, user, column, rule_name, display_format=None, filter='all', prefix_title='', show_unique_result = False):
    """
    request: request object
    user: username of user to calculate for
    column: matrix from page
    rule_name: name of rule from matrix (NOT rule_id)
    display_format: what the link display should be.  this is a python format string and will be passed the
                    keyword args rule_count (number of records that match rule) and rule_score (total score of the rule)
                    example format string:
                    "%(rule_count)s"
    filter: type of pr counts which is passed from the page
            example in Doc PR : 'all', 'monitored', etc
                       CVBC   : 'all', 'yes', 'yes-ready-for-review'
    prefix_title: addition to the standard count / score display, we might need to
                  add additional character example '+' , or '&'. This is used mainly for
                  forward display so that we can show ( +105 )
    show_unique_result: display the PR / RLI result in unique or not. GNATSweb need special parameter
                        to tell that the query display needs to be in unique result.
    """
    #
    #  For now, show_unique_result is parameter since it only make sense to do it for
    #    CVBC and DOC PR. GNATS keep non-scoped field differ with scoped field, so
    #    to show unique PR information we can only show non-scoped field
    #
    # grab the count based on the rules and view, then calculate
    # use feedback if type=='feedback' is located
    count_page_type = request.GET.get('count_type', "all")
    count_page_unique = request.GET.get('count_unique', "yes")
    is_mozilla = request.META.get('HTTP_USER_AGENT', '').find('Gecko/') > 0
    """
    pr_report_type parameter is introduced only in DOC and CVBC, so by default it will initialized
      as 'pr' to mark this page as standard reporting or other than Doc and CVBC page
    """
    pr_report_type = request.GET.get('pr_report_type','pr')

    rule_id = column['rules'][rule_name]
    rule_score = 0
    rule_count = 0
    rule_link = "0"
    record_numbers = []
    cols = []
    link_id = ""
    score = user.scores_by_rule_id.get(rule_id)
    if score:
        rule_count = score.get_unique_count_record_for_type("%s" % (count_page_type), count_page_unique)
        # XXX this needs to be based on the record type for non all
        if count_page_type == 'yes' and count_page_unique == 'yes':
            rule_score = score.score
        else:
            rule_score = score.other_scores.get('%s_%s' % (count_page_type, count_page_unique), 0)
        if pr_report_type == "cvbc" :
            record_numbers.extend(score.get_special_record("cvbc","%s" % (count_page_type) ))
            # currently the cvbc states are different with 'standard' PR, and it only used for this
            #   reporting. There are no benefit to added another count and record properties for this
            #   specific state. Unless CVBC is more widely used and have more reporting then it
            #   makes more sense to add them into the standard count type.
            rule_count = score.get_unique_count_record_from_list(record_numbers, count_page_unique)
        else:
            record_numbers.extend(score.records_for_type("%s" % (count_page_type) ))
        cols.extend([ col for col in score.rule.columns if col not in cols])
        link_id = score.id_hash
    else:
        rule_score = 0
        rule_count = 0

    if rule_score > 0 or rule_count > 0:
        if display_format:
            display_text = display_format % {'rule_score':rule_score, 'rule_count': rule_count}
        else:
            if rule_score == rule_count:
                display_text = str(rule_count)
            else:
                display_text =  '%s (%s)' % (rule_score, rule_count)
        if show_unique_result :
            rule_link = __make_link( record_numbers, cols, user, is_mozilla, link_id,  display_text, score_title=column['PRlabel'][rule_name], unique_count=count_page_unique )
        else:
            rule_link = __make_link( record_numbers, cols, user, is_mozilla, link_id,  display_text, score_title=column['PRlabel'][rule_name] )

    return rule_score, rule_count, rule_link

def __make_link (record_numbers, columns, user, is_mozilla,  score_hash, count, score_title = "", unique_count = "no", url_class="" ):
    link_context = {}
    link_context['link_id'] = "%s_tip" % hash(str(record_numbers))
    link_context['url_class'] = url_class
    link_context['sortby'] = ''
    link_context['linktext'] = count
    link_context['colType'] = 'all'

    if unique_count == "yes" :
        link_context['colType'] = 'noscoped'
    cols = columns
    if link_context['sortby'] not in cols:
        # gnatsweb can't sort on a column unless it's in the display list.
        cols = list(cols)
        cols.append(link_context['sortby'])
    link_context['restitle'] = 'PRs for metric \'' + \
           score_title + '\''

    # Not needed to generate GNATS link when there is zero records
    if len(record_numbers) == 0: return "0"

    return dash_tags.gnats_link(record_numbers, columns, user, is_mozilla, link_context)

def __calculate_BBI(current, reference, forward, target_percent=0.85):
    # Calculate the BBI and Target
    if (current == 0 and reference == 0):
        perc = 0
    else :
        count_reference = reference
        if count_reference == 0:
            count_reference = 1
        perc = round(((float(current)/float(count_reference)) * 100),2)
    target = target_percent * reference
    return (perc, target)

def __determine_visibility(datum, *check_values):
    user = get_user(datum['uid'], fall_back_to_junos = False)
    datum["displayed"] = False
    datum["bg_display"] = False
    for check_value in check_values:
        if int(check_value) > 0:
            datum["displayed"] = True
            break
    if user.is_BG :
        datum["bg_display"] = True
    return datum

def __create_coloring_scheme(perc, reference, datum, uid, top_reference, break_point = 75):
    check_through = True
    perc_color = "transparent"
    color_text = "#000000"
    # the number 1 in the top will always be the user/BU/BG that is being asked
    if datum["uid"] != "junos" :
        # for any reference PR which < 1% than junos reference, show in white
        if reference < (0.01*top_reference) :
            check_through = False
            perc_color="white"
            color_text="#000"

    if check_through:
        if perc >= 85 :
            perc_color = "red"
            color_text = "#FAF"
        elif (perc >= break_point  and perc < 85):
            perc_color = "yellow"
        elif perc < break_point :
            perc_color = "#00FF11"
    return (perc_color, color_text)

def backlog_prs(request, uid=None):
    """
       Backlog PRs is non-release based, so we differentiate the view from pr_fix to make it easier
       all the rules being used here is NON-release based

        !!! For sanity test, run the src/bin/misc/report-test.pl !!!

    """
    # ------------------------------
    # WARNING:
    # Making changes to the output context will affect the functionality of the script
    # that uses this view for collecting trending data.  If making changes, be sure
    # that the script is also updated
    # the script is at dashboard/src/dash/ui/snapshot.py
    # ------------------------------

    rules = {}
    for fbase, rbase in [['reference','reference'],['current','current'],['forward','diff']]:
        for specific in ['','_responsible','_responsible_not_dev']:
            rules['%s%s' % (fbase, specific)] = 'backlog_prs_%s%s' % (rbase, specific)
    matrix_item_view = [
              {'uid':'backlogprs', 'displayName':'Backlog PRs', 'condition':'',
               # rules are now a dict of fieldname:rule_id
               'rules': rules,
               'label':{
                        'reference':'Reference Backlog Score (12/31/2012)',
                        'current':'Current Backlog Score',
                        'forward': ''.join(['Pending (+&Delta) ',dash.Misc.future_backlog_releases,'R1 PRs ']),
                        'reference_responsible' : 'Reference Backlog Score By Responsible (12/31/2012)',
                        'current_responsible': 'Current Backlog Score by Responsible',
                        'forward_responsible': ''.join(['Pending by Responsible(+&Delta) ',dash.Misc.future_backlog_releases,'R1 PRs ']),
                        'reduction':' Reduction to date ',
                        'bbi' : 'BBI',
                        'target': 'Target Score (75% of Reference)'
                        },
               'PRlabel':{
                        'reference':'Reference Backlog PR',
                        'current':'Current Backlog PR',
                        'forward':'Forward View',
                        'reference_responsible' : 'Reference Backlog using Responsible as the primary ownership',
                        'current_responsible' : 'Current Backlog using Responsible as the primary ownership',
                        'forward_responsible' : 'Forward View Backlog using Responsible as the primary ownership',
                        'reference_responsible_not_dev' : 'Reference Backlog using Responsible as the primary ownership, responsible != dev-owner',
                        'current_responsible_not_dev' : 'Current Backlog using Responsible as the primary ownership, responsible != dev-owner',
                        'forward_responsible_not_dev' : 'Forward View Backlog using Responsible as the primary ownership, responsible != dev-owner',
                        'reduction':' % ',
                        'bbi' : 'BBI',
                        'diff' : 'Backlog Delta (Current  - Forward)'
                        },
               # a dict defining where to find fields in the data.
               # These field names MUST match what is defined as PR_COLUMNS in
               # snapshot.py for them to show up on the report correctly.
               # the order they show up in the trend report is defined by the order
               # in PR_COLUMNS
               # the key, values are <field name>, <source column>
               # this allows source columns to move around without affecting the trend report
               'trend_fields':{'Reference':0,
                               'Current':1,
                               'Delta':2,
                               'Target':3,
                               'BBI':4,
                               'Reference by Responsible':5,
                               'Current by Responsible':6,
                               'Delta by Responsible':7,
                               'Not Dev Owner Reference by Responsible':8,
                               'Not Dev Owner Current by Responsible':9,
                               'Not Dev Owner Delta by Responsible':10
                              }
               }
            ]
    default_count = "all"
    graph_type = request.GET.get('graph_type', "bar")
    count_page_type = request.GET.get('count_type', default_count)
    count_page_unique = request.GET.get('count_unique', "yes")
    all_responsible = request.GET.get('all_responsible','all')
    show_count = request.GET.get('show_count','no')
    matrix_item_view[0]['params'] = '?%s' % urlencode({'graph_type':graph_type,
                                                       'count_type':count_page_type,
                                                       'count_unique':count_page_unique,
                                                       'all_responsible':all_responsible})

    # process the matrix and do the calculation
    data_id = get_user(uid,fall_back_to_junos=False)
    data = [{'uid':data_id.id,'displayName':data_id.id}]
    data = __construct_hierarchy_display(data, data_id, "backlogprs")

    top_reference = 0
    for datum in data:
        user = get_user(datum['uid'], fall_back_to_junos = False)
        for column in matrix_item_view :
            datum[column['uid']] = {}
            rule_results = {}
            for rule_name, rule_id in column['rules'].items():
                if show_count == 'yes':
                    if rule_name.startswith('forward'):
                        display_format = '%(rule_count)s'
                    else:
                        display_format = '%(rule_score)s (%(rule_count)s)'
                else:
                    if rule_name.startswith('forward'):
                        display_format = ' %(rule_score)s '
                    else:
                        display_format = '%(rule_score)s'
                rule_score, rule_count, rule_link= __calc_fields_from_rule(request, user, column, rule_name, display_format=display_format)
                rule_results[rule_name] = {'score': rule_score, 'count': rule_count, 'link': rule_link}

            # percentage BBI
            reference = rule_results['reference']['score']
            current = rule_results['current']['score']
            forward = rule_results['forward']['score']
            if datum["uid"] == uid:
                top_reference = reference
            bbi, target = __calculate_BBI(current, reference, forward, target_percent=0.75)
            check_values = []
            if all_responsible == "all":
                check_rule = 'responsible'
            else:
                check_rule = 'responsible_not_dev'
            for base in ['current_','forward_','reference_']:
                check_values.append(rule_results['%s%s' % (base, check_rule)]['score'])
            datum= __determine_visibility(datum, current, reference, forward, *check_values)

            perc_color, color_text = __create_coloring_scheme(bbi, reference, datum, uid, top_reference, break_point=75)
            datum[column['uid']] = [
                                       {'value':rule_results['reference']['score'], 'link':rule_results['reference']['link'],'count':rule_results['reference']['count'], 'background-color':"transparent",'color_text':"#000"},
                                       {'value':rule_results['current']['score']  , 'link':rule_results['current']['link'],'count':rule_results['current']['count']  ,'background-color':"transparent",'color_text':"#000", 'add_class':'important_value'},
                                       {'value':rule_results['forward']['count']  , 'link':rule_results['forward']['link'],'count':rule_results['forward']['count']  ,'background-color':"transparent",'color_text':"#000"},
                                       {'value': "%d" % target , 'link' : "%d" % target, 'background-color':perc_color,'color_text':"#000"},
                                       {'value': "%.1f %%" % bbi , 'link' : "%.1f %%" % bbi, 'bgcolor':perc_color,'color_text':color_text},
                                       {'value':rule_results['reference_responsible']['score'], 'link':rule_results['reference_responsible']['link'],'count':rule_results['reference_responsible']['count'],'background-color':"transparent",'color_text':"#000"},
                                       {'value':rule_results['current_responsible']['score']  , 'link':rule_results['current_responsible']['link'],'count':rule_results['current_responsible']['count']  ,'background-color':"transparent",'color_text':"#000", 'add_class':'important_value'},
                                       {'value':rule_results['forward_responsible']['count']  , 'link':rule_results['forward_responsible']['link'],'count':rule_results['forward_responsible']['count']  ,'background-color':"transparent",'color_text':"#000"},
                                       {'value':rule_results['reference_responsible_not_dev']['score'], 'link':rule_results['reference_responsible_not_dev']['link'],'count':rule_results['reference_responsible_not_dev']['count'],'background-color':"transparent",'color_text':"#000"},
                                       {'value':rule_results['current_responsible_not_dev']['score']  , 'link':rule_results['current_responsible_not_dev']['link'],'count':rule_results['current_responsible_not_dev']['count']  ,'background-color':"transparent",'color_text':"#000", 'add_class':'important_value'},
                                       {'value':rule_results['forward_responsible_not_dev']['count']  , 'link':rule_results['forward_responsible_not_dev']['link'],'count':rule_results['forward_responsible_not_dev']['count']  ,'background-color':"transparent",'color_text':"#000"},
                                       ]

    feed_url = DashUtil.make_url("backlogprs", args=[uid])
    if all_responsible == "all":
        visible_columns = [0,1,2,3,4,5,6,7]
    else:
        visible_columns = [0,1,2,3,4,8,9,10]
    context = { 'title':'Backlog PRs Report ',
                'visible_columns' : visible_columns,
                'data' : data,
                'matrix' : matrix_item_view,
                'page_type': MATRIX_BACKLOG_COUNT_TYPE,
                'count_type': count_page_type,
                'count_unique': count_page_unique,
                'all_responsible': all_responsible,
                'feed_url' : feed_url,
                'graph_type' : graph_type,
                'future_releases': str(dash.Misc.future_backlog_releases),
                'current_releases' : str(dash.Misc.current_backlog_releases),
                'page_version' : '11.3', # update this for new feature/release
                'usr':   get_user(uid, fall_back_to_junos=False),
                'uid' : uid,
              }
    format = request.GET.get('format','')
    if format == 'json':
        # user objects cannot be converted to json, so this function will str them
        convert_user(context)
        return HttpResponse(cjson.encode(context), mimetype='application/json')
    elif format == 'dict':
        convert_user(context)
        return HttpResponse(pformat(context), mimetype='text/plain')
    else:
        return render_to_response('ui/backlog-pr.html', {}, RequestContext(request, context))

def doc_cvbc_backlog_prs(request, uid=None):
    """
    Backlog PR which is specifically design for Document and CVBC PRs
        !!! For sanity test, run the src/bin/misc/report-test.pl !!!

    """
    # get the type of report

    pr_report_type = request.GET.get('pr_report_type','doc')
    rules = {}
    for fbase, rbase in [['reference','reference'],['current','current'],['forward','diff']]:
        for specific in ['_doc','_cvbc']:
            rules['%s%s' % (fbase, specific)] = 'backlog_prs_%s%s' % (rbase, specific)
    matrix_item_view = [
              {'uid':'docbacklog', 'displayName':'Doc & CVBC Backlog PRs', 'condition':'',
               'rules' : rules,
               'label':{
                        'reference':'Reference Backlog %s' % pr_report_type.upper(),
                        'current':'Current Backlog %s' % pr_report_type.upper(),
                        'forward': ''.join(['Pending(+&Delta) ',dash.Misc.future_backlog_releases,'R1 %s' % pr_report_type.upper() ]),
                        'bbi' : 'BBI','target': 'Target Backlog (85% of Reference)'
                        },
               'PRlabel':{
                        'reference_doc':'Reference Document Backlog %s ' % pr_report_type.upper(),
                        'current_doc':'Current Document Backlog %s ' % pr_report_type.upper(),
                        'forward_doc': ''.join(['Pending %s '  % pr_report_type.upper() , dash.Misc.future_backlog_releases,'R1 ']),
                        'reference_cvbc' : 'Reference CVBC (Customer Visible Behavior Changed) Backlog %s' % pr_report_type.upper() ,
                        'current_cvbc' : 'Current CVBC (Customer Visible Behavior Changed) Backlog %s' % pr_report_type.upper() ,
                        'forward_cvbc': ''.join(['Pending %s '  % pr_report_type.upper() ,dash.Misc.future_backlog_releases,'R1%20PRs ']),
                        'bbi' : 'BBI','target': 'Target Backlog (85% of Reference)'
                        },
               # a dict defining where to find fields in the data.
               # These field names MUST match what is defined as PR_COLUMNS in
               # snapshot.py for them to show up on the report correctly.
               # the order they show up in the trend report is defined by the order
               # in PR_COLUMNS
               # the key, values are <field name>, <source column>
               # this allows source columns to move around without affecting the trend report
               'trend_fields':{'Doc Reference':0,
                               'Doc Current':1,
                               'Doc Delta':2,
                               'Doc Target':3,
                               'Doc BBI':4,
                               'CVBC Reference':5,
                               'CVBC Current':6,
                               'CVBC Delta':7,
                               'CVBC Target':8,
                               'CVBC BBI':9,
                              }
               }
            ]
    default_count = "all"
    graph_type = request.GET.get('graph_type', "bar")
    count_page_type = request.GET.get('count_type', default_count)
    count_page_unique = request.GET.get('count_unique', "yes")

    matrix_item_view[0]['params'] = '?%s' % urlencode({'graph_type':graph_type,
                                                       'count_type':count_page_type,
                                                       'count_unique':count_page_unique,
                                                       'pr_report_type':pr_report_type})

    # construct the hierarchy reports
    data_id = get_user(uid,fall_back_to_junos=False)
    data = [{'uid':data_id.id,'displayName':data_id.id}]
    data = __construct_hierarchy_display(data, data_id, "docbacklog")

    doc_top_reference = 0
    cvbc_top_reference = 0
    for datum in data:
        user = get_user(datum['uid'], fall_back_to_junos = False)
        for column in matrix_item_view :
            datum[column['uid']] = {}

            rule_results = {}
            for rule_name, rule_id in column['rules'].items():
                if rule_name.startswith('forward'):
                    display_format = '(%(rule_count)s)'
                else:
                    display_format = '%(rule_count)s'
                rule_score, rule_count, rule_link= __calc_fields_from_rule(request, user, column, rule_name, display_format=display_format)
                rule_results[rule_name] = {'score': rule_score, 'count': rule_count, 'link': rule_link}

            # Doc PRs
            doc_reference= rule_results['reference_doc']['count']
            doc_current  = rule_results['current_doc']['count']
            doc_forward  = rule_results['forward_doc']['count']

            # CVBC PR
            cvbc_reference = rule_results['reference_cvbc']['count']
            cvbc_current   = rule_results['current_cvbc']['count']
            cvbc_forward   = rule_results['forward_cvbc']['count']

            if datum["uid"] == uid:
                doc_top_reference = doc_reference
                cvbc_top_reference = cvbc_reference

            # percentage BBI
            doc_bbi, doc_target   = __calculate_BBI(doc_current, doc_reference, doc_forward)
            cvbc_bbi, cvbc_target = __calculate_BBI(cvbc_current, cvbc_reference, cvbc_forward)

            if pr_report_type == 'doc':
                datum = __determine_visibility(datum, doc_current, doc_reference, doc_forward)
            else :
                datum = __determine_visibility(datum, cvbc_current, cvbc_reference, cvbc_forward)

            doc_perc_color, doc_color_text   = __create_coloring_scheme(doc_bbi, doc_reference, datum, uid, doc_top_reference)
            cvbc_perc_color, cvbc_color_text = __create_coloring_scheme(cvbc_bbi, cvbc_reference, datum, uid, cvbc_top_reference)

            datum[column['uid']] = [
                                       {'value':doc_reference, 'link':rule_results['reference_doc']['link'],'background-color':"transparent",'color_text':"#000"},
                                       {'value':doc_current,'link':rule_results['current_doc']['link'],'background-color':"transparent",'color_text':"#000", 'add_class':'important_value'},
                                       {'value':"%s"%(doc_forward),'link':rule_results['forward_doc']['link'],'background-color':"transparent",'color_text':"#000"},
                                       {'value': "%d" % doc_target , 'link' : "%d" % doc_target, 'background-color':doc_perc_color,'color_text':"#000"},
                                       {'value': "%.1f %%" % doc_bbi , 'link' : "%.1f %%" % doc_bbi, 'bgcolor':doc_perc_color,'color_text':doc_color_text},
                                       {'value':cvbc_reference, 'link':rule_results['reference_cvbc']['link'],'background-color':"transparent",'color_text':"#000"},
                                       {'value':cvbc_current,'link':rule_results['current_cvbc']['link'],'background-color':"transparent",'color_text':"#000", 'add_class':'important_value'},
                                       {'value':"%s"%(cvbc_forward),'link':rule_results['forward_cvbc']['link'],'background-color':"transparent",'color_text':"#000"},
                                       {'value': "%d" % cvbc_target , 'link' : "%d" % cvbc_target, 'background-color':cvbc_perc_color,'color_text':"#000"},
                                       {'value': "%.1f %%" % cvbc_bbi , 'link' : "%.1f %%" % cvbc_bbi, 'bgcolor':cvbc_perc_color,'color_text':cvbc_color_text},
                                       ]

    feed_url = DashUtil.make_url("docbacklog", args=[uid])
    if pr_report_type == 'doc':
        visible_columns = [0,1,2,3,4]
    else :
        visible_columns = [5,6,7,8,9]
    context = { 'title':'Doc and CVBC Backlog PRs Report ',
                'visible_columns' : visible_columns,
                'data' : data,
                'matrix' : matrix_item_view,
                'page_type': MATRIX_BACKLOG_COUNT_TYPE,
                'count_type': count_page_type,
                'count_unique': count_page_unique,
                'pr_report_type': pr_report_type,
                'feed_url' : feed_url,
                'graph_type' : graph_type,
                'future_releases': str(dash.Misc.future_backlog_releases),
                'current_releases' : str(dash.Misc.current_backlog_releases),
                'page_version' : '11.1', # update this for new feature/release
                'usr':   get_user(uid, fall_back_to_junos=False),
                'uid' : uid
              }
    format = request.GET.get('format','')
    if format == 'json':
        # user objects cannot be converted to json, so this function will str them
        convert_user(context)
        return HttpResponse(cjson.encode(context), mimetype='application/json')
    elif format == 'dict':
        convert_user(context)
        return HttpResponse(pformat(context), mimetype='text/plain')
    else:
        return render_to_response('ui/doc-cvbc-backlog-pr.html', {}, RequestContext(request, context))


def sla(request, uid=None):

    upslajob = settings.SLA_PROPS['SLAJOB_CONSTANT']

    subject = settings.SLA_PROPS['PAGE_SUBJECT']
    version = settings.SLA_PROPS['SLA_VERSION']

    gnatsurl = settings.SLA_PROPS['GNATS_SEARCH_LINK_URL']
    gnatscols = settings.SLA_PROPS['GNATS_SEARCH_LINK_COLUMNS']
    gnatsqname = settings.SLA_PROPS['GNATS_SEARCH_LINK_QNAME']
    gnatssortby = settings.SLA_PROPS['GNATS_SEARCH_LINK_SORTBY']

    acktimeqname = gnatsqname.replace(upslajob, settings.SLA_PROPS['SLAJOB_ACK_TIME'])
    astimeqname = gnatsqname.replace(upslajob, settings.SLA_PROPS['SLAJOB_ASSIGN_TIME'])
    pctinfostatqname = gnatsqname.replace(upslajob, settings.SLA_PROPS['SLAJOB_PCT_INFO_STAT'])
    infotimeqname = gnatsqname.replace(upslajob, settings.SLA_PROPS['SLAJOB_INFO_TIME'])
    infostatqname = gnatsqname.replace(upslajob, settings.SLA_PROPS['SLAJOB_INFO_STAT'])
    verifytimeqname = gnatsqname.replace(upslajob, settings.SLA_PROPS['SLAJOB_VERIFY_TIME'])

    tableau_url = settings.SLA_PROPS['TABLEAU_URL']

    query = settings.SLA_PROPS['QUERY_DASH'].replace('uid', uid)
    product = settings.SLA_PROPS['PRODUCT']
    if (uid == product):
        query += ' + 1'

    resultset = db.query_dash(query)

    org_list = ''
    datarow = []
    if resultset is not None and len(resultset) > 0:
        for row in resultset:
            datalem = {}
            path = row[0].split('/')
            length = len(path)
            if length > 6:
                datalem['uid'] = path[length - 5].upper() + ' / ' + row[1]
            elif length == 5 or length == 6:
                datalem['uid'] = path[1].upper() + ' / ' + row[1]
            elif length == 4:
                if uid == product:
                    datalem['uid'] = row[1].upper()
                else:
                    datalem['uid'] = row[1]
            else:
                datalem['uid'] = row[1]
            org_list += row[1] + ','
            datalem['uid_link'] = DashUtil.make_url('sla', args=[row[1]])
            datalem['acktime'] = round(row[2],2)
            datalem['acktimeprs'] = str(row[3])
            datalem['acktimeqname'] = acktimeqname.replace('QUERY_NAME', row[1]);
            datalem['astime'] = round(row[4],2)
            datalem['astimeprs'] = str(row[5])
            datalem['astimeqname'] = astimeqname.replace('QUERY_NAME', row[1]);
            datalem['pctinfostat'] = round(row[6],2)
            datalem['pctinfostatprs'] = str(row[7])
            datalem['pctinfostatqname'] = pctinfostatqname.replace('QUERY_NAME', row[1]);
            datalem['infotime'] = round(row[8],2)
            datalem['infotimeprs'] = str(row[9])
            datalem['infotimeqname'] = infotimeqname.replace('QUERY_NAME', row[1]);
            datalem['infostat'] = round(row[10],2)
            datalem['infostatprs'] = str(row[11])
            datalem['infostatqname'] = infostatqname.replace('QUERY_NAME', row[1]);
            datalem['verifytime'] = round(row[12],2)
            datalem['verifytimeprs'] = str(row[13])
            datalem['verifytimeqname'] = verifytimeqname.replace('QUERY_NAME', row[1]);

            if uid == product:
                if row[1].find('-') < 0:
                    datalem['is_BG'] = 'True'
                else:
                    datalem['is_BG'] = 'False'
            datarow.append(datalem)

    context = { 'title': subject,
                'uid' : uid,
                'data' : datarow,
                'page_version' : version,
                'gnatsurl' : gnatsurl,
                'gnatscols' : gnatscols,
                'gnatssortby' : gnatssortby,
                'org_list' : org_list,
                'tableau_url' : tableau_url
              }

    return render_to_response('ui/sla.html', {}, RequestContext(request, context))

def fetchMttrDatafromJson(url_base, uid=None, query_type = 'all'):
    """
    Fetch url from dashboard webservices which holds data fetched from EDW.
    """
    if uid is not None:
        url = '%s/%s/%s' % (url_base, query_type, uid)
    else:
        url = '%s/%s' % (url_base, query_type)
    data = None
    try:
        data = urllib2.urlopen(url,timeout=10).read()
    except:
        import traceback
        print traceback.format_exc()                
    if data:
        try:
            data = cjson.decode(data)
        except cjson.DecodeError:
            data = None
    return data

def calc_colors(value):
    """
    Form color visibility based on the condition.
    """
    value = float(value)
    if value >= float('33.8'):
        # red
        return 'red'
    elif value >= float('30.4') and value < float('33.8') :
        # yellow
        return 'yellow'
    elif value < float('30.4') and value > float('0.0'):
        # green
        return "#00FF11"
    else:
        # not colored
        return 'transparent'

def show_mttr_tooltip (recs, record_numbers, title, uid):
    """ Retuns tooltip html for the pr list """               
    div_id = "tt_%s_tip" % hash(str(record_numbers)) # tooltip Div Id to be filled
    title = str(title) + '<br/> Average MTTR / PR for user - '+str(uid) # Title for TT
    return dash_tags.make_tooltip_link(recs, record_numbers, div_id, title)


def mttr(request, uid=None):
    """
        Operational MTTR dashboard using data from the i360 MTTR report. Webservice reads from EDW.
    """
    is_mozilla = request.META.get('HTTP_USER_AGENT', '').find('Gecko/') > 0
    user = get_user(uid, fall_back_to_junos=False)
    MTTR_URL_BASE = settings.EDW_MTTR_URL_BASE
    edw_update_time = ''
    _cols = [ 'Number', 'Synopsis', 'Problem-Level', 'Feedback-Date',  'Resolution', 'State', 'Category', 'Product', 'Planned-Release', 'Blocker', 'Submitter-Id', 'Responsible', 'Dev-Owner', 'Originator', 'Systest-Owner', 'Attributes', 'Last-Modified', 'Updated-By-Responsible', 'Arrival-Date', 'Fix-Eta', 'Committed-Release', 'Conf-Committed-Release', 'Confidential', 'Rli', 'Created','Jtac-Case-Id' ]
    matrix_item_view = [
              {'uid':'mttr', 'displayName':'MTTR', 'condition':'',
               'label':{
                       'mttr_roll_mth_val':'Rolling 12Month MTTR',
                       'mttr_cur_mth_val':'Current Month MTTR',
                       'mttr_cur_roll_qtr_val':'Current Rolling Quarter MTTR',
                       'mttr_last_roll_qtr_val':'Last Rolling Quarter MTTR',
                       'target':'MTTR Target',
                       'cur_qtr_pr_cnt':'Number of PRs in current Quarter',
                       'cur_qtr_case_cnt':'Number of Cases in current Quarter',
                       'mttr_roll_mth_cl1_val':'Rolling 12 Month CL1 MTTR',
                       'mttr_roll_mth_cl2_val':'Rolling 12 Month CL2 MTTR',
                       'pttr_val':'MTO Predicted TTR for Open CL1 & CL2',
                       'open_cl1_cl2_pr_cnt':'Number of Open CL1 & CL2',
                       'open_cl1_cl2_case_cnt':'Number of Cases Attached to Open CL1 & CL2',
                       'untouched_pr_wk_cnt':'Number of Open CL1 & CL2 untouched for 1 wk' },}]
    i360_refresh_time =  fetchMttrDatafromJson(url_base=MTTR_URL_BASE, query_type='last-update')[0]['last_refresh_ts']
    
    data_id = get_user(uid,fall_back_to_junos=False)
    data = [{'uid':data_id.id,'displayName':data_id.id}]
    data = __construct_hierarchy_display(data, data_id, "mttr")
    for datum in data:
        user = get_user(datum['uid'], fall_back_to_junos = False)

        if datum['uid'] == "junos":
            datum['displayName'] = 'Juniper' 

        # MTTR Code starts here.............
 
        json_data = fetchMttrDatafromJson(MTTR_URL_BASE, datum['uid'], 'all')
        json_dic = {}
        for mttr_dic in json_data: 
            json_dic[mttr_dic['username']] = mttr_dic

        if json_data: 
	    if edw_update_time == '' or edw_update_time is None:
                edw_update_time = json_data[0]['edw_update_time']
        for column in matrix_item_view :
            _recnum = []
            datum[column['uid']] = {}

            datum['displayed'] = True

            if user.is_BG :
                datum["bg_display"] = True
                
            bg_color = "transparent"
            color_text = "#000000"            
            found = False
            for users in json_data:
                if datum['uid'] in users['username']:
                 found = True

            if found:
                tt_col_list = ['cur_qtr_pr_cnt','open_cl1_cl2_pr_cnt','untouched_pr_wk_cnt','mttr_roll_mth_val']
                tooltip_dic = {}
                for each_tt in tt_col_list:

                    tooltip_dic[each_tt] = {}
                    path = 'pr/'+str(each_tt)
                    recs = fetchMttrDatafromJson(MTTR_URL_BASE, datum['uid'], path)
                    tooltip_dic[each_tt]['recs'] = recs
                    if recs:
                        record_numbers = [str(each_rec['defect_id'])+'-'+str(each_rec['scope_nr']) for each_rec in recs]
                        tooltip_dic[each_tt]['record_numbers'] = record_numbers
                        tooltip_dic[each_tt]['tool_tip'] = show_mttr_tooltip([each_rec for each_rec in recs],\
                                                                              record_numbers, matrix_item_view[0]['label'][each_tt],\
                                                                              datum['uid'])
                        tooltip_dic[each_tt]['link'] = __make_link( record_numbers,\
                                                                    _cols, user, is_mozilla,\
                                                                    '',json_dic[datum['uid']][each_tt],\
                                                                    score_title=matrix_item_view[0]['label'][each_tt],\
                                                                    url_class="ruletooltip" )
                    else:
                        tooltip_dic[each_tt]['record_numbers'] = 0
                        tooltip_dic[each_tt]['tool_tip'] = ''
                        tooltip_dic[each_tt]['link'] = 0
                        
                datum[column['uid']] = [
                        {'value':json_dic[datum['uid']]['mttr_roll_mth_val'],
                         'link':tooltip_dic['mttr_roll_mth_val']['link'],
                         'color_text':color_text,
                         'bgcolor':calc_colors(json_dic[datum['uid']]['mttr_roll_mth_val']),
                         'tool_tip':tooltip_dic['mttr_roll_mth_val']['tool_tip']
                         },
                        
                        {'value':json_dic[datum['uid']]['mttr_cur_mth_val'],
                         'link':json_dic[datum['uid']]['mttr_cur_mth_val'],
                         'color_text':color_text,
                         'bgcolor':calc_colors(json_dic[datum['uid']]['mttr_cur_mth_val'])
                        },
                        {'value':json_dic[datum['uid']]['mttr_cur_roll_qtr_val'],
                         'link':json_dic[datum['uid']]['mttr_cur_roll_qtr_val'],
                         'color_text':color_text,
                         'bgcolor':calc_colors(json_dic[datum['uid']]['mttr_cur_roll_qtr_val'])
                        },
                        {'value':json_dic[datum['uid']]['mttr_last_roll_qtr_val'],
                         'link':json_dic[datum['uid']]['mttr_last_roll_qtr_val'],
                         'color_text':color_text,
                         'bgcolor':calc_colors(json_dic[datum['uid']]['mttr_last_roll_qtr_val'])
                        },
                        {'value':'> '+str(json_dic[datum['uid']]['mttr_target_val']),
                         'link':'> '+ str(json_dic[datum['uid']]['mttr_target_val']),
                         'bgcolor':bg_color, 'color_text':color_text
                        }, 
                        {'value':json_dic[datum['uid']]['cur_qtr_pr_cnt'],
                         'link':tooltip_dic['cur_qtr_pr_cnt']['link'],
                         'bgcolor':bg_color,
                         'color_text':color_text,
                         'tool_tip':tooltip_dic['cur_qtr_pr_cnt']['tool_tip']
                        },
                        {'value':json_dic[datum['uid']]['cur_qtr_case_cnt'],
                         'link':json_dic[datum['uid']]['cur_qtr_case_cnt'],
                         'bgcolor':bg_color, 'color_text':color_text
                        },
                        {'value':json_dic[datum['uid']]['mttr_roll_mth_cl1_val'],
                         'link':json_dic[datum['uid']]['mttr_roll_mth_cl1_val'],
                         'color_text':color_text,
                         'bgcolor':calc_colors(json_dic[datum['uid']]['mttr_roll_mth_cl1_val'])
                        },
                        {'value':json_dic[datum['uid']]['mttr_roll_mth_cl2_val'],
                         'link':json_dic[datum['uid']]['mttr_roll_mth_cl2_val'],
                         'color_text':color_text,
                         'bgcolor':calc_colors(json_dic[datum['uid']]['mttr_roll_mth_cl2_val'])
                        },
                        {'value':json_dic[datum['uid']]['open_cl1_cl2_pr_cnt'],
                         'link':tooltip_dic['open_cl1_cl2_pr_cnt']['link'],
                         'bgcolor':bg_color,
                         'color_text':color_text,
                         'tool_tip':tooltip_dic['open_cl1_cl2_pr_cnt']['tool_tip'],
                        },                        
                        {'value':json_dic[datum['uid']]['untouched_pr_wk_cnt'],
                         'link':tooltip_dic['untouched_pr_wk_cnt']['link'],
                         'bgcolor':bg_color,
                         'color_text':color_text,
                         'tool_tip':tooltip_dic['untouched_pr_wk_cnt']['tool_tip']
                        },                           

                        {'value':json_dic[datum['uid']]['pttr_val'],
                         'link':json_dic[datum['uid']]['pttr_val'],
                         'color_text':color_text,
                         'bgcolor':calc_colors(json_dic[datum['uid']]['pttr_val'])
                         },
                        {'value':json_dic[datum['uid']]['open_cl1_cl2_case_cnt'],
                         'link':json_dic[datum['uid']]['open_cl1_cl2_case_cnt'],
                         'bgcolor':bg_color, 'color_text':color_text
                        },

                       ]
           
    mttr_url = DashUtil.make_url("mttr", args=[uid])
    context = { 'title':'MTTR Report ',
                'data' : data,
                'matrix' : matrix_item_view,
                'mttr_url' : mttr_url,
                'usr':   user,
                'uid' : uid,
                'edw_update_time':edw_update_time,
                'mttr_version':1.0,
                'i360_refresh_time':i360_refresh_time
              }
    return render_to_response('ui/mttr.html', {}, RequestContext(request, context))

def pr_fix(request, relname=None,  uid=None):
    """ Show BU Scorecard for a release. """
    is_mozilla = request.META.get('HTTP_USER_AGENT', '').find('Gecko/') > 0
    if not relname:
        relname = request.GET.get('relname', None)
        if not relname:
            # Compatibility with the old Dashboard
            relname = request.GET.get('grname', None)
    if not uid:
        uid = request.GET.get('uid','junos')

    # compatible with old release format
    relname = dash.createCompatibleBranch(relname)
    try:
        release = dash.Records.release[relname]
    except KeyError:
        return error(request, 'Release "%s" not found' % (relname))

    if release['release'] not in dash.Records.pr_fix_releases:
        return error(request, 'Release "%s" data is not calculated for PR-fix yet' % (release['release']))

    cdm_release = False
    cdm_v2_release = False
    release_look_for = relname

    numeric_release_value = dash.findNumericRelease(release['relname'])
    rel_type = dash.findReleaseType_from(relname)
    if (float(numeric_release_value) >= dash.cdm_v2_release_cutoff):
        cdm_v2_release = True
        release_look_for = "%s"%(relname)

    if (float(numeric_release_value) >= dash.cdm_release_cutoff and float(numeric_release_value) < dash.cdm_v2_release_cutoff):
        cdm_release = True
        release_look_for = "%s"%(relname)

    # uid -> id of the report
    # displayName -> will be shown on top of the report
    # rules -> consist of the rules that is used for calculation
    #     rules:{
    #            'numerator':[],
    #            'denominator':[]
    #     }
    # if the numerator/denominator consists more than 1 rule, then it will be
    #    added
    matrix_item_view = [
              {'uid':'major_pr', 'displayName':'Major PR', 'condition':'80% Required at IBRG <br />95% Required at R1',
               'rules':{
                        'denominator':[{'rule_id':'major_prs_fix'}],
                        'numerator':[{'rule_id':'major_closed_pr'},{'rule_id':'major_prs_fix','type':'feedback'}],
                        },
               'label':{
                        'denominator':'Total Open','numerator':'Closed','result':' % fixed '
                        },
               'PRlabel':{
                        'denominator':'Open Major PR','numerator':'Closed Major PR','result':' % '
                        },
               },
            {'uid':'regression', 'displayName':'Regression PR','condition':'80% Required at IBRG <br />95% Required at R1',
             'rules':{
                        'denominator':[{'rule_id':'regression_prs_fix'}],
                        'numerator':[{'rule_id':'closed_regression_prs'},{'rule_id':'regression_prs_fix','type':'feedback'}],
                        },
               'label':{
                        'denominator':'Total Open','numerator':'Closed','result':' % fixed '
                        },
               'PRlabel':{
                        'denominator':'Open Regression PR','numerator':'Closed Regression PR','result':' % '
                        },
             },
# governance no longer wants blocker prs on this page... for now.
#            {'uid':'blocker', 'displayName':'Blocker PR','condition':'100% required',
#             'rules':{
#                        'denominator':[{'rule_id':'blocker_prs_fix'}],
#                        'numerator':[{'rule_id':'closed_blocker_prs'},{'rule_id':'blocker_prs_fix','type':'feedback'}],
#                        },
#               'label':{
#                        'denominator':'Total Open','numerator':'Closed','result':' % fixed '
#                        },
#               'PRlabel':{
#                        'denominator':'Open Blocker PR','numerator':'Closed Blocker PR','result':' % '
#                        },
#             },
            ]
    default_count = "inreport"
    count_page_type = request.GET.get('count_type', default_count)
    default_dev_branch_type = "yes"
    TOT_count = ""
    dev_branch_type = request.GET.get('dev_branch_type',default_dev_branch_type)
    passed_dev_branch_type = dev_branch_type

    if dev_branch_type == "undefined":
        dev_branch_type = default_dev_branch_type
    elif dev_branch_type == "ib_" or dev_branch_type == "tot_":
        TOT_count = dev_branch_type
        dev_branch_type = "no"

    default_pr_count= "unique_"
    report_pr_count = request.GET.get('report_pr_count',default_pr_count)
    report_pr_count = "" if report_pr_count == "all" else report_pr_count

    def make_link (record_numbers, columns, user, is_mozilla,  score_hash, count, score_title = ""):
        link_context = {}
        link_context['link_id'] = "%s_tip" % score_hash
        link_context['url_class'] = ""
        link_context['sortby'] = ''
        link_context['linktext'] = count
        cols = columns
        if link_context['sortby'] not in cols:
            # gnatsweb can't sort on a column unless it's in the display list.
            cols = list(cols)
            cols.append(link_context['sortby'])
        link_context['restitle'] = 'PRs%20for%20metric%20%27' + \
               score_title + '%27'
        return dash_tags.gnats_link(record_numbers, columns, user, is_mozilla, link_context)

    # we use direct reports of JUNOS
    data_id = get_user(uid,fall_back_to_junos=False)
    if (uid == "junos"):
        data = [{'uid':data_id.id,'displayName':'Release %s' % release_look_for}]
    else:
        data = [{'uid':data_id.id,'displayName':data_id.id}]
    for direct_re in data_id.direct_reports:
        temp_ = {}
        temp_['uid'] = direct_re.id
        display_str = direct_re.id
        if (not direct_re.is_BG) :
            display_str = direct_re.bus_memberships[0].upper() + " / " +  display_str
        elif (direct_re.is_BG):
            display_str = display_str.upper()
        temp_['displayName'] = display_str
        link_str = DashUtil.make_url("pr_fix", args=[direct_re.id,relname])
        temp_['linkDisplayName'] = link_str
        data.append(temp_)
    # process the matrix and do the calculation
    for datum in data:
        user = get_user(datum['uid'], fall_back_to_junos=False)
        datum['user'] = user
        for column in matrix_item_view :
            datum[column['uid']] = {}
            # grab the count based on the rules and view, then calculate
            # use feedback if type=='feedback' is located
            # numerator calculation
            numerator = 0
            numerator_link = "0"
            _recnum = []
            _cols = []
            _hash = ""
            for item_numerator in column['rules']['numerator']:
                _type = ""
                try:
                    _score = user.statuses[release_look_for].scores_by_rule_id[item_numerator['rule_id']]
                    if  'type' in item_numerator:
                        _type = "_%s" % item_numerator['type']
                    num = _score.get_special_count("pr","%s%s%s_%s%s" % (report_pr_count,TOT_count,count_page_type, dev_branch_type, _type))
                    _recnum.extend(_score.get_special_record("pr","%s%s%s_%s%s" % (report_pr_count, TOT_count,count_page_type, dev_branch_type, _type)))
                    _cols.extend([ col for col in _score.rule.columns if col not in _cols])
                    _hash = _hash + _score.id_hash
                except (AttributeError,KeyError) as err:
                    num = 0
                numerator = numerator + num
            if numerator > 0 :
                numerator_link = make_link( _recnum, _cols, user, is_mozilla, _hash,  numerator,column['PRlabel']['numerator'])

            # denominator calculation
            denominator = 0
            denominator_link = "0"
            _recnum = []
            _cols = []
            _hash = ""
            for item_numerator in column['rules']['denominator']:
                _type = ""
                try :
                    _score = user.statuses[release_look_for].scores_by_rule_id[item_numerator['rule_id']]
                    if  'type' in item_numerator:
                        _type = "_%s" % item_numerator['type']
                    num = _score.get_special_count("pr", "%s%s%s_%s%s" % (report_pr_count, TOT_count,count_page_type, dev_branch_type, _type))
                    _recnum.extend(_score.get_special_record("pr", "%s%s%s_%s%s" % (report_pr_count, TOT_count,count_page_type, dev_branch_type, _type)))
                    _cols.extend([ col for col in _score.rule.columns if col not in _cols])
                    _hash = _hash + _score.id_hash
                except (AttributeError,KeyError) as err:
                    num = 0
                denominator = denominator + num
            if denominator > 0 :
                denominator_link = make_link(_recnum, _cols, user, is_mozilla, _hash, denominator, column['PRlabel']['denominator'])

            # percentage
            perc = 0
            if denominator == 0 and numerator == 0:
                perc = 100
            else :
                perc = round(((float(numerator)/float(numerator+denominator)) * 100),2)

            perc_color = "transparent"
            color_text = "#000"

            # changed for DEEP_RLI-1635
            # if this release is not a CDM release, OR if it *is* a CDM release, and we've passed the IBRG cutoff
            if not dash.Records.release[relname]['CDMrelease'] or \
                    (dash.Records.release[relname]['CDMrelease'] and dash.Records.release[relname]['ibrg_past']):
                # have different condition for blocker
                if column['uid'] == "blocker":
                    if perc < 90:
                        perc_color = "red"
                        color_text = "#FFF"
                    elif (perc < 100 and perc >= 90):
                        perc_color = "yellow"
                    elif perc == 100:
                        perc_color = "#00FF11"
                else:
                    if perc < 85:
                        perc_color = "red"
                        color_text = "#FFF"
                    elif (perc <= 95 and perc >= 85):
                        perc_color = "yellow"
                    elif perc > 95:
                        perc_color = "#00FF11"
            else:
                # have different condition for blocker
                if column['uid'] == "blocker":
                    if perc < 75:
                        perc_color = "red"
                        color_text = "#FFF"
                    elif (perc <= 85 and perc >= 75):
                        perc_color = "yellow"
                    elif perc > 85:
                        perc_color = "#00FF11"
                else:
                    if perc < 70:
                        perc_color = "red"
                        color_text = "#FFF"
                    elif (perc <= 80 and perc >= 70):
                        perc_color = "yellow"
                    elif perc > 80:
                        perc_color = "#00FF11"

            # count the PRs requirement
            prs_needed = 0
            if denominator > 0 :
                prs_needed = round((float(numerator + denominator) * 0.95)) - numerator
                if column['uid'] == "blocker" :
                    prs_needed = round((float(numerator + denominator) * 1)) - numerator
                # we dont need to add more bug (negative)
                if prs_needed < 0 :
                    prs_needed = 0
            datum[column['displayName']]=[
                                  {'value':"%d" % prs_needed,'link': "%d" % prs_needed,'background-color':'transparent','color_text':"#000000"},
                                 ]
            datum[column['uid']] = [
                                       {'value':numerator,'link':numerator_link,'background-color':'transparent','color_text':"#000"},
                                       {'value':denominator, 'link':denominator_link,'background-color':'transparent','color_text':"#000"},
                                       {'value': "%.1f %%" % perc , 'link' : "%.1f %%" % perc, 'bgcolor':perc_color,'color_text':color_text},
                                       ]
        datum["displayed"] = False
        for column in matrix_item_view:
            row_part = datum[column['uid']]
            if float(row_part[0]['value']) != 0 or float(row_part[1]['value']) != 0:
                datum["displayed"] = True
                break

    feed_url = DashUtil.make_url("pr_fix", args=[uid, relname])

    context = {
        'title':'PR Bug Fix Report for Release %s' %(release_look_for),
        'data': data,
        'matrix': matrix_item_view,
        'count_type': count_page_type,
        'dev_branch_type' : passed_dev_branch_type ,
        'report_pr_count' : "all" if report_pr_count=="" else report_pr_count,
        'release_name' : release_look_for,
        'feed_url' : feed_url,
        'is_cdm' : cdm_release or cdm_v2_release,
        'is_tot' : rel_type == '',
        'usr':   get_user('junoscore', fall_back_to_junos=False)
    }

    format = request.GET.get('format','')
    if format == 'dict':
        return HttpResponse(pformat(context), mimetype='text/plain')
    else:
        return render_to_response('ui/release-prfix.html', {}, RequestContext(request, context))

# Hardening views reports
def hd_view(request, repind="1", relname=None, uid=None):

    is_mozilla = request.META.get('HTTP_USER_AGENT', '').find('Gecko/') > 0

    # Collect the milestone releases
    mrs = ["R"+ str(i) for i in range(1, 16)]
    header_len = len(mrs) + 2

    matrix_item_view = [
                        {'uid': 'hdviews'}
                       ]
    # Hardening views report types
    report_type = {
                   "1": ('BU by MRs', 'Business Unit / Person'),
                   "2": ('Customer by MRs', 'Customer'),
                   "3": ('Technology by Products', 'Technology'),
                   "4": ('Customers by Use Case PRs', 'Customer')
                  }

    # Collect the products list from the database
    products = list(HardeningReport.objects.values_list('product', flat=True)\
                   .distinct())
    products.sort()

    data = []
    data_cols = []
    if repind == "1":
        data = show_hdreport_by_bu(repind, relname, uid, mrs,
                                is_mozilla)
    elif repind in ["2", "4"]:
        data, data_cols = show_hd_reports(repind, 'customer', relname,
                                mrs, is_mozilla)
    elif repind == "3":
        header_len = len(products) + 2
        data, data_cols = show_hd_reports(repind, 'technology', relname,
                                products, is_mozilla)
    context = {
        'title':'Hardening View Report for Release %s' %(relname),
        'repind': repind,
        'header_len': header_len,
        'report_title': report_type[repind][0],
        'group_by': report_type[repind][1],
        'releases': mrs,
        'products': products,
        'matrix': matrix_item_view,
        'data': data,
        'column_total': data_cols,
        'usr': get_user('junoscore', fall_back_to_junos=False),
        'uid': uid
    }

    return render_to_response('ui/hardening-views.html', {}, RequestContext(request, context))

def show_hdreport_by_bu(repind, relname, uid, mrs, is_mozilla):

    matrix_item_view = [
                        {'uid': 'hdviews'}
                       ]

    data_id = get_user(uid,fall_back_to_junos=False)
    data = [{'uid':data_id.id,'displayName':data_id.id}]

    # Construct the bu/bg tree hierarchy
    if data_id.id != "junos":
        # some display for user non JUNOS
        data = []
        temp_ = {}
        temp_['uid'] = data_id.id
        display_str = data_id.id
        if (not data_id.is_BG) :
            display_str = data_id.bus_memberships[0].upper() + " / " +  display_str
        temp_['displayName'] = display_str
        link_str = DashUtil.make_url("hd_view",  args=[repind, data_id.id, relname])
        temp_['linkDisplayName'] = link_str
        temp_['sortDisplayName'] = 'hd_view/' + data_id.id
        data.append(temp_)
    for direct_re in data_id.direct_reports:
        temp_ = {}
        temp_['uid'] = direct_re.id
        display_str = direct_re.id
        if (not direct_re.is_BG) :
            display_str = direct_re.bus_memberships[0].upper() + " / " +  display_str
        elif (direct_re.is_BG):
            display_str = display_str.upper()
        temp_['displayName'] = display_str
        link_str = DashUtil.make_url("hd_view", args=[repind, direct_re.id,relname])
        temp_['linkDisplayName'] = link_str
        temp_['sortDisplayName'] = 'hd_view/' + direct_re.id
        data.append(temp_)
    # sort it, but keep the first entry as number one
    top = data.pop(0)
    data.sort(cmp=lambda a, b: cmp(a['sortDisplayName'], b['sortDisplayName']))
    data.insert(0, top)

    for datum in data:
        user = get_user(datum['uid'], fall_back_to_junos=False)
        datum['user'] = user
       # Initialize the release data
        for column in matrix_item_view:
            datum[column['uid']] = []

            open_prs_MRs = {}
            resolved_prs_MRs = {}
            no_eta_prs_MRs = {}
            open_cols = []
            res_cols = []
            eta_cols = []
            # Hardening open PRs data
            try:
                open_pr_score = user.statuses[relname].scores_by_rule_id['hardening_open_prs']
                op_scr = open_pr_score.count
                open_prs_MRs = open_pr_score.records_by_MRs
                open_cols.extend([col for col in open_pr_score.rule.columns if col not in open_cols])
            except:
                op_scr = 0

            # Hardening resolved PRs data (both closed and feedback PRs)
            res_scr = 0
            resolved_prs_MRs = {}
            resolved_columns = []
            res_prs_MRs = {}
            res_fbk_MRs = {}
            # First, get the scores for Resolved Closed PRs
            try:
                resolved_pr_score = user.statuses[relname].scores_by_rule_id['hardening_resolved_prs']
                res_scr = res_scr + resolved_pr_score.count
                res_prs_MRs = resolved_pr_score.records_by_MRs
                resolved_columns = resolved_pr_score.rule.columns
            except:
                # Nothing to do here but report
                logging.warning("User: %s, Release: %s - score for hardening_resolved_prs rule not found" % (user, relname))

            # Now get the scores for Resolved Feedback PRs
            try:
                resolved_feedback_pr_score = user.statuses[relname].scores_by_rule_id['hardening_resolved_feedback_prs']
                res_scr = res_scr + resolved_feedback_pr_score.count
                res_fbk_MRs = resolved_feedback_pr_score.records_by_MRs
                resolved_columns = resolved_feedback_pr_score.rule.columns
            except:
                logging.warning("User: %s, Release: %s - score for hardening_resolved_feedback_prs rule not found" % (user, relname))

            # Merging resolved PRs (both closed and feedback PRs) 
            if res_prs_MRs.keys() and not res_fbk_MRs.keys():
                resolved_prs_MRs.update(res_prs_MRs) 
            elif not res_prs_MRs.keys() and res_fbk_MRs.keys():
                resolved_prs_MRs.update(res_fbk_MRs) 
            elif res_prs_MRs.keys() and res_fbk_MRs.keys():
                join_prs = set(res_prs_MRs) | set(res_fbk_MRs)
                for mrel in join_prs:
                    resolved_prs_MRs[mrel] = []
                    if res_prs_MRs.has_key(mrel):
                        resolved_prs_MRs[mrel].extend(res_prs_MRs[mrel])
                    if res_fbk_MRs.has_key(mrel):
                        resolved_prs_MRs[mrel].extend(res_fbk_MRs[mrel])

            if resolved_prs_MRs:
                res_cols.extend([col for col in resolved_columns if col not in res_cols])

            # Hardening missing eta PRs data
            try:
                eta_pr_score = user.statuses[relname].scores_by_rule_id['hardening_open_prs_eta']
                eta_scr = eta_pr_score.count
                no_eta_prs_MRs = eta_pr_score.records_by_MRs
                eta_cols.extend([col for col in eta_pr_score.rule.columns if col not in eta_cols])
            except:
                eta_scr = 0

            # Calculate the row total
            datum['row_total'] = '0 / 0 / 0'
            datum['row_total'] = '/ '.join([str(op_scr) + ' ',
                                            str(res_scr) + ' ', str(eta_scr)])

            # Iterate through MR releases
            for m_rel in mrs:
                rule_dicts = {'open_link': '0', 'res_link': '0', 'eta_link': '0'}
                # Update the record count and score for open PRs
                if m_rel in open_prs_MRs.keys():
                    rule_dicts['open_link'] = __make_hardening_links(
                        open_prs_MRs[m_rel], open_cols, user, is_mozilla,
                        len(open_prs_MRs[m_rel]), '%s%s open hardening' %(relname, m_rel))

                # Update the record count and score for resolved PRs
                if m_rel in resolved_prs_MRs.keys():
                    rule_dicts['res_link'] = __make_hardening_links(
                        resolved_prs_MRs[m_rel], res_cols, user, is_mozilla,
                        len(resolved_prs_MRs[m_rel]), '%s%s resolved hardening' %(relname, m_rel))

                # Update the record count and score for missed eta PRs
                if m_rel in no_eta_prs_MRs.keys():
                    rule_dicts['eta_link'] = __make_hardening_links(
                        no_eta_prs_MRs[m_rel], eta_cols, user, is_mozilla,
                        len(no_eta_prs_MRs[m_rel]), '%s%s missed ETA hardening' %(relname, m_rel))

                datum[column['uid']].append(rule_dicts)

        datum["displayed"] = True
        if datum['user'].is_BG:
            datum['bg_display'] = True

    return data

def show_hd_reports(repind, rep_query, relname, hdr_rows, is_mozilla):
    data = []
    data_cols = []
    hd_rep_data = {}

    for r_type in ['open', 'resolved']:
        hd_obj = HardeningReport.objects.filter(rule_type__startswith=r_type, planned_rel__startswith=relname)

        # Filtering the records based on the keywords
        if repind == "4":
            hd_obj = hd_obj.filter(keywords__iregex=r'uc-\d+')

        hd_rep_data = __group_by_report(hd_obj, repind, r_type, hdr_rows, hd_rep_data)

    # Gnats columns
    cols = ['synopsis', 'state', 'problem-level', 'category', 'product',
            'planned-release', 'blocker', 'submitter-id', 'responsible',
            'dev-owner', 'originator', 'attributes', 'last-modified',
            'updated-by-responsible', 'arrival-date', 'fix-eta',
            'committed-release', 'conf-committed-release', 'confidential', 'rli'
            ]

    col_dict = {}
    for hrd in hd_rep_data:
        rep_data = {}
        rep_data[rep_query] = hrd
        rep_data['hdviews'] = []
        rep_data['row_total'] = '0 / 0'
        op_cnt = 0
        res_cnt = 0
        for h_row in hdr_rows:
            rule_dicts = {'open_link': '0', 'res_link': '0'}
            if not col_dict.has_key(h_row):
                col_dict[h_row] = {'open': 0, 'res': 0}

            # Make gnats url for open PRs
            if hd_rep_data[hrd][h_row]['open_count'] > 0:
                rule_dicts['open_link'] = __make_hardening_links(
                          hd_rep_data[hrd][h_row]['open_record_numbers'],
                          cols, str(hrd), is_mozilla,
                          hd_rep_data[hrd][h_row]['open_count'],
                          '%s%s open hardening' %(relname, h_row))

            # Make gnats url for resolved PRs
            if hd_rep_data[hrd][h_row]['resolved_count'] > 0:
                rule_dicts['res_link'] = __make_hardening_links(
                          hd_rep_data[hrd][h_row]['resolved_record_numbers'],
                          cols, str(hrd), is_mozilla,
                          hd_rep_data[hrd][h_row]['resolved_count'],
                          '%s%s resolved hardening' %(relname, h_row))
            # Sum up the row total
            op_cnt += hd_rep_data[hrd][h_row]['open_count']
            res_cnt += hd_rep_data[hrd][h_row]['resolved_count']

            # Sum up the column total
            if col_dict[h_row].has_key('open'):
                col_dict[h_row]['open'] += hd_rep_data[hrd][h_row]['open_count']
            if col_dict[h_row].has_key('res'):
                col_dict[h_row]['res'] += hd_rep_data[hrd][h_row]['resolved_count']

            rep_data['hdviews'].append(rule_dicts)

        # Calculate the row total
        rep_data['row_total'] = '/ '.join([str(op_cnt) + ' ', str(res_cnt)])
        data.append(rep_data)

    # Calculate the column total
    data_cols = []

    if repind == "3":
        # Sort based on MRs like R1, R2, R3 etc
        temp = col_dict.keys()
        temp.sort()
        data_cols = map(lambda ct: str(col_dict[ct]['open']) + ' / ' +
                                    str(col_dict[ct]['res']), temp)
    else:
        temp = col_dict.items()
        temp = sorted(temp, key=lambda s: int(s[0][1:]))
        data_cols = map(lambda col_tot: str(col_tot[1]['open']) + ' / ' +
                                    str(col_tot[1]['res']), temp)

    # Sort data based on customer/technology
    data.sort(lambda p, q: cmp(p[rep_query], q[rep_query]))

    return (data, data_cols)

def __group_by_report(hd_obj, repind, r_type, rows, hd_data):

    for object in hd_obj:

        if repind == "3":
            key = object.technology
            if key == '': continue
            group_data = object.product
            if group_data == '': continue
        else:
            key = object.customer
            if key == '': continue
            group_data = object.rel_type

        for k in key.split(', '):
            k = k.lower()
            if not hd_data.has_key(k): hd_data[k] = {}

            for r in rows:
                if not hd_data[k].has_key(r):
                    hd_data[k][r] = {
                            'open_count': 0,
                            'open_record_numbers': [],
                            'resolved_count': 0,
                            'resolved_record_numbers': []
                            }
                rcount ="%s_count" % r_type
                rec_cnt = "%s_record_numbers" % r_type
                if r in group_data:
                    if not object.pr_numbers in hd_data[k][r][rec_cnt]:
                        hd_data[k][r][rcount] += object.counts
                        hd_data[k][r][rec_cnt].append(object.pr_numbers)

    return hd_data

def __make_hardening_links(record_nos, columns, user, is_mozilla, count, title=""):
    link_context = {}
    link_context['link_id'] = "%s_tip" % hash(str(record_nos))
    link_context['url_class'] = ''
    link_context['sortby'] = ''
    link_context['linktext'] = count
    link_context['restitle'] = 'PRs for metric \''+ \
          title + '\''

    return dash_tags.gnats_link(record_nos, columns, user, is_mozilla, link_context)

def tour(request, page_no=None):

    if not page_no:
        printable = True
        page_type = 'tour-page'
    else:
        printable = False
        page_type = 'tour'

    pages = dict([x, False] for x in range(7))
    pages[page_no] = True

    return render_to_response('docs/%s.html' % page_type, {},
        RequestContext(request, {
            'printable': printable,
            'pages': pages,
            'page_no': page_no
        }))

YMD_RE = re.compile(r'(\d{4})-(\d{2})-(\d{2})')
def release(request, relname=None, uid=None):
    """ Show release status for a user. """
    if not relname:
        relname = request.GET.get('relname', None)
        if not relname:
            # Compatibility with the old Dashboard
            relname = request.GET.get('grname', None)
    if not uid:
        uid = request.GET.get('uid', None)
    if uid:
        user = get_user(uid, fall_back_to_junos=False)
    else:
        user = get_user(request.user.username)
    # compatible with old release format
    relname = dash.createCompatibleBranch(relname)
    try:
        release = dash.Records.release[relname]
        tot_release = dash.Records.release[release['release']]
    except KeyError:
        return error(request, 'Release "%s" not found' % (relname))
    """ Get the CDM_RELEASE_CUTOFF """
    cdm_release = False
    """ Get the CDM_V2_RELEASE_CUTOFF """
    cdm_v2_release = False
    score_template = "ui/scores.html"
    numeric_release_value = dash.findNumericRelease(release['relname'])
    rel_type = dash.findReleaseType_from(relname)
    if (float(numeric_release_value) >= dash.cdm_v2_release_cutoff):
        cdm_v2_release = True
        if rel_type != "" :
            score_template = "ui/scores-branch.html"
    if (float(numeric_release_value) >= dash.cdm_release_cutoff and float(numeric_release_value) < dash.cdm_v2_release_cutoff):
        cdm_release = True
        if rel_type != "" :
            score_template = "ui/scores-branch.html"
    # Check their child branch and includes their scores
    # There is possibility that the url is passing old format
    #   of branch naming which is using IBx, so we rely on
    #   dash.findReleaseType_from(rls) to get the correct release
    #   types.
    # determine if this TOT or branches
    branch_info = []
    try:
        status = copy.deepcopy(user.statuses["%s"% relname])
    except KeyError:
        return error(request,
                     'No status for release %s for user %s.' % (relname, user.id))

    if release['relname'] == release['release']:
        # THIS IS TOT
        for x in release['childBranch_array'] :
            # get the branch release information
            try :
                branch_info.append(dash.Records.release["%s"%(x)])
                status.scores.extend(user.statuses["%s"%x].scores)
            except KeyError:
                continue
    else:
        # adding branch information
        try :
            branch_info.append(release)
        except KeyError:
            pass

    # add tot_full_regression which is defined as ibrg + 39 days
    match = YMD_RE.match(release['ibrg_string'])
    if match:
        pieces = [int(x) for x in match.groups()]
        # add 39 days
        tot_date = datetime.date(*pieces) + datetime.timedelta(days=39)
        # turn the date back into a string for display purposes
        release['tot_full_regression_string'] = tot_date.strftime('%Y-%m-%d')
        release['tot_full_regression_past'] = tot_date < datetime.date.today()
    release_context = {
        'title':'Agenda :: %s :: %s' % (user.id, relname),
        'usr': user,
        # FiXME mind that in cdm release, there is possibility that status is array
        #    it could be cleaner than this...........
        'status': status,
        'release_id': relname,
        'tot_release' : tot_release['release'],
        'release': tot_release,
        'cdm_release': cdm_release,
        'cdm_v2_release': cdm_v2_release,
        'score_template': score_template,
        'rel_type' : rel_type.upper(),
        'show_count_only': False,
        'broken_rule_displays_release': (cdm_release or cdm_v2_release) and (rel_type==""),
        'branch_info' : branch_info
    }
    return render_to_response('ui/release.html', {}, RequestContext(request, release_context))

def agenda(request, uid=None):
    """ Show the agenda for a user. """
    if not uid:
        uid = request.GET.get('uid', None)
        if not uid:
            # Compatibility with the old Dashboard NPI pages
            npi_num = request.GET.get('grname', None)
            if npi_num:
                uid = 'npi-%s' % npi_num
    if uid:
        user = get_user(uid, fall_back_to_junos=False)
    else:
        user = get_user(request.user.username)
    top_reports = []

    for person in user.direct_reports:
        if (person.total_score > 0):
            top_reports.append(person)

    top_reports.sort(lambda x,y:-cmp(x.total_score, y.total_score))
    return render_to_response('ui/agenda.html', {}, RequestContext(request, {
        'title':'Agenda :: %s' % user.id,
        'tab': 'agenda',
        'usr': user,
        'release_id': False,
        'cdm_release' : False,
        'cdm_v2_release' : False,
        'show_count_only': False,
        'broken_rule_displays_release': True,
        'statuses': top_reports,
    }))

def metric_detail(request, uid=None):
    """ Show the agenda for a user. """
    # TODO:XXX metrics naming needs to be corrected in EDW.
    names = {
              'sisyphus_break_prs': 'sisyphus_build_break_prs',
              'branch_dev': 'dev_branches',
              'branch_responsible': 'integration_branches',
              'pendingblocker_prs': 'PendingMR_Blocker_PRs',
              'prs_user': 'total_prs',
              'blocker_prs': 'all_blocker_prs'
            }

    if not uid:
        uid = request.GET.get('uid', 'junos')

    if uid:
        user = get_user(uid, fall_back_to_junos=False)
    else:
        user = get_user(request.user.username)

    met_name = request.GET.get('metric_name', 'None')
    if request.GET.get('release'):
        rel_name = request.GET.get('release', 'None')
    else:
        rel_name = 'None'

    if met_name in names.keys(): met_name = names[met_name]
    
    if (request.GET.get('detail_type')) : 
        detail_type = request.GET.get('detail_type', 'agenda')
    else : 
        detail_type = 'agenda'
    
    found_during = request.GET.get('found_during', 'None');
    states = request.GET.get('states', 'None');
    count_type = request.GET.get('cnt_type', 'X');
    return render_to_response('ui/metric_detail.html', {}, RequestContext(request, {
        'title':'Details PRs metric for %s' % user.id,
        'usr': user,
        'metric_name': met_name,
        'release': rel_name,
        'detail_type' : detail_type,
        'found_during' : found_during,
        'states' : states,
        'count_type' : count_type
    }))

def matrix_form(request, form=None):
    """ Form to build a matrix. """
    # For the PR fix rules, we need to modify the PR types
    rules = []

    for r in dash.Rule.all.values():
        if r.active or r.counts == "PRS":
            tempr = copy.deepcopy(r)
            if tempr.id in ['regression_prs_fix','major_prs_fix','blocker_prs_fix','critical_prs_fix']:
                tempr.counts = "PRfix"

            rules.append(tempr)
    #real_rules = [r for r in dash.Rule.all.values() if r.active]
    rules.sort()
    if form is None:
        if 'rules' in request.REQUEST:
            # We want to edit an existing matrix
            form = forms.MatrixForm(request.REQUEST)
        else:
            usr = dash.User.all.get(request.user.username, dash.User.JUNOS)
            form = forms.MatrixForm(initial={
                    'users': usr.id,
                    'reports': (usr.is_manager or usr.is_junos) and 1 or None,
                })
    real_types = dash.Rule.types
    # we are going to modified the list on the copy, so it wont change the ORIGINAL list
    types = copy.deepcopy(real_types)
    # add modified types for *_fix -- to be shown in different block
    types.append(('PRfix','Rules modified for PR Bug Fix Report'))
    if not request.user.is_superuser:
        # Regular users don't see the admin rules.
        types = [t for t in types if not t[0] == 'NOTHING' ]
    # these are rules that do not currently work correctly for matrix reporting
    hide_types = (('CLOSEDPRS', 'ClosedPRs'),('PRfix', 'Rules modified for PR Bug Fix Report'),('NEWLYINPRS', 'Newly Introduced PRs'))
    for hide_type in hide_types:
        types.remove(hide_type)
    return render_to_response('ui/matrix-form.html', {}, RequestContext(request, {
        'title': 'Build a Matrix',
        'rules': rules,
        'page': 'matrix-form',
        'form': form,
        'rule_types': types
    }))

def matrix_compat(request):
    """ Translate old-style matrix parameters. """
    uid = request.GET.get('uid', None)
    usernames = request.GET.get('usernames', None)
    reports = None
    if usernames:
        uid_list = usernames
    elif uid:
        uid_list = uid
        reports = 1
    else:
        uid_list = dash.User.all.get(request.user.username, dash.User.JUNOS).id
    form = forms.MatrixForm({
            'users': uid_list,
            'reports': reports,
            'rules': request.GET.getlist('ruleIds'),
        })
    return matrix(request, form=form)

def matrix(request, release_id=None, uid=None, view_type=None, form=None):
    """ Render a matrix, either across releases, or for a single release. """
    if view_type and view_type.endswith('prs'):
        # FIXME put this list somewhere more configurable
        rule_ids = 'critical_prs,regression_prs,beta_blocker_prs,release_blocker_prs,test_blockers,cam_blocker_prs,toxic_blocker_prs,tot_blocker_prs,escalation_prs,major_prs,minor_prs'.split(',')
        usernames = title = orientation = releases = ''
        feedback = request.REQUEST.get('feedback', 'yes')
        reports = True
    else:
        if not form:
            form = forms.MatrixForm(request.REQUEST)
        if form.is_valid():
            # TODO This will miss complicated checks like valid users
            rule_ids = form.cleaned_data['rules']
            usernames = form.cleaned_data['users']
            reports = form.cleaned_data['reports']
            orientation = form.cleaned_data['orient']
            release_id = release_id or form.cleaned_data['release_id']
            releases = form.cleaned_data['releases']
            feedback = form.cleaned_data['feedback']
            found_during = form.cleaned_data['found_during']
            title = form.cleaned_data['title']
        else:
            return matrix_form(request, form)

    # Collected the PR related rules selected by the user
#     selected_pr_rules = list(set(rule_ids) & set(dash.pr_rules.keys()))
    
    rules, err = id_list_to_object_list(rule_ids, dash.Rule.all)
    
    selected_pr_rules = []
    
    # Group all PR Rules
    if rules : 
        for ruleObj in rules : 
            if ruleObj.counts == "PRS" : 
                selected_pr_rules.append(ruleObj)
    
    if err:
        if not form:
            form = forms.MatrixForm()
        form.errors['rules'] = err % 'rules'

    if not usernames:
        for uname in (uid, request.user.username, 'junos'):
            if uname in dash.User.all:
                usernames = uname
                break
    users, err = id_list_to_object_list([usernames], dash.User.all, empty_ok=True)
    if err:
        if not form:
            form = forms.MatrixForm()
        form.errors['users'] = err % 'username'
    usernames = ','.join([u.id for u in users])
    if reports and len(users) == 1:
        # Append all of the user's reports
        users = users + users[0].direct_reports

    if form and form.errors:
        return matrix_form(request, form)

    context = {
        'usr': users[0], # TODO is this sensible?
        'usernames': usernames,
        'users': users,
        'reports': reports and '1' or '',
        'show_count_only': True,
        'rules': rules,
        'rule_ids': rule_ids,
        'vn':view_type,
        'feedback': feedback,
        'found_during': found_during,
        'has_pr_rules': len([r for r in rules if r.counts == 'PRS']) > 0,
        'selected_pr_rules': selected_pr_rules
    }
    if release_id:
        # A single-release matrix
        try:
            release = dash.Records.release[release_id]
        except KeyError:
            form.errors['release_id'] = 'Release "%s" is invalid.' % release_id
            return matrix_form(request, form)
        if orientation not in ('uxm', 'mxu'):
            orientation = 'uxm'

        if orientation == 'mxu':
            # metrics across the top
            if len(rules) > 4:
                table_width = '100%'
                header_col_width = ''
                body_col_width = ''
            else:
                table_width = '%d%%' % ((len(rules) * 20) + 20)
                header_col_width = '15%'
                body_col_width = '20%'
        else:
            # users across the top
            if len(users) > 5:
                table_width = '100%'
                header_col_width = '20%'
                body_col_width = '10%'
            else:
                table_width = '%d%%' % ((len(users) * 15) + 20)
                header_col_width = '25%'
                body_col_width = '15%'

        context.update({
            'table_width': table_width,
            'body_col_width': body_col_width,
            'header_col_width': header_col_width,
            'release_id': release_id,
            'orientation': orientation,
            'page': 'release-matrix',
        })
        template = 'ui/release-matrix.html'
    else:
        totals = {}
        # grab all releases including the cdm releases....
        complete_releases = []
        for r in dash.Records.active_releases:
            n = dash.findNumericRelease(r)
            complete_releases.append(r)
            if (float(n) >= dash.cdm_release_cutoff):
                for v_cdm in dash.Records.release[r]['childBranch_array']:
                    if DashUtil.isBranchExist(str(v_cdm)) :
                        complete_releases.append(str(v_cdm))

        if not releases or 'all' in releases:
            releases = complete_releases
            rel_list_for_edit = 'all'
            for user in users:
                totals[user.id] = {}
                for rid in rule_ids:
                    totals[user.id][rid] = user.scores_by_rule_id.get(rid, None)
        else:
            bogus_rels = []
            for rel in releases:
                if not rel in complete_releases:
                    bogus_rels.append(rel)
            if bogus_rels:
                form.errors['releases'] = \
                    "The following release values are invalid: %s" % \
                    ', '.join(bogus_rels)
                return matrix_form(request, form)
            rel_list_for_edit = ','.join(releases)
            for user in users:
                totals[user.id] = {}
                for rid in rule_ids:
                    totals[user.id][rid] = user.total_for_releases(releases, rid)
        context.update({
            'one_rule': len(rules) == 1,
            'has_release_based_rules': len([r for r in rules if r.release_based]) > 0,
            'releases': releases,
            'rel_list_for_edit': rel_list_for_edit,
            'page': 'matrix',
            'totals': totals,
        })
        template = 'ui/matrix.html'
    if title:
        context['generated_title'] = False
        context['title'] = title
    else:
        context['generated_title'] = True
        context['title'] = '%s%sMatrix%s%s' % \
            (release_id and release_id + ' ' or '',
             view_type and view_type.endswith('prs') and 'PR ' or '',
             (len(users) == 1 or reports) and " :: " + users[0].id or '',
             len(rules) == 1 and ' :: ' + rules[0].name or '')
    return render_to_response(template, {}, RequestContext(request, context))

def crs(request):

    user = request.GET.get('user')
    reports = request.GET.get('reports')
    states = request.GET.get('states')
    releases = request.GET.get('releases')
    types = request.GET.get('types')
    
    # The default user is junos
    if user == None or user == '':
        user = 'junos'
        
    if user.endswith(','):
       user = user[:-1]    
        
    #There is no reports selected by default    
    if reports == None or reports == '':
        reports = '0' 
    
    all_releases = []
    complete_releases = ['All']
    for r in dash.Records.active_releases:
        n = dash.findNumericRelease(r)
        complete_releases.append(r)
          
    if releases == None or releases == '':
        releases = 'All'  
            
    for r in complete_releases:
        if r in releases:
            all_releases.append((r, 'True'))
        else:
            all_releases.append((r, 'False'))                                 

    all_states = [] 
    
    # The default states selected are All  
    if states == None or states == '':
        states = 'All'
                 
    for s in settings.CRS_PROPS['states'].split(','):
        if s in states:
            all_states.append((s, 'True'))
        else:
            all_states.append((s, 'False')) 
            
    all_types = [] 
    
    # The default types selected are All       
    if types == None or types == '':
        types = 'All' 
                        
    for t in settings.CRS_PROPS['types'].split(','):
        if t in types:
            all_types.append((t, 'True'))
        else:
            all_types.append((t, 'False'))          
    
    lives = settings.CRS_PROPS['lives'].split(',')
    
    datarows = []
    
    # users are objects unlike user
    users, err = id_list_to_object_list([user], dash.User.all, empty_ok=True)
    
    if err is None:         
        if reports == '1':
            # Append all of the user's reports
            users = users + users[0].direct_reports 
                
            
        if 'All' in releases:
            releases = complete_releases[1:len(complete_releases)]
        else:
            rels = []
            for r in releases.split(','):
                rels.append(str(r))
            releases = rels                             
            
        if 'All' in states:
            states0 = settings.CRS_PROPS['states'].split(',')
            states = states0[1:len(states0)]
        else:
            states = states.split(',')           
            
        if 'All' in types:
            types0 = settings.CRS_PROPS['types'].split(',')
            types = types0[1:len(types0)]
        else:
            types = types.split(',')              
    
    
        # rule id format: crs_life_type_state_count. example: crs_aging_throttle_pend_approval_count
        rules = settings.CRS_PROPS['rules'].split(',')

        for u in users:
            
            #Do not consider those BGs like empty_spoc, deprecated_category, non_user
            if '_' in u.id:
                continue
                
            datalem = {}  
            sub_totals = []
            total_crsids = []
            expired_crs_ids = []
            
            for lf in lives:
                lf_low = lf.lower()
                if lf_low == 'total':
                    continue
                if ' ' in lf_low:
                    lf_low = lf_low.split(' ')[1]
                
                rls_lf = []
                for r in rules:
                    if lf_low == r.split('_')[1]:
                        rls_lf.append(r)   
      
                rls_types = []    
                for tp in types:
                    for r in rls_lf:
                        if tp.lower() == r.split('_')[2]:
                            rls_types.append(r)     
                
                crs_ids = [] 
                
                # The data with life = expired are not counted into totals         
                if lf_low == 'expired':                           
                    for r in rls_types:
                        score = u.total_for_releases(releases, r)
                        if score is not None:
                            crs_ids.extend(list(score.record_numbers))
                   
                    # Remove duplicates           
                    expired_crs_ids = list(set(crs_ids))                  
                    datalem['expired'] = [(','.join(expired_crs_ids), len(expired_crs_ids))] 
                    
                else:
                    # totals = sum of data in state: pending, aging, inactive
                    rls_states = []        
                    for s in states:
                        for r in rls_types:
                            rla = r.split('_')
                            if s.lower().endswith(rla[len(rla) - 2]):
                                rls_states.append(r)
                                
                    for r in rls_states:
                        score = u.total_for_releases(releases, r)
                        if score is not None:
                            crs_ids.extend(list(score.record_numbers))
                            
                    # Remove duplicates        
                    crs_ids = list(set(crs_ids))
                    sub_totals.append((','.join(crs_ids), len(crs_ids)))
                    total_crsids.extend(crs_ids)
            
            # Remove all 0 data rows
            if len(total_crsids) == 0 and len(expired_crs_ids) == 0 :
                continue;
            
            datalem['uid'] = u.id                                

            # Remove duplicates 
            total_crsids = list(set(total_crsids))
            
            # Put the total in as the 1st element
            totals = [(','.join(total_crsids), len(total_crsids))]
            
            #Append other sub-totals of each cr_life
            totals.extend(sub_totals)
            
            datalem['totals'] = totals
            
            if user == 'junos' and reports == '1':    
                if u.id.find('-') < 0:
                    datalem['is_BG'] = 'True'
                else:
                    datalem['is_BG'] = 'False'  
            
            datarows.append(datalem) 
       
    context = { 'title' : settings.CRS_PROPS['title'],
                'page_version' : settings.CRS_PROPS['version'],
                'crs_user' : user,
                'reports' : reports,
                'all_releases' : all_releases,
                'all_states' : all_states,
                'all_types' : all_types,
                'lives' : lives,
                'data' : datarows,
                'crsurl' : 'https://' + settings.CRS_PROPS['host'] + settings.CRS_PROPS['web_app_path'],
                'dashcrsurl' : DashUtil.make_url('crs'),
                'page' : "crs"
              }

    return render_to_response('ui/crs.html', {}, RequestContext(request, context))

def trend(request, data_type, report_type, uid):
    type_check(data_type)
    if report_type not in ['simple','combined']:
        raise ValueError('report type must be either simple or combines, got %s' % report_type)
    sr = SnapshotReporter(data_type)
    report = getattr(sr, '%s_trend_report' % report_type)(uid)
    # mimetype was 'application/vnd.ms-excel'
    response = HttpResponse(report, mimetype='text/csv')
    response['Content-Disposition'] = 'attachment; filename=%s_%s_%s.csv' % (uid, data_type, report_type)
    return response

def alive(request):
    return HttpResponse('yes', mimetype='text/plain')

def instance_debug(request):
    context = {}
    context['mode'] = settings.MODE
    if settings.MODE == 'proxy':
        # force instance choosing logic to happen
        dash.ProxyHelper.current_instance()
        context['current_instance'] = dash.ProxyHelper._ProxyHelper__current_instance
        loading_instance = dash.ProxyHelper._ProxyHelper__loading_instance
        context['loading_instance'] = loading_instance
        if loading_instance:
            loading, last_update, percent_loaded = dash.ProxyHelper.instance_loading_info(loading_instance)
            context['loading'] = loading
            context['last_update'] = last_update.strftime('%c')
            context['percent_loaded'] = percent_loaded
        else:
            context['loading'] = False
    else:
        loading = dash.LOADING_LOCK.locked()
        context['loading'] = loading
        if loading:
            context['percent_loaded'] = min(99,dash.PERCENT_LOADED)

    return HttpResponse(cjson.encode(context), mimetype='text/plain')

def last_updated(request):
    # always use the junos user
    user = get_user('junos')
    return HttpResponse(user.last_updated, mimetype='text/plain')

def hosting_server(request):
    hostname = socket.gethostname()
    return HttpResponse(hostname, mimetype='text/plain')

def json_ready_proxy(request):
    if request.user and request.user.username and not request.user.is_superuser:
        return error(request, "Admistrator-only function",
            "Go away, or I will replace you with a very small shell script.")
    try:
        dash.async_instance_load_all()
        retval = 'ok'
    except FATAL_EXCEPTIONS:
        raise
    except Exception, e:
        retval = "error: %s" % e
    return HttpResponse(retval, mimetype='text/plain')

# --- start proxy code from  http://code.google.com/p/django-webproxy/ ---
# there are some adjustments just to determine the instance to use and
# error hanelding if both instances are loading.
import asyncore, socket
def proxy(request):
    # missing Content-Type, Content-Length
    # added E-Host, E-Addr

    try:
        HOST = dash.ProxyHelper.current_instance()
        extra = urlencode({'_environ_REMOTE_USER' : request.environ.get('REMOTE_USER',settings.DEFAULT_REMOTE_USER),
                           '_environ_REMOTE_ADDR' : request.environ['REMOTE_ADDR']
                          })
        if request.META['QUERY_STRING']:
            querystring = '%s?%s&%s' % (request.META['PATH_INFO'], request.META['QUERY_STRING'], extra)
        else:
            querystring = '%s?%s' % (request.META['PATH_INFO'], extra)
        server_protocol = request.META['SERVER_PROTOCOL']

        data = []
        data.append(' '.join([request.method, querystring, server_protocol]))#
        for a, b in request.environ.iteritems():
            if a.startswith('HTTP_'):
                a = header_name(a)
                data.append('%s %s'%(a,b))
        if 'CONTENT_TYPE' in request.environ:
            data.append('Content-Type: %s' % request.environ['CONTENT_TYPE'])
            data.append('Content-Length: %s' % request.environ.get('CONTENT_LENGTH','0'))
        data = '\r\n'.join(data)+'\r\n\r\n'
        if request.method == 'POST':
            data += request.body
        # leaving debugging in place for when there is anothr proxy problem
#        print data
#        print dir(request)
#        print 'raw:'
#        print request.raw_post_data
#        print request.REQUEST.items()
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.connect(HOST)

        sock.sendall(data)
        output = []
        while True:
            #print 'doing'
            buf = sock.recv(1024)
            if buf:
                output.append(buf)
            else:
                break
        incoming = ''.join(output)
        #print incoming

        if incoming.startswith('HTTP/'):
            #print incoming[:incoming.index('\r\n')]
            status_line = incoming[:incoming.index('\r\n')].split()
            server_protocol, status_code = status_line[0],status_line[1]
            headers = incoming[incoming.index('\r\n')+2:incoming.index('\r\n\r\n')]
            content = incoming[incoming.index('\r\n\r\n')+4:]

            response = HttpResponse(content, status=int(status_code))
            for i in headers.split('\r\n'):
                #print i[:i.index(':')], i[i.index(':')+2:]
                response[i[:i.index(':')]] = i[i.index(':')+2:]

            return response
        else:
            return HttpResponse2(incoming)
    except:
        import traceback
        return HttpResponse(traceback.format_exc(), mimetype='text/plain')


def header_name(name):
    """Convert header name like HTTP_XXXX_XXX to Xxxx-Xxx:"""
    words = name[5:].split('_')
    for i in range(len(words)):
        words[i] = words[i][0].upper() + words[i][1:].lower()

    result = '-'.join(words) + ':'
    return result


class HttpResponse2(object):
    status_code = 200

    def __init__(self, content=''):
        if not isinstance(content, basestring) and hasattr(content, '__iter__'):
            self._container = content
            self._is_string = False
        else:
            self._container = [content]
            self._is_string = True
        self.cookies = {}#SimpleCookie()
        self._headers = {}#{'content-type': ('Content-Type', content_type)}

    def items(self):
        return self._headers.values()

    def __iter__(self):
        self._iterator = iter(self._container)
        return self

    def next(self):
        chunk = self._iterator.next()
        if isinstance(chunk, unicode):
            chunk = chunk.encode(self._charset)
        return str(chunk)
# --- end proxy code from  http://code.google.com/p/django-webproxy/ ---


def id_list_to_object_list(id_list, id_2_obj_dict, empty_ok=False):
    """ Take a list of object ids and a dict of id:object, and return a list
    of the objects corresponding to those ids.  If any of the ids are invalid,
    return an error message, into which you should interpolate the name of the
    object type.
    """
    if not empty_ok and not id_list:
        return [], "No %s supplied."
    if len(id_list) == 1 and \
            (id_list[0].find(',') > -1 or id_list[0].find(' ') > -1):
        # comma or space separated list of ids
        id_list = id_list[0].replace(',', ' ').split()
    bad_ids = []
    objects = []
    for id_ in id_list:
        if not id_: continue
        try:
            objects.append(id_2_obj_dict[id_])
        except KeyError:
            bad_ids.append(id_)
    if bad_ids:
        return objects, "The following %%s%s %s not valid: %s" % \
                   (len(bad_ids) > 1 and 's' or '',
                    len(bad_ids) > 1 and 'are' or 'is',
                    ', '.join(bad_ids))
    return objects, None

def get_user(uid, fall_back_to_junos=True):
    try:
        return dash.User.all[uid.strip()]
    except KeyError:
        if fall_back_to_junos:
            return dash.User.JUNOS
        else:
            raise dash.DashError('User "%s" not found in Dashboard.' % uid)

def convert_user(context):
    """
    used to make a data set able to be converted into JSON
    it looks for user objects in specific places and replaced them with the
    string representation so that the dict can be turned into JSON
    NOTE: All of the editing is done in place, so it does not return anything
    """
    user = context['usr']
    context['uid'] = user.id
    display_str = user.id
    if (not user.is_BG and user.id != 'junos') :
        display_str = user.bus_memberships[0].upper() + " / " +  display_str
    context['displayName'] = display_str
    context['usr'] = str(user)
    for item in context['data']:
        if item.has_key('user'):
            item['user'] = str(item['user'])

# QIR Reporting stuff
def qir_report(request):
    return render_to_response('ui/qir_report.html', {}, RequestContext(request, {
        'title':"QIR Reporting",
        'page_version' : "1.0",
    }))

# QIR Reporting stuff
def qir_report_test_trusted(request):
    return render_to_response('ui/qir_test_trusted.html', {}, RequestContext(request, {
        'title':"QIR Reporting - Testing with Trusted Auth",
        'page_version' : "1.0",
    })) 

# Component Reporting
def component_report(request):
    return render_to_response('ui/component_report.html', {}, RequestContext(request, {
        'title':"Component Reporting",
        'page_version' : "1.0",
    }))
    

# Newly in Fix.
def newlyin_pr_fix(request, relname=None,  uid=None):
    """ Show BU Scorecard for a release. """
    
    is_mozilla = request.META.get('HTTP_USER_AGENT', '').find('Gecko/') > 0
    if not relname:
        relname = request.GET.get('relname', None)
        if not relname:
            # Compatibility with the old Dashboard
            relname = request.GET.get('grname', None)
    if not uid:
        uid = request.GET.get('uid','junos')

    # compatible with old release format
    relname = dash.createCompatibleBranch(relname)
    try:
        release = dash.Records.release[relname]
    except KeyError:
        return error(request, 'Release "%s" not found' % (relname))

    if release['release'] not in dash.Records.pr_fix_releases:
        return error(request, 'Release "%s" data is not calculated for PR-fix yet' % (release['release']))

    cdm_release = False
    cdm_v2_release = False
    release_look_for = relname

    numeric_release_value = dash.findNumericRelease(release['relname'])
    rel_type = dash.findReleaseType_from(relname)
    if (float(numeric_release_value) >= dash.cdm_v2_release_cutoff):
        cdm_v2_release = True
        release_look_for = "%s"%(relname)

    if (float(numeric_release_value) >= dash.cdm_release_cutoff and float(numeric_release_value) < dash.cdm_v2_release_cutoff):
        cdm_release = True
        release_look_for = "%s"%(relname)

    matrix_item_view = [
              {'uid':'total_pr', 'displayName':'Total PR', 'condition':'80% Required at IBRG <br />95% Required at R1',
               'rules':{
                        'denominator':[{'rule_id':'all_open_prs_fix_newly_in'}],
                        'numerator':[{'rule_id':'all_closed_prs_fix_newly_in'}],
                        },
               'label':{
                        'denominator':'Total Open','numerator':'Closed','result':' % fixed '
                        },
               'PRlabel':{
                        'denominator':'Open Total PR','numerator':'Closed Total PR','result':' % '
                        },
               },


            ]

    count_page_type = "newlyinpr"
    TOT_count = ""
    dev_branch_type = "yes"
    report_pr_count = ""

    # we use direct reports of JUNOS
    data_id = get_user(uid,fall_back_to_junos=False)
    if (uid == "junos"):
        data = [{'uid':data_id.id,'displayName':'Release %s' % release_look_for}]
    else:
        data = [{'uid':data_id.id,'displayName':data_id.id}]
    for direct_re in data_id.direct_reports:
        temp_ = {}
        temp_['uid'] = direct_re.id
        display_str = direct_re.id
        if (not direct_re.is_BG) :
            display_str = direct_re.bus_memberships[0].upper() + " / " + display_str
        elif (direct_re.is_BG):
            display_str = display_str.upper()
        temp_['displayName'] = display_str
        link_str = DashUtil.make_url("newlyin_pr_fix", args=[direct_re.id,relname])
        temp_['linkDisplayName'] = link_str
        data.append(temp_)

    # process the matrix and do the calculation
    for datum in data:
        user = get_user(datum['uid'], fall_back_to_junos=False)
        datum['user'] = user
        for column in matrix_item_view :
            datum[column['uid']] = {}

            numerator = 0
            numerator_link = "0"
            _recnum = []
            _cols = []
            _hash = ""
            for item_numerator in column['rules']['numerator']:
                _type = ""
                try:
                    _score = user.statuses[release_look_for].scores_by_rule_id[item_numerator['rule_id']]
                 
                    num = _score.get_special_count("pr","%s%s%s_%s%s" % (report_pr_count,TOT_count,count_page_type, dev_branch_type, _type))
                    _recnum.extend(_score.get_special_record("pr","%s%s%s_%s%s" % (report_pr_count, TOT_count,count_page_type, dev_branch_type, _type)))
                    _cols.extend([ col for col in _score.rule.columns if col not in _cols])
                    _hash = _hash + _score.id_hash
                except (AttributeError,KeyError) as err:
                    num = 0
                numerator = numerator + num
            if numerator > 0 :
                numerator_link =  __make_link( _recnum, _cols, user,is_mozilla, _hash,\
                                               numerator,\
                                               column['PRlabel']['numerator'])

            # denominator calculation
            denominator = 0
            denominator_link = "0"
            _recnum = []
            _cols = []
            _hash = ""
            for item_denominator in column['rules']['denominator']:
                _type = ""
                try :
                    _score = user.statuses[release_look_for].scores_by_rule_id[item_denominator['rule_id']]
                    num = _score.get_special_count("pr", "%s%s%s_%s%s" % (report_pr_count, TOT_count,count_page_type, dev_branch_type, _type))
                    _recnum.extend(_score.get_special_record("pr", "%s%s%s_%s%s" % (report_pr_count, TOT_count,count_page_type, dev_branch_type, _type)))
                    _cols.extend([ col for col in _score.rule.columns if col not in _cols])
                    _hash = _hash + _score.id_hash
                except (AttributeError,KeyError) as err:
                    num = 0
                denominator = denominator + num
            if denominator > 0 :
                denominator_link = __make_link( _recnum, _cols, user, is_mozilla, _hash, \
                                                denominator, column['PRlabel']['denominator'])

            # percentage
            perc = 0
            if denominator == 0 and numerator == 0:
                perc = 100
            else :
                perc = round(((float(numerator)/float(numerator+denominator)) * 100),2)

            perc_color = "transparent"
            color_text = "#000"

            # changed for DEEP_RLI-1635
            # if this release is not a CDM release, OR if it *is* a CDM release, and we've passed the IBRG cutoff

            # changed for DEEP_RLI-1635
            # if this release is not a CDM release, OR if it *is* a CDM release, and we've passed the IBRG cutoff
            if not dash.Records.release[relname]['CDMrelease'] or \
                    (dash.Records.release[relname]['CDMrelease'] and dash.Records.release[relname]['ibrg_past']):
                # have different condition for blocker
                if column['uid'] == "blocker":
                    if perc < 90:
                        perc_color = "red"
                        color_text = "#FFF"
                    elif (perc < 100 and perc >= 90):
                        perc_color = "yellow"
                    elif perc == 100:
                        perc_color = "#00FF11"
                else:
                    if perc < 85:
                        perc_color = "red"
                        color_text = "#FFF"
                    elif (perc <= 95 and perc >= 85):
                        perc_color = "yellow"
                    elif perc > 95:
                        perc_color = "#00FF11"
            else:
                # have different condition for blocker
                if column['uid'] == "blocker":
                    if perc < 75:
                        perc_color = "red"
                        color_text = "#FFF"
                    elif (perc <= 85 and perc >= 75):
                        perc_color = "yellow"
                    elif perc > 85:
                        perc_color = "#00FF11"
                else:
                    if perc < 70:
                        perc_color = "red"
                        color_text = "#FFF"
                    elif (perc <= 80 and perc >= 70):
                        perc_color = "yellow"
                    elif perc > 80:
                        perc_color = "#00FF11"
            # count the PRs requirement
            prs_needed = 0
            if denominator > 0 :
                prs_needed = round((float(numerator + denominator) * 0.95)) - numerator
                if column['uid'] == "blocker" :
                    prs_needed = round((float(numerator + denominator) * 1)) - numerator
                # we dont need to add more bug (negative)
                if prs_needed < 0 :
                    prs_needed = 0
            datum[column['displayName']]=[
                                  {'value':"%d" % prs_needed,'link': "%d" % prs_needed,'background-color':'transparent','color_text':"#000000"},
                                 ]
            datum[column['uid']] = [
                                       {'value':numerator,'link':numerator_link,'background-color':'transparent','color_text':"#000"},
                                       {'value':denominator, 'link':denominator_link,'background-color':'transparent','color_text':"#000"},
                                       {'value': "%.1f %%" % perc , 'link' : "%.1f %%" % perc, 'bgcolor':perc_color,'color_text':color_text},
                                       ]
        datum["displayed"] = False
        for column in matrix_item_view:
            row_part = datum[column['uid']]
            if float(row_part[0]['value']) != 0 or float(row_part[1]['value']) != 0:
                datum["displayed"] = True
                break

    feed_url = DashUtil.make_url("newlyin_pr_fix", args=[uid, relname])
    context = {
        'title':'Newly Introduced PR Bug Fix Report for - %s' %(release_look_for),
        'data': data,
        'matrix': matrix_item_view,
        'count_type': count_page_type,
        'dev_branch_type' : dev_branch_type ,
        'report_pr_count' : "all" if report_pr_count=="" else report_pr_count,
        'release_name' : release_look_for,
        'feed_url' : feed_url,
        'is_cdm' : cdm_release or cdm_v2_release,
        'is_tot' : rel_type == '',
        'usr':   get_user('junoscore', fall_back_to_junos=False)
    }

    format = request.GET.get('format','')
    if format == 'dict':
        return HttpResponse(pformat(context), mimetype='text/plain')
    else:
        return render_to_response('ui/newlyin-release-prfix.html', {}, RequestContext(request, context))


# Get existing rating results from UI_Rating table
def vote_result(request):
    now = datetime.datetime.now()
    topopportunity = ''
    # fetch the respective active ratings based on current month
    onestar=Rating.objects.filter(votedatentime__month=now.month,votedatentime__year=now.year,activevote=1,rating=1).count()
    twostar=Rating.objects.filter(votedatentime__month=now.month,votedatentime__year=now.year,activevote=1,rating=2).count()
    threestar=Rating.objects.filter(votedatentime__month=now.month,votedatentime__year=now.year,activevote=1,rating=3).count()
    fourstar=Rating.objects.filter(votedatentime__month=now.month,votedatentime__year=now.year,activevote=1,rating=4).count()
    fivestar=Rating.objects.filter(votedatentime__month=now.month,votedatentime__year=now.year,activevote=1,rating=5).count()
    totalvotes=Rating.objects.filter(votedatentime__month=now.month,votedatentime__year=now.year,activevote=1).count()


    # determine if UI_rating table is empty for the current month based on number of votes
    try:
      average_rating="%.1f"%(float(Rating.objects.filter(votedatentime__month=now.month,votedatentime__year=now.year,activevote=1).\
                                   aggregate(Sum('rating'))['rating__sum'])/totalvotes)
    except:
      average_rating = "0.0"

    if (Rating.objects.exclude(topopportunity='null').filter(votedatentime__month=now.month,votedatentime__year=now.year).count()>0):
        topopportunity=Rating.objects.exclude(topopportunity='null').filter(votedatentime__month=now.month,votedatentime__year=now.year).\
                                        values('topopportunity').annotate(Count('topopportunity')).order_by('-topopportunity__count')[0]['topopportunity']
    message={
              "month":now.strftime('%B'),
              "year":now.year,
              "average_rating":average_rating,
              "noofvotes":totalvotes,
              "topopportunity":topopportunity,
              "starvalues": {
                        "FiveStar":fivestar,
                        "FourStar":fourstar,
                        "ThreeStar":threestar,
                        "TwoStar":twostar,
                        "OneStar":onestar
                    },

            }

    return HttpResponse(json.dumps(message))

# this function is used to update or insert the ui_rating table with the current rating
def user_vote(request):
    now = datetime.datetime.now()

    # check if the user is entering addtional information after he has casted his rating
    if(request.POST['userrating']!=""):
      if (Rating.objects.filter(votedatentime__month=now.month,votedatentime__year=now.year,username=request.user.username,activevote=1).count()>0):
        Rating.objects.filter(votedatentime__month=now.month,votedatentime__year=now.year,username=request.user.username,activevote=1).update(activevote=0)

      # update the existing vote
      vote=Rating(username=request.user.username,rating=request.POST['userrating'],topopportunity=request.POST['toparea'],\
                                                                                        votedatentime=datetime.datetime.now(),activevote=1)
      vote.save()
    else:
      #insert a new rating
      Rating.objects.filter(votedatentime__month=now.month,votedatentime__year=now.year,username=request.user.username,activevote=1).\
                                                                                update(topopportunity=request.POST['toparea'])

    #get the latest rating results
    onestar=Rating.objects.filter(votedatentime__month=now.month,votedatentime__year=now.year,activevote=1,rating=1).count()
    twostar=Rating.objects.filter(votedatentime__month=now.month,votedatentime__year=now.year,activevote=1,rating=2).count()
    threestar=Rating.objects.filter(votedatentime__month=now.month,votedatentime__year=now.year,activevote=1,rating=3).count()
    fourstar=Rating.objects.filter(votedatentime__month=now.month,votedatentime__year=now.year,activevote=1,rating=4).count()
    fivestar=Rating.objects.filter(votedatentime__month=now.month,votedatentime__year=now.year,activevote=1,rating=5).count()
    totalvotes=Rating.objects.filter(votedatentime__month=now.month,votedatentime__year=now.year,activevote=1).count()
    average_rating="%.1f"%(float(Rating.objects.filter(votedatentime__month=now.month,votedatentime__year=now.year,activevote=1).\
                                                                                            aggregate(Sum('rating'))['rating__sum'])/totalvotes)

    if (Rating.objects.exclude(topopportunity='null').filter(votedatentime__month=now.month,votedatentime__year=now.year).count()>0):
      topopportunity=Rating.objects.exclude(topopportunity='null').filter(votedatentime__month=now.month,votedatentime__year=now.year).\
                                    values('topopportunity').annotate(Count('topopportunity')).order_by('-topopportunity__count','-votedatentime')[0]['topopportunity']
    else:
      topopportunity=""

    message={
              "month":now.strftime('%B'),
              "year":now.year,
              "average_rating":average_rating,
              "noofvotes":totalvotes,
              "topopportunity":topopportunity,
              "starvalues": {
                        "FiveStar":fivestar,
                        "FourStar":fourstar,
                        "ThreeStar":threestar,
                        "TwoStar":twostar,
                        "OneStar":onestar
                    }
            }

    return HttpResponse(json.dumps(message))

