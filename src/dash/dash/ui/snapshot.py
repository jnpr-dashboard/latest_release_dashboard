#!/usr/bin/env python

import csv
import datetime
import logging
import os
import re
import sys
import time
import traceback
import urllib2

from cStringIO import StringIO
from optparse import OptionParser
from pprint import pformat

import cjson

# should only be used when developing, this should always be False otherwise
SKIP_ERRORS = False
#SKIP_ERRORS = True

URL_BASE = 'http://127.0.0.1:80/dashboard/'
STORE_PATH = '/opt/dashboard/snapshot'
USER = 'junos'


# columns that will not cause an error if they are missing
# this is generally for when columns are added later and you don't want an error message showing up

PR_COLUMNS = ['Reference','Current','Delta',
              'Reference by Responsible','Current by Responsible','Delta by Responsible',
              'Not Dev Owner Reference by Responsible','Not Dev Owner Current by Responsible','Not Dev Owner Delta by Responsible']

DOCPR_COLUMNS = ['Doc Reference', 'Doc Current' , 'Doc Delta',
                 'CVBC Reference','CVBC Current', 'CVBC Delta']
PR_COLUMNS_MISSING_OK = []

TYPES_TO_COLUMNS = {
                    'backlogprs': [PR_COLUMNS, PR_COLUMNS_MISSING_OK],
                    'docbacklog': [DOCPR_COLUMNS, PR_COLUMNS_MISSING_OK]
                   }

# pr fields to capture
PR_FIELDS = [
    'number',
    'synopsis',
    'state',
    'category',
    'product',
    'planned-release',
    'blocker',
    'submitter-id',
    'responsible',
    'dev-owner',
    'originator',
    'attributes',
    'last-modified',
    'updated-by-responsible',
    'arrival-date',
    'fix-eta',
    'committed-release',
    'conf-committed-release',
    'confidential',
    'rli',
    'cust-visible-behavior-changed',
    'problem-level'
] 

# how many prs to query at once
PR_BATCH_SIZE = 100
# how much sleep should be done between batch gets
PR_BATCH_WAIT = .5


# determine if the string is a valid year
IS_YEAR_RE = re.compile(r'^\d{4}$')
# determine if the string is a valid date
IS_DATE_RE = re.compile(r'^\d{4}-\d{2}-\d{2}$')
# the prs are in a link
LINK_RE =re.compile(r'.*prs=(.*?)(?:$|&).*', re.DOTALL)
# the prs are in a form field 
FORM_RE =re.compile(r'.*name="prs" value="(.*?)".*', re.DOTALL) 
# date from file path
FILE_DATE = re.compile(r'.*(\d{4}-\d{2}-\d{2}).*')

def type_check(data_type):
    if not data_type in TYPES_TO_COLUMNS.keys():
        raise ValueError('data_type must be one of: %s; got %s' % ( ','.join(TYPES_TO_COLUMNS.keys()), data_type) )

class SpiderHelper(object):
    """helps to determine what has already been hit and what still needs to be hit"""
    def __init__(self):
        self.to_get = []
        self.gotten = []
        
    def add(self, value):
        if not value in self.to_get + self.gotten:
            self.to_get.append(value)
    
    def got(self, value):
        if value in self.to_get:
            self.to_get.remove(value)
        if not value in self.gotten:
            self.gotten.append(value)
            
    def next(self):
        if self.to_get:
            return self.to_get[0]
        else:
            return None

class SnapshotCollector(object):
    """collects and saves snapshot data"""
    def __init__(self, url_base, data_type, save_prs=True, store_path=STORE_PATH):
        type_check(data_type)
        self.url_template = '%s%s/%%s?format=json' % (url_base, data_type)
        self.data_type = data_type
        self.today = datetime.date.today()
        self.today_str = self.today.strftime('%Y-%m-%d')
        self.store_path = os.path.join(store_path, data_type, str(self.today.year), self.today_str)
        self.save_prs = save_prs
        # pr_columns are the columns which have a link to gnats with a list of
        # prs, and this tells the collector to process that link and
        # turn it into a list of PRs
        if data_type == 'backlog_prs':
            self.pr_columns = [0,1,2,5,6,7,8,9,10]
        elif data_type == 'doc_pr':
            self.pr_columns = [0,1,2]
        else:
            self.pr_columns = [1,3]
    def get_data(self, user):
        """make the url request for the data and return the data as a dict"""
        url = self.url_template % (user)
        #print url
        data = None
        attempts = 0
        while attempts < 100 and not data:
            attempts += 1
            try:
                data = urllib2.urlopen(url,timeout=10).read()
            except:
                # wait a minute before trying again
                print url
                print 'could not get data, waiting 60 seconds'
                if SKIP_ERRORS:
                    print 'skipping'
                    raise ValueError('skipping error')
                print traceback.format_exc()
                #raise
                time.sleep(60)
                continue
            if data:
                try:
                    data = cjson.decode(data)
                except cjson.DecodeError:
                    if data.find('Dashboard is loading') > 0:
                        print 'Dashboard is loading...'
                    else:
                        print 'data=%s' % data
                        print url
                        print 'could not parse data, waiting 60 seconds'
                        if SKIP_ERRORS:
                            print 'skipping'
                            raise ValueError('skipping error')
                    #raise
                    data = None
                    time.sleep(60)
                    continue
        if data:
            self.cleanup(data)
            return data
        else:
            raise ValueError("Was not able to get data after 100 attempts")

    @staticmethod
    def parse_prs(value):
        """return the pr list from the link text"""
        value = str(value)
        if value.find('href="http') > 0:
            # link version
            match = LINK_RE.match(value)
            if match:
                prs = match.groups()[0]
                if prs:
                    return prs.split(',')
        elif value.find('href="#"') > 0:
            # form version
            match = FORM_RE.match(value)
            if match:
                prs = match.groups()[0]
                if prs:
                    return prs.split(',')
                
    def cleanup(self, data):
        """
        remove formatting info, pull PRs out of link fileds
        input parameter data is edited place, so doesn't return anything
        data_type must the key used in the data list, so either mttr or backlog_pr
        """
        for item in data['data']:
            for ditem in item[self.data_type]:
                # remove formatting stuff
                for remove in ['background-color','color_text','bgcolor']:
                    if ditem.has_key(remove):
                        del ditem[remove]
                prs = self.parse_prs(ditem['link'])
                if not prs is None:
                    ditem['prs'] = prs
                del ditem['link']

    def pr_check(self, data):
        """
        ensure that all line items after the first have a pr listed in the first
        one so we knwo which prs to capture
        
        this is only needed for diagnostics and not needed everytime
        """
        first = data['data'][0][self.data_type]
        base_prs = {}
        for column in self.pr_columns:
            base_prs[column] = first[column]['prs']
        for count, item in enumerate(data['data'][1:]):
            row = item[self.data_type]
            for column in self.pr_columns:
                prs = row[column].get('prs',[])
                for pr in prs:
                    if not pr in base_prs[column]:
                        print prs
                        print 'missing pr %s from item %s column %s' % (pr, count, column)

    def save_data(self, data, user):
        file_path = os.path.join(self.store_path, '%s.json' % user)
        
        if not os.path.exists(self.store_path):
            os.makedirs(self.store_path)
        
        # -- for use when data fields change to ensure data is sane, not to be used in production --
        #self.pr_check(data)
        # -- end sanity check
        
        open(file_path, 'w').write(cjson.encode(data))    

    def find_all_users(self, data):
        return [item['uid'] for item in data['data']]
    
    def spider_users(self, sh):
        user = sh.next()
        while user:
            #print 'getting %s' % user
            try:
                data = self.get_data(user)
            except ValueError:
                if SKIP_ERRORS:
                    sh.got(user)
                    break
                else:
                    raise
            self.save_data(data, user)
            sh.got(user)
            for new_user in self.find_all_users(data):
                sh.add(new_user)
            user = sh.next()

    def get_and_save_all(self, user):
        """get the page for the user and all users found "under" this user"""
        sh = SpiderHelper()
        # add the seed user
        sh.add(user)
        # let the spider work.
        self.spider_users(sh)
        if self.save_prs:
            self.get_and_save_prs(user)

    def get_and_save_prs(self, user):
        file_path = os.path.join(self.store_path, '%s.json' % user)
        data = cjson.decode(open(file_path,'r').read())
        # look at self.pr_columns and create a file for each
        # query-pr -v dashboard --list-fields
        # query-pr -v dashboard -f '"%s\n" number' 101
        # 100 at a time, sleep .5 seconds between should be good.
        # get prs into one file and then zip it all up
        # find all prs for user colum in row data, only get it once.
        prs = set()
        # find user row
        for item in data['data']:
            if item['uid'] == user:
                row = item[self.data_type]
                for counter in row:
                    if counter.has_key('prs'):
                        prs.update(counter['prs'])
                # we found our user
                break
        prs = list(prs)
        prs.sort()
        print 'have %s prs to snapshot' % len(prs)
        file_path = os.path.join(self.store_path, '%s_prs.psf' % user)
        f = open(file_path,'w+')
        header = '||'.join(PR_FIELDS)
        f.write('%s\n' % header)
        f.close()
        format = """'"%s" %s'""" % ('||'.join(['%s'] * len(PR_FIELDS)), ' '.join(PR_FIELDS))
        # // forces integer division
        loops = len(prs) // PR_BATCH_SIZE
        leftover = len(prs) % PR_BATCH_SIZE
        for loop in range(loops):
            print 'loop %s of %s' % (loop, loops)
            cmd = 'query-pr -v dashboard -f %s %s >> %s' % (format, ' '.join(prs[loop * PR_BATCH_SIZE:loop * PR_BATCH_SIZE + PR_BATCH_SIZE]), file_path)
            #print cmd
            os.system(cmd)
            time.sleep(PR_BATCH_WAIT)
        if leftover:
            print 'leftover'
            cmd = 'query-pr -v dashboard -f %s %s >> %s' % (format, ' '.join(prs[loops * PR_BATCH_SIZE:]), file_path)
            os.system(cmd)
        tar_file = os.path.join(self.store_path, '%s_prs.tgs' % user)
        cmd = 'tar cfz %s %s && rm %s' % (tar_file, file_path, file_path)
        os.system(cmd)
        print 'done writing out prs'
        
class SnapshotReporter(object):    
    def __init__(self, data_type, store_path=STORE_PATH):
        type_check(data_type)
        self.data_type = data_type
        self.base_store_path = store_path
        self.store_path = os.path.join(store_path, data_type)
        
    def get_files(self, user, start_date=None, end_date=None):
        """
        find all of the files that match the given start and end date
        if start_date is not specified, it defaults to Jan 1 of current year
        if end_date is not specified, it defaults to today

        returns a list of file names in order sorted by date, user
        """
        today = datetime.date.today()
        if start_date is None:
            start_date = datetime.date(today.year, 1, 1)
        if end_date is None:
            end_date = today
        sm = SnapshotManager(self.data_type, self.base_store_path)
        files = [[FILE_DATE.match(f.filename).groups()[0], f.filename] for f in sm.get_files(user, start_date, end_date)]
        return files
    
    def simple_trend_report(self, user, start_date=None, end_date=None):
        """
        a wrapper for combined_trend_report for only one user
        see combined_trend_report for parameter descriptions
        """
        return self.combined_trend_report(user, start_date=start_date, end_date=end_date, single=True)
            
    def combined_trend_report(self, user, start_date=None, end_date=None, single=False):
        """
        user: uid of who to run the report against
        start_date: datetime.date object of when report should start
        end_date: datetime.date object of when report should end
        single: if True, will only pull the data for the user specified.  
                the default of False will pull all of the rows that are on the page for the 
                user specified.
                 
        if dates are left as none, see get_files for default

        makes a report for the user and all other users that appear on that
        users page.
        
        returns a properly formatted CSV file as a string
        the first row will be uid, display name
        
        the column headers are in row 2: user, date 1, ... date n up to however many dates are available
        for backlog_pr there will be two rows for each user in the format of "display name - counter name"
        for mttr there will be four rows for each user in the format of "display name - counter name"
        
        the output file will sort by display name with the exception that the user passed in will 
        always be first. 

        """
        files = self.get_files(user, start_date, end_date)
        if files: 
            # data collection
            columns, missing_ok = TYPES_TO_COLUMNS.get(self.data_type, [None, None])
            if columns is None or missing_ok is None:
                raise ValueError('Unknown data_type %s' % self.data_type) 
            all_dates = []
            #report = {(displayName, uid):{date:{counter_num:value, counter_num:value}}}
            report = {}
            for date, file_path in files:
                data = cjson.decode(open(file_path,'r').read())
                trend_fields = data['matrix'][0]['trend_fields']
                for item in data['data']:
                    # if single, only allow to be processed if it's the user we are looking for, 
                    # otherwise skip to the next row
                    if single and item['uid'] != user:
                        continue

                    try:
                        row = item[self.data_type]
                    except KeyError:
                        # There's some old data laying around that references 
                        # "backlog_pr" instead of "backlogprs"
                        if self.data_type == "backlogprs":
                            # try old key name
                            row = item["backlog_pr"]
                    row_data = {}
                    for column in columns:
                        # get out the fields that are defined for the report
                        # and use trend_fields to find out where they are in the data set
                        #print trend_fields
                        #print column
                        if not trend_fields.get(column, None) is None:
                            # get the value because we know where it is
                            row_data[column] = row[trend_fields[column]]['value']
                        else:
                            # can't find what we are looking for, so it defaults to blank
                            if column in missing_ok:
                                row_data[column] = ''
                            else:
                                row_data[column] = 'MISSING'
                    key = (item['displayName'], item['uid'])
                    if report.has_key(key):
                        report[key][date] = row_data
                    else:
                        report[key] = {date:row_data}
                    if date not in all_dates:
                        all_dates.append(date)
                    # if we got the user we are looking for, we are done.
                    if single and item['uid'] == user:
                        break
            # sort by display name
            display_order = report.keys()
            display_order.sort()
            # look for the input user            
            for index, item in enumerate(display_order):
                # when found, stop the loop
                if item[1] == user:
                    break
            top_user = display_order.pop(index)
            displayName = top_user[0]
            # make input user at top
            display_order.insert(0, top_user)
            # sort the dates
            all_dates.sort()
            
            # file production
            output = StringIO()
            writer = csv.writer(output)
            first = True
            for key in display_order:
                user_info = report[key]
                displayName = key[0]
                if first:
                    writer.writerow(['User/BG/BU','Counter','User/BG/BU - Counter'] + all_dates)
                    first = False
                for column in columns:
                    display = '%s - %s' % (displayName, column)
                    date_info = []
                    for date in all_dates:
                        if user_info.has_key(date):
                            date_info.append(user_info[date][column])
                        else:
                            # we need to insert a blank
                            date_info.append('')
                    writer.writerow([displayName, column, display] + date_info)
            result = output.getvalue()
            output.close()
            return result
        else:
            return ''

class JsonFile(object):
    """abstract editing of a JSON file"""
    def __init__(self, filename, load_now=True):
        """
        filename is the JSON file to load
        load_now: if True, .load() will be called right away, default it True,
                  send as False if for some reason you don't want to load it immediately
        """
        self.filename = filename
        self.is_loaded = False
        self.is_new = False
        self.data = None
        if load_now:
            self.load()

    def create(self):
        # need to make not None so save will work        
        self.data = {}
        # fake being loaded
        self.is_loaded = True
        self.is_new = True

    def delete(self):
        os.remove(self.filename)
        # make it look like it doesn't exist
        self.is_loaded = False
        self.is_new = False
        self.data = {}
                
    def load(self):
        """
        load current filename into data where it can be modified
        """
        if self.is_loaded:
            raise ValueError('%s is already loaded')
        try:
            self.data = cjson.decode(open(self.filename,'r').read())
        except:
            print 'tried to open %s' % self.filename
            print open(self.filename,'r').read()
            raise 
        self.is_loaded = True
        self.is_new = False
        
    def save(self):
        """
        save what's in data to filename
        """
        if not self.is_loaded:
            raise ValueError('there is no open file to save to')
        if self.data is None:
            raise ValueError('data is None which is not valid for saving')
        if self.is_new:
            head, tail = os.path.split(self.filename)
            if not os.path.exists(head):
                os.makedirs(head)
        open(self.filename,'w').write(cjson.encode(self.data))        
        self.is_new = False
        
    def unload(self):
        """
        closes the open filename
        """
        if not self.is_open:
            raise ValueError('%s is not loaded' % self.filename)
        self.data = None
        self.is_loaded = False
        
class SnapshotJsonFile(JsonFile):
    def __init__(self, filename=None, load_now=True, base_path=None, user=None, date=None):
        """
        you can specify either the filename OR
        base_path AND user AND date

        filename, load_now: same as JsonFile
        base_path: path to the snapshot files, should INCLUDE mttr or backlogprs
        user: string, name of user to get
        date: datetime.date, date of file to get, datetime.date 

        if filename is given, that will be used.  if not, base_path, user and date 
        will be used to construct filename
        """
        if not filename is None and (not base_path is None or not user is None or not date is None):
            raise ValueError('filename and one of base_path, user, or date was specified.')
        if filename is None and (base_path is None or user is None or date is None):
            raise ValueError('filename was not specified but not all of base_path, user, and date were specified')
        if filename is None:
            filename = os.path.join(base_path, str(date.year), date.strftime('%Y-%m-%d'), '%s.json' % user)
        else:
            week_path, user_filename = os.path.split(filename)
            year_path, week = os.path.split(week_path)
            base_path, year = os.path.split(year_path)
            # cutoff .json
            user = user_filename[:-5]
            date = datetime.datetime.strptime(week, '%Y-%m-%d').date()
        self.user = user 
        self.date = date
        self.base_path = base_path
        return super(SnapshotJsonFile, self).__init__(filename, load_now=load_now)
        
class SnapshotManager(object):
    """used to manage the files the snapshots have created"""
    def __init__(self, data_type, store_path=STORE_PATH):
        type_check(data_type)
        self.data_type = data_type
        self.store_path = os.path.join(store_path, data_type)

    def get_files(self, user=None, start_date=None, end_date=None):
        """
        find all of the files that match the given user, start and end date
        if user is not specified, all users are returned
        start_date and end_date act like a slice range.  If start_date is not
        specified, there is not start to the range and if end_date is
        not specified, there is no end on the range.  If neither is 
        specified, files for all dates will be returned.
        
        user: string, username to look for
        start_date: datetime.date
        end_date: datetime.date 
        if none of the parameters is specified, you will get all files
        currently available. 

        returns a generator with each item being a JsonFile object opened
        files are returned in a sorted order by date, user
        """
        # the date in this format does correct sorting as a string
        if start_date:
            start_str = start_date.strftime('%Y-%m-%d')
        if end_date:
            end_str = end_date.strftime('%Y-%m-%d')
        # the structure is base will have years and then in years will have directories for each day
        # which will then have a file for each user
        
        # this will be a list of pairs [date as string, full path to file] so we can sort by date
        # and then have access to the full path to the file we are looking for
        year_files = []
        try:
            year_files = os.listdir(self.store_path)
        except:
            logging.error('could not access %s' % self.store_path)
        year_files.sort()
        for y_file in year_files:
            y_path = os.path.join(self.store_path, y_file)
            # are directories that are valid years
            if os.path.isdir(y_path) and IS_YEAR_RE.match(y_file):
                date_files = os.listdir(y_path)
                date_files.sort()
                for d_file in date_files:
                    # in our date range
                    if ( ((start_date and d_file >= start_str) or start_date is None) and   
                         ((end_date and d_file <= end_str) or end_date is None) ):                        
                        d_path = os.path.join(y_path, d_file)
                        # are directories that are valid dates
                        if os.path.isdir(d_path) and IS_DATE_RE.match(d_file):
                            user_files = os.listdir(d_path)
                            user_files.sort()                            
                            if '%s.json' % user in user_files or user is None:
                                if user is None:
                                    for user_file in user_files:
                                        if user_file.endswith('.json'):
                                            f_path = os.path.join(d_path, user_file)
                                            if os.path.exists(f_path):
                                                yield JsonFile(f_path)
                                else:
                                    f_path = os.path.join(d_path, '%s.json' % user)
                                    if os.path.exists(f_path):
                                        yield SnapshotJsonFile(filename=f_path)
        
    
def snapshot(options):
    for data_type in options.data_type:
        print 'snapshotting %s' % data_type
        sc = SnapshotCollector(options.url_base, data_type, save_prs=options.save_prs, store_path=options.store_path)
        sc.get_and_save_all(options.user)
    
def report(options):
    logging.info("report options=%s" % options)
    print 'report options=%s' % options
    for data_type in options.data_type:
        sr = SnapshotReporter(data_type, store_path=options.store_path)
        if options.action == 'simple_report':
            report = sr.simple_trend_report(options.user)
        else:
            report = sr.combined_trend_report(options.user)
        print report


if __name__ == '__main__':
    data_type_choices = TYPES_TO_COLUMNS.keys() + ['all']
    parser = OptionParser()
    parser.add_option('-a','--action', dest="action",
                      help='what to do.  can be either snapshot, simple_report or combined_report. [ default: snapshot ]',
                      default='snapshot')
    parser.add_option('-b','--url_base', dest="url_base", 
                      help="base url to use for snapshotting. [ default: %s ]" % URL_BASE,
                      default=URL_BASE)
    parser.add_option('-u','--user', dest='user', 
                      help='user to use for either snapshotting or to report on. [ default: %s ]' % USER,
                      default=USER)
    parser.add_option('-s', '--store_path', dest='store_path', 
                      help='where files will be written to for snapshotting or read from when reporting. [ default: %s] ' % STORE_PATH,
                      default=STORE_PATH)
    parser.add_option('-d','--data_type', dest='data_type',
                      help='kind of data to work on.  can be: %s. [ default: all ]' % ', '. join(data_type_choices),
                      default='all')
    parser.add_option('-p','--dont_save_prs', dest='save_prs',
                      help="specifiy if you don't want PRs to be saved which are related to the data being snapshotted [ default: no (prs will be saved) ]",
                      action="store_false",
                      default=True)
    
    (options, args) = parser.parse_args()

    if options.data_type not in data_type_choices:
        print 'ERROR: data-type must be one of: %s. got %s' % (', '.join(data_type_choices), options.data_type)
        parser.print_help()
        sys.exit(1)
    if options.data_type == 'all':
        options.data_type = data_type_choices[:-1]
    else:
        options.data_type = [options.data_type]        
    action_choices = ['snapshot','simple_report','combined_report']
    if options.action not in action_choices:
        print 'ERROR: action must be one of: %s. got %s' % (', '.join(action_choices), options.action)
        parser.print_help()
        sys.exit(1)
    
    if options.action == 'snapshot':
        snapshot(options)
    else:
        report(options)
