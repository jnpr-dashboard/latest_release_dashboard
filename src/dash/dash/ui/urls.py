"""
URL config for Dashboard

Dirk Bergstrom, dirk@juniper.net, 2009-03-31

Copyright (c) 2009, Juniper Networks, Inc.
All rights reserved.
"""
import os

from django.conf import settings
from django.conf.urls import *


# Page-like views
urlpatterns = patterns('dash.ui.views',
    url(r'^/?$','agenda',name='agenda-default'),
    

    url(r'^/dwim$','dwim',name='dwim'),
    url(r'^/agenda$','agenda',name='my-agenda'),
    url(r'^/agenda/(?P<uid>[\w\-]+)$','agenda',name='agenda'),
    url(r'^/metdetail$','metric_detail',name='metdetail'),
    #url(r'^/metdetail/(?P<uid>[\w\-]+)/(?P<met_name>[\w\-]+)$','metric_detail',name='metdetail'),
    url(r'^/metdetail/(?P<uid>[\w\-]+)/$','metric_detail',name='metdetail'),
    url(r'^/agenda(?:\.do)?$','agenda',name='agenda-compat'),
    # No 1:1 match for the old releases list...
    url(r'^/releases\.do?$','agenda',name='agenda-compat'),
    # compatibility with old branch value
    url(r'^/release/(?P<relname>(IB[1-5]?\_)?([1-9][0-9]?[\.\_][1-9]?[0-9])(IB[1-5]?)?((\w+)?\_BRANCH)?)/(?P<uid>[\w\-]+)','release',name='release'),
    # compatibility with old dashboard
    url(r'^/release(?:\.do)?$','release',name='release-compat'),
    url(r'^/npis(?:\.do)?$','npis',name='npis'),
    # compatiblity with the old Dashboard URLs
    url(r'^/npi\.do$','agenda',name='npis-compat'),
    url(r'^/matrix-form/','matrix_form',name='matrix-form'),
    url(r'^/matrix/','matrix', kwargs={ 'view_type': 'matrix' },name='matrix'),
    url(r'^/release-matrix/(?P<release_id>(IB[1-5]?\_)?([1-9][0-9]?[\.\_][1-9]?[0-9])(IB[1-5]?)?((\w+)?\_BRANCH)?)','matrix', kwargs={ 'view_type': 'release-matrix' },name='release-matrix'),
    url(r'^/prs/$','matrix', kwargs={ 'view_type': 'prs' },name='prs'),
    url(r'^/prs/(?P<uid>[\w\-]+)','matrix', kwargs={ 'view_type': 'prs' },name='prs-user'),
    # compatiblity with the old Dashboard URLs
    url(r'^/prs\.do$','matrix', kwargs={ 'view_type': 'prs' },name='prs-user-compat'),
    url(r'^/release-prs/(?P<release_id>(IB[1-5]?\_)?([1-9][0-9]?[\.\_][1-9]?[0-9])(IB[1-5]?)?((\w+)?\_BRANCH)?)','matrix', kwargs={ 'view_type': 'release-prs' },name='release-prs'),
    # compatiblity with the old Dashboard URLs
    url(r'^/by-rule\.do$','matrix_compat',name='matrix-compat'),
    # prfix page 
    url(r'^/prfix/(?P<relname>(IB[1-5]?\_)?([1-9][0-9]?[\.\_][1-9]?[0-9])(IB[1-5]?)?((\w+)?\_BRANCH)?)','pr_fix',name='pr_fix'),
    url(r'^/prfix/(?P<uid>[\w\-]+)/(?P<relname>(IB[1-5]?\_)?([1-9][0-9]?[\.\_][1-9]?[0-9])(IB[1-5]?)?((\w+)?\_BRANCH)?)','pr_fix',name='pr_fix'),
    # Total (Newly Introduced) PR Fix 
    url(r'^/newlyin-prfix/(?P<relname>(IB[1-5]?\_)?([1-9][0-9]?[\.\_][1-9]?[0-9])(IB[1-5]?)?((\w+)?\_BRANCH)?)','newlyin_pr_fix',name='newlyin_pr_fix'),
    url(r'^/newlyin-prfix/(?P<uid>[\w\-]+)/(?P<relname>(IB[1-5]?\_)?([1-9][0-9]?[\.\_][1-9]?[0-9])(IB[1-5]?)?((\w+)?\_BRANCH)?)','newlyin_pr_fix',name='newlyin_pr_fix'),

    # Hardening views 
    url(r'^/hdview/(?P<repind>([1-4]))/(?P<relname>([1-9][0-9]?[\.][1-9]?[0-9]))','hd_view',name='hd_view'),
    url(r'^/hdview/(?P<repind>([1-4]))/(?P<uid>[\w\-]+)/(?P<relname>([1-9][0-9]?[\.][1-9]?[0-9]))','hd_view',name='hd_view'),
    # backlog page
    url(r'^/backlogprs/(?P<uid>[\w\-]+)$', 'backlog_prs', name='backlogprs'),
    # Doc and CVBC backlog page
    url(r'^/docbacklog/(?P<uid>[\w\-]+)$', 'doc_cvbc_backlog_prs', name='docbacklog'),
    # MTTR Page 
    url(r'^/mttr/(?P<uid>[\w\-]+)$', 'mttr', name='mttr'),
    # SLA page
    url(r'^/sla/(?P<uid>[\w\-]+)$', 'sla', name='sla'),
    url(r'^/crs/', 'crs', name='crs'),
    # trend report
    url(r'^/trend/(?P<data_type>[\w\-]+)/(?P<report_type>[\w\-]+)/(?P<uid>[\w\-]+)$', 'trend', name='trend'),
    # Help pages
    url(r'^/help(?:\.do)?$','help', kwargs={ 'template':'help' },name='help'),
    url(r'^/help-rules$','help', kwargs={ 'template':'help-rules' },name='help-rules'),
    url(r'^/browser-reqs(?:\.do)?$','help', { 'template':'browser-reqs' },name='browser-reqs'),
    # BU/BG LIST
    url(r'^/bglist(?:\.do)?$','help',{ 'template':'help_bg' },name='help_bglist'),
    url(r'^/tour$','tour',name='tour'),
    url(r'^/tour/(?P<page_no>[\d]+)$','tour',name='tour-page'),
    url(r'^/rule/(?P<id>[\w\-]+)','rule',name='rule'),
    url(r'^/rules$','rule',name='rules'),
    url(r'^/rule-summary$','rule', {'template': 'rule-summary'},name='rule-summary'),
    url(r'^/last-updated$','last_updated', name='last-updated'),
    url(r'^/hosting-server$','hosting_server', name='hosting-server'),
    # QIR Report
    url(r'^/qir-report$','qir_report', name='qir-report'),
    url(r'^/qir-test$','qir_report_test_trusted', name='qir-test-trusted'),

    # Used by the Java layer to signal us to read updated JSON data
    url(r'^/json-ready', 'json_ready', name='json-ready'),
    # Used to make app-relative URLs in templates.
    url(r'^/$', 'static', {'template':''}, name='base'),
    # Component Report
    url(r'^/component-report$','component_report', name='component-report'),

    # Rating App URL
    url(r'^/vote_result/','vote_result',name='vote_result'),
    url(r'^/user_vote/','user_vote',name='user_vote'),

)

# AJAX or other non-page views
# >>> You MUST put /ajax at the beginning of each URL pattern!
urlpatterns += patterns('dash.ui.ajax',
    url(r'^/ajax/(?P<ac_type>[1n])-user$', 'user', name='user'),
    # generate tableau URL with ticket if trusted authenticate is supported
    url(r'^/ajax/generate_tableau_url/(?P<workbook>[\w\-\.]+)/(?P<reportname>[\w\-\.]+)$', 'generate_tableau', name='gen-tableau-url'),
    url(r'^/ajax/generate_tableau_url/(?P<workbook>[\w\-\.]+)/(?P<reportname>[\w\-\.]+)/(?P<parameter>[\w\-\.\:\=\&]+)$', 'generate_tableau', name='gen-tableau-url'),
    url(r'^/ajax/generate_tableau_url/(?P<workbook>[\w\-\.]+)/(?P<reportname>[\w\-\.]+)/(?P<parameter>[\w\-\.\:\=\&\,]+)/(?P<server_name>[\w\-\.]+)$', 'generate_tableau', name='gen-tableau-url'),
)

# To serve static foo via the dev server.
urlpatterns += patterns('',
    (r'^/(?P<path>(inc|images).*)$', 'django.views.static.serve',{'document_root': os.path.dirname(__file__), 'show_indexes': True}),
)



        
