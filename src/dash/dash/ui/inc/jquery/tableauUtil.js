/**
 * @author sannymulyono
 */

/**
 * Show tableau graph on the embedded frame
 * @param frameid, ID of the frame
 * @param workbook, Tableau workbook name
 * @param reportname, Tableau report name
 * @param parameters, any additional parameters 
 */
function showEmbedTableau(frameid ,workbook, reportname, parameters){
	LoadTableau(frameid);
	if (parameters == undefined){
		$.get('/dashboard/ajax/generate_tableau_url/' + workbook + '/' + reportname + '/:tabs=no', function(data){
			$("#" + frameid).attr("src", data);
			FinishLoad(frameid);
		});
	} else {
		$.get('/dashboard/ajax/generate_tableau_url/' + workbook + '/' + reportname , function(data){
			$("#" + frameid).attr("src", data + parameters);
			FinishLoad(frameid);
		});
	}
}

/**
 * Used for development purpose where the server is defaulted to tableau-dev.juniper.net
 * 
 * @param frameid
 * @param workbook
 * @param reportname
 * @param parameters
 */
function showEmbedTableau2(frameid, workbook, reportname, parameters){
	LoadTableau(frameid);
	if (parameters == undefined){
		$.get('/dashboard/ajax/generate_tableau_url/' + workbook + '/' + reportname + '/:tabs=no/tableau-dev.juniper.net', function(data){
			$("#" + frameid).attr("src", data);
			FinishLoad(frameid);
		});
	} else {
		$.get('/dashboard/ajax/generate_tableau_url/' + workbook + '/' + reportname + '/' + parameters + '/tableau-dev.juniper.net', function(data){
			$("#" + frameid).attr("src", data + '/' + parameters + '/tableau-dev.juniper.net');
			FinishLoad(frameid);
		});
	}
}

//Show or hide "loading" indicator and put it in the middle of page ( or near frame )
function LoadTableau(frameid) {
  // Put the loading somewhere in the middle of page
  var offset = $('#' + frameid).offset();
  $('#loading').css('top', offset.top );
  $('#loading').bgiframe().show();
}

function FinishLoad(frameid) {
  $('#loading').fadeOut('slow');
}
