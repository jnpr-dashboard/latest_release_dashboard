$jq(document).ready(function(){
    var useRelease = "";
    if (releaseId != "False"){
        useRelease = "/" + releaseId;
    }

    jnpr.component.ruleItem.draw({
        container  : "#pr-agenda-container",
        templateEl : "#agenda-rule-row-template",
        templateHeaderEl : "#agenda-rule-header-template",
        getUrl     : EDW_ENDPOINT_URL + "/rest/v1/agenda/" + userId + useRelease + "?outputmode=json_groups",
        jqueryObj : $jq,
        jqueryContext : $
    });
});
