$jq(document).ready(function(){

    var rules = "prs_user,regression_prs,major_prs,critical_prs,major_prs," +
                "minor_prs,blocker_prs,test_blockers,beta_blocker_prs"; 
    var useRelease = "";
    if (releaseId != "False") {
        useRelease = "/" + releaseId;
    }
    jnpr.component.ruleTotal.draw({
        container  : "#pr-agenda-totals-container",
        container1  : "#pr-agenda-totals-blocker-container",
        templateEl : "#agenda-rule-totals-template",
        templateElBlkr : "#agenda-rule-totals-blocker-template",
        getUrl     : EDW_ENDPOINT_URL + "/rest/v1/metrics/" + userId + useRelease + "/" + rules,
        jqueryObj : $jq,
        jqueryContext : $
    });
});
