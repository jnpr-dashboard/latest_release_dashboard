/*
 Drawing Rule Totals Metrics in Agenda page (total box)
 */
(function (fn){
    var LOADING_TEXT = "<span class='icon icon-refresh icon-spin'></span> Loading data ...... please wait";
    var ERROR_TEXT = "<span class='icon icon-warning-sign'></span> Something wrong... make user you specified username!";
    var EMPTY_DATA_TEXT = "<span class='icon icon-time'></span> No data returned......";

    var _getURL = function(_jq, url, params){
        return _jq.ajax({
            method : "GET",
            url : url,
            dataType : "json"
        });
    };

    var getJSON = function(data){
        if (typeof data != "object") {
            try {
                data = JSON.parse(data);
            } catch (ex){
                data = {};
            }
        }
        return data;
    };

    var _drawWithTemplate = function(options, data){
        var _jq = options.jqueryObj;

        // TODO:XXX Rule name Mapping needs to be moved to somewhere
        var ruleNames = {"prs_user": "Total PRs",
                "critical_prs": "Critical PRs",
                "minor_prs": "Minor PRs", "major_prs": "Major PRs",
                "regression_prs": "Regression PRs",
                "blocker_prs": "All Blocker PRs",
                "test_blockers": "Test Blockers",
                "beta_blocker_prs": "Beta Blocker PRs" };


        if (_jq === undefined){ return ; }
        if (!data.hasOwnProperty("data")){ return ;}

        var newEl = [];
        var templateStr = _jq(options.templateEl).html();
        var templateStr1 = _jq(options.templateElBlkr).html();

        for (var idx in data.data) {
            data.data[idx]["hash"] = idx;
            if (data.data[idx]["metricsName"] in ruleNames) {
                data.data[idx]["ruleName"] = ruleNames[data.data[idx]["metricsName"]];
            }
        }
        
        _jq(options.container).html(_.template(templateStr, data));
        _jq(options.container1).html(_.template(templateStr1, data));

    };

    var _mixedObject = {
        options : {
            container  : "body",
            templateEl : "agenda-rule-totals-template",
            templateElBlkr : "agenda-rule-totals-blocker-template",
            getUrl     : "/v1/metrics/",
            jqueryObj  : window.jQuery || {},
            jqueryContext : window.jQuery || {} // legacy or existing jquery in the page
        },
        ruleData : [],
        draw : function(newOpt){
            var jq = this.options.jqueryObj;
            if (jq === undefined){
                // if jquery is not defined, then for now
                // we cancel any operations
                return ;
            }
            jq.extend(this.options, newOpt);

            // Retrieve data from supplied url, if no URL then continue
            // with supplied ruleData
            if (this.options.getUrl.length === 0 && ruleData.length === 0){
                // nothing to draw
                return ;
            }

            if (this.options.getUrl.length === 0 && ruleData.length > 0){
                _drawWithTemplate(this.options, this.ruleData);
            } else {
                jq(this.options.container).html(LOADING_TEXT);

                var defer = _getURL(this.options.jqueryObj,
                                    this.options.getUrl);
                var _comp = this;

                defer.done(function(data){
                    _comp.ruleData = getJSON(data);
                    _drawWithTemplate(_comp.options, _comp.ruleData);
                }).fail(function(err){
                    console.log("ERROR : " + err);
                    jq(_comp.options.container).html(ERROR_TEXT);
                });
            }
        }
    };

    // setting the correct namespace
    fn.component = fn.component || {};
    /* Class */
    fn.component.RuleTotal = function(){
        return _mixedObject;
    };

    /* Singleton */
    fn.component.ruleTotal = _mixedObject;

})(window.jnpr = window.jnpr || {});


String.prototype.hashCode = function(){
    var hash = 0, i, char;
    if (this.length === 0) return hash;
    for (i = 0, l = this.length; i < l; i++) {
        char  = this.charCodeAt(i);
        hash  = ((hash<<5)-hash)+char;
        hash |= 0; // Convert to 32bit integer
    }
    return hash;
};
