/*
 Drawing/showing detail view list of PRs for each Metrics (Agenda page)
 */
(function (fn){
    var LOADING_TEXT = "<span class='icon icon-refresh icon-spin'></span> Loading data ...... please wait";
    var ERROR_TEXT = "<span class='icon icon-warning-sign'></span> Something wrong... make user you specified username!";
    var EMPTY_DATA_TEXT = "<span class='icon icon-time'></span> No data returned......";

    var _postURL = function(_jq, url, params){
        return _jq.ajax ({
            method : "POST",
            url : url,
            data : JSON.stringify(params)
        });
    };

    var _getURL = function(_jq, url, params){
        return _jq.ajax({
            method : "GET",
            url : url,
            dataType : "json"
        });
    };

    var getJSON = function(data){
        if (typeof data != "object") {
            try {
                data = JSON.parse(data);
            } catch (ex){
                data = {};
            }
        }
        return data;
    };

    var _drawTable = function(jsonData, tableEl, _jq){
        var _aaData = [];
        var prs = [];
        var _aoColumns = [];
        var unique_prs = {};
        var total_unique_prs = 0;
        if (jsonData.hasOwnProperty("data")){
            var idxRow = 1;
            for (var idx in jsonData.data){
                var _rowDatum = [];
                mname = jsonData.data[idx]['metricsName'];
                prs.push(jsonData.data[idx]["id"] + '-' + jsonData.data[idx]["identifier"]);
                if (!unique_prs.hasOwnProperty(jsonData.data[idx]["id"])){
                    unique_prs["" + jsonData.data[idx]["id"]] = 0;
                    total_unique_prs ++;
                }
                for (var idxProp in jsonData.data[idx]){
                    var _rowValue = jsonData.data[idx][idxProp];
                    // All results which have id and identifier will be combined automatically
                    // when identifier is picked up, then it will be omitted
                    if (idxProp !== "identifier"){
                        if (idxRow == 1) {
                            _aoColumns.push({
                                "sTitle" : idxProp
                            });
                        }
                        if (idxProp === "id" &&
                            jsonData.data[idx].hasOwnProperty("identifier")){
                            _rowValue += "-" + jsonData.data[idx]["identifier"];
                        }
                        _rowDatum.push(_rowValue);
                    }
                }
                _aaData.push(_rowDatum);
                idxRow ++;
            }
        }

        var tableDrawObj = {
            aoColumns : _aoColumns,
            aaData    : _aaData
        };
        if (tableDrawObj.aoColumns.length === 0 &&
            tableDrawObj.aaData.length === 0){
            tableEl.html(EMPTY_DATA_TEXT);
        } else {
            tableEl.html('<table cellpadding="0" cellspacing="0" border="0" class="table table-condensed"></table>');
            tableEl.find("table").dataTable(tableDrawObj);


            //GNATS link generation
            // TODO:XXX needs to do some improvement
            var gnats_url = "https://gnats.juniper.net/web/default/";
            var def_cols = "synopsis,state,class,problem-level," +
                            "found-during,category,product," +
                         "planned-release,blocker,submitter-id," +
                         "responsible,dev-owner,originator,attributes," +
                         "last-modified,updated-by-responsible,arrival-date," +
                         "fix-eta,committed-release,conf-committed-release,confidential,rli";
            var qname = "queryname=PRs metrics&sortfld1dir=asc&sortfld1=";
            var f_name = Math.random().toString().replace(".", "");

            var gnats_link = '<a href="#" id="prslist" ' +
                'onclick="javascript: document.a' + f_name + '.submit();"'+
                ' class="urlform">Click to view GNATS page</a>' +
                '<form method="POST" name="a' + f_name + '"' +
                ' action="' + gnats_url + 'do-query?" class="viewform">' +
                '<input type="hidden" name="prs" value="' + prs.join(",") + '" />' +
                '<input type="hidden" name="sortfld1" value="" />' +
                '<input type="hidden" name="sortfld1dir" value="asc" />' +
                '<input type="hidden" name="queryname" value="PRs metrics" />' +
                '<input type="hidden" name="columns" value="'+ def_cols + '" />' +
                '</form>';

            if (prs.length > 0 && prs.length != 1) {
               tableEl.append(gnats_link);
            } else {
               var form_link = gnats_url + prs.join(",");
               var final_link = '<a href="' + form_link + '">Click to view GNATS page</a>';
               tableEl.append(final_link);

            }
            // added another container result
            tableEl.append("<div class='panel-result'><em>Unique PRs : " + total_unique_prs + " record(s)</em></div>");
        }
    };

    var _mixedObject = {
        options : {
            container  : "body",
            templateEl : "detail-row-template",
            getUrl     : "/v1/details/",
            jqueryObj  : window.jQuery || {}
        },
        detailData : [],
        draw : function(newOpt){
            var jq = this.options.jqueryObj;
            if (jq == undefined){
                // if jquery is not defined, then for now
                // we cancel any operations
                return ;
            }
            jq.extend(this.options, newOpt);

            // Retrieve data from supplied url, if no URL then continue
            // with supplied ruleData
            if (this.options.getUrl.length === 0 && detailData.length === 0){
                // nothing to draw
                return ;
            }

            if (this.options.getUrl.length === 0 && detailData.length > 0){
                _drawWithTemplate(this.options, this.detailData);
            } else {
                jq(this.options.container).html(LOADING_TEXT);

                var defer;
                if (this.options.hasOwnProperty("postUrl")){
                    defer = _postURL(this.options.jqueryObj,
                                    this.options.postUrl,
                                    this.options.postData);
                } else {
                    defer = _getURL(this.options.jqueryObj,
                                        this.options.getUrl);
                }
                var _comp = this;
                defer.done(function(data){
                    _comp.detailData = getJSON(data);
                    _drawTable(_comp.detailData, jq(_comp.options.container), jq);
                }).fail(function(err){
                    console.log("ERROR : " + err);
                    jq(_comp.options.container).html(ERROR_TEXT);
                });
            }
        }
    };

    // setting the correct namespace
    fn.component = fn.component || {};
    /* Class */
    fn.component.DetailItem = function(){
        return _mixedObject;
    };

    /* Singleton */
    fn.component.detailItem = _mixedObject;

})(window.jnpr = window.jnpr || {});
