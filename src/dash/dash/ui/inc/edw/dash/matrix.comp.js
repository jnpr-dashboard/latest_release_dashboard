(function(fn){
    var LOADING_TEXT = "<span class='icon icon-refresh icon-spin'></span>";

    // matrix to translate  found during from Dashboard to EDW format
    var STATE_MAPS = {
        "yes" : "open,info,feedback",
        "no"  : "open,info",
        "all" : "open,info,analyzed,monitored,suspended,feedback",
        "only": "feedback",
        "monitored" : "monitored",
        "suspended" : "suspended",
        "inreport" : "open, info",
        "backlog"  : "open,info"
    };

    var FOUND_DURING_MAPS = {
        "all" : "all",
        "fdvals" : "Feature/Functional test,Production,Product delivery test,Regression test,Qualification,Customer-specific test,Customer escalation support,Solution test,Beta Test,Daily Sanity,do-no-harm Test"
    };

    // find all elements which needs to be replaced along with their info
    var findAllElement = function(jq, results){
        var setResult = {
            users : {},
            rules : {},
            releases : {}
        };
        $("td[data-matrix-edw='true']").each(function(){
            // some information needs to be store
            // element : <jquery object correspond to it>
            // userid  : <user name>
            // rulename: <rule name>
            // release : <releaseid>
            // Make a map [username][rulename][release] : { "element": <jquery object>}
            var userId = $(this).attr("data-user");
            var ruleName = $(this).attr("data-rule");
            var releaseid = $(this).attr("data-release");

            if (!results.hasOwnProperty(userId)){
                results[userId] = {};
            }
            if (!results[userId].hasOwnProperty(ruleName)){
                results[userId][ruleName] = {};
            }
            if (!results[userId][ruleName].hasOwnProperty(releaseid)){
                results[userId][ruleName][releaseid] = {};
            }

            results[userId][ruleName][releaseid] = {
                element : $(this)
            };
            $(this).find(".cntX").html(LOADING_TEXT);
            $(this).attr("data-processed", "false");

            // put them into set results
            setResult["users"][userId] = true;
            setResult["rules"][ruleName] = true;
            setResult["releases"][releaseid] = true;
        });
        return setResult;
    };

    var drawAllElement = function(data, collections, paramSet){
        // expecting data with array results
        // {
            // cnt_level : "X" or "Y" or "T",
            // dashboard_owner : <username>,
            // metrics_count : {
                // <each metrics_id> : <count>
            // },
            // release_value : <releaseid>
        // }

        for (var idx in data){
            // get username and release
            var username = data[idx]["dashboard_owner"] || "";
            var release  = data[idx]["release_value"] || "";
            var cntLevel = data[idx]["cnt_level"] || "X";
            // iterate on each metrics_count available
            if (data[idx].hasOwnProperty("metrics_count") &&
                typeof data[idx].metrics_count === "object" &&
                data[idx].metrics_count !== undefined){
                for (var metricsName in data[idx].metrics_count){
                    // access the collections map
                    try {
                        var datum = collections[username][metricsName][release];
                        var el = datum.element;
                        var count = data[idx].metrics_count[metricsName];
                        var oriURL;
                        if (cntLevel === "X"){
                            oriURL = $(el).find(".cntX").attr('href');
                            $(el).find(".cntX")
                                .attr("href", oriURL + "&username=" + username +
                                            "&release=" + release +
                                            "&metric_name=" + metricsName +
                                            "&found_during=" + paramSet.foundDuring +
                                            "&states=" + paramSet.states)
                                .html(count)
                                ;
                            $(el).attr("data-processed", "true");
                        } else if (cntLevel === "Y"){
                            oriURL = $(el).find(".cntY").attr('href');
                            $(el).find(".cntY")
                                .attr("href", oriURL + "&username=" + username +
                                            "&release=" + release +
                                            "&metric_name=" + metricsName +
                                            "&found_during=" + paramSet.foundDuring +
                                            "&states=" + paramSet.states)
                                .html("(" + count + ")")
                                ;
                            $(el).attr("data-processed", "true");
                        } else if (cntLevel === "T"){
                            oriURL = $(el).find(".cntX").attr('href');
                            // for total, we must pass all releases
                            var allReleases = release;
                            if (paramSet.hasOwnProperty("releases_array")){
                                allReleases = paramSet.releases_array;
                            }
                            $(el).find(".cntX")
                                .attr("href", oriURL + "&username=" + username +
                                            "&release=" + allReleases +
                                            "&metric_name=" + metricsName +
                                            "&found_during=" + paramSet.foundDuring +
                                            "&states=" + paramSet.states)
                                .html("" + count)
                                ;
                            $(el).attr("data-processed", "true");
                        }
                    } catch (ex){
                        // console.err("mapping not found!");
                        // console.err(ex);
                        console.log("username : " + username + " , " + "release : " + release + " , " + "metrics " + metricsName);
                    }
                }
            }
        }

        // other element which is not
        $("td[data-matrix-edw='true'][data-processed='false']").addClass("no-data").html("no data");

    };

    var _mixedObject = {
        options : {
            jqueryObj  : window.jQuery || {}
        },
        collections : {},
        draw : function(){
            var jq = this.options.jqueryObj;
            var paramSet = findAllElement(this.options.jqueryObj, this.collections);
            if (!jq.isEmptyObject(this.collections)){
                // added another parameter from options
                $.extend(paramSet, {
                   "states"   : STATE_MAPS[this.options.states] || "",
                   "foundDuring" : FOUND_DURING_MAPS[this.options.foundDuring] || ""
                });
                this.retrieveDataFromEDW(paramSet);
            }
        },
        retrieveDataFromEDW : function(paramSet){
            // prepare clean parameter to send to EDW
            var metricsLists = "";
            var usernames = "";
            var releases = "";

            if (paramSet.hasOwnProperty("rules") && paramSet.rules !== undefined &&
                    typeof paramSet.rules === "object"){
                var paramMetrics = [];
                for (var idx in paramSet.rules){
                    paramMetrics.push(idx);
                }
                if (paramMetrics.length > 0){
                    metricsLists = paramMetrics.join(",");
                }
            }

            if (paramSet.hasOwnProperty("users") && paramSet.users !== undefined &&
                    typeof paramSet.users === "object"){
                var paramUsers = [];
                for (var _idx in paramSet.users){
                    paramUsers.push(_idx);
                }
                if (paramUsers.length > 0){
                    usernames = paramUsers.join(",");
                }
            }

            if (paramSet.hasOwnProperty("releases") && paramSet.releases !== undefined &&
                    typeof paramSet.releases === "object"){
                var paramReleases = [];
                for (var _jdx in paramSet.releases){
                    paramReleases.push(_jdx);
                }
                if (paramReleases.length > 0){
                    releases = paramReleases.join(",");
                }
                // inject this into paramSet so it can be reused
                paramSet.releases_array = releases;
            }

            // send information to EDW
            var that = this;
            var jq = this.options.jqueryObj;

            var defer = jq.ajax({
                type : "POST",
                url : this.options.url,
                data : JSON.stringify({
                    "username" : usernames,
                    "metrics" : metricsLists,
                    "releases" : releases,
                    "states"   : paramSet.states,
                    "foundDuring" : paramSet.foundDuring
                })
            });

            defer.done(function(data){
                // iterate the results and draw them into DOM
                drawAllElement(data, that.collections, paramSet);
            });

        }
    };

    fn.component = fn.component || {};

    fn.component.MatrixItem = fn.component.MatrixItem || function(opt){
        this.options = $.extend({}, this.options, opt);
        this.draw();
    };
    fn.component.MatrixItem.prototype = _mixedObject;
    fn.component.MatrixItem.prototype.constructor = fn.component.MatrixItem;

    fn.component.matrixItem = _mixedObject;

})(window.jnpr = window.jnpr || {});

