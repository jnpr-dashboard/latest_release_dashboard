/*
 Drawing Rule / Metrics in Agenda page
 */
(function (fn){
    var CALCULATING_TEXT = "<span class='icon icon-refresh icon-spin'></span> Calculating ...... please wait";
    var LOADING_TEXT = "<span class='icon icon-refresh icon-spin'></span> Loading data ...... please wait";
    var ERROR_TEXT = "<span class='icon icon-warning-sign'></span> Something wrong... make user you specified username!";
    var EMPTY_DATA_TEXT = "<span class='icon icon-time'></span> No data returned......";

    var AGENDA_TOTAL_SCORE_ELEMENT = "#agenda-title-totalscore";

    var _getURL = function(_jq, url, params){
        return _jq.ajax({
            method : "GET",
            url : url + (params || ""),
            dataType : "json"
        });
    };

    var getJSON = function(data){
        if (typeof data != "object") {
            try {
                data = JSON.parse(data);
            } catch (ex){
                data = {};
            }
        }
        return data;
    };

    var _drawWithTemplate = function(options, data){
        var _jq = options.jqueryObj;
        var _jqContent = options.jqueryObj;

        if (options.jqueryContext !== undefined){
            _jqContent = options.jqueryContext;
        }

        if (_jq === undefined){ return ; }
        if (!data.hasOwnProperty("data")){ return ;}

        var newEl = [];
        var templateStr = _jq(options.templateEl).html();
        var headerTemplateStr = _jq(options.templateHeaderEl).html();

        var pivot = data.data;
        if (data.hasOwnProperty("totalScore")) {
            // Sum up the overall score with edw score.
            total_score = parseInt(total_score) + data.totalScore;
        }

        if (data.hasOwnProperty("jsonGroupsOutput") &&
            typeof data.jsonGroupsOutput === "object"){
            pivot = data.jsonGroupsOutput;
        }
        // Sorting wise category
        var sorted_ctg = Object.keys(pivot).sort();

        for (var i=0; i < sorted_ctg.length; i++) {
            for (var idx in pivot) {
                if (sorted_ctg[i] == idx) {
                    // iterate on each category
                    newEl.push(_.template(headerTemplateStr, {
                        "metricsCategory" : idx
                    }));

                    for (var metricsName in pivot[idx]){
                        // list all releases
                        var dataByMetrics = pivot[idx][metricsName];
                        for (var metricsRelease in dataByMetrics){
                            // list by IB / Unk
                            var dataByRelease = dataByMetrics[metricsRelease];
                            if (metricsRelease == "OTHER"){
                                continue;
                            }
                            var releaseCount = 0;
                            for (var metricsIb in dataByRelease){
                                var agendaData = dataByRelease[metricsIb];
                                agendaData = JSON.parse(agendaData);
                                var generateUniqueId = agendaData.metricsName +
                                           agendaData.dashboardRelease + agendaData.ibRelease;
                                agendaData["hash"] = generateUniqueId.hashCode();
                                dataByRelease[metricsIb] = agendaData;
                                releaseCount++;
                            }
                            newEl.push(_.template(templateStr, {
                                "release" : metricsRelease,
                                "data" : dataByRelease,
                                "count" : releaseCount
                                }
                            ));
                        }
                    }

                } // end of category matching condition
            } //end of data object loop
        } // end of sort category loop

        _jq(options.container).html(newEl.join(""));

        // draw the total score
        _jq(AGENDA_TOTAL_SCORE_ELEMENT).html(total_score);

        // iterate on the sources
        var ordered_report = [];
        // --- TOP SCORES CALCULATION
        // Get the new direct reports number from EDW and combine them
        if (data.hasOwnProperty("topScores") &&
            typeof data.topScores === "object"){
            // iterate on the sources
            ordered_report = [];
            for (var report in direct_reports){
                if (data.topScores.hasOwnProperty(report)){
                    direct_reports[report]['score'] += data.topScores[report];
                }
                ordered_report.push([report, direct_reports[report]['score']]);
            }

            // order in descending
            ordered_report.sort(function(a, b){
                var valA = a[1];
                var valB = b[1];

                return valA < valB ? 1 : ( valA > valB ? -1 : 0);
            });
        }

        // Draw the new top reports
        _jq("#high-scorers").html("<strong>Top 5 Reports</strong> &nbsp;");
        var counter = 5;
        for (var ordered_idx in ordered_report){
            // name link for the top 5 scorers
            var link = '<a href="' + 
                        direct_reports[ordered_report[ordered_idx][0]]['link'] +
                        '" id="ts_' + ordered_report[ordered_idx][0] + 
                        '_tip">' + ordered_report[ordered_idx][0] + '</a>';
            
            var delimtr = ' &nbsp;| &nbsp;';
            if (counter == 1) {delimtr = '';}
            _jq("#high-scorers").append(link + 
                                    '&nbsp;::&nbsp;' + ordered_report[ordered_idx][1] + 
                                    delimtr);
            counter--;
            if (counter < 1) {break;}
        }
        // ---- END OF TOP SCORES CALCULATION

        _jq(".edw_data .group_rule").each(function(){
            _jqContent(this).treeview();
        });

        _jq(options.container).find(".ruletooltip").each(function() {
            var tip_obj = _jq("#tt_"+_jq(this).attr("id"));
            // qtip only registered in $
            // TODO.. fix this register all jquery plugin
            _jqContent(this).qtip(_jq.extend(true, {}, tooltip_opts, {
                  position: {
                  corner: { target: 'center', tooltip: 'topRight' }
                },
                width: { min: 500, max: 700 },
                hide: { fixed: tip_obj.attr('stay_on') == 'true' },
                content: { title: { text: tip_obj.attr("title") }, text: tip_obj.html() }
            }));
          });

        // init_tooltip();
    };

    var _mixedObject = {
        options : {
            container  : "body",
            templateEl : "agenda-rule-row-template",
            templateHeaderEl : "agenda-rule-header-template",
            getUrl     : "/v1/agenda/",
            jqueryObj  : window.jQuery || {},
            jqueryContext : window.jQuery || {} // legacy or existing jquery in the page
        },
        ruleData : [],
        draw : function(newOpt){
            var jq = this.options.jqueryObj;
            if (jq === undefined){
                // if jquery is not defined, then for now
                // we cancel any operations
                return ;
            }
            jq.extend(this.options, newOpt);

            // Retrieve data from supplied url, if no URL then continue
            // with supplied ruleData
            if (this.options.getUrl.length === 0 && ruleData.length === 0){
                // nothing to draw
                return ;
            }

            if (this.options.getUrl.length === 0 && ruleData.length > 0){
                _drawWithTemplate(this.options, this.ruleData);
            } else {

                jq(this.options.container).html(LOADING_TEXT);
                jq(AGENDA_TOTAL_SCORE_ELEMENT).html(CALCULATING_TEXT);

                var direct_reports_name = "";
                if (direct_reports !== undefined){
                    direct_reports_name = "&directreports=";
                    for (var report in direct_reports){
                        direct_reports_name += report +",";
                    }
                }

                var defer = _getURL(this.options.jqueryObj,
                                    this.options.getUrl,
                                    direct_reports_name);
                var _comp = this;

                defer.done(function(data){
                    _comp.ruleData = getJSON(data);
                    if (_comp.ruleData.hasOwnProperty("jsonGroupsOutput")){
                        _comp.ruleData.jsonGroupsOutput = getJSON(data.jsonGroupsOutput);
                    }
                    _drawWithTemplate(_comp.options, _comp.ruleData);
                }).fail(function(err){
                    console.log("ERROR : " + err);
                    jq(_comp.options.container).html(ERROR_TEXT);
                    jq(AGENDA_TOTAL_SCORE_ELEMENT).html(total_score);
                });
            }
        }
    };

    // setting the correct namespace
    fn.component = fn.component || {};
    /* Class */
    fn.component.RuleItem = function(){
        return _mixedObject;
    };

    /* Singleton */
    fn.component.ruleItem = _mixedObject;

})(window.jnpr = window.jnpr || {});


String.prototype.hashCode = function(){
    var hash = 0, i, char;
    if (this.length === 0) return hash;
    for (i = 0, l = this.length; i < l; i++) {
        char  = this.charCodeAt(i);
        hash  = ((hash<<5)-hash)+char;
        hash |= 0; // Convert to 32bit integer
    }
    return hash;
};

function foo() {
    console.log("FOO");
}
