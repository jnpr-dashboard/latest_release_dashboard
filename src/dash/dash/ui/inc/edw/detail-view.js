$jq(document).ready(function(){
    var rel = '';
    if (release != 'None') {
        rel = '/' + release;
    }
    if (detailType === "agenda") {
        jnpr.component.detailItem.draw({
            container  : "#detail-container",
            templateEl : "#detail-row-template",
            getUrl     : EDW_ENDPOINT_URL + "/rest/v1/details/" + userId +
                                          "/" + metricName + rel,
            jqueryObj : $jq
        });
    } else {
        var detailMatrixURL = EDW_ENDPOINT_URL + "/rest/v1/matrixpage/alldetail";
        if (countType == "Y"){
            detailMatrixURL = EDW_ENDPOINT_URL + "/rest/v1/matrixpage/ibtotdetail";
        }
        jnpr.component.detailItem.draw({
            container  : "#detail-container",
            templateEl : "#detail-row-template",
            postUrl     : detailMatrixURL,
            postData : {
                "username" : userId,
                "metrics"  : metricName,
                "releases"  : release,
                "states"    : states,
                "foundDuring" : foundDuring
            },
            jqueryObj : $jq
        });
    }
});
