var starElement; 	 	
 var mouseentry=0; //to keep track of how many times the mouse entered the vote button 	 
 var submitvote=0; // to keep track whether the user clicked the submit button 	 
 var flag=false; 	 
 var rate; 	 
 var rating=0;

 $(document).ready(function() {
  // to make the submit button enabled if the user selects any radio button 	 
         $("input[name='top']").change(function(event) 	 
         { 	 
  	 
          $('#vote_submit').removeAttr('disabled'); 	 
          $('#votebutton').hide(); 	 
         }); 	 
  	 
  	 
         //fetch  results when the page loads 	 
         $.ajax({ 	 
             type: "POST", 	 
             url: dash_base_url+"vote_result/", 	 
                 dataType : 'json', 	 
             data: { 	 
                   }, 	 
             success: function(response) { 	 
                                                                         fillRateDiv(response) 
                                                                 } 	 
                         }); 	 
  	 
         $('#votebutton').attr("disabled", 'disabled'); 	 
  	 
  	 
         //submitting the vote 	 
         $('#vote_submit').click(function() 	 
                 { 	 
                 if ($('#yourrating').text()>0) 	 
         { 	 
                         $.ajax({ 	 
  	 
                                     type: "POST", 	 
                                     url: dash_base_url+"user_vote/", 	 
                                         dataType : 'json', 	 
                                     data: { userrating: $('#yourrating').text(), 	 
                                                 toparea:$('input:radio[name=top]:checked').val() 	 
                                           }, 	 
                                     success: function(response) { 	 
                                                                        $('#reason').text("Rating Of "+$('#yourrating').text()+" Is Submitted. Thank You For  Your Feedback"); 	 
                                                                        $('#yourrating').text(0); 	 
                                                                                                    fillRateDiv(response)	 
  	 
  	 
  	 
                                                                                            } 	 
                         }) 	 
         } 	 
                 else 	 
                         { 	 
  	 
  	 
                         var oldrating=$('#reason').text(); 	 
                         $('#reason').text(oldrating.replace("Optional Feedback Below","")).append("Thank You For Your Feedback"); 	 
  	 
                         $.ajax({ 	 
  	 
                             type: "POST", 	 
                             url: dash_base_url+"user_vote/", 	 
                                 dataType : 'json', 	 
                             data: { userrating:"", 	 
                                         toparea:$('input:radio[name=top]:checked').val() 	 
                                   }, 	 
                             success: function(response) { 	 
                                                                         $("#opportunity").text("Top Improvement Opportunity: "+response.topopportunity); 	 
  	 
  	 
                                                                                    } 	 
                 }) 	 
  	 
                         } 	 
  	 
                 submitvote++; 	 
                 //reseting everthing again 	 
                         $("input:radio[name=top]").removeAttr("checked"); 	 
                         $("input:radio[name=top]").attr("disabled", 'disabled'); 	 
                         $('#vote_submit').attr("disabled", 'disabled'); 	 
                         $('div').removeClass('selected highlighted'); 	 
                         $('#hoverstars').find('div').removeClass('jquery-ratings-full'); 	 
  	 
                         $('#hoverstars').ratings(0,mouseentry,0).bind('ratingchanged', function(event, data) { 	 
                             $('#yourrating').text(data.rating); 	 
  	 
                           }); 	 
  	 
                 });     //end of submit click function 	 
  	 
  	 
  	 
         $('#votebutton').click(function() 	 
                         {       var userarea=$('input:radio[name=top]:checked').val(); 	 
                         if(typeof userarea === "undefined") 	 
                                 { 	 
                                 userarea=null; 	 
                                 } 	 
  	 
                 $.ajax({ 	 
                                             type: "POST", 	 
                                             url: dash_base_url+"user_vote/", 	 
                                                 dataType : 'json', 	 
                                             data: { userrating: $('#yourrating').text(), 	 
                                                         toparea:userarea 	 
                                                   }, 	 
                                             success: function(response) { 	 
                                                                              fillRateDiv(response)                                  
                                                                                                    } 	 
                                 }) 	 
                 submitvote++; 	 
                         $('#hoverstars').find('div').removeClass('jquery-ratings-full'); 	 
                         $('#hoverstars').ratings(0,mouseentry,0).bind('ratingchanged', function(event, data) { 	 
                             $('#yourrating').text(data.rating); 	 
  	 
                           }); 	 
                         $('#votebutton').attr("disabled", 'disabled'); 	 
                         $('#reason').text("    Rating Of "+$('#yourrating').text()+" Is Submitted. Optional Feedback Below"); 	 
                         $('#yourrating').text(0); 	 
                         }); 	 
  	 
  	 
  	 
 mouseentry++; 	 
  	 
  	 
 if(flag==false) 	 
 { 	 
 $('#hoverstars').ratings(5,mouseentry).bind('ratingchanged', function(event, data) { 	 
 $('#yourrating').text(data.rating); 	 
 flag=true; 	 
 }); 	 
 } 	 
  	 
  	 
  	 
 bargraph(); 	 
 if(mouseentry==1) 	 
 { 	 
         $("input:radio[name=top]").removeAttr("checked"); 	 
         $("input:radio[name=top]").attr("disabled", true); 	 
         $('#vote_submit').attr("disabled", true); 	 
 } 	 
  	 
 
});
 	 
  	 
 // sorting function 	 
 function sortAsc(arr) 	 
         { 	 
                 return arr.sort(function(a, b) 	 
                                                         { 	 
                                                                 return a - b; 	 
                                                         }); 	 
         } 	 
 // End of Sort Function 	 
  	 
  	 
  	 
 // function to display the stars based on the rating 	 
                         $.fn.stars = function() { 	 
  	 
                                                                          return $(this).each(function() { 	 
                                                                 // Get the value 	 
  	 
                                                                 //var val = parseFloat($(this).html()); 	 
                                                                 var val = parseFloat(rate); 	 
                                                                 // Make sure that the value is in 0 - 5 range, multiply to get width 	 
                                                                 var size = Math.max(0, (Math.min(5, val))) * 16; 	 
                                                                 // Create stars holder 	 
                                                                 var $span = $('<span />').width(size); 	 
                                                                 // Replace the numerical value with stars 	 
                                                                 $(this).html($span); 	 
                                                             }); 	 
                                                         } 	 
         // function for drawing the graph 	 
  	 
	 
function fillRateDiv(response)
{
    
    rate=response.average_rating; 	 
    $('span.stars').stars(rate); 	 
    $("#monthyear").text("Result For "+response.month+"-"+response.year); 	 
    $("#avgrating").text(response.average_rating); 	 
    $(".hiddenrating").text(response.average_rating); 	 
    $("#totalvotes").text(response.noofvotes); 	 
    $("#opportunity").show(); 	 
    $("#opportunity").text("Top Improvement Opportunity: "+response.topopportunity); 	 
    $(".green").text(response.starvalues.FiveStar); 	 
    $(".orange").text(response.starvalues.FourStar); 	 
    $(".blue").text(response.starvalues.ThreeStar); 	 
    $(".pink").text(response.starvalues.TwoStar); 	 
    $(".reddeep").text(response.starvalues.OneStar); 	 
    bargraph(); 	 
}

 function bargraph() 	 
         { 	 
  	 
                 var myArray=[]; 	 
                 var valarray=[]; 	 
                 var pc; 	 
                 var len; 	 
                 $('.bargraph > div').each(function() 	 
                         { 	 
                                  pc= $(this).text(); 	 
                                 len= parseInt(pc, 10); 	 
                                 myArray.push(len); 	 
                         }); // get all the values in the list 	 
  	 
                  myArray=sortAsc(myArray) 	 
                 var highest=myArray[myArray.length-1]; // find the highest value 	 
  	 
  	 
                 $('.bargraph > div').each(function() 	 
                         { 	 
  	 
                                  pc = $(this).html(); 	 
  	 
                                 if(!isNaN(pc) && pc!="") 	 
                                         { 	 
                                         var outer=($(this).attr('class')); 	 
                                         var outerthis=this; 	 
  	 
                                         len=1; 	 
                                         if (pc>0) 	 
                                         { 	 
                                                 len=len+(((parseInt(pc, 10)*100)/highest)); 	 
                                         } 	 
  	 
  	 
                                         $(this).width(len+'%'); 	 
  	 
  	 
                                         $('.bargraphval > div').each(function(){ 	 
  	 
                                                  if (($(this).attr('class')).search(outer)>-1) 	 
                                                          { 	 
                                                          $(this).text(pc); 	 
  	 
                                                          var left=((len*130)/100)+37; 	 
                                                                 $(this).css('margin-left',left); 	 
                                                          } 	 
                                         }); 	 
  	 
  	 
  	 
                                         } 	 
                                 $(this).text(""); 	 
  	 
                         }); 	 
  	 
  	 
  	 
         } 	 
  	 
  	 
         //function for creating the stars for rating 	 
 jQuery.fn.ratings = function(stars, mouseentry,initialRating) { 	 
  	 
                                                         //Save  the jQuery object for later use. 	 
                                                         var elements = this; 	 
  	 
                                                         //Go through each object in the selector and create a ratings control. 	 
                                                         return this.each(function() { 	 
  	 
                                                         //Make sure intialRating is set. 	 
                                                         if(!initialRating) 	 
                                                         initialRating = 0; 	 
  	 
                                                         //Save the current element for later use. 	 
                                                         var containerElement = this; 	 
  	 
                                                         //grab the jQuery object for the current container div 	 
                                                         var container = jQuery(this); 	 
  	 
                                                         //Create an array of stars so they can be referenced again. 	 
                                                         var starsCollection = Array(); 	 
  	 
                                                         //Save the initial rating. 	 
                                                         containerElement.rating = initialRating; 	 
  	 
                                                         //Set the container div's overflow to auto.  This ensure it will grow to 	 
                                                         //hold all of its children. 	 
                                                         container.css('overflow', 'auto'); 	 
                                                         var starElement; 	 
                                                         //create each star 	 
                                                         for(var starIdx = 0; starIdx < stars; starIdx++) { 	 
                                                         if(mouseentry==1) 	 
                                                         //Create a div to hold the star. 	 
                                                         { 	 
                                                         starElement = document.createElement('div'); 	 
                                                         } 	 
  	 
                                                         //Get a jQuery object for this star. 	 
                                                         var star = jQuery(starElement); 	 
  	 
                                                         //Store the rating that represents this star. 	 
                                                         starElement.rating = starIdx + 1; 	 
  	 
                                                         //Add the style. 	 
                                                         star.addClass('jquery-ratings-star'); 	 
  	 
                                                         //Add the full css class if the star is beneath the initial rating. 	 
                                                         if(starIdx < initialRating) { 	 
                                                         star.addClass('jquery-ratings-full'); 	 
                                                         } 	 
  	 
                                                         //add the star to the container 	 
                                                         container.append(star); 	 
                                                         starsCollection.push(star); 	 
  	 
                                                         //hook up the click event 	 
                                                         star.click(function() { 	 
                                                         //When clicked, fire the 'ratingchanged' event handler.  Pass the rating through as the data argument. 	 
                                                         elements.triggerHandler("ratingchanged", {rating: this.rating}); 	 
                                                         containerElement.rating = this.rating; 	 
                                                         rating=this.rating; 	 
  	 
                                                         if(rating<=2) 	 
                                                         {       $('#votebutton').hide(); 	 
                                                         $("input:radio[name=top]").attr("disabled", false); 	 
  	 
                                                         $('#reason').text("Please Tell Us The Reason For Low Rating"); 	 
  	 
                                                         $('#area').text(""); 	 
                                                         $('#allreasons').show(); 	 
                                                         $("input:radio[name=top]").removeAttr("checked"); 	 
                                                      $('#vote_submit').attr('disabled', 'disabled'); 	 
                                                         } 	 
                                                         else 	 
                                                         {//     $('#allreasons').hide(); 	 
  	 
                                                         $('#votebutton').show(); 	 
                                                         $('#votebutton').removeAttr('disabled'); 	 
                                                         $("input:radio[name=top]").attr("disabled", false); 	 
  	 
                                                         $('#reason').text("Optional Feedback Below"); 	 
                                                         $('#area').text(""); 	 
                                                       $("input:radio[name=top]").removeAttr("checked"); 	 
                                                         $('#vote_submit').attr('disabled', 'disabled'); 	 
  	 
                                                         } 	 
  	 
  	 
 }); 	 
  	 
 star.mouseenter(function() { 	 
                                           $('#yourrating').text(this.rating); 	 
                                         //Highlight selected stars. 	 
                                         for(var index = 0; index < this.rating; index++) { 	 
                                           starsCollection[index].addClass('jquery-ratings-full'); 	 
  	 
                                         } 	 
                                         //Unhighlight unselected stars. 	 
  	 
                                         for(var index = this.rating; index < stars; index++) { 	 
                                           starsCollection[index].removeClass('jquery-ratings-full'); 	 
                                         } 	 
                                 }); 	 
  	 
 container.mouseleave(function() { 	 
                                                   $('#yourrating').text(containerElement.rating); 	 
                                                 //Highlight selected stars. 	 
                                                 for(var index = 0; index < containerElement.rating; index++) { 	 
                                                   starsCollection[index].addClass('jquery-ratings-full'); 	 
                                                 } 	 
                                                 //Unhighlight unselected stars. 	 
                                                 for(var index = containerElement.rating; index < stars ; index++) { 	 
                                                   starsCollection[index].removeClass('jquery-ratings-full'); 	 
                                                 } 	 
                                         }); 	 
  	 
 if(starIdx==4) 	 
 { 	 
 mouseentry=2; 	 
 } 	 
 } 	 
 }); 	 
 };// end of jquery.fn.function 	 
 