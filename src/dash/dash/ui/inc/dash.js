var tooltip_opts = {
 style: {
  name: 'dash'
 },
 position: {
  adjust: { screen: true}
 },
 show: { delay: 400 },
 hide: { delay: 200 }
};
var COOKIE_OPTIONS = {
 path: '/',
 expires: 999
};
$(document).ready(function() {
  // Tabs for Pr-Fix
  $("#prFixTab, #backlogPRTab, #rlireportTab, #hdViewTab").tabs({
	  show:function(event, ui){
	  		$('.visualize').trigger('visualizeRefresh');
  		}
  	});
 
  // Grouping accordion
  $(".group_rule").each(function(){
		$(this).treeview();
	});
  
  $('.tablesorter tbody tr').hover(
		  function(){
			  if ($(this).attr("name") != "nohover"){
				  $(this).css('background-color','#66EFEF');
				  $(this).css('border-width','10px');
 			  }
		  },
		  function(){
			  if ($(this).attr("name") != "nohover"){
				  if ($(this).attr("class") == "odd"){
					  $(this).css('background-color','#e6EEEE');
					  $(this).css('color','#000000');
				  } else {
					  $(this).css('background-color','#FFFFFF');
					  $(this).css('color','#000000');
				  }
			  }
		  }
  );

  // Define style for tooltips
  $.fn.qtip.styles.dash = { // Last part is the name of the style
      width: { min: 300, max: 500 },
      background: 'white',
      color: 'black',
      border: {
         width: 1,
         radius: 1,
         color: '#A5A5A5'
      },
      title: {
        'text-align': 'center',
        color: 'black',
        background: '#FFF2AC'
      },
      tip: {
        corner: 'leftMiddle',
        color: '#A5A5A5'
      }
  };
  $(".hasatooltip").each(function() {
    var tip_obj = $("#tt_"+$(this).attr("id"));
    $(this).qtip($.extend(true, {}, tooltip_opts, {
      position: {
       corner: { target: 'rightMiddle', tooltip: 'leftMiddle' }
      },
      hide: { fixed: tip_obj.attr('stay_on') == 'true' },
      content: { title: { text: tip_obj.attr("title") }, text: tip_obj.html() }
    }));
  });
  $(".ruletooltip").each(function() {
    var tip_obj = $("#tt_"+$(this).attr("id"));
    $(this).qtip($.extend(true, {}, tooltip_opts, {
      position: {
      corner: { target: 'center', tooltip: 'topRight' }
    },
    width: { min: 500, max: 700 },
    hide: { fixed: tip_obj.attr('stay_on') == 'true' },
    content: { title: { text: tip_obj.attr("title") }, text: tip_obj.html() }
    }));
  });
  $("#dwim").qtip($.extend(true, {}, tooltip_opts, {
    style: { tip: { corner: 'topMiddle' } },
    position: { corner: { target: 'bottomMiddle', tooltip: 'topMiddle' } },
    show: { delay: 200 },
    content: { title: { text: 'The "Do What I Mean" Box' },
               text: $("#tt_dwim").html() }
  }));

  $(".sf-menu").superfish({
    pathClass: 'current',
    delay: 500,
    speed: 'fast',
    disableHI: false,
    dropShadows: true,
    autoArrows: false
  });

  $('#dwim').autocomplete(dash_base_url+"/ajax/1-user", {
    matchContains: true,
    minChars: 2,
    delay: 200,
    selectFirst: false,
    max: 1000
  });
  $('#dwim-form').submit(function(e){
    $("#dwim").qtip('hide');
    show_loading();
    var dwim = $.trim($('#dwim').val());
    if (dwim.match(/\s|^prs$|^(pr|rli)\d+/)) {
      // Anything with a space, or "prs", or a PR or RLI number, pass to server
      return true;
    } else if (dwim.match(/^[\w\-]+$/)) {
      // A username, take them directly to that user's agenda
      location.href = dash_base_url + '/agenda/' + dwim;
      return false;
    } else {
      // pass to server
      return true;
    }
  });

  // The slide down/up magic for the "upcoming events" box
  $('#upcoming').hoverIntent(
    function() {
      $("#more-upcoming").slideDown(400);
    },
    function() {
      $("#more-upcoming").slideUp(400);
    }
  );

  // MotD expand/contract
  var cookieMotd = $.cookie('matrix-motd');
  if (! cookieMotd) {
    // User has no MotD cookie yet, set an empty one.
    cookieMotd = ",";
    $.cookie('matrix-motd', cookieMotd, COOKIE_OPTIONS);
  }
  $(".motd-more").each(function() {
    var motd = $(this).parent().parent();
    motd.moreLink = $(this);
    var motdId = motd.attr("id").split("-")[1];
    motd.motdId = motdId;
    if (cookieMotd.indexOf(","+motdId+",") > -1) {
      // User has seen this motd body before, don't display it again.
      motd.dash_toggle_on = true;
    } else {
      // Display the body, and note in the cookie that it's been seen
      motd.dash_toggle_on = false;
      $.cookie('matrix-motd', cookieMotd + motdId + ",", COOKIE_OPTIONS);
    }
    toggleMotd(motd)
    motd.click(function(evt) {
      toggleMotd(motd);
    });
  });

  function toggleMotd(motd) {
    if (motd.dash_toggle_on) {
      motd.dash_toggle_on = false;
      $("#motd-body-"+motd.motdId).slideUp('fast');
      motd.moreLink.html("more &raquo;");
    } else {
      motd.dash_toggle_on = true;
      $("#motd-body-"+motd.motdId).slideDown('fast');
      motd.moreLink.html("less &laquo;");
    }
  }

  // Context help.
  var ctx_count = 0;
  $(".context-help").each(function() {ctx_count++});
  if (ctx_count == 0) {
    // No help items, hide the clicker
    $("#context-help-thing").hide();
  } else {
    // Set up plumbing for context help
    var ctxThing = $("#context-help-thing");
    ctxThing.qtip($.extend(true, {}, tooltip_opts, {
      show: { delay: 0 },
      style: { width: { min: 150, max: 150 },
               border: { width: 8, color: '#FFF2AC' } },
      content: { text: "Click to show help" }
    }));
    var ctx_shown = false;
    var hide_ctx = function(evt) {
      $(".context-help").fadeOut();
      $("body").unbind('click', hide_ctx);
      ctx_shown = false;
      evt.preventDefault();
    };
    ctxThing.click(function(evt) {
      ctxThing.qtip('hide');
      if (ctx_shown) {
        hide_ctx(evt);
      } else {
        $(".context-help").fadeIn();
        ctx_shown = true;
        $("body").click(hide_ctx);
        evt.preventDefault();
      }
      return false;
    });
  }

  $('#adminInfo').hide();
  $('a#adminToggle').click(function() {
    $('#adminInfo').slideToggle(400);
    return false;
  });
/*FIXME  delete this before go-live
 * $.Autocompleter.defaults = {
  inputClass: "ac_input",
  resultsClass: "ac_results",
  loadingClass: "ac_loading",
  minChars: 1,
  delay: 400,
  matchCase: false,
  matchSubset: true,
  matchContains: false,
  cacheLength: 10,
  max: 100,
  mustMatch: false,
  extraParams: {},
  selectFirst: true,
  formatItem: function(row) { return row[0]; },
  formatMatch: null,
  autoFill: false,
  width: 0,
  multiple: false,
  multipleSeparator: ", ",
  highlight: function(value, term) {
    return value.replace(new RegExp("(?![^&;]+;)(?!<[^<>]*)(" + term.replace(/([\^\$\(\)\[\]\{\}\*\.\+\?\|\\])/gi, "\\$1") + ")(?![^<>]*>)(?![^&;]+;)", "gi"), "<strong>$1</strong>");
  },
    scroll: true,
    scrollHeight: 180
};
 */
  // Make overflow URLs submit their hidden forms
  $(".urlform").click(function(event) {
    $("#f_"+$(this).attr("id")).submit();
    event.preventDefault();
  });

  $(".quick-tour").attr('target','_blank');

  if (page == 'matrix' || page == 'release-matrix') {
    var tableCopy;
    $("a.table-sel").click(function() {
      show_loading();
      var target =  $("#"+$(this).attr("target"));
      //check if a copy has already been made
      if (tableCopy){
      	//just need to show it
      	tableCopy.show();
      } else {
      	// clone the table
      	tableCopy = target.clone();
      	tableCopy.attr("id","table-copy");
      	// insert it on the page
      	target.after(tableCopy);

      	//make all the cells plain text
      	$("td",tableCopy).each(function(i) {
      		this.innerHTML = this.textContent;
      	});
      	$("th",tableCopy).each(function(i) {
      		this.innerHTML = this.textContent;
      	});
      }
      // hide the target and select the contents of the plain text table
      target.hide();
      hide_loading();

      if ($.browser.msie) {
        // Workaround for no position:fixed in IE.
        $('#spreadsheet-instructions').css('top', $(window).scrollTop() + 100).
        css('position', 'absolute');
      }
      $("#spreadsheet-instructions").fadeIn();
      tableCopy.selectContents();

      var hide_instructions;
      hide_instructions = function() {
        tableCopy.hide();
        target.show();
        $("#spreadsheet-instructions").fadeOut();
        $("body").unbind('click', hide_instructions);
      };
      $("body").click(hide_instructions);
      return false;
    });

    $("#feed").change(function() {
      location.href = $("#feedurl").val() + $("#feed").val();
    });
    $("#fd").change(function() {
      location.href = $("#fdurl").val() + $("#fd").val();
    });
  }
  if (page == 'matrix-form' || page == 'crs') {
     $("#id_rules").sortable({axis: 'y',
                              containment: '#selected-contain'});

     $("#id_rules li span").live('click', function(){
       var rid = $(this).parent().attr("val");
       var anchor = $("#"+rid);
       if (anchor) {
         toggle_metric(anchor);
       }
     });

     var metric_opts = $('#id_rules > li');
     if (! metric_opts || metric_opts.length == 0) {
       /* The server sent an empty select list, prepopulate the
        * "selected metrics" input based on the cookie value. */
       var cookie_metrics = $.cookie('matrix-metrics');
       if (cookie_metrics && cookie_metrics.length > 0) {
         metrics = cookie_metrics.split(',');
         for (var i = 0; i < metrics.length; i++) {
          var anchor = $("#"+metrics[i]);
          if (anchor) {
            toggle_metric(anchor);
          }
         }
       }
     } else {
       /* Add the selected class to all rules contained in the select list
        * we got from the server. */
       $(metric_opts).each(function() {
         var rule_id = $(this).attr('val');
         $("#"+rule_id).addClass('selected-metric');
       });
     }
     // set the "how" radio buttons based on incoming data
     if ($('#id_release_id').val()) {
       $("#how-one").attr("checked", "checked");
     } else if ($('#id_releases').val()) {
       $("#how-many").attr("checked", "checked");
     }

     // Make the usernames field autocomplete
     $('#id_users').autocomplete(dash_base_url+"/ajax/1-user", {
       matchContains: true,
       minChars: 2,
       max: 1000,
       selectFirst: false,
       multiple: true
     });

     $('#matrix-form').submit(function() {
       /* We'll build up a query string based on the user's choices.  This
        * gives us a compact URL that someone can stick in email, as opposed
        * to something with a bunch of extra params and repeated param names. */
       var errors = [];
       var qs;
       $('#id_rules > option').attr('selected', 'selected');
       var metrics = [];
       $("#id_rules").children().each(function() {
         metrics.push($(this).attr("val"));
       });
       if (!metrics || metrics.length == 0) {
         errors.push("Please select at least one metric.");
       } else {
         qs = '?rules=' + metrics.join(',');
       }
       if ($('#how-many').attr('checked')) {
         var rels = $('#id_releases').val() || ['all'];
         qs = qs + '&releases=' + rels.join(',');
       } else {
         if ($('#id_release_id').val() == '') {
           errors.push("Please select a release.");
         }
         qs = qs + '&release_id=' + $('#id_release_id').val() +
           '&orient=' + $('#matrix-form :radio[name="orient"]:checked').val();
       }
       var userlist = $('#id_users').val().
         replace(/[, ]+/g, ' ').
         replace(/^\s+|\s+$/g, '').
         split(/ /);
       if ($('#id_reports').attr('checked') && userlist.length == 1) {
           qs += '&reports=1';
           // TODO consider posting a warning if reports checked and userlist > 1
       }
       qs = qs + '&users=' + escape(userlist.join(','));
       if (errors.length) {
         show_errors(errors);
         return false;
       }

       if ($('#id_title').val()) {
         qs = qs + '&title=' + escape($('#id_title').val());
       }
       qs = qs + '&feedback=' + $('#id_feedback').val();
       qs = qs + '&found_during=' + $('#id_found_during').val();

       /* Set a cookie with the metrics listed, so that if the user hits the
        * back button they won't have an empty list.
        */
       $.cookie('matrix-metrics', metrics.join(','), COOKIE_OPTIONS);

       show_loading();

       location.href = this.action + qs;
       return false;
     });

     // Handle swapping between one and many releases
     $('#id_release_id,#id_releases,#matrix-form :radio[name="how"]').change(function(event) {
       if ($(this).val() == 'many' || $(this).attr('id') == 'id_releases') {
         $("#id_release_id,#matrix-form :radio[name='orient']").attr('disabled', 'true');
         $("#id_releases").attr('disabled', '');
         $("#how-many").attr("checked", "checked");
       } else {
         $("#id_release_id,#matrix-form :radio[name='orient']").attr('disabled', '');
         $("#id_releases").attr('disabled', 'true');
         $("#how-one").attr("checked", "checked");
       }
     });

     $("#rules-by-type a").click(function(event) {
       toggle_metric($(this));
       event.preventDefault();
     });

     $("#clear-metrics").click(function(e) {
       // remove all metrics from the select box.
       $("#id_rules").children().each(function() {
         toggle_metric($("#"+$(this).attr("val")));
       });
       e.preventDefault();
     });
  }
});

function toggle_metric(anchor) {
  var rule_id = anchor.attr('id');
  if (anchor.hasClass('selected-metric')) {
    $('#id_rules > li[val=' + rule_id + ']').remove();
  } else {
    var li = '<li val="' + rule_id + '">' + '<span>&nbsp;X&nbsp;</span>' +
      anchor.text() + '</li>';
    $('#id_rules').append(li);
  }
  anchor.toggleClass('selected-metric');
}

//Show or hide "loading" indicator.
function show_loading() {
  // Workaround for no position:fixed in IE.
  $('#loading').css('top', $(window).scrollTop());
  $('#loading').bgiframe().show();
}
function hide_loading() {
  $('#loading').fadeOut('slow');
}

//TODO This should eventually use a jquery dialog
function show_errors(messages) {
  alert("Error:\n" + messages.join('\n'));
}



/* jQuery extension to select the contents of a node.
 * Modified from http://www.zeali.net/entry/561
 */
jQuery.fn.extend({
 selectContents: function() {
   jQuery(this).each(function(i) {
     var node = this;
     var selection, range, doc, win;
     if ((doc = node.ownerDocument) &&
         (win = doc.defaultView) &&
         typeof win.getSelection != 'undefined' &&
         typeof doc.createRange != 'undefined' &&
         (selection = window.getSelection()) &&
         typeof selection.removeAllRanges != 'undefined') {
       range = doc.createRange();
       range.selectNode(node);
       if (i == 0) {
         selection.removeAllRanges();
       }
       selection.addRange(range);
     } else if (document.body &&
                typeof document.body.createTextRange != 'undefined' &&
                (range = document.body.createTextRange())) {
       range.moveToElementText(node);
       range.select();
     }
   });
 }
});

