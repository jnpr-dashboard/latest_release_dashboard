from django.conf.urls import *
from django.conf import settings
from django.contrib import admin
admin.autodiscover()

# Custom 404 handler
handler404 = 'dash.ui.views.handler404'

# The admin site will always live at the top
#urlpatterns = patterns('', url(r'^%s/admin/(.*)' % settings.DASH_URL_BASE, admin.site.root))

# Uncomment this for > Django 1.1 (e.g Django 1.3)
urlpatterns = patterns('', url(r'^%s/admin/(.*)' % settings.DASH_URL_BASE, include(admin.site.urls)))

if settings.MODE == 'proxy':
    # define proxy url patterns
    urlpatterns += patterns('dash.ui.views',        
        url(r'^%s' % "/".join([settings.DASH_URL_BASE,"json-ready"]) ,'json_ready_proxy', name='json-ready-proxy'),
        url(r'^%s' % "/".join([settings.DASH_URL_BASE,"proxy-instance-debug"]), 'instance_debug', name='proxy-instance-debug'),
        url(r'.*','proxy', name='proxy'),
        )
else:
    urlpatterns += patterns('',
        (r'^%s' % "/".join([settings.DASH_URL_BASE,"pr_date"]), include('dash.pr_date.urls')),
        (r'^%s' % "/".join([settings.DASH_URL_BASE,"junos_ops"]), include('dash.junos_ops.urls')),
        url(r'^%s' % "/".join([settings.DASH_URL_BASE,"instance-debug"]), 'dash.ui.views.instance_debug', name='instance-debug'),
        url(r'^%s' % "/".join([settings.DASH_URL_BASE,"alive"]), 'dash.ui.views.alive', name='alive'),
        (r'^%s' % settings.DASH_URL_BASE, include('dash.ui.urls')),
        url(r'^dashjava/admin', 'never', name='dashjava-admin'),
    
        # Uncomment the admin/doc line below and add 'django.contrib.admindocs'
        # to INSTALLED_APPS to enable admin documentation:
        # (r'^admin/doc/', include('django.contrib.admindocs.urls')),
    )

    if settings.DEBUG:
        urlpatterns += patterns('',
            # Used to fake logins for the dev server.  Supply a username as the
            # second path arg, and it will be created if it doesn't exist.
            (r'^login/(.+)', 'dash.ui.views.dev_login'),
        )

