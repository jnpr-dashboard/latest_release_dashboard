"""
Unit tests for Dashboard

Dirk Bergstrom, dirk@juniper.net, 2009-05-15

Copyright (c) 2009, Juniper Networks, Inc.
All rights reserved.
"""
import unittest
import os

os.environ['TESTING_MODE'] = 'fred!'

import dash

def clear_dash_caches():
    """ Clear the all and current_ids dicts in the various dash JsonSourced classes. """
    for sub in dash.JsonSourced.__subclasses__():
        sub.all = {}
        sub.current_ids = {}

class T001_DashTests(unittest.TestCase):

    def setUp(self):
        self.json_dir = os.path.join(os.path.dirname(__file__), 'ui', 'tests')
        clear_dash_caches()

    def test_01_now(self):
        self.assertEqual(dash.now, dash._static_now)
        self.assertEqual(dash.now(), 1242438240.064925)

    def test_02_Misc(self):
        dash.parse_json(os.path.join(self.json_dir, 'misc.json'))
        self.assertEqual(dash.Misc.days_after_period, 7)
        self.assertEqual(dash.Misc.first_percent, 50)
        self.assertEqual(dash.Misc.final_percent_multiplier, 20)

    def test_03_DataSource(self):
        dash.parse_json(os.path.join(self.json_dir, 'sources.json'))
        self.assertEqual(len(dash.DataSource.all), 9)
        rli = dash.DataSource.all['RLI']
        self.assertEqual(rli.name, 'RLI')
        self.assertEqual(rli.path, '/app')
        self.assertEqual(rli.host, 'deepthought.juniper.net')

    def test_04_Rule(self):
        dash.parse_json(os.path.join(self.json_dir, 'rules.json'))
        self.assertEqual(len(dash.Rule.all), 70)
        at_risk = dash.Rule.all['at_risk']
        self.assertEqual(at_risk.name, 'RLIs At Risk')
        self.assertEqual(at_risk.weight, 3)
        self.assertEqual(at_risk.sunset_milestone, None)
        self.assertEqual(at_risk.counts, 'RLIS')
        self.assertEqual(at_risk.release_based, True)

    def test_05_Milestone(self):
        dash.parse_json(os.path.join(self.json_dir, 'milestones.json'))
        self.assertEqual(len(dash.Milestone.all), 35)
        r1_deploy = dash.Milestone.all['R1_DEPLOY']
        self.assertEqual(r1_deploy.big_window, False)
        self.assertEqual(r1_deploy.sort_key, 26)

    def test_06_Objective(self):
        dash.parse_json(os.path.join(self.json_dir, 'objectives.json'))
        self.assertEqual(len(dash.Objective.all), 8)
        hold = dash.Objective.all['HOLD_TO_SCHEDULE']
        self.assertEqual(hold.name, "Hold to Schedule")
        self.assertEqual(hold.sort_key, 6)

    def test_07_User_basic(self):
        dash.parse_json(os.path.join(self.json_dir, 'rules.json'))
        dash.parse_json(os.path.join(self.json_dir, 'all-managers.json'))
        self.assertEqual(len(dash.User.all), 2585)
        for uid in ('junos ina steven ravis ravisharma npi-540 routing'.split()):
            u = dash.User.all[uid]
            self.assertFalse(u.stub)
        self.assert_(dash.User.all['gwynne'].stub)

    def test_08_releases(self):
        dash._load_record_data(self.json_dir)
        self.assertEqual(len(dash.Records.release), 15)
        r96 = dash.Records.release['9.6']
        self.assertEqual(r96['p0_ll_exit_string'], '2008-09-09')
        self.assertEqual(r96['b1_respin_type'], 'actual')

    def test_09_npis(self):
        dash._load_record_data(self.json_dir)
        self.assertEqual(len(dash.Records.npi), 97)
        n806 = dash.Records.npi['806']
        self.assertEqual(n806['npi_alias'], 'helios-npi')
        self.assertEqual(n806['hw_lead'], 'STEVEJ')

    def test_10_prs(self):
        dash._load_record_data(self.json_dir)
        self.assertEqual(len(dash.Records.pr), 31085)
        pr = dash.Records.pr['401517']
        self.assertTrue(pr[0].startswith('rsvp bandwidth'))

    def test_11_review(self):
        dash._load_record_data(self.json_dir)
        self.assertEqual(len(dash.Records.review), 1478)
        rev = dash.Records.review['63930']
        self.assertTrue(rev[0].startswith('Review PR 434713: liliw'))

    def test_12_rli(self):
        dash._load_record_data(self.json_dir)
        self.assertEqual(len(dash.Records.rli), 4425)
        rli = dash.Records.rli['5358']
        self.assertTrue(rli[0].startswith('Portfast'))

    def test_13_active_releases(self):
        dash._load_record_data(self.json_dir)
        self.assertEqual(len(dash.Records.active_releases), 15)
        self.assertEqual(dash.Records.active_releases[0], '10.4')

    def test_14_in_flight_releases(self):
        dash._load_record_data(self.json_dir)
        self.assertEqual(len(dash.Records.in_flight_releases), 6)
        self.assertEqual(dash.Records.in_flight_releases[0], '10.1')

    def test_15_grounded_releases(self):
        dash._load_record_data(self.json_dir)
        self.assertEqual(len(dash.Records.grounded_releases), 9)
        self.assertEqual(dash.Records.grounded_releases[0], '10.4')

    def test_16_schedule(self):
        dash._load_record_data(self.json_dir)
        self.assertEqual(len(dash.Records.schedule), 133)
        evt = dash.Records.schedule[0]
        self.assertEqual(evt.name, '9.6')
        self.assertEqual(evt.date, '2009-05-26')
        self.assertEqual(evt.event, 'B2 Respin')
        self.assertEqual(evt.days, '11 Days')

    def test_17_load_all(self):
        dash.load_all(self.json_dir)
        self.assertEqual(len(dash.Milestone.all), 35)
        self.assertEqual(len(dash.Objective.all), 8)
        self.assertEqual(len(dash.User.all), 7)
        self.assertEqual(len(dash.Rule.all), 70)
        self.assertEqual(len(dash.DataSource.all), 9)
        self.assertEqual(dash.Misc.days_after_period, 7)


class T002_DashUserStatusScore(unittest.TestCase):

    def setUp(self):
        clear_dash_caches()
        dash.load_all(os.path.join(os.path.dirname(__file__), 'ui', 'tests'))

    def test_01_junos_basics(self):
        junos = dash.User.all['junos']
        self.assertEqual(junos.total_score, 31048)
        self.assert_(junos.is_junos)
        self.assert_(junos.scored)
        self.assertEqual(len(junos.reports), 2584)
        self.assertEqual(len(junos.statuses), 16)
        self.assertEqual(len(junos.scores), 335)
        self.assertEqual(len(junos.scores_by_rule_id), 51)

    def test_02_junos_status(self):
        s96 = dash.User.all['junos'].statuses['9.6']
        self.assertEqual(s96.total_score, 12798)
        self.assertEqual(s96.release_id, '9.6')
        self.assertEqual(len(s96.scores), 31)
        self.assertEqual(len(s96.scores_by_rule_id), 31)

    def test_03_junos_96_score(self):
        score = dash.User.all['junos'].statuses['9.6'].scores_by_rule_id['b2_blocker_prs']
        self.assertEqual(score.score, 4620)
        self.assertEqual(score.count, 77)
        self.assertEqual(score.count_outstanding, 29)
        self.assertEqual(score.count_feedback, 48)
        self.assertEqual(score.rule, dash.Rule.all['b2_blocker_prs'])
        self.assertEqual(len(score.record_numbers), 77)
        self.assertEqual(len(score.record_numbers_outstanding), 29)
        self.assertEqual(len(score.record_numbers_feedback), 48)

    def test_04_junos_score(self):
        score = dash.User.all['junos'].scores_by_rule_id['critical_prs']
        self.assertEqual(score.score, 2034)
        self.assertEqual(score.count, 678)
        self.assertEqual(score.count_outstanding, 351)
        self.assertEqual(score.count_feedback, 352)
        self.assertEqual(score.rule, dash.Rule.all['critical_prs'])
        self.assertEqual(len(score.record_numbers), 1099)
        self.assertEqual(len(score.record_numbers_outstanding), 505)
        self.assertEqual(len(score.record_numbers_feedback), 594)

    def test_05_score_count_for_type(self):
        score = dash.User.all['junos'].scores_by_rule_id['critical_prs']
        self.assertEqual(score.count_for_type('only'), 352)
        self.assertEqual(score.count_for_type('yes'), 678)
        self.assertEqual(score.count_for_type('no'), 351)

    def test_06_score_records_for_type(self):
        score = dash.User.all['junos'].scores_by_rule_id['critical_prs']
        self.assertEqual(len(score.records_for_type('yes')), 1099)
        self.assertEqual(len(score.records_for_type('no')), 505)
        self.assertEqual(len(score.records_for_type('only')), 594)

    def test_06_score_id_hash(self):
        self.assertEqual(dash.User.all['junos'].scores_by_rule_id['critical_prs'].
                         id_hash, '0x5104deb0')

    def test_07_score_days_left(self):
        score = dash.User.all['junos'].scores_by_rule_id['critical_prs']
        self.assertEqual(score.days_left, -1)
        self.assertEqual(score.days_left_positive, False)


if __name__ == '__main__':
    unittest.main()
