
import unittest
import sys

sys.path.append("../")

from dash import RestUtility

class T_01_retrieveDataEDW(unittest.TestCase):
    """ Retrieve EDW data through REST API """
    from pprint import pprint
    EDW_API_SERVER = 'http://localhost:8080/dashboard-rest/rest/v1/'    
    pprint(EDW_API_SERVER)
    url = "%s/%s" % (EDW_API_SERVER, "agenda/junos") 
    data = RestUtility.getJSON(url)
    pprint(data['totalRecords'])
    pprint(len(data['data']))

class T_02_retrieveAndPopulateEDWData(unittest.TestCase):
    """ Retrieve EDW data and populate them into USer object """

if __name__ == '__main__':
    unittest.main()