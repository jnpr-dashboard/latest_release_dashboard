import cx_Oracle
import copy
from settings import ORACLE_DASH_POOL 

def query_dash(query):
    rs=[]
    try:
        db = ORACLE_DASH_POOL.acquire()  
        cursor = db.cursor()
        cursor.execute(query)
        lst = cursor.fetchall()
        rs = copy.deepcopy(lst)
    except cx_Oracle.DatabaseError, exception:
        error, = exception
        # check if session was killed (ORA-00028)
        if error.code == 28:
            ORACLE_DASH_POOL.drop(db)
            print "Session dropped from the ORACLE_DASH_POOL..."
    else:
        cursor.close()
        ORACLE_DASH_POOL.release(db)

    return rs
