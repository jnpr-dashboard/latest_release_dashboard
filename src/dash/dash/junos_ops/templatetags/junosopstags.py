"""
Custom tags.

Sanny Mulyono, smulyono@juniper.net

Copyright (c) 2013, Juniper Networks, Inc.
All rights reserved.
"""
import time
import urllib
import random

from django import template
from django.utils.safestring import mark_safe

from datetime import datetime

register = template.Library()

"""
Tag to display 2 date range input with some pre-defined calendar tag, to make this works
there is dependency on the javascript which kept on inc/js/junosops.js

This tag is specifically used for junos ops page, so javascript dependency and UI dependency
are tied to thepage

@parameter framepanel       ; indicate frameid on tableau src, can be multiple frames
@parameter tabname          ; indicate tabid where the frame is located
"""
@register.simple_tag
def tableau_calendar_input(framepanel="", tabname="", showbutton=True, 
                            show_all_fltrs=True):
    single_frame = framepanel.split(',')[0];

    if (show_all_fltrs):
        filter_opts = """
                            <option value="none">No Filter</option>
                            <option value="custom">Custom</option>
                            <option value="10 days ago">10 Days Ago</option>
                            <option value="2 week ago">2 Week Ago</option>
                            <option value="3 month ago">3 Month Ago</option>
                            <option value="week_range">A week range</option>
                """
    else:
        filter_opts = """
                            <option value="none">No Filter</option>
                            <option value="custom">Custom</option>
                            <option value="2 week ago">2 Week Ago</option>
                            <option value="3 month ago">3 Month Ago</option>
                            <option value="week_range">A week range</option>
                """
    
    return_html = """
    <div class="calendarBox container-fluid">
        <div class="row-fluid formDateRange">
            <div class="span12">
            <div class="span5 form-horizontal">
                <div class="control-group">
                    <label class="control-label">
                        Choose Date
                    </label>
                    <div class="controls">
                        <select class="span12" onchange="updateDate('%s');"
                            placeholder="Choose Date Range"
                            id="rangeDate%s">
                            %s
                        </select>
                    </div>
                </div>
            </div>
            <div class="span2">
                <div class="input-append">
                    <input type="text" placeholder="From Date" id="fromDate%s"
                        data-required="true" class="datePickerInput"
                        onchange="$('#rangeDate%s').val('custom');"
                        />
                    <button class="btn btn-default"
                        onclick="$('#fromDate%s').datepicker('show');"
                        >
                        <i class="icon icon-calendar"></i>
                    </button>
                </div>
            </div>
            <div class="span2">
                <div class="input-append">
                    <input type="text" placeholder="To Date" id="toDate%s"
                        class="datePickerInput"
                        onchange="$('#rangeDate%s').val('custom');"
                        />
                    <button class="btn"
                        onclick="$('#toDate%s').datepicker('show');"
                        >
                        <i class="icon icon-calendar"></i>
                    </button>
                </div>
            </div>
    """ % (single_frame, single_frame, filter_opts, single_frame, single_frame, single_frame,
           single_frame, single_frame, single_frame)
    if (showbutton):
        return_html += """
            <div class="span2">
                <button class="btn btn-success btn-block" id="btnFilter%s" onclick="filterDate('%s', '%s')">
                    <i class="icon-filter icon-fixed-width"></i> Filter
                </button>
            </div>
        """ % (single_frame, framepanel, tabname)
    else:
        # this will be overall button to filter
        return_html += """
        <div class="span2">
        <button class="btn btn-success btn-block pull-right" onclick="updateDate('%s');filterDate('%s','%s');return false">
            <i class="icon-filter icon-fixed-width"></i> Filter
        </button>
        </div>
        """ % (framepanel, framepanel, tabname)

    return_html += """
            </div>
        </div>
        <div class="row-fluid">
            <div class="alert alert-error span8 offset1" id="dateRangeMessage%s"
                style="display:none">
                <i class="icon-exclamation icon-2x text-error"></i>
                <span id="dateRangeTextMessage%s">
                    Date Range Problem
                </span>
            </div>
        </div>
    </div> <!-- end of calendarBox -->
    """ % (framepanel, framepanel)
    return return_html

"""
Tag to display JUNOS Release Ops to select (as button in radio-button behavior)

@parameter releases         ; list of releases
@parameter framepanel       ; indicate frameid on tableau src, can be multiple frames
@parameter tabname          ; indicate tabid where the frame is located
"""
@register.simple_tag
def tableau_release_input(releases, framepanel="", tabname="", showbutton=True):
    return_html = """
        <div class="row-fluid">
        <div class="releaseInputPanel">
        <label> Pick Release </label>
        <div class="btn-group" data-toggle="buttons-radio">
    """
    for rel in releases:
        return_html += """
        <button type="button" class="btn btn-info" onclick="addReleaseToParameterMemory('%s', '%s');return false;">%s</button>
        """ % (framepanel, rel, rel)

    return_html += """
            <button type="button" class="btn btn-danger" onclick="clearReleaseFromParameterMemory('%s');return false;">None</button>
        </div> <!-- end of btn-group -->
    """ % (framepanel)

    if (showbutton) :
        return_html += """
        <button class="btn btn-success" onclick="filterRelease('%s','%s');return false">
            <i class="icon-filter icon-fixed-width"></i> Filter
        </button>
        """ % (framepanel, tabname)

    return_html +="""
        </div> <!-- end of releaseInputPanel -->
        </div> <!-- end of row -->
    """

    return return_html

"""
Tag to display both release and dates input

@parameter releases         ; list of releases
@parameter framepanel       ; indicate frameid on tableau src, can be multiple frames
@parameter tabname          ; indicate tabid where the frame is located
"""
@register.simple_tag
def tableau_release_and_date_input(releases, framepanel="", tabname=""):
    # populate the release input
    return_html = tableau_release_input(releases, framepanel, tabname, False)
    # populate the date input range
    return_html+= tableau_calendar_input(framepanel, tabname, False)

    return return_html

# Various tag for date and release filter based on previous tag
# only difference is accordion usage in the tag
@register.simple_tag
def tableau_release_filter_accordion(releases, framepanel="", tabname="", showbutton=True):
    body_html = tableau_release_input(releases, framepanel, tabname);
    single_frame = framepanel.split(',')[0];
    return_html = cover_with_accordion(single_frame, body_html)
    return return_html

@register.simple_tag
def tableau_date_filter_accordion(releases, framepanel="", tabname="",
                                  showbutton=True):
    body_html = tableau_calendar_input(framepanel, tabname)
    single_frame = framepanel.split(',')[0];
    return_html = cover_with_accordion(single_frame, body_html)
    return return_html

@register.simple_tag
def tableau_date_reduced_filter_accordion(releases, framepanel="", tabname="",
                                  showbutton=True, show_all_fltrs=False):
    body_html = tableau_calendar_input(framepanel, tabname, True, False)
    single_frame = framepanel.split(',')[0];
    return_html = cover_with_accordion(single_frame, body_html)
    return return_html

@register.simple_tag
def tableau_date_and_release_filter_accordion(releases, framepanel="", tabname=""):
    body_html = tableau_release_and_date_input(releases, framepanel, tabname);
    single_frame = framepanel.split(',')[0];
    return_html = cover_with_accordion(single_frame, body_html)
    return return_html

def cover_with_accordion(framepanel, body_html):
    return_html = """
    <div class="accordion row-fluid filterAccordion" id="filter_accordion_%s">
        <div class="accordion-group span12">
            <div class="accordion-heading">
                <a class="accordion-toggle no-link" data-toggle="collapse"
                    data-parent="#filter_accordion_%s"
                    href="#filter_accordion_%s_body">
                    <i class="icon-filter icon-fixed-width"></i>
                        Filters
                    <span class="pull-right">
                        <i class="icon-double-angle-down icon-2x"></i>
                    </span>
                </a>
            </div>
            <div id="filter_accordion_%s_body" class="accordion-body collapse">
                <div class="accordion-inner">
                    <div class="row-fluid">
                        <div class="span12">
                        %s
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    """ % (framepanel, framepanel, framepanel, framepanel, body_html)
    return return_html
