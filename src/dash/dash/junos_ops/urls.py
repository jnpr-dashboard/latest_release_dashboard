'''
Created on July 23, 2013

@author: mbasha
'''
import os

from django.conf import settings
from django.conf.urls import *

urlpatterns = patterns('dash.junos_ops.views',
    url(r'^/(?P<uid>[\w\-]+)$',
        'junosops_report',
        name='junosops-report'),
)

# To serve static foo via the dev server.
urlpatterns += patterns('',
   (r'^/static/(?P<path>.*)$', 'django.views.static.serve',{'document_root': os.path.join(os.path.dirname(__file__), 'inc'), 'show_indexes': True}),
)

