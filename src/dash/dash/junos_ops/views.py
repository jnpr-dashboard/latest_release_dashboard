"""

Mahabub Basha, mbasha@juniper.net, 2013-07-03

Copyright (c) 2013, Juniper Networks, Inc.
All rights reserved.

"""
from django.conf import settings
from django.http import HttpResponse, HttpResponseRedirect, \
                        HttpResponseNotFound
from django.template import loader
from django.shortcuts import render_to_response
from django.template.context import RequestContext

from cStringIO import StringIO
import re
import dash

import dash
from dash.ui import FATAL_EXCEPTIONS
from dash.ui import DashUtil
from dash.ui.templatetags import dash_tags
from cookielib import logger

def junosops_report(request, uid=None):
    # will be used to construct link on each direct reports and manager
    report_link = "junosops-report"

    usr = get_user(uid,fall_back_to_junos=False)

    manager_data = {}
    manager_data['uid'] = usr.manager.id
    manager_data['displayName'] = usr.manager.id
    manager_data['link'] = DashUtil.make_url(report_link, args=[usr.manager.id])

    direct_reports = []
    mgr_list = []
    bg_info = {}

    # Get the all the BG info
    try:
        for bg in usr.bg_list:
            bu_obj = get_user(bg, fall_back_to_junos=False)
            bg_info[bu_obj.manager.id] = bg
    except:
        pass

    bu_mgr_id = ''

    # Get the associated BU manager name
    if uid != 'junos':
        bu_mgr =  get_user(usr.bus_memberships[0], fall_back_to_junos=False)
        bu_mgr_id = bu_mgr.manager.id
        get_mgr_hierarchy(uid, bu_mgr_id, mgr_list, bg_info)
    else:
        new_person = {}
        new_person['id'] = usr.manager.id
        new_person['link'] = DashUtil.make_url(report_link,
                                               args=[usr.manager.id])
        new_person['total_score'] = usr.manager.total_score
        mgr_list.append(new_person)
        
    top = {}
    if mgr_list:
        mgr_list.reverse()
        top = mgr_list.pop(0)
        # Add the current user to the list
        if not usr.is_BG and not usr.is_BU:
            new_person = {}
            new_person['id'] = usr.id
            new_person['link'] = DashUtil.make_url(report_link,
                                                   args=[usr.id])
            new_person['total_score'] = usr.total_score
            mgr_list.append(new_person)

    # for tableau parameter, need to pass in the manager id PLUS all
    # direct reports username in comma separated value
    userplus = uid
    # Tableau requires only direct reports as parameter
    direct_reports_parameters = []
    for person in usr.direct_reports:
        if (person.total_score >= 0):
            # adding them to the parameter.
            userplus = '%s,%s' % (userplus, person.id)
            # adding them to the direct reports dict
            new_person = {}
            new_person['id']  = person.id;
            new_person['link'] = DashUtil.make_url(report_link,args=[person.id])
            new_person['total_score'] = person.total_score
            direct_reports.append(new_person)
            direct_reports_parameters.append(person.id)

    direct_reports.sort(lambda x,y:-cmp(x['total_score'], y['total_score']))

    # Pass the username if no direct reports
    if not usr.direct_reports:
        direct_reports.append(uid)
        direct_reports_parameters.append(uid) 

    context = { 'title': 'Junos Ops Report',
                'uid': uid,
                'page_version' : 'Beta 1.0',
                'usr': usr,
                'manager_data' : manager_data,
                'direct_reports' : direct_reports,
                'manager_reports': mgr_list,
                'top_manager': top,
                'userparameters' : userplus,
                # tableu also require to pass only direct reports
                'direct_reports_parameters': ",".join(direct_reports_parameters)
              }
    return render_to_response('junos_ops/junosops.html', {}, RequestContext(request, context))

def get_user(uid, fall_back_to_junos=True):
    try:
        return dash.User.all[uid.strip()]
    except KeyError:
        if fall_back_to_junos:
            return dash.User.JUNOS
        else:
            raise dash.DashError('User "%s" not found in Dashboard.' % uid)

def get_mgr_hierarchy(uid, bu_mgr_id, mgr_list, bg_info):

    report_link = "junosops-report"
    if uid == 'junos':
        return mgr_list
    user = get_user(uid, fall_back_to_junos=False)

    # Add the BU/BG information if the BU/BG owner is same as user.
    if uid == bu_mgr_id or user.is_BU:
        if user.is_BU:
            bu = user
        else:
            bu = get_user(user.bus_memberships[0], fall_back_to_junos=False)
        if bu.is_BU:
            new_person = {}
            new_person['id'] = bu.id
            new_person['link'] = DashUtil.make_url(report_link,
                                                args=[bu.id])
            new_person['total_score'] = bu.total_score
            mgr_list.append(new_person)
    elif uid in bg_info.keys() or user.is_BG:
        if user.is_BG:
            bg = user
        else:
            bg = get_user(bg_info[uid], fall_back_to_junos=False)
        if bg.is_BG:
            new_person = {}
            new_person['id'] = bg.id
            new_person['link'] = DashUtil.make_url(report_link,
                                                args=[bg.id])
            new_person['total_score'] = bg.total_score
            mgr_list.append(new_person)

    new_person = {}
    new_person['id'] = user.manager.id
    new_person['link'] = DashUtil.make_url(report_link,
                                           args=[user.manager.id])
    new_person['total_score'] = user.manager.total_score
    mgr_list.append(new_person)

    if user.is_BG or user.is_BU:
        mgr_list.reverse()

    get_mgr_hierarchy(user.manager.id, bu_mgr_id, mgr_list, bg_info)
