/*
 * Junos Ops Related javascript library
 *
 * @author : Sanny Mulyono
 */
var DATE_FORMAT="yyyy-MM-dd";
var ERROR_DATERANGE_1 = " Date Range Error 1 : From Date Range cannot be empty!";
var ERROR_DATERANGE_2 = " Date Range Error 2 : From Date must be older than To Date!";

var tooltip_opts = {
    style: { name: 'dash'},
    position: {adjust: { screen: true}},
    show: { delay: 400 },
    hide: { delay: 200 }
};

// global json object to track all supplied parameter for given panel
// {
//    <panelname> : {
//                      release  : ... ,
//                      startdate: ... ,
//                      enddate  : ...
//                  }
// }
var parameter_memory = {};

$(document).ready(function(){

    $(".datePickerInput").each(function(){
        $(this).datepicker({
            // the format for date.js is different
            // so for bootstrap date picker, we manually
            // specify it here
            format  : "yyyy-mm-dd",
            todayBtn: true,
            todayHighlight:true
        });
    });

    // Activate the Treeview plugin for direct reports
    $("#direct_report_top_tree,#direct_report_accordion, #manager_report_accordion").treeview();

    $('.nav-tabs').tabdrop();

    $('a[data-toggle="tab"]').on('shown', function () {
        //Set the latest active tab
        $.cookie('last_act_tab', $(this).attr('href'));
        var frame_id = $(this).attr('href');
        // Remove the '#' character.
        frame_id = frame_id.replace('#','');

        // Set the default date filter value on page load.
        comboLoadDefault(frame_id);
    });
    // Invoking dashboard header related functions
    dash_headers();

});

// Dashboard header menu jquery functions
function dash_headers() {

    // The slide down/up magic for the "upcoming events" box
    $('#upcoming').hoverIntent(function() {
        $("#more-upcoming").slideDown(400);
    },
        function() {
            $("#more-upcoming").slideUp(400);
        }
    );

    // Define style for tooltips
    $.fn.qtip.styles.dash = { // Last part is the name of the style
      width: { min: 300, max: 500 },
      background: 'white',
      color: 'black',
      border: {
         width: 1,
         radius: 1,
         color: '#A5A5A5'
      },
      title: {
        'text-align': 'center',
        color: 'black',
        background: '#FFF2AC'
      },
      tip: {
        corner: 'leftMiddle',
        color: '#A5A5A5'
      }
    };

    $("#dwim").qtip($.extend(true, {}, tooltip_opts, {
        style: { tip: { corner: 'topMiddle' } },
        position: { corner: { target: 'bottomMiddle', tooltip: 'topMiddle' } },
        show: { delay: 200 },
        content: { title: { text: 'The "Do What I Mean" Box' },
               text: $("#tt_dwim").html() }
    }));

    $('#dwim').autocomplete(dash_base_url+"/ajax/1-user", {
        matchContains: true,
        minChars: 2,
        delay: 200,
        selectFirst: false,
        max: 1000
    });
    $('#dwim-form').submit(function(e){
        $("#dwim").qtip('hide');
        show_loading();
        var dwim = $.trim($('#dwim').val());
        if (dwim.match(/\s|^prs$|^(pr|rli)\d+/)) {
            // Anything with a space, or "prs", or a PR or RLI number, pass to
            // server
        return true;
        } else if (dwim.match(/^[\w\-]+$/)) {
            // A username, take them directly to that user's agenda
            location.href = dash_base_url + '/agenda/' + dwim;
            return false;
        } else {
            // pass to server
            return true;
        }
    });
}

function show_loading() {
  // Workaround for no position:fixed in IE.
  $('#loading').css('top', $(window).scrollTop());
  $('#loading').bgiframe().show();
}


// Responsible on filtering date, will be picking up the
// from and to date. This button event is designed
// to only pass the value into current Iframe
// Also validate date range information
function filterDate(framenames, tabname){
    var singleName = framenames.split(',')[0];

    var dateRangeMessageId = "dateRangeMessage" + singleName;
    var dateRangeTextMessageId = "dateRangeTextMessage" + singleName;

    var rangeValue = $("#rangeDate" + singleName).val();

    $("#" + dateRangeMessageId).hide("fast");
    var fromDateId = "#fromDate" + singleName;
    var toDateId = "#toDate" + singleName;

    var fromDateValue = $(fromDateId).val();
    var toDateValue = $(toDateId).val();
    // if to date is empty, then by default fill with today date
    if (toDateValue == "" && rangeValue != "none"){
        $(toDateId).val(Date.parse("today").toString(DATE_FORMAT));
        toDateValue = $(toDateId).val();
    }
    // do some validation - from date cannot be empty
    if (fromDateValue == "" && rangeValue != "none"){
        $("#" + dateRangeTextMessageId).html(ERROR_DATERANGE_1);
        $("#" + dateRangeMessageId).fadeIn();
        return false;
    }
    // make sure from and to date is in range
    if (rangeValue != "none" && (Date.parse(fromDateValue) > Date.parse(toDateValue))){
        $("#" + dateRangeTextMessageId).html(ERROR_DATERANGE_2);
        $("#" + dateRangeMessageId).fadeIn();
        return false;
    }
    // if it reach here, then we can pass this into Tableau tab
    // by calling the active tab
    var frameid = framenames.split(',');
    for (var i in frameid){
        var framename = frameid[i];
        // keep data into parameter memory
        if (rangeValue != "none"){
            addDateToParameterMemory(framename, fromDateValue, toDateValue);
        }
        // first clear the iframe src first so that the view will be
        // refreshed
        clearTableuIframeSrc(framename);
    }
    // determine the IFrame src
    determineIframeSrc(tabname);
}

// Change the options in combobox to the actual date
function updateDate(framenames, isDefault){
    var singleName = framenames.split(',')[0];

    var tabname = framenames.split('_');
    var selectedVal = $("#rangeDate" + singleName).val().toLowerCase();

    var fromDate = $("#fromDate" + singleName);
    var toDate = $("#toDate" + singleName);

    // enabled filter button if any
    $("#btnFilter" + singleName).removeAttr("disabled");

    // when changed to custom, don't make any changes
    if (selectedVal != "custom" && selectedVal != "none" &&
        selectedVal != "week_range"){

        // Consider the upcoming saturday for incoming and disposal tab for
        // date filters
        if (tabname[0] + '_' + tabname[1] == 'tab_incdisp') {
            fromDate.val(Date.parse(selectedVal).saturday().toString(DATE_FORMAT));
            toDate.val(Date.parse(Date.saturday()).toString(DATE_FORMAT));
        } else {
            // from the selectedVal, determine the fromDate
            fromDate.val(Date.parse(selectedVal).toString(DATE_FORMAT));
            // make the toDate as today date
            toDate.val(Date.parse("today").toString(DATE_FORMAT));
        }
    } else
    /* Add this based on input from Anoop since report should also
       provide coming weekend date as the weekly trend reports */

    if (selectedVal == "week_range") {
        // make the from Date ranges 1 week before
        fromDate.val(Date.parse("1 week ago").toString(DATE_FORMAT));

        // make the end date as 1 week after today
        toDate.val(Date.parse("next week").toString(DATE_FORMAT));
    } else
    if (selectedVal == "none"){
        // no filter is selected
        fromDate.val('');
        toDate.val('');
        var frameid = framenames.split(',');
        for (var i in frameid){
            var framename = frameid[i];
            clearDateFromParameterMemory(framename);
        }
        // disabled the button
        $("#btnFilter" + framename).attr("disabled", "true");
    }

    if (isDefault && $('#' + singleName).attr('src') == "") {
        $('#btnFilter' + singleName).click();
    }

}

function filterRelease(framenames, tabname) {
    var frameid = framenames.split(',');
    for (var i in frameid){
        var framename = frameid[i];
        clearTableuIframeSrc(framename);
        determineIframeSrc(tabname);
    }
}

function addDateToParameterMemory(framenames, startdate, enddate){
    var frameid = framenames.split(',');
    for (var i in frameid){
        var framename = frameid[i];
        if (parameter_memory[framename] == undefined){
            parameter_memory[framename] = {};
        }
        // add the dates
        parameter_memory[framename]['startdate'] = startdate;
        parameter_memory[framename]['enddate']   = enddate;
    }
}

function clearDateFromParameterMemory(framenames){
    var frameid = framenames.split(',');
    for (var i in frameid){
        var framename = frameid[i];
        if (parameter_memory[framename] == undefined){
            parameter_memory[framename] = {};
        }
        // reset the dates
        delete parameter_memory[framename]['startdate'];
        delete parameter_memory[framename]['enddate'] ;
    }
}

function addReleaseToParameterMemory(framenames, release){
    var frameid = framenames.split(',');
    for (var i in frameid){
        var framename = frameid[i];
        if (parameter_memory[framename] == undefined){
            parameter_memory[framename] = {};
        }
        // add the release
        parameter_memory[framename]['release'] = release;
    }
}

function clearReleaseFromParameterMemory(framenames){
    var frameid = framenames.split(',');
    for (var i in frameid){
        var framename = frameid[i];
        if (parameter_memory[framename] == undefined){
            parameter_memory[framename] = {};
        }
        // reset the dates
        delete parameter_memory[framename]['release'];
    }
}

// Load the latest active tab (Tab preserving on page load)
function loadTab() {
    var tab_passed = $(location).attr('hash');
    var lastTab = $.cookie('last_act_tab');
    if (tab_passed) {
        $('a[href=' + tab_passed + ']').tab('show');
    } else
    if (lastTab) {
        $('a[href=' + lastTab + ']').tab('show');
    } else {
        $('a[data-toggle="tab"]:first').tab('show');
    }
}

// TODO: Needs to do for other tabs
function comboLoadDefault(panelname) {
    if (panelname == "tab_execsumm") {
        $('#rangeDate' + panelname + '_frame1 option[value="10 days ago"]').attr('selected', 'selected');
        updateDate(panelname + '_frame1', true);
    } else
    if (panelname == "tab_mttr") {
        $('#rangeDate' + panelname + '_frame1 option[value="3 month ago"]').attr('selected', 'selected');
        // To prevent to load the box2 in the same tab
        // So that default date filter will work for both box1 and box2
        // sequentially
        if ($('#' + panelname + '_frame2').attr("src") == "") {
            $('#' + panelname + '_frame2').attr("src", "nolink");
        }
        updateDate(panelname + '_frame1', true);
        $('#rangeDate' + panelname + '_frame2 option[value="3 month ago"]').attr('selected', 'selected');
        if ($('#' + panelname + '_frame2').attr("src") == "nolink") {
            $('#' + panelname + '_frame2').attr("src", "");
        }
        updateDate(panelname + '_frame2', true);
    } else
    if (panelname == "tab_cl") {
        $('#rangeDate' + panelname + '_frame1 option[value="2 week ago"]').attr('selected', 'selected');
        updateDate(panelname + '_frame1', true);
    } else
    if (panelname == "tab_inf") {
        $('#rangeDate' + panelname + '_frame option[value="2 week ago"]').attr('selected', 'selected');
        updateDate(panelname + '_frame', true);
    } else
    if (panelname == "tab_blk") {
        $('#rangeDate' + panelname + '_frame1 option[value="2 week ago"]').attr('selected', 'selected');
        updateDate(panelname + '_frame1', true);
    } else
    if (panelname == "tab_release") {
        $('#rangeDate' + panelname + '_frame1 option[value="2 week ago"]').attr('selected', 'selected');
        if ($('#' + panelname + '_frame2').attr("src") == "") {
            $('#' + panelname + '_frame2').attr("src", "nolink");
        }
        updateDate(panelname + '_frame1', true);
        $('#rangeDate' + panelname + '_frame2 option[value="2 week ago"]').attr('selected', 'selected');
        if ($('#' + panelname + '_frame2').attr("src") == "nolink") {
            $('#' + panelname + '_frame2').attr("src", "");
        }
        updateDate(panelname + '_frame2', true);
    } else
    if (panelname == "tab_mratt") {
        $('#rangeDate' + panelname + '_frame1 option[value="2 week ago"]').attr('selected', 'selected');
        if ($('#' + panelname + '_frame2').attr("src") == "") {
            $('#' + panelname + '_frame2').attr("src", "nolink");
        }
        updateDate(panelname + '_frame1', true);
        $('#rangeDate' + panelname + '_frame2 option[value="2 week ago"]').attr('selected', 'selected');
        if ($('#' + panelname + '_frame2').attr("src") == "nolink") {
            $('#' + panelname + '_frame2').attr("src", "");
        }
        updateDate(panelname + '_frame2', true);
    } else
    if (panelname == "tab_incdisp") {
        $('#rangeDate' + panelname + '_frame1 option[value="2 week ago"]').attr('selected', 'selected');
        if ($('#' + panelname + '_frame2').attr("src") == "") {
            $('#' + panelname + '_frame2').attr("src", "nolink");
        }
        updateDate(panelname + '_frame1', true);
        $('#rangeDate' + panelname + '_frame2 option[value="2 week ago"]').attr('selected', 'selected');
        if ($('#' + panelname + '_frame2').attr("src") == "nolink") {
            $('#' + panelname + '_frame2').attr("src", "");
        }
        updateDate(panelname + '_frame2', true);
    } else
    if (panelname == "tab_commit") {
        $('#rangeDate' + panelname + '_frame2 option[value="2 week ago"]').attr('selected', 'selected');
        updateDate(panelname + '_frame2', true);
    } else {
        determineIframeSrc(panelname);
    }
}

function clearTableuIframeSrc(frameid){
    $("#" + frameid).attr("src", "");
}

/* This library should be better constructed for re-use */
/* TODO...... make tableau related library / ticket generation as one library */
function showEmbedTableauOps(frameid, workbook, reportname, parameters) {
    if ($("#" + frameid ).attr("src") != ""){
        // if the src is already populated then don't do another
        // loading
        return ;
    }

    if (parameters == undefined){
        $.get('/dashboard/ajax/generate_tableau_url/' + workbook + '/' + reportname + '/:tabs=no', function(data){
            // Before iframe content is loaded
            $("#" + frameid + "_notification").show();
            $("#" + frameid).attr("src", data);
            $("#" + frameid).load(function(){
                $("#" + frameid + "_notification").hide();
            });
        });
    } else {
        $.get('/dashboard/ajax/generate_tableau_url/' + workbook + '/' + reportname , function(data){
            // Adding long (and unformatted) parameters will not work on the ajax method, so
            // parameters will be processed here instead.
            // Server code for handling parameters might not be needed for now.
            if ($.trim(parameters).indexOf("&") != 0){
                parameters = "&" + parameters;
            }
            // Before iframe content is loaded
            $("#" + frameid + "_notification").show();
            $("#" + frameid).attr("src", data + parameters);
            $("#" + frameid).load(function(){
                // when iframe content is loaded
                $("#" + frameid + "_notification").hide();
            });
        });
    }
}

// Convert date into GMT, relied on parameter in local time zone
// Note that a negative return value from getTimezoneOffset() indicates
// that the current location is ahead of UTC, while a positive value
// indicates that the location is behind UTC.
function toGMT(original_date){
    // get local date / time
    var date = new Date();

    var check_date = original_date;
    if (check_date.indexOf('GMT') < 0){
        // construct time string
        var strTime = date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds() + " GMT" + date.getUTCOffset();
        check_date = original_date + " " + strTime;
    }

    var diff = Date.parse(check_date).getTimezoneOffset(); // need to multiply -1 (look function description)
    var startDateGMT = Date.parse(check_date).add(diff).minutes();
    return startDateGMT.toString(DATE_FORMAT);
}

// TODO... need to use jUnit or other testing framework
// but for simplicity currently it uses manual testing
function testToGMT(){
    // all result in console must be true
    // test with PST date
    result = toGMT('08/29/2013 17:15:00 GMT-0700');
    console.log(result == '2013-08-30');

    // test with other Timezone
    result = toGMT('08/29/2013 00:15:00 GMT+0700');
    console.log(result == '2013-08-28');

    result = toGMT('09/13/2013');
    console.log(result);
}
