import os, sys

os.environ['DJANGO_SETTINGS_MODULE'] = 'dash.settings'
os.environ['PYTHON_EGG_CACHE'] = '/opt/python-eggs'

import django.core.handlers.wsgi
from django.conf import settings

import dash
# do initial load in a user friendly way
dash.async_load_all()

_application = django.core.handlers.wsgi.WSGIHandler()

def application(environ, start_response):
    environ['SCRIPT_URL'] = '/'
    environ['PATH_INFO'] = '/%s%s' % (settings.DASH_URL_BASE, environ['PATH_INFO'])
    return _application(environ, start_response)
