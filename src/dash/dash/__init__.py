"""
Base functionality for Dashboard data processing.

Dirk Bergstrom, dirk@juniper.net, 2009-03-31

Copyright (c) 2009, Juniper Networks, Inc.
All rights reserved.
"""

from copy import deepcopy
from django.conf import settings
from django.contrib import auth
from django.contrib.auth import load_backend
from django.contrib.auth.backends import RemoteUserBackend
from django.contrib.auth.middleware import RemoteUserMiddleware
from django.core.exceptions import ImproperlyConfigured
from django.core.signals import request_started
from django.shortcuts import render_to_response
from django.template.context import RequestContext
from threading import Lock
from urllib2 import urlopen, URLError
import cjson
import datetime
import gc
import logging
import os
import re
import shutil
import sys
import threading
import time
import traceback
import urllib2

# do not use json or simplejson, they are slower than cjson


# ensures two loads don't happen at the same time
LOADING_LOCK = Lock()
# used for debugging to count the number of loads
LOAD_COUNTER_LOCK = Lock()
# the value being protected by the lock above
LOAD_COUNT = 0

# used for locking the number of proxy loads
PROXY_COUNTER_LOCK = Lock()
# the value being protected by the lock above
PROXY_LOAD_COUNT = 0


# text of most recent loading traceback
LOAD_TRACEBACK = ''

JSON_LOADED = False

PERCENT_LOADED = 0

# R1, R2, ... R<MAX_R_RELEASE>
MAX_R_RELEASE = 4

cdm_release_cutoff = 10.3
cdm_v2_release_cutoff = 11.4

# Generate list for problem level rules filter
PL_FILTERS = ['CL' + str(i) for i in range(1, 5)] + \
             ['IL' + str(i) for i in range(1, 5)]

# Mapping values for problem level filters
PL_MAPS = { 'CL1': '1-CL1',
            'CL2': '2-CL2',
            'CL3': '4-CL3',
            'CL4': '5-CL4',
            'IL1': '3-IL1',
            'IL2': '4-IL2',
            'IL3': '5-IL3',
            'IL4': '6-IL4'
          }

# Used the rule for loading problem level rules on UI
RULE_NAME = 'prs_user'

# TODO:XXX Needs to be loaded separtely from EDW. 
pr_rules = {
            'blocker_prs': 'All Blocker PRs',
            'prs_user': 'Total PRs'
           }

date_pattern = re.compile(r'(\d{4})-(\d{2})-(\d{2})\s*.*')

def _static_now():
    """ Used for testing, always returns the same time, 2009-05-15 18:44:23 """
    return 1242438240.064925

def now():
    """ Returns time.time().

    Can be overridden during testing, so that date-based calculations always
    return the same value.  Set the environment variable 'TESTING_MODE' to
    a non-blank value to do so.
    """
    return time.time()

if os.environ.has_key('TESTING_MODE'):
    now = _static_now

class DashError(Exception):
    pass


class JsonSourced(object):
    """ A class whose instances are sourced from a JSON stream.

    FIXME handle disappearing id's, stubs, etc.
    """

    @classmethod
    def clear_all_subclass_current_ids(cls):
        for sub in cls.__subclasses__():
            sub.current_ids = {}

    @classmethod
    def find_or_create(cls, id, params=None):
        """ Find the existing object for id, or create one. """
        try:
            obj = cls.all[id]
        except KeyError:
            obj = cls(id)
            cls.all[id] = obj
        if params:
            obj._populate(params)
        return obj

    @classmethod
    def clear_deleted_objects(cls):
        """ Remove objects that have disappeared upstream. """
        for id_ in cls.all.keys():
            if id_ not in cls.current_ids:
                del(cls.all[id_])

    def __init__(self, id):
        """ Just create the object, _populate does the work. """
        self.id = id
        self.stub = True

    def _populate(self, params):
        self.stub = False
        self.current_ids[self.id] = 1


class User(JsonSourced):
    """ A user; either a person, a virtual team, or a pseudo-user like 'junos'
    or an NPI program.
    """

    all = {}
    current_ids = {}
    JUNOS = None
    non_bu_user = {}

    """ DEBUG VAR """
    history_junos_rec = set()
    current_junos_rec = set()
    history_sum_rec = set()
    current_sum_rec = set()



    """ Only used for MTTR Display on the dashboard with the parse_mttr_file function.
    """
    """
    Start of MTTR related data structures
    """
    bu_list = []
    bg_list = []
    """
    End of MTTR related data structures
    """
    

    def _vivify_list(self, ids):
        dest = []
        for uid in ids:
            if isinstance(uid, dict): uid = uid['id']
            dest.append(self.find_or_create(uid))
        return dest

    def __init__(self, id):
        """ User's stub is more full-featured, to avoid errors. """
        self.id = id
        self.stub = True
        self.is_manager = False
        self.department_number = None
        self.virtual_team = False
        self.department_vt = False
        # the 'junos' object must be of class User
        self.manager = id == 'junos' and self or self.JUNOS
        self.virtual_teams = []
        self.virtual_team_memberships = []
        self.direct_reports = []
        self.reports = []
        self.last_updated = None
        self.pseudo_user = False
        self.is_junos = id == 'junos'
        self.scored = False
        self.scores = []
        self.scores_by_rule_id = {}
        self.total_score = 0
        self.statuses = {}
        self.spoc_categories = []
        self.report_release_statuses = {}
        self.is_BU = False;
        self.is_BG = False;
        self.bus_memberships=[]

    def _populate(self, params):
        """ Users are always instantiated from a JSON-sourced dict.

        TODO Use properties to lazy-initialize compute-intensive attrs:
        Profiling shows that instantiating and sorting Scores are the most
        time-consuming activities.
        Commenting out Score sorting is good for ~10% less time with dev data.
        """
        super(User, self)._populate(params)
        self.is_manager = params.get('AManager', False)
        # FIXME is there any time at which a VT isn't a "manager"?  if so, we
        # need a has_reports attr...
        self.department_number = params.get('departmentNumber', None)
        self.virtual_team = params.get('virtualTeam', False)
        if self.virtual_team and self.id.startswith("dept-"):
            self.department_vt = True
        self.is_manager |= self.virtual_team
        mgr_dict = params.get('manager', None)
        if mgr_dict:
            self.manager = self.find_or_create(mgr_dict['id'])
        else:
            self.manager = self.JUNOS
        self.virtual_teams = self._vivify_list(params.get('virtualTeams', []))
        self.virtual_team_memberships = \
            self._vivify_list(params.get('virtualTeamMemberships', []))
        self.direct_reports = self._vivify_list(params.get('directReportNames', []))
        self.reports = self._vivify_list(params.get('reportNames', []))
        self.pseudo_user = params.get('pseudoUser', False)
        self.spoc_categories = params.get('spocCategories', [])
        scored_tmp = params.get('scored', False)
        if self.scored and not scored_tmp:
            # We've got cached status in the UI layer, but not yet in the rules
            # engine.  Don't overwrite what we have (since it's presumably
            # better than nothing).
            return

        # We get here if a) we were previously scored and there are new scores,
        # or b) we don't have any scores yet.
        self.scored = scored_tmp
        updated = params.get('lastUpdated', None)
        if updated:
            self.last_updated = datetime.datetime.fromtimestamp(updated / 1000)
        else:
            self.last_updated = None
        self.total_score = params.get('totalScore', 0)
        self._populate_score_from_json(params)

        # Preprocessing the Problem level rules
        for scr in self.scores:
            scr = _preprocessing_plfilters(scr)
            scr = _preprocessing_fd_scores(scr)
            scr = _preprocessing_reviews(scr)
        for scr in self.scores_by_rule_id.keys():
            self.scores_by_rule_id[scr] = _preprocessing_plfilters(
                                                self.scores_by_rule_id[scr])
            self.scores_by_rule_id[scr] = _preprocessing_fd_scores(
                                                self.scores_by_rule_id[scr])

        # Cache for computed top-scoring reports in each release
        self.report_release_statuses = {}
        # Cache for computed total scores for release subsets
        self.release_subset_totals = {}        
        
    def _populate_score_from_json(self, params):
        new_scores, new_scores_by_rule_id = \
            _vivify_scores(params.get('scores', []))

        # The total score for each release-based rule across all releases
        for rule_id, dict_ in params.get('totalsByRuleId', {}).iteritems():
            new_scores_by_rule_id[rule_id] = Score(dict_)
            if rule_id == RULE_NAME:
                for pl_rule in PL_FILTERS:
                    dict_['ruleId'] = pl_rule
                    dict_['rule'] = Rule.all[pl_rule]
                    new_scores_by_rule_id[pl_rule] = Score(dict_)
    
        # dict of release_id : status for release
        new_statuses = {}
        # dict of M.n : consist of scores & statuses Ibx 
        cdm_grouping = {}
        cdm_v2_grouping = {}
        for rel_id, dict_ in params.get('statuses', {}).iteritems():
            new_statuses[rel_id] = Status(dict_)
            if (len(rel_id) > 0 ):
                mDotN = findNumericRelease(rel_id)
                if (float(mDotN) >= cdm_v2_release_cutoff) :
                    # add it here, for more grouping later on
                    if (rel_id != mDotN):
                        if mDotN not in cdm_v2_grouping:
                            cdm_v2_grouping[mDotN] = []
                        cdm_v2_grouping[mDotN].append(new_statuses[rel_id])
                if (float(mDotN) >= cdm_release_cutoff and float(mDotN) < cdm_v2_release_cutoff) :
                    # add it here, for more grouping later on
                    if (rel_id != mDotN):
                        if mDotN not in cdm_grouping:
                            cdm_grouping[mDotN] = []
                        cdm_grouping[mDotN].append(new_statuses[rel_id])
        
        # Grouping in cdm release, need to update  totalScores
        for rel_id in cdm_grouping :
            parent = new_statuses[rel_id]
            for status in cdm_grouping[rel_id]:
                parent.total_score += status.total_score 
        
        # Grouping in cdm release, need to update  totalScores
        for rel_id in cdm_v2_grouping :
            parent = new_statuses[rel_id]
            for status in cdm_v2_grouping[rel_id]:
                parent.total_score += status.total_score 

        # Add all the per-release scores into our list of scores, for use
        # in the agenda.
        for status in new_statuses.values():
            new_scores.extend(status.scores)
        new_scores.sort()
        
        self.statuses = new_statuses
        self.scores = new_scores
        self.scores_by_rule_id = new_scores_by_rule_id

    def _populate_scores_from_EDW(self):
        """ pickup another scores from EDW databases """
        from pprint import pprint
        
    def clean_up_bus_memberships(self):
        """ Sorted out the BUS memberships """
        if self.id == "junos":
            self.bus_memberships = []
        else:
            for candidate in self.virtual_team_memberships:
                if candidate.id in User.bu_list:
                    self.bus_memberships.append(candidate.id)
            # for user who has no VT and no BU assign to him/her
            if len(self.bus_memberships) <= 0 :
                self.bus_memberships.append(self.id)
                
    def sort_reports(self):
        """ Sort reports once all user objects have been loaded. """
        if not self.stub:
            try:
                self.direct_reports.sort(key=lambda x:(x.bus_memberships[0].lower(), not x.is_BG))
            except IndexError:
                # Should never happens, but if it does then it means there are some people which not in BU
                self.direct_reports.sort()
            self.virtual_teams.sort()

    def top_reports_in_release(self, release_id):
        """ Return the top-scoring reports in the given release.
        Caches computed values.
        """
        if not release_id in self.report_release_statuses:
            statuses = []
            for rep in self.direct_reports:
                if not rep.stub:
                    relstat = rep.statuses.get(release_id, None)
                    if relstat and relstat.total_score:
                        statuses.append(relstat)
            statuses.sort()
            self.report_release_statuses[release_id] = statuses
        return self.report_release_statuses[release_id]

    def total_for_releases(self, release_ids, rule_id):
        """ Given a tuple or list of release ids and a rule id, return the
        total score (which may be None).  Caches results.
        """
        release_ids = tuple(release_ids)
        releases_dict = self.release_subset_totals.setdefault(release_ids, {})
        
        # need to scan the releases, if there is ex: 10.3IB1, 10.3IB2, 10.3
        #    then the total score only need to be calculated for 10.3
        try:
            return releases_dict[rule_id]
        except KeyError:
            # Nothing cached, build it
            total_score = None
            for rel in release_ids:
                nrel = findNumericRelease(rel)
                try:
                    score = self.statuses[nrel].scores_by_rule_id[rule_id]
                except KeyError:
                    # No status for this release, or no score for this rule/rel
                    continue
                total_score = self.__make_total_score(score,  total_score)
                
                if rel == nrel :
                    # iterate the IB
                    if float(nrel) >= cdm_release_cutoff: 
                        for v_IB in Records.release[rel]['childBranch_array']:
                            try:
                                score = self.statuses[str(v_IB)].scores_by_rule_id[rule_id]
                            except KeyError:
                                # No status for this release, or no score for this rule/rel
                                continue
                            total_score = self.__make_total_score( score, total_score)
            releases_dict[rule_id] = total_score
            return total_score

    def additional_preprocessing(self):
        '''
        Additional preprocessing data. Process is done here instead in views to reduce
            complexity and response time. 
        '''
        self._additional_BacklogPR_process()

    def _additional_BacklogPR_process(self):
        '''
           calculate the diff/delta calculation, so the UI wont 'look' slow
           creates synthetic rule results based on actual rules
        '''

        suffix_type = ['','_responsible','_responsible_not_dev','_cvbc','_doc']
        try:
            for _type in suffix_type :
                current_rule_id = 'backlog_prs_current%s' % (_type)
                forward_rule_id = 'backlog_prs_forward%s' % ( _type)
                diff_result_rule = 'backlog_prs_diff%s' % (_type)
                current_rule = self.scores_by_rule_id.get(current_rule_id)
                forward_rule = self.scores_by_rule_id.get(forward_rule_id)
                
                if current_rule and forward_rule:
                    diff_score, diff_score_map, other_scores = self._duplicate_score_for_forward_view(current_rule, forward_rule)
                    self.scores_by_rule_id[diff_result_rule] = diff_score
                    self.scores_by_rule_id[diff_result_rule].score = diff_score_map
                    self.scores_by_rule_id[diff_result_rule].other_scores = other_scores

                if forward_rule and not current_rule:
                    self.scores_by_rule_id[diff_result_rule] = forward_rule
        except KeyError:
            # There are users which don't have any backlog PR scores, so we dont count
            #    the diff 
            pass

    def _duplicate_score_for_forward_view(self, curr_score, forw_score):
        _dup = deepcopy(forw_score)
        _dup_score = 0
        diff_other_scores = {}
        try:
            set_total_rows = set()
            
            suffixes = ['']
            # CVBC rules counting
            cvbc_count = False
            
            if forw_score.rule.counts == 'PRS':
                suffixes.extend(('_feedback', '_outstanding','_suspended','_monitored',))
                
            # Unique condition for CVBC counts and records
            if forw_score.rule.id == 'backlog_prs_forward_cvbc':
                suffixes = ['']
                suffixes.extend(('all', 'yes' , 'yes-ready-for-review'))
                cvbc_count = True
            if cvbc_count:
                _dup_score = forw_score.score
            else:
                _dup_score = forw_score.score - curr_score.score
                diff_other_scores = dict((k,v-curr_score.other_scores[k]) for k,v in forw_score.other_scores.iteritems())
                
            set_total_rows = set()  
            for suffix in suffixes:
                attr = 'record_numbers%s' % suffix
                try:
                    if cvbc_count :
                        # special CVBC records retrieval
                        my_recs = curr_score.get_special_record('cvbc',suffix)
                    else:
                        my_recs = getattr(curr_score, attr)
                    for i in my_recs:
                        set_total_rows.add(i.split('-')[0])
                except AttributeError as (errDump):
                    logging.error('Failed to catch Attribute : %s ' % (errDump))
                
            # updating forward_view and diff
            for suffix in suffixes:
                attr = 'record_numbers%s' % suffix
                attr_count = 'count%s' % suffix
                diff_recs = []
                diff_recs_count = 0
                try:
                    if cvbc_count :
                        # special CVBC records retrieval
                        current_recs = curr_score.get_special_record('cvbc', suffix)
                    else:
                        current_recs = getattr(curr_score, attr)
                    
                    if cvbc_count:
                        # special CVBC counts retrieval
                        forward_recs = forw_score.get_special_record('cvbc', suffix)
                    else:
                        forward_recs = getattr(forw_score, attr)
                    
                    set_current = set()
                    set_forward = set()
                    diff_recs = []
                    for i in current_recs:
                        set_current.add(i.split('-')[0])
                    
                    for i in forward_recs:
                        set_forward.add(i.split('-')[0])
                    
                    set_diff = set_forward - set_current
                    set_find = set_total_rows - set_current 
                    diff_recs = []
                    for n in forward_recs :
                        if (n.split('-')[0] in set_diff) and (n.split('-')[0] not in set_find):
                            diff_recs.append(n)
                    diff_recs_count = len(diff_recs)
                except AttributeError as (errDump):
                    logging.error('Failed to catch Attribute : %s ' % (errDump))
                    
                if cvbc_count:
                    # Set the special counts and records for CVBC rules
                    _dup.set_special_record('cvbc', suffix, diff_recs)
                    _dup.set_special_count('cvbc', suffix, diff_recs_count)
                else:
                    setattr(_dup, attr, diff_recs)
                    setattr(_dup, attr_count, diff_recs_count)

        except KeyError:
            # There is a chance that the score didn't hold certain record number or count since they
            #    don't have any
            pass
        return _dup, _dup_score, diff_other_scores
        
    def __make_total_score (self, score, total_score):
            if not total_score:
                total_score = score.copy_as_totals_score()
            else:
                total_score.add_score(score)
            return total_score
    
    def __cmp__(self, other):
        """ Users sort by total_score descending, then by username. """
        return cmp(getattr(other, 'total_score', 0),
                   getattr(self, 'total_score', 0)) or \
                cmp(self.id, getattr(other, 'id', ''))

    def __repr__(self):
        return '<dash.User "%s", score: %d>' % \
            (self.id, getattr(self, 'total_score', 0))

    @classmethod
    def construct_BG_tree(cls):
        """ Construct the BU / BG tree """
        filename=os.path.join(settings.SPECIAL_DIR,'bg-list.properties')
        reffile = open(filename,'r')
        try:
            for line in reffile:
                whole = line.rstrip()
                temp = whole.split('=')
                if temp[0] == 'bg' :
                    # get the bg
                    first_part = temp[1].split(',')
                    for temp_list in first_part:
                        # get the bu inside each bg
                        bg = temp_list.split(':')[0]
                        cls.bg_list.append(bg)
                        cls.all[bg].bus_memberships= []
                        cls.all[bg].bus_memberships.append(bg)
                        cls.all[bg].is_BG = True
                        cls.all[bg].is_BU = False
                        # for bg who has no bu
                        bu_list = temp_list.split(':')[1]
                        for temp_bu in bu_list.split(';'):
                            try:
                                cls.all[temp_bu].bus_memberships = []
                                cls.all[temp_bu].bus_memberships.append(bg)
                                cls.bu_list.append(temp_bu)
                                cls.all[temp_bu].is_BG = False
                                cls.all[temp_bu].is_BU = True
                            except KeyError:
                                # We keep continue if this BG has no BU associated to it
                                continue
        finally:
            reffile.close()
            # this is list of user to do catch all, just for UI representation
            cls.all['empty_spoc'].bus_memberships = ['CatchAll']
            cls.all['deprecated_category'].bus_memberships = ['CatchAll']
            cls.all['non_user'].bus_memberships = ['CatchAll']


# Pre-create the junos user to speed up stub creation.
User.JUNOS = User.find_or_create('junos')

class NPIUser(User):
    """ A pseudo-user representing an NPI program. """

    is_npi = True
    programs_by_id = {}
    sorted_programs = []

    @classmethod
    def sort_programs(cls):
        """ Sort the list of NPI program users by score. """
        new_sorted_programs = cls.programs_by_id.values()
        new_sorted_programs.sort()
        cls.sorted_programs = new_sorted_programs

    def _populate(self, params):
        super(NPIUser, self)._populate(params)
        self.npi_program_id = params.get('npi_program', '')
        # TODO what if the program doesn't exist?
        self.npi_program = Records.npi.get(self.npi_program_id, None)
        self.program_name = "%s: %s" % \
            (self.npi_program_id, params.get('npi_program_name', ''))
        self.programs_by_id[self.npi_program_id] = self


class Score(object):
    """ Score container """

    def __init__(self, params):
        """ Always instantiated from a JSON-sourced dict """
        self.count = params.get('count', 0)
        self.count_outstanding = params.get('countOutstanding', 0)
        self.count_feedback = params.get('countFeedback', 0)
        self.count_monitored = params.get('countMonitored', 0)
        self.count_suspended = params.get('countSuspended', 0)
        
        self.days_left = int(params.get('daysLeft', 0))
        self.days_left_positive = self.days_left > 0
        self.multiplier = params.get('multiplier', 0)
        self.score = params.get('score', 0)
        self.record_numbers = params.get('recordNumbers', [])
        self.record_numbers_feedback = params.get('recordNumbersFeedback', [])
        self.record_numbers_monitored = params.get('recordNumbersMonitored', [])
        self.record_numbers_suspended = params.get('recordNumbersSuspended', [])
        self.record_numbers_outstanding = params.get('recordNumbersOutstanding', [])
        self.rule_id = params.get('ruleId', '')
        self.rule = Rule.all.get(self.rule_id)
        self.pr_special_count = {}
        self.pr_special_record = {}
        self.cvbc_count = {}
        self.cvbc_record = {}
        
        self.req_special_count = {}
        self.req_special_record = {}
        
        self.records_by_MRs = params.get('records_by_MR', {})
        self.problem_levels = params.get('problemLevels', {})
        self.found_during_values = params.get('fdValues', {})

        # Found-during PRs filter counts
        self.fd_count = 0
        self.fd_count_feedback = 0 
        self.fd_count_monitored = 0  
        self.fd_count_suspended = 0 
        self.fd_count_outstanding = 0 
        self.fd_count_InReported_in = 0
        self.fd_count_Backlog = 0 

        self.fd_record_numbers = []
        self.fd_record_numbers_feedback = []
        self.fd_record_numbers_monitored = [] 
        self.fd_record_numbers_suspended = []
        self.fd_record_numbers_outstanding = []
        self.fd_record_numbers_InReported_in = [] 
        self.fd_record_numbers_Backlog = []

        self.other_scores = params.get('otherScores', {})
        """
        Retrieve a Map from JSON
        """
        def retrieve_data(key_value, obj_source, list_type = "count", add_pseudo_all = False):
            retval = {}
            if list_type == "count":
                total_all = 0
            else:
                total_all = []
                
            data_temp = obj_source.get(key_value, {})
            try:
                for special_type, value in data_temp.iteritems():
                    retval[special_type] = value
                    if add_pseudo_all and list_type == "count":
                        total_all += value
                    if add_pseudo_all and list_type == "record":
                        total_all.extend(value)
                # adding key for 'all' if needed
                if add_pseudo_all :
                    retval['all'] = total_all
            except (TypeError, AttributeError):
                # since this counting in 'special cases' so 
                #   there is big possibility that not everyone
                #   has this score
                pass
            return retval
        
        # Special cases of PR counting
        if (self.rule.counts == 'PRS' or self.rule.counts == 'CLOSEDPRS' or self.rule.counts == 'NEWLYINPRS') :
            if self.rule.release_based :
                """
                As of now (12/15/2010), the needs for special record is only for
                PR bug fix reporting which always be in release_based type of rule
                """
                self.pr_special_count = retrieve_data('PRspecial',params)
                self.pr_special_record = retrieve_data('PRspecialrecord',params)

            # look for the CVBC Records
            self.cvbc_count = retrieve_data('CVBCcount', params, 'count', True)
            self.cvbc_record = retrieve_data('CVBCRecord', params, 'record', True)
            
        self.release_id = params.get('groupingId', None)
        # add another method to identify the release_type
        # it will be empty for non CDM
        self.release_is_non_cdm = True
        self.release_type = ""
        self.display_type = False
        # FIXME.... this should be done automatically from JSON in java
        try:
            rls = Records.release[self.release_id]
            self.release_type = rls['release_type']
            if self.release_type == "R1" :
                self.release_type = self.release_id
        except KeyError:
            pass
        # flags for making sure we only displaying release type in UI
        self._id_hash = None
        # flags for displaying folding rules
        self.release_group = False
        # Add pre-defined calculated score for Matrix inreport and backlog
        self.record_numbers_InReported_in = self.get_special_record('pr','inreport_no')
        self.record_numbers_Backlog = self.get_special_record('pr','backlog_no')
        self.count_InReported_in = self.get_unique_count_record_for_type("inreport", "yes")
        self.count_Backlog = self.get_unique_count_record_for_type("backlog", "yes")

    def get_special_count(self, type, key):
        retval = 0
        try:
            if type == "pr" :
                retval = self.pr_special_count[key]
            if type == "cvbc":
                retval = self.cvbc_count[key]
        except KeyError:
            retval = 0
        return retval

    def get_special_record(self, type, key):
        retval = []
        try:
            if type == "pr" :
                retval = self.pr_special_record[key]
            if type == "cvbc" :
                retval = self.cvbc_record[key]
        except KeyError:
            retval = []
        return retval
    
    def set_special_record(self, type, key, new_value):
        try:
            if type == "pr":
                self.pr_special_record[key] = new_value
            if type == "cvbc":
                self.cvbc_record[key] = new_value
        except KeyError:
            pass

    def set_special_count(self, type, key, new_value):
        try:
            if type == "pr":
                self.pr_special_count[key] = new_value
            if type == "cvbc":
                self.cvbc_count[key] = new_value
        except KeyError:
            pass
    
    def count_for_type(self, typ):
        """ Return the count for the feedback type if this is a PR-counting
        rule, otherwise return self.count. """
        if self.rule.counts != 'PRS' and self.rule.counts !='CLOSEDPRS':
            return self.count
        if typ == 'backlog':
            return self.count_Backlog
        elif typ == 'inreport':
            return self.count_InReported_in
        elif typ == 'only':
            return self.count_feedback
        elif typ == 'no':
            return self.count_outstanding
        elif typ == 'all':
            _sum = self.count + self.count_monitored + self.count_suspended
            return _sum 
        elif typ == 'monitored':
            return self.count_monitored
        elif typ == 'suspended':
            return self.count_suspended
        else:
            return self.count

    def count_for_fd(self, typ):
        """ Return the count for the feedback type if this is a PR-counting
        rule, otherwise return self.count. """
        if self.rule.counts != 'PRS' and self.rule.counts !='CLOSEDPRS':
            return self.fd_count
        if typ == 'backlog':
            return self.fd_count_Backlog
        elif typ == 'inreport':
            return self.fd_count_InReported_in
        elif typ == 'only':
            return self.fd_count_feedback
        elif typ == 'no':
            return self.fd_count_outstanding
        elif typ == 'all':
            _sum = self.fd_count + self.fd_count_monitored + self.fd_count_suspended
            return _sum 
        elif typ == 'monitored':
            return self.fd_count_monitored
        elif typ == 'suspended':
            return self.fd_count_suspended
        else:
            return self.fd_count

    def records_for_type(self, typ):
        """ Return the record_numbers for the feedback type.
        FIXME
        THE BACKLOG count here not refering to backlog-pr in Backlog Page. 
          this is other term introduced for PR bug-fix 
         """
        if self.rule.counts != 'PRS' and self.rule.counts !='CLOSEDPRS':
            return self.record_numbers
        if typ == 'backlog':
            return self.record_numbers_Backlog
        elif typ == 'inreport':
            return self.record_numbers_InReported_in
        elif typ == 'only':
            return self.record_numbers_feedback
        elif typ == 'no':
            return self.record_numbers_outstanding
        elif typ == 'all':
            recnum = []
            recnum.extend(self.record_numbers)
            recnum.extend(self.record_numbers_suspended)
            recnum.extend(self.record_numbers_monitored)
            return recnum
        elif typ == 'monitored':
            return self.record_numbers_monitored
        elif typ == 'suspended':
            return self.record_numbers_suspended
        else:
            return self.record_numbers

    def records_for_fd(self, typ):
        """ Return the record_numbers for the found-during PRs for the given type.
         """
        if self.rule.counts != 'PRS' and self.rule.counts !='CLOSEDPRS':
            return self.fd_record_numbers
        if typ == 'backlog':
            return self.fd_record_numbers_Backlog
        elif typ == 'inreport':
            return self.fd_record_numbers_InReported_in
        elif typ == 'only':
            return self.fd_record_numbers_feedback
        elif typ == 'no':
            return self.fd_record_numbers_outstanding
        elif typ == 'all':
            recnum = []
            recnum.extend(self.fd_record_numbers)
            recnum.extend(self.fd_record_numbers_suspended)
            recnum.extend(self.fd_record_numbers_monitored)
            return recnum
        elif typ == 'monitored':
            return self.fd_record_numbers_monitored
        elif typ == 'suspended':
            return self.fd_record_numbers_suspended
        else:
            return self.fd_record_numbers

    def find_unique_count_from_list(self, record_lists):
        unique_recs = set()
        for recnum_plus_scope in record_lists:
            unique_recs.add(recnum_plus_scope.split('-')[0])
        count = len(unique_recs)
        return count
    
    def get_unique_count_record_from_list(self, record_lists, is_unique):
        if is_unique == "yes":
            return self.find_unique_count_from_list(record_lists)
        else:
            return len(record_lists)
            
    def get_unique_count_record_for_type(self,typ, is_unique):
        """ Return the CORRECT Unique record_numbers  """
        if self.rule.counts != 'PRS' and self.rule.counts !='CLOSEDPRS':
            return len(self.records_for_type(typ))
        else :
            # basically we will iterate all of the unique record number here
            # the problem over here is because although the count number is
            # unique, but it is not unique on the total (all + suspended + monitored)
            # so we use this 'hack' to calculate the unique count
            if is_unique == "yes" :
                recnum = self.records_for_type(typ)
                count = self.find_unique_count_from_list(recnum)
            else:
                count = len(self.records_for_type(typ))
            return count

    def copy_as_totals_score(self):
        """ Return a copy of this score, sanitized for use as a multiple release
        "totals" score.  All record_number lists are converted to sets, and
        ordering is lost.
        """
        tmp = deepcopy(self)
        tmp.days_left = 0
        tmp.multiplier = 0
        tmp.score = 0
        tmp.release_id = None
        tmp._id_hash = None
        suffixes = ['']
        if self.rule.counts == 'PRS':
            suffixes.extend(('_feedback', '_outstanding','_Backlog',
                             '_InReported_in', '_suspended','_monitored', ))
            
        for suffix in suffixes:
            attr = 'record_numbers%s' % suffix
            attr_count = 'count%s' % suffix
            fd_attr = 'fd_record_numbers%s' % suffix
            fd_attr_count = 'fd_count%s' % suffix
            my_recs = getattr(self, attr)
            my_fd_recs = getattr(self, fd_attr)
            my_recs_count = getattr(self, attr_count)
            my_fd_recs_count = getattr(self, fd_attr_count)
            if my_recs == None:
                setattr(tmp, attr, set([]))
            else:
                setattr(tmp, attr, set(my_recs))
            if my_fd_recs == None:
                setattr(tmp, fd_attr, set([]))
            else:
                setattr(tmp, fd_attr, set(my_fd_recs))

            setattr(tmp, attr_count, my_recs_count)
            setattr(tmp, fd_attr_count, my_fd_recs_count)
        return tmp

    def add_score(self, score):
        """ Add the records and count of the given score to this one, handling
        scopes.  Only works if self is a totals_score.
            Add the records and count for the found-during filters.
        """
        suffixes = ['']
        check_scopes = False
        if self.rule.counts == 'PRS':
            suffixes.extend(('_feedback', '_outstanding','_Backlog',
                             '_InReported_in', '_suspended','_monitored'))
            check_scopes = True
        for suffix in suffixes:
            attr = 'record_numbers%s' % suffix
            fd_attr = 'fd_record_numbers%s' % suffix
            my_recs = getattr(self, attr)
            my_fd_recs = getattr(self, fd_attr)
            their_recs = getattr(score, attr)
            tmp_fd_recs = getattr(score, fd_attr)
            combined_set = my_recs.union(their_recs)
            combined_fd_set = my_fd_recs.union(tmp_fd_recs)
            if check_scopes:
                unique_recs = set()
                unique_fd_recs = set()
                for recnum_plus_scope in combined_set:
                    unique_recs.add(recnum_plus_scope.split('-')[0])
                count = len(unique_recs)
                for rec_plus_scope in combined_fd_set:
                    unique_fd_recs.add(rec_plus_scope.split('-')[0])

                count_fd = len(unique_fd_recs)
            else:
                count = len(combined_set)
                count_fd = len(combined_fd_set)
            setattr(self, 'count%s' % suffix, count)
            setattr(self, attr, combined_set)
            setattr(self, 'fd_count%s' % suffix, count_fd)
            setattr(self, fd_attr, combined_fd_set)

    def _get_id_hash(self):
        """ Used to make html id's.  (relatively) expensive to compute,
        so it's a property.
        """
        if not self._id_hash:
            self._id_hash = hex((abs(hash('%s%d%s' %
                (self.rule_id, self.score, self.release_id)))))
        return self._id_hash
    id_hash = property(_get_id_hash)

    def __cmp__(self, other):
        """ Scores sort by score descending, then by days_left ascending. """
        if isinstance(other, self.__class__):
            return cmp(other.score, self.score) or \
                cmp(self.days_left, other.days_left)
        else:
            return cmp(self.__str__, other.__str__)

    def __repr__(self):
        return '<dash.Score rule: %s, count: %d>' % (self.rule_id, self.count)

    def __str__(self):
        return 'Score for rule %s, count: %d' % (self.rule_id, self.count)


class Status(object):
    """ (Release) status container """

    def __init__(self, params):
        """ Always instantiated from a JSON-sourced dict """
        self.id = params.get('username', '')
        self.total_score = params.get('totalScore', 0)
        self.last_scored = params.get('lastScored', 0)
        self.release_id = params.get('groupingName', None)
        self.scores, self.scores_by_rule_id = \
            _vivify_scores(params.get('scores', []))

    def __cmp__(self, other):
        """ Statuses sort by total_score descending, then by username. """
        if hasattr(other, 'total_score'):
            return cmp(other.total_score, self.total_score) or \
                cmp(self.id, getattr(other, 'id', 0))
        else:
            return cmp(self.__str__, other.__str__)

    def __repr__(self):
        return '<dash.Status release: %s, %d scores>' % \
            (self.release_id, len(self.scores))


class Objective(JsonSourced):

    all = {}
    current_ids = {}
    ordered = []

    def _populate(self, params):
        super(self.__class__, self)._populate(params)
        self.id = params.get('id', '')
        self.name = params.get('name', '')
        self.description = params.get('description', '')
        self.sort_key = params.get('sort_key', 0)

    def __cmp__(self, other):
        """ Sort by sort_key. """
        if hasattr(other, 'sort_key'):
            return cmp(self.sort_key, other.sort_key)
        else:
            return cmp(self.__str__, other.__str__)

    def __repr__(self):
        return '<dash.Objective %s>' % self.id


class Milestone(JsonSourced):

    all = {}
    current_ids = {}
    ordered = []

    def _populate(self, params):
        super(self.__class__, self)._populate(params)
        self.id = params.get('id', '')
        self.name = params.get('name', '')
        self.big_window = params.get('big_window', False)
        self.npi_phase = params.get('npi_phase', False)
        self.sort_key = params.get('sort_key', 0)

    def __cmp__(self, other):
        """ Sort by sort_key. """
        if hasattr(other, 'sort_key'):
            return cmp(self.sort_key, other.sort_key)
        else:
            return cmp(self.__str__, other.__str__)

    def __repr__(self):
        return '<dash.Milestone %s>' % self.id


class Rule(JsonSourced):

    all = {}
    current_ids = {}

    types = [
        ('PRS', 'PRs'),
        ('RLIS', 'RLIs'),
        ('BTS', 'BTs'),
        ('REVIEWS', 'Code Reviews'),
        ('CRSS', 'CRS Requests'),
        ('NPIS', 'NPI Programs'),
        ('NOTHING', 'Administrative Rules'),
        ('CLOSEDPRS','ClosedPRs'),
        ('HRCLOSEPRS','HardeningClosedPRs'),
        ('REQS','Reqs'),
        ('NEWLYINPRS','Newly Introduced PRs'),
 
    ]

    def _populate(self, params):
        super(self.__class__, self)._populate(params)
        self.name = params.get('name', '')
        self.active = params.get('active', True)
        self.columns = params.get('columns', [])
        self.counts = params.get('counts', None)
        self.description = params.get('description', '')
        self.release_based = params.get('groupingBased', True)
        self.horizon = params.get('horizon', 0)
        updated = params.get('lastUpdated', None)
        if updated:
            self.last_updated = datetime.datetime.fromtimestamp(updated / 1000)
        else:
            self.last_updated = None
        self.lede = params.get('lede', '')
        self.lhs = params.get('lhs', '')
        self.milestone = Milestone.all.get(params.get('milestone', ''))
        self.objective = Objective.all.get(params.get('objective', ''))
        self.owner = params.get('owner', '')
        self.sunset_horizon = params.get('sunsetHorizon', 0)
        self.sunset_milestone = \
            Milestone.all.get(params.get('sunsetMilestone', ''))
        self.weight = params.get('weight', 1)
        self.agenda_group = params.get('agendaGroup','')

    def __repr__(self):
        return '<dash.Rule %s>' % self.id

    def __cmp__(self, other):
        """ Rules sort by counts, milestone, horizon and name. """
        if isinstance(other, self.__class__):
            return cmp(self.counts, other.counts) or \
                cmp(self.milestone, other.milestone) or \
                cmp(self.horizon, other.horizon) or \
                cmp(self.name, other.name)
        else:
            return cmp(self.__str__, other.__str__)
        

class DataSource(JsonSourced):
    """ One of the data sources that feed the Dashboard """

    all = {}
    current_ids = {}

    # FIXME get this list dynamically from the Misc pseudo-class
    # FIXME DataSource name, Rule.Counts and Record subclass name need to
    # be aligned, so that we can easily navigate from one to the other.
    classes = ['RLI', 'NPI', 'Releases', 'Gnats', 'UserSource',
               'CategoryContacts', 'ReviewTracker', 'CRSTracker', 'UndeadCvbcs',
               'ReviewGroups', 'GnatsClosed', 'HistoryGnats', 'Requirements',
               'BranchTracker', 'ReleasePortal', 'ReleaseHardening', 'HardeningClosed','GnatsNewlyInFix']

    def __init__(self, id):
        self.id = id
        setattr(self.__class__, id, self)
        self.stub = True

    def _populate(self, params):
        super(self.__class__, self)._populate(params)
        self.name = self.id
        self.record_type = params.get('recordType', '')
        self.type = params.get('type', '')
        self.host = params.get('host', '')
        self.path = params.get('path', '')
        self.record_singular = params.get('recordSingular', '')
        self.record_plural = params.get('recordPlural', '')
        self.last_record_change_time = params.get('lastRecordChangeTime', 0)

    def __repr__(self):
        return '<dash.DataSource %s>' % self.name


class Misc(object):

    minutes_per_cycle = 0
    first_percent = 0
    second_percent = 0
    second_percent_multiplier = 0
    third_percent = 0
    third_percent_multiplier = 0
    final_percent_multiplier = 0
    days_after_period = 0
    second_period_multiplier = 0
    third_period_multiplier = 0
    final_period_multiplier = 0
    pr_query_expression = 0
    everyone_else_count = 0
    manager_count = 0
    future_backlog_releases = 0
    current_backlog_releases = 0
    pr_fix_releases= []
    pr_fix_rules_openPR= []
    pr_fix_rules_closedPR = []

    @classmethod
    def populate(cls, params):
        cls.minutes_per_cycle = params.get('minutes_per_cycle', 0)
        cls.first_percent = params.get('first_percent', 0)
        cls.second_percent = params.get('second_percent', 0)
        cls.second_percent_multiplier = params.get('second_percent_multiplier', 0)
        cls.third_percent = params.get('third_percent', 0)
        cls.third_percent_multiplier = params.get('third_percent_multiplier', 0)
        cls.final_percent_multiplier = params.get('final_percent_multiplier', 0)
        cls.days_after_period = params.get('days_after_period', 0)
        cls.second_period_multiplier = params.get('second_period_multiplier', 0)
        cls.third_period_multiplier = params.get('third_period_multiplier', 0)
        cls.final_period_multiplier = params.get('final_period_multiplier', 0)
        cls.pr_query_expression = params.get('pr_query_expression', 0)
        cls.everyone_else_count = params.get('everyone_else_count',0)
        cls.manager_count = params.get('manager_count',0)
        cls.future_backlog_releases = params.get('future_backlog_releases',0)
        cls.current_backlog_releases = params.get('current_backlog_releases',0)
        cls.pr_fix_releases = params.get('pr_fix_releases','11.1').split(',')
        cls.pr_fix_rules_openPR = params.get('pr_fix_rules_openpr','regression_prs_fix,major_prs_fix,critical_prs_fix').split(',')
        cls.pr_fix_rules_closedPR = params.get('pr_fix_rules_closedpr','closed_regression_prs,major_closed_pr,closed_critical_prs').split(',')


class Event(object):
    """ An event in the release or npi schedule.

    TODO Release events in the "upcoming" display should link to the user's
    release agenda, and NPI events should link to that NPI pseudo-user's agenda.
    """

    def __init__(self, name, details, event, date, epoch, source, disp_name):
        self.name = name
        self.details = details
        # Event comes as a field name, turn it into human-readable text
        self.event = event.title().replace('_', ' ')
        self.date = date
        self.epoch = epoch / 1000
        until = int(round((self.epoch - now()) / 86400))
        self.days = until == 1 and '1 Day' or '%d Days' % until
        self.source = source
        self.disp_name = disp_name

    def __str__(self):
        return '%s %s: %s' % (self.name, self.event, self.date)

    def __repr__(self):
        return '<dash.Event %s %s: %s>' % (self.name, self.event, self.date)

    def __cmp__(self, other):
        """ Sort by epoch ascending, then by name and event descending. """
        if hasattr(other, 'epoch'):
            return cmp(self.epoch, other.epoch) or \
                cmp(getattr(other, 'name', ''), self.name) or \
                cmp(getattr(other, 'event', ''), self.event)
        else:
            return cmp(self.__str__, other.__str__)


class Records(object):

    release = {}
    npi = {}
    pr = {}
    rli = {}
    review = {}
    crs = {}
    closedpr = {}
    bt = {}
    req = {}

    schedule = []
    active_releases = []
    in_flight_releases = []
    grounded_releases = []
    deployed_releases = []
    sort_deployed_releases = []
    # Settings for PR Bug Fix related
    # Removing 10.2 as of 9/11/2010 -- RLI-1651
    pr_fix_releases = []
    pr_fix_rules_openPR = []
    pr_fix_rules_closedPR = []
    

    @classmethod
    def get_summary(cls, counts, recnum):
        """ Given a Rule Counts value and a record number, return the summary
        for the record in the corresponding DataSource.

        TODO This is an ungainly hack to get around some legacy issues in the
        codebase.  Simplifcation of other code is in order...
        """
        # Counts is generally upper case plural of record type
        rectype = counts[:-1].lower()
        rec_dict = getattr(cls, rectype, None)
        if not rec_dict:
            raise DashError('Unknown Counts type "%s" requested.' % counts)
        rec_or_summary = rec_dict.get(recnum, None)
        if isinstance(rec_or_summary, dict):
            return rec_or_summary.get('summary', None)
        else:
            return rec_or_summary

      
    @classmethod
    def process_dates(cls):
        """ Gather and munge date data from release and NPI records:

        For each *_epoch field, add a field "*_past" indicating whether the
        date is in the past.

        Collect all dates in an ordered list of events, so we can display a
        schedule.

        Build lists of active and in-flight releases.
        """
        schedule = []
        active_releases = []
        in_flight_releases = []
        grounded_releases = []
        local_deployed_releases = []
        # "now" is actually 12 hours from now, so that things that happened
        # today will not be considered to be in the past.  Also, epoch times
        # in records are in millis, so multiply by 1000
        right_now = (now() + 12 * 60 * 60) * 1000
        for typ in ('release', 'npi'):
            records = getattr(cls, typ)
            if len(records.values())> 0:
                # Only goes here if there are some values in the records
                date_fields = [fname[:-6] for fname in records.values()[0].keys()
                               if fname.endswith('_epoch') and
                               fname.find('last_modified') == -1]
            for record in records.values():
                name = record['id']
                if typ == 'release':
                    # in the release field for CDM , we will have M.nIBx
                    # all of the IB branch releases should NOT visible in UI stand point
                    # but should be still accessible in the rule etc
                    if record['active']:
                        is_cdm = False;
                        numeric_release_value = findNumericRelease(record['relname'])
                        # For cdmv1 release use pre_ibrg_one_epoch value
                        # For cdmv2 release use ibfrs_epoch value
                        dt_field = 'pre_ibrg_one_epoch';
                        if (float(numeric_release_value) > cdm_v2_release_cutoff):
                            dt_field = 'ibfrs_epoch';
                        if (float(numeric_release_value) >= cdm_release_cutoff):
                            is_cdm = True
                            name = numeric_release_value
                        # FOR CDM.. check based on the ib1_pre_ibrg_one or ibfrs_epoch
                        # need to check existance of the release name, since 10.3IB1 
                        # considered as 10.3
                        if is_cdm:
                            if record[dt_field] < right_now + 90 * 86400000 and \
                                    record['eol_epoch'] > right_now + 90 * 86400000:
                                if name not in in_flight_releases:
                                    in_flight_releases.append(name)
                            else:
                                if name not in grounded_releases:
                                    grounded_releases.append(name)
                        else:
                            if record['p1_exit_epoch'] < right_now + 15 * 86400000 and \
                                    record['eol_epoch'] > right_now + 90 * 86400000:
                                in_flight_releases.append(name)
                            else:
                                grounded_releases.append(name)
                                
                        if name not in active_releases:
                            active_releases.append(name)
                        
                    details = name
                    # look on Rn and Sn
                    for i in range(1,31):
                        if i <= MAX_R_RELEASE :
                            if record['r%s_deploy_type' % (i)] == "actual" and \
                                record['r%s_deploy_epoch' % (i)] > 0 :
                                local_deployed_releases.append({'%sr%s' % (name,i) :{'type':"r%s"%(i),
                                                                                     'epoch':get_epoch(record['r%s_deploy_string' % (i)]),
                                                                                     'release_branch_epoch' : get_epoch(record['r%s_release_branch_string' % (i)]),
                                                                                     'release_branch_type' : record['r%s_release_branch_type' % (i)],
                                                                                     'relname':name}}) 
                        if record['s%s_deploy_type' % (i)] == "actual" and \
                            record['s%s_deploy_epoch' % (i)] > 0 :
                            local_deployed_releases.append({'%ss%s' % (name,i) :{'type':"s%s"%(i),
                                                                                 'release_branch_epoch' : get_epoch(record['s%s_release_branch_string' % (i)]),
                                                                                 'release_branch_type' : record['s%s_release_branch_type' % (i)],
                                                                                 'epoch':get_epoch(record['s%s_deploy_string' % (i)]),
                                                                                 'relname':name}}) 
                        
                    
                    # Add-in field to identify whether this release/branch is collapsed/closed
                    is_branch_collapsed = False
                    if record['state'] == "closed-eod":
                        is_branch_collapsed = True
                    record['is_branch_collapsed'] = is_branch_collapsed
                    
                    # make the childBranch_string into Array
                    temp_child = record['childBranch_string']
                    # removing the [ ] in the string
                    temp_child = re.sub('[\[\]]', '', temp_child)
                    # removing some whitespaces between comma
                    temp_child = re.sub('\s','',temp_child)
                    record['childBranch_array'] = temp_child.split(',')
                    record['childBranch_array'].sort()
                else:
                    details = record['synopsis']

                for fname in date_fields:
                    val_string = record.get(fname + '_string', None)
                    val_epoch = record.get(fname + '_epoch', None)
                   
                    #Fix for RLI 1968
                    #Adding the logic to determine if is_past is False or True
                    is_past = False
                    if val_epoch:
                        is_past = val_epoch < right_now

                    # Add a pseudo field indicating if this date is past
                    record[fname + '_past'] = is_past

                    # Build the schedule
                    if not is_past and val_epoch:
                        disp_name= name
                        _name = name
                        if typ=="release":
                            # adding release type for non R1 release
                            if record['release_type'] != "R1":
                                major, minor = [int(p) for p in name.split('.')]
                                if major >= 11 and minor >= 3:                                    
                                    disp_name = record['relname']
                                    _name = record['relname']
                                else:
                                    disp_name = name + " " + record['release_type']
                                    _name = name + record['release_type']
                                                                    
                        schedule.append(Event(_name, details, fname,
                                              val_string, val_epoch, typ,disp_name))
                
        schedule.sort()
        active_releases.sort(lambda x, y: cmp(float(y), float(x)))
        in_flight_releases.sort(lambda x, y: cmp(float(y), float(x)))
        grounded_releases.sort(lambda x, y: cmp(float(y), float(x)))
        # sort the deploy epoch based on the timeline (lowest first)
        # only put it in array because of we need the sorting method
        local_deployed_releases.sort(lambda x, y: cmp(float(x.values()[0]['epoch']), float(y.values()[0]['epoch'])))
        
        cls.schedule = schedule
        cls.active_releases = active_releases
        cls.grounded_releases = grounded_releases
        cls.in_flight_releases = in_flight_releases
        # PR-fix related configuration
        cls.pr_fix_releases = Misc.pr_fix_releases
        cls.pr_fix_rules_openPR = Misc.pr_fix_rules_openPR
        cls.pr_fix_rules_closedPR = Misc.pr_fix_rules_closedPR

        # for lookup
        for dict_items in local_deployed_releases:
            for k in dict_items.iterkeys():
                cls.deployed_releases.append(k)
        
        cls.sort_deployed_releases = local_deployed_releases


def _vivify_scores(score_dict_list):
    """ Make Score objects fromt the supplied dict, and populate the scores
    and scores_by_rule_id attrs of the container.
    """
    scores = []
    scores_by_rule_id = {}
    for dict_ in score_dict_list:
        score = Score(dict_)
        scores.append(score)
        scores_by_rule_id[score.rule_id] = score
        if dict_['ruleId'] == RULE_NAME:
            for pl_rule in PL_FILTERS:
                dict_['ruleId'] = pl_rule
                score =  Score(dict_)
                scores.append(score)
                scores_by_rule_id[pl_rule] = score
    scores.sort()
    return scores, scores_by_rule_id

def _preprocessing_reviews(score_obj):
    """ Filter the unique review records count and populate the scores."""

    if score_obj.rule.counts != 'REVIEWS': return score_obj
    records = score_obj.record_numbers
    rev_recs = []

    # Collect the unique review ids without review scopes
    for rec in records:
        if rec.split('-')[0] in Records.review and \
            rec.split('-')[0] not in rev_recs: 
            rev_recs.append(rec.split('-')[0])

    try:
        setattr(score_obj, 'record_numbers', rev_recs)
        setattr(score_obj, 'count', len(rev_recs))
    except:
        score_obj.record_numbers = []
        score_obj.count = 0

    return score_obj 

def _preprocessing_plfilters(score_obj):
    """ Filter and count the problem level PRs and populate the scores."""
    # Preprocessing problem level rules
    suffixes = []
    suffixes.extend(('_feedback', '_outstanding','_Backlog', 
                     '_InReported_in', '_suspended','_monitored'))

    if score_obj.rule_id not in PL_FILTERS:
        return score_obj

    if score_obj.problem_levels.has_key(PL_MAPS[score_obj.rule_id]):
        pl_rec_nums = list(set(score_obj.record_numbers) & \
                            set(score_obj.problem_levels[PL_MAPS[score_obj.rule_id]]))
        score_obj.record_numbers = pl_rec_nums 
        score_obj.count = len(set(map(lambda l: l.split('-')[0], pl_rec_nums)))
        for suffix in suffixes:
            rec_attr = 'record_numbers%s' % suffix
            rec_cnt = 'count%s' % suffix
            pl_rec_attr = list(set(getattr(score_obj, rec_attr)) & \
                               set(score_obj.problem_levels[PL_MAPS[score_obj.rule_id]]))
            setattr(score_obj, rec_attr, pl_rec_attr)
            setattr(score_obj, rec_cnt, len(set(map(lambda l: l.split('-')[0], pl_rec_attr))))
    else:
        score_obj.record_numbers = []
        score_obj.count = 0
        for suffix in suffixes:
            rec_attr = 'record_numbers%s' % suffix
            rec_cnt = 'count%s' % suffix
            setattr(score_obj, rec_attr, [])
            setattr(score_obj, rec_cnt, 0)

    return score_obj

def _preprocessing_fd_scores(score_obj):

    """ Filter and count the found during PRs and populate the scores."""
    # Preprocessing problem level rules
    suffixes = []
    suffixes.extend(('_feedback', '_outstanding','_Backlog', 
                     '_InReported_in', '_suspended','_monitored'))

    if score_obj.rule.counts != 'PRS': return score_obj

    records = []
    if score_obj.rule_id not in PL_FILTERS:
        for k in score_obj.found_during_values.keys():
            records.extend(score_obj.found_during_values[k])
    else:
        key = PL_MAPS[score_obj.rule_id] + '_fdvals'
        if score_obj.found_during_values.has_key(key):
            records = score_obj.found_during_values[key]
        else:
            score_obj.fd_record_numbers = []
            score_obj.fd_count = 0
            for suffix in suffixes:
                rec_attr = 'fd_record_numbers%s' % suffix
                rec_cnt = 'fd_count%s' % suffix
                setattr(score_obj, rec_attr, [])
                setattr(score_obj, rec_cnt, 0)
            return score_obj
            
    pl_rec_nums = list(set(score_obj.record_numbers) & set(records))
    score_obj.fd_record_numbers = pl_rec_nums 
    score_obj.fd_count = len(set(map(lambda l: l.split('-')[0], pl_rec_nums)))

    for suffix in suffixes:
        act_attr = 'record_numbers%s' % suffix
        rec_attr = 'fd_record_numbers%s' % suffix
        rec_cnt = 'fd_count%s' % suffix
        pl_rec_attr = list(set(getattr(score_obj, act_attr)) & set(records))
        setattr(score_obj, rec_attr, pl_rec_attr)
        setattr(score_obj, rec_cnt, len(set(map(lambda l: l.split('-')[0], pl_rec_attr))))

    return score_obj

def parse_json(file_name):
    """ Called on every sub-object in a JSON stream. """
    # the sleeps are needed to allow apache still process requests while processing
    # otherwise the CPU bound process will dominate the python GIL
    # see http://www.dabeaz.com/python/GIL.pdf for some more info as to why this happens
    start = time.time()
    times = [start]
    obj_list = cjson.decode(open(file_name).read())
    times.append(time.time())
    logging.debug('%s : %s seconds : json parsed' % (file_name, times[-1]-times[-2]))
    if times[-1]-times[-2] > 1:
        time.sleep(.1)
    count = 0
    for obj in obj_list:
        count += 1
        obj_class = obj.get('class', '')
        if obj_class == 'net.juniper.dash.data.User':
            User.find_or_create(obj['name'], obj)
        elif obj_class == 'net.juniper.dash.data.NPIUser':
            NPIUser.find_or_create(obj['name'], obj)
        elif obj_class == 'net.juniper.dash.Rule':
            # Adding found-during field in query fields
            if obj['counts'] == 'PRS' and \
                 'found-during' not in obj['columns']:
                obj['columns'].insert(
                   obj['columns'].index('problem-level') + 1, 'found-during')
            Rule.find_or_create(obj['identifier'], obj)
            if obj['identifier'] == RULE_NAME:
                for pl_rule in PL_FILTERS:
                    obj['identifier'] = pl_rule.lower() 
                    obj['name'] = pl_rule 
                    Rule.find_or_create(obj['identifier'], obj)
        elif obj_class == 'net.juniper.dash.Milestone':
            Milestone.find_or_create(obj['id'], obj)
        elif obj_class == 'net.juniper.dash.Objective':
            Objective.find_or_create(obj['id'], obj)
        elif obj_class.split('.')[-1] in DataSource.classes:
            DataSource.find_or_create(obj_class.split('.')[-1], obj)
        elif obj_class == 'net.juniper.dash.Misc':
            Misc.populate(obj)
        else:
            raise DashError('Unable to handle JSON object of class "%s".' %
                            obj_class)
        if count % 50 == 0:
            time.sleep(.1)
    times.append(time.time())
    logging.debug('%s : %s seconds : %s objects' % (file_name, times[-1]-times[-2], count))
    logging.debug('%s : %s seconds/object' % (file_name, (times[-1]-times[-2])/count))
def _load_record_data(dirname):
    """ Load record data.  Split out for ease of testing. """
#    ==PPM==.... UNCOMMENT LINE BELOW FOR PPM PROJECT
#    for typ in 'release npi pr rli review bt hardening req'.split():
    for typ in 'release npi pr rli review crs bt hardening hardeningclosed'.split():
        fname = os.path.join(dirname, typ + '.json')
        if typ == 'review':
            # Filter the latest updated review records
            for id, summ in cjson.decode(open(fname).read()).iteritems():
                if summ is None: continue
                if not Records.review.has_key(id.split('-')[0]):
                    Records.review[id.split('-')[0]] = summ
                else:
                    # Filter the latest modified date from 
                    # the unique review records
                    try:
                        prev_summ = Records.review[id.split('-')[0]][1]
                        prev_dt = datetime.datetime.strptime(
                                                 prev_summ, "%Y-%m-%d")
                        cur_dt = datetime.datetime.strptime(summ[1], "%Y-%m-%d")
                        if prev_dt.date() < cur_dt.date():
                            Records.review[id.split('-')[0]] = summ
                    except:
                        pass

        else:
            setattr(Records, typ, cjson.decode(open(fname).read()))
    Records.process_dates()

def load_all(dirname):
    """ Load saved JSON data from a local directory. """
    global JSON_LOADED
    global LOAD_COUNT
    global PERCENT_LOADED
    global LOAD_TRACEBACK
    LOAD_TRACEBACK = ''
    # Switch off the gc only on json parsing/loading 
    gc.disable()
    try:
        # -- example --
        # uncomment the next two lines if you want to skip loading json data, but make it look
        # like the load was all good
        if getattr(settings,"SKIP_JSON_LOAD", False) :
            JSON_LOADED = True
            return
        # -- end example --
        # see parse_json as to why time.sleeps are scattered throughout 
        start = time.time()
        times = [start]
        # increment the load counter
        LOAD_COUNTER_LOCK.acquire()
        LOAD_COUNT += 1
        run = LOAD_COUNT
        LOAD_COUNTER_LOCK.release()
    
        logging.debug('#%s : waiting for lock, going to load from %s' % (run,dirname))
    
        # because we are working with global classes, we want to ensure we don't try and 
        # do more than one load at a time.
        LOADING_LOCK.acquire()
        PERCENT_LOADED = 1
        ProxyHelper.set_instance_loading(settings.MODE, PERCENT_LOADED)
        
        times.append(time.time())
        logging.info('#%s, %s : %.3f seconds : got lock' % (run, len(times)-1, times[-1]-times[-2]))

        load_path = os.path.join('%stmp' % os.sep, '%s-%s' % (settings.MODE, time.time()))
        try:
            logging.debug('#%s: copying from %s to %s' % (run, dirname, load_path))
                
            # clear out anything that exists
            if os.path.exists(load_path):
                shutil.rmtree(load_path)
            os.makedirs(load_path)
            for filename in [f for f in os.listdir(dirname) if f.lower().endswith('.json')]:
                shutil.copyfile(os.path.join(dirname, filename), os.path.join(load_path, filename))
            # dirname is already used everywhere, so just overwrite it
            dirname = load_path
            logging.debug('#%s: finsihed copy' % (run))        
            logging.debug('#%s : in try' % (run))
            
            JsonSourced.clear_all_subclass_current_ids()
            # Load helper classes
            for typ in 'misc sources milestones objectives rules'.split():
                parse_json(os.path.join(dirname, typ + '.json'))
            for cls in (Milestone, Objective):
                v = cls.all.values()
                v.sort()
                cls.ordered = v
            
            _load_record_data(dirname)
            time.sleep(.1)
            # Clear out any objects that have disappeared upstream
            for cls in (Rule, Milestone, DataSource, Objective):
                cls.clear_deleted_objects()
        
            times.append(time.time())
            logging.debug('#%s, %s : %s seconds : helpers loaded' % (run, len(times)-1, times[-1]-times[-2]))
    
            # Load User data
            for typ in 'all-managers all-virtual-teams all-pseudos everyone-else'.split():
                parse_json(os.path.join(dirname, typ + '.json'))
                times.append(time.time())
                logging.debug('#%s, %s : %s seconds : user load 1 iter' % (run, len(times)-1, times[-1]-times[-2]))
                # each loop is about 12.5% of the time
                PERCENT_LOADED += 12.5
                ProxyHelper.set_instance_loading(settings.MODE, PERCENT_LOADED)
            times.append(time.time())
            logging.debug('#%s, %s : %s seconds : user load 1' % (run, len(times)-1, times[-1]-times[-2]))
        
            # Loading parts of file
            def load_in_iteration(obj_counter, file_name, total_percent_increase, current_percentage):
                # check on other sets of everyone-else, we captured it from the Misc data sources
                percentage_counter = current_percentage
                counter = obj_counter
                if counter > 0:
                    percent_increase = total_percent_increase / obj_counter
                    for i in range(1,counter+1):
                        parse_json(os.path.join(dirname,'%s%d'%(file_name, i)+'.json'))
                        times.append(time.time())
                        logging.debug('#%s, %s : %s seconds : %s load 2 iter' % (run, len(times)-1, times[-1]-times[-2], file_name))
                        percentage_counter += percent_increase
                        ProxyHelper.set_instance_loading(settings.MODE, percentage_counter)
                else:
                    # and if no looping, then it's done!
                    percentage_counter += total_percent_increase
                    ProxyHelper.set_instance_loading(settings.MODE, percentage_counter)
                times.append(time.time())
                logging.debug('#%s, %s : %s seconds : %s load 2' % (run, len(times)-1, times[-1]-times[-2], file_name))
                return percentage_counter

            # check on other sets of Manager, we captured it from the Misc data sources
            PERCENT_LOADED = load_in_iteration(Misc.manager_count, "all-managers", 23.0, PERCENT_LOADED)
            # check on other sets of everyone-else, we captured it from the Misc data sources
            PERCENT_LOADED = load_in_iteration(Misc.everyone_else_count, "everyone-else", 23.0 ,PERCENT_LOADED)
            
            # Clear out any users that have disappeared upstream
            # XXX??? Note that doing it this way, instead of reading a list of usernames,
            # means that any stub users will be deleted.  This is probably a feature,
            # but the only way they get in the system is by being someone's report...
            User.clear_deleted_objects()
        
            # sort the Users' reports and the NPI programs
            bu_team = User.all['junos'].direct_reports
            # fill up the BU_BG Tree
            User.construct_BG_tree()
            # get the Users which not belong to any BU's and assign their bus_memberships
            for user in User.all.values():
                user.clean_up_bus_memberships()
                user.sort_reports()
                user.additional_preprocessing()
    
            times.append(time.time())
            logging.debug('#%s, %s : %s seconds: users processed' % (run, len(times)-1, times[-1]-times[-2]))

        
            NPIUser.sort_programs()
            PERCENT_LOADED = 100
            ProxyHelper.set_instance_loading(settings.MODE, PERCENT_LOADED)
            JSON_LOADED = True
        finally:
            #always release the lock even if something went wrong
            LOADING_LOCK.release()
            # loding lock must always be cleared
            ProxyHelper.clear_instance_loading(settings.MODE)
            try:
                # clear out loading dir
                shutil.rmtree(load_path)
            except Exception as e:
                logging.info('could not delete temp loding directory %s, error: %s' % (load_path, e))
        times.append(time.time())
        logging.info('#%s : %s seconds from start: load finished' % (run, times[-1]-times[0]))
        total_time = times[-1]-times[0]
        for x in range(1, len(times)-1):
            logging.debug('LOAD_PERCENT for %s is %.2f' % (x, ((times[x]-times[0])/total_time) * 100))
    except:
        # grab the traceback to dispaly in error messages
        LOAD_TRACEBACK = traceback.format_exc()
        raise
    # Switch on the gc back after the json data load
    gc.enable()

def async_load_all(dirname=None):
    """calls load_all in a thread so this function returns right away"""
    if dirname is None:
        dirname = settings.JSON_PATH
    t = reloadNewJson(dirname=dirname)
    t.setDaemon(True)
    t.start()

def async_instance_load_all():
    """calls load_all in a thread so this function returns right away"""
    logging.debug('in async_instance_load_all')
    t = instanceReloadNewJson()
    t.setDaemon(True)
    t.start()

class reloadNewJson(threading.Thread):
    """ 
      Reloading new JSON can take up some time,
         dashboard system will not be responding since the 
         process is synchronous. So we use thread to make
         it asynchronous.  
         
      TODO...Need to play some flips or memcached on the User object to maintain
         data accuracy
    """     
    def __init__(self, dirname=None, *args, **kwargs):
        if dirname is None:
            dirname = settings.JSON_PATH
        self.dirname = dirname
        super(reloadNewJson, self).__init__(*args, **kwargs)
        
    def run(self):
        load_all(self.dirname)

class instanceReloadNewJson(threading.Thread):
    """ 
    Calling the reloading for instances JSON can take up some time,
    """     
    def run(self):
        global PROXY_LOAD_COUNT
        # only allow one request to queue up
        logging.debug('instanceReloadNewJson.run: start')
        done = False
        first = True
        can_wait = True
        queued = False        
        while not done:
            instance_name = ProxyHelper.set_loading()
            logging.debug('instanceReloadNewJson.run: instance_name=%s' % instance_name)
            if first:
                PROXY_COUNTER_LOCK.acquire()
                # if can't load right now, but have a slot
                if not instance_name and PROXY_LOAD_COUNT < 1:
                    # increase count
                    PROXY_LOAD_COUNT += 1
                    queued = True
                # we can't load now, and someone else is already waiting
                elif not instance_name and PROXY_LOAD_COUNT >= 1:
                    can_wait = False
                first = False
                PROXY_COUNTER_LOCK.release()
            # not going to try and load
            if not can_wait:
                logging.debug('too many request, ignoring PROXY_LOAD_COUNT=%s' % PROXY_LOAD_COUNT)
                return
    
            if instance_name:
                # only allow if no more
                host, port = settings.INSTANCE_INFO[instance_name]['host'] 
                url = 'http://%s:%s/dashboard/json-ready' % (host, port)
                result = urlopen(url).read()
                logging.debug('instanceReloadNewJson.run url=%s, result=%s' % (url, result))
                if queued:
                    PROXY_COUNTER_LOCK.acquire()
                    PROXY_LOAD_COUNT -= 1
                    PROXY_COUNTER_LOCK.release()
                done = True
                
            if not done:
                logging.debug('instanceReloadNewJson.run: no available instance to load to waiting.')
                time.sleep(10)
                
def backlog_pr_test():
    u = User.all['junos']
    sc= u.scores_by_rule_id['cvbc_current_backlog_prs']
    junos_rec = sc.records_for_type('all')
    User.cvbc_junos_rec = set(junos_rec)
    
    sum_rec = []
    c_sum_rec = []
    for i in u.direct_reports:
        if i.is_BG or i.id.split('_')[0]=="non" or i.id.split('_')[0]=="deprecated" or i.id.split('_')[0]=="empty":
            print >> sys.stderr, "calculate %s" % (i.id)
            try:
                sc = i.scores_by_rule_id['cvbc_current_backlog_prs']
                sum_rec.extend(sc.records_for_type('all'))
            except:
                print >> sys.stderr, "no CVBC backlog PRs for %s" % (i.id)
                sum_rec.extend([])
    User.cvbc_sum_rec = set(sum_rec)
    User.cvbc_diff_rec = [rec for rec in User.cvbc_junos_rec if rec not in User.cvbc_sum_rec]
    

BRANCH_RELEASE_PATTERN = re.compile('(?P<relname>((?P<rel_type1>IB[1-5]?)\_)?([1-9][0-9]?[\.\_][1-9]?[0-9])(?P<old_rel_type>IB[1-5])?((\_(?P<rel_type2>(\w+))?)\_BRANCH)?)')
NUMERIC_RELEASE_PATTERN = re.compile('(IB[1-5]?\_)?([1-9][0-9]?[\.\_][1-9]?[0-9])(\_BRANCH)?')
def findNumericRelease(rls):
    """
        Find the numeric release value from the given release (rls),
        it was designed to handle CDM and non CDM release M.n,
         M.nIB and IBx_m_n_char_BRANCH
    """
    matcher= NUMERIC_RELEASE_PATTERN.search(rls)
    if matcher:
        temp =  matcher.group(2)
        temp = re.sub('\_','.',temp)
        return temp
    else:
        return "0"

def findReleaseType_from(rls):
    """
        Find the Release type of certain release or branch, this method also
        compatible with the old branch format which is using 10.3IB1 instead
        IB1_10_3_BRANCH.
    """
    matcher = BRANCH_RELEASE_PATTERN.match(rls)
    if ('old_rel_type' in matcher.groupdict() and \
         matcher.group('old_rel_type') != None) :
        return matcher.group('old_rel_type')
    else:
        retval = ""
        if matcher.group('rel_type1') != None:
            retval += matcher.group('rel_type1')
        if matcher.group('rel_type2') != None:
            retval += matcher.group('rel_type2')
        return retval

def createCompatibleBranch(rls):
    """
        Based on the old branch format M.nIBx, the method will transform
        into the Branch Naming IBx_M_n_BRANCH
    """
    matcher = BRANCH_RELEASE_PATTERN.match(rls)
    if ('old_rel_type' in matcher.groupdict() and \
         matcher.group('old_rel_type') != None) :
        # there is old_release_type here so lets create their BRANCH TYPE
        MdotN = re.sub('\.','_',findNumericRelease(matcher.group('relname')))
        return "%s_%s_BRANCH" % (matcher.group('old_rel_type'), MdotN)                      
    else:
        return rls
    
def safe_path(path):
    """
    determine if path can be processed even if in a loading state
    path: patch trying to be accessed
    
    returns: True if it's unaffected by loading and False otherwise
    """
    # assume not safe
    result = False
    # media, admin and the json-ready request are ok
    if settings.MEDIA_URL.lower() in path or 'json-ready' in path or '/admin' in path or 'instance-debug' in path:
        result = True
    safe_ends = ['help','tour','help-rules','browser-reqs','bglist']
    for safe_end in safe_ends:
        if path.endswith(safe_end):
            result = True
            break
         
    return result

def get_epoch(dtime):
    retval = 0
    try:
        m = re.match(date_pattern,dtime)
        grp = [int(x) for x in m.groups()]
        t = datetime.date(*grp)
        retval = time.mktime(t.timetuple())
    except AttributeError:
        pass
    return retval
    
class ForceAlive(threading.Thread):
    """ 
      Reloading new JSON can take up some time,
         dashboard system will not be responding since the 
         process is synchronous. So we use thread to make
         it asynchronous.  
         
      TODO...Need to play some flips or memcached on the User object to maintain
         data accuracy
    """     
    def __init__(self, instance_name, host, *args, **kwargs):
        self.instance_name = instance_name
        self.host = host
        super(ForceAlive, self).__init__(*args, **kwargs)
        
    def run(self):
        alive = False
        while not alive:
            try:
                response = urlopen('http://%s:%s/%s/alive' % (self.host[0], self.host[1], settings.DASH_URL_BASE),timeout=10).read()
                if response == 'yes':
                    logging.info('%s is alive' % self.instance_name)
                    alive = True
            except Exception, e:
                logging.debug('ForceAlive.run: %s is not alive. error: %s' % (self.instance_name, str(e)))
            time.sleep(5)

class ProxyHelper(object):
    __current_instance = None
    __loading_instance = None
    __lock = Lock()
    
    @classmethod
    def confirm_instances_running(cls):
        """continue to ping processes until we are sure they are running"""
        # this is needed becuase force loading of processes in apache is problematic
        for key, value in settings.INSTANCE_INFO.items():
            t = ForceAlive(instance_name=key, host=value['host'])
            t.setDaemon(True)
            t.start()

        
    @classmethod
    def current_instance(cls, _lock=True):
        """
        _lock is for internal use only when in a method that has already gotten the needed lock
        return (hostname, port) for the instacne that should be used, first available if everything is loading
        """
        retvalue = None
        if _lock: cls.__lock.acquire()
        if cls.__current_instance is None:
            logging.debug('ProxyHelper.current_instance: no current instance')
            # check all lock files, return whichever one is not there or either of them if both are locked
            # to get loading message
            for key in settings.INSTANCE_INFO.keys():
                # if loading has completed
                loading, last_updated, percent_loaded = cls.instance_loading_info(key)
                # if the lock file has been around more than MAX_LOADING_TIME
                if loading and (datetime.datetime.now() - last_updated) > settings.MAX_LOADING_TIME:
                    # something likely went wrong and clear it
                    logging.info('lock file for %s has been around too long, clearing' % key)
                    cls.clear_instance_loading(key)
                if not cls.is_instance_loading(key):
                    logging.debug('ProxyHelper.current_instance: found that %s is not loading' % key)
                    # set the current instance
                    cls.__current_instance = key
                    retvalue = settings.INSTANCE_INFO[key]['host']
                    # don't go any further
                    break
            # if everything was loading
            if retvalue is None:
                # pick the first one to return to show the loadin messsge
                retvalue = settings.INSTANCE_INFO[settings.INSTANCE_INFO.keys()[0]]['host']
                logging.debug('ProxyHelper.current_instance: everything is loading, picked %s' % str(retvalue))
        else:
            logging.debug('ProxyHelper.current_instance: have a current instance')
            
            if cls.__loading_instance:
                logging.debug('ProxyHelper.current_instance: somthing is loading')
                
                # check if loading is complete
                # if complete, set new current_instane and unset loading_instance
                # if not, send current_instance
                if not cls.is_instance_loading(cls.__loading_instance):
                    # loading completed, switch instances
                    logging.debug('ProxyHelper.current_instance: loding instance has completed')
                    cls.__current_instance = cls.__loading_instance
                    # reset loading
                    cls.__loading_instance = None                    
            # alwasy return current instance after doing loading check
            retvalue = settings.INSTANCE_INFO[cls.__current_instance]['host']
            logging.debug('ProxyHelper.current_instance: now using instance retvalue=%s' % str(retvalue))
                
        if _lock: cls.__lock.release()
        logging.debug('ProxyHelper.current_instance: retvalue== %s' % str(retvalue))
        return retvalue

    @classmethod
    def set_loading(cls):
        """
        retruns returns the name of the instance that can be loaded to and sets it or None if
        it was not able to do the set and no instances are available to load 
        """
        cls.__lock.acquire()
        # only try to set if a current has been set
        retvalue = None
        # ensure the current and loading are up to date 
        cls.current_instance(_lock=False)
        if cls.__current_instance:
            if not cls.__loading_instance:
                # find a non-current instance which is not loading
                for key in settings.INSTANCE_INFO.keys():
                    if key not in [cls.__current_instance, cls.__loading_instance] and not cls.is_instance_loading(key):
                        cls.__loading_instance = key
                        retvalue = cls.__loading_instance
                        logging.debug('loading instance set to %s' % cls.__loading_instance)
                        break
        cls.__lock.release()
        return retvalue
    
    @classmethod
    def is_instance_loading(cls, instance_name):
        """return True if file exists, False otherwise"""
        return os.path.exists(settings.INSTANCE_INFO[instance_name]['lock_file'])
        
    @classmethod
    def instance_loading_info(cls, instance_name):
        """
        return tuple of loading, last_update, percent_loaded
        loading: True/False.  same as is_instance_loading(). If false, last_update and percent_loaded will be None
        last_update: datetime of when lock was last updated
        percent_loaded: int of percentage loaded when last updated
        """
        loading = cls.is_instance_loading(instance_name)
        last_update = None
        percent_loaded = None
        if loading:
            last_update, percent_loaded = open(settings.INSTANCE_INFO[instance_name]['lock_file'],'r').read().split('\n')[:2]
            last_update = datetime.datetime.strptime(last_update, '%c')
            percent_loaded = int(float(percent_loaded))
        return (loading, last_update, percent_loaded)

    @classmethod
    def set_instance_loading(cls, instance_name, percent_loaded=0):
        """
        to be called by the instance actually doing a load
        creates the loding lock file
        """
        if settings.INSTANCE_INFO.has_key(instance_name):
            f = open(settings.INSTANCE_INFO[instance_name]['lock_file'],'w')
            # write out when it was written
            f.write('%s\n%s\n' % (time.strftime('%c'), percent_loaded))
            f.close()
    
    @classmethod
    def clear_instance_loading(cls, instance_name):
        """
        to be called by the loding insance when loading is complete
        """
        if settings.INSTANCE_INFO.has_key(instance_name):
            filename = settings.INSTANCE_INFO[instance_name]['lock_file']
            if os.path.exists(filename):
                os.remove(filename)
                logging.debug('remove lock file %s' % filename)
            else:
                logging.error('when trying to remove %s, file did not exist' % filename)
            
class DashLoadCheckMiddleware(object):
    """ Do not allow requests while loading and show errors when the initial load fails"""

    def process_request(self, request):
        path = request.META['PATH_INFO'].lower()
        # if loading and not a media or json ready request, show a loading message
        if LOADING_LOCK.locked() and not safe_path(path):
            return render_to_response('ui/reload.html', {}, RequestContext(request, {
                'title':'Dashboard Reloading',
                # ensure % loaded doesn't go over 100% incase calculations change
                'error_message':'Dashboard is loading, about %d%% done. Please <a href="javascript:location.reload(true)">refresh this page</a> in a few minutes, otherwise it will be automatically reloaded every few minutes.' % min(99,PERCENT_LOADED),
                'details': '',
            }))
        # json did not initially load and it's not currently loading, show an initial load error
        elif not JSON_LOADED and not LOADING_LOCK.locked() and not safe_path(path):
            return render_to_response('ui/reload.html', {}, RequestContext(request, {
                'title':'Dashboard was not able to load',
                'error_message':'''Dashboard was not able to load for some reason.  Please contact to the tools team to investigate.\n<br> The problem may resove it's self in which case you can try <a href="javascript:location.reload(true)">refreshing this page</a> in a few minutes.''' ,
                # drop in the traceback in the html, only hidden from the user
                'details': "\n<!-- Load Traceback:\n\n%s\n-->" % LOAD_TRACEBACK,
            }))
        else:
            return None

class DashProxyMiddleware(object):
    """
    Add headers to when incomming requests are from a proxy
    minimally needed for tableau to work correctly
    pass throughs are set in the ui.proxy view.
    """
    def process_request(self, request):
        for name, value in request.GET.items():
            if name.startswith('_environ'):
                request.environ[str(name[9:])] = str(value)
                #request[str(name[9:])] = str(value)
                print 'setting %s to %s' % (name[9:], value)
        return None

class DashProxyRemoteUserMiddleWare(RemoteUserMiddleware):
    """
    Inherits from django RemoteUserMiddleWare, To make sure
    proxy can pass authentication to the instances
    """
    def process_request(self, request):
        # AuthenticationMiddleware is required so that request.user exists.
        if not hasattr(request, 'user'):
            raise ImproperlyConfigured(
                "The Django remote user auth middleware requires the"
                " authentication middleware to be installed.  Edit your"
                " MIDDLEWARE_CLASSES setting to insert"
                " 'django.contrib.auth.middleware.AuthenticationMiddleware'"
                " before the RemoteUserMiddleware class.")
        try:
            username = request.META[self.header]
        except KeyError:
            """
            Starting django 1.4, if any header not found then all header
            for remote-user are cleared. Since our application rely 
            on the authentication passed through proxy, so we need to
            bypass this
            """
            return
        # If the user is already authenticated and that user is the user we are
        # getting passed in the headers, then the correct user is already
        # persisted in the session and we don't need to continue.
        if request.user.is_authenticated():
            if request.user.get_username() == self.clean_username(username, request):
                return
        # We are seeing this user for the first time in this session, attempt
        # to authenticate the user.
        user = auth.authenticate(remote_user=username)
        if user:
            # User is valid.  Set request.user and persist user in the session
            # by logging the user in.
            request.user = user
            auth.login(request, user)    
    
class RestUtility(object):
    @staticmethod
    def getJSON(url):
        data = None
        try:
            data = urllib2.urlopen(url,timeout=10).read()
        except:
            import traceback
            print traceback.format_exc()                
        if data:
            try:
                data = cjson.decode(data)
            except cjson.DecodeError:
                data = None
        return data        

if settings.MODE == 'proxy':
    # if running locally, this will not hurt, when running on apache, it forces a request to the 
    # instances to ensure they load correctly becuase apache is being a pain
    ProxyHelper.confirm_instances_running()

#cleanup any existing lock files 
ProxyHelper.clear_instance_loading(settings.MODE)
    
