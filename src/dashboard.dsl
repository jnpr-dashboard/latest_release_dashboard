# Oracle connection properties
debug=no
# Number of connections to initialize the cache with
InitialLimit=1
MinLimit=0
MaxLimit=10
# Sets the maximum time a physical connection can remain idle in a
# connection cache.
InactivityTimeout=600
# Sets the time interval at which the Connection Cache Manager inspects
# and enforces all specified cache properties
PropertyCheckInterval=500
# (Times in seconds)
conn_file=/opt/dashboard/special/dash-db.properties
# conn_file should look like this:
# server=amdahl
# database=dswtools
# user=erxrli
# password=somepass
# driver_type=oci8 (or thin)