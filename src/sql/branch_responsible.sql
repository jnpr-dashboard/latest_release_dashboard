
-- clear out existing rule
DELETE FROM "DASH"."RULES" WHERE IDENTIFIER IN ('branch_responsible');
SET DEFINE ~;

INSERT INTO "DASH"."RULES" (IDENTIFIER, NAME, OBJECTIVE, DESCRIPTION, OWNER, MILESTONE, HORIZON, COUNTS, QUERY_FIELDS, LHS, WEIGHT, ACTIVE, GROUPING_VARIABLE, LAST_UPDATED, LEDE, SUNSET_MILESTONE, SUNSET_HORIZON)
VALUES (
  'branch_responsible',
  'Integration Branch',
  'HOLD_TO_SCHEDULE',
  'Total Integration Branch (IB) on Release',
  'admin',
  'NO_MILESTONE',
  '0',
  'BTS',
  'branch_name, branch_type, state, responsible,',
  '$release :ReleaseRecord( ) 
  $user : User( ) 
  $record_list : RecordSet( ) from 
   collect( BTRecord(  
          relname == $release.relname, planned_release not matches "^1[1-9]\.[0-9]W[0-9]*",
          branch_type == "ib", 
          responsible memberOf $user.reportNames || alwaysTrue == $user.junos ) )',
  '1',
  '1',
  'release',
  to_timestamp_tz('01-MAR-10 10.28.03.909545000 PM -08:00', 'DD-MON-RR HH.MI.SS.FF AM TZR'),
  'Integration Branch', -- LEDE
  'NO_MILESTONE',         -- SUNSET_MILESTONE
  '0'                 -- SUNSET_HORIZON
) ;


