/*
 * Enabling all Requirements Rule for PPM Project
 */

UPDATE "RULES_DEV" 
SET ACTIVE='1'  
   WHERE identifier in (
   'all_requirements', 'open_requirements', 'committed_requirements',
   'partial_requirements', 'total_requirements', 'unscheduled_requirements'
   );