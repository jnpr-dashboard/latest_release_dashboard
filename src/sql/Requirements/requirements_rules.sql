/*
 * Rule for PPM Project. 
 */

DELETE FROM "RULES_DEV" 
   WHERE identifier in (
   'all_requirements', 'open_requirements', 'committed_requirements',
   'partial_requirements', 'total_requirements', 'unscheduled_requirements'
   );
   
INSERT INTO "RULES_DEV" VALUES ('all_requirements', 'All Requirements', 'HOLD_TO_SCHEDULE', 'All Requirements assigned', 
  'admin', 'NO_MILESTONE', '0', 'REQS', 'synopsis, bg_driving, status, request_at_risk, release_target, npi_program, rlis_supporting, rlis_additional, plm_driver ', 
  '$release :ReleaseRecord( )
   $user : User( )
   $record_list : RecordSet( ) from
   collect( RequirementsRecord( 
          relname == $release.relname,
          plm_driver memberOf $user.reportNames || alwaysTrue == $user.junos ) )', '1', '1', 
  TO_TIMESTAMP_TZ('2010-08-16 22:25:06:558078 -08:00', 'YYYY-MM-DD HH24:MI:SS:FF6 TZR'), 'release', null, null, null, 
  'All Requirements', 'NO_MILESTONE', '0');
  
INSERT INTO "RULES_DEV" VALUES ('open_requirements', 'Open Requirements', 'HOLD_TO_SCHEDULE', 'Open Requirements assigned', 
  'admin', 'NO_MILESTONE', '0', 'REQS', 'synopsis, bg_driving, status, request_at_risk, release_target, npi_program, rlis_supporting, rlis_additional, plm_driver ', 
  '$release :ReleaseRecord( )
   $user : User( )
   $record_list : RecordSet( ) from
   collect( RequirementsRecord( 
          relname == $release.relname,
          status matches "open",
          plm_driver memberOf $user.reportNames || alwaysTrue == $user.junos ) )', '1', '1', 
  TO_TIMESTAMP_TZ('2010-08-16 22:25:06:558078 -08:00', 'YYYY-MM-DD HH24:MI:SS:FF6 TZR'), 'release', null, null, null, 
  'Open Requirements', 'NO_MILESTONE', '0');  
  
INSERT INTO "RULES_DEV" VALUES ('committed_requirements', 'Committed Requirements', 'HOLD_TO_SCHEDULE', 'Committed Requirements assigned', 
  'admin', 'NO_MILESTONE', '0', 'REQS', 'synopsis, bg_driving, status, request_at_risk, release_target, npi_program, rlis_supporting, rlis_additional, plm_driver ', 
  '$release :ReleaseRecord( )
   $user : User( )
   $record_list : RecordSet( ) from
   collect( RequirementsRecord( 
          relname == $release.relname,
          status matches "committed",
          plm_driver memberOf $user.reportNames || alwaysTrue == $user.junos ) )', '1', '1', 
  TO_TIMESTAMP_TZ('2010-08-16 22:25:06:558078 -08:00', 'YYYY-MM-DD HH24:MI:SS:FF6 TZR'), 'release', null, null, null, 
  'Committed Requirements', 'NO_MILESTONE', '0');  
  
INSERT INTO "RULES_DEV" VALUES ('partial_requirements', 'Partial Committed Requirements', 'HOLD_TO_SCHEDULE', 'Partial Committed Requirements assigned', 
  'admin', 'NO_MILESTONE', '0', 'REQS', 'synopsis, bg_driving, status, request_at_risk, release_target, npi_program, rlis_supporting, rlis_additional, plm_driver ', 
  '$release :ReleaseRecord( )
   $user : User( )
   $record_list : RecordSet( ) from
   collect( RequirementsRecord( 
          relname == $release.relname,
          status matches "partial_committed",
          plm_driver memberOf $user.reportNames || alwaysTrue == $user.junos ) )', '1', '1', 
  TO_TIMESTAMP_TZ('2010-08-16 22:25:06:558078 -08:00', 'YYYY-MM-DD HH24:MI:SS:FF6 TZR'), 'release', null, null, null, 
  'Partial Committed Requirements', 'NO_MILESTONE', '0');  


INSERT INTO "RULES_DEV" VALUES ('total_requirements', 'Total Requirements', 'HOLD_TO_SCHEDULE', 'Total Requirements assigned', 
  'admin', 'NO_MILESTONE', '0', 'REQS', 'synopsis, bg_driving, status, request_at_risk, release_target, npi_program, rlis_supporting, rlis_additional, plm_driver ', 
  '$release :ReleaseRecord( )
   $user : User( )
   $record_list : RecordSet( ) from
   collect( RequirementsRecord( 
          plm_driver memberOf $user.reportNames || alwaysTrue == $user.junos ) )', '1', '1', 
  TO_TIMESTAMP_TZ('2010-08-16 22:25:06:558078 -08:00', 'YYYY-MM-DD HH24:MI:SS:FF6 TZR'), '', null, null, null, 
  'Total Requirements', 'NO_MILESTONE', '0');
  
INSERT INTO "RULES_DEV" VALUES ('unscheduled_requirements', 'Unscheduled Requirements', 'HOLD_TO_SCHEDULE', 'Unscheduled Requirements assigned', 
  'admin', 'NO_MILESTONE', '0', 'REQS', 'synopsis, bg_driving, status, request_at_risk, release_target, npi_program, rlis_supporting, rlis_additional, plm_driver ', 
  '$release :ReleaseRecord( )
   $user : User( )
   $record_list : RecordSet( ) from
   collect( RequirementsRecord( 
          release_target in ("unscheduled","n/a"),
          plm_driver memberOf $user.reportNames || alwaysTrue == $user.junos ) )', '1', '1', 
  TO_TIMESTAMP_TZ('2010-08-16 22:25:06:558078 -08:00', 'YYYY-MM-DD HH24:MI:SS:FF6 TZR'), '', null, null, null, 
  'Unscheduled Requirements', 'NO_MILESTONE', '0');
 
