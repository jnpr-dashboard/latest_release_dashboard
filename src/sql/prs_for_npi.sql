
-- clear out existing rule
DELETE FROM "DASH"."RULES" WHERE IDENTIFIER IN ('prs_for_npi');
SET DEFINE ~;

INSERT INTO "DASH"."RULES" (IDENTIFIER, NAME, OBJECTIVE, DESCRIPTION, OWNER, MILESTONE, HORIZON, COUNTS, QUERY_FIELDS, LHS, WEIGHT, ACTIVE, GROUPING_VARIABLE, LAST_UPDATED, RHS, LEDE, SUNSET_MILESTONE, SUNSET_HORIZON)
VALUES (
  'prs_for_npi',
  'Find PRs for NPI Program',
  'NO_OBJECTIVE',
  'bookkeeping',
  'admin',
  'NO_MILESTONE',
  '0',
  'NOTHING',
  '',
  '$npi : NPIRecord( ) 
$pr : PRRecord( npi == "", rli != "" ) 
$rli : RLIRecord( npi_program == $npi.synopsis, id memberOf $pr.rli_nums )',
  '1',
  '0',
  'release',
  to_timestamp_tz('24-JUN-09 02.05.35.513332000 PM -07:00', 'DD-MON-RR HH.MI.SS.FF AM TZR'),
  "$pr.setNpi($npi.getId()); 
 update($pr);"
  'Someone should address these PRs one of these days', -- LEDE
  'NO_MILESTONE',         -- SUNSET_MILESTONE
  '0'                 -- SUNSET_HORIZON
) ;


