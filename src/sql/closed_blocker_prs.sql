
-- clear out existing rule
DELETE FROM "DASH"."RULES" WHERE IDENTIFIER IN ('closed_blocker_prs');
SET DEFINE ~;

INSERT INTO "DASH"."RULES" (IDENTIFIER, NAME, OBJECTIVE, DESCRIPTION, OWNER, MILESTONE, HORIZON, COUNTS, QUERY_FIELDS, LHS, WEIGHT, ACTIVE, GROUPING_VARIABLE, LAST_UPDATED, LEDE, SUNSET_MILESTONE, SUNSET_HORIZON)
VALUES (
  'closed_blocker_prs',
  'All Closed Blocker PRs',
  'P5_STABILITY',
  'Owners of PRs with the [Blocking-Release] field set must prioritize these ETAs and fixes over non-blocker PRs.  This rule includes all non-suspended PRs (systest metrics exclude analyzed).',
  'admin',
  'NO_MILESTONE',
  '0',
  'CLOSEDPRS',
  'synopsis, state, problem-level, category, product, planned-release, blocker, reported-in, submitter-id, responsible, dev-owner, originator, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli,',
  '$release : ReleaseRecord( ) 
$user : User( ) 
$record_list : RecordSet( ) from 
 collect( ClosedPRRecord( blocked_release == $release.relname, planned_release not matches "^1[1-9]\.[0-9]W[0-9]*",
 (category not matches ".*hw-.*" &&  category not matches ".*build.*"), 
  npi == $user.npi_program || 
  alwaysTrue == $user.junos ||  
  (eval(alwaysTrue == blank_devowner) &&  responsible memberOf $user.reportNames) ||  
  (eval(alwaysTrue != blank_devowner) &&  dev_owner memberOf $user.reportNames)  ))',
  '0',
  '1',
  'release',
  to_timestamp_tz('09-JAN-10 12.39.36.137426000 AM -08:00', 'DD-MON-RR HH.MI.SS.FF AM TZR'),
  'Includes all non-suspended PRs.', -- LEDE
  'NO_MILESTONE',         -- SUNSET_MILESTONE
  '0'                 -- SUNSET_HORIZON
) ;


