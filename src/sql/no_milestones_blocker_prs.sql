
-- clear out existing rule
DELETE FROM "DASH"."RULES" WHERE IDENTIFIER IN ('no_milestones_blocker_prs');
SET DEFINE ~;

INSERT INTO "DASH"."RULES" (IDENTIFIER, NAME, OBJECTIVE, DESCRIPTION, OWNER, MILESTONE, HORIZON, COUNTS, QUERY_FIELDS, LHS, WEIGHT, ACTIVE, GROUPING_VARIABLE, LAST_UPDATED, LEDE, SUNSET_MILESTONE, SUNSET_HORIZON)
VALUES (
  'no_milestones_blocker_prs',
  'No Milestone Blocker PRs',
  'HOLD_TO_SCHEDULE',
  'To get non-milestone blocker PRs',
  'admin',
  'NO_MILESTONE',
  '0',
  'PRS',
  'synopsis, state, problem-level, category, product, planned-release, blocker, submitter-id, responsible, dev-owner, originator, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli,',
  '$release : ReleaseRecord( ) 
$user : User( ) 
$record_list : RecordSet( ) from 
 collect( PRRecord( blocked_release == $release.relname, planned_release not matches "^1[1-9]\.[0-9]W[0-9]*",
  alwaysTrue == not_milestones_release, npi == $user.npi_program || 
  alwaysTrue == $user.junos || responsible memberOf $user.reportNames || 
  ( dev_owner memberOf $user.reportNames &&  state != "feedback" ) ) )',
  '1',
  '1',
  'release',
  to_timestamp_tz('03-MAY-10 06.29.02.025723000 PM -07:00', 'DD-MON-RR HH.MI.SS.FF AM TZR'),
  'To get non-milestone blocker PRs', -- LEDE
  'NO_MILESTONE',         -- SUNSET_MILESTONE
  '0'                 -- SUNSET_HORIZON
) ;


