-- clear out existing rule
DELETE FROM "RULES" WHERE IDENTIFIER IN ('backlog_prs_forward_2010_responsible_not_dev');
SET DEFINE ~;

INSERT INTO "RULES" 
(IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) 
values (
   'backlog_prs_forward_2010_responsible_not_dev',
   '2010 Forward View / Future Backlog PRs by Responsible != Dev-Owner',
   'HOLD_TO_SCHEDULE',
   'All Future Backlog PR by using responsible as their primary accountability where the responsible != dev-owner',
   'admin',
   'NO_MILESTONE',
   '0',
   'PRS',
   'synopsis, state, problem-level, category, product, planned-release, blocker, submitter-id, responsible, dev-owner, originator, systest-owner, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli, created, resolution, feedback-date, jtac-case-id,',
   '$user : User( )
$record_list : RecordSet( ) from
 collect(
    PRRecord(
	    (problem_level == "1-CL1" || problem_level == "2-CL2" || problem_level == "4-CL3" || problem_level == "3-IL1" || problem_level == "4-IL2" ||  problem_level == "5-IL3"),
	    (clazz == "bug" || clazz == "unreproducible" ),functional_area=="software",
	    (alwaysTrue == possible_forward_view_backlog || alwaysTrue == possible_backlog),
	    real_responsible != dev_owner,
	    npi == $user.npi_program || alwaysTrue == $user.junos ||
	    backlog_pr_responsible memberOf $user.reportNames ))
',
    '1',
    '1',
    to_timestamp_tz('01-NOV-10 12.00.00.511060000 PM -07:00','DD-MON-RR HH.MI.SSXFF AM TZR'),
    '',
    '',
    '',
    '',
    'Forward View / Future Backlog PRs by Responsible', -- LEDE
    'NO_MILESTONE', -- SUNSET_MILESTONE
    '0' -- SUNSET_HORIZON
);
