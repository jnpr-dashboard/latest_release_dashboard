drop table ALIASES cascade constraints;

/*==============================================================*/
/* Table: ALIASES                                              */
/*==============================================================*/
create table ALIASES
(
   ALIAS              VARCHAR2(128)        not null,
   BROAD_USER         VARCHAR2(128)        not null,
   constraint PK_ALIASES primary key (ALIAS, BROAD_USER)
);
