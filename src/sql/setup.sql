--
-- SQL to set up the database schema.
--
-- Copyright (c) 2006-2007, Juniper Networks, Inc.
-- All rights reserved.
--

PROMPT CREATE TABLE &dbuser.rules

CREATE TABLE &dbuser.rules
(
    identifier                       VARCHAR2 (32)                    NOT NULL
  , name                             VARCHAR2 (255)
  , objective                        VARCHAR2 (32)
  , description                      VARCHAR2 (4000)
  , owner                            VARCHAR2 (32)
  , milestone                        VARCHAR2 (60)
  , horizon                          NUMBER
  , sunset_milestone                 VARCHAR2 (60)                    DEFAULT 'NO_MILESTONE'
  , sunset_horizon                   NUMBER                           DEFAULT 0
  , lhs                              VARCHAR2 (2000)
  , weight                           NUMBER                           DEFAULT 1
  , active                           NUMBER                           DEFAULT 1
  , counts                           VARCHAR2 (10)                    DEFAULT 'NOTHING'
  , query_fields                     VARCHAR2 (500)
  , last_updated                     TIMESTAMP(6) WITH TIME ZONE
  , grouping_variable                VARCHAR2 (20)
  , rhs                              VARCHAR2 (1000)
  , attributes                       VARCHAR2 (255)
  , agenda_group                     VARCHAR2 (100)
  , lede                             VARCHAR2 (100)
)
PARALLEL
(
  DEGREE            1
  INSTANCES         1
)
PCTFREE             10
INITRANS            1
MAXTRANS            255
STORAGE
(
  INITIAL           4194304
  NEXT              1024000
  MINEXTENTS        1
  MAXEXTENTS        unlimited
  PCTINCREASE       0
  FREELISTS         1
  FREELIST GROUPS   1
)
TABLESPACE          rli_data
;

PROMPT CREATE UNIQUE INDEX &dbuser.sys_c007621 ON &dbuser.rules

CREATE UNIQUE INDEX &dbuser.sys_c007621 ON &dbuser.rules
(
    identifier
)
PCTFREE             10
INITRANS            2
MAXTRANS            255
STORAGE
(
  INITIAL           4194304
  NEXT              1024000
  MINEXTENTS        1
  MAXEXTENTS        unlimited
  PCTINCREASE       0
  FREELISTS         1
  FREELIST GROUPS   1
)
TABLESPACE          rli_data
;

PROMPT ALTER TABLE &dbuser.rules ADD CONSTRAINT sys_c007621 PRIMARY KEY

ALTER TABLE &dbuser.rules ADD CONSTRAINT sys_c007621 PRIMARY KEY
(
    identifier
)
ENABLE;

DROP SEQUENCE id_seq;

CREATE SEQUENCE id_seq
   START WITH       1
   INCREMENT BY     1
   MINVALUE         1
   NOMAXVALUE
   CACHE            20
   NOCYCLE
   NOORDER;

show errors
