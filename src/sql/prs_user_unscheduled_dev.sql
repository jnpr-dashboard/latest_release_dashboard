
-- clear out existing rule
DELETE FROM "DASH"."RULES" WHERE IDENTIFIER IN ('prs_user_unscheduled_dev');
SET DEFINE ~;

INSERT INTO "DASH"."RULES" (IDENTIFIER, NAME, OBJECTIVE, DESCRIPTION, OWNER, MILESTONE, HORIZON, COUNTS, QUERY_FIELDS, LHS, WEIGHT, ACTIVE, GROUPING_VARIABLE, LAST_UPDATED, LEDE, SUNSET_MILESTONE, SUNSET_HORIZON)
VALUES (
  'prs_user_unscheduled_dev',
  'Unscheduled Dev Branch PRs',
  'HOLD_TO_SCHEDULE',
  'PRs for Unscheduled Development Branch and Private Branch PR. PRs is assign to user as responsible or Dev-owner. Development Branch has unscheduled or n/a in Release Target.',
  'admin',
  'NO_MILESTONE',
  '0',
  'PRS',
  'synopsis, state, problem-level, category, product, planned-release, branch, blocker, submitter-id, responsible, dev-owner, originator, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli,',
  '$user : User( ) 
$bt : BTRecord( relname in ("unscheduled","n/a"), branch_name matches "DEV_.*") 
$record_list : RecordSet( ) from 
 collect( PRRecord( branch == $bt.branch_name ,  planned_release not matches "^1[1-9]\.[0-9]W[0-9]*",
  npi == $user.npi_program || 
  alwaysTrue == $user.junos ||  
  responsible memberOf $user.reportNames || 
  (dev_owner memberOf $user.reportNames &&  state != "feedback")))',
  '1',
  '1',
  '',
  to_timestamp_tz('08-JUN-10 04.08.14.085427000 PM -07:00', 'DD-MON-RR HH.MI.SS.FF AM TZR'),
  'Unscheduled Development Branch', -- LEDE
  'NO_MILESTONE',           -- SUNSET_MILESTONE
  '0'                       -- SUNSET_HORIZON
) ;
