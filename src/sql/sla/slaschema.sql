drop table SLA cascade constraints;
drop table ORGCHART cascade constraints;

/*==============================================================*/
/* Table: ORGCHART                                              */
/*==============================================================*/
create table ORGCHART
(
   ORG_ID              VARCHAR2(32)        not null,
   SUPER_ID            VARCHAR2(32),
   LEAF                NUMBER              not null,
   constraint PK_ORGCHART primary key (ORG_ID)
);

/*==============================================================*/
/* Table: SLA                                                   */
/*==============================================================*/
create table SLA
(
   ORG_ID               VARCHAR2(32)          not null,
   ACK_TIME             NUMBER,
   ACK_TIME_LINK        CLOB,
   ASSIGN_TIME          NUMBER,
   ASSIGN_TIME_LINK     CLOB,
   PCT_INFO_STAT        NUMBER,
   PCT_INFO_STAT_LINK   CLOB,
   INFO_TIME            NUMBER,
   INFO_TIME_LINK       CLOB,
   INFO_STAT            NUMBER,
   INFO_STAT_LINK       CLOB,
   VERIFY_TIME          NUMBER,
   VERIFY_TIME_LINK     CLOB,

   constraint PK_SLA primary key (ORG_ID)
);

alter table SLA
   add constraint FK_SLA foreign key (ORG_ID)
      references ORGCHART (ORG_ID) on delete cascade; 
commit; 
