-- clear out existing rule
DELETE FROM "RULES" WHERE IDENTIFIER IN ('major_prs_fix');
SET DEFINE ~;

INSERT INTO "RULES" (IDENTIFIER, NAME, OBJECTIVE, DESCRIPTION, OWNER, MILESTONE, HORIZON, COUNTS, QUERY_FIELDS, LHS, WEIGHT, ACTIVE, GROUPING_VARIABLE, LAST_UPDATED, RHS, ATTRIBUTES, AGENDA_GROUP, LEDE, SUNSET_MILESTONE, SUNSET_HORIZON)
VALUES (
   'major_prs_fix',
   'Major PRs Fix',
   'HOLD_TO_SCHEDULE',
   'Major PRs should be prioritized over minor ones.-- only used on PR-fix report --',
   'admin',
   'NO_MILESTONE',
   '0',
   'PRS',
   'synopsis, state, problem-level, category, product, planned-release, blocker, reported-in, submitter-id, responsible, dev-owner, originator, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli,',
   '$release : ReleaseRecord( )
$user : User( )
$record_list : RecordSet( ) from
 collect( PRRecord( (problem_level in ("2-CL2","4-CL3","4-IL2","5-IL3")), clazz == "bug",
 (category not matches ".*hw-.*" && category not matches ".*build.*"),
 (releases contains $release.relname || branchRelease == $release.relname ),
  npi == $user.npi_program ||
  alwaysTrue == $user.junos ||
   ((dev_owner memberOf $user.reportNames  && ( state == "info" ||
           (state=="feedback" && eval(alwaysTrue != blank_devowner))))
   ||(responsible memberOf $user.reportNames && (state == "open" || state == "analyzed" ||
           (state=="feedback" && eval(alwaysTrue == blank_devowner))))
   )))',
    '0',
    '1',
    'release',
    to_timestamp_tz('01-FEB-10 08.25.20.675709000 PM -08:00','DD-MON-RR HH.MI.SSXFF AM TZR'),
    '',
    '',
    '',
    'Major PRs should be prioritized over minor ones.',  -- LEDE
    'NO_MILESTONE',  -- SUNSET_MILESTONE
    '0'  -- SUNSET_HORIZON
);

