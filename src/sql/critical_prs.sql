
-- clear out existing rule
DELETE FROM "DASH"."RULES" WHERE IDENTIFIER IN ('critical_prs');
SET DEFINE ~;

INSERT INTO "DASH"."RULES" (IDENTIFIER, NAME, OBJECTIVE, DESCRIPTION, OWNER, MILESTONE, HORIZON, COUNTS, QUERY_FIELDS, LHS, WEIGHT, ACTIVE, GROUPING_VARIABLE, LAST_UPDATED, LEDE, SUNSET_MILESTONE, SUNSET_HORIZON)
VALUES (
  'critical_prs',
  'Critical PRs',
  'HOLD_TO_SCHEDULE',
  'Owners of PRs with the [Severity] field set to "critical" must prioritize these ETAs and fixes over non-critical PRs.',
  'admin',
  'NEXT_BUILD',
  '45',
  'PRS',
  'synopsis, state, problem-level, category, product, planned-release, blocker, submitter-id, responsible, dev-owner, originator, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli,',
'$release : ReleaseRecord( )
$user : User( )
$record_list : RecordSet( ) from
collect( PRRecord( problem_level == "1-CL1" || problem_level == "3-IL1",
releases contains $release.relname, planned_release not matches "^1[1-9]\.[0-9]W[0-9]*", npi == $user.npi_program ||
alwaysTrue == $user.junos || responsible memberOf $user.reportNames ||
( dev_owner memberOf $user.reportNames && state != "feedback" ) ) )',
  '3',
  '1',
  'release',
  to_timestamp_tz('07-FEB-11 12.00.00.511060000 PM -07:00', 'DD-MON-RR HH.MI.SS.FF AM TZR'),
  'Owners of PRs with the [Severity] field set to "critical" mus...', -- LEDE
  'NEXT_DEPLOY',         -- SUNSET_MILESTONE
  '-7'                   -- SUNSET_HORIZON
) ;


