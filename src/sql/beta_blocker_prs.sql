
-- clear out existing rule
DELETE FROM "DASH"."RULES" WHERE IDENTIFIER IN ('beta_blocker_prs');
SET DEFINE ~;

INSERT INTO "DASH"."RULES" (IDENTIFIER, NAME, OBJECTIVE, DESCRIPTION, OWNER, MILESTONE, HORIZON, COUNTS, QUERY_FIELDS, LHS, WEIGHT, ACTIVE, GROUPING_VARIABLE, LAST_UPDATED, LEDE, SUNSET_MILESTONE, SUNSET_HORIZON)
VALUES (
  'beta_blocker_prs',
  'Beta Blocker PRs',
  'HOLD_TO_SCHEDULE',
  'Owners of PRs with the [Blocking-Release] field set to a Bx build must prioritize these ETAs and fixes over non-blocker PRs.  This rule includes all non-suspended PRs (systest metrics exclude analyzed).',
  'admin',
  'BN_DEPLOY',
  '21',
  'PRS',
  'synopsis, state, problem-level, category, product, planned-release, blocker, submitter-id, responsible, dev-owner, originator, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli,',
'$release : ReleaseRecord( )
$user : User( )
$record_list : RecordSet( ) from
 collect( PRRecord( blocked_release == $release.relname, planned_release not matches "^1[1-9]\.[0-9]W[0-9]*",
  blocker_types contains "beta", npi == $user.npi_program ||
  alwaysTrue == $user.junos || responsible memberOf $user.reportNames ||
  ( dev_owner memberOf $user.reportNames && state != "feedback" ) ) )',
  '0',
  '1',
  'release',
  to_timestamp_tz('14-MAY-09 07.53.40.000000000 PM -07:00', 'DD-MON-RR HH.MI.SS.FF AM TZR'),
  'Includes all non-suspended PRs.', -- LEDE
  'B3_DEPLOY',         -- SUNSET_MILESTONE
  '-7'                 -- SUNSET_HORIZON
) ;


