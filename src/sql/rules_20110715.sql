--------------------------------------------------------
--  File created - Thursday-July-14-2011   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Table RULES
--------------------------------------------------------
SET DEFINE ~;

  CREATE TABLE "DASH"."RULES" 
   (	"IDENTIFIER" VARCHAR2(50 BYTE), 
	"NAME" VARCHAR2(255 BYTE), 
	"OBJECTIVE" VARCHAR2(32 BYTE), 
	"DESCRIPTION" VARCHAR2(4000 BYTE), 
	"OWNER" VARCHAR2(32 BYTE), 
	"MILESTONE" VARCHAR2(60 BYTE), 
	"HORIZON" NUMBER, 
	"COUNTS" VARCHAR2(10 BYTE), 
	"QUERY_FIELDS" VARCHAR2(500 BYTE), 
	"LHS" VARCHAR2(2000 BYTE), 
	"WEIGHT" NUMBER, 
	"ACTIVE" NUMBER, 
	"LAST_UPDATED" TIMESTAMP (6) WITH TIME ZONE, 
	"GROUPING_VARIABLE" VARCHAR2(20 BYTE), 
	"RHS" VARCHAR2(1000 BYTE), 
	"ATTRIBUTES" VARCHAR2(255 BYTE), 
	"AGENDA_GROUP" VARCHAR2(100 BYTE), 
	"LEDE" VARCHAR2(100 BYTE), 
	"SUNSET_MILESTONE" VARCHAR2(60 BYTE), 
	"SUNSET_HORIZON" NUMBER
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "RLI_DATA" ;
REM INSERTING into DASH.RULES
Insert into DASH.RULES (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) values ('major_closed_pr','Major Closed PRs','HOLD_TO_SCHEDULE','Major PRs should be prioritized over minor ones.','mbasha','NO_MILESTONE',0,'CLOSEDPRS','synopsis, state, problem-level, category, product, planned-release, blocker, reported-in, submitter-id, responsible, dev-owner, originator, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli,','$release : ReleaseRecord( )  
$user : User( )  
$record_list : RecordSet( ) from  
 collect( ClosedPRRecord( problem_level == "2-CL2" || problem_level == "4-CL3" || problem_level == "4-IL2" || problem_level == "5-IL3",  
 (category not matches ".*hw-.*" &&  category not matches ".*build.*"),  
 (releases contains $release.relname || branchRelease == $release.relname ), planned_release not matches "^1[1-9]\.[0-9]W[0-9]*",  
  npi == $user.npi_program ||  
  alwaysTrue == $user.junos ||   
  (eval(alwaysTrue == blank_devowner) &&  responsible memberOf $user.reportNames) ||   
  (eval(alwaysTrue != blank_devowner) &&  dev_owner memberOf $user.reportNames)  ))',0,1,to_timestamp_tz('09-JAN-10 12.41.01.368638000 AM -08:00','DD-MON-RR HH.MI.SS.FF AM TZR'),'release',null,null,null,'Major PRs should be prioritized over minor ones.','NO_MILESTONE',0);
Insert into DASH.RULES (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) values ('closed_critical_prs','Closed Critical PRs','P5_STABILITY','Owners of PRs with the [Severity] field set to "critical" must prioritize these ETAs and fixes over non-critical PRs.','admin','NO_MILESTONE',0,'CLOSEDPRS','synopsis, state, problem-level, category, product, planned-release, blocker, reported-in, submitter-id, responsible, dev-owner, originator, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli,','$release : ReleaseRecord( )  
$user : User( )  
$record_list : RecordSet( ) from  
 collect( ClosedPRRecord( severity == "critical",  
 (category not matches ".*hw-.*" &&  category not matches ".*build.*"),  
  releases contains $release.relname, planned_release not matches "^1[1-9]\.[0-9]W[0-9]*", npi == $user.npi_program ||  
  alwaysTrue == $user.junos ||   
  (eval(alwaysTrue == blank_devowner) &&  responsible memberOf $user.reportNames) ||   
  (eval(alwaysTrue != blank_devowner) &&  dev_owner memberOf $user.reportNames)  ))',0,0,to_timestamp_tz('09-JAN-10 12.40.06.423259000 AM -08:00','DD-MON-RR HH.MI.SS.FF AM TZR'),'release',null,null,null,'Owners of PRs with the [Severity] field set to "critical" mus...','NO_MILESTONE',0);
Insert into DASH.RULES (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) values ('backlog_prs_reference_2010','2010 Reference Backlog PRs 2010-01-31','HOLD_TO_SCHEDULE','All Reference Backlog PR','admin','NO_MILESTONE',0,'PRS','synopsis, state, problem-level, category, product, planned-release, blocker, submitter-id, responsible, dev-owner, originator, systest-owner, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli, created, resolution, feedback-date, jtac-case-id,','$user : User( ) 
$record_list : RecordSet( ) 
  from collect( 
    OldPRRecord( 
      extract_date == "2010-01-31", 
      state != "closed", 
      (problem_level == "1-CL1" || problem_level == "2-CL2" || problem_level == "4-CL3" || problem_level == "3-IL1" || problem_level == "4-IL2" ||  problem_level == "5-IL3"), 
      (clazz == "bug" || clazz == "unreproducible"), 
      functional_area=="software", 
      alwaysTrue == possible_backlog, 
      npi == $user.npi_program || alwaysTrue == $user.junos || backlog_pr_dev_owner memberOf $user.reportNames 
    ) 
  )',1,1,to_timestamp_tz('01-NOV-10 12.00.00.511060000 PM -07:00','DD-MON-RR HH.MI.SS.FF AM TZR'),null,null,null,null,'Reference Backlog PRs ','NO_MILESTONE',0);
Insert into DASH.RULES (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) values ('backlog_prs_forward_2010','2010 Forward View Backlog PR','HOLD_TO_SCHEDULE','PRs which will be counted as current backlog in next R1 deploy','admin','NO_MILESTONE',0,'PRS','synopsis, state, problem-level, category, product, planned-release, blocker, submitter-id, responsible, dev-owner, originator, systest-owner, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli, created, resolution, feedback-date, jtac-case-id,','$user : User( ) 
$record_list : RecordSet( ) 
  from collect( 
    PRRecord( 
      (problem_level == "1-CL1" || problem_level == "2-CL2" || problem_level == "4-CL3" || problem_level == "3-IL1" || problem_level == "4-IL2" ||  problem_level == "5-IL3"),
      (clazz == "bug" || clazz == "unreproducible"),functional_area=="software", 
      (alwaysTrue == possible_forward_view_backlog || alwaysTrue == possible_backlog), 
      npi == $user.npi_program || alwaysTrue == $user.junos || 
      backlog_pr_dev_owner memberOf $user.reportNames 
    ) 
  )',0,1,to_timestamp_tz('01-NOV-10 12.00.00.511060000 PM -07:00','DD-MON-RR HH.MI.SS.FF AM TZR'),null,null,null,null,'Reference Backlog PRs ','NO_MILESTONE',0);
Insert into DASH.RULES (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) values ('backlog_prs_reference_2010_responsible','2010 Reference Backlog PRs by Responsible 2010-01-31','HOLD_TO_SCHEDULE','All Reference Backlog PR by using responsible as their primary accountability','admin','NO_MILESTONE',0,'PRS','synopsis, state, problem-level, category, product, planned-release, blocker, submitter-id, responsible, dev-owner, originator, systest-owner, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli, created, resolution, feedback-date, jtac-case-id,','$user : User( ) 
$record_list : RecordSet( ) 
  from collect( 
    OldPRRecord( 
      extract_date == "2010-01-31", 
      state != "closed", 
      (problem_level == "1-CL1" || problem_level == "2-CL2" || problem_level == "4-CL3" || problem_level == "3-IL1" || problem_level == "4-IL2" ||  problem_level == "5-IL3"),
      (clazz == "bug" || clazz == "unreproducible"),functional_area=="software", 
      alwaysTrue == possible_backlog, 
      npi == $user.npi_program || alwaysTrue == $user.junos || backlog_pr_responsible memberOf $user.reportNames 
    ) 
  )',1,1,to_timestamp_tz('01-NOV-10 12.00.00.511060000 PM -07:00','DD-MON-RR HH.MI.SS.FF AM TZR'),null,null,null,null,'Reference Backlog PRs ','NO_MILESTONE',0);
Insert into DASH.RULES (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) values ('backlog_prs_forward_2010_responsible','2010 Forward View / Future Backlog PRs by Responsible','HOLD_TO_SCHEDULE','All Future Backlog PR by using responsible as their primary accountability','admin','NO_MILESTONE',0,'PRS','synopsis, state, problem-level, category,  product, planned-release, blocker, submitter-id, responsible, dev-owner, originator, systest-owner, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli, created, resolution, feedback-date, jtac-case-id,','$user : User( ) 
$record_list : RecordSet( ) 
  from collect( 
    PRRecord( 
      (problem_level == "1-CL1" || problem_level == "2-CL2" || problem_level == "4-CL3" || problem_level == "3-IL1" || problem_level == "4-IL2" ||  problem_level == "5-IL3"), 
      (clazz == "bug" || clazz == "unreproducible"),functional_area=="software", 
      (alwaysTrue == possible_forward_view_backlog || alwaysTrue == possible_backlog), 
      npi == $user.npi_program || alwaysTrue == $user.junos || 
      backlog_pr_responsible memberOf $user.reportNames 
    ) 
  )',0,1,to_timestamp_tz('01-NOV-10 12.00.00.511060000 PM -07:00','DD-MON-RR HH.MI.SS.FF AM TZR'),null,null,null,null,'Reference Backlog PRs ','NO_MILESTONE',0);
Insert into DASH.RULES (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) values ('backlog_prs_reference_2010_responsible_not_dev','2010 RReference Backlog PRs by Responsible != Dev-Owner 2010-01-31','HOLD_TO_SCHEDULE','All Reference Backlog PR by using responsible as their primary accountability where the responsible != dev-owner','admin','NO_MILESTONE',0,'PRS','synopsis, state, problem-level, category, product, planned-release, blocker, submitter-id, responsible, dev-owner, originator, systest-owner, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli, created, resolution, feedback-date, jtac-case-id,','$user : User( ) 
$record_list : RecordSet( ) from 
 collect( 
    OldPRRecord( 
        extract_date == "2010-01-31", 
        state != "closed", 
        (problem_level == "1-CL1" || problem_level == "2-CL2" || problem_level == "4-CL3" || problem_level == "3-IL1" || problem_level == "4-IL2" ||  problem_level == "5-IL3"), 
        (clazz == "bug" || clazz == "unreproducible" ),functional_area=="software", 
        alwaysTrue == possible_backlog, 
        real_responsible != dev_owner, 
        npi == $user.npi_program || alwaysTrue == $user.junos || 
        backlog_pr_responsible memberOf $user.reportNames )) 
',1,1,to_timestamp_tz('01-NOV-10 12.00.00.511060000 PM -07:00','DD-MON-RR HH.MI.SS.FF AM TZR'),null,null,null,null,'Reference Backlog PRs by Responsible != Dev-Owner','NO_MILESTONE',0);
Insert into DASH.RULES (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) values ('backlog_prs_reference_doc','Reference Backlog Doc PRs','HOLD_TO_SCHEDULE','All Reference Backlog Doc PR','admin','NO_MILESTONE',0,'PRS','synopsis, state, problem-level, category, product, planned-release, blocker, submitter-id, techpubs-owner, originator, systest-owner, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli, created, resolution, feedback-date, jtac-case-id,','$user : User( ) 
$record_list : RecordSet( ) 
  from collect( 
    OldPRRecord( 
        extract_date == "2010-12-31", 
        state != "closed", 
        (problem_level == "1-CL1" || problem_level == "2-CL2" || problem_level == "4-CL3" || problem_level == "3-IL1" || problem_level == "4-IL2" ||  problem_level == "5-IL3"), 
        (clazz == "bug" || clazz == "unreproducible" ), functional_area=="documentation", 
        alwaysTrue == possible_backlog, 
        npi == $user.npi_program || alwaysTrue == $user.junos || 
        backlog_pr_techpub_owner memberOf $user.reportNames 
    ) 
  )',0,1,to_timestamp_tz('31-DEC-10 12.00.00.511060000 PM -07:00','DD-MON-RR HH.MI.SS.FF AM TZR'),null,null,null,null,'Reference Backlog Doc PRs ','NO_MILESTONE',0);
Insert into DASH.RULES (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) values ('backlog_prs_current_doc','Current Backlog Doc PRs ','HOLD_TO_SCHEDULE','Current Backlog Doc PRs ','admin','NO_MILESTONE',0,'PRS','synopsis, state, problem-level, category, product, planned-release, cvbc-documented-in, blocker, submitter-id, techpubs-owner, originator, systest-owner, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli, created, resolution, feedback-date, jtac-case-id,','$user : User( ) 
$record_list : RecordSet( ) from 
 collect( PRRecord( (problem_level == "1-CL1" || problem_level == "2-CL2" || problem_level == "4-CL3" || problem_level == "3-IL1" || problem_level == "4-IL2" ||  problem_level == "5-IL3"), 
    (clazz == "bug" || clazz == "unreproducible" ),functional_area=="documentation", 
    alwaysTrue == possible_backlog, 
    npi == $user.npi_program || alwaysTrue == $user.junos || 
    backlog_pr_techpub_owner memberOf $user.reportNames )) 
',0,1,to_timestamp_tz('01-NOV-10 12.00.00.511060000 PM -07:00','DD-MON-RR HH.MI.SS.FF AM TZR'),null,null,null,null,'Current Backlog Doc PRs','NO_MILESTONE',0);
Insert into DASH.RULES (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) values ('backlog_prs_forward_doc','Forward View Backlog Doc PRs ','HOLD_TO_SCHEDULE','Forward View / Upcoming Backlog Doc PRs ','admin','NO_MILESTONE',0,'PRS','synopsis, state, problem-level, category, product, planned-release, cvbc-documented-in, blocker, submitter-id, techpubs-owner, originator, systest-owner, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli, created, resolution, feedback-date, jtac-case-id,','$user : User( ) 
$record_list : RecordSet( ) from 
 collect( PRRecord( (problem_level == "1-CL1" || problem_level == "2-CL2" || problem_level == "4-CL3" || problem_level == "3-IL1" || problem_level == "4-IL2" ||  problem_level == "5-IL3"), 
    (clazz == "bug" || clazz == "unreproducible" ),functional_area=="documentation", 
    (alwaysTrue == possible_forward_view_backlog || alwaysTrue == possible_backlog), 
    npi == $user.npi_program || alwaysTrue == $user.junos || 
    backlog_pr_techpub_owner memberOf $user.reportNames )) 
',0,1,to_timestamp_tz('01-NOV-10 12.00.00.511060000 PM -07:00','DD-MON-RR HH.MI.SS.FF AM TZR'),null,null,null,null,'Forward View Backlog Doc PRs','NO_MILESTONE',0);
Insert into DASH.RULES (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) values ('backlog_prs_reference_cvbc','Reference Backlog CVBC PRs','HOLD_TO_SCHEDULE','All Reference Backlog CVBC PR','admin','NO_MILESTONE',0,'PRS','synopsis, state, cust-visible-behavior-changed, problem-level, category, product, planned-release, blocker, submitter-id, techpubs-owner, originator, systest-owner, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli, created, resolution, feedback-date, jtac-case-id,','$user : User( ) 
$record_list : RecordSet( ) 
  from collect( 
    OldPRRecord( 
        extract_date == "2010-12-31", 
        (cust_visible_behavior_changed == "yes" || cust_visible_behavior_changed == "yes-ready-for-review"), 
        alwaysTrue == possible_backlog, 
        npi == $user.npi_program || alwaysTrue == $user.junos || 
        backlog_pr_techpub_owner memberOf $user.reportNames 
    ) 
  )',0,1,to_timestamp_tz('31-DEC-10 12.00.00.511060000 PM -07:00','DD-MON-RR HH.MI.SS.FF AM TZR'),null,null,null,null,'Reference Backlog CVBC PRs ','NO_MILESTONE',0);
Insert into DASH.RULES (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) values ('backlog_prs_forward_cvbc','Forward View Backlog for CVBC PRs','HOLD_TO_SCHEDULE','Forward View / Upcoming Backlog for CVBC PRs ','admin','NO_MILESTONE',0,'PRS','synopsis, state, cust-visible-behavior-changed, problem-level, category, product, planned-release, cvbc-documented-in, blocker, submitter-id, techpubs-owner, originator, systest-owner, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli, created, resolution, feedback-date, jtac-case-id,','$user : User( ) 
$record_list : RecordSet( ) from 
 collect( UndeadCvbcRecord( 
    (alwaysTrue == possible_backlog || alwaysTrue == possible_forward_view_backlog), 
    alwaysTrue == $user.junos || 
    backlog_pr_techpub_owner memberOf $user.reportNames )) 
',0,1,to_timestamp_tz('01-NOV-10 12.00.00.511060000 PM -07:00','DD-MON-RR HH.MI.SS.FF AM TZR'),null,null,null,null,'Forward View Backlog for CVBC PRs','NO_MILESTONE',0);
Insert into DASH.RULES (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) values ('backlog_prs_current_2010_responsible_not_dev','2010 Current Backlog PRs by Responsible!=Dev-Owner','HOLD_TO_SCHEDULE','All Current Backlog PR by using responsible as their primary accountability where the responsible != dev-owner','mbasha','NO_MILESTONE',0,'PRS','synopsis, state, problem-level, category, product, planned-release, blocker, submitter-id, responsible, dev-owner, originator, systest-owner, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli, created, resolution, feedback-date, jtac-case-id,','$user : User( ) 
$record_list : RecordSet( ) from 
 collect( 
    PRRecord( 
	    (problem_level == "1-CL1" || problem_level == "2-CL2" || problem_level == "4-CL3" || problem_level == "3-IL1" || problem_level == "4-IL2" || problem_level == "5-IL3"),
	    (clazz == "bug" || clazz == "unreproducible"),functional_area=="software", 
	    alwaysTrue == possible_backlog, real_responsible != dev_owner, 
	    npi == $user.npi_program || alwaysTrue == $user.junos || 
	    backlog_pr_responsible memberOf $user.reportNames ) ) 
',0,1,to_timestamp_tz('01-NOV-10 12.00.00.511060000 PM -07:00','DD-MON-RR HH.MI.SS.FF AM TZR'),null,null,null,null,'Current Backlog PRs by Responsible by Responsible!=Dev-Owner','NO_MILESTONE',0);
Insert into DASH.RULES (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) values ('backlog_prs_forward_2010_responsible_not_dev','2010 Forward View / Future Backlog PRs by Responsible != Dev-Owner','HOLD_TO_SCHEDULE','All Future Backlog PR by using responsible as their primary accountability where the responsible != dev-owner','admin','NO_MILESTONE',0,'PRS','synopsis, state, problem-level, category, product, planned-release, blocker, submitter-id, responsible, dev-owner, originator, systest-owner, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli, created, resolution, feedback-date, jtac-case-id,','$user : User( ) 
$record_list : RecordSet( ) from 
 collect( 
    PRRecord( 
	    (problem_level == "1-CL1" || problem_level == "2-CL2" || problem_level == "4-CL3" || problem_level == "3-IL1" || problem_level == "4-IL2" ||  problem_level == "5-IL3"), 
	    (clazz == "bug" || clazz == "unreproducible" ),functional_area=="software", 
	    (alwaysTrue == possible_forward_view_backlog || alwaysTrue == possible_backlog), 
	    real_responsible != dev_owner, 
	    npi == $user.npi_program || alwaysTrue == $user.junos || 
	    backlog_pr_responsible memberOf $user.reportNames )) 
',0,1,to_timestamp_tz('01-NOV-10 12.00.00.511060000 PM -07:00','DD-MON-RR HH.MI.SS.FF AM TZR'),null,null,null,null,'Forward View / Future Backlog PRs by Responsible','NO_MILESTONE',0);
Insert into DASH.RULES (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) values ('backlog_prs_reference_2010-12-31','Reference Backlog PRs 2010-12-31','HOLD_TO_SCHEDULE','All Reference Backlog PR','admin','NO_MILESTONE',0,'PRS','synopsis, problem-level, state, problem-level, category, product, planned-release, blocker, submitter-id, responsible, dev-owner, originator, systest-owner, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli, created, resolution, feedback-date, jtac-case-id,','$user : User( ) 
$record_list : RecordSet( ) 
  from collect( 
    OldPRRecord( 
      extract_date == "2010-12-31", 
      state != "closed", 
      (problem_level == "1-CL1" || problem_level == "2-CL2" || problem_level == "4-CL3" || problem_level == "3-L1" || problem_level == "4-IL2" || problem_level == "5-CL4" || problem_level == "5-IL3"), 
      (clazz == "bug" || clazz == "unreproducible" || clazz == "duplicate" ), 
      functional_area=="software", 
      alwaysTrue == possible_backlog, 
      npi == $user.npi_program || alwaysTrue == $user.junos || backlog_pr_dev_owner memberOf $user.reportNames 
    ) 
  )',1,1,to_timestamp_tz('01-NOV-10 12.00.00.511060000 PM -07:00','DD-MON-RR HH.MI.SS.FF AM TZR'),null,'$user.setScore("backlog_prs_reference_2010-12-31","pr_backlog",$record_list);',null,null,'Reference Backlog PRs ','NO_MILESTONE',0);
Insert into DASH.RULES (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) values ('backlog_prs_reference','Reference Backlog PRs 2010-01-31','HOLD_TO_SCHEDULE','All Reference Backlog PR','admin','NO_MILESTONE',0,'PRS','synopsis, problem-level, state, problem-level, category, product, planned-release, blocker, submitter-id, responsible, dev-owner, originator, systest-owner, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli, created, resolution, feedback-date, jtac-case-id,','$user : User( ) 
$record_list : RecordSet( ) 
  from collect( 
    OldPRRecord( 
      extract_date == "2010-01-31", 
      state != "closed", 
      (problem_level == "1-CL1" || problem_level == "2-CL2" || problem_level == "4-CL3" || problem_level == "3-L1" || problem_level == "4-IL2" || problem_level == "5-CL4" || problem_level == "5-IL3"), 
      (clazz == "bug" || clazz == "unreproducible" || clazz == "duplicate" ), 
      functional_area=="software", 
      alwaysTrue == possible_backlog, 
      npi == $user.npi_program || alwaysTrue == $user.junos || backlog_pr_dev_owner memberOf $user.reportNames 
    ) 
  )',1,1,to_timestamp_tz('01-NOV-10 12.00.00.511060000 PM -07:00','DD-MON-RR HH.MI.SS.FF AM TZR'),null,'$user.setScore("backlog_prs_reference","pr_backlog",$record_list);',null,null,'Reference Backlog PRs ','NO_MILESTONE',0);
Insert into DASH.RULES (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) values ('backlog_prs_current_cvbc','Current Backlog for CVBC PRs','HOLD_TO_SCHEDULE','All Current Backlog for CVBC PRs ','admin','NO_MILESTONE',0,'PRS','synopsis, state, cust-visible-behavior-changed, problem-level, category, product, planned-release, cvbc-documented-in, blocker, submitter-id, techpubs-owner, originator, systest-owner, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli, created, resolution, feedback-date, jtac-case-id,','$user : User( ) 
$record_list : RecordSet( ) from 
 collect( UndeadCvbcRecord( 
    alwaysTrue == possible_backlog, 
    alwaysTrue == $user.junos || 
    backlog_pr_techpub_owner memberOf $user.reportNames )) 
',0,1,to_timestamp_tz('01-NOV-10 12.00.00.511060000 PM -07:00','DD-MON-RR HH.MI.SS.FF AM TZR'),null,null,null,null,'Current Backlog for CVBC PRs','NO_MILESTONE',0);
Insert into DASH.RULES (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) values ('backlog_prs_reference_responsible','Reference Backlog PRs by Responsible 2010-01-31','HOLD_TO_SCHEDULE','All Reference Backlog PR by using responsible as their primary accountability','admin','NO_MILESTONE',0,'PRS','synopsis, problem-level, state, problem-level, category, product, planned-release, blocker, submitter-id, responsible, dev-owner, originator, systest-owner, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli, created, resolution, feedback-date, jtac-case-id,','$user : User( ) 
$record_list : RecordSet( ) 
  from collect( 
    OldPRRecord( 
      extract_date == "2010-01-31", 
      state != "closed", 
      (problem_level == "1-CL1" || problem_level == "2-CL2" || problem_level == "4-CL3" || problem_level == "3-L1" || problem_level == "4-IL2" || problem_level == "5-CL4" || problem_level == "5-IL3"), 
      (clazz == "bug" || clazz == "unreproducible"  || clazz == "duplicate" ),functional_area=="software", 
      alwaysTrue == possible_backlog, 
      npi == $user.npi_program || alwaysTrue == $user.junos || backlog_pr_responsible memberOf $user.reportNames 
    ) 
  )',1,1,to_timestamp_tz('01-NOV-10 12.00.00.511060000 PM -07:00','DD-MON-RR HH.MI.SS.FF AM TZR'),null,'$user.setScore("backlog_prs_reference_responsible","pr_backlog",$record_list);',null,null,'Reference Backlog PRs ','NO_MILESTONE',0);
Insert into DASH.RULES (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) values ('backlog_prs_current_responsible','Current Backlog PRs by Responsible','HOLD_TO_SCHEDULE','All Current Backlog PR by using responsible as their primary accountability','admin','NO_MILESTONE',0,'PRS','synopsis, problem-level, state, problem-level, category, product, planned-release, blocker, submitter-id, responsible, dev-owner, originator, systest-owner, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli, created, resolution, feedback-date, jtac-case-id,','$user : User( ) 
$record_list : RecordSet( ) 
  from collect( 
    PRRecord( 
      (problem_level == "1-CL1" || problem_level == "2-CL2" || problem_level == "4-CL3" || problem_level == "3-L1" || problem_level == "4-IL2" || problem_level == "5-CL4" || problem_level == "5-IL3"), 
      (clazz == "bug" || clazz == "unreproducible" || clazz == "duplicate" ),functional_area=="software", 
      alwaysTrue == possible_backlog, 
      npi == $user.npi_program || alwaysTrue == $user.junos || 
      backlog_pr_responsible memberOf $user.reportNames 
    ) 
  )',0,1,to_timestamp_tz('01-NOV-10 12.00.00.511060000 PM -07:00','DD-MON-RR HH.MI.SS.FF AM TZR'),null,'$user.setScore("backlog_prs_current_responsible","pr_backlog",$record_list);',null,null,'Reference Backlog PRs ','NO_MILESTONE',0);
Insert into DASH.RULES (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) values ('backlog_prs_forward_responsible','Forward View / Future Backlog PRs by Responsible','HOLD_TO_SCHEDULE','All Future Backlog PR by using responsible as their primary accountability','admin','NO_MILESTONE',0,'PRS','synopsis, problem-level, state, problem-level, category, product, planned-release, blocker, submitter-id, responsible, dev-owner, originator, systest-owner, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli, created, resolution, feedback-date, jtac-case-id,','$user : User( ) 
$record_list : RecordSet( ) 
  from collect( 
    PRRecord( 
      (problem_level == "1-CL1" || problem_level == "2-CL2" || problem_level == "4-CL3" || problem_level == "3-L1" || problem_level == "4-IL2" || problem_level == "5-CL4" || problem_level == "5-IL3"), 
      (clazz == "bug" || clazz == "unreproducible"  || clazz == "duplicate" ),functional_area=="software", 
      (alwaysTrue == possible_forward_view_backlog || alwaysTrue == possible_backlog), 
      npi == $user.npi_program || alwaysTrue == $user.junos || 
      backlog_pr_responsible memberOf $user.reportNames 
    ) 
  )',1,1,to_timestamp_tz('01-NOV-10 12.00.00.511060000 PM -07:00','DD-MON-RR HH.MI.SS.FF AM TZR'),null,'$user.setScore("backlog_prs_forward_responsible","pr_backlog",$record_list);',null,null,'Reference Backlog PRs ','NO_MILESTONE',0);
Insert into DASH.RULES (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) values ('backlog_prs_current','Current Backlog PRs','HOLD_TO_SCHEDULE','All current backlog PR -- up to date','admin','NO_MILESTONE',0,'PRS','synopsis, problem-level, state, problem-level, category, product, planned-release, blocker, submitter-id, responsible, dev-owner, originator, systest-owner, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli, created, resolution, feedback-date, jtac-case-id,','$user : User( ) 
$record_list : RecordSet( ) 
  from collect( 
    PRRecord( 
    (problem_level == "1-CL1" || problem_level == "2-CL2" || problem_level == "4-CL3" || problem_level == "3-L1" || problem_level == "4-IL2" || problem_level == "5-CL4" || problem_level == "5-IL3"), 
    (clazz == "bug" || clazz == "unreproducible" || clazz == "duplicate" ), 
    functional_area=="software", 
    alwaysTrue == possible_backlog, 
    npi == $user.npi_program || alwaysTrue == $user.junos || backlog_pr_dev_owner memberOf $user.reportNames 
    ) 
  )',0,1,to_timestamp_tz('01-NOV-10 12.00.00.511060000 PM -07:00','DD-MON-RR HH.MI.SS.FF AM TZR'),null,'$user.setScore("backlog_prs_current","pr_backlog",$record_list);',null,null,'Reference Backlog PRs ','NO_MILESTONE',0);
Insert into DASH.RULES (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) values ('backlog_prs_forward','Forward View Backlog PR','HOLD_TO_SCHEDULE','PRs which will be counted as current backlog in next R1 deploy','admin','NO_MILESTONE',0,'PRS','synopsis, problem-level, state, problem-level, category, product, planned-release, blocker, submitter-id, responsible, dev-owner, originator, systest-owner, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli, created, resolution, feedback-date, jtac-case-id,','$user : User( ) 
$record_list : RecordSet( ) 
  from collect( 
    PRRecord( 
      (problem_level == "1-CL1" || problem_level == "2-CL2" || problem_level == "4-CL3" || problem_level == "3-L1" || problem_level == "4-IL2" || problem_level == "5-CL4" || problem_level == "5-IL3"), 
      (clazz == "bug" || clazz == "unreproducible" || clazz == "duplicate" ),functional_area=="software", 
      (alwaysTrue == possible_forward_view_backlog || alwaysTrue == possible_backlog), 
      npi == $user.npi_program || alwaysTrue == $user.junos || 
      backlog_pr_dev_owner memberOf $user.reportNames 
    ) 
  )',0,1,to_timestamp_tz('01-NOV-10 12.00.00.511060000 PM -07:00','DD-MON-RR HH.MI.SS.FF AM TZR'),null,'$user.setScore("backlog_prs_forward","pr_backlog",$record_list);',null,null,'Reference Backlog PRs ','NO_MILESTONE',0);
Insert into DASH.RULES (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) values ('backlog_prs_reference_responsible_not_dev','RReference Backlog PRs by Responsible != Dev-Owner 2010-01-31','HOLD_TO_SCHEDULE','All Reference Backlog PR by using responsible as their primary accountability where the responsible != dev-owner','admin','NO_MILESTONE',0,'PRS','synopsis, problem-level, state, problem-level, category, product, planned-release, blocker, submitter-id, responsible, dev-owner, originator, systest-owner, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli, created, resolution, feedback-date, jtac-case-id,','$user : User( ) 
$record_list : RecordSet( ) from 
 collect( 
    OldPRRecord( 
        extract_date == "2010-01-31", 
        state != "closed", 
        (problem_level == "1-CL1" || problem_level == "2-CL2" || problem_level == "4-CL3" || problem_level == "3-L1" || problem_level == "4-IL2" || problem_level == "5-CL4" || problem_level == "5-IL3"), 
        (clazz == "bug" || clazz == "unreproducible"  || clazz == "duplicate" ),functional_area=="software", 
        alwaysTrue == possible_backlog, 
        real_responsible != dev_owner, 
        npi == $user.npi_program || alwaysTrue == $user.junos || 
        backlog_pr_responsible memberOf $user.reportNames )) 
',1,1,to_timestamp_tz('01-NOV-10 12.00.00.511060000 PM -07:00','DD-MON-RR HH.MI.SS.FF AM TZR'),null,'$user.setScore("backlog_prs_reference_responsible_not_dev","pr_backlog",$record_list);',null,null,'Reference Backlog PRs by Responsible != Dev-Owner','NO_MILESTONE',0);
Insert into DASH.RULES (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) values ('backlog_prs_current_responsible_not_dev','Current Backlog PRs by Responsible!=Dev-Owner','HOLD_TO_SCHEDULE','All Current Backlog PR by using responsible as their primary accountability where the responsible != dev-owner','admin','NO_MILESTONE',0,'PRS','synopsis, problem-level, state, problem-level, category, product, planned-release, blocker, submitter-id, responsible, dev-owner, originator, systest-owner, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli, created, resolution, feedback-date, jtac-case-id,','$user : User( ) 
$record_list : RecordSet( ) from 
 collect( 
    PRRecord( 
        (problem_level == "1-CL1" || problem_level == "2-CL2" || problem_level == "4-CL3" || problem_level == "3-L1" || problem_level == "4-IL2" || problem_level == "5-CL4" || problem_level == "5-IL3"), 
        (clazz == "bug" || clazz == "unreproducible"  || clazz == "duplicate" ),functional_area=="software", 
        alwaysTrue == possible_backlog, real_responsible != dev_owner, 
        npi == $user.npi_program || alwaysTrue == $user.junos || 
        backlog_pr_responsible memberOf $user.reportNames ) ) 
',0,1,to_timestamp_tz('01-NOV-10 12.00.00.511060000 PM -07:00','DD-MON-RR HH.MI.SS.FF AM TZR'),null,'$user.setScore("backlog_prs_current_responsible_not_dev","pr_backlog",$record_list);',null,null,'Current Backlog PRs by Responsible by Responsible!=Dev-Owner','NO_MILESTONE',0);
Insert into DASH.RULES (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) values ('backlog_prs_current_2010','2010 Current Backlog PRs','HOLD_TO_SCHEDULE','All current backlog PR -- up to date','admin','NO_MILESTONE',0,'PRS','synopsis, state, problem-level, category, product, planned-release, blocker, submitter-id, responsible, dev-owner, originator, systest-owner, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli, created, resolution, feedback-date, jtac-case-id,','$user : User( ) 
$record_list : RecordSet( ) 
  from collect( 
    PRRecord( 
    (problem_level == "1-CL1" || problem_level == "2-CL2" || problem_level == "4-CL3" || problem_level == "3-IL1" || problem_level == "4-IL2" || problem_level == "5-IL3"),
    (clazz == "bug" || clazz == "unreproducible"), 
    functional_area=="software", 
    alwaysTrue == possible_backlog, 
    npi == $user.npi_program || alwaysTrue == $user.junos || backlog_pr_dev_owner memberOf $user.reportNames 
    ) 
  )',0,1,to_timestamp_tz('01-NOV-10 12.00.00.511060000 PM -07:00','DD-MON-RR HH.MI.SS.FF AM TZR'),null,null,null,null,'Reference Backlog PRs ','NO_MILESTONE',0);
Insert into DASH.RULES (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) values ('prs_user_dev_branch','Development Branch PRs','HOLD_TO_SCHEDULE','Development Branch PRs assign to user as their responsible or Dev-owner','admin','NO_MILESTONE',0,'PRS','synopsis, state, problem-level, category, product, planned-release, branch, blocker, submitter-id, responsible, dev-owner, originator, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli,','$user : User( )  
$release : ReleaseRecord( )  
$record_list : RecordSet( ) from  
 collect( PRRecord( (branchRelease == $release.relname &&  branchReleaseType=="DEV"), planned_release not matches "^1[1-9]\.[0-9]W[0-9]*", 
  npi == $user.npi_program ||  
  alwaysTrue == $user.junos ||   
  responsible memberOf $user.reportNames ||  
  (dev_owner memberOf $user.reportNames &&  state != "feedback")))',1,1,to_timestamp_tz('08-JUN-10 03.38.39.897156000 PM -07:00','DD-MON-RR HH.MI.SS.FF AM TZR'),'release',null,null,null,'Development Branch PRs assign to user as their responsible or Dev-owner','NO_MILESTONE',0);
Insert into DASH.RULES (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) values ('backlog_prs_current_2010_responsible','2010 Current Backlog PRs by Responsible','HOLD_TO_SCHEDULE','All Current Backlog PR by using responsible as their primary accountability','admin','NO_MILESTONE',0,'PRS','synopsis, state, problem-level, category, product, planned-release, blocker, submitter-id, responsible, dev-owner, originator, systest-owner, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli, created, resolution, feedback-date, jtac-case-id,','$user : User( ) 
$record_list : RecordSet( ) 
  from collect( 
    PRRecord( 
      (problem_level == "1-CL1" || problem_level == "2-CL2" || problem_level == "4-CL3" || problem_level == "3-IL1" || problem_level == "4-IL2" ||  problem_level == "5-IL3"), 
      (clazz == "bug" || clazz == "unreproducible"),functional_area=="software", 
      alwaysTrue == possible_backlog, 
      npi == $user.npi_program || alwaysTrue == $user.junos || 
      backlog_pr_responsible memberOf $user.reportNames 
    ) 
  )',0,1,to_timestamp_tz('01-NOV-10 12.00.00.511060000 PM -07:00','DD-MON-RR HH.MI.SS.FF AM TZR'),null,null,null,null,'Reference Backlog PRs ','NO_MILESTONE',0);
Insert into DASH.RULES (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) values ('dev_compl_large_rlis_wo_utp','Dev-complete Large RLIs without needed UTP','P2_EXIT','To determine readiness for system test, if a Unit Test Plan (UTP) is 
required, it must be reviewed and approved.','lbirch','NO_MILESTONE',0,'RLIS','synopsis, state, sw_proj_len, rli_class, project_status, responsible, sw_mgr_responsible, sw_responsible, unit_test_plan_status, unit_test_plan, ','$release : ReleaseRecord( hasOpenRLIs == true ) 
$user : User( ) 
$record_list : RecordSet( ) from 
 collect( RLIRecord( relname == $release.relname, $ds : dataSource, 
  state != "deferred", 
  sw_proj_len in ("large", "major", "expansive", ""), 
  npi_program == $user.npi_program_name || 
  alwaysTrue == $user.junos || responsible memberOf $user.reportNames || 
  sw_mgr_responsible memberOf $user.reportNames || 
  st_mgr_responsible memberOf $user.reportNames || 
  sw_responsible == $user.id || st_responsible == $user.id, 
  project_status_ordinal >= ($ds.picklistOrdinal("project_status", "dev-complete")), 
  unit_test_plan_status_ordinal < ($ds.picklistOrdinal("unit_test_plan_status", "reviewed-complete")) ) )',1,0,to_timestamp_tz('10-APR-09 02.51.54.940389000 PM -07:00','DD-MON-RR HH.MI.SS.FF AM TZR'),'release',null,null,null,'To determine readiness for system test, if a Unit Test Plan (...','P2_EXIT',-30);
Insert into DASH.RULES (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) values ('dev_compl_small_rlis_wo_utp','Dev-complete Small RLIs without needed UTP','P2_EXIT','To determine readiness for system test, if a Unit Test Plan (UTP) is 
required, it must be reviewed and approved.','lbirch','NO_MILESTONE',0,'RLIS','synopsis, state, sw_proj_len, rli_class, project_status, responsible, sw_mgr_responsible, sw_responsible, unit_test_plan_status, unit_test_plan, ','$release : ReleaseRecord( hasOpenRLIs == true ) 
$user : User( ) 
$record_list : RecordSet( ) from 
 collect( RLIRecord( relname == $release.relname, $ds : dataSource, 
  state != "deferred", 
  sw_proj_len in ("small", "tiny"), 
  npi_program == $user.npi_program_name || 
  alwaysTrue == $user.junos || responsible memberOf $user.reportNames || 
  sw_mgr_responsible memberOf $user.reportNames || 
  st_mgr_responsible memberOf $user.reportNames || 
  sw_responsible == $user.id || st_responsible == $user.id, 
  project_status_ordinal >= ($ds.picklistOrdinal("project_status", "dev-complete")), 
  unit_test_plan_status_ordinal < ($ds.picklistOrdinal("unit_test_plan_status", "reviewed-complete")) ) )',1,0,to_timestamp_tz('10-APR-09 02.52.41.172852000 PM -07:00','DD-MON-RR HH.MI.SS.FF AM TZR'),'release',null,null,null,'To determine readiness for system test, if a Unit Test Plan (...','P2_EXIT',-30);
Insert into DASH.RULES (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) values ('cidb_prs','Legacy CIDB PRs','P5_STABILITY','Change in Default Behavior PRs were formerly used to notify Technical Publications of any changes introduced by a PR that need to be documented for the customer.  Any remaining CIDB PRs are probably long overdue, and should be closed out promptly.  When working with these PRs, please remove the auto-gen keyword.','june','NO_MILESTONE',0,'PRS','synopsis, state, problem-level, category, product, planned-release, blocker, submitter-id, responsible, dev-owner, originator, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli,','$user : User( junos == false ) 
$record_list : RecordSet( ) from 
 collect( PRRecord( cust_visible_behavior_changed == "", 
  keywords matches ".*auto-gen:default-changed.*", 
  techpubs_owner memberOf $user.reportNames ) )',3,1,to_timestamp_tz('06-JUL-09 01.54.07.815742000 PM -07:00','DD-MON-RR HH.MI.SS.FF AM TZR'),null,null,null,null,'CIDB PRs need to be documented for the customer','NO_MILESTONE',0);
Insert into DASH.RULES (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) values ('escalation_prs','Escalation PRs','HOLD_TO_SCHEDULE','Owners of Escalated PRs need to try and resolve these issues as soon as 
possible.  Customers have raised the priority on these PRs through JTAC. 
If you do not have the time to work on the escalated PRs assigned to you, 
notify your manager.','rdhiman','NO_MILESTONE',0,'PRS','synopsis, state, problem-level, category, product, planned-release, blocker, submitter-id, responsible, dev-owner, originator, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli,','$user : User( ) 
$record_list : RecordSet( ) from 
 collect( PRRecord(customer_escalation == true, npi == $user.npi_program || 
  alwaysTrue == $user.junos || responsible memberOf $user.reportNames || 
  ( dev_owner memberOf $user.reportNames &&  state != "feedback" ) ) )',10,1,to_timestamp_tz('03-SEP-08 10.41.47.868359000 AM -07:00','DD-MON-RR HH.MI.SS.FF AM TZR'),null,null,null,null,'Owners of Escalated PRs need to try and resolve these issues ...','NO_MILESTONE',null);
Insert into DASH.RULES (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) values ('rlis_wo_tp_size_in_release','RLIs without TP Size','P2_EXIT','The TP size field shows the estimated size of the work that needs to be performed by Techpubs for this RLI. Includes unknown, no_doc, S, M, L, XL, XXL.','mwazna','FUNC_SPEC_COMPLETE',-42,'RLIS','synopsis, tp_size, functional_specification, tp_responsible, tp_lead_responsible, tp_release_status, ','$release : ReleaseRecord( ) 
$user : User( ) 
$record_list : RecordSet( ) from 
 collect( RLIRecord( tp_size == "unknown",  
  tp_approval_status != "n/a", 
  tp_release_status != "no_doc", 
  state != "deferred", 
  relname == $release.relname, 
  alwaysTrue != $user.junos &&  
  (tp_responsible memberOf $user.reportNames || 
  tp_lead_responsible memberOf $user.reportNames || 
  tp_mgr_responsible memberOf $user.reportNames) ) )',2,1,to_timestamp_tz('15-JAN-09 04.42.11.809215000 PM -08:00','DD-MON-RR HH.MI.SS.FF AM TZR'),'release',null,null,null,'A size estimate is needed for release planning.','NO_MILESTONE',0);
Insert into DASH.RULES (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) values ('docs_not_frs_ready','RLIs with Docs not FRS ready','P4_EXIT','Document is not ready for release to production/printer/web.','mwazna','P4_EXIT',14,'RLIS','synopsis, tp_status, tp_edit_status, tp_release_status, tp_responsible, ','$release : ReleaseRecord( ) 
$user : User( ) 
$record_list : RecordSet( ) from 
 collect( RLIRecord( tp_release_status not in ("doc_frs_ready", "no_doc"), 
  state != "deferred", 
  relname == $release.relname, 
  tp_responsible not in ("", "n/a", "N/A", "NA"), 
  alwaysTrue != $user.junos &&  
  (tp_responsible memberOf $user.reportNames || 
  tp_lead_responsible memberOf $user.reportNames || 
  tp_mgr_responsible memberOf $user.reportNames) ) )',4,1,to_timestamp_tz('15-JAN-09 04.42.11.991920000 PM -08:00','DD-MON-RR HH.MI.SS.FF AM TZR'),'release',null,null,null,'Document is not ready for release to production/printer/web.','NO_MILESTONE',0);
Insert into DASH.RULES (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) values ('minor_prs','Minor PRs','HOLD_TO_SCHEDULE','Owners of PRs with the [Severity] field set to "minor" must eventually fix them.','admin','NO_MILESTONE',0,'PRS','synopsis, state, problem-level, category, product, planned-release, blocker, submitter-id, responsible, dev-owner, originator, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli,','$release : ReleaseRecord( )  
$user : User( )  
$record_list : RecordSet( ) from  
 collect( PRRecord( problem_level in ("5-CL4","6-IL4"),  
  releases contains $release.relname, planned_release not matches "^1[1-9]\.[0-9]W[0-9]*", npi == $user.npi_program ||  
  alwaysTrue == $user.junos || responsible memberOf $user.reportNames ||  
  ( dev_owner memberOf $user.reportNames &&  state != "feedback" ) ) )',0,1,to_timestamp_tz('24-APR-09 04.33.20.436866000 PM -07:00','DD-MON-RR HH.MI.SS.FF AM TZR'),'release',null,null,null,'Someone should address these PRs one of these days','NO_MILESTONE',0);
Insert into DASH.RULES (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) values ('prs_wo_release','PRs without a release','HOLD_TO_SCHEDULE','These PRs have no Conf-Committed-Release, Committed-Release, Release or Reported-In value.','admin','NO_MILESTONE',0,'PRS','synopsis, state, problem-level, category, product, planned-release, blocker, submitter-id, responsible, dev-owner, originator, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli,','$user : User( )  
$record_list : RecordSet( ) from  
 collect( PRRecord( $rels : releases -> ( $rels.size() == 1 ), planned_release not matches "^1[1-9]\.[0-9]W[0-9]*", 
  releases contains "0.0",  
  npi == $user.npi_program ||  
  alwaysTrue == $user.junos || responsible memberOf $user.reportNames ) )',0,1,to_timestamp_tz('13-JUN-09 08.21.14.070791000 PM -07:00','DD-MON-RR HH.MI.SS.FF AM TZR'),null,null,null,null,'PRs that dont have a release defined','NO_MILESTONE',0);
Insert into DASH.RULES (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) values ('rli_with_green_status_plan','RLIs considered have ''GREEN'' status plan','HOLD_TO_SCHEDULE','RLIs considered have ''GREEN'' status plan and used in RLI reporting','admin','NO_MILESTONE',0,'RLIS','synopsis, release_target, cam_status, state, responsible, sw_mgr_responsible, sw_responsible, st_responsible, st_mgr_responsible, plm_responsible, tp_mgr_responsible, tp_lead_responsible, tp_responsible, ','$release : ReleaseRecord(hasOpenRLIs==true) 
$user : User( ) 
$record_list : RecordSet( ) from 
 collect( RLIRecord( ( relname == $release.relname || relname memberOf $release.childBranch), 
  product_test_plan_status in ("reviewed-complete","not-required"), 
  npi_program == $user.npi_program_name || 
  alwaysTrue == $user.junos || responsible memberOf $user.reportNames || 
    sw_mgr_responsible memberOf $user.reportNames || 
    st_mgr_responsible memberOf $user.reportNames ) )',0,1,to_timestamp_tz('01-MAR-10 10.10.05.126408000 PM -08:00','DD-MON-RR HH.MI.SS.FF AM TZR'),'release',null,null,'no_show','RLIs considered have ''GREEN'' status plan','NO_MILESTONE',0);
Insert into DASH.RULES (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) values ('rli_with_red_status_plan','RLIs considered have ''RED'' status plan','HOLD_TO_SCHEDULE','RLIs considered have ''RED'' status plan and used in RLI reporting','admin','NO_MILESTONE',0,'RLIS','synopsis, release_target, cam_status, state, responsible, sw_mgr_responsible, sw_responsible, st_responsible, st_mgr_responsible, plm_responsible, tp_mgr_responsible, tp_lead_responsible, tp_responsible, ','$release : ReleaseRecord(hasOpenRLIs==true ) 
$user : User( ) 
$record_list : RecordSet( ) from 
 collect( RLIRecord( ( relname == $release.relname || relname memberOf $release.childBranch), 
  product_test_plan_status in ("required-missing", "not-reviewed-incomplete"), 
  npi_program == $user.npi_program_name || 
  alwaysTrue == $user.junos || responsible memberOf $user.reportNames || 
    sw_mgr_responsible memberOf $user.reportNames || 
    st_mgr_responsible memberOf $user.reportNames ) )',0,1,to_timestamp_tz('01-MAR-10 10.08.38.789021000 PM -08:00','DD-MON-RR HH.MI.SS.FF AM TZR'),'release',null,null,'no_show','RLIs considered have ''RED'' status plan','NO_MILESTONE',0);
Insert into DASH.RULES (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) values ('rli_complete_criteria_met','RLIs with complete criteria is met','HOLD_TO_SCHEDULE','RLIs with complete criteria is met','admin','NO_MILESTONE',0,'RLIS','synopsis, release_target, cam_status, state, responsible, sw_mgr_responsible, sw_responsible, st_responsible, st_mgr_responsible, plm_responsible, tp_mgr_responsible, tp_lead_responsible, tp_responsible, ','$release : ReleaseRecord( hasOpenRLIs == true ) 
$user : User( ) 
$record_list : RecordSet( ) from 
 collect( RLIRecord( ( relname == $release.relname || relname memberOf $release.childBranch), 
  cam_status == "accepted", 
  npi_program == $user.npi_program_name || 
  alwaysTrue == $user.junos || responsible memberOf $user.reportNames || 
  sw_mgr_responsible memberOf $user.reportNames || 
  st_mgr_responsible memberOf $user.reportNames || 
    sw_responsible == $user.id || st_responsible == $user.id ) )',0,1,to_timestamp_tz('01-MAR-10 10.12.32.717180000 PM -08:00','DD-MON-RR HH.MI.SS.FF AM TZR'),'release',null,null,null,'RLIs with complete criteria is met','NO_MILESTONE',0);
Insert into DASH.RULES (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) values ('rli_with_yellow_status_plan','RLIs considered have ''YELLOW'' status plan','HOLD_TO_SCHEDULE','RLIs considered have ''YELLOW'' status plan and used in RLI reporting','admin','NO_MILESTONE',0,'RLIS','synopsis, release_target, cam_status, state, responsible, sw_mgr_responsible, sw_responsible, st_responsible, st_mgr_responsible, plm_responsible, tp_mgr_responsible, tp_lead_responsible, tp_responsible, ','$release : ReleaseRecord(hasOpenRLIs==true) 
$user : User( ) 
$record_list : RecordSet( ) from 
 collect( RLIRecord( ( relname == $release.relname || relname memberOf $release.childBranch), 
  product_test_plan_status == "reviewed-incomplete", 
  npi_program == $user.npi_program_name || 
  alwaysTrue == $user.junos || responsible memberOf $user.reportNames || 
    sw_mgr_responsible memberOf $user.reportNames || 
    st_mgr_responsible memberOf $user.reportNames ) )',0,1,to_timestamp_tz('01-MAR-10 10.09.21.038408000 PM -08:00','DD-MON-RR HH.MI.SS.FF AM TZR'),'release',null,null,'no_show','RLIs considered have ''YELLOW'' status plan','NO_MILESTONE',0);
Insert into DASH.RULES (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) values ('rlis_user_scoping','RLIs in scoping and planning','HOLD_TO_SCHEDULE','RLIs in scoping and planning','admin','NO_MILESTONE',0,'RLIS','synopsis, release_target, p1_release_target, state, responsible, sw_mgr_responsible, sw_responsible, st_responsible, st_mgr_responsible, plm_responsible, tp_mgr_responsible, tp_lead_responsible, tp_responsible, ','$release : ReleaseRecord( hasOpenRLIs == true ) 
  $user : User( ) 
  $record_list : RecordSet( ) from 
   collect( RLIRecord( p1relname == $release.relname, 
    npi_program == $user.npi_program_name || 
    alwaysTrue == $user.junos || responsible memberOf $user.reportNames || 
    sw_mgr_responsible memberOf $user.reportNames || 
    st_mgr_responsible memberOf $user.reportNames || 
    sw_responsible == $user.id || st_responsible == $user.id || 
    plm_responsible memberOf $user.reportNames || 
    tp_responsible memberOf $user.reportNames || 
    tp_lead_responsible memberOf $user.reportNames || 
    tp_mgr_responsible memberOf $user.reportNames ) )',0,1,to_timestamp_tz('01-MAR-10 10.13.46.474343000 PM -08:00','DD-MON-RR HH.MI.SS.FF AM TZR'),'release',null,null,null,'RLIs in scoping and planning','NO_MILESTONE',0);
Insert into DASH.RULES (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) values ('branch_dev','Dev Branches','HOLD_TO_SCHEDULE','Total Dev Branches on Release','admin','NO_MILESTONE',0,'BTS','planned-release, branch_name, branch_type, state, responsible,','$release :ReleaseRecord( ) 
  $user : User( ) 
  $record_list : RecordSet( ) from 
   collect( BTRecord(  
          relname == $release.relname,  
          branch_type == "dev", planned_release not matches "^1[1-9]\.[0-9]W[0-9]*", 
          responsible memberOf $user.reportNames || alwaysTrue == $user.junos ) )',1,1,to_timestamp_tz('01-MAR-10 10.25.07.000000000 PM -08:00','DD-MON-RR HH.MI.SS.FF AM TZR'),'release',null,null,null,'Dev Branches','NO_MILESTONE',0);
Insert into DASH.RULES (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) values ('prs_user_private_branch','Private Branch PRs','HOLD_TO_SCHEDULE','PRs for Private Branch PRs','admin','NO_MILESTONE',0,'PRS','synopsis, state, problem-level, category, product, planned-release, branch, blocker, submitter-id, responsible, dev-owner, originator, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli,','$user : User( )  
$record_list : RecordSet( ) from  
 collect( PRRecord( (branchReleaseType=="PVT"), planned_release not matches "^1[1-9]\.[0-9]W[0-9]*", 
  npi == $user.npi_program ||  
  alwaysTrue == $user.junos ||   
  responsible memberOf $user.reportNames ||  
  (dev_owner memberOf $user.reportNames &&  state != "feedback")))',1,1,to_timestamp_tz('08-JUN-10 03.40.47.171349000 PM -07:00','DD-MON-RR HH.MI.SS.FF AM TZR'),null,null,null,null,'PRs for Private Branch PRs','NO_MILESTONE',0);
Insert into DASH.RULES (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) values ('rlis_wo_release','RLIs without a release','P5_STABILITY','These RLIs are not yet assigned to a release.','admin','NO_MILESTONE',0,'RLIS','synopsis, state, release_target, responsible, category, ','$user : User( ) 
$record_list : RecordSet( ) from 
 collect( RLIRecord( release_target in ("unscheduled", "n/a"), 
  p1_release_target in ("unscheduled", "n/a"), 
  npi_program == $user.npi_program_name || alwaysTrue == $user.junos || 
  responsible memberOf $user.reportNames || 
  sw_mgr_responsible memberOf $user.reportNames || 
  st_mgr_responsible memberOf $user.reportNames ) )',0,1,to_timestamp_tz('01-MAR-10 10.28.24.688609000 PM -08:00','DD-MON-RR HH.MI.SS.FF AM TZR'),null,null,null,null,'RLIs with Release Target of "unscheduled" or "n/a"','NO_MILESTONE',0);
Insert into DASH.RULES (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) values ('blocker_prs','All Blocker PRs','HOLD_TO_SCHEDULE','Owners of PRs with the [Blocking-Release] field set must 
prioritize these ETAs and fixes over non-blocker PRs.  This rule includes all non-suspended PRs (systest metrics exclude analyzed).','gcapolupo','NO_MILESTONE',0,'PRS','synopsis, state, problem-level, category, product, planned-release, blocker, submitter-id, responsible, dev-owner, originator, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli,','$release : ReleaseRecord( ) 
$user : User( ) 
$record_list : RecordSet( ) from 
 collect( PRRecord( blocked_release == $release.relname, 
  npi == $user.npi_program || 
  alwaysTrue == $user.junos || responsible memberOf $user.reportNames || 
( dev_owner memberOf $user.reportNames &&  state != "feedback" )) )',0,1,to_timestamp_tz('08-JAN-10 06.34.43.131298000 PM -08:00','DD-MON-RR HH.MI.SS.FF AM TZR'),'release',null,null,null,'Includes all non-suspended PRs.','NO_MILESTONE',0);
Insert into DASH.RULES (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) values ('critical_prs','Critical PRs','HOLD_TO_SCHEDULE','Owners of PRs with the [Severity] field set to ''critical'' must 
prioritize these ETAs and fixes over non-critical PRs.','mbasha','NEXT_BUILD',45,'PRS','synopsis, state, problem-level, category, product, planned-release, blocker, submitter-id, responsible, dev-owner, originator, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli,','$release : ReleaseRecord( ) 
$user : User( ) 
$record_list : RecordSet( ) from 
 collect( PRRecord( problem_level == "1-CL1" || problem_level == "3-IL1", 
  releases contains $release.relname, npi == $user.npi_program || 
  alwaysTrue == $user.junos || responsible memberOf $user.reportNames || 
  ( dev_owner memberOf $user.reportNames &&  state != "feedback" ) ) )',3,1,to_timestamp_tz('10-JUN-09 07.55.50.081158000 PM -07:00','DD-MON-RR HH.MI.SS.FF AM TZR'),'release',null,null,null,'Owners of PRs with the [Severity] field set to ''critical'' mus...','NEXT_DEPLOY',-7);
Insert into DASH.RULES (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) values ('no_milestones_blocker_prs','No Milestone Blocker PRs','HOLD_TO_SCHEDULE','To get non-milestone blocker PRs','admin','NO_MILESTONE',0,'PRS','synopsis, state, problem-level, category, product, planned-release, blocker, submitter-id, responsible, dev-owner, originator, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli,','$release : ReleaseRecord( )  
$user : User( )  
$record_list : RecordSet( ) from  
 collect( PRRecord( blocked_release == $release.relname, planned_release not matches "^1[1-9]\.[0-9]W[0-9]*", 
  alwaysTrue == not_milestones_release, npi == $user.npi_program ||  
  alwaysTrue == $user.junos || responsible memberOf $user.reportNames ||  
  ( dev_owner memberOf $user.reportNames &&  state != "feedback" ) ) )',1,1,to_timestamp_tz('03-MAY-10 06.29.02.025723000 PM -07:00','DD-MON-RR HH.MI.SS.FF AM TZR'),'release',null,null,null,'To get non-milestone blocker PRs','NO_MILESTONE',0);
Insert into DASH.RULES (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) values ('b1_blocker_prs','B1 Blocker PRs','HOLD_TO_SCHEDULE','Owners of PRs with the [Blocking-Release] field set must 
prioritize these ETAs and fixes over non-blocker PRs.  This rule includes all non-suspended PRs (systest metrics exclude analyzed).','gcapolupo','B1_BUILD',45,'PRS','synopsis, state, problem-level, category, product, planned-release, blocker, submitter-id, responsible, dev-owner, originator, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli,','$release : ReleaseRecord( ) 
$user : User( ) 
$record_list : RecordSet( )  
 from collect( PRRecord( blocked_release == $release.relname, 
  blocker_types contains "b1", npi == $user.npi_program || 
  alwaysTrue == $user.junos || responsible memberOf $user.reportNames || 
  ( dev_owner memberOf $user.reportNames &&  state != "feedback" ) ) )',3,1,to_timestamp_tz('14-MAY-09 07.56.00.467441000 PM -07:00','DD-MON-RR HH.MI.SS.FF AM TZR'),'release',null,null,null,'Includes all non-suspended PRs.','B1_DEPLOY',-2);
Insert into DASH.RULES (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) values ('b2_blocker_prs','B2 Blocker PRs','HOLD_TO_SCHEDULE','Owners of PRs with the [Blocking-Release] field set must 
prioritize these ETAs and fixes over non-blocker PRs.  This rule includes all non-suspended PRs (systest metrics exclude analyzed).','gcapolupo','B2_BUILD',21,'PRS','synopsis, state, problem-level, category, product, planned-release, blocker, submitter-id, responsible, dev-owner, originator, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli,','$release : ReleaseRecord( ) 
$user : User( ) 
$record_list : RecordSet( )  
 from collect( PRRecord( blocked_release == $release.relname, 
  blocker_types contains "b2", npi == $user.npi_program || 
  alwaysTrue == $user.junos || responsible memberOf $user.reportNames || 
  ( dev_owner memberOf $user.reportNames &&  state != "feedback" ) ) )',3,1,to_timestamp_tz('14-MAY-09 08.02.58.755258000 PM -07:00','DD-MON-RR HH.MI.SS.FF AM TZR'),'release',null,null,null,'Includes all non-suspended PRs.','B2_DEPLOY',-2);
Insert into DASH.RULES (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) values ('b3_blocker_prs','B3 Blocker PRs','HOLD_TO_SCHEDULE','Owners of PRs with the [Blocking-Release] field set must 
prioritize these ETAs and fixes over non-blocker PRs.  This rule includes all non-suspended PRs (systest metrics exclude analyzed).','gcapolupo','B3_BUILD',21,'PRS','synopsis, state, problem-level, category, product, planned-release, blocker, submitter-id, responsible, dev-owner, originator, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli,','$release : ReleaseRecord( ) 
$user : User( ) 
$record_list : RecordSet( )  
 from collect( PRRecord( blocked_release == $release.relname, 
  blocker_types contains "b3", npi == $user.npi_program || 
  alwaysTrue == $user.junos || responsible memberOf $user.reportNames || 
  ( dev_owner memberOf $user.reportNames &&  state != "feedback" ) ) )',3,1,to_timestamp_tz('14-MAY-09 07.57.10.460935000 PM -07:00','DD-MON-RR HH.MI.SS.FF AM TZR'),'release',null,null,null,'Includes all non-suspended PRs.','B3_DEPLOY',-7);
Insert into DASH.RULES (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) values ('major_prs','Major PRs','HOLD_TO_SCHEDULE','Major PRs should be prioritized over minor ones.','admin','NO_MILESTONE',0,'PRS','synopsis, state, problem-level, category, product, planned-release, blocker, submitter-id, responsible, dev-owner, originator, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli,','$release : ReleaseRecord( )  
$user : User( )  
$record_list : RecordSet( ) from  
 collect( PRRecord( problem_level in ("2-CL2","4-CL3","4-IL2","5-IL3"),  
  releases contains $release.relname,  planned_release not matches "^1[1-9]\.[0-9]W[0-9]*", npi == $user.npi_program ||  
  alwaysTrue == $user.junos || responsible memberOf $user.reportNames ||  
  ( dev_owner memberOf $user.reportNames &&  state != "feedback" ) ) )',0,1,to_timestamp_tz('24-APR-09 04.36.56.943451000 PM -07:00','DD-MON-RR HH.MI.SS.FF AM TZR'),'release',null,null,null,'Major PRs should be prioritized over minor ones.','NO_MILESTONE',0);
Insert into DASH.RULES (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) values ('blocker_prs_fix','All Blocker PRs Fix','HOLD_TO_SCHEDULE','Owners of PRs with the [Blocking-Release] field set must 
prioritize these ETAs and fixes over non-blocker PRs.  This rule includes all non-suspended PRs (systest metrics exclude analyzed).','gcapolupo','NO_MILESTONE',0,'PRS','synopsis, state, problem-level, category, product, planned-release, blocker, reported-in, submitter-id, responsible, dev-owner, originator, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli,','$release : ReleaseRecord( ) 
$user : User( ) 
$record_list : RecordSet( ) from 
 collect( PRRecord( blocked_release == $release.relname, clazz=="bug", 
 (category not matches ".*hw-.*" &&  category not matches ".*build.*"), 
  npi == $user.npi_program || 
  alwaysTrue == $user.junos || 
   ((dev_owner memberOf $user.reportNames  &&  ( state == "info" ||  
           (state=="feedback" &&  eval(alwaysTrue != blank_devowner)))) 
   ||(responsible memberOf $user.reportNames &&  (state == "open" || state == "analyzed" ||  
           (state=="feedback" &&  eval(alwaysTrue == blank_devowner)))) 
   )))   
',0,1,to_timestamp_tz('01-FEB-10 08.23.51.290412000 PM -08:00','DD-MON-RR HH.MI.SS.FF AM TZR'),'release',null,null,null,'Includes all non-suspended PRs.','NO_MILESTONE',0);
Insert into DASH.RULES (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) values ('r1_blocker_prs','R1 Blocker PRs','HOLD_TO_SCHEDULE','Owners of PRs with the [Blocking-Release] field set must 
prioritize these ETAs and fixes over non-blocker PRs.  This rule includes all non-suspended PRs (systest metrics exclude analyzed).','gcapolupo','R1_BUILD',45,'PRS','synopsis, state, problem-level, category, product, planned-release, blocker, submitter-id, responsible, dev-owner, originator, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli,','$release : ReleaseRecord( ) 
$user : User( ) 
$record_list : RecordSet( )  
 from collect( PRRecord( blocked_release == $release.relname, 
  blocker_types contains "r1", npi == $user.npi_program || 
  alwaysTrue == $user.junos || responsible memberOf $user.reportNames || 
  ( dev_owner memberOf $user.reportNames &&  state != "feedback" ) ) )',4,1,to_timestamp_tz('14-MAY-09 08.03.27.216638000 PM -07:00','DD-MON-RR HH.MI.SS.FF AM TZR'),'release',null,null,null,'Includes all non-suspended PRs.','R1_DEPLOY',-7);
Insert into DASH.RULES (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) values ('cvbc_prs','CVBC PRs','HOLD_TO_SCHEDULE','The Customer-Visible-Behavior-Changed (CVBC) field is used to notify Technical Publications of any changes introduced by the PR that need to be documented for the customer.','june','NO_MILESTONE',0,'PRS','synopsis, category, planned-release, state, responsible, techpubs-owner, problem-level, confidential, cust-visible-behavior-changed, cvbc-documented-in, ','$user : User( junos == false ) 
$record_list : RecordSet( ) from 
 collect( UndeadCvbcRecord(techpubs_owner memberOf $user.reportNames ) )',1,1,to_timestamp_tz('06-JUL-09 01.54.29.579309000 PM -07:00','DD-MON-RR HH.MI.SS.FF AM TZR'),null,null,null,null,'CVBC PRs need to be documented for the customer','NO_MILESTONE',0);
Insert into DASH.RULES (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) values ('rlis_open_past_r2_ship','RLIs open after R2 shipped','P5_STABILITY','Owners of RLIs must update their [State] field to a 
"closed" value to indicate that they''ve shipped once the release 
deploys: 
<ul> 
<li>closed-released - released to field and record closed</li> 
<li>closed-unsupported - internal Development and/or Testing 
complete and record closed; no customer deliverables 
and not officially supported</li> 
</ul>','lbirch','R2_DEPLOY',-1,'RLIS','synopsis, feature_priority, state, responsible, sw_mgr_responsible, sw_responsible, plm_responsible, ','$now : Date( ) 
$release : ReleaseRecord( r2_deploy < $now ) 
$user : User( ) 
$record_list : RecordSet( ) from 
 collect( RLIRecord( relname == $release.relname, 
  state != "deferred", 
  release_target matches ".*R2", npi_program == $user.npi_program_name || 
  alwaysTrue == $user.junos || responsible memberOf $user.reportNames || 
    sw_mgr_responsible memberOf $user.reportNames || 
    st_mgr_responsible memberOf $user.reportNames ) )',2,1,to_timestamp_tz('05-JUN-09 08.16.08.051077000 PM -07:00','DD-MON-RR HH.MI.SS.FF AM TZR'),'release',null,null,null,'RLIs must be closed after they are deployed in a release.','R2_DEPLOY',-45);
Insert into DASH.RULES (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) values ('rlis_wo_feature_desc','RLIs without a Feature Description','HOLD_TO_SCHEDULE','The Feature Description is, like, <b>really</b> important, you know?','mwazna','B1_DEPLOY',10,'RLIS','synopsis, tp_feature_description, functional_specification, tp_responsible, tp_release_status, ','$release : ReleaseRecord( b1_deploy_string != "" ) 
$user : User( ) 
$record_list : RecordSet( ) from 
 collect( RLIRecord( tp_feature_description == "", 
  state != "deferred", 
  relname == $release.relname, 
  tp_approval_status != "n/a", 
  tp_release_status != "no_doc", 
  alwaysTrue != $user.junos &&  
  (tp_responsible memberOf $user.reportNames || 
  tp_lead_responsible memberOf $user.reportNames || 
  tp_mgr_responsible memberOf $user.reportNames) ) )',4,1,to_timestamp_tz('12-MAY-09 08.42.43.041782000 PM -07:00','DD-MON-RR HH.MI.SS.FF AM TZR'),'release',null,null,null,'Like, totally write a description, dude.','NO_MILESTONE',0);
Insert into DASH.RULES (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) values ('closed_blocker_prs','All Closed Blocker PRs','P5_STABILITY','Owners of PRs with the [Blocking-Release] field set must prioritize these ETAs and fixes over non-blocker PRs.  This rule includes all non-suspended PRs (systest metrics exclude analyzed).','admin','NO_MILESTONE',0,'CLOSEDPRS','synopsis, state, problem-level, category, product, planned-release, blocker, reported-in, submitter-id, responsible, dev-owner, originator, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli,','$release : ReleaseRecord( )  
$user : User( )  
$record_list : RecordSet( ) from  
 collect( ClosedPRRecord( blocked_release == $release.relname, planned_release not matches "^1[1-9]\.[0-9]W[0-9]*", 
 (category not matches ".*hw-.*" &&  category not matches ".*build.*"),  
  npi == $user.npi_program ||  
  alwaysTrue == $user.junos ||   
  (eval(alwaysTrue == blank_devowner) &&  responsible memberOf $user.reportNames) ||   
  (eval(alwaysTrue != blank_devowner) &&  dev_owner memberOf $user.reportNames)  ))',0,1,to_timestamp_tz('09-JAN-10 12.39.36.137426000 AM -08:00','DD-MON-RR HH.MI.SS.FF AM TZR'),'release',null,null,null,'Includes all non-suspended PRs.','NO_MILESTONE',0);
Insert into DASH.RULES (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) values ('release_blocker_prs','Release Blocker PRs','HOLD_TO_SCHEDULE','Owners of PRs with the [Blocking-Release] field set must prioritize these ETAs and fixes over non-blocker PRs.  This rule includes all non-suspended PRs (systest metrics exclude analyzed).','admin','RN_DEPLOY',75,'PRS','synopsis, state, problem-level, category, product, planned-release, blocker, submitter-id, responsible, dev-owner, originator, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli,','$release : ReleaseRecord( )  
$user : User( )  
$record_list : RecordSet( ) from  
 collect( PRRecord( blocked_release == $release.relname, planned_release not matches "^1[1-9]\.[0-9]W[0-9]*", 
  blocker_types contains "release", npi == $user.npi_program ||  
  alwaysTrue == $user.junos || responsible memberOf $user.reportNames ||  
  ( dev_owner memberOf $user.reportNames &&  state != "feedback" ) ) )',0,1,to_timestamp_tz('14-MAY-09 07.57.50.274970000 PM -07:00','DD-MON-RR HH.MI.SS.FF AM TZR'),'release',null,null,null,'Includes all non-suspended PRs.','RN_DEPLOY',-60);
Insert into DASH.RULES (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) values ('major_prs_fix','Major PRs Fix','HOLD_TO_SCHEDULE','Major PRs should be prioritized over minor ones. 
-- only used this on PR-fix report --','admin','NO_MILESTONE',0,'PRS','synopsis, state, problem-level, category, product, planned-release, blocker, reported-in, submitter-id, responsible, dev-owner, originator, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli,','$release : ReleaseRecord( ) 
$user : User( ) 
$record_list : RecordSet( ) from 
 collect( PRRecord( problem_level in ("2-CL2","4-CL3","4-IL2","5-IL3"), clazz == "bug", 
 (category not matches ".*hw-.*" &&  category not matches ".*build.*"), 
 (releases contains $release.relname || branchRelease == $release.relname ), 
  npi == $user.npi_program || 
  alwaysTrue == $user.junos || 
   ((dev_owner memberOf $user.reportNames  &&  ( state == "info" ||  
           (state=="feedback" &&  eval(alwaysTrue != blank_devowner)))) 
   ||(responsible memberOf $user.reportNames &&  (state == "open" || state == "analyzed" ||  
           (state=="feedback" &&  eval(alwaysTrue == blank_devowner)))) 
   )))',0,1,to_timestamp_tz('01-FEB-10 08.25.20.675709000 PM -08:00','DD-MON-RR HH.MI.SS.FF AM TZR'),'release',null,null,null,'Major PRs should be prioritized over minor ones.','NO_MILESTONE',0);
Insert into DASH.RULES (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) values ('rli_test_script_below','RLIs with test script percentage below threshold','HOLD_TO_SCHEDULE','RLIs with test script percentage below threshold used in RLI reporting','admin','NO_MILESTONE',0,'RLIS','synopsis, release_target, tests_script_percentage, state, responsible, sw_mgr_responsible, sw_responsible, st_responsible, st_mgr_responsible, plm_responsible, tp_mgr_responsible, tp_lead_responsible, tp_responsible, ','$release : ReleaseRecord( ) 
$user : User( ) 
$record_list : RecordSet( ) from 
 collect( RLIRecord(( relname == $release.relname || relname memberOf $release.childBranch), 
  tests_script < 70, 
  npi_program == $user.npi_program_name || 
  alwaysTrue == $user.junos || responsible memberOf $user.reportNames || 
    sw_mgr_responsible memberOf $user.reportNames || 
    st_mgr_responsible memberOf $user.reportNames ) )',0,0,to_timestamp_tz('01-MAR-10 10.33.45.066989000 PM -08:00','DD-MON-RR HH.MI.SS.FF AM TZR'),'release',null,null,'no_show','RLIs with test script percentage below threshold','NO_MILESTONE',0);
Insert into DASH.RULES (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) values ('rli_test_script_middle','RLIs with test script percentage middle threshold','HOLD_TO_SCHEDULE','RLIs with test script percentage middle threshold used in RLI reporting','admin','NO_MILESTONE',0,'RLIS','synopsis, release_target, tests_script_percentage, state, responsible, sw_mgr_responsible, sw_responsible, st_responsible, st_mgr_responsible, plm_responsible, tp_mgr_responsible, tp_lead_responsible, tp_responsible, ','$release : ReleaseRecord( ) 
$user : User( ) 
$record_list : RecordSet( ) from 
 collect( RLIRecord(( relname == $release.relname || relname memberOf $release.childBranch), 
  tests_script >= 70 ,tests_script <= 90, 
  npi_program == $user.npi_program_name || 
  alwaysTrue == $user.junos || responsible memberOf $user.reportNames || 
    sw_mgr_responsible memberOf $user.reportNames || 
    st_mgr_responsible memberOf $user.reportNames ) )',0,0,to_timestamp_tz('01-MAR-10 10.33.19.236888000 PM -08:00','DD-MON-RR HH.MI.SS.FF AM TZR'),'release',null,null,'no_show','RLIs with test script percentage middle threshold','NO_MILESTONE',0);
Insert into DASH.RULES (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) values ('rli_test_script_upper','RLIs with test script percentage upper threshold','HOLD_TO_SCHEDULE','RLIs with test script percentage upper threshold used in RLI reporting','admin','NO_MILESTONE',0,'RLIS','synopsis, release_target, tests_script_percentage, state, responsible, sw_mgr_responsible, sw_responsible, st_responsible, st_mgr_responsible, plm_responsible, tp_mgr_responsible, tp_lead_responsible, tp_responsible, ','$release : ReleaseRecord( ) 
$user : User( ) 
$record_list : RecordSet( ) from 
 collect( RLIRecord(( relname == $release.relname || relname memberOf $release.childBranch), 
  tests_script > 90 , 
  npi_program == $user.npi_program_name || 
  alwaysTrue == $user.junos || responsible memberOf $user.reportNames || 
    sw_mgr_responsible memberOf $user.reportNames || 
    st_mgr_responsible memberOf $user.reportNames ) )',0,0,to_timestamp_tz('01-MAR-10 10.32.54.471143000 PM -08:00','DD-MON-RR HH.MI.SS.FF AM TZR'),'release',null,null,'no_show','RLIs with test script percentage upper threshold','NO_MILESTONE',0);
Insert into DASH.RULES (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) values ('regression_prs_fix','Regression PRs Fix','NO_OBJECTIVE','It used to work and you BROKE it!  
- only use this on PR-fix report -','gcapolupo','NO_MILESTONE',0,'PRS','synopsis, state, problem-level, category, product, planned-release, blocker, reported-in, submitter-id, responsible, dev-owner, originator, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli,','$release : ReleaseRecord( ) 
$user : User( ) 
$record_list : RecordSet( ) from 
 collect( PRRecord( attributes matches ".*regression-pr.*", clazz == "bug", 
 (category not matches ".*hw-.*" &&  category not matches ".*build.*"), 
 (releases contains $release.relname || branchRelease == $release.relname ), 
  npi == $user.npi_program || 
  alwaysTrue == $user.junos || 
   ((dev_owner memberOf $user.reportNames  &&  ( state == "info" ||  
           (state=="feedback" &&  eval(alwaysTrue != blank_devowner)))) 
   ||(responsible memberOf $user.reportNames &&  (state == "open" || state == "analyzed" ||  
           (state=="feedback" &&  eval(alwaysTrue == blank_devowner)))) 
   ))) ',0,1,to_timestamp_tz('01-FEB-10 08.27.59.997742000 PM -08:00','DD-MON-RR HH.MI.SS.FF AM TZR'),'release',null,null,null,'It used to work and you BROKE it!','NO_MILESTONE',0);
Insert into DASH.RULES (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) values ('closed_regression_prs','Closed Regression PRs','P5_STABILITY','It used to work and you BROKE it!','admin','NO_MILESTONE',0,'CLOSEDPRS','synopsis, state, problem-level, category, product, planned-release, blocker, reported-in, submitter-id, responsible, dev-owner, originator, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli,','$release : ReleaseRecord( )  
$user : User( )  
$record_list : RecordSet( ) from  
 collect( ClosedPRRecord( attributes matches ".*regression-pr.*",  
 (category not matches ".*hw-.*" &&  category not matches ".*build.*"),  
 (releases contains $release.relname || branchRelease == $release.relname  ),  planned_release not matches "^1[1-9]\.[0-9]W[0-9]*", 
 npi == $user.npi_program ||  
  alwaysTrue == $user.junos ||   
  (eval(alwaysTrue == blank_devowner) &&  responsible memberOf $user.reportNames) ||   
  (eval(alwaysTrue != blank_devowner) &&  dev_owner memberOf $user.reportNames)  ))',0,1,to_timestamp_tz('09-JAN-10 12.40.34.799418000 AM -08:00','DD-MON-RR HH.MI.SS.FF AM TZR'),'release',null,null,null,'It used to work and you BROKE it!','NO_MILESTONE',0);
Insert into DASH.RULES (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) values ('critical_prs_fix','Critical PRs Fix - non active rules','HOLD_TO_SCHEDULE','Owners of PRs with the [Severity] field set to ''critical'' must 
prioritize these ETAs and fixes over non-critical PRs. 
-- Only used this on PR-fix','gcapolupo','NEXT_BUILD',45,'PRS','synopsis, state, problem-level, category, product, planned-release, blocker, reported-in, submitter-id, responsible, dev-owner, originator, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli,','$release : ReleaseRecord( ) 
$user : User( ) 
$record_list : RecordSet( ) from 
 collect( PRRecord( severity == "critical", clazz == "bug", 
 (category not matches ".*hw-.*" &&  category not matches ".*build.*"), 
  releases contains $release.relname, npi == $user.npi_program || 
  alwaysTrue == $user.junos || 
   ((dev_owner memberOf $user.reportNames  &&  ( state == "info" ||  
           (state=="feedback" &&  eval(alwaysTrue != blank_devowner)))) 
   ||(responsible memberOf $user.reportNames &&  (state == "open" || state == "analyzed" ||  
           (state=="feedback" &&  eval(alwaysTrue == blank_devowner)))) 
   )))   
',3,0,to_timestamp_tz('01-FEB-10 08.27.02.505558000 PM -08:00','DD-MON-RR HH.MI.SS.FF AM TZR'),'release',null,null,null,'Owners of PRs with the [Severity] field set to ''critical'' mus...','NEXT_DEPLOY',-7);
Insert into DASH.RULES (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) values ('rli_has_test_effort','RLIs has associated test effort','HOLD_TO_SCHEDULE','related rli which has associated test effort','admin','NO_MILESTONE',0,'RLIS','synopsis, release_target, cam_status, state, responsible, sw_mgr_responsible, sw_responsible, st_responsible, st_mgr_responsible, plm_responsible, tp_mgr_responsible, tp_lead_responsible, tp_responsible, ','$release : ReleaseRecord( hasOpenRLIs == true ) 
$user : User( ) 
$record_list : RecordSet( ) from 
 collect( RLIRecord( relname == $release.relname, 
  st_commitment == "not-applicable", 
  have_associated_test_effort == alwaysTrue, 
  npi_program == $user.npi_program_name || 
  alwaysTrue == $user.junos || responsible memberOf $user.reportNames || 
  sw_mgr_responsible memberOf $user.reportNames || 
  st_mgr_responsible memberOf $user.reportNames || 
    sw_responsible == $user.id || st_responsible == $user.id ) )',0,1,to_timestamp_tz('02-MAR-10 06.53.11.074677000 PM -08:00','DD-MON-RR HH.MI.SS.FF AM TZR'),'release',null,null,null,'RLIs has associated test effort','NO_MILESTONE',0);
Insert into DASH.RULES (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) values ('r2_blocker_prs','R2 Blocker PRs','HOLD_TO_SCHEDULE','Owners of PRs with the [Blocking-Release] field set must 
prioritize these ETAs and fixes over non-blocker PRs.  This rule includes all non-suspended PRs (systest metrics exclude analyzed).','gcapolupo','R2_BUILD',45,'PRS','synopsis, state, problem-level, category, product, planned-release, blocker, submitter-id, responsible, dev-owner, originator, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli,','$release : ReleaseRecord( ) 
$user : User( ) 
$record_list : RecordSet( )  
 from collect( PRRecord( blocked_release == $release.relname, 
  blocker_types contains "r2", npi == $user.npi_program || 
  alwaysTrue == $user.junos || responsible memberOf $user.reportNames || 
  ( dev_owner memberOf $user.reportNames &&  state != "feedback" ) ) )',3,1,to_timestamp_tz('14-MAY-09 07.59.36.213822000 PM -07:00','DD-MON-RR HH.MI.SS.FF AM TZR'),'release',null,null,null,'Includes all non-suspended PRs.','R2_DEPLOY',-7);
Insert into DASH.RULES (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) values ('beta_blocker_prs','Beta Blocker PRs','HOLD_TO_SCHEDULE','Owners of PRs with the [Blocking-Release] field set to a Bx build must 
prioritize these ETAs and fixes over non-blocker PRs.  This rule includes all non-suspended PRs (systest metrics exclude analyzed).','gcapolupo','BN_DEPLOY',21,'PRS','synopsis, state, problem-level, category, product, planned-release, blocker, submitter-id, responsible, dev-owner, originator, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli,','$release : ReleaseRecord( ) 
$user : User( ) 
$record_list : RecordSet( ) from 
 collect( PRRecord( blocked_release == $release.relname, 
  blocker_types contains "beta", npi == $user.npi_program || 
  alwaysTrue == $user.junos || responsible memberOf $user.reportNames || 
  ( dev_owner memberOf $user.reportNames &&  state != "feedback" ) ) )',0,1,to_timestamp_tz('14-MAY-09 07.53.39.727840000 PM -07:00','DD-MON-RR HH.MI.SS.FF AM TZR'),'release',null,null,null,'Includes all non-suspended PRs.','B3_DEPLOY',-7);
Insert into DASH.RULES (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) values ('tot_blocker_prs','TOT Blocker PRs','HOLD_TO_SCHEDULE','Owners of PRs in which the [Blocking-Release] field contains "TOT" must 
prioritize these ETAs and fixes over non-blocker PRs.','gcapolupo','NO_MILESTONE',0,'PRS','synopsis, state, problem-level, category, product, planned-release, blocker, submitter-id, responsible, dev-owner, originator, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli,','$user : User( ) 
$record_list : RecordSet( ) from 
 collect( PRRecord( blocker_types contains "tot", npi == $user.npi_program || 
  alwaysTrue == $user.junos || responsible memberOf $user.reportNames || 
  ( dev_owner memberOf $user.reportNames &&  state != "feedback" ) ) )',10,1,to_timestamp_tz('07-NOV-08 08.44.32.845925000 PM -08:00','DD-MON-RR HH.MI.SS.FF AM TZR'),null,null,null,null,'Owners of PRs in which the [Blocking-Release] field contains ...','NO_MILESTONE',0);
Insert into DASH.RULES (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) values ('rlis_unresourced_for_p0_exit','RLIs unresourced for P0 exit','P0_EXIT','Engineering owners of RLIs with their [State] field set to 
''initial-request'' are required to actively pursue timely resourcing. In 
order for RLIs to stay in a given JUNOS release and proceed to Phase 1, 
their [State] field must be set to ''dev-committed'' (or better) by 
Engineering so they can be included in System Test''s scoping and 
resourcing exercise. If an RLI cannot be delivered in a given release, 
its [State] should be set to ''deferred''.','lbirch','P0_EXIT',30,'RLIS','synopsis, feature_priority, state, responsible, sw_mgr_responsible, sw_responsible, plm_responsible, ','$release : ReleaseRecord( hasOpenRLIs == true ) 
$user : User( ) 
$record_list : RecordSet( ) from 
 collect( RLIRecord( relname == $release.relname, 
  state matches "initial-request|dev-maybe", 
  /* The line below is logically equivalent to: 
   *   not (category contains "trd" &&  trd_status in ("rejected", "required")) 
   * See http://en.wikipedia.org/wiki/De_Morgan''s_laws */ 
  ( category not contains "trd" || trd_status not in ("rejected", "required") ), 
  npi_program == $user.npi_program_name || 
  alwaysTrue == $user.junos || responsible memberOf $user.reportNames || 
    sw_mgr_responsible memberOf $user.reportNames || 
    st_mgr_responsible memberOf $user.reportNames ) )',2,0,to_timestamp_tz('05-OCT-09 10.17.28.858611000 PM -07:00','DD-MON-RR HH.MI.SS.FF AM TZR'),'release',null,null,null,'Engineering owners of RLIs with their [State] field set to ''i...','P0_EXIT',0);
Insert into DASH.RULES (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) values ('rlis_with_unknown_platform','RLIs with unknown platform','P0_EXIT','Owners of RLIs with a [Platform] field that contains ''unknown'' must 
enter the individual platform names or the platform series affected by 
their RLI. Without platform information, an RLI cannot be scoped 
accurately and may not pass a future NPI phase for the JUNOS release.','lbirch','IN_DEVELOPMENT',0,'RLIS','synopsis, state, rli_class, project_status, platform, responsible, sw_mgr_responsible, sw_responsible, plm_responsible, ','$release : ReleaseRecord( hasOpenRLIs == true ) 
$user : User( ) 
$record_list : RecordSet( ) from 
 collect( RLIRecord( platform contains "unknown", 
  applicability not contains "catch-all", 
  state != "deferred", 
  relname == $release.relname, 
  npi_program == $user.npi_program_name || 
  alwaysTrue == $user.junos || responsible memberOf $user.reportNames || 
    sw_mgr_responsible memberOf $user.reportNames || 
    st_mgr_responsible memberOf $user.reportNames || 
    sw_responsible == $user.id || st_responsible == $user.id ) )',2,1,to_timestamp_tz('15-JUN-09 02.50.03.253726000 PM -07:00','DD-MON-RR HH.MI.SS.FF AM TZR'),'release',null,null,null,'Owners of RLIs with a [Platform] field that contains ''unknown...','P1_EXIT',0);
Insert into DASH.RULES (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) values ('rlis_wo_change_locator','RLIs without Change Locator','P4_EXIT','When changes are incorporated into the books/topics, the TP Responsible completes information about the location of the new documentation for this RLI.','mwazna','P4_EXIT',-7,'RLIS','synopsis, change_locator, tp_responsible, ','$release : ReleaseRecord( ) 
$user : User( ) 
$record_list : RecordSet( ) from 
 collect( RLIRecord( change_locator == "", 
  state != "deferred", 
  relname == $release.relname, 
  tp_approval_status != "n/a", 
  tp_release_status != "no_doc", 
  alwaysTrue != $user.junos &&  
  (tp_responsible memberOf $user.reportNames || 
  tp_lead_responsible memberOf $user.reportNames || 
  tp_mgr_responsible memberOf $user.reportNames) ) )',3,1,to_timestamp_tz('15-JAN-09 04.42.11.979123000 PM -08:00','DD-MON-RR HH.MI.SS.FF AM TZR'),'release',null,null,null,'RLIs need to be incorporated into books or topics','NO_MILESTONE',0);
Insert into DASH.RULES (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) values ('dev-maybe-rlis','Dev-Maybe RLIs','P0_EXIT','Owners of RLIs with their [State] field set to ''dev-maybe'' are required 
to actively pursue timely resourcing. ''dev-maybe'' RLIs are allowed a 1 
week grace period following a JUNOS Phase 0 Exit, however you must 
provide a compelling reason for needing the grace period. When this 
grace period expires, the RLI will be deferred from the release if it 
remains in ''dev-maybe'' [State]. The RLI [State] field should only be set 
to ''dev-maybe'' if active scoping is underway and there are specific 
outstanding issues requiring resolution prior to making a commitment; 
you cannot just mark an RLI ''dev-maybe'' because you didn''t have time to 
look at it and need a free pass to get through a JUNOS P0 Exit. Next 
steps for outstanding issues should be identified in the RLI [Status] or 
[Action] field.','lbirch','P0_EXIT',30,'RLIS','synopsis, feature_priority, state, responsible, sw_mgr_responsible, sw_responsible, plm_responsible, st_mgr_responsible, ','$release : ReleaseRecord( hasOpenRLIs == true ) 
$user : User( ) 
$record_list : RecordSet( ) from 
 collect( RLIRecord( state == "dev-maybe", 
  relname == $release.relname, npi_program == $user.npi_program_name || 
  alwaysTrue == $user.junos || responsible memberOf $user.reportNames || 
    sw_mgr_responsible memberOf $user.reportNames || 
    st_mgr_responsible memberOf $user.reportNames ) )',1,0,to_timestamp_tz('05-OCT-09 10.16.58.544891000 PM -07:00','DD-MON-RR HH.MI.SS.FF AM TZR'),'release',null,null,null,'Owners of RLIs with their [State] field set to ''dev-maybe'' ar...','P1_EXIT',0);
Insert into DASH.RULES (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) values ('rlis_with_missing_func_specs','RLIs with func specs not ready for review','P2_EXIT','Engineering owners of RLIs with their [Functional Specification Status] 
field set to ''required'' must check in a spec and provide the CVS link in 
[Functional Specification URL] prior to the planned milestone for a 
given JUNOS release. When the spec if ready for review, the [Functional 
Specification Status] field should be set to ''ready-for-review''.','lbirch','FUNC_SPEC_COMPLETE',28,'RLIS','synopsis, responsible, sw_mgr_responsible, sw_responsible, functional_specification, functional_spec_status, ','$release : ReleaseRecord( hasOpenRLIs == true ) 
$user : User( ) 
$record_list : RecordSet( ) from 
 collect( RLIRecord( relname == $release.relname, 
 state != "deferred", 
  /* The line below is logically equivalent to: 
   *   not (category contains "trd" &&  trd_status in ("rejected", "required")) 
   * See http://en.wikipedia.org/wiki/De_Morgan''s_laws */ 
  ( category not contains "trd" || trd_status not in ("rejected", "required") ), 
  npi_program == $user.npi_program_name || 
  alwaysTrue == $user.junos || responsible memberOf $user.reportNames || 
  sw_mgr_responsible memberOf $user.reportNames || 
  st_mgr_responsible memberOf $user.reportNames || 
  sw_responsible == $user.id || st_responsible == $user.id, $ds : dataSource, 
  functional_spec_status_ordinal < ($ds.picklistOrdinal("functional_spec_status", "ready-for-review")) ) )',2,0,to_timestamp_tz('22-JUL-09 07.37.15.327398000 AM -07:00','DD-MON-RR HH.MI.SS.FF AM TZR'),'release',null,null,null,'Engineering owners of RLIs with their [Functional Specificati...','NO_MILESTONE',0);
Insert into DASH.RULES (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) values ('rlis_w_ers_spec_not_reqd','RLIs with ERs and ''spec not required''','P2_EXIT','Engineering owners of RLIs with an [ER Numbers] field that isn''t null 
can''t set the [Functional Specification Status] field to ''not-required''. 
We have a TL9000 requirement to show traceability for RLIs that have ERs 
associated with them - PLM is required to verify that a functional 
specification meets customer requirements and sign-off for any 
ER-related RLI before it can be included in a JUNOS release.','lbirch','FUNC_SPEC_COMPLETE',28,'RLIS','synopsis, responsible, sw_mgr_responsible, sw_responsible, functional_specification, functional_spec_status, plm_func_spec_approver, plm_approval_status, sw_func_spec_approver, sw_approval_status, hw_func_spec_approver, hw_approval_status, st_func_spec_approver, st_approval_status, tp_func_spec_approver, tp_approval_status, ','$release : ReleaseRecord( hasOpenRLIs == true ) 
$user : User( ) 
$record_list : RecordSet( ) from 
 collect( RLIRecord( functional_spec_status == "not-required", er_numbers != "", 
  functional_specification == "", relname == $release.relname, 
  state != "deferred", 
  /* The line below is logically equivalent to: 
   *   not (category contains "trd" &&  trd_status in ("rejected", "required")) 
   * See http://en.wikipedia.org/wiki/De_Morgan''s_laws */ 
  ( category not contains "trd" || trd_status not in ("rejected", "required") ), 
  npi_program == $user.npi_program_name || 
  alwaysTrue == $user.junos || responsible memberOf $user.reportNames || 
    sw_mgr_responsible memberOf $user.reportNames || 
    st_mgr_responsible memberOf $user.reportNames ) )',2,0,to_timestamp_tz('15-JAN-09 04.42.11.830721000 PM -08:00','DD-MON-RR HH.MI.SS.FF AM TZR'),'release',null,null,null,'Engineering owners of RLIs with an [ER Numbers] field that is...','NO_MILESTONE',0);
Insert into DASH.RULES (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) values ('specs_not_approved','RLIs with specs that haven''t been approved','P2_EXIT','Engineering owners of RLIs with specs checked in (i.e. CVS link provided 
in [Functional Specification URL]) and their [Functional Specification 
Status] field set to ''ready-for-review'' are required to actively pursue 
timely approval. Reviews are typically due 2 weeks after specs are made 
available, and comments must be incorporated during this timeframe so 
that all relevant stakeholders can formally approve the spec.','lbirch','FUNC_SPEC_APPROVED',24,'RLIS','synopsis, responsible, sw_mgr_responsible, sw_responsible, functional_specification, functional_spec_status, plm_approval_status, sw_approval_status, hw_approval_status, st_approval_status, tp_approval_status, ','$release : ReleaseRecord( hasOpenRLIs == true ) 
$user : User( ) 
$record_list : RecordSet( ) from 
 collect( RLIRecord( relname == $release.relname, 
  state != "deferred", 
  /* The line below is logically equivalent to: 
   *   not (category contains "trd" &&  trd_status in ("rejected", "required")) 
   * See http://en.wikipedia.org/wiki/De_Morgan''s_laws */ 
  ( category not contains "trd" || trd_status not in ("rejected", "required") ), 
  functional_spec_status == "ready-for-review", functional_specification != "", 
  npi_program == $user.npi_program_name || 
  alwaysTrue == $user.junos || responsible memberOf $user.reportNames || 
    sw_mgr_responsible memberOf $user.reportNames || 
    st_mgr_responsible memberOf $user.reportNames || 
    sw_responsible == $user.id || st_responsible == $user.id ) )',2,0,to_timestamp_tz('15-JAN-09 04.42.11.882394000 PM -08:00','DD-MON-RR HH.MI.SS.FF AM TZR'),'release',null,null,null,'Engineering owners of RLIs with specs checked in (i.e. CVS li...','NO_MILESTONE',0);
Insert into DASH.RULES (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) values ('dev_compl_med_rlis_wo_utp','Dev-complete Medium RLIs without needed UTP','P2_EXIT','To determine readiness for system test, if a Unit Test Plan (UTP) is 
required, it must be reviewed and approved.','lbirch','NO_MILESTONE',0,'RLIS','synopsis, state, sw_proj_len, rli_class, project_status, responsible, sw_mgr_responsible, sw_responsible, unit_test_plan_status, unit_test_plan, ','$release : ReleaseRecord( hasOpenRLIs == true ) 
$user : User( ) 
$record_list : RecordSet( ) from 
 collect( RLIRecord( relname == $release.relname, $ds : dataSource, 
  state != "deferred", 
  sw_proj_len == "medium", 
  npi_program == $user.npi_program_name || 
  alwaysTrue == $user.junos || responsible memberOf $user.reportNames || 
  sw_mgr_responsible memberOf $user.reportNames || 
  st_mgr_responsible memberOf $user.reportNames || 
  sw_responsible == $user.id || st_responsible == $user.id, 
  project_status_ordinal >= ($ds.picklistOrdinal("project_status", "dev-complete")), 
  unit_test_plan_status_ordinal < ($ds.picklistOrdinal("unit_test_plan_status", "reviewed-complete")) ) )',1,0,to_timestamp_tz('10-APR-09 02.52.18.133355000 PM -07:00','DD-MON-RR HH.MI.SS.FF AM TZR'),'release',null,null,null,'To determine readiness for system test, if a Unit Test Plan (...','P2_EXIT',-30);
Insert into DASH.RULES (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) values ('r3_blocker_prs','R3 Blocker PRs','HOLD_TO_SCHEDULE','Owners of PRs with the [Blocking-Release] field set must 
prioritize these ETAs and fixes over non-blocker PRs.  This rule includes all non-suspended PRs (systest metrics exclude analyzed).','gcapolupo','R3_BUILD',45,'PRS','synopsis, state, problem-level, category, product, planned-release, blocker, submitter-id, responsible, dev-owner, originator, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli,','$release : ReleaseRecord( ) 
$user : User( ) 
$record_list : RecordSet( )  
 from collect( PRRecord( blocked_release == $release.relname, 
  blocker_types contains "r3", npi == $user.npi_program || 
  alwaysTrue == $user.junos || responsible memberOf $user.reportNames || 
  ( dev_owner memberOf $user.reportNames &&  state != "feedback" ) ) )',3,1,to_timestamp_tz('14-MAY-09 08.00.33.795634000 PM -07:00','DD-MON-RR HH.MI.SS.FF AM TZR'),'release',null,null,null,'Includes all non-suspended PRs.','R3_DEPLOY',-7);
Insert into DASH.RULES (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) values ('uncommitted_rlis_p1_npi','Uncommitted RLIs in post-P1 NPIs','P1_EXIT','RLIs for all NPI Programs that have exited P1 should have [State]=''committed'' or better unless they''re undergoing a P1 Re-Plan. This implies that NPI program deliverables were scoped out in detail prior to exiting P1 and that management has approved the resources associated with this program.','lbirch','NO_MILESTONE',0,'RLIS','synopsis, release_target, state, responsible, sw_mgr_responsible, sw_responsible, plm_responsible, npi_program, ','$now : Date( ) 
$npi : NPIRecord( p1_exit < $now ) 
$user : User( npi_program == $npi.id ) 
$release : ReleaseRecord( ) 
$record_list : RecordSet( ) from 
 collect( RLIRecord( relname == $release.relname, 
  npi_program == $npi.synopsis, 
  $ds : dataSource, 
  state_ordinal < ($ds.picklistOrdinal("state", "committed")) ) )',1,1,to_timestamp_tz('28-APR-09 04.27.18.190611000 PM -07:00','DD-MON-RR HH.MI.SS.FF AM TZR'),'release',null,null,null,'RLIs for all NPI Programs that have exited P1 should have [St...','NO_MILESTONE',0);
Insert into DASH.RULES (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) values ('r4_blocker_prs','R4 Blocker PRs','HOLD_TO_SCHEDULE','Owners of PRs with the [Blocking-Release] field set must 
prioritize these ETAs and fixes over non-blocker PRs.  This rule includes all non-suspended PRs (systest metrics exclude analyzed).','gcapolupo','R4_BUILD',60,'PRS','synopsis, state, problem-level, category, product, planned-release, blocker, submitter-id, responsible, dev-owner, originator, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli,','$release : ReleaseRecord( ) 
$user : User( ) 
$record_list : RecordSet( )  
 from collect( PRRecord( blocked_release == $release.relname, 
  blocker_types contains "r4", npi == $user.npi_program || 
  alwaysTrue == $user.junos || responsible memberOf $user.reportNames || 
  ( dev_owner memberOf $user.reportNames &&  state != "feedback" ) ) )',3,1,to_timestamp_tz('14-MAY-09 09.42.45.585003000 PM -07:00','DD-MON-RR HH.MI.SS.FF AM TZR'),'release',null,null,null,'Includes all non-suspended PRs.','R4_DEPLOY',-7);
Insert into DASH.RULES (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) values ('reviews_user','Pending reviews','HOLD_TO_SCHEDULE','Code reviews where user is a reviewer or responsible moderator.','admin','NO_MILESTONE',0,'REVIEWS','synopsis, arrival-date, last-modified, submitter, ','$user : User( ) 
 $record_list : RecordSet( ) from 
  collect( ReviewRecord( age < 120, alwaysTrue == $user.junos || 
   allResponsibles contains $user ) )',0,1,to_timestamp_tz('07-APR-09 08.36.33.176117000 PM -07:00','DD-MON-RR HH.MI.SS.FF AM TZR'),null,null,null,null,'Code reviews where user is a reviewer or responsible moderato...','NO_MILESTONE',0);
Insert into DASH.RULES (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) values ('rlis_user','RLIs','HOLD_TO_SCHEDULE','All RLIs owned by the user in a given release.','admin','NO_MILESTONE',0,'RLIS','synopsis, release_target, state, responsible, sw_mgr_responsible, sw_responsible, st_responsible, st_mgr_responsible, plm_responsible, tp_mgr_responsible, tp_lead_responsible, tp_responsible, ','$release : ReleaseRecord( hasOpenRLIs == true ) 
  $user : User( ) 
  $record_list : RecordSet( ) from 
   collect( RLIRecord( ( relname == $release.relname || relname memberOf $release.childBranch), 
    npi_program == $user.npi_program_name || 
    alwaysTrue == $user.junos || responsible memberOf $user.reportNames || 
    sw_mgr_responsible memberOf $user.reportNames || 
    st_mgr_responsible memberOf $user.reportNames || 
    sw_responsible == $user.id || st_responsible == $user.id || 
    plm_responsible memberOf $user.reportNames || 
    tp_responsible memberOf $user.reportNames || 
    tp_lead_responsible memberOf $user.reportNames || 
    tp_mgr_responsible memberOf $user.reportNames ) )',0,1,to_timestamp_tz('01-MAR-10 10.14.23.888561000 PM -08:00','DD-MON-RR HH.MI.SS.FF AM TZR'),'release',null,null,null,'All RLIs owned by the user in a given release.','NO_MILESTONE',0);
Insert into DASH.RULES (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) values ('abandoned_reviews','Abandoned reviews','HOLD_TO_SCHEDULE','Review requests should be completed or closed in a reasonable time.  Reviews 
over two months old that have not been modified in the last month appear to 
be abandoned.  Abandoned reviews more than four months old are ignored.','skumar','NO_MILESTONE',0,'REVIEWS','synopsis, arrival-date, last-modified, submitter, ','$user : User( ) 
$record_list : RecordSet( ) from 
 collect( ReviewRecord( daysSinceLastModified > 30, 
  age > 60 &&  < 120, 
  alwaysTrue == $user.junos || moderators contains $user ) )',2,1,to_timestamp_tz('05-APR-08 08.45.44.836337000 AM -07:00','DD-MON-RR HH.MI.SS.FF AM TZR'),null,null,null,null,'Review requests should be completed or closed in a reasonable...','NO_MILESTONE',null);
Insert into DASH.RULES (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) values ('invalid_reviewers','Reviews with invalid reviewers','HOLD_TO_SCHEDULE','Review requests with invalid reviewers (eg. users that have left the company) 
must be reassigned.','skumar','NO_MILESTONE',0,'REVIEWS','synopsis, arrival-date, last-modified, submitter, ','$user : User( ) 
$record_list : RecordSet( ) from 
 collect( ReviewRecord( hasInvalidReviewers == true, 
  hasResponsibleModerators == true, age < 120, 
  alwaysTrue == $user.junos || moderators contains $user ) )',3,1,to_timestamp_tz('05-APR-08 08.45.44.841940000 AM -07:00','DD-MON-RR HH.MI.SS.FF AM TZR'),null,null,null,null,'Review requests with invalid reviewers (eg. users that have l...','NO_MILESTONE',null);
Insert into DASH.RULES (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) values ('rlis_open_past_ship','RLIs open after R1 shipped','P5_STABILITY','Owners of RLIs must update their [State] field to a 
"closed" value to indicate that they''ve shipped once the release 
deploys: 
<ul> 
<li>closed-released - released to field and record closed</li> 
<li>closed-unsupported - internal Development and/or Testing 
complete and record closed; no customer deliverables 
and not officially supported</li> 
</ul>','lbirch','R1_DEPLOY',-1,'RLIS','synopsis, feature_priority, state, responsible, sw_mgr_responsible, sw_responsible, plm_responsible, ','$now : Date( ) 
$release : ReleaseRecord( r1_deploy < $now ) 
$user : User( ) 
$record_list : RecordSet( ) from 
 collect( RLIRecord( relname == $release.relname, 
  state != "deferred", 
  release_target matches ".*R1", 
  npi_program == $user.npi_program_name || 
  alwaysTrue == $user.junos || responsible memberOf $user.reportNames || 
    sw_mgr_responsible memberOf $user.reportNames || 
    st_mgr_responsible memberOf $user.reportNames ) )',2,1,to_timestamp_tz('05-JUN-09 08.15.49.448920000 PM -07:00','DD-MON-RR HH.MI.SS.FF AM TZR'),'release',null,null,null,'RLIs must be closed after they are deployed in a release.','R1_DEPLOY',-45);
Insert into DASH.RULES (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) values ('unassigned_reviews','Unassigned reviews','HOLD_TO_SCHEDULE','Review requests must be assigned to a reviewer within two days of creation.  After two months, the review is assumed to be abandoned.','skumar','NO_MILESTONE',0,'REVIEWS','synopsis, arrival-date, last-modified, submitter, ','$user : User( ) 
$record_list : RecordSet( ) from 
 collect( ReviewRecord( hasResponsibleModerators == true, 
  age > 1 &&  < 60, 
  alwaysTrue == $user.junos || moderators contains $user ) )',2,1,to_timestamp_tz('05-APR-08 08.45.44.853463000 AM -07:00','DD-MON-RR HH.MI.SS.FF AM TZR'),null,null,null,null,'Review requests must be assigned to a reviewer within two day...','NO_MILESTONE',null);
Insert into DASH.RULES (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) values ('toxic_blocker_prs','Toxic Blocker PRs','HOLD_TO_SCHEDULE','Owners of PRs in which the [Blocking-Release] field contains "TOXIC" must 
prioritize these ETAs and fixes over non-blocker PRs.','gcapolupo','NO_MILESTONE',0,'PRS','synopsis, state, problem-level, category, product, planned-release, blocker, submitter-id, responsible, dev-owner, originator, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli,','$release : ReleaseRecord( ) 
$user : User( ) 
$record_list : RecordSet( ) from 
 collect( PRRecord( blocked_release == $release.relname, 
  blocker_types contains "toxic", npi == $user.npi_program || 
  alwaysTrue == $user.junos || responsible memberOf $user.reportNames || 
  ( dev_owner memberOf $user.reportNames &&  state != "feedback" ) ) )',10,1,to_timestamp_tz('28-MAY-09 06.39.35.768482000 PM -07:00','DD-MON-RR HH.MI.SS.FF AM TZR'),'release',null,null,null,'Owners of PRs in which the [Blocking-Release] field contains ...','NO_MILESTONE',0);
Insert into DASH.RULES (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) values ('npis_with_bad_phase','NPI Programs with wrong phase','HOLD_TO_SCHEDULE','The phase field of an NPI program should be updated to reflect the current phase.','lbirch','NO_MILESTONE',0,'NPIS','synopsis, phase, responsible, p0_exit, p1_exit, p2_exit, p3_exit, p4_exit, ','$now : Date( ) 
$user : User( ) 
$record_list : RecordSet( ) from 
 collect( NPIRecord( (phase == "P0" &&  p0_exit_epoch < $now.time &&  > 0) || 
  (phase == "P1" &&  p1_exit_epoch < $now.time &&  > 0) || 
  (phase == "P2" &&  p2_exit_epoch < $now.time &&  > 0) || 
  (phase == "P3" &&  p3_exit_epoch < $now.time &&  > 0) || 
  (phase == "P4" &&  p4_exit_epoch < $now.time &&  > 0), 
  alwaysTrue == $user.junos || 
  responsible == $user.id || 
  beta_lead == $user.id || 
  cs_lead == $user.id || 
  hw_lead == $user.id || 
  ops_lead == $user.id || 
  pgm_lead == $user.id || 
  plm_lead == $user.id || 
  sw_lead == $user.id || 
  systest_lead == $user.id || 
  techpubs_lead == $user.id ) )',10,1,to_timestamp_tz('24-APR-09 05.13.23.351753000 PM -07:00','DD-MON-RR HH.MI.SS.FF AM TZR'),null,null,null,null,'The phase field of an NPI program should be updated to reflec...','NO_MILESTONE',0);
Insert into DASH.RULES (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) values ('pending_reviews','Inactive pending reviews','HOLD_TO_SCHEDULE','Reviews should be completed in a timely manner; if there is no activity for 
two days, this is considered a problem.','skumar','NO_MILESTONE',0,'REVIEWS','synopsis, arrival-date, last-modified, submitter, ','$user : User( ) 
$record_list : RecordSet( ) from 
 collect( ReviewRecord( hasResponsibleReviewers == true, 
  daysSinceLastModified > 2, age < 120, 
  alwaysTrue == $user.junos || reviewers contains $user ) )',2,1,to_timestamp_tz('05-APR-08 08.45.44.847851000 AM -07:00','DD-MON-RR HH.MI.SS.FF AM TZR'),null,null,null,null,'Reviews should be completed in a timely manner; if there is n...','NO_MILESTONE',null);
Insert into DASH.RULES (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) values ('prs_for_npi','Find PRs for NPI Program','NO_OBJECTIVE','bookkeeping','admin','NO_MILESTONE',0,'NOTHING',null,'$npi : NPIRecord( ) 
$pr : PRRecord( npi == "", rli != "" ) 
$rli : RLIRecord( npi_program == $npi.synopsis, id memberOf $pr.rli_nums )',1,0,to_timestamp_tz('24-JUN-09 02.05.35.513332000 PM -07:00','DD-MON-RR HH.MI.SS.FF AM TZR'),null,'$pr.setNpi($npi.getId()); 
 update($pr);','lock-on-active true 
 no-loop true 
 salience 10','020 Complex Related','bookkeeping','NO_MILESTONE',0);
Insert into DASH.RULES (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) values ('npis_user','NPI programs','P5_STABILITY','All NPI programs where the user is Responsible or Lead.','admin','NO_MILESTONE',0,'NPIS','synopsis, phase, responsible, responsible_product_group, p0_exit, p1_exit, p2_exit, p3_exit, p4_exit, ','$user : User( ) 
  $record_list : RecordSet( ) from 
   collect( NPIRecord( alwaysTrue == $user.junos || 
    responsible memberOf $user.reportNames || 
    beta_lead memberOf $user.reportNames || 
        cs_lead memberOf $user.reportNames || 
        hw_lead memberOf $user.reportNames || 
        ops_lead memberOf $user.reportNames || 
        pgm_lead memberOf $user.reportNames || 
        plm_lead memberOf $user.reportNames || 
        sw_lead memberOf $user.reportNames || 
        systest_lead memberOf $user.reportNames || 
        techpubs_lead memberOf $user.reportNames ) )',0,1,to_timestamp_tz('07-APR-09 08.31.49.831802000 PM -07:00','DD-MON-RR HH.MI.SS.FF AM TZR'),null,null,null,null,'All NPI programs where the user is Responsible or Lead.','NO_MILESTONE',0);
Insert into DASH.RULES (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) values ('customer_crit_hi_prs','Customer Critical/High PRs','HOLD_TO_SCHEDULE','Owners of customer critical or high priority need to try and resolve these issues as soon as possible.  Customers have raised the priority on these PRs through JTAC.  If you do not have the time to work on the escalated PRs assigned to you, notify your manager.','admin','NO_MILESTONE',0,'PRS','synopsis, state, problem-level, category, product, planned-release, blocker, submitter-id, responsible, dev-owner, originator, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli,','    $user : User( )
    $record_list : RecordSet( ) from
     collect( PRRecord(submitter_id in ("customer", "field"),
      problem_level == "1-CL1" || problem_level == "2-CL2" || problem_level == "3-IL1" || problem_level == "4-IL2",
      state in ("open", "info", "analyzed"), npi == $user.npi_program ||
      alwaysTrue == $user.junos || responsible memberOf $user.reportNames ||
      ( dev_owner memberOf $user.reportNames && state != "feedback" )
      )
     )',6,1,to_timestamp_tz('03-SEP-08 10.41.38.337835000 AM -07:00','DD-MON-RR HH.MI.SS.FF AM TZR'),null,null,null,null,'Customers have raised the priority on these PRs through JTAC.','NO_MILESTONE',null);
Insert into DASH.RULES (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) values ('test_blockers','Test Blockers','HOLD_TO_SCHEDULE','Owners of PRs with the [Blocking-Release] field containing 
''testblocker'' must prioritize these ETAs and fixes over ALL 
other PRs.','lbirch','NO_MILESTONE',0,'PRS','synopsis, state, problem-level, category, product, planned-release, blocker, submitter-id, responsible, dev-owner, originator, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli,','$release : ReleaseRecord( ) 
$user : User( ) 
$record_list : RecordSet( ) from 
 collect( PRRecord( blocked_release == $release.relname, 
  blocker_types contains "test", npi == $user.npi_program || 
  alwaysTrue == $user.junos || responsible memberOf $user.reportNames || 
  ( dev_owner memberOf $user.reportNames &&  state != "feedback" ) ) )',5,1,to_timestamp_tz('07-NOV-08 08.21.38.947130000 PM -08:00','DD-MON-RR HH.MI.SS.FF AM TZR'),'release',null,null,null,'Owners of PRs with the [Blocking-Release] field containing ''t...','NO_MILESTONE',0);
Insert into DASH.RULES (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) values ('blockers_wo_fix_eta','Blockers without Fix-ETA','HOLD_TO_SCHEDULE','Blockers must have an estimated time for the fix.','lbirch','NEXT_BUILD',45,'PRS','synopsis, state, problem-level, category, product, planned-release, blocker, submitter-id, responsible, dev-owner, originator, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli,','$release : ReleaseRecord( ) 
$user : User( ) 
$record_list : RecordSet( ) from 
 collect( PRRecord( fix_eta_string == "", 
  blocked_release == $release.relname, 
  state in ( "open", "analyzed", "info" ), npi == $user.npi_program || 
  alwaysTrue == $user.junos || responsible memberOf $user.reportNames || 
  ( dev_owner memberOf $user.reportNames &&  state != "feedback" ) ) )',3,1,to_timestamp_tz('10-JUN-09 07.56.22.531980000 PM -07:00','DD-MON-RR HH.MI.SS.FF AM TZR'),'release',null,null,null,'Blockers must have an estimated time for the fix.','NEXT_DEPLOY',-7);
Insert into DASH.RULES (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) values ('build_break_prs','Build Break PRs','HOLD_TO_SCHEDULE','Build''s broken, we''re all doomed.','gcapolupo','NO_MILESTONE',0,'PRS','synopsis, state, problem-level, category, product, planned-release, blocker, submitter-id, responsible, dev-owner, originator, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli,','$release : ReleaseRecord( ) 
$user : User( ) 
$record_list : RecordSet( ) from 
 collect( PRRecord( releases contains $release.relname, 
  category == "build-break" || category == "daily-build-failure", npi == $user.npi_program || 
  alwaysTrue == $user.junos || responsible memberOf $user.reportNames || 
  ( dev_owner memberOf $user.reportNames &&  state != "feedback" ) ) )',6,1,to_timestamp_tz('03-SEP-08 10.40.18.238555000 AM -07:00','DD-MON-RR HH.MI.SS.FF AM TZR'),'release',null,null,null,'Build''s broken, we''re all doomed.','NO_MILESTONE',null);
Insert into DASH.RULES (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) values ('sisyphus_break_prs','Sisyphus Build Break PRs','HOLD_TO_SCHEDULE','Sisyphus build is broken, get the torches and pitchforks.','gcapolupo','NO_MILESTONE',0,'PRS','synopsis, state, problem-level, category, product, planned-release, blocker, submitter-id, responsible, dev-owner, originator, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli,','$release : ReleaseRecord( ) 
$user : User( ) 
$record_list : RecordSet( ) from 
 collect( PRRecord( category == "sisyphus-build-break", 
  releases contains $release.relname, npi == $user.npi_program || 
  alwaysTrue == $user.junos || responsible memberOf $user.reportNames || 
  ( dev_owner memberOf $user.reportNames &&  state != "feedback" ) ) )',2,1,to_timestamp_tz('03-SEP-08 10.41.11.450784000 AM -07:00','DD-MON-RR HH.MI.SS.FF AM TZR'),'release',null,null,null,'Sisyphus build is broken, get the torches and pitchforks.','NO_MILESTONE',null);
Insert into DASH.RULES (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) values ('prs_unassigned_cat_user','Unassigned category PRs','HOLD_TO_SCHEDULE','All PRs in categories "owned" by the user (SPOC, owner or group-owner) that are assigned to a non-person (category alias or non/former employee) for this release.','admin','NO_MILESTONE',0,'PRS','synopsis, state, problem-level, category, product, planned-release, blocker, submitter-id, responsible, dev-owner, originator, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli,','$release : ReleaseRecord( ) 
  $user : User( ) 
  $record_list : RecordSet( ) from 
  collect( PRRecord( releases contains $release.relname, 
   responsible != real_responsible, 
   category_spoc memberOf $user.reportNames ) )',0,1,to_timestamp_tz('24-APR-09 04.39.59.922052000 PM -07:00','DD-MON-RR HH.MI.SS.FF AM TZR'),'release',null,null,null,'All PRs in categories "owned" by the user (SPOC, owner or gro...','NO_MILESTONE',0);
Insert into DASH.RULES (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) values ('regression_prs','Regression PRs','HOLD_TO_SCHEDULE','It used to work and you BROKE it!','gcapolupo','NEXT_BUILD',21,'PRS','synopsis, state, problem-level, category, product, planned-release, blocker, submitter-id, responsible, dev-owner, originator, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli,','$release : ReleaseRecord( ) 
$user : User( ) 
$record_list : RecordSet( ) from 
 collect( PRRecord( attributes matches ".*regression-pr.*", 
  releases contains $release.relname, npi == $user.npi_program || 
  alwaysTrue == $user.junos || responsible memberOf $user.reportNames || 
  ( dev_owner memberOf $user.reportNames &&  state != "feedback" ) ) )',3,1,to_timestamp_tz('10-JUN-09 07.55.33.466419000 PM -07:00','DD-MON-RR HH.MI.SS.FF AM TZR'),'release',null,null,null,'It used to work and you BROKE it!','NEXT_DEPLOY',-7);
Insert into DASH.RULES (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) values ('prs_non_junos_cat_user','Category PRs with non-JUNOS responsible','HOLD_TO_SCHEDULE','PRs in categories "owned" by the user (SPOC, owner or group-owner) that have a non-JUNOS-engineering responsible in the release.  These are PRs that the user is "waiting on".','admin','NO_MILESTONE',0,'PRS','synopsis, state, problem-level, category, product, planned-release, blocker, submitter-id, responsible, dev-owner, originator, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli,','$release : ReleaseRecord( ) 
  $user : User( ) 
  $record_list : RecordSet( ) from 
   collect( PRRecord( releases contains $release.relname, 
    non_junos_responsible == true, 
    category_spoc memberOf $user.reportNames ) )',0,1,to_timestamp_tz('24-APR-09 04.39.47.129633000 PM -07:00','DD-MON-RR HH.MI.SS.FF AM TZR'),'release',null,null,null,'PRs in categories "owned" by the user (SPOC, owner or group-o...','NO_MILESTONE',0);
Insert into DASH.RULES (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) values ('late_dev_only_rlis','Dev/infrastructure RLIs that will be late','P2_EXIT','Engineering owners of ''dev-only'' or infrastructure RLIs must be declared 
dev-complete 2 weeks prior to the planned dev-complete milestone for a 
given JUNOS release. If the ETA provided in the [SW Dev Complete Date] 
and/or [HW Dev Complete Date] field is later than this deadline, the RLI 
will be flagged at-risk and placed on a list of "boot candidates" to be 
reviewed with Eng Directors for pruning.','lbirch','DEV_COMPLETE_ONE',60,'RLIS','synopsis, state, sw_proj_len, rli_class, project_status, responsible, sw_mgr_responsible, sw_responsible, sw_dev_complete_date, hw_dev_complete_date, ','$release : ReleaseRecord( hasOpenRLIs == true ) 
$user : User( ) 
$record_list : RecordSet( ) from 
 collect( RLIRecord( relname == $release.relname, 
  state != "deferred", 
  synopsis matches ".*(dev-only|infrastructure).*", 
  npi_program == $user.npi_program_name || 
  alwaysTrue == $user.junos || responsible memberOf $user.reportNames || 
    sw_mgr_responsible memberOf $user.reportNames || 
    st_mgr_responsible memberOf $user.reportNames || 
    sw_responsible == $user.id || st_responsible == $user.id, $ds : dataSource, 
  project_status_ordinal < ($ds.picklistOrdinal("project_status", "dev-complete")), 
  (sw_proj_len in ("large", "major", "expansive", "") &&  
  $swdc1 : sw_dev_complete_date_epoch -> ($swdc1 > $release.getDev_complete_one_epoch() - (24192 * 100000))) 
  || 
  (sw_proj_len == "medium" &&  
  $swdc2 : sw_dev_complete_date_epoch -> ($swdc2 > $release.getDev_complete_two_epoch() - (24192 * 100000))) 
  || 
  (sw_proj_len in ("small", "tiny") &&  
  $swdc3 : sw_dev_complete_date_epoch -> ($swdc3 > $release.getDev_complete_three_epoch() - (24192 * 100000))) ) )',2,0,to_timestamp_tz('15-JAN-09 04.42.11.906632000 PM -08:00','DD-MON-RR HH.MI.SS.FF AM TZR'),'release',null,null,null,'Engineering owners of ''dev-only'' or infrastructure RLIs must ...','NO_MILESTONE',0);
Insert into DASH.RULES (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) values ('large_rlis_wo_dev_complete_date','Large RLIs without dev complete date','P2_EXIT','Engineering owners of RLIs must provide ETAs in the [SW Dev Complete 
Date] and/or [HW Dev Complete Date] fields as a given JUNOS release 
approaches its planned dev-complete milestone. The date should be 
whatever you think your current ETA is. This is not necessarily a 
contract or a promise -- it''s a way of communicating your status as 
accurately as possible. You can (and should) change it whenever you know 
your ETA has moved. The earlier you can achieve dev-complete, the 
better. Don''t target to have all of your RLIs dev-complete on the very 
last date allowed for the release.','lbirch','FUNC_SPEC_APPROVED',4,'RLIS','synopsis, state, sw_proj_len, rli_class, project_status, at_risk, at_risk_reason, action, responsible, sw_mgr_responsible, sw_responsible, sw_dev_complete_date, hw_dev_complete_date, ','$release : ReleaseRecord( hasOpenRLIs == true ) 
$user : User( ) 
$record_list : RecordSet( ) from 
 collect( RLIRecord( relname == $release.relname, 
  state != "deferred", 
  /* The line below is logically equivalent to: 
   *   not (category contains "trd" &&  trd_status in ("rejected", "required")) 
   * See http://en.wikipedia.org/wiki/De_Morgan''s_laws */ 
  ( category not contains "trd" || trd_status not in ("rejected", "required") ), 
  sw_dev_complete_date_string == "", 
  sw_proj_len in ("large", "major", "expansive", ""), 
  npi_program == $user.npi_program_name || 
  alwaysTrue == $user.junos || responsible memberOf $user.reportNames || 
    sw_mgr_responsible memberOf $user.reportNames || 
    st_mgr_responsible memberOf $user.reportNames || 
    sw_responsible == $user.id || st_responsible == $user.id, $ds : dataSource, 
    project_status_ordinal < ($ds.picklistOrdinal("project_status", "dev-complete")) ) )',2,0,to_timestamp_tz('15-JAN-09 04.42.11.861710000 PM -08:00','DD-MON-RR HH.MI.SS.FF AM TZR'),'release',null,null,null,'Engineering owners of RLIs must provide ETAs in the [SW Dev C...','NO_MILESTONE',0);
Insert into DASH.RULES (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) values ('med_rlis_wo_dev_complete_date','Medium RLIs without dev complete date','P2_EXIT','Engineering owners of RLIs must provide ETAs in the [SW Dev Complete 
Date] and/or [HW Dev Complete Date] fields as a given JUNOS release 
approaches its planned dev-complete milestone. The date should be 
whatever you think your current ETA is. This is not necessarily a 
contract or a promise -- it''s a way of communicating your status as 
accurately as possible. You can (and should) change it whenever you know 
your ETA has moved. The earlier you can achieve dev-complete, the 
better. Don''t target to have all of your RLIs dev-complete on the very 
last date allowed for the release.','lbirch','FUNC_SPEC_APPROVED',4,'RLIS','synopsis, state, sw_proj_len, rli_class, project_status, at_risk, at_risk_reason, action, responsible, sw_mgr_responsible, sw_responsible, sw_dev_complete_date, hw_dev_complete_date, ','$release : ReleaseRecord( hasOpenRLIs == true ) 
$user : User( ) 
$record_list : RecordSet( ) from 
 collect( RLIRecord( relname == $release.relname, 
  state != "deferred", 
  /* The line below is logically equivalent to: 
   *   not (category contains "trd" &&  trd_status in ("rejected", "required")) 
   * See http://en.wikipedia.org/wiki/De_Morgan''s_laws */ 
  ( category not contains "trd" || trd_status not in ("rejected", "required") ), 
  sw_dev_complete_date_string == "", 
  sw_proj_len == "medium", 
  npi_program == $user.npi_program_name || 
  alwaysTrue == $user.junos || responsible memberOf $user.reportNames || 
    sw_mgr_responsible memberOf $user.reportNames || 
    st_mgr_responsible memberOf $user.reportNames || 
    sw_responsible == $user.id || st_responsible == $user.id, $ds : dataSource, 
    project_status_ordinal < ($ds.picklistOrdinal("project_status", "dev-complete")) ) )',2,0,to_timestamp_tz('15-JAN-09 04.42.11.874741000 PM -08:00','DD-MON-RR HH.MI.SS.FF AM TZR'),'release',null,null,null,'Engineering owners of RLIs must provide ETAs in the [SW Dev C...','NO_MILESTONE',0);
Insert into DASH.RULES (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) values ('large_rlis_missed_dev_complete','Large RLIs which missed dev-complete','P2_EXIT','Engineering owners of RLIs that aren''t yet dev-complete as of the 
planned milestone for a given JUNOS release are flagged at-risk and 
placed on a list of "boot candidates" to be reviewed with Eng Directors 
for pruning. Dev-complete basically means that code is written, 
reviewed, checked in -- and that the unit test plan is done and the 
results are recorded. 
When the RLI is dev-complete, the [Project Status] field should be 
flipped to "dev-complete".','lbirch','DEV_COMPLETE_ONE',-1,'RLIS','synopsis, state, sw_proj_len, rli_class, project_status, responsible, sw_mgr_responsible, sw_responsible, sw_dev_complete_date, hw_dev_complete_date, ','$now : Date( ) 
$release : ReleaseRecord( hasOpenRLIs == true, dev_complete_two < $now ) 
$user : User( ) 
$record_list : RecordSet( ) from 
 collect( RLIRecord( relname == $release.relname, 
  state != "deferred", 
  release_target matches ".*R1$", 
  /* The line below is logically equivalent to: 
   *   not (category contains "trd" &&  trd_status in ("rejected", "required")) 
   * See http://en.wikipedia.org/wiki/De_Morgan''s_laws */ 
  ( category not contains "trd" || trd_status not in ("rejected", "required") ), 
  sw_proj_len in ("large", "major", "expansive", ""), 
  npi_program == $user.npi_program_name || 
  alwaysTrue == $user.junos || responsible memberOf $user.reportNames || 
  sw_mgr_responsible memberOf $user.reportNames || 
  st_mgr_responsible memberOf $user.reportNames || 
    sw_responsible == $user.id || st_responsible == $user.id, $ds : dataSource, 
  project_status_ordinal < ($ds.picklistOrdinal("project_status", "dev-complete")) ) )',2,0,to_timestamp_tz('06-JUL-09 07.59.50.624673000 PM -07:00','DD-MON-RR HH.MI.SS.FF AM TZR'),'release',null,null,null,'Engineering owners of RLIs that aren''t yet dev-complete as of...','DEV_COMPLETE_ONE',-45);
Insert into DASH.RULES (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) values ('rli_test_execute_below','RLIs with test execution below threshold','HOLD_TO_SCHEDULE','RLIs with test execution below threshold used in RLI reporting','admin','NO_MILESTONE',0,'RLIS','synopsis, release_target, tests_executed_percentage, state, responsible, sw_mgr_responsible, sw_responsible, st_responsible, st_mgr_responsible, plm_responsible, tp_mgr_responsible, tp_lead_responsible, tp_responsible, ','$release : ReleaseRecord(hasOpenRLIs == true) 
$user : User( ) 
$record_list : RecordSet( ) from 
 collect( RLIRecord( ( relname == $release.relname || relname memberOf $release.childBranch), 
  tests_executed < 80, 
  npi_program == $user.npi_program_name || 
  alwaysTrue == $user.junos || responsible memberOf $user.reportNames || 
    sw_mgr_responsible memberOf $user.reportNames || 
    st_mgr_responsible memberOf $user.reportNames ) )',0,0,to_timestamp_tz('01-MAR-10 10.01.47.149037000 PM -08:00','DD-MON-RR HH.MI.SS.FF AM TZR'),'release',null,null,'no_show','RLIs with test execution below threshold','NO_MILESTONE',0);
Insert into DASH.RULES (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) values ('rli_test_execute_middle','RLIs with test execution middle threshold','HOLD_TO_SCHEDULE','RLIs with test execution middle threshold used in RLI reporting','admin','NO_MILESTONE',0,'RLIS','synopsis, release_target, tests_executed_percentage, state, responsible, sw_mgr_responsible, sw_responsible, st_responsible, st_mgr_responsible, plm_responsible, tp_mgr_responsible, tp_lead_responsible, tp_responsible, ','$release : ReleaseRecord( ) 
$user : User( ) 
$record_list : RecordSet( ) from 
 collect( RLIRecord( ( relname == $release.relname || relname memberOf $release.childBranch), 
  tests_executed >= 80, tests_executed <=99, 
  npi_program == $user.npi_program_name || 
  alwaysTrue == $user.junos || responsible memberOf $user.reportNames || 
    sw_mgr_responsible memberOf $user.reportNames || 
    st_mgr_responsible memberOf $user.reportNames ) )',0,0,to_timestamp_tz('01-MAR-10 10.02.40.835889000 PM -08:00','DD-MON-RR HH.MI.SS.FF AM TZR'),'release',null,null,'no_show','RLIs with test execution middle threshold','NO_MILESTONE',0);
Insert into DASH.RULES (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) values ('rli_test_execute_upper','RLIs with test execution upper threshold','HOLD_TO_SCHEDULE','RLIs with test execution upper threshold used in RLI reporting','admin','NO_MILESTONE',0,'RLIS','synopsis, release_target, tests_executed_percentage, state, responsible, sw_mgr_responsible, sw_responsible, st_responsible, st_mgr_responsible, plm_responsible, tp_mgr_responsible, tp_lead_responsible, tp_responsible, ','$release : ReleaseRecord( ) 
$user : User( ) 
$record_list : RecordSet( ) from 
 collect( RLIRecord( ( relname == $release.relname || relname memberOf $release.childBranch), 
  tests_executed == 100, 
  npi_program == $user.npi_program_name || 
  alwaysTrue == $user.junos || responsible memberOf $user.reportNames || 
    sw_mgr_responsible memberOf $user.reportNames || 
    st_mgr_responsible memberOf $user.reportNames ) )',0,0,to_timestamp_tz('01-MAR-10 10.03.55.186342000 PM -08:00','DD-MON-RR HH.MI.SS.FF AM TZR'),'release',null,null,'no_show','RLIs with test execution upper threshold','NO_MILESTONE',0);
Insert into DASH.RULES (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) values ('rli_test_pass_below','RLIs with test pass percentage below threshold','HOLD_TO_SCHEDULE','RLIs with test pass percentage below threshold used in RLI reporting','admin','NO_MILESTONE',0,'RLIS','synopsis, release_target, tests_passed_percentage, state, responsible, sw_mgr_responsible, sw_responsible, st_responsible, st_mgr_responsible, plm_responsible, tp_mgr_responsible, tp_lead_responsible, tp_responsible, ','$release : ReleaseRecord( ) 
$user : User( ) 
$record_list : RecordSet( ) from 
 collect( RLIRecord( ( relname == $release.relname || relname memberOf $release.childBranch), 
  tests_passed <  70, 
  npi_program == $user.npi_program_name || 
  alwaysTrue == $user.junos || responsible memberOf $user.reportNames || 
    sw_mgr_responsible memberOf $user.reportNames || 
    st_mgr_responsible memberOf $user.reportNames ) )',0,0,to_timestamp_tz('01-MAR-10 10.05.53.173743000 PM -08:00','DD-MON-RR HH.MI.SS.FF AM TZR'),'release',null,null,'no_show','RLIs with test pass percentage below threshold','NO_MILESTONE',0);
Insert into DASH.RULES (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) values ('rli_test_pass_middle','RLIs with test pass percentage middle threshold','HOLD_TO_SCHEDULE','RLIs with test pass percentage middle threshold used in RLI reporting','admin','NO_MILESTONE',0,'RLIS','synopsis, release_target, tests_passed_percentage, state, responsible, sw_mgr_responsible, sw_responsible, st_responsible, st_mgr_responsible, plm_responsible, tp_mgr_responsible, tp_lead_responsible, tp_responsible, ','$release : ReleaseRecord( ) 
$user : User( ) 
$record_list : RecordSet( ) from 
 collect( RLIRecord( ( relname == $release.relname || relname memberOf $release.childBranch), 
  tests_passed >= 70, tests_passed <= 90, 
  npi_program == $user.npi_program_name || 
  alwaysTrue == $user.junos || responsible memberOf $user.reportNames || 
    sw_mgr_responsible memberOf $user.reportNames || 
    st_mgr_responsible memberOf $user.reportNames ) )',0,0,to_timestamp_tz('01-MAR-10 10.06.39.554019000 PM -08:00','DD-MON-RR HH.MI.SS.FF AM TZR'),'release',null,null,'no_show','RLIs with test pass percentage middle threshold','NO_MILESTONE',0);
Insert into DASH.RULES (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) values ('rli_test_pass_upper','RLIs with test pass percentage upper threshold','HOLD_TO_SCHEDULE','RLIs with test pass percentage upper threshold used in RLI reporting','admin','NO_MILESTONE',0,'RLIS','synopsis, release_target, tests_passed_percentage, state, responsible, sw_mgr_responsible, sw_responsible, st_responsible, st_mgr_responsible, plm_responsible, tp_mgr_responsible, tp_lead_responsible, tp_responsible, ','$release : ReleaseRecord( ) 
$user : User( ) 
$record_list : RecordSet( ) from 
 collect( RLIRecord( ( relname == $release.relname || relname memberOf $release.childBranch), 
  tests_passed > 90, 
  npi_program == $user.npi_program_name || 
  alwaysTrue == $user.junos || responsible memberOf $user.reportNames || 
    sw_mgr_responsible memberOf $user.reportNames || 
    st_mgr_responsible memberOf $user.reportNames ) )',0,0,to_timestamp_tz('01-MAR-10 10.07.20.985641000 PM -08:00','DD-MON-RR HH.MI.SS.FF AM TZR'),'release',null,null,'no_show','RLIs with test pass percentage upper threshold','NO_MILESTONE',0);
Insert into DASH.RULES (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) values ('backlog_prs_forward_responsible_not_dev','Forward View / Future Backlog PRs by Responsible != Dev-Owner','HOLD_TO_SCHEDULE','All Future Backlog PR by using responsible as their primary accountability where the responsible != dev-owner','admin','NO_MILESTONE',0,'PRS','synopsis, problem-level, state, problem-level, category, product, planned-release, blocker, submitter-id, responsible, dev-owner, originator, systest-owner, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli, created, resolution, feedback-date, jtac-case-id,','$user : User( ) 
$record_list : RecordSet( ) from 
 collect( 
    PRRecord( 
        (problem_level == "1-CL1" || problem_level == "2-CL2" || problem_level == "4-CL3" || problem_level == "3-L1" || problem_level == "4-IL2" || problem_level == "5-CL4" || problem_level == "5-IL3"), 
        (clazz == "bug" || clazz == "unreproducible"  || clazz == "duplicate" ),functional_area=="software", 
        (alwaysTrue == possible_forward_view_backlog || alwaysTrue == possible_backlog), 
        real_responsible != dev_owner, 
        npi == $user.npi_program || alwaysTrue == $user.junos || 
        backlog_pr_responsible memberOf $user.reportNames )) 
',0,1,to_timestamp_tz('01-NOV-10 12.00.00.511060000 PM -07:00','DD-MON-RR HH.MI.SS.FF AM TZR'),null,'$user.setScore("backlog_prs_forward_responsible_not_dev","pr_backlog",$record_list);',null,null,'Forward View / Future Backlog PRs by Responsible','NO_MILESTONE',0);
Insert into DASH.RULES (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) values ('prs_user','Total PRs','HOLD_TO_SCHEDULE','PRs for user in a release.','admin','NO_MILESTONE',0,'PRS','synopsis, state, problem-level, category, product, planned-release, blocker, submitter-id, responsible, dev-owner, originator, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli,','$release : ReleaseRecord( ) 
 $user : User( ) 
 $record_list : RecordSet( ) from 
 collect( PRRecord( releases contains $release.relname, planned_release not matches "^1[1-9]\.[0-9]W[0-9]*", npi == $user.npi_program || 
 alwaysTrue == $user.junos || responsible memberOf $user.reportNames || 
 ( dev_owner memberOf $user.reportNames && state != "feedback" ) ) )',0,1,to_timestamp_tz('07-FEB-11 12.00.00.511060000 PM -07:00','DD-MON-RR HH.MI.SS.FF AM TZR'),'release',null,null,null,'PRs for user.','NO_MILESTONE',0);
Insert into DASH.RULES (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) values ('prs_user_unscheduled_dev','Unscheduled Dev Branch PRs','HOLD_TO_SCHEDULE','PRs for Unscheduled Development Branch and Private Branch PR. PRs is assign to user as responsible or Dev-owner. Development Branch has unscheduled or n/a in Release Target.','admin','NO_MILESTONE',0,'PRS','synopsis, state, problem-level, category, product, planned-release, branch, blocker, submitter-id, responsible, dev-owner, originator, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli,','$user : User( )  
$bt : BTRecord( relname in ("unscheduled","n/a"), branch_name matches "DEV_.*")  
$record_list : RecordSet( ) from  
 collect( PRRecord( branch == $bt.branch_name ,  planned_release not matches "^1[1-9]\.[0-9]W[0-9]*", 
  npi == $user.npi_program ||  
  alwaysTrue == $user.junos ||   
  responsible memberOf $user.reportNames ||  
  (dev_owner memberOf $user.reportNames &&  state != "feedback")))',1,1,to_timestamp_tz('08-JUN-10 04.08.14.085427000 PM -07:00','DD-MON-RR HH.MI.SS.FF AM TZR'),null,null,null,null,'Unscheduled Development Branch','NO_MILESTONE',0);
Insert into DASH.RULES (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) values ('branch_responsible','Integration Branch','HOLD_TO_SCHEDULE','Total Integration Branch (IB) on Release','admin','NO_MILESTONE',0,'BTS','branch_name, branch_type, state, responsible,','$release :ReleaseRecord( )  
  $user : User( )  
  $record_list : RecordSet( ) from  
   collect( BTRecord(   
          relname == $release.relname, 
          branch_type == "ib",  
          responsible memberOf $user.reportNames || alwaysTrue == $user.junos ) )',1,1,to_timestamp_tz('01-MAR-10 10.28.03.909545000 PM -08:00','DD-MON-RR HH.MI.SS.FF AM TZR'),'release',null,null,null,'Integration Branch','NO_MILESTONE',0);
Insert into DASH.RULES (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) values ('rlis_coming_in_late','RLIs that will be checked in late','P2_EXIT','Engineering owners of RLIs that aren''t yet dev-complete must provide 
ETAs in the [SW Dev Complete Date] and/or [HW Dev Complete Date] fields. 
If you expect to declare your RLI dev-complete later than the planned 
milestone for a given JUNOS release, it will be flagged at-risk and 
placed on a list of "boot candidates" to be reviewed with Eng Directors 
for pruning.','lbirch','NO_MILESTONE',0,'RLIS','synopsis, state, sw_proj_len, rli_class, project_status, responsible, sw_mgr_responsible, sw_responsible, sw_dev_complete_date, hw_dev_complete_date, ','$release : ReleaseRecord( hasOpenRLIs == true, 
 $dc_one_epoch : dev_complete_one_epoch, 
 $dc_two_epoch : dev_complete_two_epoch, 
 $dc_three_epoch : dev_complete_three_epoch ) 
$user : User( ) 
$record_list : RecordSet( ) from 
 collect( RLIRecord( relname == $release.relname, 
  state != "deferred", 
  (sw_proj_len in ("large", "major", "expansive", "") &&  
   eval(sw_dev_complete_date_epoch > $dc_one_epoch + 86400000)) 
  || 
  (sw_proj_len == "medium" &&  
   eval(sw_dev_complete_date_epoch > $dc_two_epoch + 86400000)) 
  || 
  (sw_proj_len in ("small", "tiny") &&  
   eval(sw_dev_complete_date_epoch > $dc_three_epoch + 86400000)), 
  /* The line below is logically equivalent to: 
   *   not (category contains "trd" &&  trd_status in ("rejected", "required")) 
   * See http://en.wikipedia.org/wiki/De_Morgan''s_laws */ 
  ( category not contains "trd" || trd_status not in ("rejected", "required") ), 
  npi_program == $user.npi_program_name || 
  alwaysTrue == $user.junos || responsible memberOf $user.reportNames || 
  sw_mgr_responsible memberOf $user.reportNames || 
    sw_responsible == $user.id, $ds : dataSource, 
  project_status_ordinal < ($ds.picklistOrdinal("project_status", "dev-complete")) ) )',2,0,to_timestamp_tz('26-MAY-09 05.08.46.115966000 PM -07:00','DD-MON-RR HH.MI.SS.FF AM TZR'),'release',null,null,null,'Engineering owners of RLIs that aren''t yet dev-complete must ...','NO_MILESTONE',0);
Insert into DASH.RULES (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) values ('rlis_unresourced_for_p1_exit','RLIs unresourced for P1 exit','P1_EXIT','System Test owners of RLIs with their [State] field set to 
''dev-committed'' are required to actively pursue timely resourcing. In 
order for RLIs to stay in a given JUNOS release and proceed to Phase 2, 
their [State] field must be set to ''committed'' by System Test. If an RLI 
cannot be delivered in a given release, its [State] should be set to 
''deferred''.','lbirch','P1_EXIT',30,'RLIS','synopsis, feature_priority, state, responsible, sw_mgr_responsible, sw_responsible, plm_responsible, st_mgr_responsible, ','$release : ReleaseRecord( hasOpenRLIs == true ) 
$user : User( ) 
$record_list : RecordSet( ) from 
 collect( RLIRecord( relname == $release.relname, 
  state matches "initial-request|dev-maybe|dev-committed", 
  /* The line below is logically equivalent to: 
   *   not (category contains "trd" &&  trd_status in ("rejected", "required")) 
   * See http://en.wikipedia.org/wiki/De_Morgan''s_laws */ 
  ( category not contains "trd" || trd_status not in ("rejected", "required") ), 
  npi_program == $user.npi_program_name || 
  alwaysTrue == $user.junos || responsible memberOf $user.reportNames || 
    sw_mgr_responsible memberOf $user.reportNames || 
    st_mgr_responsible memberOf $user.reportNames ) )',2,0,to_timestamp_tz('15-JAN-09 04.42.11.797867000 PM -08:00','DD-MON-RR HH.MI.SS.FF AM TZR'),'release',null,null,null,'System Test owners of RLIs with their [State] field set to ''d...','P1_EXIT',0);
Insert into DASH.RULES (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) values ('med_rlis_missed_dev_complete','Medium RLIs that missed dev-complete','P2_EXIT','Engineering owners of RLIs that aren''t yet dev-complete as of the 
planned milestone for a given JUNOS release are flagged at-risk and 
placed on a list of "boot candidates" to be reviewed with Eng Directors 
for pruning. Dev-complete basically means that code is written, 
reviewed, checked in -- and that the unit test plan is done and the 
results are recorded. 
When the RLI is dev-complete, the [Project Status] field should be 
flipped to "dev-complete".','lbirch','DEV_COMPLETE_TWO',-1,'RLIS','synopsis, state, sw_proj_len, rli_class, project_status, responsible, sw_mgr_responsible, sw_responsible, sw_dev_complete_date, hw_dev_complete_date, ','$now : Date( ) 
$release : ReleaseRecord( hasOpenRLIs == true, dev_complete_two < $now ) 
$user : User( ) 
$record_list : RecordSet( ) from 
 collect( RLIRecord( relname == $release.relname, 
  state != "deferred", 
  release_target matches ".*R1$", 
  /* The line below is logically equivalent to: 
   *   not (category contains "trd" &&  trd_status in ("rejected", "required")) 
   * See http://en.wikipedia.org/wiki/De_Morgan''s_laws */ 
  ( category not contains "trd" || trd_status not in ("rejected", "required") ), 
  sw_proj_len == "medium", 
  npi_program == $user.npi_program_name || 
  alwaysTrue == $user.junos || responsible memberOf $user.reportNames || 
  sw_mgr_responsible memberOf $user.reportNames || 
  st_mgr_responsible memberOf $user.reportNames || 
    sw_responsible == $user.id || st_responsible == $user.id, $ds : dataSource, 
  project_status_ordinal < ($ds.picklistOrdinal("project_status", "dev-complete")) ) )',2,0,to_timestamp_tz('05-JUN-09 08.13.09.239932000 PM -07:00','DD-MON-RR HH.MI.SS.FF AM TZR'),'release',null,null,null,'Engineering owners of RLIs that aren''t yet dev-complete as of...','DEV_COMPLETE_TWO',-45);
Insert into DASH.RULES (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) values ('small_rlis_missed_dev_complete','Small RLIs that missed dev-complete','P2_EXIT','Engineering owners of RLIs that aren''t yet dev-complete as of the 
planned milestone for a given JUNOS release are flagged at-risk and 
placed on a list of "boot candidates" to be reviewed with Eng Directors 
for pruning. Dev-complete basically means that code is written, 
reviewed, checked in -- and that the unit test plan is done and the 
results are recorded. 
When the RLI is dev-complete, the [Project Status] field should be 
flipped to "dev-complete".','lbirch','DEV_COMPLETE_THREE',-1,'RLIS','synopsis, state, sw_proj_len, rli_class, project_status, responsible, sw_mgr_responsible, sw_responsible, sw_dev_complete_date, hw_dev_complete_date, ','$now : Date( ) 
$release : ReleaseRecord( hasOpenRLIs == true, dev_complete_three < $now ) 
$user : User( ) 
$record_list : RecordSet( ) from 
 collect( RLIRecord( relname == $release.relname, 
  state != "deferred", 
  release_target matches ".*R1$", 
  /* The line below is logically equivalent to: 
   *   not (category contains "trd" &&  trd_status in ("rejected", "required")) 
   * See http://en.wikipedia.org/wiki/De_Morgan''s_laws */ 
  ( category not contains "trd" || trd_status not in ("rejected", "required") ), 
  sw_proj_len in ("small", "tiny"), 
  npi_program == $user.npi_program_name || 
  alwaysTrue == $user.junos || responsible memberOf $user.reportNames || 
  sw_mgr_responsible memberOf $user.reportNames || 
  st_mgr_responsible memberOf $user.reportNames || 
    sw_responsible == $user.id || st_responsible == $user.id, $ds : dataSource, 
  project_status_ordinal < ($ds.picklistOrdinal("project_status", "dev-complete")) ) )',2,0,to_timestamp_tz('05-JUN-09 08.14.16.240148000 PM -07:00','DD-MON-RR HH.MI.SS.FF AM TZR'),'release',null,null,null,'Engineering owners of RLIs that aren''t yet dev-complete as of...','DEV_COMPLETE_THREE',-45);
Insert into DASH.RULES (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) values ('at_risk','RLIs At Risk','HOLD_TO_SCHEDULE','Owners of RLIs with their [At Risk] field set to ''yes'' are required to 
actively pursue timely resolution. If an RLI continues to remain at 
risk, it''s important to update the [At Risk Reason] with current status 
and ETA for resolution. The RLI [Action] field can be used to identify 
next steps.','lbirch','IN_DEVELOPMENT',0,'RLIS','synopsis, state, project_status, at_risk, at_risk_reason, action, responsible, sw_mgr_responsible, sw_responsible, sw_dev_complete_date, hw_dev_complete_date, ','$release : ReleaseRecord( hasOpenRLIs == true ) 
$user : User( ) 
$record_list : RecordSet( ) from 
 collect( RLIRecord( at_risk == true, 
  state != "deferred", 
  relname == $release.relname, 
  /* The line below is logically equivalent to: 
   *   not (category contains "trd" &&  trd_status in ("rejected", "required")) 
   * See http://en.wikipedia.org/wiki/De_Morgan''s_laws */ 
  ( category not contains "trd" || trd_status not in ("rejected", "required") ), 
  npi_program == $user.npi_program_name || 
  alwaysTrue == $user.junos || responsible memberOf $user.reportNames || 
  sw_mgr_responsible memberOf $user.reportNames || 
  st_mgr_responsible memberOf $user.reportNames || 
    sw_responsible == $user.id || st_responsible == $user.id ) )',3,1,to_timestamp_tz('15-JAN-09 04.42.12.017274000 PM -08:00','DD-MON-RR HH.MI.SS.FF AM TZR'),'release',null,null,null,'Owners of RLIs with their [At Risk] field set to ''yes'' are re...','NO_MILESTONE',0);
Insert into DASH.RULES (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) values ('small_rlis_wo_dev_complete_date','Small RLIs without dev complete date','P2_EXIT','Engineering owners of RLIs must provide ETAs in the [SW Dev Complete 
Date] and/or [HW Dev Complete Date] fields as a given JUNOS release 
approaches its planned dev-complete milestone. The date should be 
whatever you think your current ETA is. This is not necessarily a 
contract or a promise -- it''s a way of communicating your status as 
accurately as possible. You can (and should) change it whenever you know 
your ETA has moved. The earlier you can achieve dev-complete, the 
better. Don''t target to have all of your RLIs dev-complete on the very 
last date allowed for the release.','lbirch','FUNC_SPEC_APPROVED',0,'RLIS','synopsis, state, sw_proj_len, rli_class, project_status, at_risk, at_risk_reason, action, responsible, sw_mgr_responsible, sw_responsible, sw_dev_complete_date, hw_dev_complete_date, ','$release : ReleaseRecord( hasOpenRLIs == true ) 
$user : User( ) 
$record_list : RecordSet( ) from 
 collect( RLIRecord( relname == $release.relname, 
  state != "deferred", 
  /* The line below is logically equivalent to: 
   *   not (category contains "trd" &&  trd_status in ("rejected", "required")) 
   * See http://en.wikipedia.org/wiki/De_Morgan''s_laws */ 
  ( category not contains "trd" || trd_status not in ("rejected", "required") ), 
  sw_dev_complete_date_string == "", 
  sw_proj_len in ("small", "tiny"), 
  npi_program == $user.npi_program_name || 
  alwaysTrue == $user.junos || responsible memberOf $user.reportNames || 
    sw_mgr_responsible memberOf $user.reportNames || 
    st_mgr_responsible memberOf $user.reportNames || 
    sw_responsible == $user.id || st_responsible == $user.id, $ds : dataSource, 
    project_status_ordinal < ($ds.picklistOrdinal("project_status", "dev-complete")) ) )',2,0,to_timestamp_tz('15-JAN-09 04.42.11.853549000 PM -08:00','DD-MON-RR HH.MI.SS.FF AM TZR'),'release',null,null,null,'Engineering owners of RLIs must provide ETAs in the [SW Dev C...','NO_MILESTONE',0);
Insert into DASH.RULES (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) values ('unapproved_trds','Unapproved TRDs','HOLD_TO_SCHEDULE','Owners of RLIs with TRDs in-process (i.e. [TRD Status] field is set to 
''required'' or ''under-review'') are required to produce impact assessments. TRDs are 
generally allowed a 2 week window to complete the approval process, 
otherwise they expire and go away. Time and resources are required for 
Engineering, Systest, and Tech Pubs to scope and propose tradeoffs. By 
letting a TRD sit around for more than 2 weeks we''re essentially burning 
up cycles and resources by either, a) continuing to work on another RLI 
that we now want to punt as a tradeoff, or b) losing valuable time on 
RLIs that are proposed tradeoffs but ultimately stay in the release when 
the TRD is rejected. It''s expensive to burn cycles too early, and 
expensive to burn cycles too late. Time is of the essence when it comes 
to determining whether or not we want to make a change and then execute 
on it. Also, we ask Eng _not_ to start work on an RLI that is being 
proposed as a TRD until it has been approved (i.e. [TRD Status] field is 
set to ''approved'').','lbirch','IN_DEVELOPMENT',0,'RLIS','synopsis, release_target, state, project_status, trd_status, customers, business_opportunity, ','$release : ReleaseRecord( hasOpenRLIs == true ) 
$user : User( ) 
$record_list : RecordSet( ) from 
 collect( RLIRecord( relname == $release.relname, 
  state != "deferred", 
  trd_status in ("required", "under-review", "rejected", "withdrawn"), 
  npi_program == $user.npi_program_name || 
  alwaysTrue == $user.junos || responsible memberOf $user.reportNames || 
  sw_mgr_responsible memberOf $user.reportNames || 
  st_mgr_responsible memberOf $user.reportNames ) )',1,0,to_timestamp_tz('15-JAN-09 04.42.12.028677000 PM -08:00','DD-MON-RR HH.MI.SS.FF AM TZR'),'release',null,null,null,'Owners of RLIs with TRDs in-process (i.e. [TRD Status] field ...','NO_MILESTONE',0);
