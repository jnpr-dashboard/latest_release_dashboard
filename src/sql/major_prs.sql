-- clear out existing rule
DELETE FROM "RULES" WHERE IDENTIFIER IN ('major_prs');
SET DEFINE ~;

INSERT INTO "RULES" (IDENTIFIER, NAME, OBJECTIVE, DESCRIPTION, OWNER, MILESTONE, HORIZON, COUNTS, QUERY_FIELDS, LHS, WEIGHT, ACTIVE, GROUPING_VARIABLE, LAST_UPDATED, LEDE, SUNSET_MILESTONE, SUNSET_HORIZON)
VALUES (
  'major_prs',
  'Major PRs',
  'HOLD_TO_SCHEDULE',
  'Major PRs should be prioritized over minor ones.',
  'admin',
  'NO_MILESTONE',
  '0',
  'PRS',
  'synopsis, state, problem-level, category, product, planned-release, blocker, submitter-id, responsible, dev-owner, originator, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli,',
  '$release : ReleaseRecord( ) 
$user : User( ) 
$record_list : RecordSet( ) from 
 collect( PRRecord( (problem_level in ("2-CL2","4-CL3","4-IL2","5-IL3")), 
  releases contains $release.relname,  planned_release not matches "^1[1-9]\.[0-9]W[0-9]*", npi == $user.npi_program || 
  alwaysTrue == $user.junos || responsible memberOf $user.reportNames || 
  ( dev_owner memberOf $user.reportNames &&  state != "feedback" ) ) )',
  '0',
  '1',
  'release',
  to_timestamp_tz('24-APR-09 04.36.56.943451000 PM -07:00', 'DD-MON-RR HH.MI.SS.FF AM TZR'),
  'Major PRs should be prioritized over minor ones.', -- LEDE
  'NO_MILESTONE',         -- SUNSET_MILESTONE
  '0'                 -- SUNSET_HORIZON
) ;


