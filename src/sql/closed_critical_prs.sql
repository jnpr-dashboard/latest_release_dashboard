
-- clear out existing rule
DELETE FROM "DASH"."RULES" WHERE IDENTIFIER IN ('closed_critical_prs');
SET DEFINE ~;

INSERT INTO "DASH"."RULES" (IDENTIFIER, NAME, OBJECTIVE, DESCRIPTION, OWNER, MILESTONE, HORIZON, COUNTS, QUERY_FIELDS, LHS, WEIGHT, ACTIVE, GROUPING_VARIABLE, LAST_UPDATED, LEDE, SUNSET_MILESTONE, SUNSET_HORIZON)
VALUES (
  'closed_critical_prs',
  'Closed Critical PRs',
  'P5_STABILITY',
  'Owners of PRs with the [Severity] field set to "critical" must prioritize these ETAs and fixes over non-critical PRs.',
  'admin',
  'NO_MILESTONE',
  '0',
  'CLOSEDPRS',
  'synopsis, state, problem-level, category, product, planned-release, blocker, reported-in, submitter-id, responsible, dev-owner, originator, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli,',
  '$release : ReleaseRecord( ) 
$user : User( ) 
$record_list : RecordSet( ) from 
 collect( ClosedPRRecord( severity == "critical", 
 (category not matches ".*hw-.*" &&  category not matches ".*build.*"), 
  releases contains $release.relname, planned_release not matches "^1[1-9]\.[0-9]W[0-9]*", npi == $user.npi_program || 
  alwaysTrue == $user.junos ||  
  (eval(alwaysTrue == blank_devowner) &&  responsible memberOf $user.reportNames) ||  
  (eval(alwaysTrue != blank_devowner) &&  dev_owner memberOf $user.reportNames)  ))',
  '0',
  '0',
  'release',
  to_timestamp_tz('09-JAN-10 12.40.06.423259000 AM -08:00', 'DD-MON-RR HH.MI.SS.FF AM TZR'),
  'Owners of PRs with the [Severity] field set to "critical" mus...', -- LEDE
  'NO_MILESTONE',         -- SUNSET_MILESTONE
  '0'                 -- SUNSET_HORIZON
) ;


