-- clear out existing rule
DELETE FROM "RULES" WHERE IDENTIFIER IN ('backlog_prs_reference_2010_responsible');
SET DEFINE ~;

INSERT INTO "RULES" 
(IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) 
values (
   'backlog_prs_reference_2010_responsible',
   '2010 Reference Backlog PRs by Responsible 2010-01-31',
   'HOLD_TO_SCHEDULE',
   'All Reference Backlog PR by using responsible as their primary accountability',
   'admin',
   'NO_MILESTONE',
   '0',
   'PRS',
   'synopsis, state, category, product, planned-release, blocker, submitter-id, responsible, dev-owner, originator, systest-owner, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli, created, resolution, feedback-date, jtac-case-id,',
   '$user : User( )
$record_list : RecordSet( )
  from collect(
    OldPRRecord(
      extract_date == "2010-01-31",
      state != "closed",
      (severity == "critical" || severity == "major"),
      (clazz == "bug" || clazz == "unreproducible"),functional_area=="software",
      alwaysTrue == possible_backlog,
      npi == $user.npi_program || alwaysTrue == $user.junos || backlog_pr_responsible memberOf $user.reportNames
    )
  )',
    '1',
    '1',
    to_timestamp_tz('01-NOV-10 12.00.00.511060000 PM -07:00','DD-MON-RR HH.MI.SSXFF AM TZR'),
    '',
    '',
    '',
    '',
    'Reference Backlog PRs ',  -- LEDE
    'NO_MILESTONE',  -- SUNSET_MILESTONE
    '0'  -- SUNSET_HORIZON
);
