
-- clear out existing rule
DELETE FROM "DASH"."RULES" WHERE IDENTIFIER IN ('prs_wo_release');
SET DEFINE ~;

INSERT INTO "DASH"."RULES" (IDENTIFIER, NAME, OBJECTIVE, DESCRIPTION, OWNER, MILESTONE, HORIZON, COUNTS, QUERY_FIELDS, LHS, WEIGHT, ACTIVE, GROUPING_VARIABLE, LAST_UPDATED, LEDE, SUNSET_MILESTONE, SUNSET_HORIZON)
VALUES (
  'prs_wo_release',
  'PRs without a release',
  'HOLD_TO_SCHEDULE',
  'These PRs have no Conf-Committed-Release, Committed-Release, Release or Reported-In value.',
  'admin',
  'NO_MILESTONE',
  '0',
  'PRS',
  'synopsis, state, problem-level, category, product, planned-release, blocker, submitter-id, responsible, dev-owner, originator, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli,',
  '$user : User( ) 
$record_list : RecordSet( ) from 
 collect( PRRecord( $rels : releases -> ( $rels.size() == 1 ), planned_release not matches "^1[1-9]\.[0-9]W[0-9]*",
  releases contains "0.0", 
  npi == $user.npi_program || 
  alwaysTrue == $user.junos || responsible memberOf $user.reportNames ) )',
  '0',
  '1',
  '',
  to_timestamp_tz('13-JUN-09 08.21.14.070791000 PM -07:00', 'DD-MON-RR HH.MI.SS.FF AM TZR'),
  'PRs that dont have a release defined', -- LEDE
  'NO_MILESTONE',           -- SUNSET_MILESTONE
  '0'                       -- SUNSET_HORIZON
) ;
