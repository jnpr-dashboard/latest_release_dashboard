/*
sqlplus dash@DSWTOOLS
@/homes/apatt/workspace/dashboard/src/sql/2011_02_disable_rules.sql
*/

-- 17 rules
-- Rules to disable, will need to fix in the future (14)

update rules set active = 0 where identifier in (
	'unapproved_trds', 
	'rlis_with_missing_func_specs', 
	'rlis_w_ers_spec_not_reqd',
	'small_rlis_wo_dev_complete_date', 
	'med_rlis_wo_dev_complete_date',
	'large_rlis_wo_dev_complete_date',  
	'small_rlis_missed_dev_complete',
	'med_rlis_missed_dev_complete',
	'large_rlis_missed_dev_complete', 
	'dev_compl_med_rlis_wo_utp',
	'dev_compl_small_rlis_wo_utp', 
	'rlis_coming_in_late',
	'rlis_unresourced_for_p1_exit', 
	'specs_not_approved');

-- Rules no longer needed (3):
delete from rules where identifier in (
	'cam_blocker_prs', 
    'specs_wo_tp_approval_in_release', 
    'specs_not_ready_for_review');
    

