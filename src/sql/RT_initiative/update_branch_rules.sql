/**
 * Updating the rules which using branch. Since release-tracking, all branch field will
 *     be read-only and they will use planned-release. So our old-rules which has branch
 *     need to add 'releases' as one of the fields to look for.
 *
 */

-- prs_user_unsheduled_dev_branch 
UPDATE "RULES"
SET lhs='$user : User( )
$record_list : RecordSet( ) from
 collect( PRRecord( (branchRelease in ("unscheduled","n/a") ||  branchReleaseType=="DEV"),
  npi == $user.npi_program ||
  alwaysTrue == $user.junos || 
  responsible memberOf $user.reportNames ||
  (dev_owner memberOf $user.reportNames && state != "feedback")))'
where identifier='prs_user_unscheduled_dev_branch';

-- prs_user_dev_branch
UPDATE "RULES"
SET lhs='$user : User( )
$release : ReleaseRecord( )
$record_list : RecordSet( ) from
 collect( PRRecord( (branchRelease == $release.relname && branchReleaseType=="DEV"),
  npi == $user.npi_program ||
  alwaysTrue == $user.junos || 
  responsible memberOf $user.reportNames ||
  (dev_owner memberOf $user.reportNames && state != "feedback")))'
WHERE identifier='prs_user_dev_branch';

-- prs_user_private_branch
UPDATE "RULES"
SET lhs ='$user : User( )
$record_list : RecordSet( ) from
 collect( PRRecord( (branchReleaseType=="PVT"),
  npi == $user.npi_program ||
  alwaysTrue == $user.junos || 
  responsible memberOf $user.reportNames ||
  (dev_owner memberOf $user.reportNames && state != "feedback")))'
WHERE identifier='prs_user_private_branch';

-- regression_prs_fix
UPDATE "RULES"
SET lhs='$release : ReleaseRecord( )
$user : User( )
$record_list : RecordSet( ) from
 collect( PRRecord( attributes matches ".*regression-pr.*", clazz == "bug",
 (category not matches ".*hw-.*" && category not matches ".*build.*"),
 (releases contains $release.relname || branchRelease == $release.relname ),
  npi == $user.npi_program ||
  alwaysTrue == $user.junos ||
   ((dev_owner memberOf $user.reportNames  && ( state == "info" || 
           (state=="feedback" && eval(alwaysTrue != blank_devowner))))
   ||(responsible memberOf $user.reportNames && (state == "open" || state == "analyzed" || 
           (state=="feedback" && eval(alwaysTrue == blank_devowner))))
   ))) '
WHERE identifier='regression_prs_fix';

-- closed_regression_prs
UPDATE "RULES"
SET lhs='$release : ReleaseRecord( )
$user : User( )
$record_list : RecordSet( ) from
 collect( ClosedPRRecord( attributes matches ".*regression-pr.*",
 (category not matches ".*hw-.*" && category not matches ".*build.*"),
 (releases contains $release.relname || branchRelease == $release.relname  ),
 npi == $user.npi_program ||
  alwaysTrue == $user.junos || 
  (eval(alwaysTrue == blank_devowner) && responsible memberOf $user.reportNames) || 
  (eval(alwaysTrue != blank_devowner) && dev_owner memberOf $user.reportNames)  ))'
WHERE identifier='closed_regression_prs';

-- major_prs_fix
UPDATE "RULES"
SET lhs='$release : ReleaseRecord( )
$user : User( )
$record_list : RecordSet( ) from
 collect( PRRecord( severity == "major", clazz == "bug",
 (category not matches ".*hw-.*" && category not matches ".*build.*"),
 (releases contains $release.relname || branchRelease == $release.relname ),
  npi == $user.npi_program ||
  alwaysTrue == $user.junos ||
   ((dev_owner memberOf $user.reportNames  && ( state == "info" || 
           (state=="feedback" && eval(alwaysTrue != blank_devowner))))
   ||(responsible memberOf $user.reportNames && (state == "open" || state == "analyzed" || 
           (state=="feedback" && eval(alwaysTrue == blank_devowner))))
   )))'
WHERE identifier='major_prs_fix';

-- major_closed_pr
UPDATE "RULES"
SET lhs='$release : ReleaseRecord( )
$user : User( )
$record_list : RecordSet( ) from
 collect( ClosedPRRecord( severity == "major",
 (category not matches ".*hw-.*" && category not matches ".*build.*"),
 (releases contains $release.relname || branchRelease == $release.relname ),
  npi == $user.npi_program ||
  alwaysTrue == $user.junos || 
  (eval(alwaysTrue == blank_devowner) && responsible memberOf $user.reportNames) || 
  (eval(alwaysTrue != blank_devowner) && dev_owner memberOf $user.reportNames)  ))'
WHERE identifier='major_closed_pr';

update "RULES"
set query_fields='synopsis, state, severity, category, priority, product, planned-release, blocker, submitter-id, responsible, dev-owner, originator, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli, created, resolution, feedback-date, jtac-case-id,'
where identifier='current_backlog_prs' or identifier='history_backlog_prs' or identifier='forward_view_backlog_prs';

update "RULES"
set query_fields='synopsis, state, severity, category, priority, product, planned-release, blocker, reported-in, submitter-id, responsible, dev-owner, originator, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli,'
where identifier like '%_fix' or identifier like 'closed_%' or identifier='major_closed_pr';

update "RULES"
set query_fields='synopsis, state, severity, category, priority, product, planned-release, blocker, submitter-id, responsible, dev-owner, originator, systest-owner, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli, created, resolution, feedback-date, jtac-case-id,'
where identifier='current_backlog_prs' or identifier='history_backlog_prs' or identifier='forward_view_backlog_prs';

