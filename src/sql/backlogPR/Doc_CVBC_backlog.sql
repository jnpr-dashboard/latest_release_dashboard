-- updating the column for identifier to 50 character
ALTER TABLE "RULES"
MODIFY "IDENTIFIER" VARCHAR(50) ;


-- Create new Backlog PR for Doc PR and CVBC
DELETE FROM "RULES"
WHERE identifier='backlog_prs_reference_doc';
DELETE FROM "RULES"
WHERE identifier='backlog_prs_current_doc';
DELETE FROM "RULES"
WHERE identifier='backlog_prs_forward_doc';

--------------------------------------------------------------------------------
INSERT INTO "DASH"."RULES" (IDENTIFIER, NAME, OBJECTIVE, DESCRIPTION, OWNER, MILESTONE, HORIZON, COUNTS, QUERY_FIELDS, LHS, WEIGHT, ACTIVE, LAST_UPDATED, LEDE, SUNSET_MILESTONE, SUNSET_HORIZON)
VALUES (
  'backlog_prs_reference_doc',
  'Reference Backlog Doc PRs',
  'HOLD_TO_SCHEDULE',
  'All Reference Backlog Doc PR',
  'admin',
  'NO_MILESTONE',
  '0',
  'PRS',
  'synopsis, state, severity, category, priority, product, planned-release, blocker, submitter-id, techpubs-owner, originator, systest-owner, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli, created, resolution, feedback-date, jtac-case-id,',
'$user : User( )
$record_list : RecordSet( )
  from collect(
    OldPRRecord(
        extract_date == "2010-12-31",
        state != "closed", 
        (severity == "critical" || severity == "major"),
        (clazz == "bug" || clazz == "unreproducible" ), functional_area=="documentation",
        alwaysTrue == possible_backlog,
        npi == $user.npi_program || alwaysTrue == $user.junos ||
        backlog_pr_techpub_owner memberOf $user.reportNames 
    )
  )',
  '0',
  '1',
  to_timestamp_tz('31-DEC-10 12.00.00.511060000 PM -07:00', 'DD-MON-RR HH.MI.SS.FF AM TZR'),
  'Reference Backlog Doc PRs ', -- LEDE
  'NO_MILESTONE',           -- SUNSET_MILESTONE
  '0'                      -- SUNSET_HORIZON
);
-----------------------------------------------------

INSERT INTO "RULES" VALUES ('backlog_prs_current_doc', 'Current Backlog Doc PRs ', 'HOLD_TO_SCHEDULE', 'Current Backlog Doc PRs ', 'admin', 'NO_MILESTONE', '0', 'PRS', 
'synopsis, state, severity, category, priority, product, planned-release, cvbc-documented-in, blocker, submitter-id, techpubs-owner, originator, systest-owner, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli, created, resolution, feedback-date, jtac-case-id,', 
'$user : User( )
$record_list : RecordSet( ) from
 collect( PRRecord( (severity == "critical" || severity == "major"),
    (clazz == "bug" || clazz == "unreproducible" ),functional_area=="documentation",
    alwaysTrue == possible_backlog,
    npi == $user.npi_program || alwaysTrue == $user.junos ||
    backlog_pr_techpub_owner memberOf $user.reportNames ))
', '0', '1', TO_TIMESTAMP_TZ('2010-11-01 12:00:00:511060 -07:00', 'YYYY-MM-DD HH24:MI:SS:FF6 TZR'), null, null, null, null, 'Current Backlog Doc PRs', 'NO_MILESTONE', '0');  
-----------------------------------------------------

INSERT INTO "RULES" VALUES ('backlog_prs_forward_doc', 'Forward View Backlog Doc PRs ', 'HOLD_TO_SCHEDULE', 'Forward View / Upcoming Backlog Doc PRs ', 'admin', 'NO_MILESTONE', '0', 'PRS', 
'synopsis, state, severity, category, priority, product, planned-release, cvbc-documented-in, blocker, submitter-id, techpubs-owner, originator, systest-owner, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli, created, resolution, feedback-date, jtac-case-id,', 
'$user : User( )
$record_list : RecordSet( ) from
 collect( PRRecord( (severity == "critical" || severity == "major"),
    (clazz == "bug" || clazz == "unreproducible" ),functional_area=="documentation",
    (alwaysTrue == possible_forward_view_backlog || alwaysTrue == possible_backlog),
    npi == $user.npi_program || alwaysTrue == $user.junos ||
    backlog_pr_techpub_owner memberOf $user.reportNames ))
', '0', '1', TO_TIMESTAMP_TZ('2010-11-01 12:00:00:511060 -07:00', 'YYYY-MM-DD HH24:MI:SS:FF6 TZR'), null, null, null, null, 'Forward View Backlog Doc PRs', 'NO_MILESTONE', '0');  
-----------------------------------------------------

DELETE FROM "RULES"
WHERE identifier='backlog_prs_reference_cvbc';
DELETE FROM "RULES"
WHERE identifier='backlog_prs_current_cvbc';
DELETE FROM "RULES"
WHERE identifier='backlog_prs_forward_cvbc';

-----------------------------------------------------

INSERT INTO "DASH"."RULES" (IDENTIFIER, NAME, OBJECTIVE, DESCRIPTION, OWNER, MILESTONE, HORIZON, COUNTS, QUERY_FIELDS, LHS, WEIGHT, ACTIVE, LAST_UPDATED, LEDE, SUNSET_MILESTONE, SUNSET_HORIZON)
VALUES (
  'backlog_prs_reference_cvbc',
  'Reference Backlog CVBC PRs',
  'HOLD_TO_SCHEDULE',
  'All Reference Backlog CVBC PR',
  'admin',
  'NO_MILESTONE',
  '0',
  'PRS',
  'synopsis, state, cust-visible-behavior-changed, severity, category, priority, product, planned-release, blocker, submitter-id, techpubs-owner, originator, systest-owner, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli, created, resolution, feedback-date, jtac-case-id,',
'$user : User( )
$record_list : RecordSet( )
  from collect(
    OldPRRecord(
        extract_date == "2010-12-31",
        (cust_visible_behavior_changed == "yes" || cust_visible_behavior_changed == "yes-ready-for-review"),
        alwaysTrue == possible_backlog,
        npi == $user.npi_program || alwaysTrue == $user.junos ||
        backlog_pr_techpub_owner memberOf $user.reportNames 
    )
  )',
  '0',
  '1',
  to_timestamp_tz('31-DEC-10 12.00.00.511060000 PM -07:00', 'DD-MON-RR HH.MI.SS.FF AM TZR'),
  'Reference Backlog CVBC PRs ', -- LEDE
  'NO_MILESTONE',           -- SUNSET_MILESTONE
  '0'                      -- SUNSET_HORIZON
);
-----------------------------------------------------

INSERT INTO "RULES" VALUES ('backlog_prs_current_cvbc', 'Current Backlog for CVBC PRs', 'HOLD_TO_SCHEDULE', 'All Current Backlog for CVBC PRs ', 'admin', 'NO_MILESTONE', '0', 'PRS', 
'synopsis, state, cust-visible-behavior-changed, severity, category, priority, product, planned-release, cvbc-documented-in, blocker, submitter-id, techpubs-owner, originator, systest-owner, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli, created, resolution, feedback-date, jtac-case-id,', 
'$user : User( )
$record_list : RecordSet( ) from
 collect( UndeadCvbcRecord( 
    alwaysTrue == possible_backlog,
    alwaysTrue == $user.junos ||
    backlog_pr_techpub_owner memberOf $user.reportNames ))
', '0', '1', TO_TIMESTAMP_TZ('2010-11-01 12:00:00:511060 -07:00', 'YYYY-MM-DD HH24:MI:SS:FF6 TZR'), null, null, null, null, 'Current Backlog for CVBC PRs', 'NO_MILESTONE', '0');  
-----------------------------------------------------

INSERT INTO "RULES" VALUES ('backlog_prs_forward_cvbc', 'Forward View Backlog for CVBC PRs', 'HOLD_TO_SCHEDULE', 'Forward View / Upcoming Backlog for CVBC PRs ', 'admin', 'NO_MILESTONE', '0', 'PRS',
'synopsis, state, cust-visible-behavior-changed, severity, category, priority, product, planned-release, cvbc-documented-in, blocker, submitter-id, techpubs-owner, originator, systest-owner, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli, created, resolution, feedback-date, jtac-case-id,', 
'$user : User( )
$record_list : RecordSet( ) from
 collect( UndeadCvbcRecord( 
    (alwaysTrue == possible_backlog || alwaysTrue == possible_forward_view_backlog),
    alwaysTrue == $user.junos ||
    backlog_pr_techpub_owner memberOf $user.reportNames ))
', '0', '1', TO_TIMESTAMP_TZ('2010-11-01 12:00:00:511060 -07:00', 'YYYY-MM-DD HH24:MI:SS:FF6 TZR'), null, null, null, null, 'Forward View Backlog for CVBC PRs', 'NO_MILESTONE', '0');  

