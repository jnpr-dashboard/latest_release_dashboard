/* 
 * Runs for Release 5.6 November 1, 2010
 * updating the column for identifier to 50 character
 */

ALTER TABLE "RULES"
MODIFY "IDENTIFIER" VARCHAR(50) ;

-- Changes in Backlog PR based on responsible
UPDATE "RULES"
   set lhs = '$user : User( )
$record_list : RecordSet( ) from
 collect( PRRecord( (severity == "critical" || severity == "major"),
    (clazz == "bug" || clazz == "unreproducible"),functional_area=="software",
    alwaysTrue == possible_backlog,
    npi == $user.npi_program || alwaysTrue == $user.junos ||
    backlog_pr_dev_owner memberOf $user.reportNames ) )'
   WHERE identifier='current_backlog_prs';
   
DELETE FROM "RULES"
WHERE identifier='history_backlog_prs';

-- Change the rule name of history to reference (which we used in column and UI)
DELETE FROM "RULES"
WHERE identifier='reference_backlog_prs';

INSERT INTO "RULES" VALUES ('reference_backlog_prs', 'Reference Backlog PRs', 'HOLD_TO_SCHEDULE', 'All Reference Backlog PR which is snapshot at 1/31/2010', 'admin', 'NO_MILESTONE', '0', 'PRS', 'synopsis, state, severity, category, priority, product, planned-release, blocker, submitter-id, responsible, dev-owner, originator, systest-owner, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli, created, resolution, feedback-date, jtac-case-id,', 
'$user : User( )
$record_list : RecordSet( ) from
 collect( OldPRRecord( (severity == "critical" || severity == "major"),
    (clazz == "bug" || clazz == "unreproducible" ),functional_area=="software",
    alwaysTrue == possible_backlog,
    npi == $user.npi_program || alwaysTrue == $user.junos ||
    backlog_pr_dev_owner memberOf $user.reportNames ))
', '1', '1', TO_TIMESTAMP_TZ('2010-11-01 12:00:00:511060 -07:00', 'YYYY-MM-DD HH24:MI:SS:FF6 TZR'), null, null, null, null, 'Reference Backlog PRs ', 'NO_MILESTONE', '0');  

UPDATE "RULES"
   set lhs = '$user : User( )
$record_list : RecordSet( ) from
 collect( PRRecord( (severity == "critical" || severity == "major"),
    (clazz == "bug" || clazz == "unreproducible"),functional_area=="software",
    (alwaysTrue == possible_forward_view_backlog || alwaysTrue == possible_backlog),
    npi == $user.npi_program || alwaysTrue == $user.junos ||
    backlog_pr_dev_owner memberOf $user.reportNames ) )'
   WHERE identifier='forward_view_backlog_prs';

/*
 * Create new Backlog PR based on Responsible
 * 
 * History View backlog (Responsible and resp!=dev_owner)
 */
DELETE FROM "RULES"
WHERE identifier='history_backlog_prs';
   
DELETE FROM "RULES"
WHERE identifier='reference_backlog_prs_responsible';

INSERT INTO "RULES" VALUES ('reference_backlog_prs_responsible', 'Reference Backlog PRs by Responsible', 'HOLD_TO_SCHEDULE', 'All Reference Backlog PR by using responsible as their primary accountability', 'admin', 'NO_MILESTONE', '0', 'PRS', 'synopsis, state, severity, category, priority, product, planned-release, blocker, submitter-id, responsible, dev-owner, originator, systest-owner, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli, created, resolution, feedback-date, jtac-case-id,', 
'$user : User( )
$record_list : RecordSet( ) from
 collect( OldPRRecord( (severity == "critical" || severity == "major"),
    (clazz == "bug" || clazz == "unreproducible" ),functional_area=="software",
    alwaysTrue == possible_backlog,
    npi == $user.npi_program || alwaysTrue == $user.junos ||
    backlog_pr_responsible memberOf $user.reportNames ))
', '1', '1', TO_TIMESTAMP_TZ('2010-11-01 12:00:00:511060 -07:00', 'YYYY-MM-DD HH24:MI:SS:FF6 TZR'), null, null, null, null, 'Reference Backlog PRs by Responsible', 'NO_MILESTONE', '0');  

DELETE FROM "RULES"
WHERE identifier='diff_reference_backlog_prs_responsible';

INSERT INTO "RULES" VALUES ('diff_reference_backlog_prs_responsible', 'Reference Backlog PRs by Responsible != Dev-Owner', 'HOLD_TO_SCHEDULE', 'All Reference Backlog PR by using responsible as their primary accountability where the responsible != dev-owner', 'admin', 'NO_MILESTONE', '0', 'PRS', 'synopsis, state, severity, category, priority, product, planned-release, blocker, submitter-id, responsible, dev-owner, originator, systest-owner, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli, created, resolution, feedback-date, jtac-case-id,', 
'$user : User( )
$record_list : RecordSet( ) from
 collect( OldPRRecord( (severity == "critical" || severity == "major"),
    (clazz == "bug" || clazz == "unreproducible" ),functional_area=="software",
    alwaysTrue == possible_backlog,
    real_responsible != dev_owner, 
    npi == $user.npi_program || alwaysTrue == $user.junos ||
    backlog_pr_responsible memberOf $user.reportNames ))
', '1', '1', TO_TIMESTAMP_TZ('2010-11-01 12:00:00:511060 -07:00', 'YYYY-MM-DD HH24:MI:SS:FF6 TZR'), null, null, null, null, 'Reference Backlog PRs by Responsible != Dev-Owner', 'NO_MILESTONE', '0');  


-- Current backlog (Responsible and resp!=dev_owner)

DELETE FROM "RULES"
WHERE identifier='current_backlog_prs_responsible';

INSERT INTO "RULES" VALUES ('current_backlog_prs_responsible', 'Current Backlog PRs by Responsible', 'HOLD_TO_SCHEDULE', 'All Current Backlog PR by using responsible as their primary accountability', 'admin', 'NO_MILESTONE', '0', 'PRS', 'synopsis, state, severity, category, priority, product, planned-release, blocker, submitter-id, responsible, dev-owner, originator, systest-owner, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli, created, resolution, feedback-date, jtac-case-id,', 
'$user : User( )
$record_list : RecordSet( ) from
 collect( PRRecord( (severity == "critical" || severity == "major"),
    (clazz == "bug" || clazz == "unreproducible"),functional_area=="software",
    alwaysTrue == possible_backlog,
    npi == $user.npi_program || alwaysTrue == $user.junos ||
    backlog_pr_responsible memberOf $user.reportNames ) )
', '1', '1', TO_TIMESTAMP_TZ('2010-11-01 12:00:00:511060 -07:00', 'YYYY-MM-DD HH24:MI:SS:FF6 TZR'), null, null, null, null, 'Current Backlog PRs by Responsible', 'NO_MILESTONE', '0');  

DELETE FROM "RULES"
WHERE identifier='diff_current_backlog_prs_responsible';

INSERT INTO "RULES" VALUES ('diff_current_backlog_prs_responsible', 'Current Backlog PRs by Responsible!=Dev-Owner', 'HOLD_TO_SCHEDULE', 'All Current Backlog PR by using responsible as their primary accountability where the responsible != dev-owner', 'admin', 'NO_MILESTONE', '0', 'PRS', 'synopsis, state, severity, category, priority, product, planned-release, blocker, submitter-id, responsible, dev-owner, originator, systest-owner, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli, created, resolution, feedback-date, jtac-case-id,', 
'$user : User( )
$record_list : RecordSet( ) from
 collect( PRRecord( (severity == "critical" || severity == "major"),
    (clazz == "bug" || clazz == "unreproducible"),functional_area=="software",
    alwaysTrue == possible_backlog, real_responsible != dev_owner,
    npi == $user.npi_program || alwaysTrue == $user.junos ||
    backlog_pr_responsible memberOf $user.reportNames ) )
', '1', '1', TO_TIMESTAMP_TZ('2010-11-01 12:00:00:511060 -07:00', 'YYYY-MM-DD HH24:MI:SS:FF6 TZR'), null, null, null, null, 'Current Backlog PRs by Responsible by Responsible!=Dev-Owner', 'NO_MILESTONE', '0');  


-- Forward View backlog (Responsible and resp!=dev_owner)

DELETE FROM "RULES"
WHERE identifier='forward_view_backlog_prs_responsible';

DELETE FROM "RULES"
WHERE identifier='diff_forward_view_backlog_prs_responsible';

INSERT INTO "RULES" VALUES ('forward_view_backlog_prs_responsible', 'Forward View / Future Backlog PRs by Responsible', 'HOLD_TO_SCHEDULE', 'All Future Backlog PR by using responsible as their primary accountability', 'admin', 'NO_MILESTONE', '0', 'PRS', 'synopsis, state, severity, category, priority, product, planned-release, blocker, submitter-id, responsible, dev-owner, originator, systest-owner, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli, created, resolution, feedback-date, jtac-case-id,', 
'$user : User( )
$record_list : RecordSet( ) from
 collect( PRRecord( (severity == "critical" || severity == "major"),
    (clazz == "bug" || clazz == "unreproducible" ),functional_area=="software",
    (alwaysTrue == possible_forward_view_backlog || alwaysTrue == possible_backlog),
    npi == $user.npi_program || alwaysTrue == $user.junos ||
    backlog_pr_responsible memberOf $user.reportNames ))
', '1', '1', TO_TIMESTAMP_TZ('2010-11-01 12:00:00:511060 -07:00', 'YYYY-MM-DD HH24:MI:SS:FF6 TZR'), null, null, null, null, 'Forward View / Future Backlog PRs by Responsible', 'NO_MILESTONE', '0');  

INSERT INTO "RULES" VALUES ('diff_forward_view_backlog_prs_responsible', 'Forward View / Future Backlog PRs by Responsible != Dev-Owner', 'HOLD_TO_SCHEDULE', 'All Future Backlog PR by using responsible as their primary accountability where the responsible != dev-owner', 'admin', 'NO_MILESTONE', '0', 'PRS', 'synopsis, state, severity, category, priority, product, planned-release, blocker, submitter-id, responsible, dev-owner, originator, systest-owner, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli, created, resolution, feedback-date, jtac-case-id,', 
'$user : User( )
$record_list : RecordSet( ) from
 collect( PRRecord( (severity == "critical" || severity == "major"),
    (clazz == "bug" || clazz == "unreproducible" ),functional_area=="software",
    (alwaysTrue == possible_forward_view_backlog || alwaysTrue == possible_backlog),
    real_responsible != dev_owner, 
    npi == $user.npi_program || alwaysTrue == $user.junos ||
    backlog_pr_responsible memberOf $user.reportNames ))
', '1', '1', TO_TIMESTAMP_TZ('2010-11-01 12:00:00:511060 -07:00', 'YYYY-MM-DD HH24:MI:SS:FF6 TZR'), null, null, null, null, 'Forward View / Future Backlog PRs by Responsible', 'NO_MILESTONE', '0');  

