-- sqlplus dash@DSWTOOLS
-- @/homes/apatt/workspace/dashboard/src/sql/backlogPR/backlog_2011_changes.sql

/*
 * delete existing rules because they are going to be renamed
 */
-- deal with diff rules

DELETE FROM "DASH"."RULES" WHERE IDENTIFIER IN 
('current_backlog_prs'                   ,'reference_backlog_prs'                ,'forward_view_backlog_prs',
 'reference_backlog_prs_responsible'     ,'current_backlog_prs_responsible'      ,'forward_view_backlog_prs_responsible',
 'diff_reference_backlog_prs_responsible','diff_current_backlog_prs_responsible' ,'diff_forward_view_backlog_prs_responsible');

 /*
 * delete rules we are going to create so we can rerun
 */
DELETE FROM "DASH"."RULES" WHERE IDENTIFIER IN 
('backlog_prs_reference_2010'                    ,'backlog_prs_current_2010'                    ,'backlog_prs_forward_2010',
 'backlog_prs_reference_2010_responsible'        ,'backlog_prs_current_2010_responsible'        ,'backlog_prs_forward_2010_responsible',
 'backlog_prs_reference_2010_responsible_not_dev','backlog_prs_current_2010_responsible_not_dev','backlog_prs_forward_2010_responsible_not_dev',
 'backlog_prs_reference_2010-12-31',
 'backlog_prs_reference'                         ,'backlog_prs_current'                         ,'backlog_prs_forward',
 'backlog_prs_reference_responsible'             ,'backlog_prs_current_responsible'             ,'backlog_prs_forward_responsible', 
 'backlog_prs_reference_responsible_not_dev'     ,'backlog_prs_current_responsible_not_dev'     ,'backlog_prs_forward_responsible_not_dev'
 );
 
 /* ------------------------
  * rename define 2010 rules with new names and slight changes in how reference works
  */
-- backlog_prs_reference_2010
-- ORIGINALLY: reference_backlog_prs
--------------------------------------------------------------------------------
INSERT INTO "DASH"."RULES" (IDENTIFIER, NAME, OBJECTIVE, DESCRIPTION, OWNER, MILESTONE, HORIZON, COUNTS, QUERY_FIELDS, LHS, WEIGHT, ACTIVE, LAST_UPDATED, LEDE, SUNSET_MILESTONE, SUNSET_HORIZON)
VALUES (
  'backlog_prs_reference_2010',
  '2010 Reference Backlog PRs 2010-01-31',
  'HOLD_TO_SCHEDULE',
  'All Reference Backlog PR',
  'admin',
  'NO_MILESTONE',
  '0',
  'PRS',
  'synopsis, state, severity, category, priority, product, planned-release, blocker, submitter-id, responsible, dev-owner, originator, systest-owner, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli, created, resolution, feedback-date, jtac-case-id,',
'$user : User( )
$record_list : RecordSet( )
  from collect(
    OldPRRecord(
      extract_date == "2010-01-31",
      state != "closed",
      (severity == "critical" || severity == "major"),
      (clazz == "bug" || clazz == "unreproducible"),
      functional_area=="software",
      alwaysTrue == possible_backlog,
      npi == $user.npi_program || alwaysTrue == $user.junos || backlog_pr_dev_owner memberOf $user.reportNames
    )
  )',
  '1',
  '1',
  to_timestamp_tz('01-NOV-10 12.00.00.511060000 PM -07:00', 'DD-MON-RR HH.MI.SS.FF AM TZR'),
  'Reference Backlog PRs ', -- LEDE
  'NO_MILESTONE',           -- SUNSET_MILESTONE
  '0'                       -- SUNSET_HORIZON
);
--------------------------------------------------------------------------------

-- backlog_prs_current_2010
-- ORIGINALLY: current_backlog_prs
--------------------------------------------------------------------------------
INSERT INTO "DASH"."RULES" (IDENTIFIER, NAME, OBJECTIVE, DESCRIPTION, OWNER, MILESTONE, HORIZON, COUNTS, QUERY_FIELDS, LHS, WEIGHT, ACTIVE, LAST_UPDATED, LEDE, SUNSET_MILESTONE, SUNSET_HORIZON)
VALUES (
  'backlog_prs_current_2010',
  '2010 Current Backlog PRs',
  'HOLD_TO_SCHEDULE',
  'All current backlog PR -- up to date',
  'admin',
  'NO_MILESTONE',
  '0',
  'PRS',
  'synopsis, state, severity, category, priority, product, planned-release, blocker, submitter-id, responsible, dev-owner, originator, systest-owner, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli, created, resolution, feedback-date, jtac-case-id,',
'$user : User( )
$record_list : RecordSet( )
  from collect(
    PRRecord(
    (severity == "critical" || severity == "major"),
    (clazz == "bug" || clazz == "unreproducible"),
    functional_area=="software",
    alwaysTrue == possible_backlog,
    npi == $user.npi_program || alwaysTrue == $user.junos || backlog_pr_dev_owner memberOf $user.reportNames
    )
  )',
  '1',
  '1',
  to_timestamp_tz('01-NOV-10 12.00.00.511060000 PM -07:00', 'DD-MON-RR HH.MI.SS.FF AM TZR'),
  'Reference Backlog PRs ', -- LEDE
  'NO_MILESTONE',           -- SUNSET_MILESTONE
  '0'                       -- SUNSET_HORIZON
);

-- backlog_prs_forward_2010
-- ORIGINALLY: forward_view_backlog_prs
--------------------------------------------------------------------------------
INSERT INTO "DASH"."RULES" (IDENTIFIER, NAME, OBJECTIVE, DESCRIPTION, OWNER, MILESTONE, HORIZON, COUNTS, QUERY_FIELDS, LHS, WEIGHT, ACTIVE, LAST_UPDATED, LEDE, SUNSET_MILESTONE, SUNSET_HORIZON)
VALUES (
  'backlog_prs_forward_2010',
  '2010 Forward View Backlog PR',
  'HOLD_TO_SCHEDULE',
  'PRs which will be counted as current backlog in next R1 deploy',
  'admin',
  'NO_MILESTONE',
  '0',
  'PRS',
  'synopsis, state, severity, category, priority, product, planned-release, blocker, submitter-id, responsible, dev-owner, originator, systest-owner, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli, created, resolution, feedback-date, jtac-case-id,',
'$user : User( )
$record_list : RecordSet( )
  from collect(
    PRRecord(
      (severity == "critical" || severity == "major"), 
      (clazz == "bug" || clazz == "unreproducible"),functional_area=="software",
      (alwaysTrue == possible_forward_view_backlog || alwaysTrue == possible_backlog),
      npi == $user.npi_program || alwaysTrue == $user.junos ||
      backlog_pr_dev_owner memberOf $user.reportNames
    )
  )', 
  '1',
  '1',
  to_timestamp_tz('01-NOV-10 12.00.00.511060000 PM -07:00', 'DD-MON-RR HH.MI.SS.FF AM TZR'),
  'Reference Backlog PRs ', -- LEDE
  'NO_MILESTONE',           -- SUNSET_MILESTONE
  '0'                       -- SUNSET_HORIZON
);


-- backlog_prs_reference_2010_responsible
-- ORIGINALLY: reference_backlog_prs_responsible
--------------------------------------------------------------------------------
INSERT INTO "DASH"."RULES" (IDENTIFIER, NAME, OBJECTIVE, DESCRIPTION, OWNER, MILESTONE, HORIZON, COUNTS, QUERY_FIELDS, LHS, WEIGHT, ACTIVE, LAST_UPDATED, LEDE, SUNSET_MILESTONE, SUNSET_HORIZON)
VALUES (
  'backlog_prs_reference_2010_responsible',
  '2010 Reference Backlog PRs by Responsible 2010-01-31',
  'HOLD_TO_SCHEDULE',
  'All Reference Backlog PR by using responsible as their primary accountability',
  'admin',
  'NO_MILESTONE',
  '0',
  'PRS',
  'synopsis, state, severity, category, priority, product, planned-release, blocker, submitter-id, responsible, dev-owner, originator, systest-owner, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli, created, resolution, feedback-date, jtac-case-id,',
'$user : User( )
$record_list : RecordSet( )
  from collect(
    OldPRRecord(
      extract_date == "2010-01-31",
      state != "closed",
      (severity == "critical" || severity == "major"),
      (clazz == "bug" || clazz == "unreproducible"),functional_area=="software",
      alwaysTrue == possible_backlog,
      npi == $user.npi_program || alwaysTrue == $user.junos || backlog_pr_responsible memberOf $user.reportNames
    )
  )',
  '1',
  '1',
  to_timestamp_tz('01-NOV-10 12.00.00.511060000 PM -07:00', 'DD-MON-RR HH.MI.SS.FF AM TZR'),
  'Reference Backlog PRs ', -- LEDE
  'NO_MILESTONE',           -- SUNSET_MILESTONE
  '0'                       -- SUNSET_HORIZON
);

-- backlog_prs_current_2010_responsible
-- ORIGINALLY: current_backlog_prs_responsible
--------------------------------------------------------------------------------
INSERT INTO "DASH"."RULES" (IDENTIFIER, NAME, OBJECTIVE, DESCRIPTION, OWNER, MILESTONE, HORIZON, COUNTS, QUERY_FIELDS, LHS, WEIGHT, ACTIVE, LAST_UPDATED, LEDE, SUNSET_MILESTONE, SUNSET_HORIZON)
VALUES (
  'backlog_prs_current_2010_responsible',
  '2010 Current Backlog PRs by Responsible',
  'HOLD_TO_SCHEDULE',
  'All Current Backlog PR by using responsible as their primary accountability',
  'admin',
  'NO_MILESTONE',
  '0',
  'PRS',
  'synopsis, state, severity, category, priority, product, planned-release, blocker, submitter-id, responsible, dev-owner, originator, systest-owner, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli, created, resolution, feedback-date, jtac-case-id,',
'$user : User( )
$record_list : RecordSet( )
  from collect(
    PRRecord(
      (severity == "critical" || severity == "major"), 
      (clazz == "bug" || clazz == "unreproducible"),functional_area=="software",
      alwaysTrue == possible_backlog,
      npi == $user.npi_program || alwaysTrue == $user.junos ||
      backlog_pr_responsible memberOf $user.reportNames
    )
  )', 
  '1',
  '1',
  to_timestamp_tz('01-NOV-10 12.00.00.511060000 PM -07:00', 'DD-MON-RR HH.MI.SS.FF AM TZR'),
  'Reference Backlog PRs ', -- LEDE
  'NO_MILESTONE',           -- SUNSET_MILESTONE
  '0'                       -- SUNSET_HORIZON
);


-- backlog_prs_forward_2010_responsible
-- ORIGINALLY: forward_view_backlog_prs_responsible
--------------------------------------------------------------------------------
INSERT INTO "DASH"."RULES" (IDENTIFIER, NAME, OBJECTIVE, DESCRIPTION, OWNER, MILESTONE, HORIZON, COUNTS, QUERY_FIELDS, LHS, WEIGHT, ACTIVE, LAST_UPDATED, LEDE, SUNSET_MILESTONE, SUNSET_HORIZON)
VALUES (
  'backlog_prs_forward_2010_responsible',
  '2010 Forward View / Future Backlog PRs by Responsible',
  'HOLD_TO_SCHEDULE',
  'All Future Backlog PR by using responsible as their primary accountability',
  'admin',
  'NO_MILESTONE',
  '0',
  'PRS',
  'synopsis, state, severity, category, priority, product, planned-release, blocker, submitter-id, responsible, dev-owner, originator, systest-owner, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli, created, resolution, feedback-date, jtac-case-id,',
'$user : User( )
$record_list : RecordSet( )
  from collect(
    PRRecord(
      (severity == "critical" || severity == "major"), 
      (clazz == "bug" || clazz == "unreproducible"),functional_area=="software",
      (alwaysTrue == possible_forward_view_backlog || alwaysTrue == possible_backlog),
      npi == $user.npi_program || alwaysTrue == $user.junos ||
      backlog_pr_responsible memberOf $user.reportNames
    )
  )', 
  '1',
  '1',
  to_timestamp_tz('01-NOV-10 12.00.00.511060000 PM -07:00', 'DD-MON-RR HH.MI.SS.FF AM TZR'),
  'Reference Backlog PRs ', -- LEDE
  'NO_MILESTONE',           -- SUNSET_MILESTONE
  '0'                       -- SUNSET_HORIZON
);

-- backlog_prs_reference_2010_responsible_not_dev
-- ORIGINALLY: diff_reference_backlog_prs_responsible
--------------------------------------------------------------------------------
INSERT INTO "DASH"."RULES" (IDENTIFIER, NAME, OBJECTIVE, DESCRIPTION, OWNER, MILESTONE, HORIZON, COUNTS, QUERY_FIELDS, LHS, WEIGHT, ACTIVE, LAST_UPDATED, LEDE, SUNSET_MILESTONE, SUNSET_HORIZON)
VALUES (
  'backlog_prs_reference_2010_responsible_not_dev',
  '2010 RReference Backlog PRs by Responsible != Dev-Owner 2010-01-31',
  'HOLD_TO_SCHEDULE',
  'All Reference Backlog PR by using responsible as their primary accountability where the responsible != dev-owner',
  'admin',
  'NO_MILESTONE',
  '0',
  'PRS',
  'synopsis, state, severity, category, priority, product, planned-release, blocker, submitter-id, responsible, dev-owner, originator, systest-owner, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli, created, resolution, feedback-date, jtac-case-id,',
'$user : User( )
$record_list : RecordSet( ) from
 collect( 
    OldPRRecord(
        extract_date == "2010-01-31", 
        state != "closed",
        (severity == "critical" || severity == "major"),
        (clazz == "bug" || clazz == "unreproducible" ),functional_area=="software",
        alwaysTrue == possible_backlog,
        real_responsible != dev_owner,
        npi == $user.npi_program || alwaysTrue == $user.junos ||
        backlog_pr_responsible memberOf $user.reportNames ))
',
  '1',
  '1',
  to_timestamp_tz('01-NOV-10 12.00.00.511060000 PM -07:00', 'DD-MON-RR HH.MI.SS.FF AM TZR'),
  'Reference Backlog PRs by Responsible != Dev-Owner', -- LEDE
  'NO_MILESTONE',           -- SUNSET_MILESTONE
  '0'                       -- SUNSET_HORIZON
);

-- backlog_prs_current_2010_responsible_not_dev
-- ORIGINALLY: diff_current_backlog_prs_responsible
--------------------------------------------------------------------------------
INSERT INTO "DASH"."RULES" (IDENTIFIER, NAME, OBJECTIVE, DESCRIPTION, OWNER, MILESTONE, HORIZON, COUNTS, QUERY_FIELDS, LHS, WEIGHT, ACTIVE, LAST_UPDATED, LEDE, SUNSET_MILESTONE, SUNSET_HORIZON)
VALUES (
  'backlog_prs_current_2010_responsible_not_dev',
  '2010 Current Backlog PRs by Responsible!=Dev-Owner',
  'HOLD_TO_SCHEDULE',
  'All Current Backlog PR by using responsible as their primary accountability where the responsible != dev-owner',
  'admin',
  'NO_MILESTONE',
  '0',
  'PRS',
  'synopsis, state, severity, category, priority, product, planned-release, blocker, submitter-id, responsible, dev-owner, originator, systest-owner, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli, created, resolution, feedback-date, jtac-case-id,',
'$user : User( )
$record_list : RecordSet( ) from
 collect( 
    PRRecord( 
	    (severity == "critical" || severity == "major"),
	    (clazz == "bug" || clazz == "unreproducible"),functional_area=="software",
	    alwaysTrue == possible_backlog, real_responsible != dev_owner,
	    npi == $user.npi_program || alwaysTrue == $user.junos ||
	    backlog_pr_responsible memberOf $user.reportNames ) )
', 
  '1',
  '1',
  to_timestamp_tz('01-NOV-10 12.00.00.511060000 PM -07:00', 'DD-MON-RR HH.MI.SS.FF AM TZR'),
  'Current Backlog PRs by Responsible by Responsible!=Dev-Owner', -- LEDE
  'NO_MILESTONE',           -- SUNSET_MILESTONE
  '0'                       -- SUNSET_HORIZON
);

-- backlog_prs_forward_2010_responsible_not_dev
-- ORIGINALLY: diff_forward_view_backlog_prs_responsible
--------------------------------------------------------------------------------
INSERT INTO "DASH"."RULES" (IDENTIFIER, NAME, OBJECTIVE, DESCRIPTION, OWNER, MILESTONE, HORIZON, COUNTS, QUERY_FIELDS, LHS, WEIGHT, ACTIVE, LAST_UPDATED, LEDE, SUNSET_MILESTONE, SUNSET_HORIZON)
VALUES (
  'backlog_prs_forward_2010_responsible_not_dev',
  '2010 Forward View / Future Backlog PRs by Responsible != Dev-Owner',
  'HOLD_TO_SCHEDULE',
  'All Future Backlog PR by using responsible as their primary accountability where the responsible != dev-owner',
  'admin',
  'NO_MILESTONE',
  '0',
  'PRS',
  'synopsis, state, severity, category, priority, product, planned-release, blocker, submitter-id, responsible, dev-owner, originator, systest-owner, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli, created, resolution, feedback-date, jtac-case-id,',
'$user : User( )
$record_list : RecordSet( ) from
 collect( 
    PRRecord( 
	    (severity == "critical" || severity == "major"),
	    (clazz == "bug" || clazz == "unreproducible" ),functional_area=="software",
	    (alwaysTrue == possible_forward_view_backlog || alwaysTrue == possible_backlog),
	    real_responsible != dev_owner,
	    npi == $user.npi_program || alwaysTrue == $user.junos ||
	    backlog_pr_responsible memberOf $user.reportNames ))
', 
  '1',
  '1',
  to_timestamp_tz('01-NOV-10 12.00.00.511060000 PM -07:00', 'DD-MON-RR HH.MI.SS.FF AM TZR'),
  'Forward View / Future Backlog PRs by Responsible', -- LEDE
  'NO_MILESTONE',           -- SUNSET_MILESTONE
  '0'                       -- SUNSET_HORIZON
);


/* ---------------------------------------------------------------------
 * new rules
 * ---------------------------------------------------------------------
 */

-- new rule
INSERT INTO "DASH"."RULES" (IDENTIFIER, NAME, OBJECTIVE, DESCRIPTION, OWNER, MILESTONE, HORIZON, COUNTS, QUERY_FIELDS, LHS, WEIGHT, ACTIVE, LAST_UPDATED, LEDE, SUNSET_MILESTONE, SUNSET_HORIZON, RHS)
VALUES (
  'backlog_prs_reference_2010-12-31',
  'Reference Backlog PRs 2010-12-31',
  'HOLD_TO_SCHEDULE',
  'All Reference Backlog PR',
  'admin',
  'NO_MILESTONE',
  '0',
  'PRS',
  'synopsis, problem-level, state, severity, category, priority, product, planned-release, blocker, submitter-id, responsible, dev-owner, originator, systest-owner, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli, created, resolution, feedback-date, jtac-case-id,',
'$user : User( )
$record_list : RecordSet( )
  from collect(
    OldPRRecord(
      extract_date == "2010-12-31",
      state != "closed",
      (problem_level == "1-CL1" || problem_level == "2-CL2" || problem_level == "4-CL3" || problem_level == "3-L1" || problem_level == "4-IL2" || problem_level == "5-CL4" || problem_level == "5-IL3"),
      (clazz == "bug" || clazz == "unreproducible" || clazz == "duplicate" ),
      functional_area=="software",
      alwaysTrue == possible_backlog,
      npi == $user.npi_program || alwaysTrue == $user.junos || backlog_pr_dev_owner memberOf $user.reportNames
    )
  )',
  '1',
  '1',
  to_timestamp_tz('01-NOV-10 12.00.00.511060000 PM -07:00', 'DD-MON-RR HH.MI.SS.FF AM TZR'),
  'Reference Backlog PRs ', -- LEDE
  'NO_MILESTONE',           -- SUNSET_MILESTONE
  '0',                      -- SUNSET_HORIZON
  '$user.setScore("backlog_prs_reference_2010-12-31","pr_backlog",$record_list);'
);
--------------------------------------------------------------------------------

-- backlog_prs_reference
-- BASED ON: reference_backlog_prs
--------------------------------------------------------------------------------
INSERT INTO "DASH"."RULES" (IDENTIFIER, NAME, OBJECTIVE, DESCRIPTION, OWNER, MILESTONE, HORIZON, COUNTS, QUERY_FIELDS, LHS, WEIGHT, ACTIVE, LAST_UPDATED, LEDE, SUNSET_MILESTONE, SUNSET_HORIZON, RHS)
VALUES (
  'backlog_prs_reference',
  'Reference Backlog PRs 2010-01-31',
  'HOLD_TO_SCHEDULE',
  'All Reference Backlog PR',
  'admin',
  'NO_MILESTONE',
  '0',
  'PRS',
  'synopsis, problem-level, state, severity, category, priority, product, planned-release, blocker, submitter-id, responsible, dev-owner, originator, systest-owner, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli, created, resolution, feedback-date, jtac-case-id,',
'$user : User( )
$record_list : RecordSet( )
  from collect(
    OldPRRecord(
      extract_date == "2010-01-31",
      state != "closed",
      (problem_level == "1-CL1" || problem_level == "2-CL2" || problem_level == "4-CL3" || problem_level == "3-L1" || problem_level == "4-IL2" || problem_level == "5-CL4" || problem_level == "5-IL3"),
      (clazz == "bug" || clazz == "unreproducible" || clazz == "duplicate" ),
      functional_area=="software",
      alwaysTrue == possible_backlog,
      npi == $user.npi_program || alwaysTrue == $user.junos || backlog_pr_dev_owner memberOf $user.reportNames
    )
  )',
  '1',
  '1',
  to_timestamp_tz('01-NOV-10 12.00.00.511060000 PM -07:00', 'DD-MON-RR HH.MI.SS.FF AM TZR'),
  'Reference Backlog PRs ', -- LEDE
  'NO_MILESTONE',           -- SUNSET_MILESTONE
  '0',                      -- SUNSET_HORIZON
  '$user.setScore("backlog_prs_reference","pr_backlog",$record_list);'
);
--------------------------------------------------------------------------------


-- backlog_prs_current
-- BASED ON: current_backlog_prs
--------------------------------------------------------------------------------
INSERT INTO "DASH"."RULES" (IDENTIFIER, NAME, OBJECTIVE, DESCRIPTION, OWNER, MILESTONE, HORIZON, COUNTS, QUERY_FIELDS, LHS, WEIGHT, ACTIVE, LAST_UPDATED, LEDE, SUNSET_MILESTONE, SUNSET_HORIZON, RHS)
VALUES (
  'backlog_prs_current',
  'Current Backlog PRs',
  'HOLD_TO_SCHEDULE',
  'All current backlog PR -- up to date',
  'admin',
  'NO_MILESTONE',
  '0',
  'PRS',
  'synopsis, problem-level, state, severity, category, priority, product, planned-release, blocker, submitter-id, responsible, dev-owner, originator, systest-owner, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli, created, resolution, feedback-date, jtac-case-id,',
'$user : User( )
$record_list : RecordSet( )
  from collect(
    PRRecord(
    (problem_level == "1-CL1" || problem_level == "2-CL2" || problem_level == "4-CL3" || problem_level == "3-L1" || problem_level == "4-IL2" || problem_level == "5-CL4" || problem_level == "5-IL3"), 
    (clazz == "bug" || clazz == "unreproducible" || clazz == "duplicate" ),
    functional_area=="software",
    alwaysTrue == possible_backlog,
    npi == $user.npi_program || alwaysTrue == $user.junos || backlog_pr_dev_owner memberOf $user.reportNames
    )
  )',
  '1',
  '1',
  to_timestamp_tz('01-NOV-10 12.00.00.511060000 PM -07:00', 'DD-MON-RR HH.MI.SS.FF AM TZR'),
  'Reference Backlog PRs ', -- LEDE
  'NO_MILESTONE',           -- SUNSET_MILESTONE
  '0',                      -- SUNSET_HORIZON
  '$user.setScore("backlog_prs_current","pr_backlog",$record_list);'  
);


-- backlog_prs_forward
-- BASED ON: forward_view_backlog_prs
--------------------------------------------------------------------------------
INSERT INTO "DASH"."RULES" (IDENTIFIER, NAME, OBJECTIVE, DESCRIPTION, OWNER, MILESTONE, HORIZON, COUNTS, QUERY_FIELDS, LHS, WEIGHT, ACTIVE, LAST_UPDATED, LEDE, SUNSET_MILESTONE, SUNSET_HORIZON, RHS)
VALUES (
  'backlog_prs_forward',
  'Forward View Backlog PR',
  'HOLD_TO_SCHEDULE',
  'PRs which will be counted as current backlog in next R1 deploy',
  'admin',
  'NO_MILESTONE',
  '0',
  'PRS',
  'synopsis, problem-level, state, severity, category, priority, product, planned-release, blocker, submitter-id, responsible, dev-owner, originator, systest-owner, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli, created, resolution, feedback-date, jtac-case-id,',
'$user : User( )
$record_list : RecordSet( )
  from collect(
    PRRecord(
      (problem_level == "1-CL1" || problem_level == "2-CL2" || problem_level == "4-CL3" || problem_level == "3-L1" || problem_level == "4-IL2" || problem_level == "5-CL4" || problem_level == "5-IL3"), 
      (clazz == "bug" || clazz == "unreproducible" || clazz == "duplicate" ),functional_area=="software",
      (alwaysTrue == possible_forward_view_backlog || alwaysTrue == possible_backlog),
      npi == $user.npi_program || alwaysTrue == $user.junos ||
      backlog_pr_dev_owner memberOf $user.reportNames
    )
  )', 
  '1',
  '1',
  to_timestamp_tz('01-NOV-10 12.00.00.511060000 PM -07:00', 'DD-MON-RR HH.MI.SS.FF AM TZR'),
  'Reference Backlog PRs ', -- LEDE
  'NO_MILESTONE',           -- SUNSET_MILESTONE
  '0',                      -- SUNSET_HORIZON
  '$user.setScore("backlog_prs_forward","pr_backlog",$record_list);'
);


-- backlog_prs_reference_responsible
-- BASED ON: reference_backlog_prs_responsible
--------------------------------------------------------------------------------
INSERT INTO "DASH"."RULES" (IDENTIFIER, NAME, OBJECTIVE, DESCRIPTION, OWNER, MILESTONE, HORIZON, COUNTS, QUERY_FIELDS, LHS, WEIGHT, ACTIVE, LAST_UPDATED, LEDE, SUNSET_MILESTONE, SUNSET_HORIZON, RHS)
VALUES (
  'backlog_prs_reference_responsible',
  'Reference Backlog PRs by Responsible 2010-01-31',
  'HOLD_TO_SCHEDULE',
  'All Reference Backlog PR by using responsible as their primary accountability',
  'admin',
  'NO_MILESTONE',
  '0',
  'PRS',
  'synopsis, problem-level, state, severity, category, priority, product, planned-release, blocker, submitter-id, responsible, dev-owner, originator, systest-owner, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli, created, resolution, feedback-date, jtac-case-id,',
'$user : User( )
$record_list : RecordSet( )
  from collect(
    OldPRRecord(
      extract_date == "2010-01-31",
      state != "closed",
      (problem_level == "1-CL1" || problem_level == "2-CL2" || problem_level == "4-CL3" || problem_level == "3-L1" || problem_level == "4-IL2" || problem_level == "5-CL4" || problem_level == "5-IL3"), 
      (clazz == "bug" || clazz == "unreproducible"  || clazz == "duplicate" ),functional_area=="software",
      alwaysTrue == possible_backlog,
      npi == $user.npi_program || alwaysTrue == $user.junos || backlog_pr_responsible memberOf $user.reportNames
    )
  )',
  '1',
  '1',
  to_timestamp_tz('01-NOV-10 12.00.00.511060000 PM -07:00', 'DD-MON-RR HH.MI.SS.FF AM TZR'),
  'Reference Backlog PRs ', -- LEDE
  'NO_MILESTONE',           -- SUNSET_MILESTONE
  '0',                       -- SUNSET_HORIZON
  '$user.setScore("backlog_prs_reference_responsible","pr_backlog",$record_list);'
);

-- backlog_prs_current_responsible
-- BASED ON: current_backlog_prs_responsible
--------------------------------------------------------------------------------
INSERT INTO "DASH"."RULES" (IDENTIFIER, NAME, OBJECTIVE, DESCRIPTION, OWNER, MILESTONE, HORIZON, COUNTS, QUERY_FIELDS, LHS, WEIGHT, ACTIVE, LAST_UPDATED, LEDE, SUNSET_MILESTONE, SUNSET_HORIZON, RHS)
VALUES (
  'backlog_prs_current_responsible',
  'Current Backlog PRs by Responsible',
  'HOLD_TO_SCHEDULE',
  'All Current Backlog PR by using responsible as their primary accountability',
  'admin',
  'NO_MILESTONE',
  '0',
  'PRS',
  'synopsis, problem-level, state, severity, category, priority, product, planned-release, blocker, submitter-id, responsible, dev-owner, originator, systest-owner, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli, created, resolution, feedback-date, jtac-case-id,',
'$user : User( )
$record_list : RecordSet( )
  from collect(
    PRRecord(
      (problem_level == "1-CL1" || problem_level == "2-CL2" || problem_level == "4-CL3" || problem_level == "3-L1" || problem_level == "4-IL2" || problem_level == "5-CL4" || problem_level == "5-IL3"), 
      (clazz == "bug" || clazz == "unreproducible" || clazz == "duplicate" ),functional_area=="software",
      alwaysTrue == possible_backlog,
      npi == $user.npi_program || alwaysTrue == $user.junos ||
      backlog_pr_responsible memberOf $user.reportNames
    )
  )', 
  '1',
  '1',
  to_timestamp_tz('01-NOV-10 12.00.00.511060000 PM -07:00', 'DD-MON-RR HH.MI.SS.FF AM TZR'),
  'Reference Backlog PRs ', -- LEDE
  'NO_MILESTONE',           -- SUNSET_MILESTONE
  '0',                      -- SUNSET_HORIZON
  '$user.setScore("backlog_prs_current_responsible","pr_backlog",$record_list);'
);

-- backlog_prs_forward_responsible
-- BASSED ON: forward_view_backlog_prs_responsible
--------------------------------------------------------------------------------
INSERT INTO "DASH"."RULES" (IDENTIFIER, NAME, OBJECTIVE, DESCRIPTION, OWNER, MILESTONE, HORIZON, COUNTS, QUERY_FIELDS, LHS, WEIGHT, ACTIVE, LAST_UPDATED, LEDE, SUNSET_MILESTONE, SUNSET_HORIZON, RHS)
VALUES (
  'backlog_prs_forward_responsible',
  'Forward View / Future Backlog PRs by Responsible',
  'HOLD_TO_SCHEDULE',
  'All Future Backlog PR by using responsible as their primary accountability',
  'admin',
  'NO_MILESTONE',
  '0',
  'PRS',
  'synopsis, problem-level, state, severity, category, priority, product, planned-release, blocker, submitter-id, responsible, dev-owner, originator, systest-owner, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli, created, resolution, feedback-date, jtac-case-id,',
'$user : User( )
$record_list : RecordSet( )
  from collect(
    PRRecord(
      (problem_level == "1-CL1" || problem_level == "2-CL2" || problem_level == "4-CL3" || problem_level == "3-L1" || problem_level == "4-IL2" || problem_level == "5-CL4" || problem_level == "5-IL3"), 
      (clazz == "bug" || clazz == "unreproducible"  || clazz == "duplicate" ),functional_area=="software",
      (alwaysTrue == possible_forward_view_backlog || alwaysTrue == possible_backlog),
      npi == $user.npi_program || alwaysTrue == $user.junos ||
      backlog_pr_responsible memberOf $user.reportNames
    )
  )', 
  '1',
  '1',
  to_timestamp_tz('01-NOV-10 12.00.00.511060000 PM -07:00', 'DD-MON-RR HH.MI.SS.FF AM TZR'),
  'Reference Backlog PRs ', -- LEDE
  'NO_MILESTONE',           -- SUNSET_MILESTONE
  '0',                      -- SUNSET_HORIZON
  '$user.setScore("backlog_prs_forward_responsible","pr_backlog",$record_list);'
);




-- backlog_prs_reference_responsible_not_dev
-- BASED ON: diff_reference_backlog_prs_responsible
--------------------------------------------------------------------------------
INSERT INTO "DASH"."RULES" (IDENTIFIER, NAME, OBJECTIVE, DESCRIPTION, OWNER, MILESTONE, HORIZON, COUNTS, QUERY_FIELDS, LHS, WEIGHT, ACTIVE, LAST_UPDATED, LEDE, SUNSET_MILESTONE, SUNSET_HORIZON, RHS)
VALUES (
  'backlog_prs_reference_responsible_not_dev',
  'RReference Backlog PRs by Responsible != Dev-Owner 2010-01-31',
  'HOLD_TO_SCHEDULE',
  'All Reference Backlog PR by using responsible as their primary accountability where the responsible != dev-owner',
  'admin',
  'NO_MILESTONE',
  '0',
  'PRS',
  'synopsis, problem-level, state, severity, category, priority, product, planned-release, blocker, submitter-id, responsible, dev-owner, originator, systest-owner, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli, created, resolution, feedback-date, jtac-case-id,',
'$user : User( )
$record_list : RecordSet( ) from
 collect( 
    OldPRRecord(
        extract_date == "2010-01-31", 
        state != "closed",
        (problem_level == "1-CL1" || problem_level == "2-CL2" || problem_level == "4-CL3" || problem_level == "3-L1" || problem_level == "4-IL2" || problem_level == "5-CL4" || problem_level == "5-IL3"),
        (clazz == "bug" || clazz == "unreproducible"  || clazz == "duplicate" ),functional_area=="software",
        alwaysTrue == possible_backlog,
        real_responsible != dev_owner,
        npi == $user.npi_program || alwaysTrue == $user.junos ||
        backlog_pr_responsible memberOf $user.reportNames ))
',
  '1',
  '1',
  to_timestamp_tz('01-NOV-10 12.00.00.511060000 PM -07:00', 'DD-MON-RR HH.MI.SS.FF AM TZR'),
  'Reference Backlog PRs by Responsible != Dev-Owner', -- LEDE
  'NO_MILESTONE',           -- SUNSET_MILESTONE
  '0',                      -- SUNSET_HORIZON
  '$user.setScore("backlog_prs_reference_responsible_not_dev","pr_backlog",$record_list);'  
);

-- backlog_prs_current_responsible_not_dev
-- BASED ON: diff_current_backlog_prs_responsible
--------------------------------------------------------------------------------
INSERT INTO "DASH"."RULES" (IDENTIFIER, NAME, OBJECTIVE, DESCRIPTION, OWNER, MILESTONE, HORIZON, COUNTS, QUERY_FIELDS, LHS, WEIGHT, ACTIVE, LAST_UPDATED, LEDE, SUNSET_MILESTONE, SUNSET_HORIZON, RHS)
VALUES (
  'backlog_prs_current_responsible_not_dev',
  'Current Backlog PRs by Responsible!=Dev-Owner',
  'HOLD_TO_SCHEDULE',
  'All Current Backlog PR by using responsible as their primary accountability where the responsible != dev-owner',
  'admin',
  'NO_MILESTONE',
  '0',
  'PRS',
  'synopsis, problem-level, state, severity, category, priority, product, planned-release, blocker, submitter-id, responsible, dev-owner, originator, systest-owner, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli, created, resolution, feedback-date, jtac-case-id,',
'$user : User( )
$record_list : RecordSet( ) from
 collect( 
    PRRecord( 
        (problem_level == "1-CL1" || problem_level == "2-CL2" || problem_level == "4-CL3" || problem_level == "3-L1" || problem_level == "4-IL2" || problem_level == "5-CL4" || problem_level == "5-IL3"),
        (clazz == "bug" || clazz == "unreproducible"  || clazz == "duplicate" ),functional_area=="software",
        alwaysTrue == possible_backlog, real_responsible != dev_owner,
        npi == $user.npi_program || alwaysTrue == $user.junos ||
        backlog_pr_responsible memberOf $user.reportNames ) )
', 
  '1',
  '1',
  to_timestamp_tz('01-NOV-10 12.00.00.511060000 PM -07:00', 'DD-MON-RR HH.MI.SS.FF AM TZR'),
  'Current Backlog PRs by Responsible by Responsible!=Dev-Owner', -- LEDE
  'NO_MILESTONE',           -- SUNSET_MILESTONE
  '0',                      -- SUNSET_HORIZON
  '$user.setScore("backlog_prs_current_responsible_not_dev","pr_backlog",$record_list);'
);

-- backlog_prs_forward_responsible_not_dev
-- BASED ON: diff_forward_view_backlog_prs_responsible
--------------------------------------------------------------------------------
INSERT INTO "DASH"."RULES" (IDENTIFIER, NAME, OBJECTIVE, DESCRIPTION, OWNER, MILESTONE, HORIZON, COUNTS, QUERY_FIELDS, LHS, WEIGHT, ACTIVE, LAST_UPDATED, LEDE, SUNSET_MILESTONE, SUNSET_HORIZON, RHS)
VALUES (
  'backlog_prs_forward_responsible_not_dev',
  'Forward View / Future Backlog PRs by Responsible != Dev-Owner',
  'HOLD_TO_SCHEDULE',
  'All Future Backlog PR by using responsible as their primary accountability where the responsible != dev-owner',
  'admin',
  'NO_MILESTONE',
  '0',
  'PRS',
  'synopsis, problem-level, state, severity, category, priority, product, planned-release, blocker, submitter-id, responsible, dev-owner, originator, systest-owner, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli, created, resolution, feedback-date, jtac-case-id,',
'$user : User( )
$record_list : RecordSet( ) from
 collect( 
    PRRecord( 
        (problem_level == "1-CL1" || problem_level == "2-CL2" || problem_level == "4-CL3" || problem_level == "3-L1" || problem_level == "4-IL2" || problem_level == "5-CL4" || problem_level == "5-IL3"),
        (clazz == "bug" || clazz == "unreproducible"  || clazz == "duplicate" ),functional_area=="software",
        (alwaysTrue == possible_forward_view_backlog || alwaysTrue == possible_backlog),
        real_responsible != dev_owner,
        npi == $user.npi_program || alwaysTrue == $user.junos ||
        backlog_pr_responsible memberOf $user.reportNames ))
', 
  '1',
  '1',
  to_timestamp_tz('01-NOV-10 12.00.00.511060000 PM -07:00', 'DD-MON-RR HH.MI.SS.FF AM TZR'),
  'Forward View / Future Backlog PRs by Responsible', -- LEDE
  'NO_MILESTONE',           -- SUNSET_MILESTONE
  '0',                       -- SUNSET_HORIZON
  '$user.setScore("backlog_prs_forward_responsible_not_dev","pr_backlog",$record_list);'
);

