
-- clear out existing rule
DELETE FROM "DASH"."RULES" WHERE IDENTIFIER IN ('prs_user_private_branch');
SET DEFINE ~;

INSERT INTO "DASH"."RULES" (IDENTIFIER, NAME, OBJECTIVE, DESCRIPTION, OWNER, MILESTONE, HORIZON, COUNTS, QUERY_FIELDS, LHS, WEIGHT, ACTIVE, GROUPING_VARIABLE, LAST_UPDATED, LEDE, SUNSET_MILESTONE, SUNSET_HORIZON)
VALUES (
  'prs_user_private_branch',
  'Private Branch PRs',
  'HOLD_TO_SCHEDULE',
  'PRs for Private Branch PRs',
  'admin',
  'NO_MILESTONE',
  '0',
  'PRS',
  'synopsis, state, problem-level, category, product, planned-release, branch, blocker, submitter-id, responsible, dev-owner, originator, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli,',
  '$user : User( ) 
$record_list : RecordSet( ) from 
 collect( PRRecord( (branchReleaseType=="PVT"), planned_release not matches "^1[1-9]\.[0-9]W[0-9]*",
  npi == $user.npi_program || 
  alwaysTrue == $user.junos ||  
  responsible memberOf $user.reportNames || 
  (dev_owner memberOf $user.reportNames &&  state != "feedback")))',
  '1',
  '1',
  'release',
  to_timestamp_tz('08-JUN-10 03.40.47.171349000 PM -07:00', 'DD-MON-RR HH.MI.SS.FF AM TZR'),
  'PRs for Private Branch PRs', -- LEDE
  'NO_MILESTONE',           -- SUNSET_MILESTONE
  '0'                       -- SUNSET_HORIZON
) ;
