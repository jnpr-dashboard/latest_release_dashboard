DELETE FROM "DASH"."RULES" WHERE IDENTIFIER IN ('customer_crit_hi_prs');
SET DEFINE ~;

INSERT INTO "DASH"."RULES" (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) 
VALUES ('customer_crit_hi_prs','Customer Critical/High PRs','HOLD_TO_SCHEDULE','Owners of customer critical or high priority need to try and resolve these issues as soon as possible.  Customers have raised the priority on these PRs through JTAC.  If you do not have the time to work on the escalated PRs assigned to you, notify your manager.','admin','NO_MILESTONE',0,'PRS','synopsis, state, class, problem-level, category, product, planned-release, blocker, submitter-id, responsible, dev-owner, originator, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli,',
'$user : User(eval($user.isValidUser("pr:responsibles:dev_owners")))
    $record_list : RecordSet( ) from 
     collect( PRRecord(submitter_id in ("customer", "field"),
      problem_level == "1-CL1" || problem_level == "2-CL2" || problem_level == "3-IL1" || problem_level == "4-IL2",
      (alwaysTrue != $user.junos || planned_release not matches "^[1-9][0-9]?\.[0-9][wWxX].*"),
      state in ("open", "info", "analyzed"), npi == $user.npi_program
      || alwaysTrue == $user.junos
      || responsible memberOf $user.reportNames
      || ( dev_owner memberOf $user.reportNames && state != "feedback")))',
6,1,to_timestamp_tz('17-FEB-12 03.00.00.000000000 PM -07:00','DD-MON-RR HH.MI.SS.FF AM TZR'),null,null,null,null,'Customers have raised the priority on these PRs through JTAC.','NO_MILESTONE',null);
