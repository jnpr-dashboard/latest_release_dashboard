DELETE FROM "DASH"."RULES" WHERE IDENTIFIER IN ('rlis_with_unknown_platform');
SET DEFINE ~;

INSERT INTO "DASH"."RULES" (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) 
VALUES ('rlis_with_unknown_platform','RLIs with unknown platform','HOLD_TO_SCHEDULE','Owners of RLIs with a [Platform] field that contains ''unknown'' must 
enter the individual platform names or the platform series affected by 
their RLI. Without platform information, an RLI cannot be scoped 
accurately.
','lbirch','NO_MILESTONE',0,'RLIS','synopsis, state, rli_class, project_status, platform, responsible, sw_mgr_responsible, sw_responsible, plm_responsible, ','$release : ReleaseRecord( hasOpenRLIs == true )  
$user : User(eval($user.isValidUser("rli:responsibles:sw_mgr_responsibles:st_mgr_responsibles:sw_responsibles:st_responsibles")))
$record_list : RecordSet( ) from  
 collect( RLIRecord( platform contains "unknown",  
  state == "committed",  
  (relname == $release.relname || relname memberOf $release.childBranch),
  npi_program == $user.npi_program_name ||  
  alwaysTrue == $user.junos || responsible memberOf $user.reportNames ||  
    sw_mgr_responsible memberOf $user.reportNames ||  
    st_mgr_responsible memberOf $user.reportNames ||  
    sw_responsible == $user.id || st_responsible == $user.id ) )',1,1,to_timestamp_tz('15-JUN-12 02.42.12.253726000 PM -07:00','DD-MON-RR HH.MI.SS.FF AM TZR'),'release',null,null,null,'RLIs must have [Platform] filled out for accurate scoping.','NO_MILESTONE',0);
