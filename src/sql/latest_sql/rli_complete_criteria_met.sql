DELETE FROM "DASH"."RULES" WHERE IDENTIFIER IN ('rli_complete_criteria_met');
SET DEFINE ~;

INSERT INTO "DASH"."RULES" (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) 
VALUES ('rli_complete_criteria_met','RLIs with complete criteria is met','HOLD_TO_SCHEDULE','RLIs with complete criteria is met','admin','NO_MILESTONE',0,'RLIS','synopsis, release_target, cam_status, state, responsible, sw_mgr_responsible, sw_responsible, st_responsible, st_mgr_responsible, plm_responsible, tp_mgr_responsible, tp_lead_responsible, tp_responsible, tests_executed_percentage, tests_passed_percentage, tests_script_percentage, product_test_plan_status, ','$release : ReleaseRecord( hasOpenRLIs == true )  
$user : User(eval($user.isValidUser("rli:responsibles:sw_mgr_responsibles:st_mgr_responsibles:sw_responsibles:st_responsibles")))
$record_list : RecordSet( ) from  
 collect( RLIRecord( ( relname == $release.relname || relname memberOf $release.childBranch),  
  cam_status == "accepted",  
  npi_program == $user.npi_program_name ||  
  alwaysTrue == $user.junos || responsible memberOf $user.reportNames ||  
  sw_mgr_responsible memberOf $user.reportNames ||  
  st_mgr_responsible memberOf $user.reportNames ||  
    sw_responsible == $user.id || st_responsible == $user.id ) )',0,1,to_timestamp_tz('01-MAR-10 10.12.32.717180000 PM -08:00','DD-MON-RR HH.MI.SS.FF AM TZR'),'release',null,null,null,'RLIs with complete criteria is met','NO_MILESTONE',0);
