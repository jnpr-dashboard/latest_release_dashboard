DELETE FROM "DASH"."RULES" WHERE IDENTIFIER IN ('unassigned_reviews');
SET DEFINE ~;

INSERT INTO "DASH"."RULES" (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) 
VALUES ('unassigned_reviews','Unassigned reviews','HOLD_TO_SCHEDULE','Review requests must be assigned to a reviewer within two days of creation.  After two months, the review is assumed to be abandoned.','skumar','NO_MILESTONE',0,'REVIEWS','synopsis, arrival-date, last-modified, submitter, ',
'$user : User(eval($user.isValidUser("review:moderators")))
$record_list : RecordSet( ) from  
 collect( ReviewRecord( hasResponsibleModerators == true,  
  age > 1 && age < 60,  
  alwaysTrue == $user.junos || moderators contains $user ) )',2,1,to_timestamp_tz('05-APR-08 08.45.44.853463000 AM -07:00','DD-MON-RR HH.MI.SS.FF AM TZR'),null,null,null,null,'Review requests must be assigned to a reviewer within two day...','NO_MILESTONE',null);
