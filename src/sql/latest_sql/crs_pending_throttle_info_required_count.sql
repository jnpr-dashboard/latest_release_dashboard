DELETE FROM "DASH"."RULES" WHERE IDENTIFIER IN ('crs_pending_throttle_info_required_count');
SET DEFINE ~;

INSERT INTO "DASH"."RULES" (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) 
VALUES ('crs_pending_throttle_info_required_count','Pending CRS Requests : Throttle/Information Required','ENSURE_COMMIT_REQUEST','Commit requests should be reviewed and completed within a timely manner.','admin','NO_MILESTONE',0,'CRSS','throttle_request_id',
'$release : ReleaseRecord(relname matches "[1-9][0-9]?.?[0-9]?$")
$user : User(eval($user.isValidUser("crs:users")))
$record_list : RecordSet() from 
               collect(
                       CRSRecord(
                                 release_names contains $release.standard_release_name, 
                                 cr_life < 7 || cr_life == 7, 
                                 throttle_request_type == "throttle", 
                                 throttle_request_state == "Information Required",
                                 alwaysTrue == $user.junos || eval($user.isOwned(user_username))
                                )
                      )',
0,1,to_timestamp_tz('17-MAR-13 07.00.00.000000000 PM -07:00','DD-MON-RR HH.MI.SS.FF AM TZR'),'release',null,null,null,'Open/Info/Pending CRS Requests','NO_MILESTONE',0);
