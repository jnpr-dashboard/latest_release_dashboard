DELETE FROM "DASH"."RULES" WHERE IDENTIFIER IN ('rlis_wo_feature_desc');
SET DEFINE ~;

INSERT INTO "DASH"."RULES" (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) 
VALUES ('rlis_wo_feature_desc','RLIs without a Feature Description','HOLD_TO_SCHEDULE','The Feature Description is, like, <b>really</b> important, you know?','mwazna','B1_DEPLOY',10,'RLIS','synopsis, tp_feature_description, functional_specification, tp_responsible, tp_release_status, ','$release : ReleaseRecord( b1_deploy_string != "" )  
$user : User(eval($user.isValidUser("rli:tp_responsibles:tp_lead_responsibles:tp_mgr_responsibles")))
$record_list : RecordSet( ) from  
 collect( RLIRecord( tp_feature_description == "",  
  state != "deferred",  
  relname == $release.relname,  
  tp_release_status != "no_doc",  
  alwaysTrue != $user.junos &&   
  (tp_responsible memberOf $user.reportNames ||  
  tp_lead_responsible memberOf $user.reportNames ||  
  tp_mgr_responsible memberOf $user.reportNames) ) )',4,1,to_timestamp_tz('12-MAY-09 08.42.43.041782000 PM -07:00','DD-MON-RR HH.MI.SS.FF AM TZR'),'release',null,null,null,'Like, totally write a description, dude.','NO_MILESTONE',0);
