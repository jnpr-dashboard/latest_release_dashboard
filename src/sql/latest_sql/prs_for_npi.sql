DELETE FROM "DASH"."RULES" WHERE IDENTIFIER IN ('prs_for_npi');
SET DEFINE ~;

INSERT INTO "DASH"."RULES" (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) 
VALUES ('prs_for_npi','Find PRs for NPI Program','NO_OBJECTIVE','bookkeeping','admin','NO_MILESTONE',0,'NOTHING',null,'$npi : NPIRecord( )  
$pr : PRRecord( npi == "", rli != "" )  
$rli : RLIRecord( npi_program == $npi.synopsis, id memberOf $pr.rli_nums )',1,1,to_timestamp_tz('24-JUN-09 02.05.35.513332000 PM -07:00','DD-MON-RR HH.MI.SS.FF AM TZR'),null,'$pr.setNpi($npi.getId());  
 update($pr);','lock-on-active true  
 no-loop true  
 salience 10','020 Complex Related','bookkeeping','NO_MILESTONE',0);