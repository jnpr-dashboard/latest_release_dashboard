DELETE FROM "DASH"."RULES" WHERE IDENTIFIER IN ('cidb_prs');
SET DEFINE ~;

INSERT INTO "DASH"."RULES" (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) 
VALUES ('cidb_prs','Legacy CIDB PRs','P5_STABILITY','Change in Default Behavior PRs were formerly used to notify Technical Publications of any changes introduced by a PR that need to be documented for the customer.  Any remaining CIDB PRs are probably long overdue, and should be closed out promptly.  When working with these PRs, please remove the auto-gen keyword.','june','NO_MILESTONE',0,'PRS','synopsis, state, class, problem-level, category, product, planned-release, blocker, submitter-id, responsible, dev-owner, originator, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli,',
'$user : User( junos == false && eval($user.isValidNonJunosUser("pr:techpubs_owners")))
$record_list : RecordSet( ) from  
 collect( PRRecord( cust_visible_behavior_changed == "",  
  keywords matches ".*auto-gen:default-changed.*",  
  techpubs_owner memberOf $user.reportNames ) )',3,1,to_timestamp_tz('06-JUL-09 01.54.07.815742000 PM -07:00','DD-MON-RR HH.MI.SS.FF AM TZR'),null,null,null,null,'CIDB PRs need to be documented for the customer','NO_MILESTONE',0);
