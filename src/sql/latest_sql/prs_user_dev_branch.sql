DELETE FROM "DASH"."RULES" WHERE IDENTIFIER IN ('prs_user_dev_branch');
SET DEFINE ~;

INSERT INTO "DASH"."RULES" (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) 
VALUES ('prs_user_dev_branch','Development Branch PRs','HOLD_TO_SCHEDULE','Development Branch PRs assign to user as their responsible or Dev-owner','admin','NO_MILESTONE',0,'PRS','synopsis, state, class, problem-level, category, product, planned-release, branch, blocker, submitter-id, responsible, dev-owner, originator, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli,',
'$user : User(eval($user.isValidUser("pr:responsibles:dev_owners")))
$release : ReleaseRecord( )
$record_list : RecordSet( ) from
 collect( PRRecord( (branchRelease == $release.relname &&  branchReleaseType=="DEV"),
         (alwaysTrue != $user.junos || planned_release not matches "DEV_X.*"),
         npi == $user.npi_program
         || alwaysTrue == $user.junos
         || responsible memberOf $user.reportNames
         || (dev_owner memberOf $user.reportNames &&  state != "feedback")))',
1,1,to_timestamp_tz('17-FEB-12 03.00.00.000000000 PM -07:00','DD-MON-RR HH.MI.SS.FF AM TZR'),'release',null,null,null,'Development Branch PRs assign to user as their responsible or Dev-owner','NO_MILESTONE',0);
