DELETE FROM "DASH"."RULES" WHERE IDENTIFIER IN ('abandoned_reviews');
SET DEFINE ~;

INSERT INTO "DASH"."RULES" (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) 
VALUES ('abandoned_reviews','Abandoned reviews','HOLD_TO_SCHEDULE','Review requests should be completed or closed in a reasonable time.  Reviews  
over two months old that have not been modified in the last month appear to  
be abandoned.  Abandoned reviews more than four months old are ignored.','skumar','NO_MILESTONE',0,'REVIEWS','synopsis, arrival-date, last-modified, submitter, ',
'$user : User(eval($user.isValidUser("review:moderators")))
$record_list : RecordSet( ) from  
 collect( ReviewRecord( daysSinceLastModified > 30,  
  age > 60 &&  age < 120,  
  alwaysTrue == $user.junos || moderators contains $user ) )',2,1,to_timestamp_tz('05-APR-08 08.45.44.836337000 AM -07:00','DD-MON-RR HH.MI.SS.FF AM TZR'),null,null,null,null,'Review requests should be completed or closed in a reasonable...','NO_MILESTONE',null);
