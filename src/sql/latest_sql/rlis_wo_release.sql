DELETE FROM "DASH"."RULES" WHERE IDENTIFIER IN ('rlis_wo_release');
SET DEFINE ~;

INSERT INTO "DASH"."RULES" (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) 
VALUES ('rlis_wo_release','RLIs without a release','P5_STABILITY','These RLIs are not yet assigned to a release.','admin','NO_MILESTONE',0,'RLIS','synopsis, state, release_target, responsible, category, ',
'$user : User(eval($user.isValidUser("rli:responsibles:sw_mgr_responsibles:st_mgr_responsibles")))
$record_list : RecordSet( ) from  
 collect( RLIRecord( alwaysTrue == withoutRelease,
  npi_program == $user.npi_program_name || alwaysTrue == $user.junos ||  
  responsible memberOf $user.reportNames ||  
  sw_mgr_responsible memberOf $user.reportNames ||  
  st_mgr_responsible memberOf $user.reportNames ) )',0,1,to_timestamp_tz('01-MAR-10 10.28.24.688609000 PM -08:00','DD-MON-RR HH.MI.SS.FF AM TZR'),null,null,null,null,'RLIs with Release Target of "unscheduled" or "n/a"','NO_MILESTONE',0);
