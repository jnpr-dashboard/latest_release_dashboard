DELETE FROM "DASH"."RULES" WHERE IDENTIFIER IN ('rlis_wo_dev_complete_date');
SET DEFINE ~;

INSERT INTO "DASH"."RULES" (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) 
VALUES ('rlis_wo_dev_complete_date','RLIs without dev-complete date','HOLD_TO_SCHEDULE','Engineering owners of RLIs must provide ETAs in the [SW Dev Complete Date] fields. The date should be whatever you think your current ETA is. This is not necessarily a contract or a promise -- it''s a way of communicating your status as accurately as possible. You can (and should) change it whenever you know your ETA has moved.','lbirch','NO_MILESTONE',0,'RLIS','synopsis, state, project_status, rli_class, sw_proj_len, sw_dev_complete_date, responsible, sw_mgr_responsible, sw_responsible,','$now: Date()
$release : ReleaseRecord( hasOpenRLIs == true )
$user : User(eval($user.isValidUser("rli:responsibles:sw_mgr_responsibles:st_mgr_responsibles:sw_responsibles:st_responsibles")))
$record_list : RecordSet( ) from
 collect( RLIRecord((relname == $release.relname || relname memberOf $release.childBranch),
  state == "committed",
  class != "hardware",
  ((sw_proj_len != "n/a") || (tracking_pr != 0)),
  ((sw_dev_complete_date < $now) || (sw_dev_complete_date_string == "")),
  npi_program == $user.npi_program_name ||
  alwaysTrue == $user.junos || responsible memberOf $user.reportNames ||
    sw_mgr_responsible memberOf $user.reportNames ||
    st_mgr_responsible memberOf $user.reportNames ||
    sw_responsible == $user.id || st_responsible == $user.id, $ds : dataSource,
    project_status_ordinal < ($ds.picklistOrdinal("project_status", "dev-complete")) ) )',1,1,to_timestamp_tz('15-JUN-12 02.42.12.474343000 PM -08:00','DD-MON-RR HH.MI.SS.FF AM TZR'),'release',null,null,null,'RLIs that aren''t yet dev-complete must provide ETAs.','NO_MILESTONE',0);
