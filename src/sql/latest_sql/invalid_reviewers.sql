DELETE FROM "DASH"."RULES" WHERE IDENTIFIER IN ('invalid_reviewers');
SET DEFINE ~;

INSERT INTO "DASH"."RULES" (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) 
VALUES ('invalid_reviewers','Reviews with invalid reviewers','HOLD_TO_SCHEDULE','Review requests with invalid reviewers (eg. users that have left the company)  
must be reassigned.','skumar','NO_MILESTONE',0,'REVIEWS','synopsis, arrival-date, last-modified, submitter, ',
'$user : User(eval($user.isValidUser("review:moderators")))
$record_list : RecordSet( ) from  
 collect( ReviewRecord( hasInvalidReviewers == true,  
  hasResponsibleModerators == true, age < 120,  
  alwaysTrue == $user.junos || moderators contains $user ) )',3,1,to_timestamp_tz('05-APR-08 08.45.44.841940000 AM -07:00','DD-MON-RR HH.MI.SS.FF AM TZR'),null,null,null,null,'Review requests with invalid reviewers (eg. users that have l...','NO_MILESTONE',null);
