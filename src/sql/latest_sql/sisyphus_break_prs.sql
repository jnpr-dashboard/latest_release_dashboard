DELETE FROM "DASH"."RULES" WHERE IDENTIFIER IN ('sisyphus_break_prs');
SET DEFINE ~;

INSERT INTO "DASH"."RULES" (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) 
VALUES ('sisyphus_break_prs','Sisyphus Build Break PRs','HOLD_TO_SCHEDULE','Sisyphus build is broken, get the torches and pitchforks.','gcapolupo','NO_MILESTONE',0,'PRS','synopsis, state, class, problem-level, category, product, planned-release, blocker, submitter-id, responsible, dev-owner, originator, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli,','$release : ReleaseRecord( )  
$user : User(eval($user.isValidUser("pr:responsibles:dev_owners")))
$record_list : RecordSet( ) from
 collect( PRRecord( category == "sisyphus-build-break",
         releases contains $release.relname,
         (alwaysTrue != $user.junos || planned_release not matches "^[1-9][0-9]?\.[0-9][wWxX].*"),
         npi == $user.npi_program
         || alwaysTrue == $user.junos
         || responsible memberOf $user.reportNames 
         || ( dev_owner memberOf $user.reportNames &&  state != "feedback")))',
2,1,to_timestamp_tz('17-FEB-12 03.00.00.000000000 PM -08:00','DD-MON-RR HH.MI.SS.FF AM TZR'),'release',null,null,null,'Sisyphus build is broken, get the torches and pitchforks.','NO_MILESTONE',null);
