DELETE FROM "DASH"."RULES" WHERE IDENTIFIER IN ('prs_unassigned_cat_user');
SET DEFINE ~;

INSERT INTO "DASH"."RULES" (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) 
VALUES ('prs_unassigned_cat_user','Unassigned category PRs','HOLD_TO_SCHEDULE','All PRs in categories "owned" by the user (SPOC, owner or group-owner) that are assigned to a non-person (category alias or non/former employee) for this release.','admin','NO_MILESTONE',0,'PRS','synopsis, state, class, problem-level, category, product, planned-release, blocker, submitter-id, responsible, dev-owner, originator, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli,','$release : ReleaseRecord( )  
  $user : User(eval($user.isValidUser("pr:category_spocs")))
  $record_list : RecordSet( ) from
  collect( PRRecord( releases contains $release.relname,
          responsible != real_responsible,
          (alwaysTrue != $user.junos || planned_release not matches "^[1-9][0-9]?\.[0-9][wWxX].*"),
          category_spoc memberOf $user.reportNames))',
0,1,to_timestamp_tz('17-FEB-12 03.00.00.000000000 PM -07:00','DD-MON-RR HH.MI.SS.FF AM TZR'),'release',null,null,null,'All PRs in categories "owned" by the user (SPOC, owner or gro...','NO_MILESTONE',0);
