DELETE FROM "DASH"."RULES" WHERE IDENTIFIER IN ('rlis_user_scoping');
SET DEFINE ~;

INSERT INTO "DASH"."RULES" (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) 
VALUES ('rlis_user_scoping','RLIs in Scoping and Planning','HOLD_TO_SCHEDULE','RLIs in Scoping and Planning','admin','NO_MILESTONE',0,'RLIS','synopsis, p1_release_target, state, responsible, sw_mgr_responsible, sw_responsible, st_responsible, st_mgr_responsible, plm_responsible, tp_mgr_responsible, tp_lead_responsible, tp_responsible, ','$release : ReleaseRecord( hasOpenRLIs == true )  
  $user : User(eval($user.isValidUser("rli:responsibles:sw_mgr_responsibles:st_mgr_responsibles:sw_responsibles:st_responsibles:plm_responsibles:tp_responsibles:tp_lead_responsibles:tp_mgr_responsibles")))
  $record_list : RecordSet( ) from  
   collect( RLIRecord((p1relname == $release.relname || p1relname memberOf $release.childBranch),
    state == "committed",  
    npi_program == $user.npi_program_name ||  
    alwaysTrue == $user.junos || responsible memberOf $user.reportNames ||  
    sw_mgr_responsible memberOf $user.reportNames ||  
    st_mgr_responsible memberOf $user.reportNames ||  
    sw_responsible == $user.id || st_responsible == $user.id ||  
    plm_responsible memberOf $user.reportNames ||  
    tp_responsible memberOf $user.reportNames ||  
    tp_lead_responsible memberOf $user.reportNames ||  
    tp_mgr_responsible memberOf $user.reportNames ) )',0,1,to_timestamp_tz('13-JUL-12 02.42.12.474343000 PM -08:00','DD-MON-RR HH.MI.SS.FF AM TZR'),'release',null,null,null,'RLIs in scoping and planning','NO_MILESTONE',0);
