DELETE FROM "DASH"."RULES" WHERE IDENTIFIER IN ('prs_wo_release');
SET DEFINE ~;

INSERT INTO  "DASH"."RULES" (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) 
VALUES ('prs_wo_release','PRs without a release','HOLD_TO_SCHEDULE','These PRs have no Conf-Committed-Release, Committed-Release, Release or Reported-In value.','admin','NO_MILESTONE',0,'PRS','synopsis, state, class, problem-level, category, product, planned-release, blocker, submitter-id, responsible, dev-owner, originator, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli,',
'$user : User(eval($user.isValidUser("pr:responsibles")))
$record_list : RecordSet( ) from   
 collect( PRRecord( alwaysTrue == withoutRelease,   
         npi == $user.npi_program ||   
         alwaysTrue == $user.junos || responsible memberOf $user.reportNames ) )',
0,1,to_timestamp_tz('17-FEB-12 03.00.00.000000000 PM -08:00','DD-MON-RR HH.MI.SS.FF AM TZR'),null,null,null,null,'PRs that dont have a release defined','NO_MILESTONE',0);
