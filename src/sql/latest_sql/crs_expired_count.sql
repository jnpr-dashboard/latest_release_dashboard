DELETE FROM "DASH"."RULES" WHERE IDENTIFIER IN ('crs_expired_count');
SET DEFINE ~;

INSERT INTO "DASH"."RULES" (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) 
VALUES ('crs_expired_count','Expired CRS Requests','HOLD_TO_SCHEDULE','To count expipred CRS requests.','admin','NO_MILESTONE',0,'CRSS','throttle_request_id',
'$release : ReleaseRecord(relname matches "[1-9][0-9]?.?[0-9]?$")
$user : User(eval($user.isValidUser("crs:users")))
$record_list : RecordSet() from 
               collect(
                       CRSRecord(
                                 release_names contains $release.standard_release_name, 
                                 throttle_request_state == "Expired", 
                                 alwaysTrue == $user.junos || eval($user.isOwned(user_username))
                                )
                      )',
0,1,to_timestamp_tz('13-SEP-12 07.00.00.000000000 PM -07:00','DD-MON-RR HH.MI.SS.FF AM TZR'),'release',null,null,null,'CRS requests','NO_MILESTONE',0);

