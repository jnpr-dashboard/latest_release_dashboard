DELETE FROM "DASH"."RULES" WHERE IDENTIFIER IN ('critical_prs_fix');
SET DEFINE ~;

INSERT INTO "DASH"."RULES" (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) 
VALUES ('critical_prs_fix','Critical PRs Fix - non active rules','HOLD_TO_SCHEDULE','Owners of PRs with the [Severity] field set to ''critical'' must  
prioritize these ETAs and fixes over non-critical PRs.  
-- Only used this on PR-fix','gcapolupo','NEXT_BUILD',45,'PRS','synopsis, state, class, problem-level, category, product, planned-release, blocker, reported-in, submitter-id, responsible, dev-owner, originator, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli,','$release : ReleaseRecord( )  
$user : User(eval($user.isValidUser("pr:responsibles:dev_owners")))
$record_list : RecordSet( ) from
 collect( PRRecord( problem_level == "1-CL1" || problem_level == "3-IL1", clazz == "bug",
         (category not matches ".*hw-.*" &&  category not matches ".*build.*"),
         releases contains $release.relname,
         (alwaysTrue != $user.junos || planned_release not matches "^[1-9][0-9]?\.[0-9][wWxX].*"),
         npi == $user.npi_program
         || alwaysTrue == $user.junos
         || ((dev_owner memberOf $user.reportNames
                 &&  ( state == "info" || (state=="feedback" &&  eval(alwaysTrue != blank_devowner))))
             || (responsible memberOf $user.reportNames
                 && (state == "open" || state == "analyzed"
                     || (state=="feedback" &&  eval(alwaysTrue == blank_devowner)))))))',
3,1,to_timestamp_tz('17-FEB-12 03.00.00.000000000 PM -08:00','DD-MON-RR HH.MI.SS.FF AM TZR'),'release',null,null,null,'Owners of PRs with the [Severity] field set to ''critical'' mus...','NEXT_DEPLOY',-7);
