DELETE FROM "DASH"."RULES" WHERE IDENTIFIER IN ('all_closed_prs_fix_newly_in');
SET DEFINE ~
insert into "DASH"."RULES" (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,
GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) 
values (
        'all_closed_prs_fix_newly_in',
        'All Closed PRs Fix Newly In',
        'HOLD_TO_SCHEDULE',
        'All Closed PRs Fix',
        'admin',
        'NO_MILESTONE',
        0,'NEWLYINPRS',
        'synopsis, state, class, problem-level, category, product, planned-release, blocker, reported-in, submitter-id, responsible, dev-owner, originator, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli,',
        '$release : ReleaseRecord( )      
                $user : User(eval($user.isValidUser("newlyinpr:responsibles:dev_owners")))
                $record_list : RecordSet( ) from   
                 collect( NewlyInPRFixRecord( category not matches ".*hw-.*",     
                        ib_name == $release.relname, state == "closed",
                        alwaysTrue == $user.junos || (eval(alwaysTrue == blank_devowner)  &&  responsible memberOf $user.reportNames) 
                        || (eval(alwaysTrue != blank_devowner)  &&  dev_owner memberOf $user.reportNames)))',
        0,1,to_timestamp_tz('27-AUG-13 01.50.36.016000000 PM -08:00','DD-MON-RR HH.MI.SS.FF AM TZR'),
        'release',null,null,null,'All Closed PRs Fix - Newly In.','NO_MILESTONE',0
);
