DELETE FROM "DASH"."RULES" WHERE IDENTIFIER IN ('cvbc_prs');
SET DEFINE ~;

INSERT INTO "DASH"."RULES" (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) 
VALUES ('cvbc_prs','CVBC PRs','HOLD_TO_SCHEDULE','The Customer-Visible-Behavior-Changed (CVBC) field is used to notify Technical Publications of any changes introduced by the PR that need to be documented for the customer.','june','NO_MILESTONE',0,'PRS','synopsis, category, planned-release, state, responsible, techpubs-owner, class, problem-level, confidential, cust-visible-behavior-changed, cvbc-documented-in, ',
'$user : User( junos == false && eval($user.isValidNonJunosUser("undeadcvbc:techpubs_owners")))
$record_list : RecordSet( ) from  
 collect( UndeadCvbcRecord(techpubs_owner memberOf $user.reportNames ) )',1,1,to_timestamp_tz('06-JUL-09 01.54.29.579309000 PM -07:00','DD-MON-RR HH.MI.SS.FF AM TZR'),null,null,null,null,'CVBC PRs need to be documented for the customer','NO_MILESTONE',0);
