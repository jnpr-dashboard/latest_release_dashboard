DELETE FROM "DASH"."RULES" WHERE IDENTIFIER IN ('third_party_sw_legal_prs');
SET DEFINE ~;

INSERT INTO "DASH"."RULES" (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) 
VALUES ('third_party_sw_legal_prs','Third party software legal PRs','HOLD_TO_SCHEDULE','Third party software legal PRs','admin','NO_MILESTONE',0,'PRS','synopsis, state, class, problem-level, category, product, planned-release, blocker, submitter-id, responsible, dev-owner, originator, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli,',
'$release : ReleaseRecord(relname matches "[1-9][0-9]?.?[0-9]?$")
$user : User(eval($user.isValidUser("pr:responsibles:dev_owners")))
$record_list : RecordSet( ) from
collect( PRRecord( keywords matches ".*per-legal-mustfix.*", 
                   age < 63072000000, 
                   releases contains $release.standard_release_name, 
                   alwaysTrue == $user.junos || responsible memberOf $user.reportNames || dev_owner memberOf $user.reportNames
                 )
        )',  --2 years = 2*365*24*60*60*1000 = 63072000000 milliseconds
0,1,to_timestamp_tz('19-NOV-13 03.00.00.000000000 PM -07:00','DD-MON-RR HH.MI.SS.FF AM TZR'),'release',null,null,null,'Third party software legal PRs','NO_MILESTONE',0);

