DELETE FROM "DASH"."RULES" WHERE IDENTIFIER IN ('backlog_prs_reference_responsible_not_dev');
SET DEFINE ~;

INSERT INTO "DASH"."RULES" (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) 
VALUES ('backlog_prs_reference_responsible_not_dev','Reference Backlog PRs by Responsible != Dev-Owner 2012-12-31','HOLD_TO_SCHEDULE','All Reference Backlog PR by using responsible as their primary accountability where the responsible != dev-owner','admin','NO_MILESTONE',0,'PRS','synopsis, problem-level, state, class, category, product, planned-release, blocker, submitter-id, responsible, dev-owner, originator, systest-owner, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli, created, resolution, feedback-date, jtac-case-id,',
'$user : User(eval($user.isValidUser("oldpr:backlog_pr_responsibles")))
$record_list : RecordSet( ) from   
 collect(   
    OldPRRecord(   
        extract_date == "2012-12-31",   
        state != "closed",   
        (problem_level == "1-CL1" || problem_level == "2-CL2" || problem_level == "4-CL3"
         || problem_level == "3-IL1" || problem_level == "4-IL2"
         || problem_level == "5-CL4" || problem_level == "5-IL3"), 
        (clazz == "bug" || clazz == "unreproducible"  || clazz == "duplicate" ),functional_area=="software",   
        alwaysTrue == possible_backlog,   
        real_responsible != dev_owner,   
        npi == $user.npi_program || alwaysTrue == $user.junos ||   
        backlog_pr_responsible memberOf $user.reportNames ))   
',1,1,to_timestamp_tz('21-FEB-13 12.00.00.511060000 PM -07:00','DD-MON-RR HH.MI.SS.FF AM TZR'),null,'$user.setScore("backlog_prs_reference_responsible_not_dev","pr_backlog",$record_list);',null,null,'Reference Backlog PRs by Responsible != Dev-Owner','NO_MILESTONE',0);
