DELETE FROM "DASH"."RULES" WHERE IDENTIFIER IN ('pending_reviews');
SET DEFINE ~;

INSERT INTO "DASH"."RULES" (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) 
VALUES ('pending_reviews','Inactive pending reviews','HOLD_TO_SCHEDULE','Reviews should be completed in a timely manner; if there is no activity for  
two days, this is considered a problem.','skumar','NO_MILESTONE',0,'REVIEWS','synopsis, arrival-date, last-modified, submitter, ',
'$user : User(eval($user.isValidUser("review:reviewers")))
$record_list : RecordSet( ) from  
 collect( ReviewRecord( hasResponsibleReviewers == true,  
  daysSinceLastModified > 2, age < 120,  
  alwaysTrue == $user.junos || reviewers contains $user ) )',2,1,to_timestamp_tz('05-APR-08 08.45.44.847851000 AM -07:00','DD-MON-RR HH.MI.SS.FF AM TZR'),null,null,null,null,'Reviews should be completed in a timely manner; if there is n...','NO_MILESTONE',null);
