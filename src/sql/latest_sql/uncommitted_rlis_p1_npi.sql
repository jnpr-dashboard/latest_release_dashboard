DELETE FROM "DASH"."RULES" WHERE IDENTIFIER IN ('uncommitted_rlis_p1_npi');
SET DEFINE ~;

INSERT INTO "DASH"."RULES" (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) 
VALUES ('uncommitted_rlis_p1_npi','Uncommitted RLIs in post-P1 NPIs','P1_EXIT','RLIs for all NPI Programs that have exited P1 should have [State]=''committed'' or better unless they''re undergoing a P1 Re-Plan. This implies that NPI program deliverables were scoped out in detail prior to exiting P1 and that management has approved the resources associated with this program.','lbirch','NO_MILESTONE',0,'RLIS','synopsis, release_target, state, responsible, sw_mgr_responsible, sw_responsible, plm_responsible, npi_program, ','$now : Date( )  
$npi : NPIRecord( p1_exit < $now )  
$user : User( npi_program == $npi.id )  
$release : ReleaseRecord( )  
$record_list : RecordSet( ) from  
 collect( RLIRecord( relname == $release.relname,  
  npi_program == $npi.synopsis,  
  $ds : dataSource,  
  state_ordinal < ($ds.picklistOrdinal("state", "committed")) ) )',1,1,to_timestamp_tz('28-APR-09 04.27.18.190611000 PM -07:00','DD-MON-RR HH.MI.SS.FF AM TZR'),'release',null,null,null,'RLIs for all NPI Programs that have exited P1 should have [St...','NO_MILESTONE',0);