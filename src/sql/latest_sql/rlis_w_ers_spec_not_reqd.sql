DELETE FROM "DASH"."RULES" WHERE IDENTIFIER IN ('rlis_w_ers_spec_not_reqd');
SET DEFINE ~;

INSERT INTO "DASH"."RULES" (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) 
VALUES ('rlis_w_ers_spec_not_reqd','RLIs with ERs and ''spec not required''','HOLD_TO_SCHEDULE','Engineering owners of RLIs with an [ER Numbers] field that isn''t null  
can''t set the [Functional Specification Status] field to ''not-required''.  
We have a TL9000 requirement to show traceability for RLIs that have ERs  
associated with them - PLM is required to verify that a functional  
specification meets customer requirements and sign-off for any  
ER-related RLI before it can be included in a JUNOS release.','lbirch','NO_MILESTONE',0,'RLIS','synopsis, responsible, sw_mgr_responsible, sw_responsible, er-numbers, functional_specification, functional_spec_status,','$release : ReleaseRecord( hasOpenRLIs == true )  
$user : User(eval($user.isValidUser("rli:responsibles:sw_mgr_responsibles:st_mgr_responsibles")))
$record_list : RecordSet( ) from  
 collect( RLIRecord( functional_spec_status == "not-required", er_numbers != "",  
  functional_specification == "",
  (relname == $release.relname || relname memberOf $release.childBranch),
  state == "committed",  
  npi_program == $user.npi_program_name ||  
  alwaysTrue == $user.junos || responsible memberOf $user.reportNames ||  
    sw_mgr_responsible memberOf $user.reportNames ||  
    st_mgr_responsible memberOf $user.reportNames ) )',1,0,to_timestamp_tz('21-AUG-12 12.00.00.000000000 PM -07:00','DD-MON-RR HH.MI.SS.FF AM TZR'),'release',null,null,null,'RLIs with associated [ER Numbers] must have Func Specs.','NO_MILESTONE',0);
