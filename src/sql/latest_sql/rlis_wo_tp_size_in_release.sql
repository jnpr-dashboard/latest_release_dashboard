DELETE FROM "DASH"."RULES" WHERE IDENTIFIER IN ('rlis_wo_tp_size_in_release');
SET DEFINE ~;

INSERT INTO "DASH"."RULES" (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) 
VALUES ('rlis_wo_tp_size_in_release','RLIs without TP Size','P2_EXIT','The TP size field shows the estimated size of the work that needs to be performed by Techpubs for this RLI. Includes unknown, no_doc, S, M, L, XL, XXL.','mwazna','FUNC_SPEC_COMPLETE',-42,'RLIS','synopsis, tp_size, functional_specification, tp_responsible, tp_lead_responsible, tp_release_status, ','$release : ReleaseRecord( )  
$user : User(eval($user.isValidUser("rli:tp_responsibles:tp_lead_responsibles:tp_mgr_responsibles")))
$record_list : RecordSet( ) from  
 collect( RLIRecord( tp_size == "unknown",   
  tp_release_status != "no_doc",  
  state != "deferred",  
  relname == $release.relname,  
  alwaysTrue != $user.junos &&   
  (tp_responsible memberOf $user.reportNames ||  
  tp_lead_responsible memberOf $user.reportNames ||  
  tp_mgr_responsible memberOf $user.reportNames) ) )',2,1,to_timestamp_tz('15-JAN-09 04.42.11.809215000 PM -08:00','DD-MON-RR HH.MI.SS.FF AM TZR'),'release',null,null,null,'A size estimate is needed for release planning.','NO_MILESTONE',0);
