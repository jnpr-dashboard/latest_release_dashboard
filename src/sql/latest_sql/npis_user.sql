DELETE FROM "DASH"."RULES" WHERE IDENTIFIER IN ('npis_user');
SET DEFINE ~;

INSERT INTO "DASH"."RULES" (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) 
VALUES ('npis_user','NPI programs','P5_STABILITY','All NPI programs where the user is Responsible or Lead.','admin','NO_MILESTONE',0,'NPIS','synopsis, phase, responsible, responsible_product_group, p0_exit, p1_exit, p2_exit, p3_exit, p4_exit, ',
'$user : User(eval($user.isValidUser("npi:responsibles:beta_leads:cs_leads:hw_leads:ops_leads:pgm_leads:plm_leads:sw_leads:systest_leads:techpubs_leads")))
  $record_list : RecordSet( ) from  
   collect( NPIRecord( alwaysTrue == $user.junos ||  
    responsible memberOf $user.reportNames ||  
    beta_lead memberOf $user.reportNames ||  
        cs_lead memberOf $user.reportNames ||  
        hw_lead memberOf $user.reportNames ||  
        ops_lead memberOf $user.reportNames ||  
        pgm_lead memberOf $user.reportNames ||  
        plm_lead memberOf $user.reportNames ||  
        sw_lead memberOf $user.reportNames ||  
        systest_lead memberOf $user.reportNames ||  
        techpubs_lead memberOf $user.reportNames ) )',0,1,to_timestamp_tz('07-APR-09 08.31.49.831802000 PM -07:00','DD-MON-RR HH.MI.SS.FF AM TZR'),null,null,null,null,'All NPI programs where the user is Responsible or Lead.','NO_MILESTONE',0);
