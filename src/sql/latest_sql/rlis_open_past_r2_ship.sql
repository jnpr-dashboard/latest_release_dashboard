DELETE FROM "DASH"."RULES" WHERE IDENTIFIER IN ('rlis_open_past_r2_ship');
SET DEFINE ~;

INSERT INTO "DASH"."RULES" (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) 
VALUES ('rlis_open_past_r2_ship','RLIs open after R2 shipped','P5_STABILITY','Owners of RLIs must update their [State] field to a  
"closed" value to indicate that they''ve shipped once the release  
deploys:  
<ul>  
<li>closed-released - released to field and record closed</li>  
<li>closed-unsupported - internal Development and/or Testing  
complete and record closed; no customer deliverables  
and not officially supported</li>  
</ul>','lbirch','R2_DEPLOY',-1,'RLIS','synopsis, release_target, state, responsible, sw_mgr_responsible, sw_responsible, plm_responsible, ','$now : Date( )  
$release : ReleaseRecord( r2_deploy < $now )  
$user : User(eval($user.isValidUser("rli:responsibles:sw_mgr_responsibles:st_mgr_responsibles")))
$record_list : RecordSet( ) from  
 collect( RLIRecord( relname == $release.relname,  
  state == "committed",  
  release_target matches ".*R2", npi_program == $user.npi_program_name ||  
  alwaysTrue == $user.junos || responsible memberOf $user.reportNames ||  
    sw_mgr_responsible memberOf $user.reportNames ||  
    st_mgr_responsible memberOf $user.reportNames ) )',2,1,to_timestamp_tz('05-JUN-09 08.16.08.051077000 PM -07:00','DD-MON-RR HH.MI.SS.FF AM TZR'),'release',null,null,null,'RLIs must be closed after they are deployed in a release.','R2_DEPLOY',-45);
