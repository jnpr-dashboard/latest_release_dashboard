DELETE FROM "DASH"."RULES" WHERE IDENTIFIER IN ('branch_dev');
SET DEFINE ~;

INSERT INTO "DASH"."RULES" (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) 
VALUES ('branch_dev','Dev Branches','HOLD_TO_SCHEDULE','Total Dev Branches on Release','admin','NO_MILESTONE',0,'BTS','planned-release, branch_name, branch_type, state, responsible,','$release :ReleaseRecord( )
  $user : User(eval($user.isValidUser("bt:responsibles")))
  $record_list : RecordSet( ) from
   collect( BTRecord(
          relname == $release.relname,
          branch_type == "dev",
          (alwaysTrue != $user.junos || branch_name not matches "DEV_X.*"),
          responsible memberOf $user.reportNames
          || alwaysTrue == $user.junos))',
1,1,to_timestamp_tz('17-FEB-12 03.00.00.000000000 PM -08:00','DD-MON-RR HH.MI.SS.FF AM TZR'),'release',null,null,null,'Dev Branches','NO_MILESTONE',0);
