DELETE FROM "DASH"."RULES" WHERE IDENTIFIER IN ('major_closed_pr');
SET DEFINE ~;

INSERT INTO "DASH"."RULES" (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) 
VALUES ('major_closed_pr','Major Closed PRs','HOLD_TO_SCHEDULE','Major PRs should be prioritized over minor ones.','mbasha','NO_MILESTONE',0,'CLOSEDPRS','synopsis, state, class, problem-level, category, product, planned-release, blocker, reported-in, submitter-id, responsible, dev-owner, originator, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli,','$release : ReleaseRecord( )   
$user : User(eval($user.isValidUser("closedpr:responsibles:dev_owners")))
$record_list : RecordSet( ) from
 collect( ClosedPRRecord( problem_level == "2-CL2" || problem_level == "4-CL3" || problem_level == "4-IL2" || problem_level == "5-IL3",
         (category not matches ".*hw-.*" &&  category not matches ".*build.*"),
         (releases contains $release.relname || branchRelease == $release.relname),
         (alwaysTrue != $user.junos || planned_release not matches "^[1-9][0-9]?\.[0-9][wWxX].*"),
         npi == $user.npi_program
         || alwaysTrue == $user.junos
         || (eval(alwaysTrue == blank_devowner) &&  responsible memberOf $user.reportNames) 
         || (eval(alwaysTrue != blank_devowner) &&  dev_owner memberOf $user.reportNames)))',
0,1,to_timestamp_tz('17-FEB-12 03.00.00.000000000 PM -08:00','DD-MON-RR HH.MI.SS.FF AM TZR'),'release',null,null,null,'Major PRs should be prioritized over minor ones.','NO_MILESTONE',0);
