DELETE FROM "DASH"."RULES" WHERE IDENTIFIER IN ('docs_not_frs_ready');
SET DEFINE ~;

INSERT INTO "DASH"."RULES" (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) 
VALUES ('docs_not_frs_ready','RLIs with Docs not FRS ready','P4_EXIT','Document is not ready for release to production/printer/web.','mwazna','P4_EXIT',14,'RLIS','synopsis, tp_status, tp_edit_status, tp_release_status, tp_responsible, ','$release : ReleaseRecord( )  
$user : User(eval($user.isValidUser("rli:tp_responsibles:tp_lead_responsibles:tp_mgr_responsibles")))
$record_list : RecordSet( ) from  
 collect( RLIRecord( tp_release_status not in ("doc_frs_ready", "no_doc"),  
  state != "deferred",  
  relname == $release.relname,  
  tp_responsible not in ("", "n/a", "N/A", "NA"),  
  alwaysTrue != $user.junos &&   
  (tp_responsible memberOf $user.reportNames ||  
  tp_lead_responsible memberOf $user.reportNames ||  
  tp_mgr_responsible memberOf $user.reportNames) ) )',4,1,to_timestamp_tz('15-JAN-09 04.42.11.991920000 PM -08:00','DD-MON-RR HH.MI.SS.FF AM TZR'),'release',null,null,null,'Document is not ready for release to production/printer/web.','NO_MILESTONE',0);
