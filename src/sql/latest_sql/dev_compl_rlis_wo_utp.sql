DELETE FROM "DASH"."RULES" WHERE IDENTIFIER IN ('dev_compl_rlis_wo_utp');
SET DEFINE ~;

INSERT INTO "DASH"."RULES" (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) 
VALUES ('dev_compl_rlis_wo_utp','Dev-Complete RLIs without approved UTP','HOLD_TO_SCHEDULE','Junos process requires that all SW RLIs involving new code being written and checked in must have an approved Unit Test Plan (UTP), even in cases where RLIs are infrastructure work or won''t be customer-visible.  UTPs are not required for RLIs where no code is being developed.','lbirch','NO_MILESTONE',0,'RLIS','synopsis, state, project_status, rli_class, sw_dev_complete_date, tracking_pr, responsible, sw_mgr_responsible, sw_responsible, sw_proj_len, unit_test_plan_status, unit_test_plan,','$release : ReleaseRecord( hasOpenRLIs == true )
$user : User(eval($user.isValidUser("rli:responsibles:sw_mgr_responsibles:st_mgr_responsibles:sw_responsibles:st_responsibles")))
$record_list : RecordSet( ) from
 collect( RLIRecord((relname == $release.relname || relname memberOf $release.childBranch),
  $ds : dataSource,
  state == "committed",
  rli_class != "hardware",
  ((sw_dev_complete_date_string != "") && (sw_proj_len != "n/a") && (tracking_pr != 0)),
  npi_program == $user.npi_program_name ||
  alwaysTrue == $user.junos || responsible memberOf $user.reportNames ||
  sw_mgr_responsible memberOf $user.reportNames ||
  st_mgr_responsible memberOf $user.reportNames ||
  sw_responsible == $user.id || st_responsible == $user.id,
  project_status_ordinal >= ($ds.picklistOrdinal("project_status", "dev-complete")),
  unit_test_plan_status_ordinal < ($ds.picklistOrdinal("unit_test_plan_status", "approved")) ) )',1,0,to_timestamp_tz('21-AUG-12 12.00.00.000000000 PM -07:00','DD-MON-RR HH.MI.SS.FF AM TZR'),'release',null,null,null,'Junos process requires an approved UTP for checking in code.','NO_MILESTONE',0);
