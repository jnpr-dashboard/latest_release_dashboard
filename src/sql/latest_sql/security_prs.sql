DELETE FROM "DASH"."RULES" WHERE IDENTIFIER IN ('security_prs');
SET DEFINE ~;

INSERT INTO "DASH"."RULES" (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) 
VALUES ('security_prs','Security PRs','HOLD_TO_SCHEDULE','Security PRs.','admin','NO_MILESTONE',0,'PRS','synopsis, state, class, problem-level, category, product, planned-release, blocker, submitter-id, responsible, dev-owner, originator, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli,',
'$release : ReleaseRecord(relname matches "[1-9][0-9]?.?[0-9]?$")
$user : User(eval($user.isValidUser("pr:responsibles:dev_owners")))
$record_list : RecordSet( ) from
collect( PRRecord(
attributes == "SIRT-review" || attributes matches "^SIRT-review[:].*" || attributes matches ".*[:]SIRT-review[:].*" || attributes matches ".*[:]SIRT-review$",
((((((((((((product == "esr-series" || product matches "^esr-series[:].*" || product matches ".*[:]esr-series[:].*" || product matches ".*[:]esr-series$") ||(product == "ex-series" || product matches "^ex-series[:].*" || product matches ".*[:]ex-series[:].*" || product matches ".*[:]ex-series$")) ||
          (product == "j-series" || product matches "^j-series[:].*" || product matches ".*[:]j-series[:].*" || product matches ".*[:]j-series$")) || 
         (product == "m-series" || product matches "^m-series[:].*" || product matches ".*[:]m-seriesv[:].*" || product matches ".*[:]m-series$")) || 
        (product matches "mx[\\/]x" || product matches "^mx[\\/]x[:].*" || product matches ".*[:]mx[\\/]x[:].*" || product matches ".*[:]mx[\\/]x$")) || 
       (product == "ptx-series" || product matches "^ptx-series[:].*" || product matches".*[:]ptx-series[:].*" || product matches ".*[:]ptx-series$")) || 
      (product == "srx-branch-series" || product matches "^srx-branch-series[:].*" || product matches".*[:]srx-branch-series[:].*" || product matches ".*[:]srx-branch-series$")) || 
     (product == "srx-series" || product matches "^srx-series[:].*" || product matches".*[:]srx-series[:].*" || product matches ".*[:]srx-series$")) || 
    (product == "t-series" || product matches "^t-series[:].*" || product matches ".*[:]t-series[:].*" || product matches ".*[:]t-series$")
   ) || (product == "tx" || product matches "^tx[:].*" || product matches ".*[:]tx[:].*" || product matches ".*[:]tx$")
  ) || (product == "vsrx-series" || product matches "^vsrx-series[:].*" || product matches ".*[:]vsrx-series[:].*" || product matches ".*[:]vsrx-series$")
 ) || (product == "all-junos" || product matches "^all-junos[:].*" || product matches ".*[:]all-junos[:].*" || product matches ".*[:]all-junos$")),
releases contains $release.standard_release_name, 
alwaysTrue == $user.junos || responsible memberOf $user.reportNames || dev_owner memberOf $user.reportNames))',
0,1,to_timestamp_tz('19-NOV-13 03.00.00.000000000 PM -07:00','DD-MON-RR HH.MI.SS.FF AM TZR'),'release',null,null,null,'Security Prs.','NO_MILESTONE',0);
