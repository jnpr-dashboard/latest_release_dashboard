DELETE FROM "DASH"."RULES" WHERE IDENTIFIER IN ('crs_aging_count');
SET DEFINE ~;

INSERT INTO "DASH"."RULES" (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) 
VALUES ('crs_aging_count','Aging CRS Requests','HOLD_TO_SCHEDULE','To count aging CRS requests.','admin','NO_MILESTONE',0,'CRSS','throttle_request_id',
'$release : ReleaseRecord(relname matches "[1-9][0-9]?.?[0-9]?$")
$user : User(eval($user.isValidUser("crs:approvers:jrms:users")))
$record_list : RecordSet() from 
               collect(
                       CRSRecord(
                                 release_names contains $release.standard_release_name, 
                                 cr_life > 7 && (cr_life < 14 || cr_life == 14), 
                                 throttle_request_state == "Open" && (
                                                                      alwaysTrue == $user.junos || 
                                                                      eval($user.isOwned(approver_username)) && approval_tier == "1" ||                                
                                                                      (
                                                                        eval($user.isOwned(approver_username)) && mgr_approval_state == "Open" || 
                                                                        eval($user.isOwned(jrm_username)) && throttle_approval_jrm_state == "Open"
                                                                      ) && approval_tier == "2"
                                                                     ) 
                                 || 
                                 (
                                  throttle_request_state == "Information Required" || throttle_request_state == "Pending on Approval"
                                 ) && 
                                 (
                                  alwaysTrue == $user.junos || eval($user.isOwned(user_username))
                                 )
                                )
                       )',
1,1,to_timestamp_tz('13-SEP-12 07.00.00.000000000 PM -07:00','DD-MON-RR HH.MI.SS.FF AM TZR'),'release',null,null,null,'CRS requests','NO_MILESTONE',0);
