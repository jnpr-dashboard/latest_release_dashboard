DELETE FROM "DASH"."RULES" WHERE IDENTIFIER IN ('prs_non_junos_cat_user');
SET DEFINE ~;

INSERT INTO "DASH"."RULES" (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) 
VALUES ('prs_non_junos_cat_user','Category PRs with non-JUNOS responsible','HOLD_TO_SCHEDULE','PRs in categories "owned" by the user (SPOC, owner or group-owner) that have a non-JUNOS-engineering responsible in the release.  These are PRs that the user is "waiting on".','admin','NO_MILESTONE',0,'PRS','synopsis, state, class, problem-level, category, product, planned-release, blocker, submitter-id, responsible, dev-owner, originator, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli,','$release : ReleaseRecord( )
  $user : User(eval($user.isValidUser("pr:category_spocs")))
  $record_list : RecordSet( ) from
   collect( PRRecord( releases contains $release.relname,
            non_junos_responsible == true,
            (alwaysTrue != $user.junos || planned_release not matches "^[1-9][0-9]?\.[0-9][wWxX].*"),
            category_spoc memberOf $user.reportNames))',
0,1,to_timestamp_tz('17-FEB-12 03.00.00.000000000 PM -07:00','DD-MON-RR HH.MI.SS.FF AM TZR'),'release',null,null,null,'PRs in categories "owned" by the user (SPOC, owner or group-o...','NO_MILESTONE',0);
