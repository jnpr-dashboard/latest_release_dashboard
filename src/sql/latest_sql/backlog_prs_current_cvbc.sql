DELETE FROM "DASH"."RULES" WHERE IDENTIFIER IN ('backlog_prs_current_cvbc');
SET DEFINE ~;

INSERT INTO "DASH"."RULES" (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) 
VALUES ('backlog_prs_current_cvbc','Current Backlog for CVBC PRs','HOLD_TO_SCHEDULE','All Current Backlog for CVBC PRs ','admin','NO_MILESTONE',0,'PRS','synopsis, state, cust-visible-behavior-changed, class, problem-level, category, product, planned-release, cvbc-documented-in, blocker, submitter-id, techpubs-owner, originator, systest-owner, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli, created, resolution, feedback-date, jtac-case-id,',
'$user : User(eval($user.isValidUser("undeadcvbc:backlog_pr_techpub_owners")))
$record_list : RecordSet( ) from  
 collect( UndeadCvbcRecord(  
    alwaysTrue == possible_backlog,  
    alwaysTrue == $user.junos ||  
    backlog_pr_techpub_owner memberOf $user.reportNames ))  
',0,1,to_timestamp_tz('01-NOV-10 12.00.00.511060000 PM -07:00','DD-MON-RR HH.MI.SS.FF AM TZR'),null,null,null,null,'Current Backlog for CVBC PRs','NO_MILESTONE',0);
