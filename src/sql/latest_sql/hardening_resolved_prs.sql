-- Release Hardening: Open PRs

-- clear out existing rule
DELETE FROM "DASH"."RULES" WHERE IDENTIFIER IN ('hardening_resolved_prs');
SET DEFINE ~;

INSERT INTO "DASH"."RULES" (IDENTIFIER, NAME, OBJECTIVE, DESCRIPTION, OWNER, MILESTONE, HORIZON, COUNTS, QUERY_FIELDS, LHS, WEIGHT, ACTIVE, GROUPING_VARIABLE, LAST_UPDATED, LEDE, SUNSET_MILESTONE, SUNSET_HORIZON)
VALUES (
  'hardening_resolved_prs',
  'Release Hardening Resolved PRs',
  'HOLD_TO_SCHEDULE',
  'Release Hardening Resolved PRs for user grouped by a release.',
  'admin',
  'NO_MILESTONE',
  '0',
  'HRCLOSEPRS',
  'synopsis, state, problem-level, category, product, planned-release, blocker, submitter-id, responsible, dev-owner, originator, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli,',
'$release : ReleaseRecord( )
 $user : User(eval($user.isValidUser("hardeningclosedpr:dev_owners")))
 $record_list : RecordSet( ) from
 collect( HardeningClosedPRRecord( releases contains $release.relname,
                          hardening_program != "",
                          resolution == "fixed",
                          branch != "",
                          alwaysTrue == $user.junos || dev_owner memberOf $user.reportNames ) )',
  '0',
  '1',
  'release',
  to_timestamp_tz('25-APR-12 03.38.39.897156000 PM -07:00', 'DD-MON-RR HH.MI.SS.FF AM TZR'),
  'Release Hardening Resolved PRs', -- LEDE
  'NO_MILESTONE',           -- SUNSET_MILESTONE
  '0'                       -- SUNSET_HORIZON
) ;
