DELETE FROM "DASH"."RULES" WHERE IDENTIFIER IN ('at_risk');
SET DEFINE ~;

INSERT INTO "DASH"."RULES" (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) 
VALUES ('at_risk','RLIs At Risk','HOLD_TO_SCHEDULE','Owners of RLIs with their [At Risk] field set to ''yes'' are required to  
actively pursue timely resolution. If an RLI continues to remain at  
risk, it''s important to update the [At Risk Reason] with current status  
and ETA for resolution. The RLI [Action] field can be used to identify  
next steps.','lbirch','IN_DEVELOPMENT',0,'RLIS','synopsis, state, project_status, at_risk, at_risk_reason, action, responsible, sw_mgr_responsible, sw_responsible, sw_dev_complete_date, hw_dev_complete_date, ','$release : ReleaseRecord( hasOpenRLIs == true )  
$user : User(eval($user.isValidUser("rli:responsibles:sw_mgr_responsibles:st_mgr_responsibles:sw_responsibles:st_responsibles")))
$record_list : RecordSet( ) from  
 collect( RLIRecord( at_risk == true,  
  state != "deferred",  
  (relname == $release.relname || relname memberOf $release.childBranch),  
  /* The line below is logically equivalent to:  
   *   not (category contains "trd" &&  trd_status in ("rejected", "required"))  
   * See http://en.wikipedia.org/wiki/De_Morgan''s_laws */  
  ( category not contains "trd" || trd_status not in ("rejected", "required") ),  
  npi_program == $user.npi_program_name ||  
  alwaysTrue == $user.junos || responsible memberOf $user.reportNames ||  
  sw_mgr_responsible memberOf $user.reportNames ||  
  st_mgr_responsible memberOf $user.reportNames ||  
    sw_responsible == $user.id || st_responsible == $user.id ) )',3,1,to_timestamp_tz('15-JUN-12 02.42.12.017274000 PM -08:00','DD-MON-RR HH.MI.SS.FF AM TZR'),'release',null,null,null,'Owners of RLIs with their [At Risk] field set to ''yes'' are re...','NO_MILESTONE',0);
