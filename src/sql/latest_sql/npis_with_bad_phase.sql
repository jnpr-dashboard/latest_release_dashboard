DELETE FROM "DASH"."RULES" WHERE IDENTIFIER IN ('npis_with_bad_phase');
SET DEFINE ~;


INSERT INTO "DASH"."RULES" (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) 
VALUES ('npis_with_bad_phase','NPI Programs with wrong phase','HOLD_TO_SCHEDULE','The phase field of an NPI program should be updated to reflect the current phase.','lbirch','NO_MILESTONE',0,'NPIS','synopsis, phase, responsible, p0_exit, p1_exit, p2_exit, p3_exit, p4_exit, ','$now : Date( )  
$user : User(eval($user.isValidUser("npi:responsibles:beta_leads:cs_leads:hw_leads:ops_leads:pgm_leads:plm_leads:sw_leads:systest_leads:techpubs_leads")))
$record_list : RecordSet( ) from  
 collect( NPIRecord( (phase == "P0" &&  p0_exit_epoch < $now.time &&  > 0) ||  
  (phase == "P1" &&  p1_exit_epoch < $now.time &&  > 0) ||  
  (phase == "P2" &&  p2_exit_epoch < $now.time &&  > 0) ||  
  (phase == "P3" &&  p3_exit_epoch < $now.time &&  > 0) ||  
  (phase == "P4" &&  p4_exit_epoch < $now.time &&  > 0),  
  alwaysTrue == $user.junos ||  
  responsible == $user.id ||  
  beta_lead == $user.id ||  
  cs_lead == $user.id ||  
  hw_lead == $user.id ||  
  ops_lead == $user.id ||  
  pgm_lead == $user.id ||  
  plm_lead == $user.id ||  
  sw_lead == $user.id ||  
  systest_lead == $user.id ||  
  techpubs_lead == $user.id ) )',10,1,to_timestamp_tz('24-APR-09 05.13.23.351753000 PM -07:00','DD-MON-RR HH.MI.SS.FF AM TZR'),null,null,null,null,'The phase field of an NPI program should be updated to reflec...','NO_MILESTONE',0);
