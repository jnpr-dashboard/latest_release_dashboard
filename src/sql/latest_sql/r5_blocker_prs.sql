DELETE FROM "DASH"."RULES" WHERE IDENTIFIER IN ('r5_blocker_prs');
SET DEFINE ~;

INSERT INTO "DASH"."RULES" (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) 
VALUES ('r5_blocker_prs','R5 Blocker PRs','HOLD_TO_SCHEDULE','Owners of PRs with the [Blocking-Release] field set must  
prioritize these ETAs and fixes over non-blocker PRs.  This rule includes all non-suspended PRs (systest metrics exclude analyzed).','admin','R5_BUILD',60,'PRS','synopsis, state, class, problem-level, category, product, planned-release, blocker, submitter-id, responsible, dev-owner, originator, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli,','$release : ReleaseRecord( )  
$user : User(eval($user.isValidUser("pr:responsibles:dev_owners")))
$record_list : RecordSet( )   
 from collect( PRRecord( blocked_release == $release.relname,  
  (blocker_types contains "r5" && blocker_types not contains "pendingblocker"), npi == $user.npi_program ||  
  alwaysTrue == $user.junos || responsible memberOf $user.reportNames ||  
  ( dev_owner memberOf $user.reportNames &&  state != "feedback" ) ) )',3,1,to_timestamp_tz('31-OCT-13 00.00.00.585003000 PM -07:00','DD-MON-RR HH.MI.SS.FF AM TZR'),'release',null,null,null,'Includes all non-suspended PRs.','R5_DEPLOY',-7);
