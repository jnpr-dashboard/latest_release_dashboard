DELETE FROM "DASH"."RULES" WHERE IDENTIFIER IN ('backlog_prs_reference_cvbc');
SET DEFINE ~;

INSERT INTO "DASH"."RULES" (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) 
VALUES ('backlog_prs_reference_cvbc','Reference Backlog CVBC PRs','HOLD_TO_SCHEDULE','All Reference Backlog CVBC PR','admin','NO_MILESTONE',0,'PRS','synopsis, state, class, cust-visible-behavior-changed, problem-level, category, product, planned-release, blocker, submitter-id, techpubs-owner, originator, systest-owner, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli, created, resolution, feedback-date, jtac-case-id,',
'$user : User(eval($user.isValidUser("oldpr:backlog_pr_techpub_owners")))
$record_list : RecordSet( )  
  from collect(  
    OldPRRecord(  
        extract_date == "2012-12-31",  
        (cust_visible_behavior_changed == "yes" || cust_visible_behavior_changed == "yes-ready-for-review"),  
        alwaysTrue == possible_backlog,  
        npi == $user.npi_program || alwaysTrue == $user.junos ||  
        backlog_pr_techpub_owner memberOf $user.reportNames  
    )  
  )',0,1,to_timestamp_tz('31-DEC-10 12.00.00.511060000 PM -07:00','DD-MON-RR HH.MI.SS.FF AM TZR'),null,null,null,null,'Reference Backlog CVBC PRs ','NO_MILESTONE',0);
