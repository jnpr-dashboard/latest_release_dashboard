DELETE FROM "DASH"."RULES" WHERE IDENTIFIER IN ('unclosed_test');
SET DEFINE ~;

INSERT INTO "DASH"."RULES" (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) 
VALUES ('unclosed_test','Total number of PRs with unclosed test status','HOLD_TO_SCHEDULE','To know PRs with unclosed test-gap-analysis-status.','admin','NO_MILESTONE',0,'PRS','synopsis, state, class, problem-level, category, product, planned-release, blocker, submitter-id, responsible, systest-owner, test-gap-analysis-status, originator, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli,',
'$user : User(eval($user.isValidUser("pr:systest_owners")))
$record_list : RecordSet() from collect(PRRecord( test_gap_analysis_status != "" && test_gap_analysis_status != "closed-not-fixed" && test_gap_analysis_status != "closed", systest_owner != "" && (alwaysTrue == $user.junos || systest_owner memberOf $user.reportNames || systest_owner memberOf $user.aliases || eval($user.isSystestOwnerAliasPartOfReportNames(systest_owner)))))', 
2,1,to_timestamp_tz('23-MAY-12 07.00.00.000000000 PM -07:00','DD-MON-RR HH.MI.SS.FF AM TZR'),null,null,null,null,'Test owners of PRs','NO_MILESTONE',0);
