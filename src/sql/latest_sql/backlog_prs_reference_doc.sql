DELETE FROM "DASH"."RULES" WHERE IDENTIFIER IN ('backlog_prs_reference_doc');
SET DEFINE ~;

INSERT INTO "DASH"."RULES" (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) 
VALUES ('backlog_prs_reference_doc','Reference Backlog Doc PRs','HOLD_TO_SCHEDULE','All Reference Backlog Doc PR','admin','NO_MILESTONE',0,'PRS','synopsis, state, class, problem-level, category, product, planned-release, blocker, submitter-id, techpubs-owner, originator, systest-owner, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli, created, resolution, feedback-date, jtac-case-id,',
'$user : User(eval($user.isValidUser("oldpr:backlog_pr_techpub_owners")))
 $record_list : RecordSet( )   
   from collect(   
     OldPRRecord(   
         extract_date == "2012-12-31",   
         state != "closed",   
         (problem_level == "1-CL1" || problem_level == "2-CL2" || problem_level == "4-CL3" || problem_level == "3-IL1" || problem_level == "4-IL2" ||  problem_level == "5-IL3"),
         (clazz == "bug" || clazz == "unreproducible" ), functional_area=="documentation",   
         alwaysTrue == possible_backlog,   
         npi == $user.npi_program || alwaysTrue == $user.junos ||   
         backlog_pr_techpub_owner memberOf $user.reportNames   
     )   
   ) 
',0,1,to_timestamp_tz('31-DEC-10 12.00.00.511060000 PM -07:00','DD-MON-RR HH.MI.SS.FF AM TZR'),null,null,null,null,'Reference Backlog Doc PRs','NO_MILESTONE',0);
