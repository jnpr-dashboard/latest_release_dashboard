DELETE FROM "DASH"."RULES" WHERE IDENTIFIER IN ('no_milestones_blocker_prs');
SET DEFINE ~;

INSERT INTO "DASH"."RULES" (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) 
VALUES ('no_milestones_blocker_prs','No Milestone Blocker PRs','HOLD_TO_SCHEDULE','To get non-milestone blocker PRs','admin','NO_MILESTONE',0,'PRS','synopsis, state, class, problem-level, category, product, planned-release, blocker, submitter-id, responsible, dev-owner, originator, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli,','$release : ReleaseRecord( )   
$user : User(eval($user.isValidUser("pr:responsibles:dev_owners")))
$record_list : RecordSet( ) from
 collect( PRRecord( blocked_release == $release.relname,
         alwaysTrue == not_milestones_release,
         planned_release matches "^[1-9][0-9]?\.[0-9]$",
         blocker_types not contains "pendingblocker",
         npi == $user.npi_program
         || alwaysTrue == $user.junos
         || responsible memberOf $user.reportNames
         || (dev_owner memberOf $user.reportNames &&  state != "feedback")))',
1,1,to_timestamp_tz('31-OCT-13 00.00.00.000000000 PM -07:00','DD-MON-RR HH.MI.SS.FF AM TZR'),'release',null,null,null,'To get non-milestone blocker PRs','NO_MILESTONE',0);
