DELETE FROM "DASH"."RULES" WHERE IDENTIFIER IN ('branch_responsible');
SET DEFINE ~;

INSERT INTO "DASH"."RULES" (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) 
VALUES ('branch_responsible','Integration Branch','HOLD_TO_SCHEDULE','Total Integration Branch (IB) on Release','admin','NO_MILESTONE',0,'BTS','branch_name, branch_type, state, responsible,','$release :ReleaseRecord( )   
  $user : User(eval($user.isValidUser("bt:responsibles")))
  $record_list : RecordSet( ) from   
   collect( BTRecord(    
          relname == $release.relname,  
          branch_type == "ib",   
          responsible memberOf $user.reportNames || alwaysTrue == $user.junos ) )',1,1,to_timestamp_tz('01-MAR-10 10.28.03.909545000 PM -08:00','DD-MON-RR HH.MI.SS.FF AM TZR'),'release',null,null,null,'Integration Branch','NO_MILESTONE',0);
