DELETE FROM "DASH"."RULES" WHERE IDENTIFIER IN ('major_prs_fix');
SET DEFINE ~;

INSERT INTO "DASH"."RULES" (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) 
VALUES ('major_prs_fix','Major PRs Fix','HOLD_TO_SCHEDULE','Major PRs should be prioritized over minor ones.  
-- only used this on PR-fix report --','admin','NO_MILESTONE',0,'PRS','synopsis, state, class, problem-level, category, product, planned-release, blocker, reported-in, submitter-id, responsible, dev-owner, originator, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli,','$release : ReleaseRecord( )  
$user : User(eval($user.isValidUser("pr:responsibles:dev_owners")))
$record_list : RecordSet( ) from  
 collect( PRRecord( problem_level in ("2-CL2","4-CL3","4-IL2","5-IL3"), clazz == "bug",  
         (category not matches ".*hw-.*" &&  category not matches ".*build.*"),  
         (releases contains $release.relname || branchRelease == $release.relname ),  
         (alwaysTrue != $user.junos || planned_release not matches "^[1-9][0-9]?\.[0-9][wWxX].*"),
         npi == $user.npi_program 
         || alwaysTrue == $user.junos
         || ((dev_owner memberOf $user.reportNames
                 && (state == "info" || (state=="feedback" &&  eval(alwaysTrue != blank_devowner))))
             || (responsible memberOf $user.reportNames
                 && (state == "open" || state == "analyzed"
                     || (state=="feedback" &&  eval(alwaysTrue == blank_devowner)))))))',
0,1,to_timestamp_tz('17-FEB-12 03.00.00.000000000 PM -08:00','DD-MON-RR HH.MI.SS.FF AM TZR'),'release',null,null,null,'Major PRs should be prioritized over minor ones.','NO_MILESTONE',0);
