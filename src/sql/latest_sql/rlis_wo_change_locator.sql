DELETE FROM "DASH"."RULES" WHERE IDENTIFIER IN ('rlis_wo_change_locator');
SET DEFINE ~;

INSERT INTO "DASH"."RULES" (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) 
VALUES ('rlis_wo_change_locator','RLIs without Change Locator','P4_EXIT','When changes are incorporated into the books/topics, the TP Responsible completes information about the location of the new documentation for this RLI.','mwazna','P4_EXIT',-7,'RLIS','synopsis, change_locator, tp_responsible, ','$release : ReleaseRecord( )  
$user : User(eval($user.isValidUser("rli:tp_responsibles:tp_lead_responsibles:tp_mgr_responsibles")))
$record_list : RecordSet( ) from  
 collect( RLIRecord( change_locator == "",  
  state != "deferred",  
  relname == $release.relname,  
  tp_release_status != "no_doc",  
  alwaysTrue != $user.junos &&   
  (tp_responsible memberOf $user.reportNames ||  
  tp_lead_responsible memberOf $user.reportNames ||  
  tp_mgr_responsible memberOf $user.reportNames) ) )',3,1,to_timestamp_tz('15-JAN-09 04.42.11.979123000 PM -08:00','DD-MON-RR HH.MI.SS.FF AM TZR'),'release',null,null,null,'RLIs need to be incorporated into books or topics','NO_MILESTONE',0);
