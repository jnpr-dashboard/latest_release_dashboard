-- Release Hardening: Open PRs

-- clear out existing rule
DELETE FROM "DASH"."RULES" WHERE IDENTIFIER IN ('hardening_open_prs');
SET DEFINE ~;

INSERT INTO "DASH"."RULES" (IDENTIFIER, NAME, OBJECTIVE, DESCRIPTION, OWNER, MILESTONE, HORIZON, COUNTS, QUERY_FIELDS, LHS, WEIGHT, ACTIVE, GROUPING_VARIABLE, LAST_UPDATED, LEDE, SUNSET_MILESTONE, SUNSET_HORIZON)
VALUES (
  'hardening_open_prs',
  'Release Hardening Open PRs',
  'HOLD_TO_SCHEDULE',
  'Release Hardening Open PRs for user grouped by a release.',
  'admin',
  'NO_MILESTONE',
  '0',
  'PRS',
  'synopsis, state, class, problem-level, category, product, planned-release, blocker, submitter-id, responsible, dev-owner, originator, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli,',
'$release : ReleaseRecord( )
 $user : User(eval($user.isValidUser("pr:dev_owners")))
 $record_list : RecordSet( ) from
 collect( PRRecord( releases contains $release.relname,
                    state == "open" || state == "anayzed" || state == "info",
                    hardening_program != "",
                    alwaysTrue == $user.junos || dev_owner memberOf $user.reportNames ) )',
  '0',
  '1',
  'release',
  to_timestamp_tz('19-APR-12 12.00.00.511060000 PM -07:00', 'DD-MON-RR HH.MI.SS.FF AM TZR'),
  'Release Hardening Open PRs.', -- LEDE
  'NO_MILESTONE',           -- SUNSET_MILESTONE
  '0'                       -- SUNSET_HORIZON
) ;
