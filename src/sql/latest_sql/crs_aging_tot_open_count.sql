DELETE FROM "DASH"."RULES" WHERE IDENTIFIER IN ('crs_aging_tot_open_count');
SET DEFINE ~;

INSERT INTO "DASH"."RULES" (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) 
VALUES ('crs_aging_tot_open_count','Aging CRS Requests : TOT/Open','ENSURE_COMMIT_REQUEST','Commit requests should be reviewed and completed within a timely manner.','admin','NO_MILESTONE',0,'CRSS','throttle_request_id',
'$release : ReleaseRecord(relname matches "[1-9][0-9]?.?[0-9]?$")
 $user : User(eval($user.isValidUser("crs:approvers:jrms")))
 $record_list : RecordSet() from 
               collect(
                       CRSRecord(
                                 release_names contains $release.standard_release_name, 
                                 cr_life > 7 && (cr_life < 14 || cr_life == 14),
                                 throttle_request_type == "tot",
                                 throttle_request_state == "Open" && (
                                                                      alwaysTrue == $user.junos || 
                                                                      eval($user.isOwned(approver_username)) && approval_tier == "1" ||                                
                                                                      (
                                                                        eval($user.isOwned(approver_username)) && mgr_approval_state == "Open" || 
                                                                        eval($user.isOwned(jrm_username)) && throttle_approval_jrm_state == "Open"
                                                                      ) && approval_tier == "2"
                                                                     ) 
                                )
                       )',
0,1,to_timestamp_tz('10-APR-13 07.00.00.000000000 PM -07:00','DD-MON-RR HH.MI.SS.FF AM TZR'),'release',null,null,null,'Open/Info/Pending CRS Requests','NO_MILESTONE',0);

