DELETE FROM "DASH"."RULES" WHERE IDENTIFIER IN ('reviews_user');
SET DEFINE ~;

INSERT INTO "DASH"."RULES" (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) 
VALUES ('reviews_user','Pending reviews','HOLD_TO_SCHEDULE','Code reviews where user is a reviewer or responsible moderator.','admin','NO_MILESTONE',0,'REVIEWS','synopsis, arrival-date, last-modified, submitter, ',
'$user : User(eval($user.isValidUser("review:allResponsibles")))
 $record_list : RecordSet( ) from  
  collect( ReviewRecord( age < 120, alwaysTrue == $user.junos ||  
   allResponsibles contains $user ) )',0,1,to_timestamp_tz('07-APR-09 08.36.33.176117000 PM -07:00','DD-MON-RR HH.MI.SS.FF AM TZR'),null,null,null,null,'Code reviews where user is a reviewer or responsible moderato...','NO_MILESTONE',0);
