DELETE FROM "DASH"."RULES" WHERE IDENTIFIER IN ('closed_critical_prs');
SET DEFINE ~;

INSERT INTO "DASH"."RULES" (IDENTIFIER,NAME,OBJECTIVE,DESCRIPTION,OWNER,MILESTONE,HORIZON,COUNTS,QUERY_FIELDS,LHS,WEIGHT,ACTIVE,LAST_UPDATED,GROUPING_VARIABLE,RHS,ATTRIBUTES,AGENDA_GROUP,LEDE,SUNSET_MILESTONE,SUNSET_HORIZON) 
VALUES ('closed_critical_prs','Closed Critical PRs','P5_STABILITY','Owners of PRs with the [Severity] field set to "critical" must prioritize these ETAs and fixes over non-critical PRs.','admin','NO_MILESTONE',0,'CLOSEDPRS','synopsis, state, class, problem-level, category, product, planned-release, blocker, reported-in, submitter-id, responsible, dev-owner, originator, attributes, last-modified, updated-by-responsible, arrival-date, fix-eta, committed-release, conf-committed-release, confidential, rli,','$release : ReleaseRecord( )   
$user : User(eval($user.isValidUser("closedpr:responsibles:dev_owners")))
$record_list : RecordSet( ) from
 collect( ClosedPRRecord( problem_level == "1-CL1" || problem_level == "3-IL1",
 (category not matches ".*hw-.*" &&  category not matches ".*build.*"),
  releases contains $release.relname,
  (alwaysTrue != $user.junos || planned_release not matches "^[1-9][0-9]?\.[0-9][wWxX].*"),
  npi == $user.npi_program
  || alwaysTrue == $user.junos
  || (eval(alwaysTrue == blank_devowner) &&  responsible memberOf $user.reportNames)
  || (eval(alwaysTrue != blank_devowner) &&  dev_owner memberOf $user.reportNames) ))',
0,1,to_timestamp_tz('17-FEB-12 03.00.00.000000000 PM -08:00','DD-MON-RR HH.MI.SS.FF AM TZR'),'release',null,null,null,'Owners of PRs with the [Severity] field set to "critical" mus...','NO_MILESTONE',0);
