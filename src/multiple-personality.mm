<map version="0.9.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1233631873366" ID="Freemind_Link_1469092374" MODIFIED="1233631896128" TEXT="multiple-personality">
<node CREATED="1233632015089" ID="_" MODIFIED="1233721306235" POSITION="right" TEXT="Background">
<node CREATED="1233632029967" ID="Freemind_Link_1070152126" MODIFIED="1233721314532" TEXT="The Dashboard was originally built for JUNOS sw-mgrs"/>
<node CREATED="1233632046954" ID="Freemind_Link_1345030639" MODIFIED="1233632063384" TEXT="Later expanded to developers"/>
<node CREATED="1233632063866" ID="Freemind_Link_471856651" MODIFIED="1233722542399" TEXT="Other groups (release leads, systest, governance) use the Dashboard, but it could be much more valuable for them"/>
</node>
<node CREATED="1233632211249" ID="Freemind_Link_1566223956" MODIFIED="1233709954376" POSITION="right" TEXT="High Level Goals">
<node CREATED="1233632220792" ID="Freemind_Link_1963525286" MODIFIED="1233722697658" TEXT="The Dashboard becomes the primary source for JUNOS release status and development priorities"/>
<node CREATED="1233632563117" ID="Freemind_Link_1048953304" MODIFIED="1233722451985" TEXT="The Dashboard provides tailored &quot;views&quot; to different user communties">
<node CREATED="1233632590972" ID="Freemind_Link_1398193167" MODIFIED="1233633476559" TEXT="Development Manager"/>
<node CREATED="1233633454915" ID="Freemind_Link_825875186" MODIFIED="1233633482215" TEXT="Developer"/>
<node CREATED="1233632599141" ID="Freemind_Link_1227460534" MODIFIED="1233632603163" TEXT="Systest"/>
<node CREATED="1233632603640" ID="Freemind_Link_928091187" MODIFIED="1233633503159" TEXT="Release lead / Governance"/>
<node CREATED="1233633503840" ID="Freemind_Link_974574598" MODIFIED="1233633521071" TEXT="BU / BG Management"/>
<node CREATED="1233709708181" ID="Freemind_Link_1608476030" MODIFIED="1233709738416" TEXT="Others?"/>
</node>
<node CREATED="1233722937890" ID="Freemind_Link_1014305957" MODIFIED="1233723016333" TEXT="The Dashboard has more reporting capabilities">
<node CREATED="1233632488534" ID="Freemind_Link_1226121223" MODIFIED="1236100566810" TEXT="Users should think &quot;Dashboard&quot; when they need a report"/>
<node CREATED="1233709739718" ID="Freemind_Link_888351645" MODIFIED="1233723233908" TEXT="Additional ad-hoc reporting facilities"/>
<node CREATED="1233632362328" ID="Freemind_Link_522232674" MODIFIED="1233723103252" TEXT="Replaces many or most of the current laboriously maintained spreadsheets of PR and RLI data">
<edge WIDTH="thin"/>
</node>
</node>
<node CREATED="1233632631297" ID="Freemind_Link_1054282605" MODIFIED="1233632652274" TEXT="Data is available on a per-BU or per-BG basis"/>
</node>
<node CREATED="1233710020463" ID="Freemind_Link_1615847786" MODIFIED="1233710023847" POSITION="right" TEXT="Justification">
<node CREATED="1233710266609" ID="Freemind_Link_697385531" MODIFIED="1233710279066" TEXT="Need for a (near) real-time source of metrics"/>
<node CREATED="1233710023829" ID="Freemind_Link_1348282471" MODIFIED="1233710053617" TEXT="Confusion over multiple, sometimes contradicting, sources of &quot;truth&quot;"/>
<node CREATED="1233710302560" ID="Freemind_Link_701507439" MODIFIED="1233723203476" TEXT="Increased efficiency of having a single source used by all JUNOS engineering">
<node CREATED="1236100593402" ID="ID_1648293030" MODIFIED="1236100609277" TEXT="Dashboard is already used by development &amp; Governanace"/>
<node CREATED="1236049857856" ID="ID_368683552" MODIFIED="1236049869513" TEXT="Systest staff is increasingly interested in using the Dashboard"/>
</node>
<node CREATED="1233710054675" ID="Freemind_Link_864773856" MODIFIED="1233710199978" TEXT="Too much time collectively spent maintaining spreadsheets and preparing reports">
<node CREATED="1236051408383" ID="ID_22844264" MODIFIED="1236051432679" TEXT="Some anecdotal reports:"/>
<node CREATED="1233717162889" ID="Freemind_Link_1400433328" MODIFIED="1236110754094" TEXT="southern - 2-3 hrs/wk (&quot;several weeks behind schedule&quot;)"/>
<node CREATED="1233717170641" ID="Freemind_Link_1193490663" MODIFIED="1233717175942" TEXT="jshaw - 8 hrs/wk"/>
<node CREATED="1236051720530" ID="ID_375087713" MODIFIED="1236051745641" TEXT="vladimir - 1 hr/wk per SLT BU, plus time to go over them in mtgs"/>
<node CREATED="1236051307910" ID="ID_126098560" MODIFIED="1236051324799" TEXT="ritao - looking to hire a &quot;release manager&quot; to handle this stuff"/>
<node CREATED="1236051356518" ID="ID_1644902516" MODIFIED="1236051390747" TEXT="vengal - &quot;We do spend significant time on trying to massage data from deepthought&quot;."/>
</node>
</node>
<node CREATED="1236101261411" ID="ID_727545879" MODIFIED="1236101271164" POSITION="right" TEXT="Project Plan">
<node CREATED="1236101271166" ID="ID_1571622939" MODIFIED="1236101504073" TEXT="Interview users">
<node CREATED="1236101307649" ID="ID_1984356064" MODIFIED="1236101329527" TEXT="Talk to users from each community">
<node CREATED="1238571406242" ID="ID_1235084971" LINK="http://www-homes.juniper.net/~dbergstr/projects/dashboard/" MODIFIED="1238571420967" TEXT="Interview transcripts"/>
</node>
<node CREATED="1236101346158" ID="ID_892687980" MODIFIED="1236101364479" TEXT="How are you using Dashboard?"/>
<node CREATED="1236101357973" ID="ID_316276685" MODIFIED="1236101362327" TEXT="What do you want to see?"/>
<node CREATED="1236101330397" ID="ID_1852109009" MODIFIED="1236101448088" TEXT="Early to mid March"/>
</node>
<node CREATED="1236101366621" ID="ID_649221793" MODIFIED="1236101501025" TEXT="Re-Architect back-end">
<node CREATED="1236101396159" ID="ID_1322895920" MODIFIED="1236110099016" TEXT="Switch UI layer to Django"/>
<node CREATED="1236101420093" ID="ID_735040571" MODIFIED="1236101441576" TEXT="March"/>
</node>
<node CREATED="1236101462974" ID="ID_1201825302" MODIFIED="1236101495609" TEXT="Design new views">
<node CREATED="1236110203017" ID="ID_1126294892" MODIFIED="1238571309262" TEXT="Decide on initial new views - late March"/>
<node CREATED="1236101507353" ID="ID_222506874" MODIFIED="1238571319070" TEXT="User feedback on paper prototypes - early April"/>
<node CREATED="1236101525458" ID="ID_1384166642" MODIFIED="1238571323454" TEXT="Feedback on first draft in HTML - mid April"/>
</node>
<node CREATED="1236101630449" ID="ID_1063891478" MODIFIED="1236101638985" TEXT="Beta test">
<node CREATED="1236101638987" ID="ID_318714202" MODIFIED="1238571330166" TEXT="Run-through with interviewees - late April"/>
<node CREATED="1236110283829" ID="ID_864037195" MODIFIED="1238571336830" TEXT="Open beta - early May"/>
<node CREATED="1236110363346" ID="ID_583944393" MODIFIED="1236110377869" TEXT="Prioritize and schedule further work"/>
</node>
<node CREATED="1236110344012" ID="ID_1062440538" MODIFIED="1236110346125" TEXT="Release">
<node CREATED="1236110346127" ID="ID_810843690" MODIFIED="1238571343038" TEXT="Mid May"/>
</node>
</node>
<node CREATED="1235595050711" FOLDED="true" ID="Freemind_Link_1684134687" MODIFIED="1238607277403" POSITION="right" TEXT="Process" VSHIFT="19">
<node CREATED="1235605454466" ID="Freemind_Link_1636175582" MODIFIED="1235605486000" TEXT="Start design work on back-end changes">
<node CREATED="1235616140894" ID="ID_1677666559" MODIFIED="1235616161253" TEXT="Will Django/Java split really be feasible?">
<node CREATED="1235616403404" ID="ID_1745946280" MODIFIED="1235616426862" TEXT="What roadblocks will I hit in the data xfer code?"/>
<node CREATED="1235616327780" ID="ID_252997604" MODIFIED="1235616349605" TEXT="Where do I make the split?">
<node CREATED="1235616349607" ID="ID_917107383" MODIFIED="1235616377966" TEXT="Seems simplest to do it at the Struts layer"/>
<node CREATED="1235616378779" ID="ID_627329639" MODIFIED="1235616397342" TEXT="Some of the logic in User is UI based.  What to do with that?"/>
</node>
<node CREATED="1235616295669" ID="ID_105259934" MODIFIED="1235616319350" TEXT="How hard will it be to port existing pages to Django?"/>
</node>
<node CREATED="1235616162003" ID="ID_1649297992" MODIFIED="1235616195972" TEXT="Look at other ideas in the pipeline">
<node CREATED="1235616224822" ID="ID_89165575" MODIFIED="1235616239909" TEXT="Rollups in finalizeScores()"/>
<node CREATED="1235616258567" ID="ID_1069484240" MODIFIED="1235616264870" TEXT="Resistance to disappearing fields"/>
</node>
<node CREATED="1235616276615" ID="ID_867420841" MODIFIED="1235616293358" TEXT="Dojo =&gt; jQuery change"/>
</node>
<node CREATED="1235595060260" ID="Freemind_Link_942618126" MODIFIED="1235595070698" TEXT="Talk to users, see what they want">
<node CREATED="1235607252278" ID="Freemind_Link_973248706" MODIFIED="1235608269524" TEXT="Systester">
<node CREATED="1235678687514" ID="ID_968084689" MODIFIED="1235678693899" TEXT="jennyc"/>
</node>
<node COLOR="#000000" CREATED="1235608269987" ID="Freemind_Link_1694241934" MODIFIED="1235705156396" TEXT="Systest lead">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Other suggestions from jshaw:
    </p>
    <p>
      Vladimir Matveyev
    </p>
    <p>
      Sreedhevi Sankar - (Core Systest manager) - frequent user
    </p>
    <p>
      JennyC (SLT CSS QA / Systest) - not sure how much she uses / knows
    </p>
    <p>
      Alireza Assadzadeh (Core Software) - uses a lot, good insight, but you know that!
    </p>
    <p>
      Karthikeyan Nathillvar (NBBU Systest) - familiar with dashboard
    </p>
    <p>
      Clyde Neville (EABU Systest / subscriber service) - believe he uses dashboard
    </p>
    <p>
      Samaresh Nair (SLT HSS QA / Systest)- know the HSS team who tried Virtual teams
    </p>
    <p>
      Stacy Smith - (Core senior systester) - not a user, has thoughts on what he likes, was volunteered by his manager.
    </p>
  </body>
</html></richcontent>
<node CREATED="1235678695761" ID="ID_1328881937" MODIFIED="1235678699515" TEXT="cneville"/>
</node>
<node CREATED="1235608169707" ID="Freemind_Link_66428650" MODIFIED="1235608183539" TEXT="Governance">
<node CREATED="1235612310756" ID="Freemind_Link_1127593136" MODIFIED="1235612369101" TEXT="mmitchell"/>
</node>
<node CREATED="1235608195300" ID="Freemind_Link_1732968072" MODIFIED="1235608206627" TEXT="BU or BG management">
<node CREATED="1235609575427" ID="Freemind_Link_773032616" MODIFIED="1235609577513" TEXT="ritao"/>
<node CREATED="1235609577928" ID="Freemind_Link_222575705" MODIFIED="1235609579185" TEXT="rpe"/>
<node CREATED="1235609974403" ID="Freemind_Link_1885820548" MODIFIED="1235609977291" TEXT="pgacek"/>
<node CREATED="1235609987113" ID="Freemind_Link_1180202861" MODIFIED="1235610018075" TEXT="michel??"/>
</node>
<node CREATED="1235608215882" ID="Freemind_Link_763407506" MODIFIED="1235608245475" TEXT="Development Manager">
<node CREATED="1235612374920" ID="Freemind_Link_1953900760" MODIFIED="1235612377261" TEXT="steven"/>
<node CREATED="1235612459661" ID="Freemind_Link_1915686613" MODIFIED="1235612462981" TEXT="xzhu"/>
<node CREATED="1235612517395" ID="Freemind_Link_1751699265" MODIFIED="1235612518981" TEXT="anilk"/>
</node>
<node CREATED="1235608220442" ID="Freemind_Link_291293570" MODIFIED="1235608226339" TEXT="Developer">
<node CREATED="1235612410701" ID="Freemind_Link_42577444" MODIFIED="1235612413453" TEXT="mpeng"/>
<node CREATED="1235612433821" ID="Freemind_Link_165078044" MODIFIED="1235612439933" TEXT="sgoudar (BNG)"/>
<node CREATED="1235612724878" ID="Freemind_Link_1607081455" MODIFIED="1235612726598" TEXT="ravis"/>
</node>
</node>
<node CREATED="1235605488014" ID="Freemind_Link_1537181093" MODIFIED="1235605493592" TEXT="Start work on back-end changes"/>
<node CREATED="1235595087231" ID="Freemind_Link_523058495" MODIFIED="1235615770500" TEXT="Plan &amp; Prioritize changes">
<node CREATED="1235615827772" ID="ID_1831943232" MODIFIED="1235615916108" TEXT="There will be many good ideas"/>
<node CREATED="1235615916479" ID="ID_1031428208" MODIFIED="1235615933708" TEXT="Pick one or two per user group"/>
<node CREATED="1235615937005" ID="ID_9345915" MODIFIED="1235615959532" TEXT="List the rest somewhere on the app"/>
</node>
<node CREATED="1235595071168" ID="Freemind_Link_772489005" MODIFIED="1235595086626" TEXT="Mock up pages, show them to users">
<node CREATED="1235615999386" ID="ID_1885431582" MODIFIED="1235616081381" TEXT="Paper mockups for the first round"/>
<node CREATED="1235616023109" ID="ID_1386168343" MODIFIED="1235616037261" TEXT="Try to have at least two mockups per idea"/>
<node CREATED="1235616082457" ID="ID_1624501302" MODIFIED="1235616097357" TEXT="Get feedback, whip out new mockups"/>
<node CREATED="1235616103724" ID="ID_147712476" MODIFIED="1235616128733" TEXT="After second feedback, do an HTML mockup for verification"/>
</node>
<node CREATED="1235595102272" ID="Freemind_Link_379044637" MODIFIED="1235595106258" TEXT="Schedule work"/>
<node CREATED="1235595106719" ID="Freemind_Link_940640062" MODIFIED="1235595110242" TEXT="Roll out in stages"/>
</node>
<node CREATED="1235616513535" FOLDED="true" ID="ID_1347982931" MODIFIED="1238607279390" POSITION="right" TEXT="User interview scripts">
<node CREATED="1235704200935" ID="ID_27496762" MODIFIED="1235704208767" TEXT="Initial">
<node CREATED="1235617357864" ID="ID_1358238686" MODIFIED="1235617377251" TEXT="Show me how you use the Dashboard"/>
<node CREATED="1235702808352" ID="ID_791028208" MODIFIED="1235702824897" TEXT="What other sources of status info do you use regularly?">
<node CREATED="1235702836786" ID="ID_24078551" MODIFIED="1235702847897" TEXT="custom scripts"/>
<node CREATED="1235702848341" ID="ID_859358237" MODIFIED="1235702850169" TEXT="nag emails"/>
<node CREATED="1235702852256" ID="ID_712206041" MODIFIED="1235702856353" TEXT="bookmarks"/>
<node CREATED="1235702857000" ID="ID_98919880" MODIFIED="1235702858065" TEXT="etc."/>
</node>
<node CREATED="1235702759820" ID="ID_1333832425" MODIFIED="1235702830385" TEXT="What else would you like to see on the Dashboard?"/>
</node>
<node CREATED="1235704224748" ID="ID_715611419" MODIFIED="1235704231116" TEXT="First Mockup">
<node CREATED="1235704231118" ID="ID_23296651" MODIFIED="1235705579002" TEXT="Does this meet your needs?"/>
<node CREATED="1235705669557" ID="ID_347891984" MODIFIED="1235705683421" TEXT="How&apos;s about this?"/>
<node CREATED="1235705684488" ID="ID_1710285148" MODIFIED="1235705704494" TEXT="What features of each do you like or dislike?"/>
<node CREATED="1235705579754" ID="ID_994799797" MODIFIED="1235705589621" TEXT="What would you like to see changed?"/>
<node CREATED="1235705590212" ID="ID_1946964410" MODIFIED="1235705598029" TEXT="Does it give you any other good ideas?"/>
</node>
</node>
<node CREATED="1233710334750" ID="Freemind_Link_480187734" MODIFIED="1233717399250" POSITION="right" TEXT="User Requests">
<node CREATED="1233710350041" ID="Freemind_Link_1172415714" MODIFIED="1233710381106" TEXT="Different rule sets for different audiences">
<node CREATED="1233710383772" ID="Freemind_Link_1063138142" MODIFIED="1233710397922" TEXT="It would be fairly easy to have multiple sets of rules, and allow users to switch from one set to another.  Combining set may or may not make sense.  This would require minimal UI changes, and could be implemented fairly quickly."/>
<node CREATED="1233713070070" ID="Freemind_Link_838482234" MODIFIED="1233713081542" TEXT="Tunable time-based multipliers per group"/>
<node CREATED="1233713095893" ID="Freemind_Link_592420834" MODIFIED="1233713098830" TEXT="Different weights and mulitpliers for different user communities.  A given issue may be more important for one group than another."/>
</node>
<node CREATED="1233713000365" ID="Freemind_Link_1402919780" MODIFIED="1233713024487" TEXT="Alternate UIs">
<node CREATED="1233713025299" ID="Freemind_Link_1304543305" MODIFIED="1233713026726" TEXT="Some views/profiles will be better served by different presentations"/>
</node>
<node CREATED="1233713028411" ID="Freemind_Link_271569218" MODIFIED="1233713040370" TEXT="Overdue/Today/This Week/Later">
<node CREATED="1233713040350" ID="Freemind_Link_45760059" MODIFIED="1233713051246" TEXT="Steven Lin points out that it&apos;s difficult to determine what needs to be done now.  A single PR blocking tomorrow&apos;s build will be overshadowed by a dozen unresourced RLIs for next quarter&apos;s release. Gwynne suggests a UI that separates issues based on when they are due.  A tabbed UI might be appropriate, or something vertical with accordion sections. "/>
<node CREATED="1233713182838" ID="Freemind_Link_251399704" MODIFIED="1233713191431" TEXT="vineet says:Additionally, is it possible for me to see the stuff which has arrived in the last seven days. For instance my PR count (Open PRs) seems to be jumping too fast and I want to see what is going on here..."/>
</node>
<node CREATED="1233713053220" ID="Freemind_Link_767810855" MODIFIED="1233713111007" TEXT="&quot;Mechanical Turk&quot; prioritization of PRs">
<node CREATED="1233713110987" ID="Freemind_Link_1972930209" MODIFIED="1233716745546" TEXT="Gwynne writes: It might be simpler to allow system test to pick the PRs that they need to have fixed next, put a virtual &quot;star&quot; by them, and have those PRs displayed in a column of &quot;what&apos;s next&quot;. If they (system test) have a long, continuously changing set of rules, it might be simpler for them to order a list that shows up on the responsible manager&apos;s dashboard in a separate column. Javascript to let them drag and drop the PRs into the order they want would be neat-o."/>
</node>
<node CREATED="1233713135678" ID="Freemind_Link_1719720422" MODIFIED="1233716761590" TEXT="Allow different sets of output columns for rules based on either persona, or by personal preference (aassadza, jshaw)."/>
<node CREATED="1233713137829" ID="Freemind_Link_446514879" MODIFIED="1233713161703" TEXT="A matrix-like view for a single release with users (BU teams) as rows, and rules as columns. (jshaw)"/>
<node CREATED="1233713212982" ID="Freemind_Link_662121233" MODIFIED="1233716662230" TEXT="sjain, 2009-01-16  &quot;After dev-complete of an RLI is done, during systest cycle, systest team will file PRs which will have corresponding RLI field populated. Now, to measure health of RLI post-dev-complete, I&apos;d like to know how many PRs have been filed against that RLI.&quot;  He says that links on the Dashboard for &quot;all the PRs filed against your RLIs&quot; and &quot;...against your RLIs in a given release&quot; would be valuable. "/>
<node CREATED="1234905921599" ID="Freemind_Link_189711994" MODIFIED="1234905945845" TEXT="Systest would like to see monitored and suspended PRs (and perhaps others would as well)"/>
</node>
<node CREATED="1233717400136" ID="Freemind_Link_509847294" MODIFIED="1235594574359" POSITION="right" TEXT="Other ideas" VSHIFT="16">
<node CREATED="1233717423775" ID="Freemind_Link_65469687" MODIFIED="1233717445751" TEXT="Export to spreadsheet">
<node CREATED="1233717445720" ID="Freemind_Link_881478000" MODIFIED="1233717551655" TEXT="Yes, I know one of the goals is death to spreadsheets, but there&apos;s a limit to the number of special reports I can build, and excel has a lot of expressiveness and graphing capabilities"/>
</node>
<node CREATED="1236892441388" ID="ID_476342793" MODIFIED="1236892448003" TEXT="Show &quot;new since yesterday&quot;">
<node CREATED="1236892448005" ID="ID_17288635" MODIFIED="1236892456483" TEXT="probably not possible to save state for this"/>
</node>
<node CREATED="1235594647109" ID="Freemind_Link_325151759" MODIFIED="1235594663192" TEXT="Interface with Howdy for trending">
<node CREATED="1235594670265" ID="Freemind_Link_1377130341" MODIFIED="1235594680440" TEXT="Dump data into Howdy daily"/>
<node CREATED="1235594681197" ID="Freemind_Link_951998773" MODIFIED="1235594690216" TEXT="Read existing Howdy metrics"/>
</node>
</node>
<node CREATED="1235594585070" ID="Freemind_Link_1445468974" MODIFIED="1235594591552" POSITION="right" TEXT="Technical Details">
<node COLOR="#000000" CREATED="1233717630234" ID="Freemind_Link_448571806" MODIFIED="1235615681665" TEXT="Replace UI layer with Django">
<node CREATED="1233717642593" ID="Freemind_Link_774285821" MODIFIED="1235594528591" TEXT="This would *really* speed up UI development"/>
<node CREATED="1235705756422" ID="ID_365430742" MODIFIED="1235705777983" TEXT="Allow tight integration with Howdy">
<node CREATED="1235705777985" ID="ID_90789805" MODIFIED="1235705785742" TEXT="Could even merge the codebases"/>
</node>
</node>
<node CREATED="1235594784540" ID="Freemind_Link_842173191" MODIFIED="1235594790569" TEXT="BU / BG data"/>
<node CREATED="1233717403969" ID="Freemind_Link_264039249" MODIFIED="1233717423322" TEXT="API for access by other systems"/>
<node CREATED="1236208228068" ID="ID_1887552729" MODIFIED="1236208252402" TEXT="Knob to control update frequency">
<node CREATED="1236208252423" ID="ID_488837981" MODIFIED="1236208268774" TEXT="Allows for dev system to pause updates, so it can remain on hot standby."/>
</node>
<node CREATED="1236209740538" ID="ID_980172337" MODIFIED="1236209774010" TEXT="Ability for java layer to dump data to filesystem, and py layer to read it">
<node CREATED="1236209774016" ID="ID_317573444" MODIFIED="1236209791839" TEXT="Allows for quick restart of UI"/>
</node>
</node>
<node CREATED="1238628961450" ID="ID_1775121396" MODIFIED="1238628965353" POSITION="right" TEXT="New Data Sources">
<node CREATED="1238628965355" ID="ID_1820420363" MODIFIED="1238628968770" TEXT="Storm"/>
<node CREATED="1238628973783" ID="ID_1216924753" MODIFIED="1238628979106" TEXT="Sisyphus"/>
<node CREATED="1238628980100" ID="ID_193857042" MODIFIED="1238628989650" TEXT="Release Status Page"/>
</node>
<node CREATED="1238607294112" HGAP="15" ID="ID_858822030" MODIFIED="1238612227220" POSITION="right" TEXT="UI" VSHIFT="35">
<node CREATED="1238609842984" ID="ID_374036669" MODIFIED="1238609849870" TEXT="Look n feel changes">
<node CREATED="1238607301514" ID="ID_366776874" MODIFIED="1238609841710" TEXT="Tabbed UI?"/>
<node CREATED="1238609852462" ID="ID_481216377" MODIFIED="1238609870462" TEXT="steal gnatatui l&amp;f?"/>
<node CREATED="1238609871444" ID="ID_1079270847" MODIFIED="1238610011959" TEXT="new jnpr color scheme"/>
</node>
<node CREATED="1238610205629" ID="ID_1477840215" MODIFIED="1238610271408" TEXT="Global UI elements">
<node CREATED="1238610309095" ID="ID_232155457" MODIFIED="1238610311097" TEXT="nav">
<node CREATED="1238610324310" ID="ID_1101020974" MODIFIED="1238610327833" TEXT="static links"/>
<node CREATED="1238611546087" ID="ID_1118162265" MODIFIED="1238611550870" TEXT="input/form">
<node CREATED="1238610390420" ID="ID_1520513751" MODIFIED="1238610397513" TEXT="username / release pair?"/>
<node CREATED="1238610328252" ID="ID_1082501885" MODIFIED="1238611563254" TEXT="dwim?">
<node CREATED="1238611177860" ID="ID_546365832" MODIFIED="1238611184980" TEXT="auto drop-down help"/>
<node CREATED="1238611187675" ID="ID_56227770" MODIFIED="1238611200812" TEXT="should handle redirects to rlis / prs"/>
<node CREATED="1238612523617" ID="ID_132968387" MODIFIED="1238612530914" TEXT="can&apos;t (sensibly) do autocomplete"/>
</node>
</node>
</node>
<node CREATED="1238607323899" ID="ID_1546404863" MODIFIED="1238607334363" TEXT="upcoming milestones"/>
<node CREATED="1238610217600" ID="ID_1075599427" MODIFIED="1238610222359" TEXT="MotD">
<node CREATED="1238610222362" ID="ID_1411807318" MODIFIED="1238610230136" TEXT="global to start with"/>
<node CREATED="1238610230558" ID="ID_1230623927" MODIFIED="1238610258160" TEXT="scoped by VT, dept, BU, reporting, etc."/>
</node>
<node CREATED="1238612172192" ID="ID_640263420" MODIFIED="1238612176408" TEXT="Useful links">
<node CREATED="1238612176410" ID="ID_276265558" MODIFIED="1238612178377" TEXT="footer?"/>
</node>
</node>
<node CREATED="1238607335211" ID="ID_1096399135" MODIFIED="1238607337151" TEXT="agenda">
<node CREATED="1238609105723" ID="ID_659461583" MODIFIED="1238609107539" TEXT="overall"/>
<node CREATED="1238609108640" ID="ID_1151693090" MODIFIED="1238609110875" TEXT="per-release"/>
<node CREATED="1238607308053" ID="ID_528721695" MODIFIED="1238607322947" TEXT="overdue now soon later"/>
</node>
<node CREATED="1238609136913" ID="ID_1070778045" MODIFIED="1238609145195" TEXT="My PRs"/>
<node CREATED="1238612447451" ID="ID_897553793" MODIFIED="1238612454330" TEXT="My notes / TODO"/>
<node CREATED="1238627390507" ID="ID_287472555" MODIFIED="1238627409460" TEXT="Portlet / customizable view"/>
<node CREATED="1238607700549" ID="ID_1813028846" MODIFIED="1238607708921" TEXT="release statuses">
<node CREATED="1238607708922" ID="ID_304473128" MODIFIED="1238607713956" TEXT="tab per release">
<node CREATED="1238611824288" ID="ID_1009775733" MODIFIED="1238611967952" TEXT="Tabs for &quot;active&quot; (P0-P5 not eol)"/>
<node CREATED="1238611833086" ID="ID_1904350193" MODIFIED="1238611982232" TEXT="Dropdown for &quot;not so active&quot; (P0-LL and P5 eoled)"/>
</node>
<node CREATED="1238627997916" ID="ID_747031342" MODIFIED="1238628012574" TEXT="How to handle sisyphus branches that don&apos;t correspond to a release?"/>
<node CREATED="1238607733251" ID="ID_121204714" MODIFIED="1238612127985" TEXT="Branch State: open/closed/locked/etc&#xa;Locked? Why, what is the ETA to open?&#xa;   Toxic? PRs, platforms affected, ETA of PRs&#xa;Closed? Why?&#xa;regression results&#xa;&#xa;last-sane: date of last sane&#xa;last-good: date of last good&#xa;&#xa;dev-start date:&#xa;dev-complete date:&#xa;CAM date:&#xa;&#xa;Planed merges:&#xa;&#xa;Contacts for this branch:&#xa;&#xa;SMALL RLI  branch dates&#xa;MEDIUM RLI branch dates&#xa;LARGE RLI ...&#xa;&#xa;Link to RLI list for this release&#xa;etc....&#xa;">
<node CREATED="1238612115790" ID="ID_1486911074" MODIFIED="1238612125096" TEXT="Same thing is used in the per-release agenda"/>
</node>
</node>
<node CREATED="1238607971294" ID="ID_1474019629" MODIFIED="1238607974405" TEXT="Matrix">
<node CREATED="1238607974406" ID="ID_323029352" MODIFIED="1238607983126" TEXT="old style"/>
<node CREATED="1238607983602" ID="ID_547397916" MODIFIED="1238608005310" TEXT="single release / multiple user (systest style)"/>
<node CREATED="1238608011865" ID="ID_1074800603" MODIFIED="1238608018334" TEXT="&quot;my saved matrices&quot;"/>
<node CREATED="1238610057702" ID="ID_250527966" MODIFIED="1238610063567" TEXT="copy to spreadsheet"/>
</node>
</node>
</node>
</map>
