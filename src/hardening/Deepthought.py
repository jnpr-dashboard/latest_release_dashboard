import config
import subprocess
import logging

class Deepthought:
    @classmethod
    def query(cls, host, table, params, fields):
        logging.info("Deepthought query: %s" % params)

        process = subprocess.Popen([
            "%s/%s" % (config.bin_path, "get-deep-records.pl"),
            host,
            table,
            params
        ] + fields, shell=False, stdout=subprocess.PIPE)
        return process
