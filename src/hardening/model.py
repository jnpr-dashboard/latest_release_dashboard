import config
import logging

from Deepthought import *
from Gnats import *

class ReleaseHardening:
    """
    Represents Release Hardening record.
    """
    RH_FIELDS = [
        { 'field': 'pr_scope',          'data_type': 'Text' },
        { 'field': 'planned_rel',       'data_type': 'Text' },
        { 'field': 'program_release',   'data_type': 'PickList' },
        { 'field': 'hardng_cust',       'data_type': 'MultiPick' },
        { 'field': 'hardng_tech',       'data_type': 'MultiPick' },
        { 'field': 'hardening_source',  'data_type': 'MultiPick' },
    ]
    RH_TABLE = "Release_Hardening"

    @classmethod
    def __getQueryParams(cls):
        return "skipClosed=yes&not_status=not&status_pick=closed"

    @classmethod
    def __getFields(cls):
        return [field['field'] for field in cls.RH_FIELDS]

    @staticmethod
    def __splitMultiPick(val):
        """
        Split MultiPick values into a List
        """
        return ( val[2:].split('\x1d') if val.startswith('\x1c') else val )

    @staticmethod
    def __splitGnatsMultiPick(val):
        """
        Split Gnats MultiPick values into a List
        """
        return ( val.split(':') if ':' in val else val )

    @classmethod
    def query_deepthought(cls):
        host = config.deepthought_host
        table = cls.RH_TABLE
        process = Deepthought.query(host, table, cls.__getQueryParams(), cls.__getFields())

        records = {}
        return_fields = cls.__getFields()
        # Deepthought always returns record_number in 1st position in
        # addition to the requested fields
        return_fields.insert(0, 'record_number')
        while True:
            out = process.stdout.readline()
            # quit the loop if no more data
            if out == '' and process.poll() != None:
                break
            # skip the header
            if out.startswith('record_number'):
                continue
            # process non-empty lines
            if out != '' and out != '\n':
                values = out.split('\x1f')
                record = dict(zip(return_fields, (cls.__splitMultiPick(val) for val in values)))
                records[record['pr_scope']] = record

        logging.info("Deepthought returned %s records." % len(records))
        logging.debug("Records:\n%s" % ("\n".join("%s %s" % (key, records[key]) for key in records.keys())))
        return records

    @classmethod
    def query_gnats(cls, prs):
        """
        Retrieve the state of the specified PRs from Gnats.
        """
        host = config.gnats_host
        port = config.gnats_port
        database = config.gnats_database
        fields = ['Number', 'State', 'Resolution', 'Product', 'Branch', 'Keywords']
        product_index = fields.index('Product')

        process = Gnats.query(host, port, database, prs, fields)

        records = {}
        while True:
            out = process.stdout.readline()
            if out == '' and process.poll() != None:
                break
            if out != '' and out != '\n':
                values = out.split('^')
                if ":" in values[product_index]:
                    products = values[product_index].split(":")
                    logging.debug("Splitting PR %s into %s records by products %s." %
                        (values[fields.index('Number')], len(products), products))
                    for product in products:
                        values[product_index] = product
                        record = dict(zip(fields, (cls.__splitGnatsMultiPick(val) for val in values)))
                        records["%s|%s" % (record['Number'], product)] = record
                else:
                    record = dict(zip(fields, (cls.__splitGnatsMultiPick(val) for val in values)))
                    records[record['Number']] = record

        logging.info("Gnats returned %s records (exploded by product)." % len(records))
        logging.debug("Records:\n%s" % ("\n".join("%s %s" % (key, records[key]) for key in records.keys())))
        missing_prs = [key for key in prs if not key in (records[key]['Number'] for key in records.keys())]
        if len(missing_prs) > 0:
            logging.error("PRs referenced by Release_Hardening records, but missing in GNATS: %s" % missing_prs)
        return records

    @classmethod
    def all(cls):
        # Get Release Hardening records
        hardening_records = cls.query_deepthought()
        # If no records returned by Deepthought ...
        if len(hardening_records) == 0:
            # return empty list
            return []

        # Get state for each PR referenced in Release Hardening table
        pr_state = cls.query_gnats(hardening_records.keys())

        # Merge everything together
        records = {}
        for key in (pr_state.keys()):
            pr = pr_state[key]
            hr = hardening_records[pr['Number']]
            records[key] = dict(list(hr.items()) + list(pr.items()))

        return records
