# Local configuration parameters
import os.path

bin_path = "src/bin" # for testing purposes, when running from project root
if os.path.exists("/opt/dashboard/application/build/WEB-INF/classes/bin"):
    bin_path = "/opt/dashboard/application/build/WEB-INF/classes/bin"

deepthought_host = "deepthought.juniper.net"

gnats_host = "gnats.juniper.net"
gnats_port = "1529"
gnats_database = "default"

oracle_connection = "dash/hackandslash@pswtools"
