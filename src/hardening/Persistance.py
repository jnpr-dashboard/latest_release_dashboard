import config
import logging
import re
from datetime import datetime

class Persistance:
    """
    Persistance mechanism factory.
    """
    @staticmethod
    def save(format, records, opts):
        if format == 'oracle':
            logging.info("Saving the results to Oracle.")
            PersistInOracle.save(records)
        elif format == 'csv':
            logging.info("Saving the results to a CSV file.")
            PersistToCSV.save(records, opts.output_file)
        else:
            pass

class PersistanceBase(object):
    fields = [
        'rel_type',
        'planned_rel',
        'product',
        'source',
        'customer',
        'technology',
        'pr_numbers',
        'keywords',
        'counts',
        'rule_type',
        'modified_on',
    ]

    @classmethod
    def extractMR(cls, release):
        match = re.match('.*(?P<MR>R[0-9]{1,2})', release)
        return match.group('MR') if match is not None else 'Other'

    @classmethod
    def rule_type(cls, record):
        """
        Returns one of the following strings:
            open - if the record is either in 'open', 'analyzed' or 'info' state
            resolved - if the state is either 'closed' or 'feedback', resolution is 'fixed' and branch is not empty
            other - everything else
        """
        if (record['State'] in ['open', 'analyzed', 'info']):
            return 'open'
        elif (record['State'] in ['closed', 'feedback'] and
              record['Resolution'].startswith('fixed') and
              record['Branch'] != ''):
            return 'resolved'
        else:
            return 'other'

    @classmethod
    def translate_fields(cls, records):
        return [{
                'rel_type': cls.extractMR(records[key]['planned_rel']),
                'planned_rel': records[key]['planned_rel'][:10], # TODO: Increase field size
                'product': str(records[key]['Product']),
                'source': ', '.join(records[key]['hardening_source']),
                'customer': ', '.join(records[key]['hardng_cust']),
                'technology': ', '.join(records[key]['hardng_tech']),
                'pr_numbers': records[key]['Number'],
                'keywords': str(records[key]['Keywords']),
                'counts': 1,
                'rule_type': cls.rule_type(records[key]),
                'modified_on': datetime.now(),
            } for key in records.keys()]

class PersistToCSV(PersistanceBase):
    @classmethod
    def save(cls, records, output_file):
        f = open(output_file, 'w')
        f.write("%s\n" % ','.join(cls.fields))
        for record in super(PersistToCSV, cls).translate_fields(records):
            f.write("%s\n" % ','.join([re.sub(r'\n', ' ', re.sub(r',',';',str(record[field]))) for field in cls.fields]))

class PersistInOracle(PersistanceBase):
    """
    Persist data in Oracle
    """
    @classmethod
    def save(cls, records):
        try:
            import cx_Oracle
        except:
            logging.error("Oracle driver not found!")
            exit(-1)

        connection = cx_Oracle.connect(config.oracle_connection)
        # Truncate the table before inserting new records
        truncCursor = connection.cursor()
        truncCursor.execute('TRUNCATE TABLE ui_hardeningreport');

        # Insert new records
        cursor = connection.cursor()
        cursor.prepare('INSERT INTO ui_hardeningreport(%s) VALUES (%s)' %
            (','.join(cls.fields), ','.join(':%s' % f for f in cls.fields)))
        cursor.executemany(None, super(PersistInOracle, cls).translate_fields(records))

        # Commit
        cursor.close()
        truncCursor.close()
        connection.commit()
        connection.close()
