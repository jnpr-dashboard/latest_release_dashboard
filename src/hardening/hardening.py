#!/usr/bin/env python
# encoding: utf-8
"""
hardening.py

Copyright (c) 2012 Juniper Networks. All rights reserved.

Read Deepthought's Release Hardening table. For each records in the table,
for a given TOT release number (11.4), query a PR that the record is
referencing, evaluate its state and calculate the metrics.

The following metrics are calculated:

    Open PRs:       State = [Open, Analyzed, Info]
    Resolved PRs:   State = [Feedback, Closed with Commit (Resolution="fixed", Branch!="")]

"""

import os
import sys
import optparse
import glob
import inspect
import logging

from model import *

parser = None
description = 'Calculate metrics for the Release Hardening reports.'
epilog = 'Available commands:'


class Usage(Exception):
    def __init__(self, msg):
        self.msg = msg

class InvalidOperation(Exception): pass

class RuntimeError(Exception):
    def __init__(self, msg):
        self.msg = msg


# -----------------------------------------------------------------------------
# Operations
# -----------------------------------------------------------------------------

class Operation(object):
    """
    Base class for operations.

    To define new operations, subclass Operation and implement
    an appropriate invoke() method.
    """
    options = ''
    usage = ''
    notes = ''

    def __init__(self, opts):
        self.name = self.__class__.name()
        self.options = opts

    @classmethod
    def name(cls):
        return cls.__name__.lower()

    @classmethod
    def args(operation):
        allArgsStr = inspect.formatargspec(
            *inspect.getargspec(operation.invoke))[1:-1]
        allArgsList = allArgsStr.split(', ')

        return ' '.join(['<%s>' % arg for arg in allArgsList
            if arg not in ['self', 'host', 'vm']])

def getOperationByName(opts, name):
    ## `name` must be a string
    if not hasattr(name, 'endswith'):
        return None

    opClass = opsDict.get(name, None)

    if opClass:
        return opClass(opts)
    else:
        return None

def getOperationFromArgs(opts, args):
    for arg in args:
        operation = getOperationByName(opts, arg)
        if operation: return operation

def processArgs(opts, args):
    return [arg for arg in args if not getOperationByName(opts, arg)]


class Help(Operation):
    notes = 'Display help.'

    def invoke(self):
        lines = []
        lines.append("\n  Available Commands:")

        parser.print_help()

        for key, opClass in opsDict.items():
            if not key.startswith("__"):
                args = opClass.args()
                usageString = "%s %s" % (opClass.name(), opClass.usage or args)
                lines.append('    %-19s %s' % (usageString, opClass.notes))

        print '\n'.join(lines)


# -----------------------------------------------------------------------------
# Your concrete operations
# -----------------------------------------------------------------------------
class Calculate(Operation):
    notes = 'Calculate metrics: Open PRs, Resolved PRs.'

    def invoke(self):
        hardening_records = ReleaseHardening.all()

        if len(hardening_records) > 0 and not self.options.no_persistance:
            from Persistance import Persistance
            Persistance.save(self.options.format, hardening_records, self.options)


# -----------------------------------------------------------------------------
# Main
# -----------------------------------------------------------------------------
globalsDict = dict(globals())
opsDict = dict(
   [(key.lower(), val)
    for key, val in globalsDict.items()
    if hasattr(val, 'invoke')]
   )

def main(argv=None):
    if argv is None:
        argv = sys.argv[1:]
    try:
        global parser
        parser = optparse.OptionParser("usage: %prog [options] command [arguments...]")
        parser.allow_interspersed_args = False
        parser.add_option("-v", "--verbose",
            action="store_true", dest="verbose",
            help="Verbose")
        parser.add_option("-n", "--no-persistance",
            action="store_true", dest="no_persistance",
            help="Do not store the results in the database")

        outputFormatOptGroup = optparse.OptionGroup(parser, 'Output Format Options')
        outputFormatOptGroup.add_option(
            "--output-file",
            action="store", dest="output_file", metavar="OUTPUT",
            help="Output file name")
        outputFormatOptGroup.add_option(
           "--format",
           action="store", dest="format", metavar="FORMAT",
           type="choice", choices=['oracle', 'csv'],
           default='oracle',
           help="Output format (oracle, json, csv, dict)")
        parser.add_option_group(outputFormatOptGroup)

        opts, args = parser.parse_args(argv)

        if opts.format != 'oracle' and not opts.output_file:
            raise Usage("\"--format=%s\" requires \"--output-file=FILE_NAME\"" % opts.format)

        log_level = logging.INFO
        if opts.verbose:
            log_level=logging.DEBUG
        logging.basicConfig(level=log_level,
            format='%(asctime)s %(levelname)-8s %(message)s',
            datefmt='%a, %d %b %Y %H:%M:%S')

        # Perform requested operation
        operation = getOperationFromArgs(opts, args)
        if not operation:
            raise InvalidOperation()

        processedArgs = processArgs(opts, args)
        result = operation.invoke(*processedArgs)

    except Usage, err:
        sys.stderr.write("Usage error: %s\n\n" % str(err.msg))
        return 2

    except RuntimeError, err:
        sys.stderr.write("Runtime error: %s\n\n" % str(err.msg))
        return 3

    except InvalidOperation:
        sys.stderr.write("Invalid operation.\n")
        return 4

if __name__ == "__main__":
    sys.exit(main())
