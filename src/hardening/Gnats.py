import config
import subprocess
import logging

class Gnats:
    @classmethod
    def query(cls, host, port, database, prs, fields):
        format = "\"%s\" %s" % ("^".join(["%s" for i in range(len(fields))]), " ".join(fields))
        logging.info("Retrieving details from GNATS.")

        process = subprocess.Popen([
            "/usr/local/bin/query-pr",
            "--host", host,
            "--port", port,
            "--database", database,
            "-v", "dashboard",
            "--format", format,
            " ".join(prs)
        ], shell=False, stdout=subprocess.PIPE)

        return process
