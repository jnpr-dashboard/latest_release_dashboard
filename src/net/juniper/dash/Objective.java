/**
 * dbergstr@juniper.net, January 3, 2007
 *
 * Copyright (c) 2007, Juniper Networks, Inc.
 * All rights reserved.
 */
package net.juniper.dash;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import flexjson.JSONSerializer;

/**
 * The objectives Rules can work towards.
 *
 * @author dbergstr
 */
public enum Objective {
    P0_EXIT("P0 Exit",
        "Meet requirements for NPI Phase 0 - Concept and Feasibility"),
    P1_EXIT("P1 Exit",
        "Meet requirements for NPI Phase 1 - Planning and Specification"),
    P2_EXIT("P2 Exit",
        "Meet requirements for NPI Phase 2 - Design, Implementation, and Prototype"),
    P3_EXIT("P3 Exit",
        "Meet requirements for NPI Phase 3 - System Test"),
    P4_EXIT("P4 Exit",
        "Meet requirements for NPI Phase 4 - Beta Test and Pilot Build"),
    P5_STABILITY("Product Stability",
        "Ensure product stability during NPI Phase 5: Production"),
    HOLD_TO_SCHEDULE("Hold to Schedule",
        "Hold to JUNOS release schedule (All NPI Phases)"),
    ENSURE_COMMIT_REQUEST("Ensure commit requests",
        "Ensure commit requests are reviewed in a timely manner"),
    ENSURE_COMMIT_REQUEST_EXPIRED("Ensure commit requests expired",
        "Ensure commit requests which have recently expired remain visible in case they should be resurrected"),
    NO_OBJECTIVE("No Clue", "We don't know what this rule is about.");

    String prettyName;
    String description;
    
    Objective(String name, String desc) {
        prettyName = name;
        description = desc;
    }
    
    public String getDescription() {
    		return description;
    }
    
    public String toString() {
        return prettyName;
    }
    
    public static String serializeAll() {
        List<Map<String, Object>> objectives =
            new ArrayList<Map<String, Object>>(Objective.values().length);
        int sort = 0;
        for (Objective o : Objective.values()) {
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("class", "net.juniper.dash.Objective");
            map.put("id", o.name());
            map.put("name", o.prettyName);
            map.put("description", o.description);
            map.put("sort_key", sort++);
            objectives.add(map);
        }
        return new JSONSerializer().serialize(objectives);
    }
}
