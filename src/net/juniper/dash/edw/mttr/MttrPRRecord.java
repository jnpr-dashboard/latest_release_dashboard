package net.juniper.dash.edw.mttr;

public class MttrPRRecord {
	
	private String defect_id; 
	private String scope_nr;
	private String category_cd;
	private String dev_owner;
	private String manager_username;
	private Double mttr_val;
	private Integer case_cnt;
	
	public void setDefect_id(String defect_id){
		this.defect_id = defect_id;
	}
	
	public String getDefect_id() {
		return defect_id;
	}
	
	public void setScope_nr(String scope_nr){
		this.scope_nr = scope_nr;
	}
	
	public String getScope_nr() {
		return scope_nr;
	}	
	
	public void setCategory_cd(String category_cd){
		this.category_cd = category_cd;
	}
	
	public String getCategory_cd() {
		return category_cd;
	}	
	
	public void setDev_owner(String dev_owner){
		this.dev_owner = dev_owner;
	}
	
	public String getDev_owner() {
	    return dev_owner;
	}	
	
	public void setManager_username(String manager_username){
		this.manager_username = manager_username;
	}
	
	public String getManager_username() {
		return manager_username;
	}	
	
	public void setMttr_val(Double mttr_val){
		this.mttr_val = mttr_val;
	}	
	
	public Double getMttr_val() {
		return mttr_val;
	}
	
	public void setCase_cnt(Integer case_cnt){
		this.case_cnt = case_cnt;
	}	
	
	public Integer getCase_cnt() {	
		return case_cnt;
	}
}