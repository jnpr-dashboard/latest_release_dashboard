package net.juniper.dash.edw.mttr;

import java.util.List;
import java.util.Collections;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class MttrController {

	protected final Log logger = LogFactory.getLog(getClass());
   
	@Autowired
    MttrService service;
    
    @RequestMapping(value="/mttr/all/{username}", method=RequestMethod.GET)
    @ResponseBody 
    public List<MttrRecord> getMttrRecords(@PathVariable String username) {
  		
  		List<MttrRecord> lst = service.getMttrRecords(username);
  		if (lst != null){
  			return lst;
  		}
	    return Collections.EMPTY_LIST;
    } 
    
    @RequestMapping(value="/mttr/pr/{metric_name}/{username}", method=RequestMethod.GET)
    @ResponseBody 
    public List<MttrPRRecord> getPRRecords(@PathVariable String metric_name, @PathVariable String username) {
  		
  		List<MttrPRRecord> lst = service.getPRRecords(metric_name, username);
  		if (lst != null){
  			return lst;
  		}
	    return Collections.EMPTY_LIST;
    }  

    @RequestMapping(value="/mttr/last-update", method=RequestMethod.GET)
    @ResponseBody
    public List<MttrLastRefreshRecord> getLastRefreshRecord() {

                List<MttrLastRefreshRecord> lst = service.getLastRefreshRecord();
                if (lst != null){
                        return lst;
                }
            return Collections.EMPTY_LIST;
    }

}
