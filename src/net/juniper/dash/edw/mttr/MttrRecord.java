package net.juniper.dash.edw.mttr;

import java.io.Serializable;

public class MttrRecord implements Serializable {
	private String manager_username;
	private String username;
	private Double mttr_roll_mth_val;
	private Double mttr_cur_mth_val;
	private Double mttr_cur_roll_qtr_val;
	private Double mttr_last_roll_qtr_val;
	private Integer cur_qtr_pr_cnt;
	private Integer cur_qtr_case_cnt;
	private Double mttr_roll_mth_cl1_val;
	private Double mttr_roll_mth_cl2_val;
	private Double pttr_val;
	private Integer open_cl1_cl2_pr_cnt;
	private Integer open_cl1_cl2_case_cnt;
	private Integer untouched_pr_wk_cnt;
        private Double mttr_target_val;
	private String edw_update_time;	
	
	public String getManager_username() {
		return this.manager_username;
	}
	
	public void setManager_username(String manager_username){
		this.manager_username = manager_username;
	}
	
	public String getUsername() {
		return this.username;
	}
	
	public void setUsername(String username){
		this.username = username;
	}
	
	public Double getMttr_roll_mth_val() {
		return this.mttr_roll_mth_val;
	}
	
	public void setMttr_roll_mth_val(Double mttr_roll_mth_val){
		this.mttr_roll_mth_val = mttr_roll_mth_val;
	}
	
	public Double getMttr_cur_mth_val() {
		return this.mttr_cur_mth_val;
	}
	
	public void setMttr_cur_mth_val(Double mttr_cur_mth_val){
		this.mttr_cur_mth_val = mttr_cur_mth_val;
	}
	
	public Double getMttr_cur_roll_qtr_val() {
		return this.mttr_cur_roll_qtr_val;
	}
	
	public void setMttr_cur_roll_qtr_val(Double mttr_cur_roll_qtr_val){
		this.mttr_cur_roll_qtr_val = mttr_cur_roll_qtr_val;
	}
	
	public Double getMttr_last_roll_qtr_val() {
		return this.mttr_last_roll_qtr_val;
	}
	
	public void setMttr_last_roll_qtr_val(Double mttr_last_roll_qtr_val){
		this.mttr_last_roll_qtr_val = mttr_last_roll_qtr_val;
	}
	
	public Integer getCur_qtr_pr_cnt() {
		return this.cur_qtr_pr_cnt;
	}
	
	public void setCur_qtr_pr_cnt(Integer cur_qtr_pr_cnt){
		this.cur_qtr_pr_cnt = cur_qtr_pr_cnt;
	}	
	
	public Integer getCur_qtr_case_cnt() {
		return this.cur_qtr_case_cnt;
	}
	
	public void setCur_qtr_case_cnt(Integer cur_qtr_case_cnt){
		this.cur_qtr_case_cnt = cur_qtr_case_cnt;
	}
	
	public Double getMttr_roll_mth_cl1_val() {
		return this.mttr_roll_mth_cl1_val;
	}
	
	public void setMttr_roll_mth_cl1_val(Double mttr_roll_mth_cl1_val){
		this.mttr_roll_mth_cl1_val = mttr_roll_mth_cl1_val;
	}		
	
	public Double getMttr_roll_mth_cl2_val() {
		return this.mttr_roll_mth_cl2_val;
	}
	
	public void setMttr_roll_mth_cl2_val(Double mttr_roll_mth_cl2_val){
		this.mttr_roll_mth_cl2_val = mttr_roll_mth_cl2_val;
	}
	
	public Double getPttr_val() {
		return this.pttr_val;
	}
	
	public void setPttr_val(Double pttr_val){
		this.pttr_val = pttr_val;
	}
	
	public Integer getOpen_cl1_cl2_pr_cnt() {
		return this.open_cl1_cl2_pr_cnt;
	}
	
	public void setOpen_cl1_cl2_pr_cnt(Integer open_cl1_cl2_pr_cnt){
		this.open_cl1_cl2_pr_cnt = open_cl1_cl2_pr_cnt;
	}
	
	public Integer getOpen_cl1_cl2_case_cnt() {
		return this.open_cl1_cl2_case_cnt;
	}
	
	public void setOpen_cl1_cl2_case_cnt(Integer open_cl1_cl2_case_cnt){
		this.open_cl1_cl2_case_cnt = open_cl1_cl2_case_cnt;
	}
	
	public Integer getUntouched_pr_wk_cnt() {
		return this.untouched_pr_wk_cnt;
	}
	
	public void setUntouched_pr_wk_cnt(Integer untouched_pr_wk_cnt){
		this.untouched_pr_wk_cnt = untouched_pr_wk_cnt;
	}

        public void setEdw_update_time(String edw_update_time){
                this.edw_update_time = edw_update_time;
        }
        public String getEdw_update_time() {
                return this.edw_update_time;
        }	
        public void setMttr_target_val(Double mttr_target_val){
                this.mttr_target_val = mttr_target_val;
        }
        public Double getMttr_target_val() {
                return this.mttr_target_val;
        }
}
