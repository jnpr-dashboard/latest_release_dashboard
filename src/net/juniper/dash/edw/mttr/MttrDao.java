package net.juniper.dash.edw.mttr;

import java.sql.ResultSet;
import java.sql.SQLException;
import javax.sql.DataSource;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

@Repository
public class MttrDao {
	
    protected final Log logger = LogFactory.getLog(getClass());
    
    @Autowired
    @Qualifier("jdbcTemplate")
    private JdbcTemplate jdbcTemplate;

    public void setDataSource(DataSource dataSource) { 	
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }
   
    public List<MttrRecord> getMttrRecords(String userName) {
        
        String fields = "lower(m.manager_username), "
		           +"lower(m.username), "
		           +"m.mttr_roll_mth_val, "
		           +"m.mttr_cur_mth_val, "
                   +"m.mttr_cur_roll_qtr_val, "
                   +"m.mttr_last_roll_qtr_val, "
                   +"m.cur_qtr_pr_cnt, "
                   +"m.cur_qtr_case_cnt, "
                   +"m.mttr_roll_mth_cl1_val, "
                   +"m.mttr_roll_mth_cl2_val, "
                   +"m.pttr_val, "
                   +"m.open_cl1_cl2_pr_cnt, "
                   +"m.open_cl1_cl2_case_cnt, "
                   +"m.untouched_pr_wk_cnt, "
                   +"m.mttr_target_val, " 
                   +"(m.edw_update_time - cast(g.gmt_offset_val as interval hour)) as edw_update_time ";
    String tables = "from edw.mttr_metrics m join edw.mtdt_gmt_offset g on 1=1 ";
    String sql = "sel " + fields + tables + "where m.manager_username='" + userName + "' "    		
               + "and m.dashboard_ind='Y'";        
        return jdbcTemplate.query(sql, new MttrRecordMapper());
    }
    
    private class MttrRecordMapper implements RowMapper<MttrRecord> {

        public MttrRecord mapRow(ResultSet rs, int rowNum) throws SQLException {

        	MttrRecord m = new MttrRecord();
            m.setManager_username(rs.getString("manager_username")); 
            m.setUsername(rs.getString("username")); 
            m.setMttr_roll_mth_val(rs.getDouble("mttr_roll_mth_val"));
            m.setMttr_cur_mth_val(rs.getDouble("mttr_cur_mth_val")); 
            m.setMttr_cur_roll_qtr_val(rs.getDouble("mttr_cur_roll_qtr_val")); 
            m.setMttr_last_roll_qtr_val(rs.getDouble("mttr_last_roll_qtr_val"));
            m.setCur_qtr_pr_cnt(rs.getInt("cur_qtr_pr_cnt")); 
            m.setCur_qtr_case_cnt(rs.getInt("cur_qtr_case_cnt"));
            m.setMttr_roll_mth_cl1_val(rs.getDouble("mttr_roll_mth_cl1_val")); 
            m.setMttr_roll_mth_cl2_val(rs.getDouble("mttr_roll_mth_cl2_val")); 
            m.setPttr_val(rs.getDouble("pttr_val"));           
            m.setOpen_cl1_cl2_pr_cnt(rs.getInt("open_cl1_cl2_pr_cnt")); 
            m.setOpen_cl1_cl2_case_cnt(rs.getInt("open_cl1_cl2_case_cnt"));
            m.setUntouched_pr_wk_cnt(rs.getInt("untouched_pr_wk_cnt"));
            m.setMttr_target_val(rs.getDouble("mttr_target_val")); 
            m.setEdw_update_time(rs.getString("edw_update_time"));	                
            return m;
        }
    }
    
    public List<MttrPRRecord> getPRRecords(String metric_name, String manager_username) {
    	String fields = "defect_id, "
    			      + "scope_nr, "
    			      + "category_cd, "
    			      + "dev_owner, "
    			      + "manager_username, "
    			      + "mttr_val, "
    			      + "case_cnt ";
    	String tables = "from edw.mttr_pr ";
    	String sql = "sel " + fields + tables + "where metric_name='" + metric_name + "' and manager_username='" + manager_username + "'";
    	
    	return jdbcTemplate.query(sql, new PRRecordMapper());
    }
    
    private class PRRecordMapper implements RowMapper<MttrPRRecord> {

        public MttrPRRecord mapRow(ResultSet rs, int rowNum) throws SQLException {

        	MttrPRRecord p = new MttrPRRecord();
            p.setDefect_id(rs.getString("defect_id")); 
            p.setScope_nr(rs.getString("scope_nr")); 
            p.setCategory_cd(rs.getString("category_cd")); 
            p.setDev_owner(rs.getString("dev_owner")); 
            p.setManager_username(rs.getString("manager_username")); 
            p.setMttr_val(rs.getDouble("mttr_val"));           
            p.setCase_cnt(rs.getInt("case_cnt"));
            
            return p;
        }
    }    

   public List<MttrLastRefreshRecord> getLastRefreshRecord() {
        String sql = "sel max(last_refresh_ts) from edw.its_quality";

        return jdbcTemplate.query(sql, new MttrLastRefreshRecordMapper());
    }

    private class MttrLastRefreshRecordMapper implements RowMapper<MttrLastRefreshRecord> {

        public MttrLastRefreshRecord mapRow(ResultSet rs, int rowNum) throws SQLException {

                MttrLastRefreshRecord ls = new MttrLastRefreshRecord();
            ls.setLast_refresh_ts(rs.getString("last_refresh_ts"));
            return ls;
        }
    }
 

  
}
