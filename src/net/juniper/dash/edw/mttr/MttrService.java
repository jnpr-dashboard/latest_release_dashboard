package net.juniper.dash.edw.mttr; 

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MttrService {
	
	@Autowired
	private MttrDao mttrDao;	
	
	public List<MttrRecord> getMttrRecords(String username){		
		return mttrDao.getMttrRecords(username);
	}
	
	public List<MttrPRRecord> getPRRecords(String metric_name, String username){		
		return mttrDao.getPRRecords(metric_name, username);
	}
        public List<MttrLastRefreshRecord> getLastRefreshRecord(){
                return mttrDao.getLastRefreshRecord();
        }	
	
}
