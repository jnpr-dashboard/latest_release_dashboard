package net.juniper.dash.edw.mttr;

public class MttrLastRefreshRecord {
	
	private String last_refresh_ts; 
	
	public void setLast_refresh_ts(String last_refresh_ts){
		this.last_refresh_ts = last_refresh_ts;
	}

        public String getLast_refresh_ts() {
                return last_refresh_ts;
        }
	
}
