/**
 * dbergstr@juniper.net, Oct 27, 2006
 *
 * Copyright (c) 2006, Juniper Networks, Inc.
 * All rights reserved.
 */
package net.juniper.dash.data;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import net.juniper.dash.DashboardException;
import net.juniper.dash.Refresher;
import net.juniper.dash.Repository;
import net.juniper.dash.SleepyTimes;
import net.juniper.dash.Refresher.TimeoutException;

import org.apache.log4j.Logger;
import org.drools.WorkingMemory;

/**
 * PR DataSource.
 *
 * @author dbergstr
 */
public class ReviewGroups extends DataSource {

    static Logger logger = Logger.getLogger(ReviewGroups.class.getName());

    private Map<String, ReviewGroup> records = new HashMap<String, ReviewGroup>();

    private String reloaderPath;

    public ReviewGroups(String name) {
        super(name, "revgroups", "ReviewGroup", "Review Group", "Review Groups");
    }

    public void init() throws DashboardException {
//        this.workingMemory = workingMem;
    	loaderProps = null;
    	propFileName = "";
        reloaderPath = Repository.createPath(Repository.BIN_DIR , "get-review-groups.pl", absolutePropPath);
        File f = new File(reloaderPath);
        if ( ! (f.exists() && f.canRead())) {
            // No way to test for executable, so we still might blow up later.
            throw new DashboardException("No external record reloader for" +
                    " ReviewGroups at path " + reloaderPath);
        }
        List<String> cmd = new ArrayList<String>();
        cmd.add(reloaderPath);
        processBuilder = new ProcessBuilder(cmd);

        /* We need all the Users to be loaded before we can process our
         * raw data. */
        dependsOn = USERSOURCE;
    }

    @Override
    protected List<String[]> getRawRecordData(Refresher refresher)
            throws DashboardException, TimeoutException {
        List<String> cmd = new ArrayList<String>();
        cmd.add(reloaderPath);
        cmd.addAll(REVIEWTRACKER.getReviewGroups(refresher));
        processBuilder.command(cmd);
        return refresher.getDelimitedOutput(processBuilder,
            SleepyTimes.EXTERNAL_RELOADER_TIMEOUT.t());
    }

    /**
     * We don't actually assert ReviewGroup records into the working memory.
     *
     * This just performs the bookkeeping of maintaining a current list of
     * groups.
     */
    @Override
    @SuppressWarnings("unused")
    protected void reconcileAssertedRecords(Refresher refresher,
            Map<String, Record> currentRecords, Collection<Record> toModify,
            Collection<Record> toAssert) {
        for (String id : new HashSet<String>(getRecords().keySet())) {
            if (! currentRecords.containsKey(id)) {
                    removeRecord(id);
            }
        }
        refresher.checkIfKilled();
    }

    @Override
    protected Record constructRecord(String[] fields) throws DashboardException {
        return new ReviewGroup(fields, this);
    }

    @Override
    public Record getRecord(String id) {
        return records.get(id);
    }

    @Override
    public Map<String, Record> getRecords() {
        return new HashMap<String, Record>(records);
    }

    @Override
    protected void removeRecord(String number) {
        records.remove(number);
    }

    @Override
    protected void addRecord(Record record) {
        records.put(record.getId(), (ReviewGroup) record);
    }

    @Override
    protected boolean hasRecord(String id) {
        return records.containsKey(id);
    }
}
