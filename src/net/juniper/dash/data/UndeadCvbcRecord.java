/**
 * self-populating value bean for a PR.
 *
 * Dirk Bergstrom, dirk@juniper.net, 2008-10-21
 *
 * Copyright (c) 2008, Juniper Networks, Inc.
 * All rights reserved.
 */
package net.juniper.dash.data;

import java.util.ArrayList;
import java.util.Date;
import java.util.Set;
import java.util.HashSet;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net.juniper.dash.DashboardException;

/**
 *  A JUNOS PR where Cust-Visible-Behavior-Changed is "yes" or
 *  "yes-ready-for-review", and state is pretty much anything (including
 *  closed).  Some of them may not actually be undead, but the name sounds good.
 */
public class UndeadCvbcRecord extends Record {

    public static final String queryExpression = "product[group]==\"junos\" & " +
        "( cust-visible-behavior-changed==\"yes\" | " +
        "cust-visible-behavior-changed==\"yes-ready-for-review\" )";

    private static Pattern releasePat = Pattern.compile(RELEASE_REGEX,
            Pattern.CASE_INSENSITIVE | Pattern.COMMENTS);
    
    private static final String[] fieldList = new String[] {
        "number",
        "last-modified",
        "arrival-date",
        "cust-visible-behavior-changed",
        "techpubs-owner",
        "synopsis",
        // Added for backlog PR
        "planned-release",
        // "severity",
        "class",
        "functional-area",
        "category",
        "reported-in",
        "state",
        
    };

 // Backlog PR stuff
    private boolean possibleBacklog=false;
    private boolean possibleForwardViewBacklog=false;
 // End Backlog PR stuff    
    
    public static String[] getFieldList() {
        return fieldList;
    }
    private Date last_modified;
    private String last_modified_string;
    private long last_modified_epoch;
    private String real_techpubs_owner;
	private String techpubs_owner;
    private String cust_visible_behavior_changed;
    private String synopsis;
    private String planned_release;
    private String clazz;
    private Integer clazz_ordinal;
    private String reported_in;
    private String category;
    private Integer category_ordinal;
    private String categoryTechpubOwner;
    private String functional_area;
    private String state;
    private String backlog_pr_techpub_owner;
    
	private String containingRecordId;
    
	public UndeadCvbcRecord(String[] values, DataSource dataSource)
            throws DashboardException {
        super(values, dataSource);
        populate(values);
    }

    public Date getLast_modified() {
        return last_modified;
    }
    public String getLast_modified_string() {
        return last_modified_string;
    }
    public long getLast_modified_epoch() {
        return last_modified_epoch;
    }

    public String getCust_visible_behavior_changed() {
        return cust_visible_behavior_changed;
    }

    public String getReal_techpubs_owner() {
		return real_techpubs_owner;
	}

    public String getTechpubs_owner() {
        return techpubs_owner;
    }

    public String getPlanned_release() {
        return planned_release;
    }
    public String getClazz() {
        return clazz;
    }
    public Integer getClazz_ordinal(){
    	return clazz_ordinal;
    }
    
    public String getReported_in() {
        return reported_in;
    }
    
    public String getCategory(){
    	return category;
    }
    public Integer getCategory_ordinal(){
    	return category_ordinal;
    }
    public String getCategory_spoc(){
    	return categoryTechpubOwner;
    }

    public String getFunctional_area() {
		return functional_area;
	}

    public String getBacklog_pr_techpub_owner() {
		return backlog_pr_techpub_owner;
	}

    /**
	 * @return if this PR is possible backlog PR
	 */
	public boolean isPossible_backlog(){
		return possibleBacklog;
	}

    /**
     * @return if this PR is possible Future backlog PR
     */
    public boolean isPossible_forward_view_backlog(){
        return possibleForwardViewBacklog;
    }    
    
    public boolean populate(String[] values) throws DashboardException {
        if (null == values) {
            throw new DashboardException("Null input.");
        } else if (values.length != fieldList.length + 1) {
            // values always comes with a throwaway on the end
            throw new DashboardException("Wrong number of elements: Got " +
                values.length + ", expected " + (fieldList.length + 1));
        }
        boolean changed = false;
        List<String> changed_field = new ArrayList<String>();
        lastUpdated = new Date();
        String lastModifiedString = values[1];
        if (lastModifiedString.trim().length() == 0) {
            // Never been modified, use arrival-date
            lastModifiedString = values[2];
        }
        Date newLastModified = dataSource.parseDate(lastModifiedString);
        if (! newLastModified.equals(lastModified)) {
            lastModified = newLastModified;
            changed = true;
            changed_field.add("LastModified");
        }

        last_modified = lastModified;
        last_modified_epoch = last_modified.getTime();
        last_modified_string = dataSource.yyyymmdd(last_modified);

        containingRecordId = findPRNum(id);
        if (! values[3].equals(cust_visible_behavior_changed)) {
            cust_visible_behavior_changed = values[3].toLowerCase();
            changed = true;
            changed_field.add("CVBC");
        }
        if (! values[4].equals(real_techpubs_owner)) {
        	real_techpubs_owner = values[4];
            changed = true;
            changed_field.add("real_techpubs_owner");
        }
        if (! values[5].equals(synopsis)) {
            synopsis= values[5];
            changed = true;
            changed_field.add("synopsis");
        }
        
        if (! values[6].equals(planned_release)){
        	planned_release = values[6];
        	changed = true;
            changed_field.add("planned-release");
        }
        if (! values[7].equals(clazz)){
        	clazz = values[7];
        	clazz_ordinal = dataSource.picklistOrdinal("clazz", clazz);
        	changed = true;
            changed_field.add("clazz");
        }
        if (! values[8].equals(functional_area)){
        	functional_area = values[8];
        	changed = true;
            changed_field.add("functional_area");
        }
        if (! values[9].equals(category)){
        	category = values[9];
        	category_ordinal = dataSource.picklistOrdinal("category",category);
        	changed = true;
            changed_field.add("category");
        }
        if (! values[10].equals(reported_in)){
        	reported_in = values[10];
        	changed = true;
            changed_field.add("reported_in");
        }
        if (! values[11].equals(state)){
        	state = values[11];
        	changed = true;
            changed_field.add("state");
        }

        
        /**
         * For category Techpub-owner, there is a case where it will return some email alias
         *   so we will use the category SPOC as the last resort
         */
        String categorySPOC = DataSource.CATEGORY_CONTACTS.getContacts().get(category);
        if (null == categorySPOC){
        	categorySPOC = "deprecated_category";
        } else if (!DataSource.USERSOURCE.getRealUsernames().contains(categorySPOC) &&
                !DataSource.USERSOURCE.getUserNames().contains(categorySPOC)) {
        	// SPOC Is non_user
        	categorySPOC = "non_user";
        }
        
        String categoryTechpubOwnerOld = categoryTechpubOwner;
        categoryTechpubOwner = DataSource.CATEGORY_CONTACTS.getTechpub_contacts().get(category);
        if (null == categoryTechpubOwner ) {
            categoryTechpubOwner = "deprecated_category";
        } else if (!DataSource.USERSOURCE.getRealUsernames().contains(categoryTechpubOwner) &&
	                !DataSource.USERSOURCE.getUserNames().contains(categoryTechpubOwner)) {
          	// Techpub owner is not a real person so we use the category SPOC
       		categoryTechpubOwner = categorySPOC;
        }
        changed |= ! categoryTechpubOwner.equals(categoryTechpubOwnerOld);
        if (changed){
        	changed_field.add("category TechPub Owner");
        }

        if (categoryTechpubOwner.length() > 0 &&
                ! DataSource.USERSOURCE.getRealUsernames().contains(real_techpubs_owner)) {
                /* The techpubs-owner party isn't a real person.  We'll  pretend that
                 * it's the category techpub Owner contact. */
                changed |= ! categoryTechpubOwner.equals(techpubs_owner);
                techpubs_owner = categoryTechpubOwner;
        } else {
        	changed |= ! real_techpubs_owner.equals(techpubs_owner);
        	techpubs_owner = real_techpubs_owner;
        }
        if (changed){
        	changed_field.add("techpubs owner");
        }

        /**
         * Doc & CVBC Backlog PR checks
         * Determine the owner of DOC / CVBC Backlog PR by using this logic:
         * - techpub-owner
         * - IF techpub-owner is EMPTY/INVALID/NON_JUNOS_RESPONSIBLE THEN look to backlog_pr_dev_owner
         */
        String old_backlog_pr_techpubs_owner = backlog_pr_techpub_owner;
        backlog_pr_techpub_owner = techpubs_owner;
        if (DataSource.USERSOURCE.getRealUsernames().contains(techpubs_owner) && 
                ! DataSource.USERSOURCE.getUserNames().contains(techpubs_owner)) {
            backlog_pr_techpub_owner = categoryTechpubOwner;
        } else {
            if ((techpubs_owner.trim().length() > 0) & (! techpubs_owner.equals("-"))){
                if (!DataSource.USERSOURCE.getRealUsernames().contains(techpubs_owner) &&
                        !DataSource.USERSOURCE.getUserNames().contains(techpubs_owner)) {
                    backlog_pr_techpub_owner = categoryTechpubOwner;
                } else {
                    backlog_pr_techpub_owner = techpubs_owner;
                }
            } else {
                backlog_pr_techpub_owner = categoryTechpubOwner;
            }
        }
        if (! backlog_pr_techpub_owner.equals(old_backlog_pr_techpubs_owner)){
            changed = true;
        }
        if (changed){
        	changed_field.add("backlogPR techpub owner");
        }
        
        /*
         * Determine if the PR is possible a backlog PR
         * the flag try to mimic this expression
         * (release=="" & reported-in~"(^| )('active_shipped_releases')") | release="'active_shipped_releases'")'
         * 
         * We run this everytime since the changed field might not affect the active shipped release
         */
        String backlog_release_value = "";
        if ((planned_release.trim().length() > 0) & (!planned_release.equals("-"))) {
            backlog_release_value = planned_release;
        }else {
            backlog_release_value = reported_in;
        }
        Pattern pat = releasePat;
        boolean old_possibleBacklog = possibleBacklog;
        boolean old_possibleForwardViewBacklog = possibleForwardViewBacklog;
        possibleBacklog = false;
        possibleForwardViewBacklog = false;
        for (String rel : backlog_release_value.split("\\s+")) {
            Matcher mat_backlog = pat.matcher(rel);
            if (mat_backlog.lookingAt()) {
                String mDotN = mat_backlog.group(1);
                String type = "";
                type = mat_backlog.group(2); // need the type for cdm releases
                if (type != null ){
                	type = type.toUpperCase();
                	if (type.startsWith("IB")){
						//mDotN += type;
                	}
                } 
                if (DataSource.RELEASES.getShippedReleaseIds().contains(mDotN)) {
                	possibleBacklog = true;
                } else {
                	possibleBacklog = false;
                }
                if (DataSource.RELEASES.getFutureReleaseId().equalsIgnoreCase(mDotN)) {
                    possibleForwardViewBacklog = true;
                } else {
                    possibleForwardViewBacklog = false;
                }
            }
        }
        if (!(old_possibleBacklog == possibleBacklog) ||
        	!(old_possibleForwardViewBacklog == possibleForwardViewBacklog)){
        	changed = true;
        	changed_field.add("Possible Backlog");
        }        
        
        if (changed) {
            summary = new String[] {
                trimString(synopsis, maxSummarySynopsisLength),
                last_modified_string
            };
            calculateHashCode();
        }
      
        valid_techpubs_owners.add(techpubs_owner);
        valid_backlog_pr_techpub_owners.add(backlog_pr_techpub_owner);        

        return changed;
    }

    @Override
    public String dump() {
        StringBuilder sb = dumpCommon();
        sb.append("last_modified => \"");
        sb.append(last_modified);
        sb.append("\"\nRecord Number=> \"");
        sb.append(id);
        sb.append("\"\nlast_modified_string => \"");
        sb.append(last_modified_string);
        sb.append("\"\nlast_modified_epoch => \"");
        sb.append(last_modified_epoch);
        sb.append("\"\ncust_visible_behavior_changed => \"");
        sb.append(cust_visible_behavior_changed);
        sb.append("\"\ntechpubs_owner => \"");
        sb.append(techpubs_owner);
        sb.append("\"\n");

        return sb.toString();
    }

    /**
     * Find the PR number given either number-scope or number.
     */
    protected static String findPRNum(String idOrScope) {
        if (idOrScope.indexOf("-") > 0) {
            return idOrScope.substring(0, idOrScope.lastIndexOf("-"));
        } else {
            return idOrScope;
        }
    }
    
	public String getContainingRecordId() {
		// TODO Auto-generated method stub
		return containingRecordId;
	}

	public String getState() {
		// TODO Auto-generated method stub
		return state;
	}
	
    //Performance tuning for user filters in rules
    private static Set<String> valid_techpubs_owners = new HashSet<String>();
    private static Set<String> valid_backlog_pr_techpub_owners = new HashSet<String>();
    
    public static void initValidUserList() {
    	valid_techpubs_owners.clear();
    	valid_backlog_pr_techpub_owners.clear();
    }
    
    public static Set<String> getValidUsers(String user) {
    	Set<String> s =  new HashSet<String>();
    	if (user.contains(":backlog_pr_techpub_owners")) {
    		s.addAll(valid_backlog_pr_techpub_owners);
    	} 
        if (user.contains(":techpubs_owners")) {
        	s.addAll(valid_techpubs_owners);
    	}
        return s;
    }
}
