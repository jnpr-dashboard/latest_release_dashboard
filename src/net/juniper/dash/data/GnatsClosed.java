/**
 * dbergstr@juniper.net, Oct 27, 2006
 *
 * Copyright (c) 2006, Juniper Networks, Inc.
 * All rights reserved.
 */
package net.juniper.dash.data;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.HashSet;
import java.util.Set;

import net.juniper.dash.DashboardException;
import net.juniper.dash.Refresher;
import net.juniper.dash.Repository;
import net.juniper.dash.Refresher.TimeoutException;

import org.apache.log4j.Logger;
import org.drools.WorkingMemory;

import flexjson.JSONSerializer;

/**
 * PR DataSource.
 *
 * @author dbergstr
 */
public class GnatsClosed extends DataSource {

    static Logger logger = Logger.getLogger(GnatsClosed.class.getName());

    private Map<String, ClosedPRRecord> records = new HashMap<String, ClosedPRRecord>();
    private JSONSerializer recordSerializer;

    // Used by both Gnats and CategoryContacts
    String port;
    String database;
    String query_prPath;
    String extra_opts;
    String categorySubfields;

    public GnatsClosed(String name) {
        super(name, "gnats_closed", "ClosedPRRecord", "ClosedPR", "ClosedPRs");
        // Mon Jun 26 12:16:19 -0700 2006
        dateParser = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzzz yyyy");
        dateParser.setLenient(false);
        recordSerializer = new JSONSerializer();
    }

    public void init() throws DashboardException {
        propFileName = Repository.createPath(Repository.dash_special_directory, "gnats.properties", absolutePropPath);
        loaderProps =
            Repository.readProperties(propFileName,absolutePropPath);
        host = loaderProps.getProperty("host");
        database = loaderProps.getProperty("database");
        port = loaderProps.getProperty("port");
        webAppPath = loaderProps.getProperty("web_app_path");
        categorySubfields = loaderProps.getProperty("category_subfields");

        query_prPath = loaderProps.getProperty("query_pr");
        extra_opts = loaderProps.getProperty("extra_opts");
        File f = new File(query_prPath);
        if ( ! (f.exists() && f.canRead())) {
            // No way to test for executable, so we still might blow up later.
            throw new DashboardException("Can't find query-pr at path " +
                query_prPath);
        }
        List<String> cmd = new ArrayList<String>();
        cmd.add(query_prPath);
        cmd.add(extra_opts);
        cmd.add("--database=" + database);
        cmd.add("--host=" + host);
        cmd.add("--port=" + port);
        
        // It will use the debug expression in testing mode, or it will pick up the 
        // necessary release from /opt/dashboard/special/dash.properties (prfix_releases)
        String expr = Repository.testingMode ? 
        		ClosedPRRecord.debugQueryExpression : ClosedPRRecord.getExpressions();
//        String expr = ClosedPRRecord.getExpressions();

        // --expr='!(state=="closed" | state=="suspended") & (product[os]=="junos" | product[os]=="")' \
        // --format='"%s\037%s\037..." number etc....'
        cmd.add("--expr=" + expr);
        StringBuilder format = new StringBuilder("--format=\"");
        StringBuilder formatFields = new StringBuilder();
        for (String fld : ClosedPRRecord.getFieldList()) {
            format.append("%s");
            format.append(Repository.FIELD_SEPARATOR);
            formatFields.append(fld);
            formatFields.append(" ");
        }
        format.append(Repository.RECORD_SEPARATOR);
        format.append("\" ");
        format.append(formatFields);
        format.append("");
        cmd.add(format.toString());
        processBuilder = new ProcessBuilder(cmd);
        dependsOn = USERSOURCE;
    }
    
    protected Record constructRecord(String[] fields) throws DashboardException {
        return new ClosedPRRecord(fields, this);
    }

    @Override
    public Record getRecord(String id) {
        return records.get(id);
    }

    @Override
    public Map<String, Record> getRecords() {
        return new HashMap<String, Record>(records);
    }

    @Override
    protected void removeRecord(String number) {
        records.remove(number);
    }

    @Override
    protected void addRecord(Record record) {
        records.put(record.getId(), (ClosedPRRecord) record);
    }

    @Override
    protected boolean hasRecord(String id) {
        return records.containsKey(id);
    }
    //Performance tuning
    @Override
    protected List<String[]> getRawRecordData(Refresher refresher) throws DashboardException, TimeoutException {    	
    	ClosedPRRecord.initValidUserList();  	
        return super.getRawRecordData(refresher);
    }    
}
