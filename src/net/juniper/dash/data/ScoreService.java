/**
 * smulyono@juniper.net, Sep 14, 2010
 *
 * Holds the Services / Logic layer of the Score object
 *
 * Differentiate the 'Special' counting into this layer, since
 * they are so 'special' which is not look right to be stuffed
 * into Score.java
 *
 * Copyright (c) 2010, Juniper Networks, Inc.
 * All rights reserved.
 */
package net.juniper.dash.data;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import net.juniper.dash.Repository;
import net.juniper.dash.Rule;

import org.apache.log4j.Logger;

import flexjson.JSON;

public abstract class ScoreService {
    static Logger logger = Logger.getLogger(Record.class.getName());

    public static final ScoreService NO_SCORE = new Score();

    protected int daysLeft = 0;

    protected boolean recordsChanged = true;

    protected int count = 0;

    protected int countFeedback = 0;

    protected int countOutstanding = 0;

    protected int countSuspended = 0;

    protected int countMonitored = 0;

    protected int multiplier = 0;

    protected int score = 0;

    private Map<String, Integer> other_scores = new HashMap<String, Integer>();

    protected String scoreType = "default";

    protected List<String> recordNumbers = null;
    protected List<String> uniqueRevRecs = null;

    protected List<String> recordNumbersFeedback = null;

    protected List<String> recordNumbersSuspended = null;

    protected List<String> recordNumbersMonitored = null;

    protected List<String> recordNumbersOutstanding = null;

    /**
     * Standard property
     */
    protected Rule rule;

    protected RecordSet records;
    // needed for calculating other_scores
    protected RecordSet included_records;
    protected RecordSet feedback_records;
    protected RecordSet monitored_records;
    protected RecordSet suspended_records;
    protected RecordSet outstanding_records;

    protected Record grouping;

    public ScoreService(Rule rule, String scoreType, RecordSet records, Record grouping) {
        this.rule = rule;
        this.scoreType = scoreType;
        this.grouping = grouping;
        this.records = records;
    }

    public ScoreService(Rule rule, String scoreType, RecordSet records) {
        this.rule = rule;
        this.scoreType = scoreType;
        this.records = records;
    }

    public ScoreService() {
        this.records = RecordSet.EMPTY_RECORDSET;
        this.scoreType = "default";
    }

    protected void updateRecords() {
        updatingRecordsScore();
        //Additional Service Layer
        additionalAbstractionLayer();
    }

    protected void updatingRecordsScore() {
        included_records = new RecordSet();
        feedback_records = new RecordSet();
        monitored_records = new RecordSet();
        suspended_records = new RecordSet();
        outstanding_records = new RecordSet();
        if (records.size() == 0) {
            count = 0;
            countFeedback = 0;
            countOutstanding = 0;
            countMonitored = 0;
            countSuspended = 0;
            recordNumbers = Collections.emptyList();
            recordNumbersFeedback = Collections.emptyList();
            recordNumbersSuspended = Collections.emptyList();
            recordNumbersMonitored = Collections.emptyList();
            recordNumbersOutstanding = Collections.emptyList();
            /** END of PR-FIX counting related **/
        } else if (rule.getCounts() == Rule.Counts.PRS) {
            /* If this is a PR rule, we collect two sets of record numbers, one
             * for feedback, one for all records, and counts for total,
             * feedback, and non-feedback.  The Python layer later strips
             * the feedback from the total record number lists to make a
             * list containing non-feedback records.  This means we send two
             * copies of each feedback PR number, but allows us to preserve
             * record number ordering. */
            recordNumbers = new ArrayList<String>();
            recordNumbersFeedback = new ArrayList<String>();
            recordNumbersMonitored = new ArrayList<String>();
            recordNumbersSuspended = new ArrayList<String>();
            recordNumbersOutstanding = new ArrayList<String>();
            Set<String> recs = new HashSet<String>();
            Set<String> recsFeed = new HashSet<String>();
            Set<String> recsSuspended = new HashSet<String>();
            Set<String> recsMonitored = new HashSet<String>();
            Set<String> recsOutstanding = new HashSet<String>();
            // Get record numbers sorted by lastModified
            for (Record record : new TreeSet<Record>(records)) {
                if (record instanceof ScopedRecord) {
                    ScopedRecord rec = (ScopedRecord) record;
                    boolean included = true;
                    boolean outstanding = true;
                    if (rec.getState().equals("feedback")) {
                        feedback_records.add(record);
                        recordNumbersFeedback.add(rec.getRecordId().trim());
                        recsFeed.add(rec.getContainingRecordId());
                        outstanding = false;
                    } else if (rec.getState().equals("suspended")) {
                        suspended_records.add(record);
                        recordNumbersSuspended.add(rec.getRecordId().trim());
                        recsSuspended.add(rec.getContainingRecordId());
                        included = false;
                        outstanding = false;
                    } else if (rec.getState().equals("monitored")) {
                        monitored_records.add(record);
                        recordNumbersMonitored.add(rec.getRecordId().trim());
                        monitored_records.add(record);
                        recsMonitored.add(rec.getContainingRecordId());
                        included = false;
                        outstanding = false;
                    }
                    /**
                     * We are not including suspended and monitored in overall
                     */
                    if (included) {
                        included_records.add(record);
                        recordNumbers.add(rec.getRecordId().trim());
                        recs.add(rec.getContainingRecordId());
                    }
                    /**
                     * For Outsanding calculation, which also means
                     *   Excluding feedback, suspended and monitored PR
                     */
                    if (outstanding) {
                        outstanding_records.add(record);
                        recordNumbersOutstanding.add(rec.getRecordId().trim());
                        recsOutstanding.add(rec.getContainingRecordId());
                    }
                    /**
                     * For Undead CVBC, we grab all counts since it has different
                     * state machine (cvbc)
                     */
                } else if (record instanceof UndeadCvbcRecord) {
                    UndeadCvbcRecord rec = (UndeadCvbcRecord) record;
                    recordNumbers.add(rec.getRecordId().trim());
                    /**
                     * Make sure it have unique counts
                     */
                    recs.add(rec.getContainingRecordId());
                }
            }
            countFeedback = recsFeed.size();
            countSuspended = recsSuspended.size();
            countMonitored = recsMonitored.size();
            count = recs.size();
            countOutstanding = recsOutstanding.size();
        } else if(rule.getCounts() == Rule.Counts.REVIEWS) {
            recordNumbers = new ArrayList<String>();
            uniqueRevRecs = new ArrayList<String>();
            // Collect the unique review id records without review scopes
            for (Record rec : new TreeSet<Record>(records)) {
                recordNumbers.add(rec.getRecordId().trim());
                String[] new_id = rec.getRecordId().trim().split("-");
                if (! uniqueRevRecs.contains(new_id[0])) {
                    uniqueRevRecs.add(new_id[0]);
                }
            }
            // Get the unique review record counts
            count = uniqueRevRecs.size();

        } else {
            /**
             * NOTHING, RLI... goes here
             */
            count = records.size();
            recordNumbers = new ArrayList<String>();
            recordNumbersFeedback = Collections.emptyList();
            // Get record numbers sorted by lastModified
            if (rule.getCounts() != Rule.Counts.CLOSEDPRS) {
                for (Record rec : new TreeSet<Record>(records)) {
                    recordNumbers.add(rec.getRecordId().trim());
                }
            }
        }
        recordsChanged = false;
    }

    /**
     * Put the other Logic for calculating score here
     */
    protected abstract void additionalAbstractionLayer();

    /**
     * (Re)-calculate multiplier and daysLeft, since they change as time
     * passes, and set score accordingly.
     *
     * @return true if score or daysLeft has changed.
     */
    public boolean calculateScore() {
        if (recordsChanged) {
            updateRecords();
        }
        int oldDaysLeft = daysLeft;
        if (null == grouping) {
            daysLeft = -1;
            multiplier = 1;
        } else if (grouping.getRecordId().equals(DataSource.RELEASES.NO_RELEASE.getRecordId())) {
            // The fake release never gets any score.
            daysLeft = -1;
            multiplier = 0;
        } else if (grouping instanceof ReleaseRecord) {
            daysLeft = rule.daysUntil((ReleaseRecord) grouping);
            multiplier = rule.multiplier((ReleaseRecord) grouping);
        } else {
            daysLeft = 1;
            multiplier = 1;
        }
        int oldScore = score;
        // this calculation ONLY works on PR type records
        if (scoreType.equalsIgnoreCase("pr_backlog")) {
            // types: all, yes, only, monitored, suspended, no (exclude monitored, suspended, feedback)
            // unique and not unique versions

            // in python side, this is count_type == "yes"
            score = calculatePrBacklogScore(included_records, true);
            other_scores.put("yes_yes", score);
            other_scores.put("yes_no", calculatePrBacklogScore(included_records, false));

            // on python side, this is count_type == "only"
            other_scores.put("only_yes", calculatePrBacklogScore(feedback_records, true));
            other_scores.put("only_no", calculatePrBacklogScore(feedback_records, false));

            // these match the python side
            other_scores.put("monitored_yes", calculatePrBacklogScore(monitored_records, true));
            other_scores.put("monitored_no", calculatePrBacklogScore(monitored_records, false));

            other_scores.put("suspended_yes", calculatePrBacklogScore(suspended_records, true));
            other_scores.put("suspended_no", calculatePrBacklogScore(suspended_records, false));

            // on python side his is count_type == "no"
            other_scores.put("no_yes", calculatePrBacklogScore(outstanding_records, true));
            other_scores.put("no_no", calculatePrBacklogScore(outstanding_records, false));

            // on python side, this is count_type == "all"
            RecordSet all = new RecordSet();
            Set<String> all_ids = new HashSet<String>();
            addUniqueRecords(all, included_records, all_ids);
            addUniqueRecords(all, monitored_records, all_ids);
            addUniqueRecords(all, suspended_records, all_ids);

            other_scores.put("all_yes", calculatePrBacklogScore(all, true));
            other_scores.put("all_no", calculatePrBacklogScore(all, false));

        } else {
            score = calculateStandardScore();
        }
        return score != oldScore || daysLeft != oldDaysLeft;
    }

    protected void addUniqueRecords(RecordSet base, RecordSet adding, Set<String> unique_index) {
        for (Record record : new TreeSet<Record>(adding)) {
            if (!unique_index.contains(record.getRecordId())) {
                unique_index.add(record.getRecordId());
                base.add(record);
            }
        }

    }
    protected int calculateStandardScore() {
        // In case someone calls calculateScore on the NO_SCORE object.
        int weight = (null == rule ? 1 : rule.getWeight());
        return count * multiplier * weight;
    }

    protected int calculatePrBacklogScore(RecordSet working_records, boolean unique) {
        Set<String> unique_prs = new HashSet<String>();
        String number = "";
        int result = 0;

        for (Record r : working_records) {
            ScopedRecord working_record = (ScopedRecord) r;
            if (unique) {
                number = working_record.getContainingRecordId();
                if (!unique_prs.contains(number)) {
                    unique_prs.add(number);
                    result += calculatePrBacklogRecordScore(r);
                }
            } else {
                result += calculatePrBacklogRecordScore(r);
            }

        }
        return result;
    }

    protected int calculatePrBacklogRecordScore(Record r) {
        //  (CL1+CL2)x10 + (CL3+IL1+IL2)x5 +(CL4+IL3)x1 + (IL4)x0
        // for some reason, the business wanted to add a number to the
        // front of all of the problem-levels, so here is the list
        // with the prepended numbers
        // "1-CL1" "2-CL2" "3-IL1" "4-CL3" "4-IL2" "5-CL4" "5-IL3" "6-IL4"
        SharedPRRecord rec = (SharedPRRecord) r;
        if (rec.getProblem_level().equals("1-CL1") || rec.getProblem_level().equals("2-CL2") )
            return 10;
        else if (rec.getProblem_level().equals("4-CL3") || rec.getProblem_level().equals("3-IL1") || rec.getProblem_level().equals("4-IL2"))
            return 5;
        else if (rec.getProblem_level().equals("5-CL4") || rec.getProblem_level().equals("5-IL3"))
            return 1;
        else
            return 0;
    }


    /**
     * Add the given records to our set and update the count and score.
     *
     * Used to build a total across all groupings (eg. releases) for a user.
     */
    public void addRecords(RecordSet rset) {
        records.addAll(rset);
        recordsChanged  = true;
    }

    /**
     * @return the count
     */
    public int getCount() {
        return count;
    }

    public int getCountFeedback() {
        return countFeedback;
    }

    public int getCountOutstanding() {
        return countOutstanding;
    }

    public int getCountSuspended() {
        return countSuspended;
    }

    public int getCountMonitored() {
        return countMonitored;
    }

    /**
     * @return the daysLeft
     */
    public int getDaysLeft() {
        return daysLeft;
    }

    /**
     * @return the multiplier
     */
    public int getMultiplier() {
        return multiplier;
    }

    /**
     * @return the rule
     */
    @JSON(include = false)
    public Rule getRule() {
        return rule;
    }

    @JSON
    public String getRuleId() {
        return (null != rule) ? rule.getIdentifier() : "";
    }

    /**
     * @return the score
     */
    public int getScore() {
        return score;
    }

    @JSON
    public Map<String, Integer> getOtherScores() {
        return other_scores;
    }
    /**
     * @return the grouping
     */
    @JSON(include = false)
    public Record getGrouping() {
        return grouping;
    }

    @JSON
    public String getGroupingId() {
        return (null != grouping) ? grouping.getRecordId() : "";
    }

    /**
     * @return the things
     */
    @JSON(include = false)
    public RecordSet getRecords() {
        return records;
    }

    /**
     * @return the record numbers of the things
     */
    @JSON
    public List<String> getRecordNumbers() {
        return recordNumbers;
    }

    @JSON
    public List<String> getRecordNumbersSuspended() {
        return recordNumbersSuspended;
    }

    @JSON
    public List<String> getRecordNumbersMonitored() {
        return recordNumbersMonitored;
    }

    @JSON
    public List<String> getRecordNumbersFeedback() {
        return recordNumbersFeedback;
    }

    @JSON
    public List<String> getRecordNumbersOutstanding() {
        return recordNumbersOutstanding;
    }
    /**
     * Re-fetch our Rule from the repository.  Needed when rules are updated
     * from xml.
     *
     * @return True if the Rule is a different object.
     */
    public boolean reloadRule() {
        if (null == rule) {
            return false;
        }
        Rule oldRule = rule;
        rule = Repository.getRule(oldRule.getIdentifier());
        return rule == oldRule;
    }


}
