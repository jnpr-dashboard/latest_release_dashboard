/**
 * ravikp@juniper.net, July 20, 2013
 *
 * Copyright (c) 2013, Juniper Networks, Inc.
 * All rights reserved.
 */
package net.juniper.dash.data;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.juniper.dash.DashboardException;
import net.juniper.dash.Repository;
import net.juniper.dash.SleepyTimes;
import net.juniper.dash.Refresher;
import net.juniper.dash.Refresher.TimeoutException;
import org.apache.log4j.Logger;


/**
 * Newly in PR DataSource.
 *
 */
public class GnatsNewlyInFix extends DataSource {

    static Logger logger = Logger.getLogger(Gnats.class.getName());

    private Map<String, NewlyInPRFixRecord> records = new HashMap<String, NewlyInPRFixRecord>();

    public GnatsNewlyInFix(String name) {
        super(name, "newly_in_fix", "NewlyInPRFixRecord", "NEWLYINPR", "NEWLYINPRs");
        // Mon Jun 26 12:16:19 -0700 2013
        dateParser = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzzz yyyy");
        dateParser.setLenient(false);        
    }

    public void init() throws DashboardException {
        
        
        propFileName = Repository.createPath(Repository.dash_special_directory, "gnats.properties", absolutePropPath);
        loaderProps =
            Repository.readProperties(propFileName, absolutePropPath);
        host = loaderProps.getProperty("host");
        webAppPath = loaderProps.getProperty("web_app_path");
        String reloaderPath = Repository.createPath(Repository.BIN_DIR, "/newly-in-prfix-data.pl", true);
        File f = new File(reloaderPath);
        if ( ! (f.exists() && f.canRead())) {
            // No way to test for executable, so we still might blow up later.
            throw new DashboardException("No external record reloader for " +  getName() + " at path " + reloaderPath);
        }
        List<String> cmd = new ArrayList<String>();
        cmd.add(reloaderPath);
        logger.debug("Creating Newly In PR report records processor: " + cmd.toString());
        processBuilder = new ProcessBuilder(cmd);
        // Need to get the categories sorted out first.
        dependsOn = USERSOURCE;
    }

    protected Record constructRecord(String[] fields) throws DashboardException {
        return new NewlyInPRFixRecord(fields, this);
    }

    protected String[] getFieldList() {
        return NewlyInPRFixRecord.getFieldList();
    }

    @Override
    public Record getRecord(String id) {
        return records.get(id);
    }

    @Override
    public Map<String, Record> getRecords() {
        return new HashMap<String, Record>(records);
    }

    @Override
    protected void removeRecord(String number) {
        records.remove(number);
    }

    @Override
    protected void addRecord(Record record) {
        records.put(record.getId(), (NewlyInPRFixRecord) record);
    }

    @Override
    protected boolean hasRecord(String id) {
        return records.containsKey(id);
    }
}
