/**
 * Self-populating value bean for a Review Tracker review group.
 *
 * dbergstr@juniper.net, Wed Mar 28 01:22:08 2007
 *
 * Copyright (c) 2007, Juniper Networks, Inc.
 * All rights reserved.
 */
package net.juniper.dash.data;

import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import net.juniper.dash.DashboardException;

/**
 *  A JUNOS Code Review Group
 */
public class ReviewGroup extends Record {

    private Set<User> moderators;
    private boolean invalidModerator;

    public ReviewGroup(String[] values, DataSource dataSource)
            throws DashboardException {
        super(values, dataSource);
        lastUpdated = new Date();
        populate(values);
        calculateHashCode();
    }

    public Set<User> getModerators() {
        return moderators;
    }

    public boolean isInvalidModerator() {
        return invalidModerator;
    }

    public boolean populate(String[] values) throws DashboardException {
        if (null == values) {
            throw new DashboardException("Null input.");
        } else if (values.length != 3) {
            throw new DashboardException("Wrong number of elements: Got " +
                values.length + ", expected 3");
        }

        Set<User> newModerators = new HashSet<User>();
        String mod = values[1];
        if (null == mod || mod.length() == 0) {
            invalidModerator = true;
        } else {
            for (String name : mod.toLowerCase().split(",")) {
                User aModerator = (User) DataSource.USERSOURCE.getRecord(name);
                if (null != aModerator) {
                    newModerators.add(aModerator);
                }
            }
        }

        if (newModerators.equals(moderators)) {
            // No change
            return false;
        } else {
            if (newModerators.size() == 0) {
                moderators = Collections.emptySet();
            } else {
                moderators = newModerators;
            }
            lastUpdated = new Date();
            calculateHashCode();
            return true;
        }
    }

    @Override
    public String dump() {
        StringBuilder sb = dumpCommon();
        sb.append("invalidModerator => \"");
        sb.append(invalidModerator);
        sb.append("\"\n");
        sb.append("moderators => \"");
        for (User u : moderators) {
            sb.append(u.getId());
            sb.append(", ");
        }
        sb.append("\"\n");

        return sb.toString();
    }
}
