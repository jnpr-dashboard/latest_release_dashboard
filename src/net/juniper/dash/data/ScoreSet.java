/**
 * dbergstr@juniper.net, Dec 21, 2006
 *
 * Copyright (c) 2006, Juniper Networks, Inc.
 * All rights reserved.
 */
package net.juniper.dash.data;

import java.util.Date;
import java.util.SortedSet;
import java.util.TreeSet;


/**
 * Holds a bunch of Scores.
 *
 * Note that this is *not* designed to deal with changes in the Score objects.
 * Use once then throw away.
 *
 * @author dbergstr
 */
public class ScoreSet implements Comparable<ScoreSet> {

    private SortedSet<Score> scores;
    private int minutesSinceScored = -1;
    private User user;
    private int totalScore = 0;
    private Date scored;

    public ScoreSet(SortedSet<Score> scores, Date scored,
            User user) {
        this.scores = scores;
        this.scored = scored;
        this.minutesSinceScored = Math.round(
            (System.currentTimeMillis() - scored.getTime()) / 60000f);
        this.user = user;
        for (Score s : scores) {
            totalScore += s.getScore();
        }
    }

    public ScoreSet() {
        scores = new TreeSet<Score>();
    }

    /**
     * @return the scores
     */
    public SortedSet<Score> getScores() {
        return scores;
    }

    /**
     * @return the Date this set was scored
     */
    public Date getScored() {
        return scored;
    }

    /**
     * @return the minutesSinceOldestUpdate
     */
    public int getMinutesSinceScored() {
        return minutesSinceScored;
    }

    /**
     * @return the totalScore
     */
    public int getTotalScore() {
        return totalScore;
    }

    /**
     * @return the number of issues.
     */
    public int size() {
        return scores.size();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        } else if (null == o || ! o.getClass().equals(this.getClass())) {
            return false;
        }
        ScoreSet ss = (ScoreSet) o;
        if (totalScore != ss.totalScore) {
            return false;
        }
        if (null == user && null != ss.user) {
            return false;
        } else if (null != user && ! user.equals(ss.user)) {
            return false;
        }
        return true;
    }

    /**
     * ScoreSets sort on totalScore descending, number of scores descending,
     * then User.
     */
    public int compareTo(ScoreSet o) {
        int retval = o.totalScore - totalScore;
        if (0 == retval) {
            retval = o.scores.size() - scores.size();
        }
        if (0 == retval) {
            retval = user.compareTo(o.user);
        }
        return retval;
    }

    /**
     * @return the user
     */
    public User getUser() {
        return user;
    }
}
