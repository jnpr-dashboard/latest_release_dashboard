/**
 * dbergstr@juniper.net, Jan 9, 2008
 *
 * Copyright (c) 2008, Juniper Networks, Inc.
 * All rights reserved.
 */
package net.juniper.dash.data;

import java.util.HashMap;
import java.util.Map;

import flexjson.JSONSerializer;

import net.juniper.dash.JSONSerializable;
import net.juniper.dash.DashboardException;

/**
 * Abstraction of the Deepthought NPI table.
 *
 * @author dbergstr
 */
public class NPI extends Deepthought implements JSONSerializable {

    private Map<String, NPIRecord> records = new HashMap<String, NPIRecord>();
    private JSONSerializer recordSerializer;

    public NPI(String name) {
        super(name, "npi", "NPIRecord", "NPI Program", "NPI Programs");
        tableName = "npi";
        recordSerializer = new JSONSerializer().exclude("dataSource", "factHandle").
            include("summary");
        dependsOn = RELEASES;
    }

    @Override
    protected Record constructRecord(String[] fields) throws DashboardException {
        return new NPIRecord(fields, this);
    }

    @Override
    public Record getRecord(String id) {
        return records.get(id);
    }

    @Override
    public Map<String, Record> getRecords() {
        return new HashMap<String, Record>(records);
    }

    @Override
    protected void removeRecord(String number) {
        records.remove(number);
    }

    @Override
    protected void addRecord(Record record) {
        records.put(record.getId(), (NPIRecord) record);
    }

    @Override
    protected boolean hasRecord(String id) {
        return records.containsKey(id);
    }

    @Override
    protected String getQueryParams() {
        return NPIRecord.queryParams;
    }

    @Override
    protected String[] getFieldList() {
        return NPIRecord.getFieldList();
    }

    @Override
    protected String[] getPickLists() {
        return NPIRecord.getPickLists();
    }

    public String serializeRecords() {
        return recordSerializer.serialize(records);
    }
}
