/**
 * ravikp@juniper.net, July 20, 2013
 *
 * Copyright (c) 2013, Juniper Networks, Inc.
 * All rights reserved.
 */
package net.juniper.dash.data;

import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net.juniper.dash.DashboardException;
/**
 *  A JUNOS  PR record to calculate Newly in PR for an IB
 */
public class NewlyInPRFixRecord extends Record{
    
    // Reg ex to pull user names out of email addresses
    private static final String ORIGINATOR_REGEX =  "(?:\\s*<.+?>)?\\s*([\\w\\-]+)(@|\\s*$)";
    private static Pattern originatorPat = Pattern.compile(ORIGINATOR_REGEX);

    private static final String[] fieldList = new String[] {
        "number",
        "planned-release",
        "state",
        "last-modified",
        "arrival-date",        
        "dev-owner",
        "originator",
        "responsible",
        "submitter-id",
        "class",
        "category",
        "ib"
    };

    public static String[] getFieldList() {
        return fieldList;
    }

    //Performance tuning for user filters in rules
    private static Set<String> valid_dev_owners = new HashSet<String>();
    private static Set<String> valid_responsibles = new HashSet<String>();
    private static Set<String> valid_category_spocs = new HashSet<String>();
    private static Set<String> valid_real_responsibles = new HashSet<String>();
    private boolean BlankDevowner;
    private String real_responsible;
    private String category_spoc;    
    private boolean non_junos_responsible = false;
    private Date last_modified;
    private String last_modified_string;
    private long last_modified_epoch;
    private Date arrival_date;
    private String arrival_date_string;
    private long arrival_date_epoch;
    private String containingRecordId;
    private String ib_name;
    private String clazz;
    private Integer clazz_ordinal;
    private String dev_owner;
    private String originator;
    private String responsible;
    private String state;
    private Integer state_ordinal;
    private String submitter_id;
    private String category;
    private Integer category_ordinal;    

    public NewlyInPRFixRecord(String[] values, DataSource dataSource)
            throws DashboardException {
        super(values, dataSource);
        populate(values);
    }

    public boolean populate(String[] values) throws DashboardException {
        if (null == values) {
            throw new DashboardException("Null input.");
        } else if (values.length != fieldList.length + 2) {
            // values always comes with a throw away on the end
            throw new DashboardException("Wrong number of elements: Got " +
                values.length + ", expected " + (fieldList.length + 2));
        }
        StringBuffer str = new StringBuffer();
        for(String str1: values){
            str.append(str1);
            str.append(",");
        }

        boolean changed = false;
        lastUpdated = new Date();
        String lastModifiedString = values[3];
        // going to need arrival date for some pre-processing
        String ArrivalDateString = values[4];
        
        if (lastModifiedString.trim().length() == 0) {
            // Never been modified, use arrival-date
            lastModifiedString = values[4];
        }
        Date newLastModified = dataSource.parseDate(lastModifiedString);
        Date newArrivalDate = dataSource.parseDate(ArrivalDateString);
        if (! newLastModified.equals(lastModified)) {
            lastModified = newLastModified;
            changed = true;
        }
        
        /**
         * This should never happened... if it does there are
         * some discrepancy in GNATS system
         */
        if (! newArrivalDate.equals(arrival_date)){
            arrival_date = newArrivalDate;
            changed = true;
        }
        last_modified = lastModified;
        last_modified_epoch = last_modified.getTime();
        last_modified_string = dataSource.yyyymmdd(last_modified);
        
        arrival_date_epoch = arrival_date.getTime();
        arrival_date_string= dataSource.yyyymmdd(arrival_date);

        containingRecordId = findPRNum(id);

        if (! values[10].equals(category)) {
            category = values[10];
            category_ordinal = dataSource.picklistOrdinal("category", category);
            changed = true;
        }

        if (! values[9].equals(clazz)) {
            clazz = values[9];
            clazz_ordinal = dataSource.picklistOrdinal("clazz", clazz);
            changed = true;
        }

        if (! values[5].toLowerCase().equals(dev_owner)) {
            dev_owner = values[5].toLowerCase();
            changed = true;
        }
        
        
        String origtmp = "";
        Matcher origMatch = originatorPat.matcher(values[6]);
        if (origMatch.find()) {
            origtmp = origMatch.group(1);
        }
        if (! origtmp.equals(originator)) {
            originator = origtmp;
            changed = true;
        }
        
        if (! values[7].toLowerCase().equals(real_responsible)) {
            real_responsible = values[7].toLowerCase();
            changed = true;
        }

        if (! values[2].equals(state)) {
            state = values[2];
            state_ordinal = dataSource.picklistOrdinal("state", state);
            changed = true;
        }

        if (! values[8].equals(submitter_id)) {
            submitter_id = values[8];
            changed = true;
        }

        if (! values[11].equals(ib_name)) {
            ib_name = values[11];
            changed = true;
        }
        
        String category_spoc_old = category_spoc;
        category_spoc = DataSource.CATEGORY_CONTACTS.getContacts().get(category);
        if (null == category_spoc ) {
            category_spoc = "deprecated_category";
        } else if (!DataSource.USERSOURCE.getRealUsernames().contains(category_spoc) &&
                    !DataSource.USERSOURCE.getUserNames().contains(category_spoc)) {
            // user is non_user --> leave company or not found in ldap
            category_spoc = "non_user";
        }
        changed |= ! category_spoc.equals(category_spoc_old);
        
        if (category_spoc.length() > 0 &&
            ! DataSource.USERSOURCE.getRealUsernames().contains(real_responsible)) {
            /* The responsible party isn't a real person.  We'll  pretend that
             * it's the category contact. */
            changed |= ! category_spoc.equals(responsible);
            responsible = category_spoc;
        } else {
            changed |= ! real_responsible.equals(responsible);
            responsible = real_responsible;
        }
        
        boolean old_non_junos_responsible = non_junos_responsible;
        if (DataSource.USERSOURCE.getRealUsernames().contains(real_responsible) && 
            ! DataSource.USERSOURCE.getUserNames().contains(real_responsible)) {
            /* responsible is a person, but not in JUNOS engineering.
             * We list these PRs separately for the category owner to keep
             * track of.  
             * */
            non_junos_responsible = true;
        } 
        changed |= ! (non_junos_responsible == old_non_junos_responsible);

        if (changed) {
            /**
             * Determine if dev-owner is blank, we need this
             * since identifying blank dev-owner is needed in rules
             */
            BlankDevowner = false;
            if ((getDev_owner().length() <= 0) || (getDev_owner().equals("-"))){
                BlankDevowner = true;
            }
            
        }
        summary = new String[] {ib_name, state };
        valid_dev_owners.add(dev_owner);
        valid_responsibles.add(responsible);
        valid_category_spocs.add(category_spoc);
        valid_real_responsibles.add(real_responsible);
        return changed;
    }

    public static void initValidUserList() {
        valid_dev_owners.clear();
        valid_responsibles.clear();
        valid_category_spocs.clear();
        valid_real_responsibles.clear();        
    }

    public static Set<String> getValidUsers(String user) {
        Set<String> s =  new HashSet<String>();
        if (user.contains(":dev_owners")) {
            s.addAll(valid_dev_owners);
        } 
        if (user.contains(":responsibles")) {
            s.addAll(valid_responsibles);
        } 
        if (user.contains(":category_spocs")) {
            s.addAll(valid_category_spocs);
        } 
        if (user.contains(":real_responsibles")) {
            s.addAll(valid_real_responsibles);
        }       
        return s;     
    }

    /**
     * 
     * @return  True if dev_owner is empty
     */
    public boolean isBlank_devowner(){
        return BlankDevowner;
    }

    /**
     * @return the real_responsible
     */
    public String getReal_responsible() {
        return real_responsible;
    }

    /**
     * @return the non_junos_responsible
     */
    public boolean isNon_junos_responsible() {
        return non_junos_responsible;
    }

    public Date getLast_modified() {
        return last_modified;
    }

    public String getLast_modified_string() {
        return last_modified_string;
    }

    public long getLast_modified_epoch() {
        return last_modified_epoch;
    }

    public Date getArrival_date() {
        return arrival_date;
    }

    public String getArrival_date_string() {
        return arrival_date_string;
    }

    public long getArrival_date_epoch() {
        return arrival_date_epoch;
    }

    public String getContainingRecordId() {
        return containingRecordId;
    }

    public String getClazz() {
        return clazz;
    }

    public Integer getClazz_ordinal() {
        return clazz_ordinal;
    }

    public String getDev_owner() {
        return dev_owner;
    }

    public String getIb_name() {
        return ib_name;
    }

    public String getResponsible() {
        return responsible;
    }

    /**
     * @return the category_owner
     */
    public String getCategory_spoc() {
        return category_spoc;
    }

    public String getCategory() {
        return category;
    }
   
    public Integer getCategory_ordinal() {
        return category_ordinal;
    }
    
    public String getState() {
        return state;
    }

    public Integer getState_ordinal() {
        return state_ordinal;
    }

    public String getSubmitter_id() {
        return submitter_id;
    }

    @Override
    public String dump() {
        StringBuilder sb = dumpCommon();
        sb.append("last_modified => \"");
        sb.append(last_modified);
        sb.append("\"\nlast_modified_string => \"");
        sb.append(last_modified_string);
        sb.append("\"\nlast_modified_epoch => \"");
        sb.append(last_modified_epoch);
        sb.append("\"\nnon_junos_responsible => \"");
        sb.append(non_junos_responsible);
        sb.append("\"\nreal_responsible => \"");
        sb.append(real_responsible);
        sb.append("\"\n");
        sb.append("clazz => \"");
        sb.append(clazz);
        sb.append("\"");
        sb.append(" (");
        sb.append(clazz_ordinal);
        sb.append(")");
        sb.append("\n");
        sb.append("originator => \"");
        sb.append(originator);
        sb.append("\"");
        sb.append("\n");
        sb.append("dev_owner => \"");
        sb.append(dev_owner);
        sb.append("\"");
        sb.append("\n");
        sb.append("Ib Name => \"");
        sb.append(ib_name);
        sb.append("\"");
        sb.append("\n");
        sb.append("responsible => \"");
        sb.append(responsible);
        sb.append("\"");
        sb.append("\n");
        sb.append("state => \"");
        sb.append(state);
        sb.append("\"");
        sb.append(" (");
        sb.append(state_ordinal);
        sb.append(")");
        sb.append("\n");
        sb.append("submitter_id => \"");
        sb.append(submitter_id);
        sb.append("\"");
        sb.append("\n");
        return sb.toString();
    }
}
