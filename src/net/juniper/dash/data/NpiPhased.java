/**
 * dbergstr@juniper.net, Jan 10, 2008
 *
 * Copyright (c) 2008, Juniper Networks, Inc.
 * All rights reserved.
 */
package net.juniper.dash.data;

import java.util.Date;

/**
 * An object that has NPI phases
 *
 * @author dbergstr
 */
public interface NpiPhased {
    public String getPhase();
    public Integer getPhase_ordinal();
    public Date getP0_ll_exit();
    public String getP0_ll_exit_string();
    public long getP0_ll_exit_epoch();
    public Date getP0_exit();
    public String getP0_exit_string();
    public long getP0_exit_epoch();
    public Date getP1_exit();
    public String getP1_exit_string();
    public long getP1_exit_epoch();
    public Date getP2_exit();
    public String getP2_exit_string();
    public long getP2_exit_epoch();
    public Date getP3_exit();
    public String getP3_exit_string();
    public long getP3_exit_epoch();
    public Date getP4_exit();
    public String getP4_exit_string();
    public long getP4_exit_epoch();
    public Date getP5_exit();
    public String getP5_exit_string();
    public long getP5_exit_epoch();
}
