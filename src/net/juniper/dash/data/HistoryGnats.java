/**
 * dbergstr@juniper.net, Oct 27, 2006
 *
 * Copyright (c) 2006, Juniper Networks, Inc.
 * All rights reserved.
 */
package net.juniper.dash.data;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import net.juniper.dash.DashboardException;
import net.juniper.dash.Refresher;
import net.juniper.dash.Repository;
import net.juniper.dash.Refresher.TimeoutException;

import org.apache.log4j.Logger;
import org.drools.WorkingMemory;

import flexjson.JSONSerializer;

/**
 * OldPR DataSource.
 *
 * @author dbergstr
 */
public class HistoryGnats extends DataSource {

//    static Logger logger = Logger.getLogger(HistoryGnats.class.getName());

    private Map<String, OldPRRecord> records = new HashMap<String, OldPRRecord>();
    private JSONSerializer recordSerializer;

    // Used by both Gnats and CategoryContacts
    String port;
    String database;
    String query_prPath;
    String categorySubfields;

    public HistoryGnats(String name) {
        super(name, "historygnats", "OldPRRecord", "OldPR", "OldPRs");
        // Mon Jun 26 12:16:19 -0700 2006
        dateParser = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzzz yyyy");
        dateParser.setLenient(false);
        recordSerializer = new JSONSerializer();
    }

    public void init() throws DashboardException {
    	loaderProps = null;
    	propFileName = "";
    	
        String reloaderPath = Repository.createPath(Repository.BIN_DIR, "/historypr.pl", true);
        File f = new File(reloaderPath);
        if ( ! (f.exists() && f.canRead())) {
            // No way to test for executable, so we still might blow up later.
            throw new DashboardException("No external record reloader for" +
                    " Releases at path " + reloaderPath);
        }
        List<String> cmd = new ArrayList<String>();
        cmd.add(reloaderPath);
        cmd.add("--filename");
        String data_filename = Repository.createPath(Repository.dash_special_directory, "data", false);
        data_filename = Repository.createPath(data_filename, "gnats.txt.bz2", false);
        cmd.add(data_filename);
        
        for (String fld : OldPRRecord.getFieldList()) {
        	if (!fld.equals("number") && !fld.equals("line-number") && !fld.equals("extract-date")){
        		cmd.add(fld);
        	}
        }
        processBuilder = new ProcessBuilder(cmd);

        // Need to get the categories sorted out first.
        dependsOn = USERSOURCE;
    }

    protected Record constructRecord(String[] fields) throws DashboardException {
        return new OldPRRecord(fields, this);
    }


    /**
     * For summary purposes, an UndeadCvbc is really a PR, so include them. 
     * 
     * @return A JSON representation of a Map of record_number => [ synopsis,
     * last_modified]. 
     */
    @Override
    public String getSummaries() {
        Collection<? extends Record> recs = getRecords().values();
        Map<String, String[]> out = new HashMap<String, String[]>(recs.size());
        for (Record r : recs) {
            out.put(r.getId(), r.getSummary());
        }
        return recordSerializer.serialize(out);
    }

    @Override
    public Record getRecord(String id) {
        return records.get(id);
    }

    @Override
    public Map<String, Record> getRecords() {
        return new HashMap<String, Record>(records);
    }

    @Override
    protected void removeRecord(String number) {
        records.remove(number);
    }

    @Override
    protected void addRecord(Record record) {
        records.put(record.getId(), (OldPRRecord) record);
    }

    @Override
    protected boolean hasRecord(String id) {
        return records.containsKey(id);
    }
    
    //Performance tuning
    @Override
    protected List<String[]> getRawRecordData(Refresher refresher) throws DashboardException, TimeoutException {    	
    	OldPRRecord.initValidUserList();  	
        return super.getRawRecordData(refresher);
    }
}
