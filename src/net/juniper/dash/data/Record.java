/**
 * dbergstr@juniper.net, Oct 5, 2006
 *
 * Copyright (c) 2006-2009, Juniper Networks, Inc.
 * All rights reserved.
 */
package net.juniper.dash.data;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net.juniper.dash.DashboardException;

import org.apache.log4j.Logger;
import org.drools.FactHandle;

/**
 * Generic Record superclass.
 *
 * @author dbergstr
 */
public abstract class Record implements Comparable<Record> {

    private static Pattern firstWordPat = Pattern.compile("^([\\w\\-]+)");

    /**
	 * Standard Regex used to recognize Releases and Branch 
	 */
    protected static final String RELEASE_REGEX =
        "^(?:\\w+-|\\[)? # optional leading junk, generally JUNOS- or [\n" +
        "(\\b[1-9][0-9]?\\.[1-9]?[0-9]) # major dot minor, 1-2 digits\n" +
        "(([rbs])[1-9][0-9]? | (IB\\w+))? # optional type is either release or beta or cdm release\n" +
        "(\\-?[sd][1-9][0-9]?)? # optional service release\n";
    protected static final String BRANCH_REGEX =
        "(IB[1-5]?)\\_ # Type of branch \n" +
        "([1-9][0-9]?\\_[1-4])((\\_)(\\w+))?\\_BRANCH # major dot minor, 1-2 digits\n" ;

    /**
     * @return The first word from the input, or the entire input if there's
     * no obvious "first word".
     */
    protected static String getFirstWord(String input) {
        if (input.length() == 0) {
            return input;
        }
        Matcher mat = firstWordPat.matcher(input);
        if (mat.lookingAt()) {
            return mat.group();
        } else {
            return input;
        }
    }

    static Logger logger = Logger.getLogger(Record.class.getName());

    /**
     * The number of characters synopsis will be trimmed to in the summary.
     */
    protected final int maxSummarySynopsisLength = 50;

    /**
     * When was this record last updated from the external data source?
     */
    protected Date lastUpdated = null;

    /**
     * When was the underlying data last modified in the external data source?
     *
     * Used to determine if the record has changed, and in equals()
     * and hashCode().
     *
     * Set the initial value to something different from the 0l "no value" flag
     * used in dataSource.parseDate(), so that the first time populate() runs
     * it always finds a difference.
     */
    protected Date lastModified = new Date(-1l);

    /**
     * Used to present a summary of the Record in the UI.  Generally consists
     * of synopsis and last-modified date.
     */
    protected String[] summary;

    protected DataSource dataSource;

    protected String id;

    protected FactHandle factHandle;

    private int hashCode = 0;

    private Date lastUpdatedInDrools;

    private Map<DataSource, RecordSet> records = Collections.emptyMap();

    public Record(String[] values, DataSource dataSource)
            throws DashboardException {

        this.id = values[0];
        this.dataSource = dataSource;
    }

    /**
     * Not generally implemented.
     */
    protected Record(String id, DataSource dataSource) {
 
        this.id = id;
        this.dataSource = dataSource;
        calculateHashCode();
    }

    /**
     * Exists to hack around a Drools limitation.
     *
     * You can't write "$boundFromAnotherClass == true", so I'm writing
     * "alwaysTrue == $boundFromAnotherClass".
     *
     * FIXME Replace this by creating a Set subclass whose contains() method
     * always returns true, and using that as the junos user's reportNames set.
     */
    public boolean isAlwaysTrue() {
        return true;
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @return the unique identifier from the source record
     * may be overridden when the id is something different than
     * the record identifier (example:  OldPRRecord)
     */
    public String getRecordId() {
    	return id;
    }
    /**
     * Returns id by default; may be overridden by subclasses.
     *
     * @return Something that passes as a name for this record.
     */
    public String getName() {
        return id;
    }

    /**
     * @return the summary array, consisting of synopsis and last_modified date.
     */
    public String[] getSummary() {
        return summary;
    }

    /**
     * @return the dataSource
     */
    public DataSource getDataSource() {
        return dataSource;
    }

    /**
     * @return the factHandle
     */
    public FactHandle getFactHandle() {
        return factHandle;
    }

    /**
     * @param factHandle the factHandle to set
     */
    public void setFactHandle(FactHandle factHandle) {
        this.factHandle = factHandle;
        updatedInDrools();
    }

    /**
     * Populate fields and do any necessary postprocessing.
     *
     * XXX??? To determine if the record has changed, implementations
     * generally just look at the last-modified time of the record as
     * supplied by the external data source.  This may lead to false positives,
     * as fields not used by the Dashboard may have been modified.  This is
     * deemed acceptable, as the overhead and complexity of comparing every
     * field is high, and the cost of false positives looks low.  It may
     * be necessary to revisit this later...
     *
     * @param values Values for all the fields, in the order returned by
     * getFieldList(), with record id prepended to the beginning.
     *
     * @return True if the record has "changed".
     *
     * @throws DashboardException
     */
    public abstract boolean populate(String[] values) throws DashboardException;

    /**
     * @return A dump of all the field => value mappings for this Record.
     */
    public abstract String dump();

    /**
     * Split Deepthought MultiPick field values.
     *
     * @param value The raw delimited value returned by RLI::api.
     * @return An array of pick values.
     */
    protected String[] splitMultiPick(String value) {
        if (value.length() > 2) {
            return value.substring(2).split("\\x1D");
        } else {
            return new String[0];
        }
    }

    /**
     * Munge a Deepthought Multipick value into a pretty string.
     *
     * @param value The raw delimited value returned by RLI::api.
     * @return a pipe-delimited string.
     */
    protected String mungeMultiPick(String value) {
        if (value.length() > 2) {
            return value.substring(2).replace("\\x1D", "|");
        } else {
            return "";
        }
    }

    /**
     * @return The first len characters of value followed by an elipsis, or
     * the entirety of value if it's shorter than len.
     */
    protected String trimString(String value, int len) {
        return value.length() > len ? value.substring(0, len) + "..." :
            value;
    }

    /**
     * @param set The new list of related Records for the DataSource.
     * @return True if the new list is different than the old.
     */
    public synchronized boolean setRelatedRecords(RecordSet set, DataSource ds) {
        if (logger.isTraceEnabled() && set.size() > 0) {
            logger.trace("Setting " + set.size() + " " +
                ds.getRecordPlural() + " on " +
                dataSource.getRecordSingular() + " " + id);
        }
        if (null == set) {
            // We don't take null for an answer.
            return false;
        } else if (records.size() == 0) {
            /* First time setting related records, replace EMPTY_MAP with
             * a real HashMap. */
            records = new HashMap<DataSource, RecordSet>();
        } else if (set.equals(records.get(ds))) {
            // no change from previous value
            return false;
        }
        records.put(ds, set);
        return true;
    }

    /**
     * @return All Records from the DataSource that are somehow related
     * to this Record.
     */
    public RecordSet getRelatedRecords(DataSource ds) {
        synchronized (records) {
            RecordSet retval = records.get(ds);
            if (null == retval) {
                if (records.size() == 0) {
                    records = new HashMap<DataSource, RecordSet>();
                }
                retval = RecordSet.EMPTY_RECORDSET;
                records.put(ds, retval);
            }
            return retval;
        }
    }

    /**
     * Drools convenience method for getRelatedRecords(DataSource.GNATS).
     */
    public RecordSet getRelatedPRs() {
        return getRelatedRecords(DataSource.GNATS);
    }

    /**
     * Drools convenience method for getRelatedRecords(DataSource.NPI).
     */
    public RecordSet getRelatedNPIs() {
        return getRelatedRecords(DataSource.NPI);
    }

    /**
     * Drools convenience method for getRelatedRecords(DataSource.RELEASES).
     */
    public RecordSet getRelatedReleases() {
        return getRelatedRecords(DataSource.RELEASES);
    }

    /**
     * Drools convenience method for getRelatedRecords(DataSource.RLI).
     */
    public RecordSet getRelatedRLIs() {
        return getRelatedRecords(DataSource.RLI);
    }

    /**
     * Drools convenience method for getRelatedRecords(DataSource.REVIEWTRACKER).
     */
    public RecordSet getRelatedReviews() {
        return getRelatedRecords(DataSource.REVIEWTRACKER);
    }

    public RecordSet getRelatedCRSs() {
        return getRelatedRecords(DataSource.CRSTRACKER);
    }

    /**
     * @return the Date this object was last updated from the external source.
     */
    public Date getLastUpdated() {
        return lastUpdated;
    }

    public int getMinutesSinceLastUpdated() {
        if (null == lastUpdated) {
            return -1;
        } else {
            return Math.round(
                (System.currentTimeMillis() - lastUpdated.getTime()) / 60000f);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        } else if (null == o || ! o.getClass().equals(this.getClass())) {
            return false;
        }
        Record rec = (Record) o;
        if (null == id && null != rec.id) {
            return false;
        } else if (null != id && ! id.equals(rec.id)) {
            return false;
        }
        return true;
    }

    /**
     * Records sort in reverse lastModified and regular id order.
     *
     * Yes, compareTo() is inconsistent with equals().  However, this is not
     * likely to present real-world difficulties, because:
     *  - A given collection will only contain a single record subclass
     *  - All instances of a subclass should have unique ids
     */
    public int compareTo(Record o) {
        if (null == o) {
            return 1;
        } else if (this == o) {
            return 0;
        } else if (null == lastModified && null != o.lastModified) {
            return -1;
        } else if (null == o.lastModified) {
            return 1;
        }
        int retval = o.lastModified.compareTo(lastModified);
        if (0 == retval) {
            if (null == id && null != o.id) {
                return -1;
            } else if (null == o.id) {
                return 1;
            }
            retval = id.compareTo(o.id);
        }
        return retval;
    }

    /**
     * This must be called at the end of populate() to generate a hashCode.
     */
    protected final void calculateHashCode() {
        hashCode = 37;
        hashCode = 31 * hashCode + id.hashCode();
        hashCode = 31 * hashCode + getClass().hashCode();
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     * Subclasses *must* call calculateHashCode during populate().
     */
    @Override
    public int hashCode() {
        return hashCode ;
    }

    /**
     * Called whenever this Record has been asserted into or updated in
     * the Drools WorkingMemory.
     */
    public void updatedInDrools() {
        lastUpdatedInDrools = new Date();
    }

    /**
     * @return the lastUpdatedInDrools
     */
    public Date getLastUpdatedInDrools() {
        return lastUpdatedInDrools;
    }

    /**
     * Find the PR number given either number-scope or number.
     */
    protected static String findPRNum(String idOrScope) {
        if (idOrScope.indexOf("-") > 0) {
            return idOrScope.substring(0, idOrScope.lastIndexOf("-"));
        } else {
            return idOrScope;
        }
    }
    
    @Override
    public String toString() {
        return id;
    }

    /**
     * @return A dump() representation of the fields common to Record.
     */
    protected StringBuilder dumpCommon() {
        StringBuilder sb = new StringBuilder(500);
        sb.append("id => \"");
        sb.append(id);
        sb.append("\"\nlastUpdated => \"");
        sb.append(lastUpdated);
        sb.append("\"\nlastModified => \"");
        sb.append(lastModified);
        sb.append("\"\nlastUpdatedInDrools => \"");
        sb.append(lastUpdatedInDrools);
        sb.append("\"\n");
        return sb;
    }
}
