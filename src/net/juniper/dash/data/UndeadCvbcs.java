/**
 * dbergstr@juniper.net, Oct 27, 2006
 *
 * Copyright (c) 2006, Juniper Networks, Inc.
 * All rights reserved.
 */
package net.juniper.dash.data;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.juniper.dash.DashboardException;
import net.juniper.dash.Refresher.TimeoutException;
import net.juniper.dash.Repository;
import net.juniper.dash.Refresher;

import org.apache.log4j.Logger;
import org.drools.WorkingMemory;

/**
 * DataSource for "Undead" CVBC PRs.
 *
 * @author dbergstr
 */
public class UndeadCvbcs extends DataSource {

    static Logger logger = Logger.getLogger(UndeadCvbcs.class.getName());

    private Map<String, UndeadCvbcRecord> records =
        new HashMap<String, UndeadCvbcRecord>();

    public UndeadCvbcs(String name) {
        super(name, "undeadcvbcs", "UndeadCvbcRecord", "Undead CVBC PR",
            "Undead CVBC PRs");
        // Mon Jun 26 12:16:19 -0700 2006
        dateParser = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzzz yyyy");
        dateParser.setLenient(false);
    }

    public void init() throws DashboardException {
//        this.workingMemory = workingMem;
    	loaderProps = GNATS.loaderProps;
    	propFileName = GNATS.propFileName;
        List<String> cmd = new ArrayList<String>();
        cmd.add(GNATS.query_prPath);
        cmd.add(GNATS.extra_opts);
        cmd.add("--database=" + GNATS.database);
        cmd.add("--host=" + GNATS.host);
        cmd.add("--port=" + GNATS.port);

        cmd.add("--expr=" + UndeadCvbcRecord.queryExpression);
        StringBuilder format = new StringBuilder("--format=\"");
        StringBuilder formatFields = new StringBuilder();
        for (String fld : UndeadCvbcRecord.getFieldList()) {
            format.append("%s");
            format.append(Repository.FIELD_SEPARATOR);
            formatFields.append(fld);
            formatFields.append(" ");
        }
        format.append(Repository.RECORD_SEPARATOR);
        format.append("\" ");
        format.append(formatFields);
        format.append("");
        cmd.add(format.toString());
        processBuilder = new ProcessBuilder(cmd);
        
        this.dependsOn = USERSOURCE;
    }

    protected Record constructRecord(String[] fields) throws DashboardException {
        return new UndeadCvbcRecord(fields, this);
    }

    @Override
    public Record getRecord(String id) {
        return records.get(id);
    }

    @Override
    public Map<String, Record> getRecords() {
        return new HashMap<String, Record>(records);
    }

    @Override
    protected void removeRecord(String number) {
        records.remove(number);
    }

    @Override
    protected void addRecord(Record record) {
        records.put(record.getId(), (UndeadCvbcRecord) record);
    }

    @Override
    protected boolean hasRecord(String id) {
        return records.containsKey(id);
    }
    
    //Performance tuning
    @Override
    protected List<String[]> getRawRecordData(Refresher refresher) throws DashboardException, TimeoutException {    	
    	UndeadCvbcRecord.initValidUserList(); 	
        return super.getRawRecordData(refresher);
    }
}
