/**
 * Autogenerated value bean for a JUNOS Requirements.
 *
 * smulyono@juniper.net, Tue Oct  5 17:32:53 2010
 *
 * Copyright (c) 2010, Juniper Networks, Inc.
 * All rights reserved.
 */
package net.juniper.dash.data;

import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net.juniper.dash.DashboardException;

/**
 *  A JUNOS Requirements
 */
public class RequirementsRecord extends Record {

    public static final String queryParams = "skipClosed=yes&not_status=not&status_pick=closed";
    public static final String debugQueryParams = "skipClosed=yes&not_status=not&status_pick=closed";
	
    private static final String RELEASE_REGEX =
        "^(?:\\w+-|\\[)? # optional leading junk, generally JUNOS- or [\n" +
        "(\\b[1-9][0-9]?\\.[1-9]?[0-9]) # major dot minor, 1-2 digits\n" +
        "((IB|[rb])[1-9][0-9]?)? # optional type is either release or beta or cdm release\n";
    private static final String BRANCH_REGEX =
        "(IB[1-9])\\_ # Type of branch \n" +
        "(\\b[1-9][0-9]?\\_[1-9]?[0-9])\\_BRANCH # major dot minor, 1-2 digits\n" ;
    private static Pattern releasePat = Pattern.compile(RELEASE_REGEX,
        Pattern.CASE_INSENSITIVE | Pattern.COMMENTS);
    private static Pattern branchPat = Pattern.compile(BRANCH_REGEX,
            Pattern.CASE_INSENSITIVE | Pattern.COMMENTS);

    private String relname = "";

    public String getRelname() {
        return relname;
    }

    // last_modified release_target status plm_driver 
    private static final String[] fieldList = new String[] {
        "last_modified",
        "release_target",
        "status",
        "plm_driver",
    };

    public static String[] getFieldList() {
        return fieldList;
    }

    private static final String[] pickLists = new String[] {
        "comp_dependencies",
        "release_target",
        "rel_target_commit",
        "commitment_release_target",
        "commit_user_id",
        "status",
        "plm_driver",
        "bg_driving",
        "request_priority_fst",
        "execution_priority_fst",
        "req_pri_ft",
        "exec_pri_ft",
        "req_pri_ipg",
        "exec_pri_ipg",
        "req_pri_jrs",
        "exec_pri_jrs",
        "req_pri_slt",
        "exec_pri_slt",
        "npv_type",
    };

    public static String[] getPickLists() {
        return pickLists;
    }

    private Date last_modified;
    private String last_modified_string;
    private long last_modified_epoch;
    private String release_target;
    private Integer release_target_ordinal;
    private String status;
    private Integer status_ordinal;
    private String plm_driver;
    private Integer plm_driver_ordinal;
    public RequirementsRecord(String[] values, DataSource dataSource)
            throws DashboardException {
        super(values, dataSource);
        populate(values);
    }

    public Date getLast_modified() {
        return last_modified;
    }
    public String getLast_modified_string() {
        return last_modified_string;
    }
    public long getLast_modified_epoch() {
        return last_modified_epoch;
    }

    public String getRelease_target() {
        return release_target;
    }
    public Integer getRelease_target_ordinal() {
        return release_target_ordinal;
    }

    public String getStatus() {
        return status;
    }
    public Integer getStatus_ordinal() {
        return status_ordinal;
    }

    public String getPlm_driver() {
        return plm_driver;
    }
    public Integer getPlm_driver_ordinal() {
        return plm_driver_ordinal;
    }

    public boolean populate(String[] values) throws DashboardException {
        if (null == values) {
            throw new DashboardException("Null input.");
        } else if (values.length != fieldList.length + 2) {
            // values always comes with a throwaway on the end, and the
            // record number at the beginning.
            throw new DashboardException("Wrong number of elements: Got " +
                values.length + ", expected " + (fieldList.length + 2));
        }
        boolean changed = false;
        lastUpdated = new Date();
        Date newLastModified = dataSource.parseDate(values[1]);
        if (! newLastModified.equals(lastModified)) {
            lastModified = newLastModified;
            changed = true;
        }

        last_modified = lastModified;
        last_modified_epoch = last_modified.getTime();
        last_modified_string = dataSource.yyyymmdd(last_modified);
        if (! values[2].equals(release_target)) {
            release_target = values[2];
            release_target_ordinal = dataSource.picklistOrdinal("release_target", release_target);
            changed = true;
        }
        if (! values[3].equals(status)) {
            status = values[3];
            status_ordinal = dataSource.picklistOrdinal("status", status);
            changed = true;
        }
        if (! values[4].equals(plm_driver)) {
            plm_driver = values[4];
            plm_driver_ordinal = dataSource.picklistOrdinal("plm_driver", plm_driver);
            changed = true;
        }

        if (changed) {

            if (release_target.length() == 0) {
                relname = "unscheduled";
            } else if (release_target.equals("unscheduled") ||
                release_target.equals("n/a")) {
                relname = release_target;
            } else {    
                // Fall to this if the Release target is 
                // - Major.Minor
                // - IB Branch
                // - DEV Branch (not really checked correctly yet)
                Matcher mat = releasePat.matcher(release_target);
                if (mat.lookingAt()){
                    // Major.Minor
                    relname = mat.group(1);
                } else {
                    // IB Branch
                    mat = branchPat.matcher(release_target);
                    if (mat.lookingAt()){
                        // remove all _ into "."
                        String branch = mat.group(2)+mat.group(1);
                        relname = branch.replaceFirst("\\_","\\.");
                    } else {
                        // DEV Branch..... still no process
                        relname = release_target;
                    }
                }
            }

            summary = new String[] {
                trimString(getId(), maxSummarySynopsisLength),
                last_modified_string
            };

            calculateHashCode();
        }

        return changed;
    }

    @Override
    public String dump() {
        StringBuilder sb = dumpCommon();
        sb.append("last_modified => \"");
        sb.append(last_modified);
        sb.append("\"\nlast_modified_string => \"");
        sb.append(last_modified_string);
        sb.append("\"\nlast_modified_epoch => \"");
        sb.append(last_modified_epoch);
        sb.append("\"\n");
        sb.append("release_target => \"");
        sb.append(release_target);
        sb.append("\"");
        sb.append(" (");
        sb.append(release_target_ordinal);
        sb.append(")");
        sb.append("\n");
        sb.append("status => \"");
        sb.append(status);
        sb.append("\"");
        sb.append(" (");
        sb.append(status_ordinal);
        sb.append(")");
        sb.append("\n");
        sb.append("plm_driver => \"");
        sb.append(plm_driver);
        sb.append("\"");
        sb.append(" (");
        sb.append(plm_driver_ordinal);
        sb.append(")");
        sb.append("\n");

        return sb.toString();
    }
}
