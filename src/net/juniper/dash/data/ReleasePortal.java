/**
 * smulyono@juniper.net, Jul 15, 2010
 *
 * Copyright (c) 2010, Juniper Networks, Inc.
 * All rights reserved.
 */
package net.juniper.dash.data;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.juniper.dash.DashboardException;
import net.juniper.dash.Refresher;
import net.juniper.dash.Repository;
import net.juniper.dash.SleepyTimes;
import net.juniper.dash.Refresher.TimeoutException;

import org.apache.log4j.Logger;

public class ReleasePortal extends DataSource{
    static Logger logger = Logger.getLogger(ReleasePortal.class.getName());

    private Map<String, String> releases = Collections.emptyMap();

	private String reloaderPath;
    private List<String> cmd ;

    public List<String> getCmd() {
		return cmd;
	}

	public ReleasePortal(String name) {
        super(name, "releaseportal", "ReleasePortal", "Release Portal",
            "Release Portal");
    }

    public void init() throws DashboardException {
    	loaderProps = GNATS.loaderProps;
    	propFileName = GNATS.propFileName;
    	
        reloaderPath = Repository.createPath(Repository.BIN_DIR , "get-subfields.sh", true);
        File f = new File(reloaderPath);
        if ( ! (f.exists() && f.canRead())) {
            // No way to test for executable, so we still might blow up later.
            throw new DashboardException("No external record reloader for" +
                    " CategoryContacts at path " + reloaderPath);
        }
        cmd = new ArrayList<String>();
        /* Category fetcher syntax:
         * get-categories.sh <query-pr path> <host> <port> <database> <adm_field> <subfield,subfield,...> */
        cmd.add(reloaderPath);
        cmd.add(GNATS.query_prPath);
        cmd.add(GNATS.extra_opts);
        cmd.add(GNATS.host);
        cmd.add(GNATS.port);
        cmd.add(GNATS.database);
        cmd.add("planned-release");
        cmd.add(GNATS.releaseSubfields);
        processBuilder = new ProcessBuilder(cmd);

        /* We need all the Users to be loaded before we can process our
         * raw data. */
        dependsOn = DataSource.NPI;
    }
    
    @Override
    /**
     * Load records with values from an external source.
     *
     * @param refresher The Thread that is calling this method.
     * @throws DashboardException
     */
    public void populateRecords(Refresher refresher) throws DashboardException,
           TimeoutException {
       recordsUpdated = false;
       if (logger.isDebugEnabled()) {
           logger.debug("populating Records for " + getName());
       }

       List<String> output = refresher.getOutput(processBuilder,
           SleepyTimes.EXTERNAL_RELOADER_TIMEOUT.t());
       if (logger.isTraceEnabled()) {
           logger.trace(this.toString() + ": runExternal returned " +
               output.size() + " records.");
       }

       waitForDependencies(refresher);
       
       parseOutput(output);
    }
    
    private void parseOutput(List<String> output){
        Map<String, String> newReleases = new HashMap<String, String>();

        int count = 0;
        for (String categoryEntry: output) {
            String[] twoParts = categoryEntry.split("\\|");
            String release_name = twoParts[0].toUpperCase();
            boolean is_need = false;
            for (String subfield : twoParts[1].toUpperCase().split("\\s*:\\s*")) {
                // format current:branch
            	// current shows current association to product_group
            	//    our case, we only need 'JUNOS'
            	if (is_need){
            		newReleases.put(release_name, subfield);
            	} else if (subfield.equalsIgnoreCase("JUNOS")){
            		is_need = true;
            	}else break;
            }
            count++;
        }

        if (! releases.equals(newReleases)) {
            lastRecordChangeTime = System.currentTimeMillis();
        }

        releases = Collections.unmodifiableMap(newReleases);

        if (logger.isDebugEnabled()) {
            logger.debug(this.getName() + ": Processed " + count + " categories");
        }

        recordsUpdated = true;
    }
    
    /**
     * @return the Release
     */
    public Map<String, String> getReleases() {
        return releases;
    }

    @Override
    public Record getRecord(String id) {
        throw new UnsupportedOperationException("ReleasePortal has no Records.");
    }

    @Override
    public Map<String, Record> getRecords() {
        // Easier to return an empty map here.
        return Collections.emptyMap();
    }

    @Override
    protected void removeRecord(String number) {
        throw new UnsupportedOperationException("ReleasePortal has no Records.");
    }

    @Override
    protected void addRecord(Record record) {
        throw new UnsupportedOperationException("ReleasePortal has no Records.");
    }

    @Override
    protected boolean hasRecord(String id) {
        throw new UnsupportedOperationException("ReleasePortal has no Records.");
    }

    @Override
    protected Record constructRecord(String[] fields) throws DashboardException {
        throw new UnsupportedOperationException("ReleasePortal has no Records.");
    }
    
    
}
