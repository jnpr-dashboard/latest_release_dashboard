/**
 * dbergstr@juniper.net, Nov 30, 2006
 *
 * Copyright (c) 2006-2009, Juniper Networks, Inc.
 * All rights reserved.
 */
package net.juniper.dash.data;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net.juniper.dash.Repository;
import net.juniper.dash.Rule;
import net.juniper.dash.Rule.Counts;

import org.apache.log4j.Logger;
import org.drools.rule.VariableRestriction.CharVariableContextEntry;

import flexjson.JSON;

/**
 * Holds the results of a rule's violation.
 *
 * @author dbergstr
 */
public class Score extends ScoreService implements Comparable<Score> {
    protected Map<String, Integer> PRspecial = new HashMap<String, Integer>();

    protected Map<String, Set<String>> PRspecialrecord = new HashMap<String, Set<String>>();

    /**  PR Related Counts and Records * */
    protected Map<String, Set<String>> recordNumbersInRs = new HashMap<String, Set<String>>();
    protected Map<String, Set<String>> problemLevels = new HashMap<String, Set<String>>();
    protected Map<String, Set<String>> fdValues = new HashMap<String, Set<String>>();

    /** Initialize the Found During values to be filtered.The following values
    * only needs to be filtered for PR related rules * */
    private Set<String> found_during_vals = new HashSet<String>(Arrays.asList(
                        new String [] { "Feature/Functional test",
                        "Production", 
                        "Product delivery test",
                        "Regression test",
                        "Qualification",
                        "Customer-specific test",
                        "Customer escalation support",
                        "Solution test",
                        "Beta test",
                        "Daily sanity",
                        "Do-no-harm test"
                        }));

    protected int countInReported_in = 0;

    protected int countBackLog = 0;

    protected int countInReported_in_feedback = 0;

    protected int countBackLog_feedback = 0;

    protected List<String> recordNumbersInReported_in = null;

    protected List<String> recordNumbersBacklog = null;

    protected List<String> recordNumbersInReported_in_feedback = null;

    protected List<String> recordNumbersBacklog_feedback = null;
    /****************  END *******************/

    /** CVBC PR count **/
    /*
     * using state of CVBC as key
     */
    protected Map<String, Set<String>> CVBCRecord = new HashMap<String, Set<String>>();

    protected Map<String, Integer> CVBCcount = new HashMap<String, Integer>();


    /**************** END ********************/

    protected static Pattern releasePat = Pattern.compile(Record.RELEASE_REGEX,
                                          Pattern.CASE_INSENSITIVE | Pattern.COMMENTS);
    protected static Pattern branchPat = Pattern.compile(Record.BRANCH_REGEX,
                                         Pattern.CASE_INSENSITIVE | Pattern.COMMENTS);


    public Score(Rule rule, String scoreType, RecordSet records, Record grouping) {
        super(rule, scoreType, records, grouping);
    }

    /**
     * Non grouping-based Scores.
     */
    public Score(Rule rule, String scoreType, RecordSet records) {
        super(rule, scoreType, records);
    }


    public Score() {
        super();
    }

    @Override
    protected void additionalAbstractionLayer() {
        split_Rns();
        filter_PlCount();
        special_pr_bug_fix_process();
        specialCVBCBacklogCount();
    }

    private void split_Rns() {
		// Split PRs for a given release into Rn bins.
        for (Record record : new TreeSet<Record>(records)) {
			// Split both open and closed PRs
            if ((record instanceof PRRecord) || 
                (record instanceof ClosedPRRecord) || 
                (record instanceof HardeningClosedPRRecord)) {
                String release = ((ScopedRecord)record).getPlanned_release();
                Matcher m = releasePat.matcher(release);
                if (m.matches()) {
                    String Rn = m.group(2);
                    if ((Rn != null) && (Rn.toUpperCase().startsWith("R"))) {
                        if (recordNumbersInRs.get(Rn) == null) {
                            recordNumbersInRs.put(Rn, new HashSet<String>());
                        }
                        recordNumbersInRs.get(Rn).add(record.getId());
                    }
                }
            }
        }
    }

    private void filter_PlCount() {
        if (rule.getCounts() == Rule.Counts.PRS) {
		    // Collect PRs based on problem level values.
            for (Record record : new TreeSet<Record>(records)) {
                if (record instanceof PRRecord) {
                    String Plval =  ((PRRecord)record).getProblem_level();
                    String fd_value =  ((PRRecord)record).getFound_during();
                    if (Plval != null) {
                        String key = Plval + "_fdvals";
                        if (problemLevels.get(Plval) == null) {
                            problemLevels.put(Plval, new HashSet<String>());
                            fdValues.put(key, new HashSet<String>());
                        }
                        problemLevels.get(Plval).add(record.getId());
                        // Filter the found during values. 
                        if (found_during_vals.contains(fd_value)) {
                            fdValues.get(key).add(record.getId()); 
                        }
                    }
                }
            }
        }
    }

    private void special_pr_bug_fix_process() {
        /* PR bug fix special stuff */
        bug_fix_process();
        prepare_default_special_counts_properties();
        /** This counts only make sense for these Rules **/
        if (Repository.PR_fix_rules_closedPR.contains(rule.getIdentifier()) ||
                Repository.PR_fix_rules_openPR.contains(rule.getIdentifier())) {
            /**
             * Some counts which only needs to be available for PR Bug fix
             */
            prepare_special_pr_bug_fix_counts();
        }

    }

    /**
     * Preparing the count for CVBC PR which looks on the CVBC states.
     * This method will separates the count for each CVBC states (yes, yes-ready-for-review, all)
     *
     * TODO... apply this storage for CVBC type to the PR state. This will eliminate a lot of processing code
     *    either in python side or Java side
     */
    private void specialCVBCBacklogCount() {
        for (Record record : new TreeSet<Record>(records)) {
            if (record instanceof UndeadCvbcRecord ) {
                UndeadCvbcRecord rec = (UndeadCvbcRecord)record;
                addCVBCPR(rec.getCust_visible_behavior_changed(), rec.getRecordId());
            }
            if (record instanceof OldPRRecord ) {
                OldPRRecord rec = (OldPRRecord)record;
                addCVBCPR(rec.getCust_visible_behavior_changed(), rec.getRecordId());
            }
        }
    }

    protected void bug_fix_process() {
        // Calculate for Reporting, mostly for PR BUG-FIX
        if (records.size() == 0) {
            /** PR FIX-related **/
            countInReported_in = 0;
            countBackLog = 0;
            countInReported_in_feedback = 0;
            countBackLog_feedback = 0;

            recordNumbersInReported_in = Collections.emptyList();
            recordNumbersBacklog = Collections.emptyList();
            recordNumbersInReported_in_feedback = Collections.emptyList();
            recordNumbersBacklog_feedback = Collections.emptyList();
        } else if ((rule.getCounts() == Rule.Counts.PRS || rule.getCounts() == Rule.Counts.CLOSEDPRS 
                   || rule.getCounts() == Rule.Counts.NEWLYINPRS ) && (grouping instanceof ReleaseRecord)) {

            // if it is group based by release...
            //    most of them should fall here
            PRspecial = new HashMap<String, Integer>();
            PRspecialrecord = new HashMap<String, Set<String>>();

            ReleaseRecord _rel = (ReleaseRecord)grouping;
            recordNumbersInReported_in = new ArrayList<String>();
            recordNumbersBacklog = new ArrayList<String>();
            recordNumbersInReported_in_feedback = new ArrayList<String>();
            recordNumbersBacklog_feedback = new ArrayList<String>();
            Set<String> recs = new HashSet<String>();
            Set<String> recsBack = new HashSet<String>();
            Set<String> recs_feedback = new HashSet<String>();
            Set<String> recsBack_feedback = new HashSet<String>();
            for (Record record : new TreeSet<Record>(records)) {
                if (record instanceof ScopedRecord ) {
                    ScopedRecord rec = (ScopedRecord) record;
                    // Format of the key
                    // [reported_in/all_release]_[state]_[branch/noBranch]
                    StringBuffer key = new StringBuffer();
                    StringBuffer key2 = new StringBuffer();
                    StringBuffer key_new = new StringBuffer();
                    StringBuffer unique_key = new StringBuffer();
                    StringBuffer unique_key2 = new StringBuffer();
                    key.append("all_release").append("_").append(rec.getState());
                    addspecialPR(key.toString(), rec.getRecordId());
                    if (pr_fix_reported_in(record, _rel, rule.getCounts())
                            ||
                            (_rel.getChildBranch().contains(rec.getPlanned_release()) &
                             pr_fix_branch_reported_in(record, rule.getCounts()))
                       ) {
                        key2.append("reported_in").append("_").append(rec.getState());
                        addspecialPR(key2.toString(), rec.getRecordId());
                    }
                    if (_rel.getChildBranch().contains(rec.getPlanned_release())) {
                        key.append("_branch"); unique_key.append("_branch");
                        key2.append("_branch"); unique_key2.append("_branch");
                        addspecialPR(key.toString(), rec.getRecordId());
                        addspecialPR(key2.toString(), rec.getRecordId());
                    } else {
                        key.append("_non_branch"); unique_key.append("_non_branch");
                        key2.append("_non_branch"); unique_key2.append("_non_branch");
                        addspecialPR(key.toString(), rec.getRecordId());
                        addspecialPR(key2.toString(), rec.getRecordId());
                    }
                } else {
                    // Other PR instance.....
                    if (record instanceof NewlyInPRFixRecord ) {
                        NewlyInPRFixRecord rec = (NewlyInPRFixRecord) record;
                        StringBuffer key_new1 = new StringBuffer();
                        if(rule.getCounts() == Rule.Counts.NEWLYINPRS){
                            key_new1.append("newly_in_pr");
                            key_new1.append("_").append(rec.getState());
                            addspecialPR(key_new1.toString(), rec.getRecordId());
                        }  
                    }
                }
            }
            countInReported_in = recs.size();
            countBackLog = recsBack.size();
            countInReported_in_feedback = recs_feedback.size();
            countBackLog_feedback = recsBack_feedback.size();
        }
    }

    @SuppressWarnings("unchecked")
    private void prepare_default_special_counts_properties() {
        Set<String> val = new HashSet<String>();
        // REPORTED-in NON BRANCH
        val = Repository.SetUnion(PRspecialrecord.get("reported_in_open_non_branch"),
                                  PRspecialrecord.get("reported_in_info_non_branch"),
                                  PRspecialrecord.get("reported_in_analyzed_non_branch"));
        if (this.rule.getCounts() == Rule.Counts.CLOSEDPRS) {
            val = Repository.SetUnion(PRspecialrecord.get("reported_in_closed_non_branch"));
        }
        this.PRspecial.put("inreport_no", val == null ? 0 : val.size());
        this.PRspecialrecord.put("inreport_no", val == null ? new HashSet<String>() : val);

        // ALL-RELEASE NON BRANCH
        val = Repository.SetUnion(PRspecialrecord.get("all_release_open_non_branch"),
                                  PRspecialrecord.get("all_release_info_non_branch"),
                                  PRspecialrecord.get("all_release_analyzed_non_branch"));
        if (this.rule.getCounts() == Rule.Counts.CLOSEDPRS) {
            val = Repository.SetUnion(PRspecialrecord.get("all_release_closed_non_branch"));
        }
        this.PRspecial.put("total_no", val == null ? 0 : val.size());
        this.PRspecialrecord.put("total_no", val == null ? new HashSet<String>() : val);

        // BACKLOG NON BRANCH
        this.PRspecialrecord.put("backlog_no", Repository.SetDiff(this.PRspecialrecord.get("total_no"), this.PRspecialrecord.get("inreport_no")));
        this.PRspecial.put("backlog_no", this.PRspecialrecord.get("backlog_no").size());
    }

    @SuppressWarnings("unchecked")
    private void prepare_special_pr_bug_fix_counts() {
        Set<String> val = new HashSet<String>();
        /**
         * Start adding predefined calculated score which is used by PR bug Fix Reporting
         * [type]_[including_branch]_[feedback/blank]
         * type             = inreport, total
         * including_branch = yes     , no    , only
         */
        // REPORTED-IN INCLUDE BRANCH
        val = Repository.SetUnion(this.PRspecialrecord.get("reported_in_open"),
                                  this.PRspecialrecord.get("reported_in_info"),
                                  this.PRspecialrecord.get("reported_in_analyzed"));
        if (this.rule.getCounts() == Rule.Counts.CLOSEDPRS) {
            val = Repository.SetUnion(PRspecialrecord.get("reported_in_closed"));
        }
        this.PRspecial.put("inreport_yes", val == null ? 0 : val.size());
        this.PRspecialrecord.put("inreport_yes", val == null ? new HashSet<String>() : val);
        //  NewlyIn Pr Fix 
        if(this.rule.getIdentifier().contains("all_closed_prs_fix_n")){
            val = Repository.SetUnion(PRspecialrecord.get("newly_in_pr_closed"));
            this.PRspecial.put("newlyinpr_yes", val == null ? 0 : val.size());
            this.PRspecialrecord.put("newlyinpr_yes", val == null ? new HashSet<String>() : val);        
        }
        if(this.rule.getIdentifier().contains("all_open_prs_fix_n")){
            val = Repository.SetUnion(this.PRspecialrecord.get("newly_in_pr_open"),
                                      this.PRspecialrecord.get("newly_in_pr_info"),
                                      this.PRspecialrecord.get("newly_in_pr_analyzed"),
                                      this.PRspecialrecord.get("newly_in_pr_feedback"));
            this.PRspecial.put("newlyinpr_yes", val == null ? 0 : val.size());
            this.PRspecialrecord.put("newlyinpr_yes", val == null ? new HashSet<String>() : val);           
        }

        // REPORTED-IN INCLUDE BRANCH(FEEDBACK)
        val = this.PRspecialrecord.get("reported_in_feedback");
        this.PRspecial.put("inreport_yes_feedback", val == null ? 0 : val.size());
        this.PRspecialrecord.put("inreport_yes_feedback", val == null ? new HashSet<String>() : val);

        // REPORTED-IN NON BRANCH(FEEDBACK)
        val = this.PRspecialrecord.get("reported_in_feedback_non_branch");
        this.PRspecial.put("inreport_no_feedback", val == null ? 0 : val.size());
        this.PRspecialrecord.put("inreport_no_feedback", val == null ? new HashSet<String>() : val);

        // REPORTED-IN ONLY BRANCH
        val = Repository.SetUnion(this.PRspecialrecord.get("reported_in_open_branch"),
                                  this.PRspecialrecord.get("reported_in_info_branch"),
                                  this.PRspecialrecord.get("reported_in_analyzed_branch"));
        if (this.rule.getCounts() == Rule.Counts.CLOSEDPRS) {
            val = Repository.SetUnion(PRspecialrecord.get("reported_in_closed_branch"));
        }
        this.PRspecial.put("inreport_only", val == null ? 0 : val.size());
        this.PRspecialrecord.put("inreport_only", val == null ? new HashSet<String>() : val);
        // REPORTED-IN ONLY BRANCH(FEEDBACK)
        val = this.PRspecialrecord.get("reported_in_feedback_branch");
        this.PRspecial.put("inreport_only_feedback", val == null ? 0 : val.size());
        this.PRspecialrecord.put("inreport_only_feedback", val == null ? new HashSet<String>() : val);

        // ALL-RELEASE INCLUDE BRANCH
        val = Repository.SetUnion(this.PRspecialrecord.get("all_release_open"),
                                  this.PRspecialrecord.get("all_release_info"),
                                  this.PRspecialrecord.get("all_release_analyzed"));
        if (this.rule.getCounts() == Rule.Counts.CLOSEDPRS) {
            val = Repository.SetUnion(PRspecialrecord.get("all_release_closed"));
        }
        this.PRspecial.put("total_yes", val == null ? 0 : val.size());
        this.PRspecialrecord.put("total_yes", val == null ? new HashSet<String>() : val);
        // ALL-RELEASE INCLUDE BRANCH (FEEDBACK)
        val = this.PRspecialrecord.get("all_release_feedback");
        this.PRspecial.put("total_yes_feedback", val == null ? 0 : val.size());
        this.PRspecialrecord.put("total_yes_feedback", val == null ? new HashSet<String>() : val);

        // ALL RELEASE NOT BRANCH (FEEDBACK)
        val = this.PRspecialrecord.get("all_release_feedback_non_branch");
        this.PRspecial.put("total_no_feedback", val == null ? 0 : val.size());
        this.PRspecialrecord.put("total_no_feedback", val == null ? new HashSet<String>() : val);

        // ALL RELEASE BRANCH ONLY
        val = Repository.SetUnion(this.PRspecialrecord.get("all_release_open_branch"),
                                  this.PRspecialrecord.get("all_release_info_branch"),
                                  this.PRspecialrecord.get("all_release_analyzed_branch"));
        if (this.rule.getCounts() == Rule.Counts.CLOSEDPRS) {
            val = Repository.SetUnion(PRspecialrecord.get("all_release_closed_branch"));
        }
        this.PRspecial.put("total_only", val == null ? 0 : val.size());
        this.PRspecialrecord.put("total_only", val == null ? new HashSet<String>() : val);
        // ALL RELEASE BRANCH ONLY (FEEDBACK)
        val = this.PRspecialrecord.get("all_release_feedback_branch");
        this.PRspecial.put("total_only_feedback", val == null ? 0 : val.size());
        this.PRspecialrecord.put("total_only_feedback", val == null ? new HashSet<String>() : val);
    }

    protected void addspecialPR(String key, String newrec) {
        if (PRspecial.containsKey(key)) {
            int oldval = PRspecial.get(key);
            Set<String> oldrecval = PRspecialrecord.get(key);
            oldval++;
            oldrecval.add(newrec);
            PRspecial.remove(key); PRspecial.put(key, oldval);
            PRspecialrecord.remove(key); PRspecialrecord.put(key, oldrecval);
            oldrecval = null;
        } else {
            PRspecial.put(key, 1);
            Set<String> newval = new HashSet<String>();
            newval.add(newrec);
            PRspecialrecord.put(key, newval);
            newval = null;
        }
    }

    /**
     *
     * @param key, represents CVBC state
     * @param newrec, represents the new counts respect to CVBC state
     */
    protected void addCVBCPR(String key, String newrec) {
        if (CVBCRecord.containsKey(key)) {
            // Update existing one
            int oldval = CVBCcount.get(key);
            Set<String> oldrecval = CVBCRecord.get(key);
            oldval++;
            oldrecval.add(newrec);
            CVBCcount.remove(key); CVBCcount.put(key, oldval);
            CVBCRecord.remove(key); CVBCRecord.put(key, oldrecval);
            oldrecval = null;
        } else {
            CVBCcount.put(key, 1);
            Set<String> newval = new HashSet<String>();
            newval.add(newrec);
            CVBCRecord.put(key, newval);
            newval = null;
        }
    }

    /**
     * Condition ONLY FOR development branches in PR bug fix.
     *
     * Every PRs which is arrived after certain branch is created, will be
     * grouped as reported-in
     *
     * @param arrival_date, epoch time on when does the PR is created/arrive
     * @param Branch_name, branch name to look for in Branch Tracker
     * @return
     *  True -> part of reported-in
     *  False->not part of reported-in
     */
    private boolean pr_fix_branch_reported_in(Record record, Counts count_type) {
        boolean retval = false;
        long pr_arrival_date = 0;
        String Branch_name = "";
        if (count_type == Rule.Counts.PRS) {
            PRRecord pr_rec = (PRRecord)record;
            pr_arrival_date = pr_rec.getArrival_date_epoch();
            Branch_name = pr_rec.getPlanned_release();
        }
        if (count_type == Rule.Counts.CLOSEDPRS) {
            ClosedPRRecord pr_rec = (ClosedPRRecord)record;
            pr_arrival_date = pr_rec.getArrival_date_epoch();
            Branch_name = pr_rec.getPlanned_release();
        }

        if (DataSource.BRANCHTRACKER.hasRecordByName(Branch_name)) {
            BTRecord rec = (BTRecord)DataSource.BRANCHTRACKER.getRecordByBranchName(Branch_name);
            /**
             * Condition: Every PR which is created after the dev-branch is created will
             *     be assumed as reported-in PR
             */
            if (pr_arrival_date >= rec.getCreated_date_epoch() && rec.getBranch_type().equalsIgnoreCase("dev")) {
                retval = true;
            }
        }
        return retval;
    }

    /**
     * This is the condition for reported_in release on PR bug fix
     *    for consistency, they are all being compared in Upper case text
     * @param rec
     * @param _rel
     * @return
     */
    private boolean pr_fix_reported_in(Record rec, ReleaseRecord _rel, Counts count_type) {
        boolean retval = false;
        Set<String> releases_value = null;
        String reported_in = "";
        if (count_type == Rule.Counts.PRS) {
            PRRecord pr_rec = (PRRecord)rec;
            releases_value = pr_rec.getReleases();
            reported_in = pr_rec.getReported_in().toUpperCase();
        } else {
            ClosedPRRecord pr_rec = (ClosedPRRecord)rec;
            releases_value = pr_rec.getReleases();
            reported_in = pr_rec.getReported_in();
            reported_in = pr_rec.getReported_in().toUpperCase();
        }
        String branch_name = _rel.getRelease();
        /**
         * Iterate for all possible release value in PR/ClosedPR records and get the appropriate
         *    branch name from it.
         * Scenario to look for:
         *      - IB compared to TOT
         *      - TOT compared to TOT
         */
        for (String rels : releases_value) {
            if (DataSource.RELEASE_PORTAL.getReleases().containsKey(rels.toUpperCase())) {
                branch_name = DataSource.RELEASE_PORTAL.getReleases().get(rels.toUpperCase());
                // check if the reported-in using full name of branch
                retval = retval || reported_in.contains(branch_name.toUpperCase());
                // check if reported-in using the short name e.g ib4_10_4
                // Check on Branch Pattern
                // to cover all bases, e.g IB4_10_4_BRANCH then we look for IB4_10_4 pattern and 10_4_IB4
                Matcher mat = branchPat.matcher(branch_name);
                if (mat.lookingAt()) {
                    String temp_match = mat.group(1).toUpperCase();
                    if (mat.groupCount() >= 2) {
                        temp_match = mat.group(1).toUpperCase() + "_" + mat.group(2).toUpperCase();
                        retval = retval || reported_in.contains(temp_match);
                        temp_match = mat.group(2).toUpperCase() + "_" + mat.group(1).toUpperCase();
                        retval = retval || reported_in.contains(temp_match);
                    } else {
                        retval = retval || reported_in.contains(temp_match);
                    }
                }
            }
            /**
             * Old calculation (before Release Portal and RT), the scoring will grouped all IBs and DEVs
             * to their parent Release (TOT). To make sure it's consistency we'll do the same. Probably
             * need another thought or clarification since we are able to get accurate data from RELEASE
             * PORTAL already.
             */
            // Global Check to group all IBx and Dev to their TOT
            retval = retval || reported_in.contains(_rel.getRelease());
        }

        // This part of condition to check for TOT release or Throttle
        // for reported-in in TOT, dashboard will look for x.y or x_y format ONLY
        Matcher mat = releasePat.matcher(_rel.getRelname()); // just for cleaning TOT value
        reported_in = reported_in.toUpperCase();
        if (mat.lookingAt()) {
            retval = retval || reported_in.contains(mat.group(1).toUpperCase());
            retval = retval || reported_in.contains(mat.group(1).replace(".", "_"));
        }
        return retval;
    }

    /*
     * Debugging method to catch certain Records with their Fired Rules
     */
    private void catch_me(Rule rule, RecordSet records) {
        if (rule.getCounts() == Rule.Counts.PRS) {
            for (Record record : new TreeSet<Record>(records)) {
                PRRecord rec = (PRRecord)record;
                if (rec.getRecordId().startsWith("521589")) {
                    logger.error("PR " + rec.getRecordId() + " in rule " + rule.getIdentifier());
                    logger.error("   blocker_release = " + rec.getBlocked_release());
                    logger.error("   blocker_type = " + rec.getBlocker_types().toString());
                    logger.error("   responsible  = " + rec.getResponsible());
                }
            }
        }
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + count;
        result = prime * result + ((grouping == null) ? 0 : grouping.hashCode());
        result = prime * result + ((rule == null) ? 0 : rule.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        Score other = (Score) obj;
        if (count != other.count) return false;
        if (grouping == null) {
            if (other.grouping != null) return false;
        } else if (!grouping.equals(other.grouping)) return false;
        if (rule == null) {
            if (other.rule != null) return false;
        } else if (!rule.equals(other.rule)) return false;
        return true;
    }

    /**
     * Sorts on score descending, then daysLeft ascending, then Grouping
     * descending, then Rule.
     *
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    public int compareTo(Score o) {
        int retval = o.score - score;
        if (0 == retval) {
            // This only works when the diff is > 1 day, but that's good enough.
            retval = (int) (daysLeft - o.daysLeft);
        }
        if (0 == retval && null != grouping) {
            retval =  grouping.compareTo(o.grouping);
        }
        if (0 == retval) {
            retval =  rule.compareTo(o.rule);
        }
        return retval;
    }

    public int getCountInReported_in() {
        return countInReported_in;
    }

    public int getCountBackLog() {
        return countBackLog;
    }

    public int getCountInReported_in_feedback() {
        return countInReported_in_feedback;
    }

    public int getCountBackLog_feedback() {
        return countBackLog_feedback;
    }

    @JSON
    public Map<String, Set<String>> getRecords_by_MR() {
        return recordNumbersInRs;
    }

    @JSON
    public Map<String, Set<String>> getProblemLevels() {
        return problemLevels;
    }
    @JSON
    public Map<String, Set<String>> getFdValues() {
        return fdValues;
    }

    /**
     *  CVBC count
     */
    @JSON
    public Map<String, Set<String>> getCVBCRecord() {
        return CVBCRecord;
    }
    @JSON
    public Map<String, Integer> getCVBCcount() {
        return CVBCcount;
    }

    /**
     * return THE special PR count
     */
    @JSON
    public Map<String, Integer> getPRspecial() {
        return PRspecial;
    }
    @JSON
    public Map<String, Set<String>> getPRspecialrecord() {
        return PRspecialrecord;
    }

    @JSON
    public List<String> getRecordNumbersInReported_in() {
        return recordNumbersInReported_in;
    }

    @JSON
    public List<String> getRecordNumbersBacklog() {
        return recordNumbersBacklog;
    }

    @JSON
    public List<String> getRecordNumbersInReported_in_feedback() {
        return recordNumbersInReported_in_feedback;
    }

    @JSON
    public List<String> getRecordNumbersBacklog_feedback() {
        return recordNumbersBacklog_feedback;
    }

}
