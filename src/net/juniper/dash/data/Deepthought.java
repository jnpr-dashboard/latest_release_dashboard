/**
 * dbergstr@juniper.net, Jan 9, 2008
 *
 * Copyright (c) 2008, Juniper Networks, Inc.
 * All rights reserved.
 */
package net.juniper.dash.data;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import net.juniper.dash.DashboardException;
import net.juniper.dash.Refresher;
import net.juniper.dash.Repository;
import net.juniper.dash.SleepyTimes;
import net.juniper.dash.Refresher.TimeoutException;

import org.apache.log4j.Logger;
import org.drools.WorkingMemory;

public abstract class Deepthought extends DataSource {

    static Logger logger = Logger.getLogger(Deepthought.class.getName());

    protected String tableName = "UNKNOWN";

    private ProcessBuilder metadataProcessBuilder;

    public Deepthought(String name, String type, String recordType, String recordSingular,
            String recordPlural) {
        super(name, type, recordType, recordSingular, recordPlural);
        dropFirstLineFromExternal = true;
        dateParser = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss zzzz");
        dateParser.setLenient(false);
        picklists = new HashMap<String, Map<String, Integer>>();
    }

    @Override
    public Integer picklistOrdinal(String fname, String value) {
        Integer ordinal;
        Map<String, Integer> list = picklists.get(fname);
        if (null == list) {
            logger.warn("Request for non-existent field '" + fname +
                "' in PicklistOrdinal for DataSource " + getName());
            ordinal = 0;
        } else {
            ordinal = list.get(value);
            if (null == ordinal) {
                logger.warn("Request for non-existent value '" + value +
                    "' in field '" + fname +
                    "' in PicklistOrdinal for DataSource " + getName());
                ordinal = 0;
            }
        }
        return ordinal;
    }

    @Override
    public void init() throws DashboardException {
        propFileName = Repository.createPath(Repository.dash_special_directory , type +".properties", absolutePropPath);
        loaderProps =
            Repository.readProperties(propFileName, absolutePropPath);
        host = loaderProps.getProperty("host");
        webAppPath = loaderProps.getProperty("web_app_path");

        String reloaderPath = Repository.createPath(Repository.BIN_DIR, "/get-deep-records.pl", true);
        File f = new File(reloaderPath);
        if ( ! (f.exists() && f.canRead())) {
            // No way to test for executable, so we still might blow up later.
            throw new DashboardException("No external record reloader for " +
                    getName() + " at path " + reloaderPath);
        }
        List<String> cmd = new ArrayList<String>();
        cmd.add(reloaderPath);
        cmd.add(host);
        cmd.add(tableName);
        cmd.add(getQueryParams());
        cmd.addAll(Arrays.asList(getFieldList()));
        logger.debug("Creating deepthought records processor: " + cmd.toString());
        processBuilder = new ProcessBuilder(cmd);

        reloaderPath = Repository.createPath(Repository.BIN_DIR , "/deep-metadata.pl", true);
        f = new File(reloaderPath);
        if ( ! (f.exists() && f.canRead())) {
            // No way to test for executable, so we still might blow up later.
            throw new DashboardException("No metadata loader for " +
                getName() + " at path " + reloaderPath);
        }
        List<String> metadataCmd = new ArrayList<String>();
        metadataCmd.add(reloaderPath);
        metadataCmd.add(host);
        metadataCmd.add(tableName);
        metadataCmd.addAll(Arrays.asList(getPickLists()));
        logger.debug("Creating deepthought metadata processor: " + metadataCmd.toString());
        metadataProcessBuilder = new ProcessBuilder(metadataCmd);
    }

    @Override
    protected void additionalPreProcessing(){};
    
    /**
     * Overridden to additionally fetch PickList ordering info from an
     * external RLI::api command.
     * @throws TimeoutException
     *
     * @see net.juniper.dash.data.DataSource#getRawRecordData(net.juniper.dash.Refresher)
     */
    @SuppressWarnings("null")
    @Override
    protected List<String[]> getRawRecordData(Refresher refresher)
            throws DashboardException, TimeoutException {
        // First we fetch the picklists
        logger.info("Fetching " + getName() + " PickList ordering.");
        List<String[]> picklistData =
            refresher.getDelimitedOutput(metadataProcessBuilder,
                SleepyTimes.EXTERNAL_RELOADER_TIMEOUT.t());
        String fld = "XX not a valid field name XX";
        Map<String, Integer> pl = null;
        for (String[] data : picklistData) {
            if (! fld.equals(data[0])) {
                // New field, need a new Map
                fld = data[0];
                pl = new LinkedHashMap<String, Integer>();
                picklists.put(fld, pl);
            }
            // Compiler thinks pl might be null, but it won't be
            pl.put(data[1], new Integer(data[2]));
        }
        logger.debug("Fetched " + picklistData.size() + " lines, representing "
            + picklists.size() + " fields for " + getName() +
            " PickList ordering.");

        /* Sleep for up to 5 seconds, to avoid hitting the database with
         * multiple queries at the same time. */
        refresher.nap(Math.round(5000 * Math.random()));
        // Now fetch the actual record data
        return refresher.getDelimitedOutput(processBuilder,
            SleepyTimes.EXTERNAL_RELOADER_TIMEOUT.t());
    }

    /**
     * Implementation should return XXXRecord.getPickLists().
     *
     * @return Names of the PickList fields in this table.
     */
    protected abstract String[] getPickLists();

    /**
     * Implementation should return XXXRecord.getFieldList().
     *
     * @return The list of fields to be loaded from Deepthought.
     */
    protected abstract String[] getFieldList();

    /**
     * Implementation should return XXXRecord.queryParams.
     *
     * @return The list of Deepthought query params.
     */
    protected abstract String getQueryParams();
}
