/**
 * dbergstr@juniper.net, Oct 27, 2006
 *
 * Copyright (c) 2006-2007, Juniper Networks, Inc.
 * All rights reserved.
 */
package net.juniper.dash.data;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.Map.Entry;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import net.juniper.dash.DashboardException;
import net.juniper.dash.JSONSerializable;
import net.juniper.dash.OuterAppCommunicator;
import net.juniper.dash.Repository;
import net.juniper.dash.Refresher;
import net.juniper.dash.Refresher.TimeoutException;

import org.apache.log4j.Logger;
import org.drools.FactHandle;

import flexjson.JSONSerializer;

/**
 * Abstraction of a Deepthought table.
 *
 * @author dbergstr
 */
public class Releases extends DataSource implements JSONSerializable {

    static Logger logger = Logger.getLogger(Releases.class.getName());

    /**
     * A fake release used to catch all PRs that have no active release.
     */
    public ReleaseRecord NO_RELEASE;

    private SortedSet<ReleaseRecord> activeReleases;
    private SortedSet<ReleaseRecord> ShippedReleases;
    private Set<String> activeReleaseIds;
    private Set<String> ShippedReleaseIds;
    private Map<Date, String> ShippedReleaseIdsByDate = new HashMap<Date, String>();
    private static Map<String, String> ancestors = new HashMap<String, String>();    
    private String FutureReleaseId;
    
    private final int ServiceReleaseRetrieved = 15; 

    private Map<String, ReleaseRecord> records = new HashMap<String, ReleaseRecord>();
    private JSONSerializer serializer;
    
    private ReentrantReadWriteLock lock = new ReentrantReadWriteLock();
    
    public Releases(String name) {
        super(name, "release", "ReleaseRecord", "Release", "Releases");
        dateParser = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss zzzz");
        dateParser.setLenient(false);
        serializer = new JSONSerializer().exclude("dataSource", "factHandle");
    }

    @Override
    public void init() throws DashboardException {
//        this.workingMemory = workingMem;
        propFileName = Repository.createPath(Repository.dash_special_directory , "release.properties", absolutePropPath);
        loaderProps =
            Repository.readProperties(propFileName,absolutePropPath);
        host = loaderProps.getProperty("host");
        webAppPath = loaderProps.getProperty("web_app_path");

        String reloaderPath = Repository.BIN_DIR + "/releases.pl";
        File f = new File(reloaderPath);
        if ( ! (f.exists() && f.canRead())) {
            // No way to test for executable, so we still might blow up later.
            throw new DashboardException("No external record reloader for" +
                    " Releases at path " + reloaderPath);
        }
        List<String> cmd = new ArrayList<String>();
        cmd.add(reloaderPath);
        cmd.add(host);
        cmd.addAll(Arrays.asList(ReleaseRecord.getFieldList()));
        
        processBuilder = new ProcessBuilder(cmd);

        /* Create a pseudo-release that has a bogus relname.  This will be
         * used to catch any PRs that have no active releases. */
        int numflds = ReleaseRecord.getFieldList().length + 1;
        String[] fakeFields = new String[numflds];
        fakeFields[0] = "0.0"; // id
        fakeFields[1] = "1900-01-01"; // lastModified
        for (int i=2; i<numflds; i++) {
            fakeFields[i] = "";
        }
        NO_RELEASE = new ReleaseRecord(fakeFields, this);

        /* Now stick it in the working memory.  Since it never changes, we
         * shouldn't need to update it for any reason. */
        FactHandle fh = Repository.workingMemory.insert(NO_RELEASE);
        NO_RELEASE.setFactHandle(fh);
    }

    /**
     * Filter out inactive Releases.
     */
    @Override
    protected void filterRecords(Set<Record> toAssert,
            Map<String, Record> currentRecords) {
        SortedSet<ReleaseRecord> newActives = new TreeSet<ReleaseRecord>();
        Set<String> newActiveIds = new HashSet<String>();
        SortedSet<ReleaseRecord> newShipped = new TreeSet<ReleaseRecord>();
        Set<String> newShippedIds = new HashSet<String>();
        // clear out cache since we have new data
        
    	Date _current = new Date();
    	long last_shipped_epoch_time = 0;
    	ReleaseRecord future_release = null;
        for (Iterator<Entry<String, Record>> i =
                currentRecords.entrySet().iterator(); i.hasNext();) {
            ReleaseRecord rec = (ReleaseRecord) i.next().getValue();
            if (! rec.isActive()) {
                // Don't assert them into the rules engine.
                toAssert.remove(rec);
                notAsserted.remove(rec);
                /* And if they're in there (ie. because they were active on
                 * the previous pass), get them removed. */
                i.remove();
            } else {
                newActives.add(rec);
                newActiveIds.add(rec.getId());
            }
        	/**
        	 * check if the release already shipped
        	 */
        	if (rec.getR1_deploy_type().equals("actual") &&
        		rec.getR1_deploy().getTime() <= _current.getTime()
        	    ){
        		newShipped.add(rec);
        		newShippedIds.add(rec.getId());
        		ShippedReleaseIdsByDate.put(rec.getR1_deploy(), rec.getId());
        		if (last_shipped_epoch_time < rec.getR1_deploy().getTime()){
        			last_shipped_epoch_time = rec.getR1_deploy().getTime();
        		}
        	}
        	/** 
        	 * Get the closest R1 deploy (only need 1 closest R1 deploy date)
        	 *   we don't really need actual deploy date at this point since 
        	 *   it is looking forward. So revised or planned is good enough.
        	 */
        	if (rec.getR1_deploy().getTime() > last_shipped_epoch_time){
        		if (future_release == null){
                                // Excluding IB releases
				if(! rec.getId().startsWith("IB")) {
        				future_release = rec;
				}
        		} else {
            		Date temp = rec.getR1_deploy();
            		Long diff = temp.getTime() - last_shipped_epoch_time;
            		Long diff2 = future_release.getR1_deploy().getTime() - last_shipped_epoch_time;
            		if (diff < diff2 ){
                                // Excluding IB releases
				if(! rec.getId().startsWith("IB")) {
            				future_release = rec;
				}
            		}
        		}
           	}
        }
        activeReleases = newActives;
        activeReleaseIds = newActiveIds;
        ShippedReleases = newShipped;
        ShippedReleaseIds = newShippedIds;
        FutureReleaseId = future_release.getId();
    }

    public Set<String> getActiveReleaseIds() {
        return activeReleaseIds;
    }

    public Set<String> getShippedReleaseIds() {
        return ShippedReleaseIds;
    }

    public Set<String> getShippedReleaseIds(Date date) {
    	Set<String> result = new HashSet<String>();
	for (Iterator i = ShippedReleaseIdsByDate.entrySet().iterator(); i.hasNext();) {
			Map.Entry item = (Map.Entry)i.next();
			Date release_date = (Date)item.getKey();
			String release_id = (String)item.getValue(); 
			if (release_date.getTime() <= date.getTime() ) {
				result.add(release_id);
			}
    	} 
    	return result;
    }
    
    public String getFutureReleaseId(){
    	return FutureReleaseId;
    }
    
    public int getServiceReleaseCount(){
    	return ServiceReleaseRetrieved;
    }
    
    protected Record constructRecord(String[] fields) throws DashboardException {
        return new ReleaseRecord(fields, this);
    }

    @Override
    public ReleaseRecord getRecord(String id) {
        return records.get(id);
    }

    @Override
    public Map<String, ReleaseRecord> getRecords() {
        return Collections.synchronizedMap(records);
//        return Collections.synchronizedMap(new HashMap<String, ReleaseRecord>(records));
    }

    @Override
    public SortedSet<ReleaseRecord> getRecordCollection() {
        return new TreeSet<ReleaseRecord>(activeReleases);
    }

    @Override
    protected void removeRecord(String number) {
        Integer retries = NUMBER_OF_RETRIES;
        /* "records" is a shared resource, lock it before writing */
        while (retries > 0) {
            retries--;
            try {
                if (lock.writeLock().tryLock(500, TimeUnit.MILLISECONDS)) {
                    try {
                        records.remove(number);
                        return;
                    } finally {
                        lock.writeLock().unlock();
                    }
                } else {
                    logger.debug("unable to lock thread, will re try again");
                }
            } catch (InterruptedException e) {
                if (retries == 0) {
                    e.printStackTrace();
                }
            }
        }
        
        /* If control reaches here, it means locking failure */ 
        logger.error(this.getName() + " not able to acquire writeLock in "
                     + NUMBER_OF_RETRIES + "trials while removeRecord");
    }

    @Override
    protected void addRecord(Record record) {
        Integer retries = NUMBER_OF_RETRIES;
        /* "records" is a shared resource, lock it before writing */
        
        while (retries >0) {
            retries--;
            try {
                if (lock.writeLock().tryLock(500, TimeUnit.MILLISECONDS)) {
                    try {
                        records.put(record.getId(), (ReleaseRecord) record);
                        return;
                    } finally {
                        lock.writeLock().unlock();
                    }
                } else {
                    logger.debug("unable to lock thread, will re try again");
                }
            } catch (InterruptedException e) {
                if (retries == 0) {
                    e.printStackTrace();
                }
            }
        }

        /* If control reaches here, it means locking failure */
        logger.error(this.getName() + " not able to acquire writeLock in "
                     + NUMBER_OF_RETRIES + "trials while addRecord");
    }

    @Override
    protected boolean hasRecord(String id) {
        return records.containsKey(id);
    }

    protected String getParentRecord(String branch_records){
        
        String retval = "unscheduled";
        Boolean acquire_lock = false;
        Integer retries = NUMBER_OF_RETRIES;
        /* Lock it before accessing to ensure that the data is not accessed
           while other thread is writing. */
        while (retries > 0) {
            retries--;
            try {
                if (lock.readLock().tryLock(500, TimeUnit.MILLISECONDS)) {
                    try {
                        acquire_lock = true;
                        for (ReleaseRecord rec: records.values()){
                            if (rec.getChildBranch().contains(branch_records)){
                                retval = rec.getRelname();
                            }
                        }
                    } finally {
                        lock.readLock().unlock();
                    }
                } else {
                    logger.debug("unable to lock thread, will re try again");
                }
            } catch (InterruptedException e) {
                if (retries == 0) {
                    e.printStackTrace();
                }
            }
        }

        /* If control reaches here, it means locking failure */
        if(!acquire_lock) {
            logger.error(this.getName() + " not able to acquire readLock in "
                    + NUMBER_OF_RETRIES + "trials while getParentRecord");
        }
        return retval;
    }

    public String serializeRecords() {
        return serializer.serialize(records);
    }
    
    public String getAncestor(String branch, Map<String, String> branch_tree){        
        int i = 0;
        do {
            i++;  
            String parent = branch_tree.get(branch);
            if (parent == null || "unscheduled".equalsIgnoreCase(parent)) {
                return branch;
            } else if (Character.isDigit(parent.charAt(0))) {
            	return parent;
            }
            branch = parent;
        } while (i<1000);
        return null;
    }
    
    public void getBranchTree(Refresher refresher) throws DashboardException,TimeoutException {
        String reloaderPath = Repository.BIN_DIR + "branch-tree.pl";
        File f = new File(reloaderPath);
        if ( ! (f.exists() && f.canRead())) {
            throw new DashboardException("No external record reloader for banch tree at path " + reloaderPath);
        }
        List<String> cmd = new ArrayList<String>();
        cmd.add(reloaderPath);
        cmd.add(host);
                        
        Map<String, Object> m = new HashMap<String, Object>();
	    m.put("job", "getOutInvocationResult");
	    List<String[]> output = new ArrayList<String[]>();
	    m.put("output", output);
	    m.put("cmd", cmd);
	    ExecutorService executor = Executors.newFixedThreadPool(1);
	    executor.execute(new OuterAppCommunicator(m));
	    executor.shutdown();
	    while (!executor.isTerminated());
        
        Map<String, String> branch_tree = new HashMap<String, String>();
        for (String[] fields : output) {
             branch_tree.put(fields[0].trim().toUpperCase(), fields[1].trim().toUpperCase());       
        }
          
        for (String decestor : branch_tree.keySet()) {
             ancestors.put(decestor, getAncestor(decestor, branch_tree));
        }    
    }
    
    public void populateRecords(Refresher refresher) throws DashboardException,TimeoutException {
        super.populateRecords(refresher);
        getBranchTree(refresher);
    } 
    
    public static boolean isWithoutRelease(String branch) {
        branch = branch.trim().toUpperCase();
        String realBranch = DataSource.RELEASE_PORTAL.getReleases().get(branch);
        if (realBranch != null && !"".equals(realBranch)){
            branch = realBranch.trim().toUpperCase();
        }       
        String ancestor = ancestors.get(branch);
        if (ancestor == null) {           
            if (!ancestors.containsValue(branch) || ancestors.containsValue(branch) && !Character.isDigit(branch.charAt(0))) {
                return true;
            }     
            return false;
        } 
        return !Character.isDigit(ancestor.charAt(0));
    }     
}
