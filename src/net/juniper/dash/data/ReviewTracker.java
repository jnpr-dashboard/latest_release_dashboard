/**
 * dbergstr@juniper.net, Oct 27, 2006
 *
 * Copyright (c) 2006, Juniper Networks, Inc.
 * All rights reserved.
 */
package net.juniper.dash.data;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import net.juniper.dash.DashboardException;
import net.juniper.dash.Refresher;
import net.juniper.dash.Repository;
import net.juniper.dash.SleepyTimes;
import net.juniper.dash.Refresher.TimeoutException;

import org.apache.log4j.Logger;
import org.drools.WorkingMemory;

/**
 * PR DataSource.
 *
 * @author dbergstr
 */
public class ReviewTracker extends DataSource {

    static Logger logger = Logger.getLogger(ReviewTracker.class.getName());

    private static final String QUERY_EXPR = "--expr=state=\"open\"";
    private static final String DEBUG_QUERY_EXPR = "--expr=state==\"open\" & last-modified > \"1 weeks ago\"";

    private Map<String, ReviewRecord> records =
        new HashMap<String, ReviewRecord>();

    private ArrayList<String> queryprBaseCmd;

    private ArrayList<String> listFieldsCmd;

    public ReviewTracker(String name) {
        super(name, "reviews", "ReviewRecord", "Review", "Reviews");
        // Mon Jun 26 12:16:19 -0700 2006
        dateParser = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzzz yyyy");
        dateParser.setLenient(false);
    }

    public void init() throws DashboardException {
//        this.workingMemory = workingMem;
        propFileName = Repository.createPath(Repository.dash_special_directory , type + ".properties", absolutePropPath);
        loaderProps =
            Repository.readProperties(propFileName, absolutePropPath);
        host = loaderProps.getProperty("host");
        webAppPath = loaderProps.getProperty("web_app_path");

        String reloaderPath = loaderProps.getProperty("query_pr");
        File f = new File(reloaderPath);
        if ( ! (f.exists() && f.canRead())) {
            // No way to test for executable, so we still might blow up later.
            throw new DashboardException("Can't find query-pr at path " +
                reloaderPath);
        }
        queryprBaseCmd = new ArrayList<String>();
        queryprBaseCmd.add(reloaderPath);
        queryprBaseCmd.add(loaderProps.getProperty("extra_opts"));
        queryprBaseCmd.add("--database=" + loaderProps.getProperty("database"));
        queryprBaseCmd.add("--host=" + host);
        queryprBaseCmd.add("--port=" + loaderProps.getProperty("port"));

        listFieldsCmd = new ArrayList<String>(queryprBaseCmd);

        // Collect the review groups from RT tables
        listFieldsCmd.add("--adm-field=review-group");
        listFieldsCmd.add("--adm-subfield=name"); 

        processBuilder = new ProcessBuilder(queryprBaseCmd);

        /* We need all the ReviewGroups to be loaded before we can process our
         * raw data. */
        dependsOn = REVIEW_GROUPS;
    }

    @Override
    protected List<String[]> getRawRecordData(Refresher refresher)
            throws DashboardException, TimeoutException {

        ReviewRecord.initValidUserList();
        List<String> getRecordsCmd = new ArrayList<String>(queryprBaseCmd);
        if (Repository.testingMode) {
            getRecordsCmd.add(DEBUG_QUERY_EXPR);
        } else {
            getRecordsCmd.add(QUERY_EXPR);
        }

        List<String> fields = new ArrayList<String>();
        for (String field : ReviewRecord.getFieldList()) {
            fields.add(field);
        }
        List<String> groupfields = new ArrayList<String>();

        // Adding New Review tracker fields "review-group, reviewer, group-state, approver"
        groupfields.add("review-group");
        groupfields.add("reviewer");
        groupfields.add("group-state");
        groupfields.add("approver");
        

        // ReviewRecord needs to have this list handy
        ReviewRecord.setGroupFields(groupfields);

        fields.addAll(groupfields);
        StringBuilder format = new StringBuilder("--format=\"");
        StringBuilder formatFields = new StringBuilder();
        for (String fld : fields) {
            format.append("%s");
            format.append(Repository.FIELD_SEPARATOR);
            formatFields.append(fld);
            formatFields.append(" ");
        }
        format.append(Repository.RECORD_SEPARATOR);
        format.append("\" ");
        format.append(formatFields);
        format.append("");
        getRecordsCmd.add(format.toString());
        processBuilder.command(getRecordsCmd);
        return refresher.getDelimitedOutput(processBuilder,
            SleepyTimes.EXTERNAL_RELOADER_TIMEOUT.t());
    }

    /**
     * Get the list of review groups from gnats.
     *
     * @return A list of the review group names, minus the trailing "-review".
     * @throws TimeoutException
     */
    public List<String> getReviewGroups(Refresher refresher)
            throws DashboardException, TimeoutException {
        ProcessBuilder pb = new ProcessBuilder(listFieldsCmd);
        List<String> fields = refresher.getOutput(pb,
            SleepyTimes.EXTERNAL_RELOADER_TIMEOUT.t());
        List<String> groups = new ArrayList<String>(100);
        for (String field : fields) {
            field = field.toLowerCase();
            if (field.endsWith("-review")) {
                groups.add(field.substring(0, field.indexOf("-review")));
            }
        }
        return groups;
    }

    @Override
    protected Record constructRecord(String[] fields) throws DashboardException {
        return new ReviewRecord(fields, this);
    }

    @Override
    public Record getRecord(String id) {
        return records.get(id);
    }

    @Override
    public Map<String, Record> getRecords() {
        return new HashMap<String, Record>(records);
    }

    @Override
    protected void removeRecord(String number) {
        records.remove(number);
    }

    @Override
    protected void addRecord(Record record) {
        records.put(record.getId(), (ReviewRecord) record);
    }

    @Override
    protected boolean hasRecord(String id) {
        return records.containsKey(id);
    }
}
