/**
 * dbergstr@juniper.net, Oct 19, 2006
 *
 * Copyright (c) 2006-2009, Juniper Networks, Inc.
 * All rights reserved.
 */
package net.juniper.dash.data;

import java.util.Collection;
import java.util.HashSet;

/**
 * Set to hold Records, since Drools doesn't "do" generics.
 *
 * @author dbergstr
 */
public class RecordSet extends HashSet<Record> {

    /**
     * Here to satisfy the serializable interface.
     */
    private static final long serialVersionUID = 1L;

    public static final RecordSet EMPTY_RECORDSET = new RecordSet();

    public RecordSet() {
        super();
    }

    public RecordSet(Collection<Record> c) {
        super(c);
        c = null;
    }

    @Override
    public boolean add(Record rec) {
        if (this == EMPTY_RECORDSET) {
            throw new UnsupportedOperationException();
        }
        boolean retval = super.add(rec);
        rec = null;
        return retval;
    }
}
