/**
 * dbergstr@juniper.net, Feb 14, 2008
 *
 * Copyright (c) 2008, Juniper Networks, Inc.
 * All rights reserved.
 */
package net.juniper.dash.data;

/**
 * Allow rules to work across multiple version of PRRecord
 *
 * @author apatt
 */
public interface SharedPRRecord {

    /**
     * @return the problem level
     */
    public String getProblem_level();
}
