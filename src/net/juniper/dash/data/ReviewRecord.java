/**
 * Bean for a code review.
 *
 * dbergstr@juniper.net, Wed Mar 28 01:22:08 2007
 *
 * Copyright (c) 2007, Juniper Networks, Inc.
 * All rights reserved.
 */
package net.juniper.dash.data;

import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import net.juniper.dash.DashboardException;

/**
 *  A JUNOS Code Review
 */
public class ReviewRecord extends Record {

    private static final String[] fieldList = new String[] {
        "id",
        "last-modified",
        "arrival-date",
        "pr",
        "state",
        "synopsis",
    };

    public static String[] getFieldList() {
        return fieldList;
    }

    private static List<String> groupFields;

    /**
     * @param fields The list of group-related fields in the Review Tracker.
     */
    static void setGroupFields(List<String> fields) {
        groupFields = fields;
    }

    private Date last_modified;
    private String last_modified_string;
    private long last_modified_epoch;
    private Date arrival_date;
    private String arrival_date_string;
    private long arrival_date_epoch;
    private String pr;
    private String state;
    private String synopsis;

    private boolean hasResponsibleReviewers;

    private boolean hasResponsibleModerators;

    private boolean hasInvalidReviewers;

    private Set<User> moderators = Collections.emptySet();

    private Set<User> reviewers = Collections.emptySet();

    private Set<User> allResponsibles = Collections.emptySet();

    private int daysSinceLastModified;

    private int age;

    public ReviewRecord(String[] values, DataSource dataSource)
            throws DashboardException {
        super(values, dataSource);
        populate(values);
    }

    public Date getLast_modified() {
        return last_modified;
    }
    public String getLast_modified_string() {
        return last_modified_string;
    }
    public long getLast_modified_epoch() {
        return last_modified_epoch;
    }

    public Date getArrival_date() {
        return arrival_date;
    }
    public String getArrival_date_string() {
        return arrival_date_string;
    }
    public long getArrival_date_epoch() {
        return arrival_date_epoch;
    }

    public String getPr() {
        return pr;
    }

    public String getState() {
        return state;
    }

    /**
     * @return the age
     */
    public int getAge() {
        return age;
    }

    /**
     * @return the timeSinceModification
     */
    public int getDaysSinceLastModified() {
        return daysSinceLastModified;
    }

    public boolean getHasInvalidReviewers() {
        return hasInvalidReviewers;
    }

    public boolean getHasResponsibleModerators() {
        return hasResponsibleModerators;
    }

    public boolean getHasResponsibleReviewers() {
        return hasResponsibleReviewers;
    }

    public Set<User> getModerators() {
        return moderators;
    }

    public Set<User> getReviewers() {
        return reviewers;
    }

    public Set<User> getAllResponsibles() {
        return allResponsibles;
    }

    public boolean populate(String[] values) throws DashboardException {
        if (null == values) {
            throw new DashboardException("Null input.");
        } else if (values.length != fieldList.length + groupFields.size() + 1) {
            // values always comes with a throwaway on the end
            throw new DashboardException("Wrong number of elements: Got " +
                values.length + ", expected " + (fieldList.length +
                    groupFields.size() + 1));
        }
        boolean changed = false;
        lastUpdated = new Date();
        String lastModifiedString = values[1];
        if (lastModifiedString.trim().length() == 0) {
            // Never been modified, use arrival-date
            lastModifiedString = values[2];
        }
        Date newLastModified = dataSource.parseDate(lastModifiedString);
        if (! newLastModified.equals(lastModified)) {
            lastModified = newLastModified;
            changed = true;
        }

        last_modified = lastModified;
        last_modified_epoch = last_modified.getTime();
        last_modified_string = dataSource.yyyymmdd(last_modified);

        arrival_date = dataSource.parseDate(values[2]);
        if (arrival_date_epoch != arrival_date.getTime()) {
            arrival_date_epoch = arrival_date.getTime();
            arrival_date_string = dataSource.yyyymmdd(arrival_date);
            changed = true;
        }
        if (! values[3].equals(pr)) {
            pr = values[3];
            changed = true;
        }
        if (! values[4].equals(state)) {
            state = values[4];
            changed = true;
        }
        if (! values[5].equals(synopsis)) {
            synopsis= values[5];
            changed = true;
        }
        /* WARNING WARNING WARNING WARNING WARNING WARNING
         * If you change the number of fields, you need to adjust this value. */
        int firstGroupIndex = 6;


        int old_age = age;
        age = (int) Math.round((System.currentTimeMillis() -
            arrival_date.getTime()) / (1000 * 60 * 60 * 24.0));
        changed |= old_age != age;

        int old_dslm = daysSinceLastModified;
        daysSinceLastModified = (int) Math.round((System.currentTimeMillis() -
            last_modified.getTime()) / (1000 * 60 * 60 * 24.0));
        changed |= old_dslm != daysSinceLastModified;

        Set<User> moderators_old = moderators;
        Set<User> reviewers_old = reviewers;
        Set<User> allResponsibles_old = allResponsibles;
        moderators = null;
        reviewers = null;
        allResponsibles = null;
        boolean hasInvalidReviewers_old = hasInvalidReviewers;
        boolean hasResponsibleModerators_old = hasResponsibleModerators;
        boolean hasResponsibleReviewers_old = hasResponsibleReviewers;
        hasInvalidReviewers = false;
        hasResponsibleModerators = false;
        hasResponsibleReviewers = false;

        // Collect the reviewers name
        String assignee = values[firstGroupIndex + 1];
        if (null == assignee || assignee.length() == 0) {
            return changed;
        }
        assignee = assignee.toLowerCase();

        // Collect the review group state
        String groupState = values[firstGroupIndex + 2];

        // Not countable for approved & rejected reviews
        if ("approved".equals(groupState) ||
            "rejected".equals(groupState)) {
            return changed;
        }

        // Collect the review group
        String group = values[firstGroupIndex];

        // Collect the approvers name
        List<String> approvedBy =
            Arrays.asList(values[firstGroupIndex + 3].toLowerCase().split("[,\\s]"));

        // Keep track of moderators for all reviews.
        int mods = null == moderators ? 0 : moderators.size();

        for (String candidate : assignee.split("[,\\s]")) {
            /* assignee can sometimes look like:
             * "groupname, username, username..." */
            if (candidate.equalsIgnoreCase(group)) {
                /* Either partially or completely unassigned, thus this
                 * review is still the moderator's responsibility. */
                 int newMods = addModerator(group);
                 if (newMods > mods) {
                    /* Can't say we've got moderators unless we actually found
                     * some for the group in question. */
                     hasResponsibleModerators = true;
                     mods = newMods;
                 }
            } else if (! approvedBy.contains(candidate)) {
                User reviewer =
                        (User) DataSource.USERSOURCE.getRecord(candidate);
                if (null == reviewer) {
                    /* Reviewer invalid (this generally means "left
                     * the company"). Moderator's responsibility. */
                     hasInvalidReviewers = true;
                     addModerator(group);
                } else {
                    // This user is (among the) responsible
                    hasResponsibleReviewers = true;
                    if (null == reviewers || reviewers.size() == 0) {
                        reviewers = new HashSet<User>();
                    }
                    addResponsibles(reviewer, reviewers);
                }
           }
        }

        if (null == allResponsibles || allResponsibles.size() == 0) {
            allResponsibles = Collections.emptySet();
        }
        if (null == reviewers || reviewers.size() == 0) {
            reviewers = Collections.emptySet();
        }
        if (null == moderators || moderators.size() == 0) {
            moderators = Collections.emptySet();
        }
        changed |= ! allResponsibles.equals(allResponsibles_old);
        changed |= ! reviewers.equals(reviewers_old);
        changed |= ! moderators.equals(moderators_old);

        changed |= hasInvalidReviewers_old ^ hasInvalidReviewers;
        changed |= hasResponsibleModerators_old ^ hasResponsibleModerators;
        changed |= hasResponsibleReviewers_old ^ hasResponsibleReviewers;

        if (changed) {
            summary = new String[] {
                trimString(synopsis, maxSummarySynopsisLength),
                last_modified_string
            };

            calculateHashCode();
        }
        
        for (User u : moderators) {
            valid_moderators.add(u.getId());
        }
        for (User u : reviewers) {
            valid_reviewers.add(u.getId());
        }
        for (User u : allResponsibles) {
            valid_allResponsibles.add(u.getId());
        }
        
        return changed;
    }

    /**
     * @param reviewer
     */
    private void addResponsibles(User reviewer, Set<User> responsibleSet) {
        if (null == allResponsibles || allResponsibles.size() == 0) {
            allResponsibles = new HashSet<User>();
        }
        while (reviewer != User.JUNOS) {
            responsibleSet.add(reviewer);
            allResponsibles.add(reviewer);
            reviewer = reviewer.getManager();
        }
    }

    private int addModerator(String groupName) {
        if (null == moderators || moderators.size() == 0) {
            moderators = new HashSet<User>();
        }
        if (groupName.endsWith("-review")) {
            groupName = groupName.substring(0, groupName.length() - 7);
        }
        ReviewGroup group = ((ReviewGroup) DataSource.REVIEW_GROUPS.
            getRecord(groupName));
        if (null == group) {
            logger.warn("Review " + id + " lists unknown group '" + groupName + "'.");
            // Huh?  Bad group name.  Very unlikely.
            return 0;
        }
        for (User moderator : group.getModerators()) {
            addResponsibles(moderator, moderators);
        }
        return moderators.size();
    }

    @Override
    public String dump() {
        StringBuilder sb = dumpCommon();
        sb.append("last_modified_string => \"");
        sb.append(last_modified_string);
        sb.append("\"\nlast_modified_epoch => \"");
        sb.append(last_modified_epoch);
        sb.append("\"\narrival_date => \"");
        sb.append(arrival_date);
        sb.append("\"\ndaysSinceLastModified => \"");
        sb.append(daysSinceLastModified);
        sb.append("\"\nage => \"");
        sb.append(age);
        sb.append("\"\npr => \"");
        sb.append(pr);
        sb.append("\"\nstate => \"");
        sb.append(state);
        sb.append("\"\nhasResponsibleModerators => \"");
        sb.append(hasResponsibleModerators);
        sb.append("\"\nhasResponsibleReviewers => \"");
        sb.append(hasResponsibleReviewers);
        sb.append("\"\nhasInvalidReviewers => \"");
        sb.append(hasInvalidReviewers);
        sb.append("\"\n");

        sb.append("moderators => \"");
        for (User u : moderators) {
            sb.append(u.getId());
            sb.append(", ");
        }
        sb.append("\"\nreviewers => \"");
        for (User u : reviewers) {
            sb.append(u.getId());
            sb.append(", ");
        }
        sb.append("\"\nallResponsibles => \"");
        for (User u : allResponsibles) {
            sb.append(u.getId());
            sb.append(", ");
        }
        sb.append("\"\n");

        return sb.toString();
    }   

    //Performance tuning for user filters in rules
    private static Set<String> valid_moderators = new HashSet<String>();
    private static Set<String> valid_reviewers = new HashSet<String>();
    private static Set<String> valid_allResponsibles= new HashSet<String>();
    
    public static void initValidUserList() {
    	valid_moderators.clear();
    	valid_reviewers.clear();
    	valid_allResponsibles.clear();
    }
    
    public static Set<String> getValidUsers(String user) {
    	Set<String> s = new HashSet<String>();
    	if (user.contains(":moderators")) {
    		s.addAll(valid_moderators);
    	} 
    	if (user.contains(":reviewers")) {
    		s.addAll(valid_reviewers);
    	} 
    	if (user.contains(":allResponsibles")) {
    		s.addAll(valid_allResponsibles);
    	}
        return s;
    }
}
