/**
 * dbergstr@juniper.net, Apr 24, 2009
 *
 * Copyright (c) 2009, Juniper Networks, Inc.
 * All rights reserved.
 */
package net.juniper.dash.data;

import net.juniper.dash.DashboardException;

/**
 * A pseudo-User for an NPI program.  
 *
 * @author dbergstr
 */
public class NPIUser extends User {

    private String npi_program = "";
    private String npi_program_name = "";
    
    public NPIUser(String[] values, DataSource dataSource)
        throws DashboardException {
        super(values, dataSource);
        this.pseudoUser = true;
        this.manager = User.JUNOS;
    }

    public String getNpi_program() {
        return npi_program;
    }

    void setNpi_program(String prog) {
        npi_program = prog;
    }

    public String getNpi_program_name() {
        return npi_program_name;
    }
    
    void setNpi_program_name(String prog) {
        npi_program_name = prog;
    }
}
