/**
 * Copyright (c) 2012, Juniper Networks, Inc.
 * All rights reserved.
 */
package net.juniper.dash.data;

import java.util.HashMap;
import java.util.Map;

import flexjson.JSONSerializer;

import net.juniper.dash.JSONSerializable;
import net.juniper.dash.DashboardException;
import net.juniper.dash.Repository;

/**
 * Abstraction of a Deepthought Release Hardening table.
 *
 */
public class ReleaseHardening extends Deepthought implements JSONSerializable {
    private Map<String, ReleaseHardeningRecord> records = new HashMap<String, ReleaseHardeningRecord>();
    private JSONSerializer recordSerializer;

    public ReleaseHardening(String name) {
        super(name, "release_hardening", "ReleaseHardeningRecord", "PR", "PRs");
        tableName = "Release_Hardening";
        recordSerializer = new JSONSerializer().include("summary");
        dependsOn = GNATS;
    }
    @Override
    protected void additionalPreProcessing() { }

    @Override
    protected Record constructRecord(String[] fields) throws DashboardException {
        return new ReleaseHardeningRecord(fields, this);
    }

    @Override
    public Record getRecord(String id) {
        return records.get(id);
    }

    @Override
    public Map<String, Record> getRecords() {
        return new HashMap<String, Record>(records);
    }

    @Override
    protected void removeRecord(String number) {
        records.remove(number);
    }

    @Override
    protected void addRecord(Record record) {
        records.put(record.getId(), (ReleaseHardeningRecord) record);
    }

    @Override
    protected boolean hasRecord(String id) {
        return records.containsKey(id);
    }

    @Override
    protected String getQueryParams() {
        return Repository.testingMode ?
               ReleaseHardeningRecord.debugQueryParams : ReleaseHardeningRecord.queryParams;
    }

    @Override
    protected String[] getFieldList() {
        return ReleaseHardeningRecord.getFieldList();
    }

    @Override
    protected String[] getPickLists() {
        return ReleaseHardeningRecord.getPickLists();
    }

    public String serializeRecords() {
        return recordSerializer.serialize(records);
    }
}
