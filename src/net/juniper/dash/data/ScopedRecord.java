/**
 * dbergstr@juniper.net, Feb 14, 2008
 *
 * Copyright (c) 2008, Juniper Networks, Inc.
 * All rights reserved.
 */
package net.juniper.dash.data;

/**
 * Indicates that a class consists of scopes (sub-parts of containing
 * records).
 *
 * Originally created to handle GNATS multi-state.
 * TODO.... generated this on-fly using perl 
 *
 * @author dbergstr
 */
public interface ScopedRecord {

    /**
     * @return The id of the notional record that contains this scope.
     */
    public String getContainingRecordId();
    /**
     * 
     * @return state of the scoped  
     */
    public String getState();
    
    /**
     * 
     * @return id 
     */
    public String getId();
    public String getRecordId();
    /**
     * This field is common for all scoped
     * @return planned-release
     */
    public String getPlanned_release();
}
