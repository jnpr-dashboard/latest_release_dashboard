/**
 * smulyono@juniper.net, Sat Aug 14 08:55:56 2010
 *
 * Copyright (c) 2010, Juniper Networks, Inc.
 * All rights reserved.
 */
package net.juniper.dash.data;

import java.util.HashMap;
import java.util.Map;

import net.juniper.dash.DashboardException;
import net.juniper.dash.Repository;

/**
 * Abstraction of a Deepthought Requirements table.
 *
 * @author smulyono
 * @version 
 */
public class Requirements extends Deepthought {
    private Map<String, RequirementsRecord> records = new HashMap<String, RequirementsRecord>();

    public Requirements(String name) {
        super(name, "requirements", "RequirementsRecord", "REQ", "REQs");
        tableName = "REQUIREMENTS";
    }
    @Override
    protected void additionalPreProcessing(){ }

    @Override
    protected Record constructRecord(String[] fields) throws DashboardException {
        return new RequirementsRecord(fields, this);
    }

    @Override
    public Record getRecord(String id) {
        return records.get(id);
    }

    @Override
    public Map<String, Record> getRecords() {
        return new HashMap<String, Record>(records);
    }

    @Override
    protected void removeRecord(String number) {
        records.remove(number);
    }

    @Override
    protected void addRecord(Record record) {
        records.put(record.getId(), (RequirementsRecord) record);
        RequirementsRecord btrec = (RequirementsRecord)getRecord(record.getId());
    }

    @Override
    protected boolean hasRecord(String id) {
        return records.containsKey(id);
    }

    @Override
    protected String getQueryParams() {
        return Repository.testingMode ?
            RequirementsRecord.debugQueryParams : RequirementsRecord.queryParams;
    }

    @Override
    protected String[] getFieldList() {
        return RequirementsRecord.getFieldList();
    }

    @Override
    protected String[] getPickLists() {
        return RequirementsRecord.getPickLists();
    }
}
