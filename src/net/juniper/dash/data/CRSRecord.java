/**
 * Bean for CRS
 *
 * allenyu@juniper.net, Sep 26, 2012
 *
 * Copyright (c) 2012, Juniper Networks, Inc.
 * All rights reserved.
 */
package net.juniper.dash.data;

import java.util.Collections;
import java.util.Date;
import java.util.Set;
import java.util.HashSet;
import java.util.Arrays;

import org.apache.log4j.Logger;

import net.juniper.dash.DashboardException;

public class CRSRecord extends Record {
	
	static String cls = CRSTracker.class.getName();
    static Logger logger = Logger.getLogger(cls);

    private static final String[] fieldList = new String[] {
        "throttle_request_id",
        "user_username",
        "throttle_request_state",
        "throttle_request_type",
        "approval_tier",
        "issue_synopsis",
        "last_modified_date",
        "mgr_approval_state",
        "release_name",
        "approver_username",
        "throttle_approval_jrm_state",
        "jrm_username",
        "date_part('day',cr_life)",
        "default_approver_list"
    };

    public static String[] getFieldList() {
        return fieldList;
    }
    
    private String throttle_request_id;
    private Set<String> user_username = new HashSet<String>();
    private String throttle_request_state;
    private String throttle_request_type;
    private String approval_tier;
    private String issue_synopsis;
    private Date last_modified_date = new Date(0l);
    private String mgr_approval_state;
    private Set<String> release_names = new HashSet<String>();
    private Set<String> approver_username = new HashSet<String>();
    private String throttle_approval_jrm_state;
    private Set<String> jrm_username = new HashSet<String>();
    private int cr_life;
    private Set<String> default_approver_list = new HashSet<String>();
    
    public CRSRecord(String[] values, DataSource dataSource) throws DashboardException {
        super(values, dataSource);
        throttle_request_id = super.id;
        populate(values);
    }
    
    public String getThrottle_reques_id() {
        return throttle_request_id;
    }
    
    public Set<String> getUser_username() {
        return user_username;
    }
 
    public String getThrottle_request_state() {
        return throttle_request_state;
    }

    public String getThrottle_request_type() {
        return throttle_request_type;
    }

    public String getApproval_tier() {
        return approval_tier;
    }
    
    public String getIssue_synopsis() {
        return issue_synopsis;
    }

    public Date getLast_modified_date() {
        return last_modified_date;
    }
    
    public String getMgr_approval_state() {
        return mgr_approval_state;
    }
    
    public Set<String> getRelease_names(){
    	return release_names;
    }
    
    public Set<String> getApprover_username() {
        return approver_username;
    }
    
    public String getThrottle_approval_jrm_state() {
        return throttle_approval_jrm_state;
    }

    public Set<String> getJrm_username() {
        return jrm_username;
    }
    
    public int getCr_life() {
        return cr_life;
    }

    /*
     * Standard release name format d.d or dd.d like 9.1 or 10.3
     * release_names not starting with a digit are not considered
     */
    private String getStandard_release_name(String release_name) {
        if (release_name == null || "".equals(release_name)){ 
        	return "";
        }
        int char_relname = (int)release_name.charAt(0);
        if (char_relname < 48 || char_relname > 57){  // ASCII 48==>'0'  57==>'9'
        	return "";
        }
               
        int idx = 0;
        for (int i=1; i<release_name.length(); i++){
        	char_relname = (int)release_name.charAt(i);
        	if (char_relname < 48 && char_relname != 46  || char_relname > 57){  // ASCII 46==>'.'
        		break;
        	}
        	idx = i;
        }
        String std_relname = release_name.substring(0, idx + 1);

        if (!std_relname.contains(".")){
        	std_relname += ".0";
        }
        return std_relname;
    }
    
    public boolean populate(String[] values) throws DashboardException {
    	
        if (null == values) {
            throw new DashboardException("Null input.");
        } 
        boolean changed = false;

        if (!values[0].equals(super.id)) {
        	super.id = values[0];
            throttle_request_id = super.id;
            changed = true;
        }
        
        Set<String> set = removeAllEmptyStringElements(new HashSet<String>(Arrays.asList(values[1].split(","))));
        if (!user_username.containsAll(set)) {
        	user_username.addAll(set);
        	valid_users.addAll(user_username);
            changed = true;
        }
        
        if (!values[2].equals(throttle_request_state)) {
        	throttle_request_state = values[2];
            changed = true;
        }

        if (!values[3].equals(throttle_request_type)) {
        	throttle_request_type = values[3];
            changed = true;
        }

        if (!values[4].equals(approval_tier)) {
        	approval_tier = values[4];
            changed = true;
        }  

        if (!values[5].equals(issue_synopsis)) {
        	issue_synopsis = values[5];
            changed = true;
        } 
        
        Date adate6 = null;
        if (values[6] != null && !"".equals(values[6])){
            adate6 = dataSource.parseDate(values[6]+"00"); 
            if (adate6.getTime() != last_modified_date.getTime()) {
            	last_modified_date = adate6;
            	lastModified = adate6;
                changed = true;
            } 
        } 
        
        if (!values[7].equals(mgr_approval_state)) {
        	mgr_approval_state = values[7];
            changed = true;
        } 
        
        String release_name = null;
        if (values[8] != null && !"".equals(values[8])){		
        	release_name = getStandard_release_name(values[8]);
            if (!release_names.contains(release_name)) {
            	release_names.add(release_name);
                changed = true;
            } 
        }
        
        set = removeAllEmptyStringElements(new HashSet<String>(Arrays.asList(values[9].split(","))));
        if (!approver_username.containsAll(set)) {
        	approver_username.addAll(set);
        	valid_approvers.addAll(approver_username);
            changed = true;
        }      

        if (!values[10].equals(throttle_approval_jrm_state)) {
        	throttle_approval_jrm_state = values[10];
            changed = true;
        }  
        
        set = removeAllEmptyStringElements(new HashSet<String>(Arrays.asList(values[11].split(","))));
        if (!jrm_username.containsAll(set)) {
        	jrm_username.addAll(set);
        	valid_jrms.addAll(jrm_username);
            changed = true;
        } 
        
        if (values[12] != null) {
        	cr_life = Integer.parseInt(values[12]); 
        	changed = true;
        }
        
        if (values.length > 13 && values[13] != null){
        	set = removeAllEmptyStringElements(new HashSet<String>(Arrays.asList(values[13].split(","))));
            if (!default_approver_list.containsAll(set)) {
            	default_approver_list.addAll(set);
                changed = true;
            } 
        }
        
        if ("Open".equals(throttle_request_state)) {
        	if (approver_username.isEmpty()) {
        		approver_username.addAll(default_approver_list);
        		valid_approvers.addAll(approver_username);
        	}
        	if (jrm_username.isEmpty()) {
        		jrm_username.addAll(default_approver_list);
        		valid_jrms.addAll(jrm_username);
        	}
        }

        if (changed) {
            summary = new String[] {
                trimString(issue_synopsis, maxSummarySynopsisLength),
                dataSource.yyyymmdd(last_modified_date)
            };

            calculateHashCode();
        }

        return changed;
    }
    
    private Set<String> removeAllEmptyStringElements(Set<String> set){
    	if (set == null || set.isEmpty()){
    		return set;
    	}
    	Set<String> new_set = new HashSet<String>();
    	for (String s : set){
    		if (!"".equals(s)){
    			new_set.add(s.trim());
    		}
    	}
    	return new_set;
    }
    
    @Override
    public String dump() {
        StringBuilder sb = dumpCommon();
        sb.append("throttle_request_id => \"");
        sb.append(throttle_request_id);
        sb.append("\"\nuser_username => \"");
        sb.append(user_username);
        sb.append("\"\nthrottle_request_state => \"");
        sb.append(throttle_request_state);
        sb.append("\"\nthrottle_request_type => \"");
        sb.append(throttle_request_type);
        sb.append("\"\napproval_tier => \"");
        sb.append(approval_tier);
        sb.append("\"\nissue_synopsis => \"");
        sb.append(issue_synopsis);
        sb.append("\"\nlast_modified_date => \"");
        sb.append(dataSource.yyyymmdd(last_modified_date));
        sb.append("\"\nmgr_approval_state => \"");
        sb.append(mgr_approval_state);
        sb.append("\"\nrelease_name => \"");
        sb.append(release_names.toString());
        sb.append("\"\nthrottle_approval_jrm_state => \"");
        sb.append("\"\njrm_username => \"");
        sb.append(jrm_username);
        sb.append("\"\ncr_life => \"");
        sb.append("\"\n");
        sb.append("\"\n");

        return sb.toString();
    }
    
    /* Performance tuning: 
     * Cache all possible valid users for rules
     */
    private static Set<String> valid_users = new HashSet<String>();
    private static Set<String> valid_approvers = new HashSet<String>();
    private static Set<String> valid_jrms = new HashSet<String>();
    
    public static void initValidUserList() {
        valid_users.clear();
        valid_approvers.clear();
        valid_jrms.clear();
    }
    
    public static Set<String> getValidUsers(String user) {
    	Set<String> s = new HashSet<String>();
    	if (user.contains(":users")) {
    	    s.addAll(valid_users);
    	} 
        if (user.contains(":approvers")) {
            s.addAll(valid_approvers);
    	} 
        if (user.contains(":jrms")) {
            s.addAll(valid_jrms);
    	}
        return s;
    }
}
