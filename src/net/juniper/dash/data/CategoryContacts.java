/**
 * dbergstr@juniper.net, September 18, 2008
 *
 * Copyright (c) 2008, Juniper Networks, Inc.
 * All rights reserved.
 */
package net.juniper.dash.data;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.juniper.dash.DashboardException;
import net.juniper.dash.Refresher;
import net.juniper.dash.Repository;
import net.juniper.dash.SleepyTimes;
import net.juniper.dash.Refresher.TimeoutException;

import org.apache.log4j.Logger;

/**
 * DataSource for GNATS Category contacts.
 *
 * PRs assigned to non-employees (category aliases, mostly) will be meta-assigned
 * to the category "owner", and accrue opportunity according to the rules.
 * Thus a blocker assigned to bug-sw-ui-misc would show up in Gwynne's agenda.
 *
 * "Ownership" will be determined by looking at subfields of the categories file,
 * in order, SPoC, owner, group-owner.  If none of those are valid Users, the
 * category will be un-owned.
 *
 * PRs assigned to employees outside engineering will be listed in a separate
 * line item on the right column of the SPoC's agenda, below "Open PRs".  They
 * won't accrue opportunity, but will be accessible as an undifferentiated pile
 * of "PRs you shouldn't just forget about".  This will cover things like PRs
 * in feedback where the responsible is someone in JTAC.
 *
 * @author dbergstr
 */
public class CategoryContacts extends DataSource {

    static Logger logger = Logger.getLogger(CategoryContacts.class.getName());

    private Map<String, String> contacts = Collections.emptyMap();
    private Map<String, String> techpubContacts = Collections.emptyMap();

    private String reloaderPath;

    public CategoryContacts(String name) {
        super(name, "catcontacts", "CategoryContacts", "Category Contact",
            "Category Contacts");
    }

    public void init() throws DashboardException {
    	loaderProps = GNATS.loaderProps;
    	propFileName = GNATS.propFileName;
        reloaderPath = Repository.createPath(Repository.BIN_DIR , "get-subfields.sh", true);
        File f = new File(reloaderPath);
        if ( ! (f.exists() && f.canRead())) {
            // No way to test for executable, so we still might blow up later.
            throw new DashboardException("No external record reloader for" +
                    " CategoryContacts at path " + reloaderPath);
        }
        List<String> cmd = new ArrayList<String>();
        /* Category fetcher syntax:
         * get-categories.sh <query-pr path> <host> <port> <database> <adm_field> <subfield,subfield,...> */
        cmd.add(reloaderPath);
        cmd.add(GNATS.query_prPath);
        cmd.add(GNATS.extra_opts);     
        cmd.add(GNATS.host);
        cmd.add(GNATS.port);
        cmd.add(GNATS.database);
        cmd.add("category");
        cmd.add(GNATS.categorySubfields);
        processBuilder = new ProcessBuilder(cmd);

        /* We need all the Users to be loaded before we can process our
         * raw data. */
        dependsOn = DataSource.NPI;
    }

    @Override
    /**
     * Load records with values from an external source.
     *
     * @param refresher The Thread that is calling this method.
     * @throws DashboardException
     */
   public void populateRecords(Refresher refresher) throws DashboardException,
           TimeoutException {
       recordsUpdated = false;
       if (logger.isDebugEnabled()) {
           logger.debug("populating Records for " + getName());
       }

       List<String> output = refresher.getOutput(processBuilder,
           SleepyTimes.EXTERNAL_RELOADER_TIMEOUT.t());
       if (logger.isTraceEnabled()) {
           logger.trace(this.toString() + ": runExternal returned " +
               output.size() + " records.");
       }

       waitForDependencies(refresher);

       Map<String, String> newContacts = new HashMap<String, String>();
       Map<String, String> newTechpubContacts = new HashMap<String, String>();

       int count = 0;
       for (String categoryEntry: output) {
           String[] twoParts = categoryEntry.split("\\|");
           String category = twoParts[0];
           String[] subfield = twoParts[1].toLowerCase().split("\\s*:\\s*");
           // Find the first subfield that contains a valid SPOC.
    	   if (subfield.length >= 1){
               newContacts.put(category, subfield[0]);
    	   } else {
               newContacts.put(category, "empty_spoc");
    	   }
    	   // Find the second subfield which contains techpub Owner
    	   if (subfield.length >= 2){
               newTechpubContacts.put(category, subfield[1]);
    	   } else {
               newTechpubContacts.put(category, "empty_category_techpub_owner");
    	   }
    	   
           count++;
       }

       if (! contacts.equals(newContacts)) {
           lastRecordChangeTime = System.currentTimeMillis();
       }

       contacts = Collections.unmodifiableMap(newContacts);
       techpubContacts = Collections.unmodifiableMap(newTechpubContacts);

       if (logger.isDebugEnabled()) {
           logger.debug(this.getName() + ": Processed " + count + " categories");
       }

       recordsUpdated = true;
   }
    
    /**
     * @return the contacts
     */
    public Map<String, String> getContacts() {
        return contacts;
    }

    /**
     * 
     * @return the Techpub Owner / contacts
     */
	public Map<String, String> getTechpub_contacts() {
		return techpubContacts;
	}

	@Override
    public Record getRecord(String id) {
        throw new UnsupportedOperationException("CategoryContacts has no Records.");
    }

    @Override
    public Map<String, Record> getRecords() {
        //throw new UnsupportedOperationException("CategoryContacts has no Records.");
        // Easier to return an empty map here.
        return Collections.emptyMap();
    }

    @Override
    protected void removeRecord(String number) {
        throw new UnsupportedOperationException("CategoryContacts has no Records.");
    }

    @Override
    protected void addRecord(Record record) {
        throw new UnsupportedOperationException("CategoryContacts has no Records.");
    }

    @Override
    protected boolean hasRecord(String id) {
        throw new UnsupportedOperationException("CategoryContacts has no Records.");
    }

    @Override
    protected Record constructRecord(String[] fields) throws DashboardException {
        throw new UnsupportedOperationException("CategoryContacts has no Records.");
    }

}
