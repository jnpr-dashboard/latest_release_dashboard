/**
 * smulyono@juniper.net, February 05, 2010
 *
 * Copyright (c) 2010-2011, Juniper Networks, Inc.
 * All rights reserved.
 */
package net.juniper.dash.data;

import java.util.HashMap;
import java.util.Map;
import java.util.List;

import net.juniper.dash.DashboardException;
import net.juniper.dash.Refresher.TimeoutException;
import net.juniper.dash.Refresher;
import net.juniper.dash.Repository;

/**
 * Abstraction of a Deepthought table.
 *
 * @author smulyono
 * @version 
 */
public class BranchTracker extends Deepthought {

    private Map<String, BTRecord> records = new HashMap<String, BTRecord>();
    private Map<String, String> records_by_branchName = new HashMap<String, String>();

    public BranchTracker(String name) {
        super(name, "branch", "BTRecord", "BT", "BTs");
        tableName = "branch";
    }
    @Override
    protected void additionalPreProcessing(){ }

    @Override
    protected Record constructRecord(String[] fields) throws DashboardException {
        return new BTRecord(fields, this);
    }

    @Override
    public Record getRecord(String id) {
        return records.get(id);
    }

    @Override
    public Map<String, Record> getRecords() {
        return new HashMap<String, Record>(records);
    }

    @Override
    protected void removeRecord(String number) {
        records.remove(number);
    }

    @Override
    protected void addRecord(Record record) {
        records.put(record.getId(), (BTRecord) record);
        BTRecord btrec = (BTRecord)getRecord(record.getId());
        records_by_branchName.put(btrec.getBranch_name(), btrec.getId());
    }

    @Override
    protected boolean hasRecord(String id) {
        return records.containsKey(id);
    }

    @Override
    protected String getQueryParams() {
        return Repository.testingMode ?
            BTRecord.debugQueryParams : BTRecord.queryParams;
    }

    @Override
    protected String[] getFieldList() {
        return BTRecord.getFieldList();
    }

    @Override
    protected String[] getPickLists() {
        return BTRecord.getPickLists();
    }
    
    public Record getRecordByBranchName(String Branch_name){
    	return getRecord(records_by_branchName.get(Branch_name));
    }
    
    public boolean hasRecordByName(String Branch_name){
    	return records_by_branchName.containsKey(Branch_name);
    }
    //Performance tuning
    @Override
    protected List<String[]> getRawRecordData(Refresher refresher) throws DashboardException, TimeoutException {    	
    	BTRecord.initValidUserList();  	
        return super.getRawRecordData(refresher);
    }       
}
