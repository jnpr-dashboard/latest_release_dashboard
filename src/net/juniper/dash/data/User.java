/**
 * dbergstr@juniper.net, Dec 15, 2006
 *
 * Copyright (c) 2006-2007, Juniper Networks, Inc.
 * All rights reserved.
 */
package net.juniper.dash.data;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.Map.Entry;

import net.juniper.dash.DashboardException;
import net.juniper.dash.Repository;
import net.juniper.dash.Rule;

import org.apache.log4j.Logger;

/**
 * A user and their reporting structure.
 *
 * @author dbergstr
 */
public class User extends Record {

    static Logger logger = Logger.getLogger(User.class.getName());

    /**
     * XXX??? Pseudo users have a null manager; seems wrong...
     */
    public static final User JUNOS = new User("junos");

    public static User[] pseudoUsers() {
        return new User[] {JUNOS};
    }

    public static boolean prepPseudoUser(User pseudo, Set<User> directReports,
            Set<User> allReports) {
        pseudo.newDirectReports = directReports;
        pseudo.newReports = new HashSet<User>();
        for (User u : allReports) {
            if (!(u instanceof NPIUser)) {
                // Everyone except NPIUsers reports to junos
                pseudo.newReports.add(u);
            }
        }
        try {
            return pseudo.populate(null);
        } catch (DashboardException e) {
            // Never throws
            return false;
        }
    }

    // ======================= Instance Fields

    protected boolean pseudoUser = false;

    private Map<Record, GroupingStatus> statuses =
        new HashMap<Record, GroupingStatus>();

    private Set<Score> scores = new HashSet<Score>();

    /**
     * Working copy of non release-based scores.
     */
    private Map<String, Score> workingScores = new HashMap<String, Score>();

    private int totalScore = 0;

    private boolean scored = false;

    /**
     * When was this User last run thru the rules engine?
     */
    private Date lastScored;

    private boolean isAManager;
    /**
     * This User's manager.
     */
    protected User manager;
    private User newManager = null;

    /**
     * All users directly under this user.
     */
    private Set<User> directReports;
    private Set<User> newDirectReports;
    private Set<User> directReportUsernames;

    /**
     * All users in this user's reporting structure.
     */
    private Set<User> reports;
    private Set<User> newReports;
    private Set<String> reportUsernames;

    private boolean virtualTeam;
    /**
     * virtual team that user own
     */
    private Set<User> virtualTeams = Collections.emptySet();
    private Set<User> newVirtualTeams;
    /**
     * virtual team that user belongs to
     */
    private Set<User> virtualTeamMemberships = Collections.emptySet();
    private Set<User> newVirtualTeamMemberships;

    /** Keep track of any new scores.
     * Starts true because we need to run through finalizeScores() once,
     * even if there are no scores. */
    private boolean hasNewScores = true;

    private Map<String, Score> totalsByRuleId = Collections.emptyMap();

    private long lastViewed = 0;

    private int numViews = 0;


    private Set<String> spocCategories = Collections.emptySet();
    private Set<String> newSpocCategories;

    public User(String[] values, DataSource dataSource)
            throws DashboardException {
        super(values, dataSource);
        populate(values);
    }

    /**
     * Serves as the constructor for pseudo-users.
     */
    private User(String name) {
        super(name, DataSource.USERSOURCE);
        pseudoUser = true;
    }


    private boolean populating = false;

    /**
     * Replace manager, directReports and reports with updated values.
     *
     * @param values Unused in this class.
     * @return True if any of the fields have changed.
     */
    @Override
    public boolean populate(String[] values)
            throws DashboardException {
        populating = true;
        lastUpdated = new Date();
        boolean changed = false;
        if (!pseudoUser) {
            if (null == newManager) {
                /* We should only get here when called via the constructor. */
                return false;
            }
            if (! newManager.equals(manager)) {
                manager = newManager;
                changed = true;
            }
        }
        if (! newDirectReports.equals(directReports)) {
            directReports = newDirectReports;
            directReportUsernames =
                new HashSet<User>(directReports.size());
            for (User u : directReports) {
                directReportUsernames.add(u);
            }
            changed = true;
        }
        if (! newReports.equals(reports)) {
            reports = newReports;
            reportUsernames = new HashSet<String>(reports.size());
            for (User u : reports) {
                reportUsernames.add(u.getId());
            }
            changed = true;
        }
        if (null == newVirtualTeamMemberships &&
                virtualTeamMemberships.size() > 0) {
            virtualTeamMemberships = Collections.emptySet();
            changed = true;
        } else if (null != newVirtualTeamMemberships &&
                ! newVirtualTeamMemberships.equals(virtualTeamMemberships)) {
            virtualTeamMemberships = newVirtualTeamMemberships;
            changed = true;
        }
//        if (null == newBusMemberships &&
//                BusMemberships.size() > 0) {
//            BusMemberships = Collections.emptySet();
//            changed = true;
//        } else if (null != newBusMemberships &&
//                ! newBusMemberships.equals(BusMemberships)) {
//            BusMemberships = newBusMemberships;
//            changed = true;
//        }
        if (null == newVirtualTeams || newVirtualTeams.size() == 0) {
            virtualTeams = Collections.emptySet();
        } else if (null != newVirtualTeams &&
            ! newVirtualTeams.equals(virtualTeams)) {
            virtualTeams = new TreeSet<User>(newVirtualTeams);
            changed = true;
        }

        newManager = null;
        newReports = null;
        newDirectReports = null;
        newVirtualTeamMemberships = null;
        newVirtualTeams = null;
        
        spocsFound = false;

        if (changed) {
            /* We don't really know when the external datasource was changed,
             * so we'll just say "now"... */
            lastModified = lastUpdated;
            calculateHashCode();
        }

        if (dataSource.inReset) {
        	clearScores();
        }

        populating = false;

        return changed;
    }
    
    public void clearScores(){
        /* Clear out old data. */
        workingScores = new HashMap<String, Score>();
        for (GroupingStatus gs : statuses.values()) {
            gs.clearWorkingData();
        }
    }

    /**
     * Used only in UserSource.loadUsers();
     */
    void setManager(User newMgr) {
        newManager = newMgr;
    }

    /**
     * Add a user as a direct report.
     *
     * Note that, because of the way we get the results from LDAP, users are
     * added in alphabetical order.  This may change, in which case we'll
     * need to change newDirectReports and directReports to SortedSet.
     *
     * @param dr
     */
    void addDirectReport(User dr) {
        if (null == newDirectReports) {
            newDirectReports = new TreeSet<User>();
        }
        newDirectReports.add(dr);
    }

    void addVirtualTeam(User u) {
        if (null == newVirtualTeams) {
            newVirtualTeams= new TreeSet<User>();
        }
        newVirtualTeams.add(u);
    }

    Set<User> findAllReports() {
        if (null == newDirectReports) {
            newDirectReports = Collections.emptySet();
            newReports = Collections.singleton(this);
            isAManager = false;
        } else if (virtualTeam) {
            isAManager = false;
            newReports = new TreeSet<User>();
            newReports.addAll(newDirectReports);
            Set<User> tmpDirReports = new HashSet<User>(newDirectReports);
            for (User dr : tmpDirReports) {
                // Add all of the reports of any managers in newDirectReports
                Set<User> theirReports = dr.findAllReports();
                newReports.addAll(theirReports);
                /* Remove anyone from newDirectReports who reports to
                 * someone already in the set.  Since theirReports includes
                 * dr, we have to make a temp list first. */
                Set<User> tmpTheirReports = new HashSet<User>(theirReports);
                tmpTheirReports.remove(dr);
                newDirectReports.removeAll(tmpTheirReports);
                tmpTheirReports = null;
                theirReports = null;
            }
            for (User rep : newReports) {
                /* Tell each report that they're a member of this group. */
            	rep.setTeamMembership(this);
            }
            tmpDirReports = null;
            
        } else if (null == newReports || newReports.size() == 0) {
            newReports = new TreeSet<User>();
            newReports.add(this);
            isAManager = newDirectReports.size() > 0;
            for (User uId : newDirectReports) {
                Set<User> theirReports =uId.findAllReports();
                newReports.addAll(theirReports);
            }
        }
        if (null == newManager) {
            /* XXX??? I think we only get here if this is the mgr at the top
             * of the chain. */
            // FIXME I'm not sure this is what we want here...
            if (logger.isTraceEnabled()) {
                logger.trace("User " + id + " has no Manager, setting to JUNOS.");
            }
            newManager = JUNOS;
        }

        return newReports;
    }

    /**
     * Note that this user is a member of the given Virtual Team.
     *
     * @param vt
     */
    private void setTeamMembership(User vt) {
        if (null == newVirtualTeamMemberships) {
            newVirtualTeamMemberships = new HashSet<User>();
        }
        newVirtualTeamMemberships.add(vt);
    }

    public Set<User> getVirtualTeamMemberships() {
        return virtualTeamMemberships;
    }

    void addSpocCategory(String cat) {
        if (null == newSpocCategories) {
            newSpocCategories = new TreeSet<String>();
        }
        newSpocCategories.add(cat);
        spocsFound = false;
    }

    private boolean spocsFound = false;

    private String departmentNumber;

    Set<String> findSpocCategories() {
        if (spocsFound) {
            return spocCategories;
        }
        for (User uId : directReports) {
            Set<String> spocs = uId.findSpocCategories();
            if (! spocs.isEmpty()) {
                if (null == newSpocCategories) {
                    newSpocCategories = new TreeSet<String>();
                }
                newSpocCategories.addAll(spocs);
            }
        }
        if (null == newSpocCategories) {
            spocCategories = Collections.emptySet();
        } else {
            spocCategories = newSpocCategories;
        }
        newSpocCategories = null;
        spocsFound = true;
        return spocCategories;
    }

    /**
     * Do NOT use this in rules, as it is calculated in a way that is incompatible
     * with Drools usage.
     *
     * @return The Set of categories this User is the SPoC for.
     */
    public Set<String> getSpocCategories() {
        return spocCategories;
    }

    /**
     * @return true if this is the JUNOS user
     */
    public boolean isJunos() {
        return this == JUNOS;
    }

    /**
     * @return true if this is a pseudo user
     */
    public boolean isPseudoUser() {
        return pseudoUser;
    }

    public boolean isVirtualTeam() {
        return virtualTeam;
    }

    void setVirtualTeam() {
        this.virtualTeam = true;
    }

    /**
     * @return true if this user has reports.
     */
    public boolean isAManager() {
        return isAManager;
    }

    /**
     * @return the reports
     */
    public Set<User> getReports() {
        return Collections.unmodifiableSet(reports);
    }

    /**
     * @return the usernames of all reports
     */
    public Set<String> getReportNames() {
        while (populating) {
            try {
                Thread.sleep(1000);
            } catch (Exception e){
            }
        }
        return Collections.unmodifiableSet(reportUsernames);
    }

    /**
     * @return the usernames of all directReports
     */
    public Set<User> getDirectReportNames() {
        return  directReportUsernames;
    }

    /**
     * @return the directReports
     */
    public Set<User> getDirectReports() {
        return Collections.unmodifiableSet(directReports);
    }

    /**
     * @return the virtual teams owned by this User
     */
    public Set<User> getVirtualTeams() {
        return Collections.unmodifiableSet(virtualTeams);
    }

    /**
     * @return this user's manager.
     */
    public User getManager() {
        return manager;
    }

    void setDepartmentNumber(String val) {
        departmentNumber = val;
    }

    public String getDepartmentNumber() {
        return departmentNumber;
    }

    /**
     * Only used in the NPIUser subclass.
     */
    public String getNpi_program() {
        return "BZZT!";
    }

    /**
     * Only used in the NPIUser subclass.
     */
    public String getNpi_program_name() {
        return "BZZT!";
    }

    public String toString() {
        return id;
    }

    /**
     * Users sort by descending score, then alphabetically by username.
     *
     * XXX??? We're sorting on score, which is not used in equals().  However,
     * since all users have unique names, this shouldn't be an issue.
     */
    public int compareTo(Record o) {
        int retval = 0;
        if (o instanceof User) {
            retval = ((User) o).totalScore - totalScore;
        }
        if (0 == retval) {
            retval = id.compareTo(o.id);
        }
        return retval;
    }

    public GroupingStatus getGroupingStatus(Record grouping) {
        synchronized (statuses) {
            GroupingStatus retval = statuses.get(grouping);
            if (null == retval) {
                retval = new GroupingStatus(grouping, id, lastScored);
                statuses.put(grouping, retval);
            }
            return retval;
        }
    }

    /**
     * @return The Map of Record to GroupingStatus -- basically the per-Release
     * scores.
     */
    public Map<Record, GroupingStatus> getStatuses() {
        return statuses;
    }

    /**
     * The rules yield Collections, the size of which is the
     * count of violations.
     */
    public void setScore(String ruleId, String scoreType, RecordSet rset, Record grouping) {
        if (logger.isTraceEnabled() && (scored || rset.size() > 0)) {
            logger.trace("Scoring user " + id + " for rule " + ruleId +
                " in grouping " + grouping.getName() + ": cnt=" + rset.size());
        }
        Rule rule = Repository.getRule(ruleId);
        synchronized (this) {
            if (rset.size() == 0) {
                // Remove any score we may have for this rule.
                getGroupingStatus(grouping).unsetScore(ruleId);
            } else {
                getGroupingStatus(grouping).setScore(ruleId,
                    new Score(rule, scoreType, rset, grouping));
            }
            hasNewScores = true;
        }
    }

    /**
     * For Rules that are not grouping-based.
     */
    public void setScore(String ruleId, String scoreType, RecordSet rset) {
        if (logger.isTraceEnabled() && (scored || rset.size() > 0)) {
            logger.trace("Scoring user " + id + " for rule " + ruleId +
                ": cnt=" + rset.size());
        }
        Rule rule = Repository.getRule(ruleId);
        synchronized (this) {
            if (rset.size() == 0) {
                // Remove any score we may have for this rule.
                workingScores.remove(ruleId);
            } else {
                workingScores.put(ruleId, new Score(rule, scoreType, rset));
            }
            hasNewScores = true;
        }
    }

    /**
     * Called after rule processing to make the new scores available to readers.
     *
     * @return true if any changes were made to the user in this cycle.
     */
    public synchronized boolean finalizeScores(Date lastUpdateTime) {
        if (null == factHandle) {
            /* This user hasn't yet been through the rules engine, and
             * thus hasn't actually been scored. */
            return false;
        }

        this.lastScored = lastUpdateTime;
        if (Repository.needToCullRules) {
            /* One or more rules have been deleted, deactivated, or reloaded
             * (changing the actual object).  Reload them from the Repository
             * and, if needed, remove them from our scores. */
            for (Iterator<Entry<String, Score>> i =
                    workingScores.entrySet().iterator(); i.hasNext();) {
                Score s = i.next().getValue();
                hasNewScores |= s.reloadRule();
                if (null == s.getRule() || ! s.getRule().isActive()) {
                    i.remove();
                    hasNewScores = true;
                }
            }
        }
        /* Update scores as time passes. */
        for (Score s : workingScores.values()) {
            hasNewScores |= s.calculateScore();
        }
        /* Update scores in GroupingStatuses, and cull if necessary */
        for (GroupingStatus gs : statuses.values()) {
            hasNewScores |= gs.updateScores();
        }

        if (! hasNewScores) {
            // Nothing has changed since last time through, we're done.
            return false;
        }
        hasNewScores = false;
        scores = new HashSet<Score>(workingScores.values());
        int newTotalScore = 0;
        for (Score s : scores) {
        	s.calculateScore();
            newTotalScore = newTotalScore + s.getScore();
        }
        Map<String, Score> newTotalsByRuleId = new HashMap<String, Score>();
        /* finalize GroupingStatuses, and build per-rule release total scores. */
        for (GroupingStatus gs : statuses.values()) {
            gs.finalizeScores(lastUpdateTime);

            // Build totals for all the grouping-based rules
            for (Score s : gs.getScores()) {
                newTotalScore = newTotalScore + s.getScore();
                String ruleId = s.getRule().getIdentifier();
                Score ruleTotal = newTotalsByRuleId.get(ruleId);
                if (null == ruleTotal) {
                    ruleTotal =
                        new Score(s.getRule(), "default", new RecordSet(s.getRecords()));
                    newTotalsByRuleId.put(ruleId, ruleTotal);
                } else {
                    ruleTotal.addRecords(s.getRecords());
                }
            }
        }
        /* Finish score calculation and recordNumber population for each of
         * the "total" rules */
        for (Score s : newTotalsByRuleId.values()) {
            s.calculateScore();
        }
        totalsByRuleId = newTotalsByRuleId;
        totalScore = newTotalScore;
        scored = true;
        newTotalsByRuleId = null;

        if (logger.isTraceEnabled()) {
            logger.trace("User " + id + " score=" + totalScore );
        }
        return true;
    }

    /**
     * Additional Preprocessing Layer for PR bug fix data
     * 
     * TODO.... backlog data can also be ported here, but it will significantly
     * increase the processing time since reflection in java (which we use a lot 
     * for backlog) is heavier than using getattr in python.
     */
    protected void AdditionalPreprocessing(){
    	BugfixGroupTOT();
    	BugfixUniqueCount();
    }
    
    /**
     * Re-normalize the count by make the feedback and closed count unique.
     * Re counts the overall open with the new normalize closed count.
     * 
     */
    @SuppressWarnings("unchecked")
    private void BugfixUniqueCount(){
    	Map<String, String> rules = Repository.getBugFixRulesPair();
    	// need to check the TOT and child Branches, construct all possible releases
    	Set<String> rels_to_look = new HashSet<String>();
    	rels_to_look.addAll(Arrays.asList(Repository.PR_fix_releases.split(",")));
    	// make copy of the iterator
    	Set<String> rels_to_look_iterator = new HashSet<String>(rels_to_look);
    	for (String rel_to_look : rels_to_look_iterator){
    		rels_to_look.addAll(DataSource.RELEASES.getRecord(rel_to_look).getChildBranch());
    	}
    	for (String rel_to_look : rels_to_look){
			GroupingStatus group = this.statuses.get(DataSource.RELEASES.getRecord(rel_to_look));
			// Pull the scores
			for (String openRule : rules.keySet()){
				Score openScore = (group != null ? group.getScoreByRuleId(openRule) : null);
				// Make sure the open rules are unique
				if (openScore != null){
    		    	// make copy here so ConcurrentModificationException won't happen. The exception
					//   happens because of during the iteration, Set value is being changed.
        			Set<String> KeyCopy = new HashSet<String>(openScore.PRspecial.keySet());
        			for (String k : KeyCopy){
        				if ( (k.startsWith("inreport") || k.startsWith("total") ||
        					  k.startsWith("tot_inreport")|| k.startsWith("tot_total") ||
        					  k.startsWith("ib_inreport")|| k.startsWith("ib_total") )
        					& !(k.endsWith("feedback"))){
        					Map<String, Set<String>> pairResult = getUniqueSet(openScore.PRspecialrecord.get(k));
        					Set<String> uniqueRecs= pairResult.get("records");
        					Set<String> uniqueRecsForCount = pairResult.get("count");
        					// try see if there are feedback records associated to this counts
    						// find the unique set for feedback and remove the doubles with open
    	        			Map<String, Set<String>> pairFeedback = getUniqueSet(openScore.PRspecialrecord.get(k + "_feedback"));
    	        			Set<String> uniqueFeedback = pairFeedback .get("records");
    	        			if (uniqueFeedback.size() > 0){ 
    	        				// if feedback is 0, then it will not a concern
    	        				Map<String, Set<String>> pairUniqueFeedback = getUniqueDiff(uniqueFeedback, uniqueRecs); 
        	        			uniqueFeedback = pairUniqueFeedback.get("records");
        	        			
        	        			openScore.PRspecialrecord.put("unique_" + k + "_feedback", uniqueFeedback);
        	        			openScore.PRspecial.put("unique_" + k + "_feedback", pairUniqueFeedback.get("count").size());
    	        			}
        					openScore.PRspecialrecord.put("unique_" + k, uniqueRecs);
        					openScore.PRspecial.put("unique_" + k, uniqueRecsForCount .size());
        				}
        			}
        			group.setScore(openRule, openScore);
				}
				String closedRule = rules.get(openRule);
				// lets' takle the closed rule + feedback
				Score closedScore = (group != null ? group.getScoreByRuleId(closedRule) : null);
				if (closedScore != null){
    		    	// make copy here so ConcurrentModificationException won't happen. The exception
					//   happens because of during the iteration, Set value is being changed.
        			Set<String> KeyCopy = new HashSet<String>(closedScore.PRspecial.keySet());
        			for (String k : KeyCopy){
        				if ( (k.startsWith("inreport") || k.startsWith("total") ||
          					  k.startsWith("tot_inreport")|| k.startsWith("tot_total") ||
          					  k.startsWith("ib_inreport")|| k.startsWith("ib_total"))  
          					  & !k.contains("feedback") ){
        					// count the closed 
        					Map<String, Set<String>> pairResult = getUniqueSet(closedScore.PRspecialrecord.get(k)); 
        					Set<String> unique_recs = pairResult.get("records");
        					Set<String> unique_counts = pairResult.get("count");
        					// get the feedback count
        					// There are possibility that the open rules are null, so we need to check on that
        					if (openScore != null){
        						Map<String, Set<String>> pairFeedback = getUniqueSet(openScore.PRspecialrecord.get(k + "_feedback")); 
        	        			Set<String> unique_feedback = pairFeedback.get("records");
        	        			
        	        			Map<String, Set<String>> pairDiffFeedback = getUniqueXOR(unique_recs, unique_feedback); 
        	        			unique_recs = pairDiffFeedback.get("records");
        	        			
        	        			// update the feedback by reset the value to 0, since their counts will go to closed
        	        			openScore.PRspecialrecord.put("unique_" + k + "_feedback", new HashSet<String>());
        	        			openScore.PRspecial.put("unique_" + k + "_feedback", 0);
        	        			group.setScore(openRule, openScore);
        	        			
        	        			// we can calculate the open. Basically drops the closed if there is something open
        	        			Map<String, Set<String>> pairOpen = getUniqueSet(openScore.PRspecialrecord.get(k)); 
        	        			Set<String> unique_open = pairOpen.get("records");
        	        			pairDiffFeedback = getUniqueDiff(unique_recs, unique_open); 
        	        			unique_recs = pairDiffFeedback.get("records");
        	        			unique_counts = pairDiffFeedback.get("count");
        					} 
        					closedScore.PRspecialrecord.put("unique_" + k, unique_recs);
        					closedScore.PRspecial.put("unique_" + k,unique_counts.size());
        				}
        			}
        			group.setScore(closedRule, closedScore);
				} 
			}
    	}
    }
    
    @SuppressWarnings("unchecked")
	private void BugfixGroupTOT(){
    	Set<String> collective_rules = new HashSet<String>();
    	collective_rules.addAll(Arrays.asList(Repository.PR_fix_rules_openPR.split(",")));
    	collective_rules.addAll(Arrays.asList(Repository.PR_fix_rules_closedPR.split(",")));
    	for (String rule : collective_rules){
    		for (String rel_to_look : Repository.PR_fix_releases.split(",")){
    			GroupingStatus group = this.statuses.get(DataSource.RELEASES.getRecord(rel_to_look));
    			// Pull the scores
    			Score score = (group != null ? group.getScoreByRuleId(rule) : null);
    			if (score != null){
        			// initial data which consist the TOT only data
    		    	// make copy here so ConcurrentModificationException won't happen
        			Map<String, Set<String>> tot_records = new HashMap<String, Set<String>>(score.getPRspecialrecord());
        			for (String k : tot_records.keySet()){
        				if (k.startsWith("inreport_") || 
        					k.startsWith("total_")){
        					// initialize the ib placeholder
        					score.PRspecialrecord.put("ib_"+k, new HashSet<String>());
        					score.PRspecialrecord.put("tot_"+k, score.PRspecialrecord.get(k));
        					score.PRspecial.put("tot_"+k, score.PRspecialrecord.get(k).size());
        				}
        			}
        			// Finding their development child branches
        			for (String rel : DataSource.RELEASES.getRecord(rel_to_look).getChildBranch()){
        				GroupingStatus child_group = this.statuses.get(DataSource.RELEASES.getRecord(rel));
        				Score score_child = (child_group == null ? null : child_group.getScoreByRuleId(rule));
        				if (score_child != null){
            		    	// make copy here so ConcurrentModificationException won't happen
        					Set<String> copy_score_key= new HashSet<String>(score.getPRspecialrecord().keySet());
            				for (String k : copy_score_key){
            					if (k.startsWith("inreport_") ||
            						k.startsWith("total_")){
            						score.PRspecialrecord.put("ib_" + k, Repository.SetUnion(score.PRspecialrecord.get("ib_" + k), score_child.PRspecialrecord.get(k)));
            						score.PRspecial.put("ib_"+k, score.PRspecialrecord.get("ib_" + k).size());
            						Set<String> v = Repository.SetUnion(score_child.getPRspecialrecord().get(k), score.getPRspecialrecord().get(k));
            						score.PRspecialrecord.put( k, v);
            						score.PRspecial.put( k, v.size());
            					}
            				}
        				}
        			}
        			// Push the scores back
        			group.setScore(rule, score);
    			}
    		}
    	}
    }
    
    /**
     * @return Per-Release total Scores for this User.
     */
    public Map<String, Score> getTotalsByRuleId() {
        return totalsByRuleId;
    }

    /**
     * @return All Scores for this User.
     */
    public Set<Score> getScores() {
        return scores;
    }

    /**
     * @return The total of all scores.
     */
    public Integer getTotalScore() {
        return totalScore;
    }

    /**
     * @return true if rules have been run on this status at least once.
     */
    public boolean isScored() {
        return scored;
    }

    /**
     * Utility helper to get the unique sets of PR
     * @param sets
     * @return
     *   'count'   -> return the unique list WITHOUT the scope number
     *   'records' -> return the unique list WITH the scope number
     */
    private Map<String, Set<String>> getUniqueSet(Set<String> sets){
    	Set<String> uniquePRSet = new HashSet<String>();
    	Set<String> uniquePRrecs = new HashSet<String>();
    	if (sets != null){
        	for (String pr : sets){
        		if (!uniquePRSet.contains(PRRecord.findPRNum(pr))){
            		uniquePRSet.add(PRRecord.findPRNum(pr));
            		uniquePRrecs.add(pr);
        		}
        	}
        	sets = new HashSet<String>(uniquePRrecs);
    	}
    	Map<String, Set<String>> retval = new HashMap<String, Set<String>>();
    	retval.put("count", uniquePRSet);
    	retval.put("records", sets);
    	return retval;
    }
    
    /**
     * Remove the occurence of item in source, only if it is available in collections
     *    as implemented, it was designed to checks on PR number 
     * @param source
     * @param collections
     * @return
     *   'count'   -> return the unique list WITHOUT the scope number
     *   'records' -> return the unique list WITH the scope number
     */
    private Map<String, Set<String>> getUniqueDiff(Set<String> source, Set<String>... collections){
    	Set<String> uniquePRRecs = new HashSet<String>();
    	Set<String> uniquePRSet = new HashSet<String>();
    	if (source!= null){
    		uniquePRRecs = new HashSet<String>(source);
    		// combine the other collections to one
    		Set<String> all = new HashSet<String>();
    		for (Set<String> item : collections){
        		//combine the source into 1 unique list
    			if (item!= null){
        			for (String pr : item){
        				all.add(PRRecord.findPRNum(pr));
        			}
    			}
    		}
    		
    		// find the unique between source and all
    		for (String item : source){
    			if (item!=null){
        			if (all.contains(PRRecord.findPRNum(item))){
        				uniquePRRecs.remove(item);
        			}
    			}
    		}
    		
    		// Return the Records and counts
    		uniquePRSet= new HashSet<String>();
    		for (String item : uniquePRRecs){
    			if (item!=null){
        			uniquePRSet.add(PRRecord.findPRNum(item));
    			}
    		}
    	}
    	Map<String, Set<String>> retval = new HashMap<String, Set<String>>();
    	retval.put("count", uniquePRSet);
    	retval.put("records", uniquePRRecs);
    	return retval;
    }
    
    /**
     * 
     * @param source
     * @param collections
     * @return
     *   'count'   -> return the unique list WITHOUT the scope number
     *   'records' -> return the unique list WITH the scope number
     */
    @SuppressWarnings("unchecked")
    private Map<String, Set<String>> getUniqueXOR(Set<String> source, Set<String>... collections){
    	Set<String> uniquePRRecs = new HashSet<String>();
    	Set<String> uniquePRSet = new HashSet<String>();
    	Set<String> uniqueAllSet = new HashSet<String>();
    	Set<String> uniqueAllRecs = new HashSet<String>();
    	for (Set<String> items : collections){
    		if (items!=null){
	    		for (String item : items){
	        		if (!uniqueAllSet.contains(PRRecord.findPRNum(item))){
	        			uniqueAllSet.add(PRRecord.findPRNum(item));
	        		}
	    		}
	    		uniqueAllRecs.addAll(items);
    		}
    	}
    	
    	if (source != null){
    		// construct the collections for source
    		uniquePRRecs.addAll(source);
    		for (String item : source){
    			if (!uniquePRSet.contains(PRRecord.findPRNum(item))){
    				uniquePRSet.add(PRRecord.findPRNum(item));
    			}
    		}
    		
    		// do XOR against source and collections
    		Map<String, Set<String>> operationOne = getUniqueDiff(uniquePRRecs, uniqueAllRecs);
    		Map<String, Set<String>> operationTwo = getUniqueDiff(uniqueAllRecs, uniquePRRecs);
    		
    		uniquePRRecs = new HashSet<String>();
    		uniquePRSet = new HashSet<String>();
    		uniquePRRecs.addAll(operationOne.get("records"));
    		uniquePRRecs.addAll(operationTwo.get("records"));
    		uniquePRSet.addAll(operationOne.get("count"));
    		uniquePRSet.addAll(operationTwo.get("count"));
    		
    	} else {
    		uniquePRRecs = new HashSet<String>(uniqueAllRecs);
    		uniquePRSet = new HashSet<String>(uniqueAllSet);
    	}
    	
    	Map<String, Set<String>> retval = new HashMap<String, Set<String>>();
    	retval.put("count", uniquePRSet);
    	retval.put("records", uniquePRRecs);
    	return retval;
    }

    public Set<String> getAliases(){
        return Repository.getAliases(this.getId()); //one user could be in more than one aliases
    }
    
    public boolean isSystestOwnerAliasPartOfReportNames(String systest_owner){
        Set<String> s = Repository.getUsers(systest_owner);
        if (s != null) {
        	for (String user: s) {
            	if (this.getReportNames().contains(user) || user.equalsIgnoreCase(this.getId()))
              	    return true;
            }
        }     
        return false;
    }

    public boolean isOwned(Set<String> names){
    	if (names != null && !names.isEmpty()){
            if (names.contains(this.getId())) {
            	return true;
            }
    		for (String user: this.getReportNames()) {
    			if (names.contains(user)){
    				return true;
    			}
    		}
    		Set<String> aliases = getAliases();
    		if (aliases != null && !aliases.isEmpty()){
    			for (String alias: aliases){
    				if (names.contains(alias)){
    					return true;
    				}
    			}
    		}
    		for (String name: names) {
    			if (isSystestOwnerAliasPartOfReportNames(name)){
    				return true;
    			}
    		}
    	}   
    	return false;
    }
    
    /* Performance tuning:
     * Adding new methods which will provide a feature to the rules
     * and hence can check valid users.
     */
    public boolean isValidUser(String user) {
    	if (this == JUNOS){
    		return true;
    	}
    	return isValidNonJunosUser(user);
    }

    public boolean isValidNonJunosUser(String user) {
    
        //Fetching valid users in initial user assertion time only
        if (!DataSource.INITIAL_USER_ASSERTION) {
            return true;
        }
    
    	Set<String> s0 = getValidUsers(user);
    	if (null == s0 || s0.isEmpty()) {
    	    return false;
    	}
    	Set<String> s = new HashSet<String>(s0);
        if (s.contains(this.getId())) {
            return true;
        }
        s.retainAll(this.getReportNames());
        if (!s.isEmpty()) {
            return true;
        }
        Set<String> aliases = getAliases();
        if (null == aliases || aliases.isEmpty()) {
            return false;
        }
        s = new HashSet<String>(s0);
        s.retainAll(aliases);         
        return !s.isEmpty();
    }
    
    private Set<String> getValidUsers(String user) { 	
        if (user.startsWith("pr")){
            return PRRecord.getValidUsers(user);
        } else if (user.startsWith("crs")){
            return CRSRecord.getValidUsers(user);
        } else if (user.startsWith("rli")){
            return RLIRecord.getValidUsers(user);
        } else if (user.startsWith("oldpr")){
            return OldPRRecord.getValidUsers(user);
        } else if (user.startsWith("review")){
            return ReviewRecord.getValidUsers(user);
        } else if (user.startsWith("closedpr")){
            return ClosedPRRecord.getValidUsers(user);
        } else if (user.startsWith("undeadcvbc")){
            return UndeadCvbcRecord.getValidUsers(user);
        } else if (user.startsWith("hardeningclosedpr")){
            return HardeningClosedPRRecord.getValidUsers(user);
        } else if (user.startsWith("bt")){
            return BTRecord.getValidUsers(user);
        } else if (user.startsWith("npi")){
            return NPIRecord.getValidUsers(user);
        } else if (user.startsWith("newlyinpr")){
            return NewlyInPRFixRecord.getValidUsers(user);
        }
        return Collections.EMPTY_SET;
    }
    
    @Override
    public String dump() {
        StringBuilder sb = dumpCommon();
        sb.append("scored => \"");
        sb.append(scored);
        sb.append("\"\nlastScored => \"");
        sb.append(lastScored);
        sb.append("\"\nminutesSinceScored => \"");
        sb.append(Math.round((System.currentTimeMillis() -
            lastScored.getTime()) / 60000f));
        sb.append("\"\ntotalScore => \"");
        sb.append(totalScore);
        sb.append("\"\nnumViews=> \"");
        if (numViews > 0) {
            sb.append(numViews);
            sb.append("\"\nlastViewed=> \"");
            sb.append(new Date(lastViewed));
        } else {
            sb.append("not viewed since startup");
        }
        sb.append("\"\nmanager => \"");
        sb.append(manager);
        sb.append("\"\nisAManager => \"");
        sb.append(isAManager);
        sb.append("\"\npseudoUser => \"");
        sb.append(pseudoUser);
        sb.append("\"\nisJunos => \"");
        sb.append(isJunos());
        sb.append("\"\n");

        int i = 1;
        sb.append("spocCategories=> \"");
        for (String s : spocCategories) {
            sb.append(s);
            sb.append(", ");
            if (i++ % 5 == 0) {
                sb.append("\n");
            }
        }
        i = 1;
        sb.append("\"\nreportNames => \"");
        for (String s : reportUsernames) {
            sb.append(s);
            sb.append(", ");
            if (i++ % 10 == 0) {
                sb.append("\n");
            }
        }
        i = 1;
        sb.append("\"\ndirectReports => \"");
        for (User uId : directReports) {
            sb.append(uId.getId());
            sb.append(", ");
            if (i++ % 10 == 0) {
                sb.append("\n");
            }
        }
        sb.append("\"\n");

        return sb.toString();
    }
}
