/**
 * dbergstr@juniper.net, Oct 31, 2006
 *
 * Copyright (c) 2006-2007, Juniper Networks, Inc.
 * All rights reserved.
 */
package net.juniper.dash.data;

import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import net.juniper.dash.Repository;
import net.juniper.dash.Rule;
import flexjson.JSON;

/**
 * Holds status (scores) for a particular grouping for a User.
 *
 * @author dbergstr
 */
public class GroupingStatus implements Comparable<GroupingStatus> {

//    private static Logger logger =
//        Logger.getLogger(GroupingStatus.class.getName());

    protected Record grouping;

    protected String username;

    private Map<String, Score> scores = new HashMap<String, Score>();

    private Map<String, Score> workingScores = new HashMap<String, Score>();

    /** Keep track of any new scores.
     * Starts true because we need to run through finalizeScores() once,
     * even if there are no scores. */
    private boolean hasNewScores = true;

    private int totalScore = 0;

    private Date lastScored;

    public GroupingStatus(Record grp, String username, Date scored) {
        this.grouping = grp;
        this.username = username;
        this.lastScored = scored;
    }

    @JSON(include=false)
    public Record getGrouping() {
        return this.grouping;
    }

    @JSON
    public String getGroupingName() {
        return this.grouping.getName();
    }

    @JSON
    public String getUsername() {
        return this.username;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(grouping.getName());
        sb.append(" GroupingStatus for ");
        sb.append(username);
        return sb.toString();
    }

    public void setScore(String ruleid, Score score) {
        scores.put(ruleid, score);
        workingScores.put(ruleid, score);
        hasNewScores = true;
    }

    /**
     * Used when a rule no longer applies in this grouping.
     */
    public void unsetScore(String ruleid) {
        scores.remove(ruleid);
        workingScores.remove(ruleid);
        hasNewScores = true;
    }

    /**
     * Called during system soft restart.
     */
    void clearWorkingData() {
        scores = new HashMap<String, Score>();
        workingScores = new HashMap<String, Score>();
        hasNewScores = true;
    }

    /**
     * Remove any scores associated with deleted rules, and update scores of
     * time-based rules.
     *
     * @return true if any scores are changed or removed.
     */
    public boolean updateScores() {
        if (Repository.needToCullRules) {
            for (Iterator<Entry<String, Score>> i =
                workingScores.entrySet().iterator(); i.hasNext();) {
                Rule r = Repository.getRule(i.next().getKey());
                if (null == r || ! r.isActive()) {
                    i.remove();
                    hasNewScores |= true;
                }
            }
        }
        for (Score s : workingScores.values()) {
            hasNewScores |= s.calculateScore();
        }
        boolean tmp = hasNewScores;
        hasNewScores = false;
        return tmp;
    }

    /**
     * Called after rule processing to make the new scores available to readers.
     */
    public synchronized void finalizeScores(Date lastUpdated) {
        int tot = 0;
        for (Score s : scores.values()) {
            tot = tot + s.getScore();
        }
        totalScore = tot;
        this.lastScored = lastUpdated;
    }

    @JSON
    public int getNumScores() {
        return scores.size();
    }

    @JSON
    public Collection<Score> getScores() {
        return scores.values();
    }

    /**
     * @return The total of all scores.
     */
    @JSON
    public Integer getTotalScore() {
        return totalScore;
    }

    /**
     * Sorts on totalScore descending, lastScored ascending, then
     * grouping ascending.
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    public int compareTo(GroupingStatus o) {
        int retval = o.totalScore - totalScore;
        if (0 == retval) {
            if (null == o.lastScored && null == lastScored) {
                retval = 0;
            } else if (null == o.lastScored) {
                retval = 1;
            } else if (null == lastScored) {
                retval = -1;
            } else {
                retval = o.lastScored.compareTo(lastScored);
            }
        }
        if (0 == retval) {
            if (null == o.grouping && null == grouping) {
                retval = 0;
            } else if (null == o.grouping) {
                retval = 1;
            } else if (null == grouping) {
                retval = -1;
            } else {
                retval = o.grouping.compareTo(grouping);
            }
        }
        return retval;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     *
     * FIXME this uses mutable values, which means that unpredictable things
     * will happen if we stuff GroupingStatuses into a hash.
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
            + ((grouping == null) ? 0 : grouping.hashCode());
        result = prime * result
            + ((lastScored == null) ? 0 : lastScored.hashCode());
        result = prime * result + totalScore;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        final GroupingStatus other = (GroupingStatus) obj;
        if (grouping == null) {
            if (other.grouping != null) return false;
        } else if (!grouping.equals(other.grouping)) return false;
        if (lastScored == null) {
            if (other.lastScored != null) return false;
        } else if (!lastScored.equals(other.lastScored)) return false;
        if (totalScore != other.totalScore) return false;
        return true;
    }

    @JSON
    public Date getLastScored() {
        return lastScored;
    }

    @JSON(include=false)
    public int getMinutesSinceScored() {
        if (null == lastScored) {
            return -1;
        } else {
            return Math.round(
                (System.currentTimeMillis() - lastScored.getTime()) / 60000f);
        }
    }
    
    public Score getScoreByRuleId(String id){
    	return this.scores.get(id);
    }
}
