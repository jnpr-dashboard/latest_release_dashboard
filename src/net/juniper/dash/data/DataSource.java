/**
 * dbergstr@juniper.net, Feb 22, 2007
 *
 * Copyright (c) 2007, Juniper Networks, Inc.
 * All rights reserved.
 */
package net.juniper.dash.data;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import net.juniper.dash.DashboardException;
import net.juniper.dash.Refresher;
import net.juniper.dash.Repository;
import net.juniper.dash.SleepyTimes;
import net.juniper.dash.Refresher.TimeoutException;

import org.apache.log4j.Logger;
import org.drools.FactException;
import org.drools.FactHandle;

import flexjson.JSONSerializer;

/**
 * Superclass of various data sources (RLI, Releases, GNATS, etc.)
 *
 * populateRecords calls getRawRecordData, then Record.populate for each
 * retrieved record, then filterRecords, then reconcileAssertedRecords.
 *
 * TODO.....
 *    To gather Records, we need to separate the business and domain layer so it
 *    will be easier and we don't need all of this DEPENDS_ON thread dependency and
 *    it probably can works faster!
 *
 * @author dbergstr
 */
public abstract class DataSource implements Comparable<DataSource> {

    private static Logger logger = Logger.getLogger(DataSource.class.getName());

    public static final RLI RLI = new RLI("RLIs");
    public static final Releases RELEASES = new Releases("Releases");
    public static final Gnats GNATS = new Gnats("Gnats");
    public static final ReviewTracker REVIEWTRACKER =
        new ReviewTracker("Review Tracker");
    public static final ReviewGroups REVIEW_GROUPS =
        new ReviewGroups("Review Groups");
    public static final CRSTracker CRSTRACKER = new CRSTracker("CRS Tracker");
    public static final NPI NPI = new NPI("NPI Programs");
    public static final UserSource USERSOURCE = new UserSource("Users");
    public static final CategoryContacts CATEGORY_CONTACTS =
        new CategoryContacts("Category Contacts");
    public static final ReleasePortal RELEASE_PORTAL =
        new ReleasePortal("Release Portal");
    public static final UndeadCvbcs UNDEAD_CVBCS =
        new UndeadCvbcs("Undead CVBC PRs");
    public static final GnatsClosed GNATSCLOSED = new GnatsClosed("GnatsClosed");
    public static final GnatsNewlyInFix NEWLYINPRFIX = new GnatsNewlyInFix("NewlyInPrFix");
    public static final HardeningClosed HARDENINGCLOSED = new HardeningClosed("HardeningClosed");
    public static final HistoryGnats HISTORYGNATS = new HistoryGnats("HistoryGnats");
    public static final BranchTracker BRANCHTRACKER = new BranchTracker("BranchTracker");
    public static final ReleaseHardening HARDENING = new ReleaseHardening("Release Hardening");
    // ==PPM==.... UNCOMMENT LINE BELOW FOR PPM PROJECT
//    public static final Requirements REQUIREMENTS = new Requirements("Requirements");


    /* We use a LinkedHashMap so that one DS can depend on stuff in another's
     * init() method. */
    private static Map<String, DataSource> sources = new LinkedHashMap<String, DataSource>();

    /* Number_Of_Retries before quitting acquire lock */
    public static final int NUMBER_OF_RETRIES = 120;

    public static DataSource sourceByType(String type) {
        return sources.get(type);
    }

    public static SortedSet<DataSource> sources() {
        return new TreeSet<DataSource>(sources.values());
    }
    
    /* Performance tuning:
     * This set is a cache which holds the information of populated data
     * sources. UserSouce relies on this cache to start asserting data.
     */
    protected static Set<String> populated;
    
    /* Performance tuning:
     * A controller to fetch valid users in user assertion time
     */
    public static boolean INITIAL_USER_ASSERTION = true;

    public static void init_DataSource() {
        sources = new LinkedHashMap<String, DataSource>();
        /* Order matters here, because the init() method of some sources
         * depends on stuff that happens in other sources.  This is independent
         * of the dependsOn system, which is post-init.
         *
         * FIXME..... For simplicity we only use IF, it will be better to make this sources more
         * manageable
         * */
        sources.put(RLI.type, RLI);
        // ==PPM==.... UNCOMMENT LINE BELOW FOR PPM PROJECT
//        sources.put(REQUIREMENTS.type, REQUIREMENTS);
        // FIXME: Release Hardening data source is unnecessary, remove
//        sources.put(HARDENING.type, HARDENING);
        sources.put(USERSOURCE.type, USERSOURCE);
        sources.put(RELEASES.type, RELEASES);
        sources.put(BRANCHTRACKER.type, BRANCHTRACKER);
        sources.put(GNATS.type, GNATS);
        sources.put(GNATSCLOSED.type, GNATSCLOSED);
        sources.put(HARDENINGCLOSED.type, HARDENINGCLOSED);
        sources.put(HISTORYGNATS.type, HISTORYGNATS);
        sources.put(REVIEWTRACKER.type, REVIEWTRACKER);
        sources.put(REVIEW_GROUPS.type, REVIEW_GROUPS);
        sources.put(CRSTRACKER.type, CRSTRACKER);
        sources.put(NPI.type, NPI);
        sources.put(CATEGORY_CONTACTS.type, CATEGORY_CONTACTS);
        sources.put(RELEASE_PORTAL.type, RELEASE_PORTAL);
        sources.put(UNDEAD_CVBCS.type, UNDEAD_CVBCS);
        sources.put(NEWLYINPRFIX.type, NEWLYINPRFIX);
        populated = new HashSet<String>();
    }

    public static void initializeSources() throws DashboardException {
        for (DataSource src : sources.values()) {
            src.init();
        }
    }

    public static void resetAllSources() {
        for (DataSource src : sources.values()) {
            src.inReset = true;
            for (Record r : src.getRecords().values()) {
                r.setFactHandle(null);
            }
        }
    }

    private static JSONSerializer serializer =
        new JSONSerializer().include("class", "name", "recordType", "type", "host",
                                     "path", "recordSingular", "recordPlural", "lastRecordChangeTime").
    exclude("*");

    /**
     * @return All DataSources serialized as JSON.
     */
    public static String serializeAll() {
        return serializer.serialize(sources.values());
    }

//    protected WorkingMemory workingMemory;

    protected SimpleDateFormat dateParser;

    protected boolean dropFirstLineFromExternal = false;

    protected ProcessBuilder processBuilder;

    /** Stores a mapping of field name to picklist Map */
    protected Map<String, Map<String, Integer>> picklists;

    private String name;

    private String recordType;

    protected String host = "";

    protected String webAppPath = "";

    private SimpleDateFormat yyyymmdd = new SimpleDateFormat("yyyy-MM-dd");

    protected boolean recordsUpdated = false;

    protected DataSource dependsOn;

    /**
     * These are the Records that for various reasons weren't asserted.
     */
    protected Set<Record> notAsserted;

    protected String recordSingular;

    protected String recordPlural;

    protected String type;

    protected boolean inReset;

    protected long lastRecordChangeTime;

    protected Properties loaderProps;

    protected String propFileName;

    protected boolean absolutePropPath = true;

    public DataSource(String name, String type, String recordType,
                      String recordSingular, String recordPlural) {
        this.name = name;
        this.type = type;
        this.recordType = recordType;
        this.recordPlural = recordPlural;
        this.recordSingular = recordSingular;
    }

    public String getRecordType() {
        return recordType;
    }

    public String getName() {
        return name;
    }

    public String toString() {
        return name;
    }

    /**
     * Some Properties handler for Admin purposes.
     * @return
     */
    public Properties getLoaderProps() {
        return loaderProps;
    }

    public String getPropFileName() {
        return propFileName;
    }

    public boolean reloadProps(boolean absolute) throws DashboardException {
        boolean retval = true;
        if (!getPropFileName().equalsIgnoreCase("")) {
            loaderProps = Repository.readProperties(getPropFileName(), absolute);
            retval = loaderProps.isEmpty();
        }
        return retval;
    }

    public boolean isAbsolutePropPath() {
        return absolutePropPath;
    }

    public void setAbsolutePropPath(boolean absolutePropPath) {
        this.absolutePropPath = absolutePropPath;
    }

    /**
     * @return A JSON representation of a Map of record_number => [ synopsis,
     * last_modified].
     */
    public String getSummaries() {
        Collection <? extends Record > recs = getRecords().values();
        Map<String, String[]> out = new HashMap<String, String[]>(recs.size());
        for (Record r : recs) {
            out.put(r.getRecordId(), r.getSummary());
        }
        return serializer.serialize(out);
    }

    public String yyyymmdd(Date date) {
        if (date.getTime() == 0l) {
            // Wasn't parseable as a date in the first place.
            return "";
        }
        return yyyymmdd.format(date);
    }

    public abstract void init()
    throws DashboardException;

    protected abstract Record constructRecord(String[] fields)
    throws DashboardException;

    protected void additionalPreProcessing() {};
    /**
     * Load records with values from an external source.
     *
     * @param refresher The Thread that is calling this method.
     * @throws DashboardException
     * @throws TimeoutException
     */
    public void populateRecords(Refresher refresher) throws DashboardException,
        TimeoutException {
        recordsUpdated = false;
        if (logger.isDebugEnabled()) {
            logger.debug("populating Records for " + getName());
        }

        long start = System.currentTimeMillis();
        List<String[]> output;

        if (INITIAL_USER_ASSERTION) {
            /* Performance tuning: Making sure these exceptions not to block 
             * UserSource processing 
             */
            try {
                output = getRawRecordData(refresher);
            } catch (DashboardException de) {
                logger.debug("DataSource: " + this.type + " DashboardException.");
                logger.debug("DataSource: " + this.type + " is still added to populate in order to keep UserSource going forward.");
                populated.add(this.type);
                logger.debug("DataSource: " + this.type + " throws exception.");
                throw de;
            } catch (TimeoutException te) {
                logger.debug("DataSource: " + this.type + " TimeoutException.");
                logger.debug("DataSource: " + this.type + " is still added to populate in order to keep UserSource going forward.");
                populated.add(this.type);
                logger.debug("DataSource: " + this.type + " throws exception.");
                throw te;
            }
        } else {  
            output = getRawRecordData(refresher);       
        }
        
        logger.debug(this.getName() + ": got raw data in " +
                     (System.currentTimeMillis() - start) / 1000 + " seconds.");

        if (dropFirstLineFromExternal && output.size() > 0) {
            /* First record is a list of the fields, which we don't need */
            output.remove(0);
        }
        if (logger.isTraceEnabled()) {
            logger.trace(this.toString() + ": runExternal returned " +
                         output.size() + " records.");
        }

        // some datasources might depend on other datasources
        // for example, Gnats depends on the category contacts
        waitForDependencies(refresher);

        Map<String, Record> currentRecords = new HashMap<String, Record>();
        List<Record> toModify = new ArrayList<Record>();
        Set<Record> toAssert = new HashSet<Record>();
        if (null == notAsserted) {
            notAsserted = new HashSet<Record>();
        } else if (notAsserted.size() > 0) {
            /* In case there were errors on a previous pass, add any remaining
             * records to the assertion list. */
            toAssert.addAll(notAsserted);
        }
        int count = 0;
        for (String[] fields : output) {
            // the first field is always the totally unique identifier
            String number = fields[0];

            Record rec;
            /* XXX??? These might throw, and if they do, we'll bail on further
             * processing.  This is probably desirable, since things are
             * doubtless fubared, but it seems wrong nonetheless... */
            if (hasRecord(number)) {
                // We already have this record
                rec = getRecord(number);
                // see if it's changed
                boolean changed = rec.populate(fields);
                if (inReset) {
                    toAssert.add(rec);
                } else if (changed) {
                    toModify.add(rec);
                }
            } else {
                // Haven't seen this record yet
                rec = constructRecord(fields);
                toAssert.add(rec);
                notAsserted.add(rec);
            }
            currentRecords.put(number, rec);
            addRecord(rec);
            refresher.checkIfKilled();
            count++;
            if (logger.isTraceEnabled() && count % 1000 == 0) {
                logger.trace(this.getName() + ": Read " + count);
            }
        }
        
        if (INITIAL_USER_ASSERTION) {
            //Performance tuning: Tell UserSource that this datasource has completed data population
            populated.add(this.type);
        } 
        
        if (logger.isDebugEnabled()) {
            logger.debug(this.getName() + ": Read " + count + " records");
        }
        recordsUpdated = true;
        // do additional pre-processing (ex. RLI)
//       additionalPreProcessing();

        filterRecords(toAssert, currentRecords);

        reconcileAssertedRecords(refresher, currentRecords, toModify, toAssert);
        currentRecords = null;

    }

    /**
     * If dependsOn is non-null, wait until that DataSource has updated
     * its records, or we exceed the timeout (in which case we'll use
     * whatever data we have.
     *
     * @param refresher
     */
    protected void waitForDependencies(Refresher refresher) {
        if (null != dependsOn) {
            long waitUntil = System.currentTimeMillis() +
                             SleepyTimes.DEPENDENCY_TIMEOUT.t();
            while (! dependsOn.recordsUpdated) {
                refresher.nap(SleepyTimes.DEPENDENCY_WAIT.t());
                if (System.currentTimeMillis() > waitUntil) {
                    logger.warn(name + " timed out waiting for " + dependsOn +
                                " to update records.");
                    break;
                }
            }
        }
    }

    /**
     * Optional method to filter out any records from toAssert and
     * notAsserted that should not go into the engine, and remove any records
     * that should be retracted from currentRecords.
     */
    protected void filterRecords(@SuppressWarnings("unused")
                                 Set<Record> toAssert,
                                 @SuppressWarnings("unused") Map<String, Record> currentRecords) {
        return;
    }

    /**
     * Acquire raw data for records from the external source.
     *
     * Generally calls an external program via processBuilder.
     *
     * @throws DashboardException
     * @throws TimeoutException
     */
    protected List<String[]> getRawRecordData(Refresher refresher)
    throws DashboardException, TimeoutException {
        /* Sleep for up to 5 seconds, to avoid hitting the database with
         * multiple queries at the same time. */
        refresher.nap(Math.round(5000 * Math.random()));
        return refresher.getDelimitedOutput(processBuilder,
                                            SleepyTimes.EXTERNAL_RELOADER_TIMEOUT.t());
    }

    /**
     * Juggle the records in the working memory; assert new records, retract
     * old ones, and modify() changed ones.  Also updates the records Map.
     *
     * Any class overriding this must set lastRecordChangeTime as needed.
     *
     * @param refresher
     * @param currentRecords
     * @param toModify
     * @param toAssert
     * @throws FactException
     */
    protected void reconcileAssertedRecords(Refresher refresher,
                                            Map<String, Record> currentRecords, Collection<Record> toModify,
                                            Collection<Record> toAssert) {
        recordsUpdated = true;
        logger.debug("Reconciling asserted records...");
        int count = 0;
        boolean recordsChanged = false;
        // loop through key in records
        for (String id : new HashSet<String>(getRecords().keySet())) {
            // if id exist in drools, but not in updated data, remove from drools
            if (! currentRecords.containsKey(id)) {
                recordsChanged = true;
                FactHandle fh = getRecord(id).getFactHandle();
                if (inReset) fh = null;
                try {
                    if (null != fh) {
                        Repository.workingMemory.retract(fh);
                    } // Records w/o FactHandles were never asserted
                    removeRecord(id);
                    notAsserted.remove(getRecord(id));
                    count++;
                    if (logger.isDebugEnabled() && count % 25 == 0) {
                        logger.debug("Removed " + count);
                        refresher.checkIfKilled();
                    }
                } catch (FactException fe) {
                    String errmsg = "Error retracting " + name + " record " + id +
                                    ": " + fe.getMessage();
                    Repository.logError(errmsg);
                    logger.error(errmsg);
                }
                refresher.checkIfKilled();
                fh = null;
            }
        }
        logger.info("Retracted " + count + " old records for " + name);

        if (logger.isDebugEnabled()) {
            logger.debug("Attempt to assert = " + toAssert.size() + " new records for " + name);
        }
        count = 0;
        // add new records
        for (Record rec : toAssert) {
            recordsChanged = true;
            try {
                FactHandle fh = Repository.workingMemory.insert(rec);
                rec.setFactHandle(fh);
                notAsserted.remove(rec);
                if (logger.isDebugEnabled() && count % 50 == 0) {
                    logger.debug("Asserted " + count + " for " + name);
                    refresher.checkIfKilled();
                }
                count++;
                fh = null;
            } catch (FactException fe) {
                logger.error("Error asserting " + name + " record " +
                             rec.getId() + ": " + fe.getMessage());
            } catch (NullPointerException npe) {
                logger.error("Error asserting " + name + " record " +
                             rec.getId(), npe);
            }
        }
        logger.info("Asserted " + count + " new records for " + name);

        if (logger.isDebugEnabled()) {
            logger.debug("Attempt to modify = " + toModify.size() + " new records for " + name);
        }
        count = 0;
        // modify existing records
        for (Record rec : toModify) {
            recordsChanged = true;
            try {
                FactHandle fh = rec.getFactHandle();
                if (null != fh) {
                    Repository.workingMemory.update(fh, rec);
                    rec.updatedInDrools();
                    count++;
                    if (logger.isDebugEnabled() && count % 10 == 0) {
                        logger.debug("Modified " + count);
                        refresher.checkIfKilled();
                    }
                    fh = null;
                } else {
                    // Record wasn't properly asserted in the first place
                    logger.error(name + " record " + rec.getId() +
                                 " wasn't asserted, but needs modification.");
                    notAsserted.add(rec);
                }
            } catch (FactException fe) {
                logger.error("Error asserting " + name + " record " +
                             rec.getId() + ": " + fe.getMessage());
            }
        }
        logger.info("Modified " + count + " changed records for " + name);

        // If we were resetting, we're done now.
        inReset = false;

        if (recordsChanged) {
            lastRecordChangeTime = System.currentTimeMillis();
        }
        toAssert = null;
        toModify = null;
    }

    protected abstract boolean hasRecord(String id);

    protected abstract void addRecord(Record rec);

    protected abstract void removeRecord(String id);

    /**
     * @return The desired record.
     */
    public abstract Record getRecord(String id);

    /**
     * @return A Map of record id => Record.
     */
    public abstract Map < String, ? extends Record > getRecords();

    /**
     * This may return a filtered list of records (eg. the Releases source
     * returns all ReleaseRecords with active==true), but generally is just
     * getRecords.values().
     *
     * @return All the records the DataSource thinks you want.
     */
    public SortedSet <? extends Record > getRecordCollection() {
        return new TreeSet<Record>(getRecords().values());
    }

    /**
     * @return The epoch time at which a Record was most recently changed.
     */
    public long getLastRecordChangeTime() {
        return lastRecordChangeTime;
    }

    /**
     * Parse the input as a java.util.Date.
     *
     * @param input String to parse
     * @return a java.util.Date corresponding to the input. returns
     *     new Date(0l) if the string can't be parsed.
     */
    public Date parseDate(String input) {
        if (null == input || input.length() == 0) {
            /* that sure as heck won't parse... */
            return new Date(0l);
        }
	// TBD is considered as 14 days from now
	// Adding 14 days to today's date
	if (input.equalsIgnoreCase("TBD")) {
        	Calendar now = Calendar.getInstance();
        	now.add(Calendar.DATE, 14);
        	return now.getTime();
        }
		
        try {
            /* try to parse a full date + time */
            return dateParser.parse(input.trim());
        } catch (java.text.ParseException e) {
            return new Date(0l);
        }
    }

    /**
     * Parse a generic number into a float.
     *
     * @param input Should contain a number.
     * @return The input as a float, or 0 if it's unparseable or blank.
     */
    public float parseNumber(String input) {
        if (null == input || input.length() == 0) {
            return 0;
        }
        try {
            return Float.parseFloat(input);
        } catch (NumberFormatException nfe) {
            logger.warn("Bad number value '" + input + "' in parseNumber.");
            return 0;
        }
    }

    /**
     * Default implementation, always returns zero; override if needed in
     * a particular subclass.
     */
    public Integer picklistOrdinal(@SuppressWarnings("unused") String fname,
                                   @SuppressWarnings("unused") String value) {
        return 0;
    }


    /**
     * @return the dependsOn
     */
    public DataSource getDependsOn() {
        return dependsOn;
    }


    /**
     * @return the notAsserted
     */
    public Set<Record> getNotAsserted() {
        return notAsserted;
    }


    /**
     * @return true if records have been updated (part of the dependency system).
     */
    public boolean isRecordsUpdated() {
        return recordsUpdated;
    }

    /**
     * Not applicable to most DataSources.
     *
     * @return the picklist ordering data.
     */
    public Map<String, Map<String, Integer>> getPicklists() {
        if (null == picklists) {
            return Collections.emptyMap();
        } else {
            return Collections.unmodifiableMap(picklists);
        }
    }

    public String getRecordSingular() {
        return recordSingular;
    }

    public String getRecordPlural() {
        return recordPlural;
    }

    public String getType() {
        return type;
    }

    /**
     * Optional method.
     *
     * @return The name of the host this source resides on (eg. for building
     * a URL to a record list).
     */
    public String getHost() {
        return host;
    }

    /**
     * @return The path to the web app on the host (for building URLs).
     */
    public String getPath() {
        return webAppPath;
    }

    public int compareTo(DataSource other) {
        if (null == other) {
            return 1;
        }
        return name.compareTo(other.name);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                 + ((recordType == null) ? 0 : recordType.hashCode());
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        final DataSource other = (DataSource) obj;
        if (recordType == null) {
            if (other.recordType != null) return false;
        } else if (!recordType.equals(other.recordType)) return false;
        if (type == null) {
            if (other.type != null) return false;
        } else if (!type.equals(other.type)) return false;
        return true;
    }
}
