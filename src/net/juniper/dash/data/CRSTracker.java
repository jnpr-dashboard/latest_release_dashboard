/**
 * allenyu@juniper.net, Sep 26, 2012
 *
 * Copyright (c) 2012, Juniper Networks, Inc.
 * All rights reserved.
 */
package net.juniper.dash.data;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import net.juniper.dash.DashboardException;
import net.juniper.dash.Refresher;
import net.juniper.dash.Repository;
import net.juniper.dash.CRSDBHandler;

public class CRSTracker extends DataSource {
	
    private Map<String, CRSRecord> records = new HashMap<String, CRSRecord>();

    public CRSTracker(String name) {
        super(name, "crs", "CRSRecord", "CRS", "CRSs");
        dateParser = new SimpleDateFormat("yyyy-MM-dd HH:mm:ssz");
        dateParser.setLenient(false);       
    }

    public void init() throws DashboardException {
    	propFileName = Repository.createPath(Repository.dash_special_directory , type + ".properties", absolutePropPath);
        loaderProps =
            Repository.readProperties(propFileName, absolutePropPath);
        host = loaderProps.getProperty("host");
        webAppPath = loaderProps.getProperty("web_app_path");
    }

    @Override
    protected List<String[]> getRawRecordData(Refresher refresher) {
    	
    	CRSRecord.initValidUserList();
    	
        StringBuilder all_fields = new StringBuilder();
        for (String field : CRSRecord.getFieldList()) {
        	all_fields.append(field);
        	all_fields.append(",");
        }
        
		Map<String, Object> m = new HashMap<String, Object>();
		m.put("job", "getAll");
		m.put("all_fields", all_fields.toString().substring(0, all_fields.length()-1));
		List<String[]> all = new ArrayList<String[]>();
		m.put("all", all);
		ExecutorService executor = Executors.newFixedThreadPool(1);
		executor.execute(new CRSDBHandler(m));
	    executor.shutdown();
	    while (!executor.isTerminated());
        return all;
    }

    @Override
    protected Record constructRecord(String[] fields) throws DashboardException {
        return new CRSRecord(fields, this);
    }

    @Override
    public Record getRecord(String id) {
        return records.get(id);
    }

    @Override
    public Map<String, Record> getRecords() {
        return new HashMap<String, Record>(records);
    }

    @Override
    protected void removeRecord(String number) {
        records.remove(number);
    }

    @Override
    protected void addRecord(Record record) {
        records.put(record.getId(), (CRSRecord) record);
    }

    @Override
    protected boolean hasRecord(String id) {
        return records.containsKey(id);
    }
}
