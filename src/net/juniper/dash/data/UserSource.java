/**
 * dbergstr@juniper.net, Mar 1, 2007
 *
 * Copyright (c) 2007, Juniper Networks, Inc.
 * All rights reserved.
 */
package net.juniper.dash.data;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;

import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.BasicAttribute;
import javax.naming.directory.SearchResult;

import net.juniper.dash.DashboardException;
import net.juniper.dash.Group;
import net.juniper.dash.Refresher;
import net.juniper.dash.Repository;
import net.juniper.dash.Updater;

import org.apache.log4j.Logger;
import org.drools.FactException;
import org.drools.FactHandle;

import flexjson.JSONSerializer;

/**
 * DataSource for Users
 *
 * @author dbergstr
 */
public class UserSource extends DataSource {

    private static final String UNUSED = "unused";

	static Logger logger = Logger.getLogger(UserSource.class.getName());

    /**
     *
     */
    public UserSource(String name) {
        super(name, "user", "User", "User", "Users");
        serializer = new JSONSerializer()
            .include("totalScore", "totalsByRuleId",
                "virtualTeamMemberships.id", "virtualTeams.id",
                "manager.id", "directReportNames.id", "reportNames", 
                "spocCategories", "scores", "statuses")
            .exclude("manager.*", "topIssues", "alwaysTrue",
                "virtualTeams.*", "virtualTeamMemberships.*", 
                "directReportNames.*","newManager", "newDirectReports", "newReports",
                "workingScores", "workingRecords",
                "dataSource", "factHandle", "id");
    }

    /**
     * An ordered Map of epoch time => Users changed at that time.
     */
    private SortedMap<Long, Set<String>> changedSince =
        new TreeMap<Long, Set<String>>();

    private Map<String, User> records = new HashMap<String, User>();

    /** The ordered list of users that's exposed in the UI */
    private SortedSet<User> allUsers = new TreeSet<User>();

    /**
     * On the first pass through, we only assert the JUNOS managers.
     * With all users in the working memory, assertion of the full PR list
     * takes an hour.  With only the managers, it's relatively fast.
     * The important users get status quickly, and we add the rest
     * of the users in the second pass (which is thus quite slow).
     */
    private boolean firstPass = true;

    /**
     * Our best shot at "usernames of all the humans in the company".
     *
     * Used when assigning CategoryContacts, and PR ownership based on
     * category.
     */
    private Set<String> realUsernames = Collections.emptySet();

    /**
     * Usernames of all the folks with status in the Dashboard.
     */
    private SortedSet<String> allUserNames;

    private JSONSerializer serializer;
    
    private short total_asserted = 0;
    /**
     * Number of everyone else that will be asserted automatically without unassertion
     */
    private short NUM_SIZE_MEM_USER = 8000;

    @Override
    public void init() throws DashboardException {
        // Need to get the NPI records loaded to make the related pseudo-users.
    	/**
    	 * TODO...... remove the necessity of NUM_SIZE_MEM_USER, this is hack to make it
    	 * run in 32bit environment
    	 */
        dependsOn = CATEGORY_CONTACTS;
        propFileName = Repository.createPath(Repository.dash_special_directory, "dash-perm-user.properties", absolutePropPath);
        loaderProps =
        	Repository.readProperties(propFileName, absolutePropPath);
        logger.info("Starting UserSource with perm_user size of = " + NUM_SIZE_MEM_USER);
    }

    private User findOrCreate(Map<String, Record> currUsers,
            Map<String, User> newUsers, String name, boolean isNPI) {
        User u = records.get(name);
        if (null == u) {
            u = newUsers.get(name);
            if (null == u) {
                try {
                    if (isNPI) {
                        u = new NPIUser(new String[] { name }, this);
                    } else {
                        u = new User(new String[] { name }, this);
                    }
                    newUsers.put(name, u);
                }
                catch (DashboardException de) {
                    // The User two-arg constructor never throws.
                }
            }
        }
        currUsers.put(name, u);
        return u;
    }

    @Override
    public void populateRecords(Refresher refresher) throws DashboardException {
        // Get the list of users that we care about
        Set<String> dashUsers = Repository.
            getGroup(Group.Groups.ALL_USERS).getMembers();
        refresher.checkIfKilled();

        Map<String, User> newUsers = new HashMap<String, User>();
        Map<String, Record> currentRecords = new HashMap<String, Record>();
        Set<User> newVirtualTeams = new HashSet<User>();
        Set<String> newRealUsernames= new HashSet<String>();
        TreeSet<User> tmpAllUsers = new TreeSet<User>();
        List<Record> toModify = new ArrayList<Record>();
        List<Record> toAssert = new ArrayList<Record>();

        // need to wait till category contacts is running
        waitForDependencies(refresher);
        
        getPersons(refresher, dashUsers, newUsers, currentRecords,
            newRealUsernames);
        realUsernames = Collections.unmodifiableSet(newRealUsernames);
        addIndividual(refresher, dashUsers, newUsers, currentRecords, 
        		newRealUsernames, "non_user", true);
        addIndividual(refresher, dashUsers, newUsers, currentRecords, 
        		newRealUsernames, "deprecated_category", true);
        addIndividual(refresher, dashUsers, newUsers, currentRecords, 
        		newRealUsernames, "empty_spoc", true);
        populateSPOC(refresher, dashUsers, newUsers, currentRecords, 
        		newRealUsernames);
        
        getVirtualTeams(refresher, dashUsers, newUsers, currentRecords,
            newVirtualTeams, newRealUsernames, "vt-*");
        getBuBgDept(refresher, newUsers, currentRecords, newVirtualTeams);
        // differentiate the authgroup for BU / BG
        getVirtualTeams(refresher, dashUsers, newUsers, currentRecords,
                newVirtualTeams, newRealUsernames,"b*vt-*");
        
        getNPIs(refresher, newUsers, currentRecords);

        Set<User> bus = new HashSet<User>();

        // TODO read this from the db?
        // This part of list is BG
        Properties bgProps = Repository.readProperties(
                "/opt/dashboard/special/bg-list.properties", true);
        /**
         * Format of the BG / BU
         * BG:bu1;bu2;bu3;bu4;bu5,BG2:bu1;bu2;bu3
         */
        for (String uname : bgProps.getProperty("bg", "").split("[, ]")) {
        	// get the BG first
            User bg = (User) currentRecords.get(uname.split(":")[0]);
            if (null == bg) {
                logger.warn("Unknown user '" + uname + "' in BG names list at '" +
                    "/opt/dashboard/special/bg-list.properties'");
                continue;
            } 
            bus.add(bg);
            if (uname.split(":").length > 1){
            	String temp_bu = uname.split(":")[1];
            	for (String buname : temp_bu.split("[; ]")){
                    User bu = (User) currentRecords.get(buname);
                    if (null == bu) {
                        logger.warn("Unknown user '" + buname + "' in BU names list at '" +
                            "/opt/dashboard/special/bg-list.properties'");
                        continue;
                    }
                    bus.add(bu);
            	}
            } 
            
        }
        
        /* We've got a hash of name => User where every User knows all its
         * dr's and it's mgr.  Now we need to fill in the full reporting
         * structure for each User. */
        for (Record u : currentRecords.values()) {
            ((User) u).findAllReports();
        }
        refresher.checkIfKilled();
        // Now we have updated data for all users
        int count = 0;
        for (Map.Entry<String, Record> ent : currentRecords.entrySet()) {
            String name = ent.getKey();
            User u = (User) ent.getValue();
            boolean changed = u.populate(null);
            if (changed && ! newUsers.containsKey(name) && ! inReset) {
                // We already have this user, but it has changed
                toModify.add(u);
            }
            addRecord(u);
            tmpAllUsers.add(u);
            refresher.checkIfKilled();
            count++;
            if (logger.isTraceEnabled() && count % 1000 == 0) {
                logger.trace(this.toString() + ": Read " + count);
            }
        }
        if (logger.isDebugEnabled()) {
            logger.debug(this.toString() + ": Read " + count + " records");
        }
        allUsers = tmpAllUsers;
        allUserNames = new TreeSet<String>(records.keySet());

        if (inReset) {
            firstPass = true;
            toAssert = new ArrayList<Record>(allUsers);
        } else {
            toAssert = new ArrayList<Record>(newUsers.values());
        }
        if (firstPass) {
            /* On the first pass through we only assert managers, to reduce the
             * time until we have something to show. */
            toAssert.clear();
            notAsserted = new HashSet<Record>();
            for (Record user : currentRecords.values()) {
                if (((User) user).isAManager()) {
                    toAssert.add(user);
                }
                notAsserted.add(user);
            }
            if (logger.isDebugEnabled()) {
                logger.debug("Filtered users, retained " + toAssert.size() +
                    " out of " + notAsserted.size());
            }
            firstPass = false;
        } else if (notAsserted.size() > 0) {
            /* Assert any users that may have been missed (eg. due to
             * exceptions on a previous pass). */
            toAssert.addAll(notAsserted);
        } 

        // fix up the JUNOS object.
        for (User pu : User.pseudoUsers()) {
            // This is currently just the JUNOS user...
            currentRecords.put(pu.getId(), pu);
            allUsers.add(pu);
            boolean changed =
                User.prepPseudoUser(pu, bus, new HashSet<User>(allUsers));
            if (inReset) {
                toAssert.add(pu);
                addRecord(pu);
            } else if (changed) {
                if (null == pu.getFactHandle()) {
                    // First time through, needs to be asserted
                    toAssert.add(pu);
                    addRecord(pu);
                } else {
                    // Already in the working memory, needs to be modify()ed
                    toModify.add(pu);
                }
            }
        }

    	recordsUpdated = true;
    	
    	//Sleep before initial user assertion time
    	if (INITIAL_USER_ASSERTION) {
            /* Performance tuning:
             * Sleep here and check if those DataSources have completed 
             * method populate() then start its own assertion.
             * Without wait/sleep, there could be ConcurrentModificationExceptions
             * The related rules need the pertinent user ids for filtering out 
             * non-pertinent users
             */

            do {
    	        try {
    		    Thread.sleep(60*1000);
    	        } catch (Exception e) {}
    	    } while (!(populated.contains(GNATS.type) && 
    		       populated.contains(HISTORYGNATS.type) && 
    		       populated.contains(CRSTRACKER.type) &&
    		       populated.contains(RLI.type) &&
    		       populated.contains(REVIEWTRACKER.type) &&
    		       populated.contains(GNATSCLOSED.type) &&
    		       populated.contains(UNDEAD_CVBCS.type) &&
    		       populated.contains(HARDENINGCLOSED.type) &&
    		       populated.contains(NEWLYINPRFIX.type) &&
    		       populated.contains(BRANCHTRACKER.type)));

         }
        
    	reconcileAssertedRecords(refresher, currentRecords, toModify, toAssert);
        bus = null;
        tmpAllUsers = null;
    	currentRecords = null;

    }

    /* (non-Javadoc)
     * @see net.juniper.dash.data.DataSource#reconcileAssertedRecords(net.juniper.dash.Refresher, java.util.Map, java.util.Collection, java.util.Collection)
     */
    @Override
    protected void reconcileAssertedRecords(Refresher refresher,
        Map<String, Record> currentRecords, Collection<Record> toModify,
        Collection<Record> toAssert) {
        // Find all users who have changed since last update
        List<Record> allChanged = new ArrayList<Record>(toModify);

        allChanged.addAll(toAssert);
        Set<String> changed = new HashSet<String>(allChanged.size());
        for (Record r: allChanged) {
            changed.add(((User) r).getName());
        }
        super.reconcileAssertedRecords(refresher, currentRecords, toModify, toAssert);

        /* TODO This isn't really correct, since the users won't actually be
         * "changed" until fireAllRules() finishes.  That (generally) happens
         *  within a few seconds, so the problem is minor. */
		changedSince.put(System.currentTimeMillis(), changed);
		allChanged  = null;
		changed = null;
    }

    public void doUserAssertion (Refresher refresher) {
        // Find all users who have changed since last update
        List<String> NonAsserted_rec = new ArrayList<String>() ;
        Set<String> newNonAsserted = new HashSet<String>() ;
        Map<String, Record> currRecs = new HashMap<String, Record>(records);
        
        // nothing to modify since they should be handled before
        List<Record> toModify = Collections.emptyList();
        List<Record> toAssert = new ArrayList<Record>();
        
        int oldNAsize = 0;
        while (! (notAsserted.size() == oldNAsize) ) {
              oldNAsize = notAsserted.size();
              toAssert.clear();
              NonAsserted_rec.clear();
              for (Record r : notAsserted) {
                  toAssert.add(r);
                  total_asserted++;
                  if (total_asserted > NUM_SIZE_MEM_USER){
                  	NonAsserted_rec.add(r.id);
                  	newNonAsserted.add(r.id);
                  }

                  if (toAssert.size() == 400) break;
              }
              String message = " users in RE-assertion  (" +
                  (notAsserted.size() - toAssert.size()) +
                  " remaining for next batch).";
              doStagedAssertion(refresher, toModify, currRecs, toAssert, message);
              if (NonAsserted_rec.size() > 0){
                  doRemoveAssertion(refresher,NonAsserted_rec);
              }
        }
        Repository.writeJson(true);
        currRecs.clear();
        // put them in the batch for upcoming process
        if (newNonAsserted.size() > 0) {
        	for (String t_id : newNonAsserted){
            	notAsserted.add(DataSource.USERSOURCE.getRecord(t_id));
        	}
        	newNonAsserted.clear();
        }
    }
   
    /**
     * 
     * @param refresher
     * @param dashUsers
     * @param newUsers
     * @param currentRecords
     * @param newRealUsernames
     * @param uid
     * @throws DashboardException
     */
    private void addIndividual(Refresher refresher, Set<String> dashUsers,
        Map<String, User> newUsers, Map<String, Record> currentRecords,
        Set<String> newRealUsernames, String uid, boolean isPseudo) throws DashboardException {
    		newRealUsernames.add(uid);
            /* Create the User object, and perhaps their manager, and add
             * them to their manager's reports list. */
            User u = findOrCreate(currentRecords, newUsers, uid, false);
            u.setManager(User.JUNOS);
            u.pseudoUser = isPseudo;
            refresher.checkIfKilled();
    }
    
    /**
     * 
     * @param refresher
     * @param dashUsers
     * @param newUsers
     * @param currentRecords
     * @param newRealUsernames
     * @throws DashboardException
     */
    private void populateSPOC(Refresher refresher, Set<String> dashUsers,
            Map<String, User> newUsers, Map<String, Record> currentRecords,
            Set<String> newRealUsernames) throws DashboardException {
    			Map<String, String> catg = DataSource.CATEGORY_CONTACTS.getContacts();
    			for (String key : catg.keySet()){
    				String uid = catg.get(key);
    				if (!newRealUsernames.contains(uid)){
    					// not a user anymore?
    					continue;
    				}
                    User u = findOrCreate(currentRecords, newUsers, uid, false);
                    if (null != u){
                    	u.addSpocCategory(key);
                    } 
    			}
                refresher.checkIfKilled();
        }

    /**
     * Load in User objects for all the people in the Dashboard.
     *
     * @param refresher
     * @param dashUsers
     * @param newUsers
     * @param currentRecords
     * @throws DashboardException
     */
    private void getPersons(Refresher refresher, Set<String> dashUsers,
        Map<String, User> newUsers, Map<String, Record> currentRecords,
        Set<String> newRealUsernames) throws DashboardException {
        try {
        	// Category SPOC set 
        	Collection<String> spoc_user = DataSource.CATEGORY_CONTACTS.getContacts().values();
        	boolean is_spoc = false;

        	/* Query LDAP for all active users that have managers,
             * on the assumption that only real people will have managers,
             * since that data comes from the HR system. */
            String query = "(&(uid=*)(manager=*))";
            NamingEnumeration<SearchResult> enumeration =
                Repository.queryLDAP(query, false);

            /* We get back an enumeration which may contain any number of
             * results, so work through them one at a time.
             * The list comes back in alphabetical order, but that may not
             * be guaranteed.  Things down the line currently depend on
             * this ordering...*/
            while (enumeration.hasMore()) {
                SearchResult result = enumeration.next();
                // Extract the attributes (if any) from the current entry.
                Attributes attribs = result.getAttributes();
                Attribute uidAttr = attribs.get("uid");
                if (null == uidAttr) {
                	Repository.logError("Missing uid LDAP property on UserSource.java::getPersons");
                	logger.error("LDAP property missing while retrieving uid property");
                	continue;
                }
                String uid = (String) uidAttr.get();
                if (null == uid) {
                	Repository.logError("Unable to retrieve uid LDAP property on UserSource.java::getPersons");
                	logger.error("uid LDAP property cannot be retrieved");
                	continue;
                }
                uid = uid.toLowerCase();
                newRealUsernames.add(uid);
                Attribute mgrAttr = attribs.get("manager");
                Attribute deptAttr = attribs.get("departmentNumber");
                String mgr;
                if (null == mgrAttr) {
                    // No manager listed.
                    mgr = "UNKNOWN";
                } else {
                    String mgrDN = (String) mgrAttr.get();
                    if (mgrDN.startsWith("uid=")) {
                        mgr = mgrDN.substring(4,
                            mgrDN.indexOf(",")).toLowerCase();
                    } else {
                        mgr = "UNKNOWN";
                    }
                }
                is_spoc = spoc_user.contains(uid);
                if (!(dashUsers.contains(uid) || dashUsers.contains(mgr)) & (!is_spoc)) {
                    // Not a user we care about
                    continue;
                }
                /* Create the User object, and perhaps their manager, and add
                 * them to their manager's reports list. */
                User u = findOrCreate(currentRecords, newUsers, uid, false);
                User m =
                    findOrCreate(currentRecords, newUsers, mgr, false);
                u.setManager(m);
                if (deptAttr == null) {
                	Repository.logError("Missing departmentNumber LDAP property on UserSource.java::getPersons");
                	logger.error("department property is missing for " + u.id);
				} else {
	                u.setDepartmentNumber((String) deptAttr.get());
	                }
	                m.addDirectReport(u);
	                refresher.checkIfKilled();
	            }
        } catch (NamingException ne) {
            throw new DashboardException("Error fetching users: " +
                ne.getMessage());
        }
    }

    /**
     * Load in User objects for all the Virtual Teams in the Dashboard.
     *
     * @param refresher
     * @param dashUsers
     * @param newUsers
     * @param currentRecords
     * @throws DashboardException
     */
    @SuppressWarnings("unchecked")
	private void getVirtualTeams(Refresher refresher, Set<String> dashUsers,
        Map<String, User> newUsers, Map<String, Record> currentRecords,
        Set<User> newVirtualTeams, Set<String> newRealUsernames,String queryExpr)
    throws DashboardException {
        NamingEnumeration<SearchResult> enumeration = null;
    	// Category SPOC set 
    	Collection<String> spoc_user = DataSource.CATEGORY_CONTACTS.getContacts().values();
    	boolean is_spoc = false;

    	try {
            String query = "(&(objectClass=groupOfNames)(cn="+queryExpr+"))";
            enumeration = Repository.queryLDAP(query, true);

            /* We get back an enumeration which may contain any number of
             * results, so work through them one at a time. */
            while (enumeration.hasMore()) {
                SearchResult result = enumeration.next();
                // Extract the attributes (if any) from the current entry.
                Attributes attribs = result.getAttributes();
                BasicAttribute nameAttr =
                    (BasicAttribute) attribs.get("authGroupName");
                if (null == nameAttr) {
                	Repository.logError("Missing authGroupName property on UserSource.java::getVirtualTeams " + queryExpr);
                	logger.error("LDAP AuthGroupname property missing!");
                	continue;
                }
                String name = (String) nameAttr.get();
                // Get the name without the "vt-" / "buvt-" prefix, length will do
                name = name.substring(queryExpr.length()-1, name.length()).toLowerCase();
                BasicAttribute ownerAttr =
                    (BasicAttribute) attribs.get("authGroupOwner");
                if (null == ownerAttr) {
                	Repository.logError("Missing authGroupOwner property on UserSource.java::getVirtualTeams " + queryExpr);
                	logger.error("LDAP authGroupOwner property missing from " + name);
                	continue;
            	}
                String mgr = (String) ownerAttr.get();
                mgr = mgr.toLowerCase();
                // Now find all the members of the team
                BasicAttribute memAttr = (BasicAttribute) attribs.get("member");
                if (null == memAttr) {
                	Repository.logError("Missing member property on UserSource.java::getVirtualTeams " + queryExpr);
                	logger.error("LDAP member property missing from " + name);
                	continue;
            	}
                
                NamingEnumeration members = memAttr.getAll();

                if (! members.hasMore()) {
                    // This Virtual Team is empty, skip it
                    // FIXME We may want to nag the owner...
                    logger.warn("Virtual Team \"" + name +
                        "\" is empty, skipping it.");
                    continue;
                }
                /* Create the User object, and perhaps their manager. */
                User vt = findOrCreate(currentRecords, newUsers, name, false);
                // what happen if owner is left the company?....
                vt.setVirtualTeam();
                if (newRealUsernames.contains(mgr)){
                    User owner = findOrCreate(currentRecords, newUsers, mgr, false);
                    vt.setManager(owner);
                    owner.addVirtualTeam(vt);
                }
                if (!newVirtualTeams.contains(vt)){
                    newVirtualTeams.add(vt);
                }

                while (members.hasMore()) {
                    String memDN = (String) members.next();
                    String uid = memDN.substring(4, memDN.indexOf(","));
                    if ("authldapplaceholder".equals(uid)) {
                        continue;
                    }
                    is_spoc = spoc_user.contains(uid);
                    if ( (!dashUsers.contains(uid)) & (!is_spoc)) {
                        continue;
                    }
                    User member = findOrCreate(currentRecords, newUsers, uid, false);
                    vt.addDirectReport(member);
                }
                refresher.checkIfKilled();
            }
        } catch (NamingException ne) {
        	logger.error("o.o   error   o.o ");
            throw new DashboardException("Error fetching virtual teams: " +
                ne.getMessage());
        } finally {
            if (null != enumeration) {
                try {
                    enumeration.close();
                } catch (NamingException ne) {
                    // Don't really care at this point.
                }
            }
        }
    }

    /**
     * Find or create the department number virtual team for each department
     * represented by a user in the system.
     *
     * Once BU and BG are ldap attributes, will handle those as well.  For now
     * they're done as straight ldap virtual teams.
     */
    private void getBuBgDept(Refresher refresher, Map<String, User> newUsers,
        Map<String, Record> currentRecords, Set<User> newVirtualTeams) {
        for (Record r : new ArrayList<Record>(currentRecords.values())) {
            User u = (User) r;
            if (u.isVirtualTeam() || u.isPseudoUser() ||
                null == u.getDepartmentNumber()) continue;
            String deptuid = "dept-" + u.getDepartmentNumber();
            User deptUser = findOrCreate(currentRecords, newUsers, deptuid, false);
            deptUser.setVirtualTeam();
            deptUser.setManager(User.JUNOS);
            newVirtualTeams.add(deptUser);
            deptUser.addDirectReport(u);
            refresher.checkIfKilled();
        }
    }

    /**
     * Build an NPIUser record for each NPI program in the system.
     */
    private void getNPIs(Refresher refresher, Map<String, User> newUsers,
        Map<String, Record> currentRecords) {
        for (Record r : NPI.getRecordCollection()) {
            NPIRecord npi = (NPIRecord) r;
            String newId = "npi-" + npi.getId();
            NPIUser npiUser = (NPIUser) findOrCreate(currentRecords, newUsers,
                newId, true);
            npiUser.setNpi_program(npi.getId());
            npiUser.setNpi_program_name(npi.getSynopsis());
            refresher.checkIfKilled();
        }
    }

    /**
     * Users are asserted in several stages.  Core managers, virtual teams,
     * pseudo-users, other managers and then individuals.
     *
     * In order to keep the watchdog happy, we'll note an update cycle after
     * every batch of asserted users.
     */
	public void finishStagedAssertion(Refresher refresher) {
        /* We're not modifying or retracting any Users in this method. */
        List<Record> toModify = Collections.emptyList();
        Map<String, Record> currRecs = new HashMap<String, Record>(records);

        /* On the second pass, we assert all the virtual teams. */
        List<Record> toAssert = new ArrayList<Record>();
        for (Record r : notAsserted) {
            if (((User) r).isVirtualTeam()) {
                toAssert.add(r);
            }
        }
        String message = " Virtual Teams in stage 2.";
        doStagedAssertion(refresher, toModify, currRecs, toAssert, message);
        Repository.writeVirtualTeams();
        Repository.informUI();

        /* On the third pass, we assert all the NPIUsers (and any other
         * pseudo-users). */
        toAssert.clear();
        for (Record r : notAsserted) {
            if (((User) r).isPseudoUser()) {
                toAssert.add(r);
            }
        }
        message = " pseudo-users in stage 3.";
        doStagedAssertion(refresher, toModify, currRecs, toAssert, message);
        Repository.writePseudos();
        Repository.informUI();

        /* We assert the individual contributors in batches of 400, to
         * make startup seem faster. */
        int oldNAsize = 0;
        total_asserted = 0;
        while (! (notAsserted.size() == oldNAsize) ) {
            oldNAsize = notAsserted.size();
            toAssert.clear();
            for (Record r : notAsserted) {
                toAssert.add(r);
                if (toAssert.size() == 400) break;
            }
            message = " users in stage 5 (" +
                (notAsserted.size() - toAssert.size()) +
                " remaining for next batch).";
            logger.info("Asserting " + toAssert.size() + message);
            doStagedAssertion(refresher, toModify, currRecs, toAssert, message);
            Repository.noteUpdateCycle();
            Repository.writeEveryoneElse();
            Repository.writeMisc();
            Repository.informUI();
        }
        
        currRecs = null;
        toModify = null;

        //No longer fetch the valid users
        INITIAL_USER_ASSERTION = false;
    }

	/**
	 * To reduce the memory consumption in Tomcat, we will unasserted the users. This
	 * supposedly happen on the users which have lower score/status than the one we asserted
	 * @param refresher
	 * @param toModify
	 * @param currRecs
	 * @param toAssert
	 * @param message
	 */
	private void doRemoveAssertion(Refresher refresher, List<String> toRemove){
        // Remove the record we just asserted for memory sake!
        int count =0;
        for (String rec_id : toRemove) {
        	Record rec = DataSource.USERSOURCE.getRecord(rec_id);
            FactHandle fh = rec.getFactHandle();
            if (inReset) fh = null;
            try {
                if (null != fh) {
                    Repository.workingMemory.retract(fh);
                } // Records w/o FactHandles were never asserted
                if (logger.isDebugEnabled() && count % 50 == 0) {
                    logger.debug("UN-Asserted " + count);
                    refresher.checkIfKilled();
                }
                total_asserted--;
                count++;
                fh = null;
            } catch (FactException fe) {
                logger.error("Error UN-asserting USER record " +
                    rec.getId() + ": " + fe.getMessage());
            }
            catch (NullPointerException npe) {
               logger.error("Error UN-asserting User record " +
                    rec.getId(), npe);
            }
        }
        logger.info("UN-Asserted " + count + " new records for USER ");     
	}
	
    private void doStagedAssertion(Refresher refresher, List<Record> toModify,
        Map<String, Record> currRecs, List<Record> toAssert, String message) {
        if (toAssert.size() > 0) {
            logger.info("Asserting " + toAssert.size() + message);
            reconcileAssertedRecords(refresher,  currRecs, toModify, toAssert);
            Updater.fireRules(Repository.workingMemory);
            Repository.noteUpdateCycle();
        }
    }

    /**
     * Not supported in UserSource.
     */ 
    @Override
    protected Record constructRecord(@SuppressWarnings(UNUSED) String[] ignored)
            throws DashboardException {
        throw new UnsupportedOperationException("Don't you be callin'" +
                " UserSource.constructRecord()!");
    }

    /**
     * Process the results of the rules for each User.
     *
     * @param updateTime The time that the data used by the rules engine was
     * updated.
     */
    public void processUserStatuses(Date updateTime) {
        if (logger.isDebugEnabled()) {
            logger.debug("Processing status for all Users.");
        }
        long lastKey = changedSince.lastKey();
        Set<String> changed = changedSince.get(lastKey);
        for (User user : allUsers) {
            boolean retval = user.finalizeScores(updateTime);
            if (retval) {
                changed.add(user.getName());
                user.AdditionalPreprocessing();
            }
        }
        logger.info("Last change at " + lastKey + "; changed " + changed.size());
    }

    public SortedSet<User> allUsers() {
        return allUsers;
    }
    /**
     * @return the names of all Dashboard Users (allUsers).
     */
    public SortedSet<String> getUserNames() {
        return allUserNames;
    }

    /**
     * @return the realUsernames
     */
    public Set<String> getRealUsernames() {
        return realUsernames;
    }

    /**
     * @param time A time in epoch seconds.
     * @return The names of all Users changed since time.
     */
    public Set<String> getChangedSince(long time) {
        Set<String> names = new HashSet<String>();
        for (Set<String> bunch: changedSince.tailMap(time).values()) {
            names.addAll(bunch);
        }
        return names;
    }

    public void resetAllScores(){
    	/**
    	 * Reseting all scores
    	 */
    	for (User a : records.values()){
    		a.clearScores();
    	}
    }
    
    @Override
    public Record getRecord(String id) {
        return records.get(id);
    }

    @Override
    public Map<String, Record> getRecords() {
        return new HashMap<String, Record>(records);
    }

    @Override
    protected void removeRecord(String number) {
        records.remove(number);
    }

    @Override
    protected void addRecord(Record record) {
        records.put(record.getId(), (User) record);
    }

    @Override
    protected boolean hasRecord(String id) {
        return records.containsKey(id);
    }

    public String serialize(Collection<String> names) {
        Set<User> users = new HashSet<User>(names.size());
        for (String name : names) {
            users.add(records.get(name));
        }
        if (Repository.prettyPrint) {
            return serializer.prettyPrint(users);
        } else {
            return serializer.serialize(users);
        }
    }

}
