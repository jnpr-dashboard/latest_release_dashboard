/**
 * mbasha@juniper.net, Aug 24, 2012
 *
 * Copyright (c) 2012, Juniper Networks, Inc.
 * All rights reserved.
 */
package net.juniper.dash.data;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.HashSet;
import java.util.Set;

import net.juniper.dash.DashboardException;
import net.juniper.dash.Refresher;
import net.juniper.dash.Repository;
import net.juniper.dash.Refresher.TimeoutException;

import org.apache.log4j.Logger;
import org.drools.WorkingMemory;

import flexjson.JSONSerializer;

/**
 * HardeningClosed DataSource.
 *
 * @author mbasha 
 */
public class HardeningClosed extends DataSource {

    static Logger logger = Logger.getLogger(HardeningClosed.class.getName());

    private Map<String, HardeningClosedPRRecord> records = new HashMap<String, HardeningClosedPRRecord>();
    private JSONSerializer recordSerializer;

    String port;
    String database;
    String query_prPath;
    String extra_opts;
    String categorySubfields;

    public HardeningClosed(String name) {
        super(name, "hardening_closed", "HardeningClosedPRRecord", "HardeningClosedPR", "HardeningClosedPRs");
        dateParser = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzzz yyyy");
        dateParser.setLenient(false);
        recordSerializer = new JSONSerializer();
    }

    public void init() throws DashboardException {
        propFileName = Repository.createPath(Repository.dash_special_directory, "gnats.properties", absolutePropPath);
        loaderProps =
            Repository.readProperties(propFileName,absolutePropPath);
        host = loaderProps.getProperty("host");
        database = loaderProps.getProperty("database");
        port = loaderProps.getProperty("port");
        webAppPath = loaderProps.getProperty("web_app_path");
        categorySubfields = loaderProps.getProperty("category_subfields");

        query_prPath = loaderProps.getProperty("query_pr");
        extra_opts = loaderProps.getProperty("extra_opts");
        File f = new File(query_prPath);
        if ( ! (f.exists() && f.canRead())) {
            // No way to test for executable, so we still might blow up later.
            throw new DashboardException("Can't find query-pr at path " +
                query_prPath);
        }
        List<String> cmd = new ArrayList<String>();
        cmd.add(query_prPath);
        cmd.add(extra_opts);
        cmd.add("--database=" + database);
        cmd.add("--host=" + host);
        cmd.add("--port=" + port);
        
        String expr = Repository.testingMode ? 
        		HardeningClosedPRRecord.debugQueryExpression : HardeningClosedPRRecord.queryExpression;

        cmd.add("--expr=" + expr);
        StringBuilder format = new StringBuilder("--format=\"");
        StringBuilder formatFields = new StringBuilder();
        for (String fld : HardeningClosedPRRecord.getFieldList()) {
            format.append("%s");
            format.append(Repository.FIELD_SEPARATOR);
            formatFields.append(fld);
            formatFields.append(" ");
        }
        format.append(Repository.RECORD_SEPARATOR);
        format.append("\" ");
        format.append(formatFields);
        format.append("");
        cmd.add(format.toString());
        processBuilder = new ProcessBuilder(cmd);
        dependsOn = USERSOURCE;
    }
    
    protected Record constructRecord(String[] fields) throws DashboardException {
        return new HardeningClosedPRRecord(fields, this);
    }

    @Override
    public Record getRecord(String id) {
        return records.get(id);
    }

    @Override
    public Map<String, Record> getRecords() {
        return new HashMap<String, Record>(records);
    }

    @Override
    protected void removeRecord(String number) {
        records.remove(number);
    }

    @Override
    protected void addRecord(Record record) {
        records.put(record.getId(), (HardeningClosedPRRecord) record);
    }

    @Override
    protected boolean hasRecord(String id) {
        return records.containsKey(id);
    }
       
    //Performance tuning
    @Override
    protected List<String[]> getRawRecordData(Refresher refresher) throws DashboardException, TimeoutException {    	
    	HardeningClosedPRRecord.initValidUserList(); 	
        return super.getRawRecordData(refresher);
    }    
}
