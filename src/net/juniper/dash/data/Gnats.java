/**
 * dbergstr@juniper.net, Oct 27, 2006
 *
 * Copyright (c) 2006, Juniper Networks, Inc.
 * All rights reserved.
 */
package net.juniper.dash.data;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import net.juniper.dash.DashboardException;
import net.juniper.dash.Refresher.TimeoutException;
import net.juniper.dash.Repository;
import net.juniper.dash.Refresher;

import org.apache.log4j.Logger;
import org.drools.WorkingMemory;

import flexjson.JSONSerializer;

/**
 * PR DataSource.
 *
 * @author dbergstr
 */
public class Gnats extends DataSource {

    static Logger logger = Logger.getLogger(Gnats.class.getName());

    private Map<String, PRRecord> records = new HashMap<String, PRRecord>();
    private JSONSerializer recordSerializer;

    // Used by both Gnats and CategoryContacts
    String port;
    String database;
    String query_prPath;
    String extra_opts;
    String categorySubfields;
    String releaseSubfields;

    public Gnats(String name) {
        super(name, "gnats", "PRRecord", "PR", "PRs");
        // Mon Jun 26 12:16:19 -0700 2006
        dateParser = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzzz yyyy");
        dateParser.setLenient(false);
        recordSerializer = new JSONSerializer();
    }

    public void init() throws DashboardException {
        propFileName = Repository.createPath(Repository.dash_special_directory , "gnats.properties", absolutePropPath);
        loaderProps =
            Repository.readProperties(propFileName,absolutePropPath);
        host = loaderProps.getProperty("host");
        database = loaderProps.getProperty("database");
        port = loaderProps.getProperty("port");
        webAppPath = loaderProps.getProperty("web_app_path");
        categorySubfields = loaderProps.getProperty("category_subfields", "7,11");
        releaseSubfields = loaderProps.getProperty("release_subfields","2,4");

        query_prPath = loaderProps.getProperty("query_pr");
        extra_opts = loaderProps.getProperty("extra_opts");

        File f = new File(query_prPath);
        if ( ! (f.exists() && f.canRead())) {
            // No way to test for executable, so we still might blow up later.
            throw new DashboardException("Can't find query-pr at path " +
                query_prPath);
        }
        List<String> cmd = new ArrayList<String>();
        cmd.add(query_prPath);
        cmd.add(extra_opts);
        cmd.add("--database=" + database);
        cmd.add("--host=" + host);
        cmd.add("--port=" + port);

        String expr = Repository.testingMode ?
            PRRecord.debugQueryExpression : PRRecord.queryExpression;

        // --expr='!(state=="closed" | state=="suspended") & (product[os]=="junos" | product[os]=="")' \
        // --format='"%s\037%s\037..." number etc....'
        cmd.add("--expr=" + expr);
        StringBuilder format = new StringBuilder("--format=\"");
        StringBuilder formatFields = new StringBuilder();
        for (String fld : PRRecord.getFieldList()) {
            format.append("%s");
            format.append(Repository.FIELD_SEPARATOR);
            formatFields.append(fld);
            formatFields.append(" ");
        }
        format.append(Repository.RECORD_SEPARATOR);
        format.append("\" ");
        format.append(formatFields);
        format.append("");
        cmd.add(format.toString());
        processBuilder = new ProcessBuilder(cmd);

        // Need to get the categories sorted out first.
        dependsOn = USERSOURCE;
    }

    protected Record constructRecord(String[] fields) throws DashboardException {
        return new PRRecord(fields, this);
    }


    /**
     * For summary purposes, an UndeadCvbc is really a PR, so include them. 
     * 
     * @return A JSON representation of a Map of record_number => [ synopsis,
     * last_modified]. 
     */
    @Override
    public String getSummaries() {
        Collection<? extends Record> recs = getRecords().values();
        Map<String, String[]> out = new HashMap<String, String[]>(recs.size());
        for (Record r : recs) {
            out.put(r.getId(), r.getSummary());
        }
        for (Record r : UNDEAD_CVBCS.getRecords().values()) {
            out.put(r.getId(), r.getSummary());
        }
        recs   =  null;
        return recordSerializer.serialize(out);
    }

    @Override
    public Record getRecord(String id) {
        return records.get(id);
    }

    @Override
    public Map<String, Record> getRecords() {
        return new HashMap<String, Record>(records);
    }

    @Override
    protected void removeRecord(String number) {
        records.remove(number);
    }

    @Override
    protected void addRecord(Record record) {
        records.put(record.getId(), (PRRecord) record);
    }

    @Override
    protected boolean hasRecord(String id) {
        return records.containsKey(id);
    }
    
    //Performance tuning
    @Override
    protected List<String[]> getRawRecordData(Refresher refresher) throws DashboardException, TimeoutException {    	
    	PRRecord.initValidUserList();  	
        return super.getRawRecordData(refresher);
    }    
}
