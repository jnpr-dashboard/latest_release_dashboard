/**
 * dbergstr@juniper.net, Nov 13, 2006
 *
 * Copyright (c) 2006, Juniper Networks, Inc.
 * All rights reserved.
 */
package net.juniper.dash.struts;

import java.beans.Introspector;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import net.juniper.dash.DashboardException;
import net.juniper.dash.Repository;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 * Mediates between Tomcat and the Dashboard.
 *
 * @author dbergstr
 */
public class ApplicationListener implements ServletContextListener {

    static Logger logger = Logger.getLogger(ApplicationListener.class.getName());

    /**
     * The ServletContext for this web application.
     */
    private ServletContext context = null;

    /**
     * Initialize the application.
     *
     * @param event The context initialization event
     */
    // THIS IS WHERE EVERYTHING STARTS
    // Follow the white rabbit
    public void contextInitialized(ServletContextEvent event) {

        logger.info("Starting up the dashboard.");

        // Remember our associated ServletContext
        this.context = event.getServletContext();

        try {
        	// the rabbit goes down this hole
            Repository.initialize(context.getRealPath("/"));
            logger.info("\n\n    >>>>> Repository initialized. <<<<<\n\n");
        } catch (DashboardException e) {
            /* FIXME we should change all struts stuff to point to an
             * "Everything's broken" page.
             */
            logger.fatal("Exception initializing Repository:"+
                    e.getMessage());
        }
    }

    /**
     * Shut down the application.
     *
     * @param event ServletContextEvent to process
     */
    public void contextDestroyed(@SuppressWarnings("unused")
            ServletContextEvent event) {
        logger.info("Closing up shop.");
        Repository.shutdown();
        LogManager.shutdown();
        /* A call to:
         * Introspector.flushCaches();
         * may be needed to prevent a problem that causes webapps to be
         * unreloadable See:
         * http://marc.theaimsgroup.com/?l=log4j-user&m=109585712427674&w=2
         */
        Introspector.flushCaches();
        context = null;
    }
}
