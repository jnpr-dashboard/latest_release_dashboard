/**
 * smulyono@juniper.net, Aug 30, 2010
 *
 * Copyright (c) 2010, Juniper Networks, Inc.
 * All rights reserved.
 */
package net.juniper.dash.struts;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import freemarker.log.Logger;

import net.juniper.dash.Repository;
import net.juniper.dash.Rule;
import net.juniper.dash.data.DataSource;


public class ConfigurationSupport extends DashSupport {
	
    /**
	 * 
	 */
	private static final long serialVersionUID = -841770239761673254L;

	private String dsName;
	
	private Map<String, String> dbConfig = null;
	private Map<String, String> ldapConfig = null;
	private Map<String, String> logConfig = null;
	private Map<String, String> multiplierConfig = null;
	private Map<String, String> appConfig = null;

	/**
	 * There are multiple data source configuration 
	 */
	private Map<String, Map<String, String>> dataConfig = null;

	public Map<String, String> getDbConfig() {
		return dbConfig;
	}

	public void setDbConfig(Map<String, String> dbConfig) {
		this.dbConfig = dbConfig;
	}

	public Map<String, String> getLdapConfig() {
		return ldapConfig;
	}

	public void setLdapConfig(Map<String, String> ldapConfig) {
		this.ldapConfig = ldapConfig;
	}

	public Map<String, String> getLogConfig() {
		return logConfig;
	}

	public Map<String, Map<String, String>> getDataConfig() {
		return dataConfig;
	}

	public Map<String, String> getMultiplierConfig() {
		return multiplierConfig;
	}

	public Map<String, String> getAppConfig() {
		return appConfig;
	}

	public String getDsName() {
		return dsName;
	}

	public void setDsName(String dsName) {
		this.dsName = dsName;
	}

	private void fill_configuration(){
		/**
		 * Database configuration
		 * 
		 * Dashboard didn't rely too much on database properties and connection,
		 * currently this object is obtained for informational and debugging
		 * purposes only. 
		 * 
		 */
		if (null == appConfig){
			appConfig = new HashMap<String, String>();
			for (Enumeration<Object> e = Repository.getAppProps().keys(); e.hasMoreElements();){
				String key = (String) e.nextElement();
				appConfig.put(key,Repository.getAppProps().getProperty(key));
			}
			appConfig.put("Application config file location", Repository.getAppFileName());
		}
		if (null == dbConfig){
			dbConfig = new HashMap<String, String>();
			dbConfig.put("dbconnfile", Repository.dbConnInfoFile);
			dbConfig.put("dbname",Repository.dbname);
			dbConfig.put("Server Name", Repository.getOds().getServerName());
			dbConfig.put("Database Name", Repository.getOds().getDatabaseName());
		}
		
		/**
		 * LDAP configuration
		 */
		if (null == ldapConfig){
			ldapConfig = new HashMap<String, String>();
			for (Enumeration<Object> e = Repository.getLdapProps().keys(); e.hasMoreElements();){
				String key = (String) e.nextElement();
				ldapConfig.put(key,Repository.getLdapProps().getProperty(key));
			}
			ldapConfig.put("ldap properties file location", Repository.getLdapFileName());
		}
		/**
		 * Logging configuration
		 * 
		 */
		if (null == logConfig){
			logConfig = new HashMap<String, String>();
		}
		/**
		 * Multiplier Configuration
		 */
		if (null == multiplierConfig){
			multiplierConfig = new HashMap<String, String>();
			for (Enumeration<Object> e = Rule.loaderProps.keys(); e.hasMoreElements();){
				String key = (String) e.nextElement();
				multiplierConfig.put(key, Rule.loaderProps.getProperty(key));
			}
			
		}
		/**DataSources**/
		if (null == dataConfig){
			reloadDataSourcesConfig();
		}
	}

	protected void reloadDataSourcesConfig(){
		/**
		 * DataSources configuration
		 */
		dataConfig = new HashMap<String, Map<String, String>>();
		for (DataSource ds : DataSource.sources()){
			Map<String, String> inner = new HashMap<String, String>();
			DataSource typed_ds = DataSource.sourceByType(ds.getType());
			if (typed_ds.getLoaderProps() != null){
				for (Enumeration<Object> e = typed_ds.getLoaderProps().keys(); e.hasMoreElements();){
					String key = (String) e.nextElement();
					inner.put(key, typed_ds.getLoaderProps().getProperty(key));
				}
				// put the properties filename so people can edit them
				inner.put("Properties file location", typed_ds.getPropFileName());
			}
			dataConfig.put(ds.getType(), inner);
		}
	}
	
	public String execute() throws Exception{
		super.execute();
		fill_configuration();
		return SUCCESS;
	}
	
    public String doReloadMultipliers() throws Exception {
        super.execute();
        Rule.loadMultipliers();
    	fill_configuration();
        return SUCCESS;
    }
    
    public String doReloadDB() throws Exception {
    	super.execute();
    	/**XXX
    	 * 
    	 */
    	return SUCCESS;
    }
    
    public String doReconnectDB() throws Exception {
    	super.execute();
    	Repository.setupConnectionPool();
    	fill_configuration();
    	return SUCCESS;
    }
    
    public String doReloadDS() throws Exception {
    	super.execute();
    	fill_configuration();
    	StringBuffer ret_title = new StringBuffer();
    	boolean partial_success = false;
		if (dsName.equalsIgnoreCase("all")){
    		for (Iterator<String> it = dataConfig.keySet().iterator(); it.hasNext(); ){
    			String key = (String)it.next();
    			DataSource typed_ds = DataSource.sourceByType(key);
    			if (null != typed_ds){
    				typed_ds.reloadProps(typed_ds.isAbsolutePropPath());
    				partial_success &= true;
    			} else {
    				ret_title.append(key + " failed");
    				partial_success &= false;
    			}
    		}
		} else {
			DataSource typed_ds = DataSource.sourceByType(dsName);
			if (null != typed_ds){
				typed_ds.reloadProps(typed_ds.isAbsolutePropPath());
			} else {
				ret_title.append("Reloading " + dsName + " FAILED");
				partial_success = false;
			}
		}
    	if (partial_success){
        	setTitle(ret_title.toString());
    	} else {
        	setTitle(dsName.toUpperCase() + " Data Sources Reloaded");
    		
    	}
    	reloadDataSourcesConfig();
    	return SUCCESS;
    }
    
    public String doReloadLDAP() throws Exception {
    	super.execute();
    	Repository.loadLDAPproperties();
    	fill_configuration();
    	return SUCCESS;
    }
    
    public String doReloadAppConfig() throws Exception {
    	super.execute();
    	Repository.loadApplicationProperties();
    	fill_configuration();
    	return SUCCESS;
    }
    
}
