package net.juniper.dash.struts;

import java.util.SortedSet;

import javax.servlet.http.HttpServletRequest;

import net.juniper.dash.Group;
import net.juniper.dash.MotD;
import net.juniper.dash.Repository;
import net.juniper.dash.Rule;
import net.juniper.dash.data.DataSource;
import net.juniper.dash.data.Record;
import net.juniper.dash.data.ReleaseRecord;
import net.juniper.dash.data.GroupingStatus;
import net.juniper.dash.data.User;

import org.apache.struts2.interceptor.PrincipalAware;
import org.apache.struts2.interceptor.PrincipalProxy;
import org.apache.struts2.interceptor.ServletRequestAware;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

/**
 * Base Action class for the dashboard.
 *
 * FIXME Remove methods no longer used.
 */
public class DashSupport extends ActionSupport
        implements PrincipalAware, ServletRequestAware {

    public String getVersion() {
        return Repository.VERSION;
    }
    public String getReleaseDate() {
        return Repository.RELEASE_DATE;
    }

    public String getUiBasePath() {
        return Repository.uiBasePath;
    }

    protected Record grouping;
    protected String grname;
    protected String grtype;

    protected User user = User.JUNOS;
    protected String title;

    protected long now = System.currentTimeMillis();

    private boolean showLinksForUser;
    /**
     * Do we need to show the agenda/release/prs links for this user?
     */
    public boolean isShowLinksForUser() {
        return showLinksForUser;
    }

    protected boolean statusAvailableForRemoteUser;
    /**
     * Does the system intend to keep status for this user?
     */
    public boolean isStatusAvailableForRemoteUser() {
        return statusAvailableForRemoteUser;
    }

    @Deprecated
    public SortedSet<ReleaseRecord> getAllReleases() {
        throw new RuntimeException("getAllReleases is deprecated.");
    }

    @Deprecated
    public SortedSet<ReleaseRecord> getActiveReleases() {
        throw new RuntimeException("getActiveReleases is deprecated.");
    }

    public SortedSet<Rule> getRules() {
        return Repository.getRules();
    }

    /**
     * Legacy support for old relname param.
     */
    public void setRelname(String rel) {
        grname = rel;
        grtype = "release";
    }
    public void setGrname(String grp) {
        grname = grp;
    }
    public String getGrname() {
        return grname;
    }
    public void setGrtype(String grt) {
        grtype = grt;
    }
    public String getGrtype() {
        return grtype;
    }

    public Record getGrouping() {
        return grouping;
    }

    /**
     * Should we register this as a view on the User object?
     */
    private boolean countView = false;
    public void setCountView(boolean cv) {
        this.countView = cv;
    }

    /**
     * Do we need to show status on the page being rendered (false for help,
     * admin, rules, etc.).
     */
    private boolean needsStatus = true;
    public boolean isNeedsStatus() {
        return needsStatus;
    }
    public void setNeedsStatus(boolean needsStatus) {
        this.needsStatus = needsStatus;
    }

    protected GroupingStatus groupingStatus;
    public GroupingStatus getGroupingStatus() throws Exception {
        return groupingStatus;
    }

    private String actionName;
    protected DataSource dataSource;

    public String getActionName() {
        return actionName;
    }

    /**
     * Implementing Serializable
     */
    private static final long serialVersionUID = -5290716972127708293L;

    public String execute() throws Exception {
        String ua = request.getHeader("user-agent");
        isMozilla = (null != ua && ua.indexOf("Gecko/") > -1);
        isMac= (null != ua && ua.indexOf("Macintosh") > -1);

        if (Repository.getGroup(Group.Groups.ADMIN).
            getMembers().contains(remoteUser)) {
            isAdmin = true;
        }

        /* What status should we show the user? */
        if (null == uid || uid.length() == 0) {
            // No uid specified, maybe we've got status for the user?
            user = Repository.getUser(remoteUser);
            if (null == user) {
                // We don't know them.  Give them overall status.
                user = User.JUNOS;
            } else if (needsStatus) {
                if (! user.isScored() && null != user.getManager() &&
                        (user.getManager()).isScored()) {
                    // Their manager has status, show them that
                    user = user.getManager();
                    infoMessage = "Your status not yet available, showing " +
                            "status for your manager.";
                } else {
                    /* No score for user or uid, go with user, which will
                     * give them the "not yet scored" message. */
                }
            }
        } else if (uid.equals(User.JUNOS.getId()) || uid.equals("all-mgrs")) {
            // Explicit request for overall status
            user = User.JUNOS;
        } else {
            // See if we have status for this username
            user = Repository.getUser(uid);
            if (null == user ) {
                // explicit request for a user not in the system
                errorMessage = "No Dashboard available for user-name '" +
                    uid + "'.";
                return ERROR;
            }
        }

        showLinksForUser = ! (user.getId().equals(remoteUser) ||
            user.isPseudoUser());

        statusAvailableForRemoteUser =
            (null != DataSource.USERSOURCE.getRecord(remoteUser));

        if (null != grtype && grtype.length() > 0) {
            dataSource = DataSource.sourceByType(grtype);
            if (null == dataSource) {
                errorMessage = "Grouping type '" + grtype + "' not in the " +
                "Dashboard.";
                return ERROR;
            }
            if (null != grname && grname.length() > 0) {
                grouping = dataSource.getRecord(grname);
                if (null == grouping) {
                    errorMessage = dataSource.getRecordSingular() +
                    " '" + grname + "' not in the Dashboard.";
                    return ERROR;
                }
            }
        }

        if (null != grouping) {
            groupingStatus = user.getGroupingStatus(grouping);
        }

        // We need the name of the action to do some UI snazziness
        actionName = ActionContext.getContext().getName();

        return SUCCESS;
    }

    protected boolean isAdmin = false;
    public boolean isAdmin() {
        return isAdmin;
    }

    protected String infoMessage = "";
    public String getInfoMessage() {
        return null ==  infoMessage ? "" : infoMessage;
    }

    protected String errorMessage = "";
    public String getErrorMessage() {
        return null == errorMessage ? "" : errorMessage;
    }

    /**
     * @param user the user to set
     */
    public void setUser(User user) {
        this.user = user;
    }

    /**
     * @return the user
     */
    public User getUser() {
        return user;
    }

    /**
     * @return true if the current User is a pseudo user.
     */
    public boolean isPseudoUser() {
        return user.isPseudoUser();
    }

    /**
     * @return the JUNOS object
     */
    public User getTheJunos() {
        return User.JUNOS;
    }

    /**
     * This is here to quell warnings from struts due to the autocompleter.
     *
     * @param unused
     */
    public void setUid_selected(String unused) {
        // Does nothing
    }


    private String uid;
    public void setUid(String name) {
        this.uid = name;
    }
    public String getUid() {
        return uid;
    }

    private String dojodebug;
    public void setDojodebug(String name) {
        this.dojodebug = name;
    }
    public String getDojodebug() {
        return null == dojodebug ? "false" : dojodebug;
    }

    private boolean showAll = false;
    public void setShowAll(boolean val) {
        this.showAll = val; //(null != val && val.equals("yes"));
    }
    public boolean isShowAll() {
        return showAll;
    }

    /**
     * The 'mgr' param is now an alias for 'uid', but it shouldn't override it.
     */
    public void setMgr(String name) {
        if (null == uid || uid.length() == 0) {
            this.uid = name;
        }
    }

    /**
     * @return the long now
     */
    public long getNow() {
        return now;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    private boolean brokenRuleDisplaysGrouping = false;
    public boolean isBrokenRuleDisplaysGrouping() {
        return brokenRuleDisplaysGrouping;
    }
    public void setBrokenRuleDisplaysGrouping(boolean brdg) {
        this.brokenRuleDisplaysGrouping = brdg;
    }

    private HttpServletRequest request;
    public void setServletRequest(HttpServletRequest req) {
        this.request = req;
    }

    public String getUserAgent() {
        return request.getHeader("user-agent");
    }

    private boolean isMozilla;
    public boolean isMozilla() {
        return isMozilla;
    }

    private boolean isMac;
    public boolean isMac() {
        return isMac;
    }

    /**
     * Need this to build a URL to the dojo javascript library that doesn't
     * have the jsessionid embedded, since that chokes dojo.
     */
    public String getContextPath () {
        return request.getContextPath();
    }

    protected String remoteUser;
    public void setPrincipalProxy(PrincipalProxy pp) {
        this.remoteUser = pp.getRemoteUser();
    }

    /**
     * @return the remoteUser
     */
    public String getRemoteUser() {
        return remoteUser;
    }

    public MotD getMotd() {
        return Repository.getMotd();
    }

    public DataSource getDataSource() {
        return dataSource;
    }

    public DataSource getNpiDataSource() {
        return DataSource.NPI;
    }

    public DataSource getRliDataSource() {
        return DataSource.RLI;
    }

    public DataSource getGnatsDataSource() {
        return DataSource.GNATS;
    }

    public DataSource getReleasesDataSource() {
        return DataSource.RELEASES;
    }

    public DataSource getUserDataSource() {
        return DataSource.USERSOURCE;
    }

    public DataSource getReviewtrackerDataSource() {
        return DataSource.REVIEWTRACKER;
    }
    public DataSource getCRStrackerDataSource() {
        return DataSource.CRSTRACKER;
    }
    public DataSource getNEWLYINPRDataSource() {
        return DataSource.NEWLYINPRFIX;
    }
}
