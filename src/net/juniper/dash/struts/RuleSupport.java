/**
 * dbergstr@juniper.net, Dec 12, 2006
 *
 * Copyright (c) 2006, Juniper Networks, Inc.
 * All rights reserved.
 */
package net.juniper.dash.struts;

import java.util.Arrays;
import java.util.List;

import net.juniper.dash.DashboardException;
import net.juniper.dash.Milestone;
import net.juniper.dash.Objective;
import net.juniper.dash.Repository;
import net.juniper.dash.Rule;

import org.apache.log4j.Logger;

import com.opensymphony.xwork2.Preparable;

/**
 * Extra plumbing for Rule display.
 *
 * FIXME The admin stuff should be moved to a separate class.
 *
 * @author dbergstr
 */
public class RuleSupport extends DashSupport implements Preparable {

    static Logger logger = Logger.getLogger(RuleSupport.class.getName());

    private static final long serialVersionUID = 2L;

    protected String ruleid;
    private Rule rule;
    private boolean cloning;

    private String ruleAsXML;

    public List<Rule.Counts> getAllCounts() {
        return Arrays.asList(Rule.Counts.values());
    }

    public List<Milestone> getAllMilestones() {
        return Arrays.asList(Milestone.values());
    }

    public List<Objective> getAllObjectives() {
        return Arrays.asList(Objective.values());
    }

    public String fromStream() throws Exception {
        super.execute();
        rule = Rule.fromXML(ruleAsXML);
        ruleid = rule.getIdentifier();
        return SUCCESS;
    }

    public String doStream() throws Exception {
        super.execute();
        ruleAsXML = rule.toXML();
        return SUCCESS;
    }

    public String doDelete() throws Exception {
        logger.info("doDelete called.");
        super.execute();
        if (! isAdmin) {
            errorMessage = "Only admins may delete rules.";
            return ERROR;
        }
        try {
            Repository.deleteRule(ruleid);
        }
        catch (DashboardException e) {
            errorMessage = e.getMessage();
            return ERROR;
        }
        title = "Rule '" + ruleid + "' Deleted";
        return SUCCESS;
    }

    public String doCheck() throws Exception {
        super.execute();
        if (! isAdmin) {
            errorMessage = "Only admins may administer rules.";
            return ERROR;
        }
        try {
            Repository.getRuler().parseRule(rule);
        }
        catch (DashboardException e) {
            errorMessage = e.getMessage();
            return ERROR;
        }
        title = "Rule '" + ruleid + "' Validated";
        return SUCCESS;
    }

    public String doActivity() throws Exception {
        super.execute();
        if (! isAdmin) {
            errorMessage = "Only admins may administer rules.";
            return ERROR;
        }
        try {
            if (rule.isActive()) {
                // We're deactivating an active rule.
                Repository.getRuler().removeRule(rule);
                title = "Rule '" + ruleid + "' De-Activated";
            } else {
                Repository.getRuler().addRuleToRuleBase(rule);
                title = "Rule '" + ruleid + "' Activated";
            }
        }
        catch (DashboardException e) {
            errorMessage = e.getMessage();
            return ERROR;
        }
        return SUCCESS;
    }

    public String doReload() throws Exception {
        super.execute();
        if (! isAdmin) {
            errorMessage = "Only admins may administer rules.";
            return ERROR;
        }
        if (! rule.isActive()) {
            errorMessage = "Rule '" + ruleid + "' is inactive, and thus can " +
            		"not be RE-loaded.";
            return ERROR;
        }
        try {
            Repository.getRuler().removeRule(rule);
            Repository.getRuler().addRuleToRuleBase(rule);
            title = "Rule '" + ruleid + "' Reloaded";
        }
        catch (DashboardException e) {
            errorMessage = e.getMessage();
            return ERROR;
        }
        return SUCCESS;
    }

    public String doSave() throws Exception {
        super.execute();
        if (! isAdmin) {
            errorMessage = "Only admins may administer rules.";
            return ERROR;
        }
        Repository.getRuler().saveRule(rule);
        title = "Rule '" + ruleid + "' Saved";
        return SUCCESS;
    }

    public String doEditOrCreate() throws Exception {
        super.execute();
        if (! isAdmin) {
            errorMessage = "Only admins may edit rules.";
            return ERROR;
        }
        rule.processEdit();
        title = "Rule '" + ruleid + "' Processed";
        return SUCCESS;
    }

    public String doShowEdit() throws Exception {
        super.execute();
        if (cloning) {
            title = "Clone";
        } else {
            title = "Edit";
        }
        title = title + " rule '" + ruleid + "'";
        return SUCCESS;
    }

    public String execute() throws Exception {
        String v = super.execute();
        rule = Repository.getRule(ruleid);
        if (null == rule) {
            title = "Details of Rule \"" + ruleid + "\"";
            errorMessage = "There is no rule with identifier '" + ruleid +
                "' in the Dashboard.";
            return ERROR;
        }
        title = "Details of Rule \"" + rule.getName() + "\"";
        return v;
    }

    public void prepare() {
        rule = Repository.getRule(ruleid);
        if (null == rule) {
            rule = new Rule();
        }
    }

    public void setRuleid(String id) {
        ruleid = id;
    }

    /**
     * @return the rule
     */
    public Rule getRule() {
        return rule;
    }

    public void setRule(Rule rule) {
        this.rule = rule;
        this.ruleid = rule.getIdentifier();
    }

    /**
     * @return the cloning
     */
    public boolean isCloning() {
        return cloning;
    }

    /**
     * @param cloning the cloning to set
     */
    public void setCloning(boolean cloning) {
        this.cloning = cloning;
    }


    /**
     * @return the ruleAsXML
     */
    public String getRuleAsXML() {
        return ruleAsXML;
    }


    /**
     * @param ruleAsXML the ruleAsXML to set
     */
    public void setRuleAsXML(String ruleAsXML) {
        this.ruleAsXML = ruleAsXML;
    }
}
