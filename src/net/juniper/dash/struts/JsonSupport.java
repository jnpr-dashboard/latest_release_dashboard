package net.juniper.dash.struts;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import net.juniper.dash.JSONSerializable;
import net.juniper.dash.Milestone;
import net.juniper.dash.Objective;
import net.juniper.dash.Repository;
import net.juniper.dash.Rule;
import net.juniper.dash.data.DataSource;
import net.juniper.dash.data.User;
import net.juniper.dash.data.UserSource;

import org.apache.struts2.interceptor.ServletRequestAware;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

import flexjson.JSONSerializer;

/**
 * Action class for JSON requests.
 */
public class JsonSupport extends ActionSupport
        implements ServletRequestAware {

    /**
     * Implementing Serializable
     */
    private static final long serialVersionUID = -5290716972127708293L;

    protected String infoMessage = "";
    public String getInfoMessage() {
        return null ==  infoMessage ? "" : infoMessage;
    }

    protected String errorMessage = "";
    public String getErrorMessage() {
        return null == errorMessage ? "" : errorMessage;
    }

    public String execute() throws Exception {
        String actionName = ActionContext.getContext().getName();
        if (actionName.equals("ds-records")) {
            DataSource dataSource = DataSource.sourceByType(ds);
            if (! (dataSource instanceof JSONSerializable)) {
                if (null == dataSource) {
                    errorMessage = "DataSource '" + ds +
                        "' does not exist.";
                } else {
                    errorMessage = "DataSource '" + dataSource.getName() +
                    "' does not support JSON serialization.";
                }
                return ERROR;
            }
            output = ((JSONSerializable) dataSource).serializeRecords();

        } else if (actionName.equals("users")) {
            UserSource us = DataSource.USERSOURCE;
            output = us.serialize(Arrays.asList(usernames.split("[\\s,;]+")));

        } else if (actionName.equals("users-since")) {
            UserSource us = DataSource.USERSOURCE;
            output = us.serialize(us.getChangedSince(since));

        } else if (actionName.equals("usernames-since")) {
            UserSource us = DataSource.USERSOURCE;
            output = new JSONSerializer().serialize(us.getChangedSince(since));

        } else if (actionName.equals("all-usernames")) {
            UserSource us = DataSource.USERSOURCE;
            output = new JSONSerializer().serialize(us.getUserNames());

        } else if (actionName.equals("all-managers")) {
            UserSource us = DataSource.USERSOURCE;
            Set<String> mgrs = new HashSet<String>();
            for (User u: us.allUsers()) {
                if (u.isAManager()) {
                    mgrs.add(u.getId());
                }
            }
            mgrs.add(User.JUNOS.getId());
            output = us.serialize(mgrs);

        } else if (actionName.equals("all-pseudos")) {
            UserSource us = DataSource.USERSOURCE;
            Set<String> pseudos = new HashSet<String>();
            for (User u: us.allUsers()) {
                if (u.isPseudoUser() && ! u.isJunos()) {
                    pseudos.add(u.getId());
                }
            }
            output = us.serialize(pseudos);
            
        } else if (actionName.equals("all-virtual-teams")) {
            UserSource us = DataSource.USERSOURCE;
            Set<String> vts = new HashSet<String>();
            for (User u: us.allUsers()) {
                if (u.isVirtualTeam()) {
                    vts.add(u.getId());
                }
            }
            output = us.serialize(vts);

        } else if (actionName.equals("everyone-else")) {
            UserSource us = DataSource.USERSOURCE;
            Set<String> users = new HashSet<String>();
            for (User u: us.allUsers()) {
                if (! (u.isVirtualTeam() || u.isAManager() || u.isPseudoUser())) {
                    users.add(u.getId());
                }
            }
            output = us.serialize(users);

        } else if (actionName.equals("rules")) {
            output = Rule.serializeAll();

        } else if (actionName.equals("milestones")) {
            output = Milestone.serializeAll();

        } else if (actionName.equals("sources")) {
            output = DataSource.serializeAll();
            
        } else if (actionName.equals("objectives")) {
            output = Objective.serializeAll();

        } else if (actionName.equals("misc")) {
            output = Repository.serializeMisc();
            
        } else if (actionName.equals("index")) {
            return "index";
        
        } else if (actionName.equals("write")) {
            infoMessage = "JSON written.";
            Repository.writeJson(false);
            return "index";

        } else if (actionName.equals("exception")) {
            throw new Exception("your mama");

        } else {
            errorMessage = "Unknown JSON action \"" + actionName +
                "\" requested.";
            return ERROR;
        }

        return SUCCESS;
    }

    private HttpServletRequest request;
    public void setServletRequest(HttpServletRequest req) {
        this.request = req;
    }

    private String output = "";
    public String getOutput() {
        return output;
    }

    private String usernames = "";
    public void setUsernames(String val) {
        usernames = val;
    }

    String ds= "";
    public void setDs(String type) {
        ds = type;
    }

    long since = 0;
    public void setSince(long val) {
        since = val;
    }
    public long getSince() {
        return since;
    }

    public String jsonify(String name, String value) {
        return new JSONSerializer().prettyPrint(name, value);
    }
}
