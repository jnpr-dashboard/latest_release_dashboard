package net.juniper.dash.struts;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import javax.servlet.http.HttpServletRequest;

import net.juniper.dash.Repository;
import net.juniper.dash.data.DataSource;
import net.juniper.dash.data.Record;
import net.juniper.dash.data.User;

import org.apache.struts2.interceptor.ServletRequestAware;

import com.opensymphony.xwork2.ActionSupport;

/**
 * Action class for static resources, which need the context path, but do not
 * otherwise change over the life of the application.
 */
public class StaticSupport extends ActionSupport
        implements ServletRequestAware {

    /**
     * Implementing Serializable
     */
    private static final long serialVersionUID = -5290716972127708293L;

    public String getVersion() {
        return Repository.VERSION;
    }
    public String getReleaseDate() {
        return Repository.RELEASE_DATE;
    }

    public String execute() throws Exception {
        return SUCCESS;
    }

    private HttpServletRequest request;
    public void setServletRequest(HttpServletRequest req) {
        this.request = req;
    }

    /**
     * Need this to build a URL to the dojo javascript library that doesn't
     * have the jsessionid embedded, since that chokes dojo.
     */
    public String getContextPath () {
        return request.getContextPath();
    }

    DataSource dataSource = null;
    public void setDs(String type) {
        dataSource = DataSource.sourceByType(type);
    }

    /** ss is the search string for AJAX autocompletion */
    String ss = "";
    public void setSs(String val) {
        ss = val;
    }
    public String getSs() {
        return ss;
    }

    /**
     * Autocompletion search for usernames.
     *
     * This is hard to implement in struts tags.
     *
     * @return a List of usernames that start with the value.
     */
    public List<String> getUserCompletionList(String value) {
        if (null == value || value.length() == 0) {
            return Collections.emptyList();
        }
        value = value.toLowerCase();
        boolean found = false;
        List<String> results = new ArrayList<String>();
        for (String id : DataSource.USERSOURCE.getUserNames()) {
            if (id.startsWith(value)) {
                found = true;
                results.add(id);
            } else if (found) {
                // We're past the part of the list that matches
                break;
            }
        }
        return results;
    }

    /**
     * Autocompletion search for Records by DataSource.
     *
     * @return a List of Records whose name contains the value, or whose id
     * starts with the value.
     */
    public Set<Record> getRecordCompletions() {
        if (null == ss || ss.length() == 0) {
            return Collections.emptySet();
        }
        ss = ss.toLowerCase();
        Set<Record> results = new TreeSet<Record>();
        for (Record r : dataSource.getRecordCollection()) {
            if (r.getId().startsWith(ss) ||
                r.getName().toLowerCase().contains(ss)) {
                results.add(r);
            }
        }
        return results;
    }
}
