/**
 * dbergstr@juniper.net, Dec 12, 2006
 *
 * Copyright (c) 2006-2007, Juniper Networks, Inc.
 * All rights reserved.
 */
package net.juniper.dash.struts;

import java.util.Collection;
import java.util.SortedSet;

import net.juniper.dash.Group;
import net.juniper.dash.MotD;
import net.juniper.dash.Repository;
import net.juniper.dash.Rule;
import net.juniper.dash.data.DataSource;
import net.juniper.dash.data.Record;
import net.juniper.dash.data.User;
import net.juniper.dash.data.UserSource;

/**
 * Bits for administrative pages.
 *
 * @author dbergstr
 */
public class AdminSupport extends DashSupport {
    private static final long serialVersionUID = 2L;

    public int scoreForRelease(String rel) {
        return User.JUNOS.getGroupingStatus(DataSource.RELEASES.getRecord(rel)).
            getTotalScore();
    }

    public SortedSet getRefreshers() {
        return Repository.listRefreshers();
    }

    public String getConnCacheStats() {
        return Repository.getConnectionCacheStats();
    }

//    public Collection<User> getAllVirtualTeams() {
//        return ((UserSource) getUserDataSource()).getAllVirtualTeams();
//    }

    public Collection<User> getAllUsers() {
        return Repository.getAllUsers();
    }

    public Collection<Group> getAllGroups() {
        return Repository.groups();
    }

    public int getUpdateCycles() {
        return Repository.getUpdateCycles();
    }

    public SortedSet<DataSource> getDataSources() {
        return DataSource.sources();
    }

    public boolean isShowingAdmin() {
        return true;
    }

    public DataSource getDataSource() {
        return dataSource;
    }

    private String dsName = "";
    public String getDsName() {
        return dsName;
    }
    public void setDsName(String val) {
        this.dsName = val;
    }

    private String recId = "";
    public String getRecId() {
        return recId;
    }
    public void setRecId(String val) {
        this.recId = val;
    }

    /**
     * @return the pauseMinutes
     */
    public int getPauseMinutes() {
        return Repository.updatePauseMinutes;
    }

    /**
     * @param pauseMinutes the pauseMinutes to set
     */
    public void setPauseMinutes(int pauseMinutes) {
        this.pauseMinutes = pauseMinutes;
    }

    private Record record;
    private MotD motd;
    private int pauseMinutes;

    public Record getRecord() {
        return record;
    }

    public String doReconnectDB() throws Exception {
        super.execute();
        Repository.setupConnectionPool();
        return SUCCESS;
    }

    public String doFireRules() throws Exception {
        super.execute();
        Repository.fireAllRules();
        Repository.writeJson(true);
        return SUCCESS;
    }

    public String doWakeUpdater() throws Exception {
        super.execute();
        Repository.wakeUpStatusProcessor();
        return SUCCESS;
    }

    public String doRestartProcessing() throws Exception {
        super.execute();
        Repository.restartProcessing();
        return SUCCESS;
    }

    public String doReloadMultipliers() throws Exception {
        super.execute();
        Rule.loadMultipliers();
        return SUCCESS;
    }

    public void setMotd(MotD m) {
        motd = m;
    }

    public MotD getMotd() {
        return motd;
    }

    public String updateMotD() throws Exception {
        super.execute();
        Repository.setMotd(motd);
        return SUCCESS;
    }

    public String doPauseUpdates() throws Exception {
        super.execute();
        Repository.updatePauseMinutes = pauseMinutes;
        return SUCCESS;
    }

    public String doLoadRules() throws Exception {
        super.execute();
        int cnt = Rule.loadRules(dirname, skipExisting);
        title = "Rules Loaded";
        infoMessage = cnt + " Rules loaded from '" + dirname + "'";
        return SUCCESS;
    }

    public String doPersistAllRules() throws Exception {
        super.execute();
        int cnt = Rule.persistAllRules(dirname);
        title = "Rules Persisted";
        infoMessage = cnt + " Rules written to '" + dirname + "'";
        return SUCCESS;
    }

    public String doSaveAllChangedRules() throws Exception {
        super.execute();
        int cnt = Rule.saveAllChangedRules();
        title = "Rules Saved";
        infoMessage = cnt + " Rules saved to the database.";
        return SUCCESS;
    }

    public String execute() throws Exception {
        super.execute();
        if (null == motd) motd = Repository.getMotd();
        if (null != dsName && dsName.length() > 0) {
            for (DataSource ds : DataSource.sources()) {
                if (ds.getName().equals(dsName)) {
                    dataSource = ds;
                    break;
                }
            }
        }
        if (null != recId && recId.length() > 0) {
            record = dataSource.getRecord(recId);
        }

        return SUCCESS;
    }

    private String dirname;

    private boolean skipExisting;

    public String getDirname() {
        return dirname;
    }

    /**
     * Used in loadRules and persistAllRules
     */
    public void setDirname(String dirname) {
        this.dirname = dirname;
    }

    public boolean isSkipExisting() {
        return skipExisting;
    }

    /**
     * Used in loadRules
     */
    public void setSkipExisting(boolean skipExisting) {
        this.skipExisting = skipExisting;
    }
}
