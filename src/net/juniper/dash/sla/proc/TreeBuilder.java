package net.juniper.dash.sla.proc;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Iterator;
import java.util.Comparator;

import net.juniper.dash.sla.conf.Constants;
import net.juniper.dash.sla.bean.Org;

public class TreeBuilder {
	
    private Map<String, Org> tree;
    private String product;
    private Map<String, List<String>> bgbus; 
    private Map<String, ArrayList<String>> buOrgChart; //orgs for each bu with relations
    private Map<String, Set<String>> buManagers;       //each bu's managers;
    
    private Comparator<String> strcp;
    private Comparator<String> strcp2;
    
    public TreeBuilder(){
    	tree = new HashMap();
    	Org u = new Org();
    	u.setOrgId(Constants.ROOT);
    	u.setLeaf(false);
    	tree.put(Constants.ROOT, u);
    	buManagers = new HashMap();
    	buOrgChart = new HashMap();
    	
    	strcp = new StringComparator();
        strcp2 = new StringComparator2();
    }
    
    public  Map<String, Org> getTree(){
    	return tree;
    }
      
    public void setProduct(String product){
    	this.product = product;
    	Org u = new Org();
    	u.setOrgId(this.product);
    	u.setSuperId(Constants.ROOT);
    	u.setLeaf(false);
    	tree.put(product, u);
    }
    
    public void setBGBUs(Map<String, List<String>> bgbus){
    	this.bgbus = bgbus;
    }
  
    public void setBUOrgChart(Map<String, ArrayList<String>> buOrgChart){
    	this.buOrgChart =buOrgChart;  	
    }
    
    public Map<String, ArrayList<String>> getBUOrgChart(){
    	return buOrgChart;  	
    }
    
    public Map<String, Set<String>> getBUManagers(){
    	return buManagers;  	
    }
    
    public void setBUManagers(Map<String, Set<String>> buManagers){
    	this.buManagers = buManagers;  	
    }
    
    public void buildTree(String prodRoot){
    	Org u;
    	List<String> bus;
    	Iterator k = bgbus.keySet().iterator();
    	String bg;
    	Set<String> buMgr;
    	while (k.hasNext()){  
    		
    		//add product to the tree, to be extended to multiple products, junos, screenos, ...
    		bg = (String) k.next();
    		u = new Org();
	    	u.setOrgId(bg);
	    	u.setSuperId(prodRoot);
	    	u.setLeaf(false);
	    	tree.put(bg, u);
    		bus = bgbus.get(bg);
    		for (String bu: bus){
    			
    			//add bu to the tree
    			u = new Org();
    	    	u.setOrgId(bu);
    	    	u.setSuperId(bg);
    	    	u.setLeaf(false);
    	    	tree.put(bu, u);
    	    	
    	    	//add bu's manager, bu's children and grand children to the tree recursively
    	    	if (buOrgChart.containsKey(bu)) {
    	    		buMgr = buManagers.get(bu);
    	    		for (String mgr: buMgr){
    	    			u = new Org();
    	    	    	u.setOrgId(mgr);
    	    	    	u.setSuperId(bu);
    	    	    	u.setLeaf(buildSubTree(bu, mgr));   
    	    	    	tree.put(mgr, u);
    	    		}
    	    	}
    		}   		
    	}
    }
    
    private boolean buildSubTree(String bu, String parent){
    	boolean isLeaf = true;
    	int first;
    	ArrayList<String> workers = buOrgChart.get(bu);
    	
    	//find the all orgs under parent, or return true if parent is a leaf already
    	List<String> l = new ArrayList();
    	for (String w: workers){
    		if (w.startsWith(parent+Constants.COLON)){  			
    			isLeaf = false;
    			l.add(w);
    		}
    	}
    	
    	if (isLeaf){
    		return true;
    	}
    	//find the direct reports to the parent
    	int counter;
    	String[] tmp;
    	Map<String, Integer> m = new HashMap();

    	for (String w: l){
    		tmp = w.split(Constants.COLON);
    		first = Collections.binarySearch(workers, tmp[1], strcp2);
    		if (first < 0){
				continue;
			}
			if (m.containsKey(tmp[1])){
				counter = m.get(tmp[1]);
				m.put(tmp[1], ++counter);
			} else {
				m.put(tmp[1], 1);
			}
			for (int i=first + 1; i<workers.size() && workers.get(i).endsWith(Constants.COLON+tmp[1]); i++){
				if (m.containsKey(tmp[1])){
					counter = m.get(tmp[1]);
					m.put(tmp[1], ++counter);
				} else {
					m.put(tmp[1], 1);
				}
			}
			for (int i=first -1; i >= 0 && workers.get(i).endsWith(Constants.COLON+tmp[1]); i--){
				if (m.containsKey(tmp[1])){
					counter = m.get(tmp[1]);
					m.put(tmp[1], ++counter);
				} else {
					m.put(tmp[1], 1);
				}
			}
    	}
    	List<String> diectReports = new ArrayList();
    	for (String k: m.keySet()){
    		if (m.get(k)>1){
    			workers.remove(parent+Constants.COLON+k);
    		} else {
    			diectReports.add(k);
    		}
    	}
    	for (String u : l){
    		 workers.remove(u);
    	}
    	
    	// add these direct reports to tree 
    	for (String report: diectReports){
    	    Org u = new Org();
    	    u.setOrgId(report);
    	    u.setSuperId(parent);
    	    u.setLeaf(buildSubTree(bu, report));
    	    tree.put(report, u);
    	}
    	return false;
    }
    
    public void sumUp(Org parent){
    	if (parent.isLeaf()){
    		return;
    	}
    	Org child;
    	String sid;
    	for (String key: tree.keySet()){
    		child = tree.get(key);
    		sid = child.getSuperId();
    		if (sid == null){
    			continue;
    		}
    	    if (sid.equals(parent.getOrgId())){
    	    	sumUp(child);
    	    	parent.setAckTime(parent.getAckTime() + child.getAckTime());
    			parent.setAckTimePrs(parent.getAckTimePrs() + child.getAckTimePrs());
    			parent.setAckTimePrsList(child.getAckTimePrsList());
    			
    			parent.setAssignTime(parent.getAssignTime()+ child.getAssignTime());
    			parent.setAssignTimePrs(parent.getAssignTimePrs()+ child.getAssignTimePrs());
    			parent.setAssignTimePrsList(child.getAssignTimePrsList());
    			
    			parent.setInfoStat(parent.getInfoStat() + child.getInfoStat());
    			parent.setInfoStatPrs(parent.getInfoStatPrs() + child.getInfoStatPrs());
    			parent.setInfoStatPrsList(child.getInfoStatPrsList());
    			
    			parent.setInfoTime(parent.getInfoTime() + child.getInfoTime());
    			parent.setInfoTimePrs(parent.getInfoTimePrs() + child.getInfoTimePrs());
    			parent.setInfoTimePrsList(child.getInfoTimePrsList());
    			
    			parent.setPctInfoStatPrsA(parent.getPctInfoStatPrsA() + child.getPctInfoStatPrsA());
    			parent.setPctInfoStatPrsB(parent.getPctInfoStatPrsB() + child.getPctInfoStatPrsB());
    			parent.setPctInfoStatPrsBList(child.getPctInfoStatPrsBList());
    			parent.setPctInfoStatPrsAList(child.getPctInfoStatPrsAList());
    			
    			parent.setVerifyTime(parent.getVerifyTime() + child.getVerifyTime());
    			parent.setVerifyTimePrs(parent.getVerifyTimePrs() + child.getVerifyTimePrs()); 
    			parent.setVerifyTimePrsList(child.getVerifyTimePrsList());
    	    }
		} 
    }
    
    public class StringComparator implements Comparator<String> {
        public int compare(String s1, String s2) {
             return s1.split(Constants.COLON)[1].compareTo(s2.split(Constants.COLON)[1]);           
        }
    }
    
    private class StringComparator2 implements Comparator<String> {
        public int compare(String s1, String s2) {
             return s1.split(Constants.COLON)[1].compareTo(s2);           
        }
    }
}
