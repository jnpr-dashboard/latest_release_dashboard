/**
 * allenyu@juniper.net, Dec 9, 2011
 *
 * Copyright (c) 2011, Juniper Networks, Inc.
 * All rights reserved.
 */
package net.juniper.dash.sla.proc;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Set;
import java.util.HashSet;

import javax.naming.NamingException;

import net.juniper.dash.sla.conf.Constants;
import net.juniper.dash.sla.dao.LDAPDao;
import net.juniper.dash.sla.util.SLALogger;

public class LDAPAdapter implements Runnable {
	
	private LDAPOrgChartProcessor parent;
	private String buname;
	private Set<String> members;
	private SLALogger logger;
	
	public LDAPAdapter (LDAPOrgChartProcessor parent, String buname, Set<String> members) {
		this.parent = parent;
		this.buname = buname;
		this.members = members;
		logger = SLALogger.getInstance();
	}
	

	@Override
	public void run() {
		try {
			Set<String> reports;
			ArrayList<String> list = new ArrayList();
		    for (String m: members){
		    	reports = LDAPDao.getReports(m);
		    	if (reports.size() > 0) {
		    		for (String r: reports){
	    	    		list.add(m + Constants.COLON + r);
	    	    	}
		    	} else {
		    		list.add(m + Constants.COLON + m);
		    	}
		    }
		    
		    //Get all managers of BU
		    Set<String> set = new HashSet();
			Set<String> set2 = new HashSet();
			Set<String> set3 = new HashSet();
			String[] strs;
			for (String s: list){
				strs = s.split(Constants.COLON);
				if (!strs[0].equals(strs[1])){
					set.add(s);
					set2.add(strs[1]);
				} else {
					set3.add(s);
				}
			}
			Set<String> set4 = new HashSet(); //hold BU managers
			String tmp;
			for (String s: set){
				tmp = s.split(Constants.COLON)[0];
				if (!set2.contains(tmp)){
					set4.add(tmp);
				}
			}
			for (String s: set3){
				tmp = s.split(Constants.COLON)[0];
				if (!set2.contains(tmp)){
					set4.add(tmp);
				} 
			} 
			//Remove xxx:xxx from list
			for (String s: set3){
				list.remove(s);
			}
			Collections.sort(list, new StringComparator());
			parent.getBUOrgChart().put(buname, list);
			parent.getBUManagers().put(buname, set4);
		} catch (NamingException e){
			logger.logException(e);
		}
	}
	
    public class StringComparator implements Comparator<String> {
        public int compare(String s1, String s2) {
             return s1.split(Constants.COLON)[1].compareTo(s2.split(Constants.COLON)[1]);           
        }
    }
}
