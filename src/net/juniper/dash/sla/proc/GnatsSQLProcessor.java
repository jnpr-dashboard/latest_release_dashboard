/**
 * allenyu@juniper.net, Oct 26, 2011
 *
 * Copyright (c) 2011, Juniper Networks, Inc.
 * All rights reserved.
 */
package net.juniper.dash.sla.proc;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Map;
import java.util.HashMap;
import java.util.Set;

import net.juniper.dash.sla.conf.Constants;
import net.juniper.dash.sla.util.SLALogger;
import net.juniper.dash.sla.SLARunner;
import net.juniper.dash.sla.bean.Org;
import net.juniper.dash.sla.dao.GnatsDao;

public class GnatsSQLProcessor implements Runnable {

	private String job;
	private Map<String, Org> tree;
	private GnatsDao dao;
	private Set<String> set;
	private SLARunner parent;
	SLALogger logger;
	
	public GnatsSQLProcessor(SLARunner parent, String job, Map<String, Org> tree, GnatsDao dao){
		this.job = job.toLowerCase();
		this.tree = tree;
		this.dao = dao;
		this.parent = parent;
		set = new HashSet();
		logger = SLALogger.getInstance();
	}
	
	@Override
	public void run()  {
			
		Class cls = this.getClass();
		try {
			dao.setup();
			Method method = cls.getDeclaredMethod(job);
			method.invoke(this);	
		} catch (ClassNotFoundException e) {
			parent.setDoPersistence(false);
			logger.logException(e);
		} catch (SQLException e) {
			parent.setDoPersistence(false);
			logger.logException(e);
		} catch (SecurityException e) {
			parent.setDoPersistence(false);
			logger.logException(e);
		} catch (NoSuchMethodException e) {
			logger.logException(e);
	    } catch (IllegalAccessException e) {
	    	parent.setDoPersistence(false);
	    	logger.logException(e);
	    } catch (InvocationTargetException e) {
	    	parent.setDoPersistence(false);
	    	logger.logException(e);
	    } catch (Exception e) {
	    	parent.setDoPersistence(false);
	    	logger.logException(e);
	    } catch (Error e)  {
	    	parent.setDoPersistence(false);
	    } finally {
	    	try {
	    	   dao.close(true);
	    	} catch (SQLException e){
	    		parent.setDoPersistence(false);
	    	}
	    }
	}
	
	private void ack_time() throws SQLException {
        set = dao.getAckTime();
		Set<String> set2;
		Org org;
		String[] strs;
		String pr = Constants.EMPTY;
		float ack = 0;
		for (String key: set){
			strs = key.split(Constants.COLON);
			if (strs.length < 4 || strs[0] == null||Constants.EMPTY.equals(strs[0]) || strs[1] == null||Constants.EMPTY.equals(strs[1])
					|| strs[2] == null||Constants.EMPTY.equals(strs[2]) || strs[3] == null||Constants.EMPTY.equals(strs[3])){
					continue;
			}
			if (tree.containsKey(strs[0])){
				org = tree.get(strs[0]);
			} else {
				if (tree.containsKey(strs[3])){
					org = tree.get(strs[3]);
				} else {
				    continue;
				}
			}
			if (strs[2] != null && !Constants.EMPTY.equals(strs[2])){
        		try {
        		    ack = Float.parseFloat(strs[2]);
        		    if (ack < 0 ) {
        		    	ack = 0;
        		    }
        		} catch (NumberFormatException e){
        			ack = 0;
        		}
        	} 
			set2 = new HashSet();
			set2.add(strs[1]);
			synchronized (org){
				org.setAckTimePrs(org.getAckTimePrs() + 1);
				org.setAckTime(org.getAckTime() + ack);
				org.setAckTimePrsList(set2);
			}
		}
	}
	
	private void assign_time() throws SQLException {
        set = dao.getAssignTime();
		Set<String> set2;
		Org org;
		String[] strs;
		float assign = 0;
		for (String key: set){
			strs = key.split(Constants.COLON);
			if (strs.length < 4 || strs[0] == null||Constants.EMPTY.equals(strs[0]) || strs[1] == null||Constants.EMPTY.equals(strs[1])
					|| strs[2] == null || Constants.EMPTY.equals(strs[2]) || strs[3] == null ||Constants.EMPTY.equals(strs[3])){
					continue;
			}
			if (tree.containsKey(strs[0])){
				org = tree.get(strs[0]);
			} else {
				if (tree.containsKey(strs[3])){;
					org = tree.get(strs[3]);
				} else {
				    continue;
				}
			}
			if (strs[2] != null && !Constants.EMPTY.equals(strs[2])){
        		try {
        		    assign = Float.parseFloat(strs[2]);
        		    if (assign < 0 ) {
        		    	assign = 0;
        		    }
        		} catch (NumberFormatException e){
        			assign = 0;
        		}
        	} 
			set2 = new HashSet();
			set2.add(strs[1]);
			synchronized (org){
				org.setAssignTimePrs(org.getAssignTimePrs() + 1);
				org.setAssignTime(org.getAssignTime() + assign);		
				org.setAssignTimePrsList(set2);
			}
		} 
	}
	
	private void info_time_stat() throws SQLException {       
		set = dao.getInfoTimeStat();
		Map<String, String> m1 = new HashMap();
		Map<String, String> m2 = new HashMap();
		Set<String> set3;
		Org org;
		String[] strs;
		String[] strs2;
		String tmp = Constants.EMPTY;
		float time = 0;
		float stat = 0;
		for (String key: set){
			strs = key.split(Constants.COLON);
			if (strs.length < 5 || strs[0] == null||Constants.EMPTY.equals(strs[0]) || strs[1] == null||Constants.EMPTY.equals(strs[1])
					|| strs[4] == null || Constants.EMPTY.equals(strs[4])){
					continue;
			}
			if (tree.containsKey(strs[0])){
				org = tree.get(strs[0]);
			} else {
				if (tree.containsKey(strs[4])){
					org = tree.get(strs[4]);
				} else {
				    continue;
				}
			}
			strs2 = strs[1].split(Constants.HYPHEN);
			if (strs[2] != null && !Constants.EMPTY.equals(strs[2]) && (!m1.keySet().contains(strs2[0]) || Integer.parseInt(m1.get(strs2[0])) > Integer.parseInt(strs2[1]))){		
        		try {
        		    time = Float.parseFloat(strs[2]);
        		    if (time < 0 ) {
        		    	time = 0;
        		    }
        		} catch (NumberFormatException e){
        			time = 0;
        		}
				synchronized (org){
					if (m1.keySet().contains(strs2[0])){
				    	for (String s: org.getInfoTimePrsList()) {
				    		if (s.startsWith(strs2[0])){
				    			tmp = s;
				    		}
				    	}
				    	if (!Constants.EMPTY.equals(tmp)){
			    		    org.getInfoTimePrsList().remove(tmp);
			    		}
				    	tmp = Constants.EMPTY;
				    } else {
				    	org.setInfoTimePrs(org.getInfoTimePrs() + 1);
						org.setInfoTime(org.getInfoTime() + time);
				    }
					set3 = new HashSet();
				    set3.add(strs[1]);
				    org.setInfoTimePrsList(set3);
				}
				m1.put(strs2[0], strs2[1]);
			}
			if (strs[3] != null && !Constants.EMPTY.equals(strs[3]) && (!m2.keySet().contains(strs2[0]) || Integer.parseInt(m2.get(strs2[0])) > Integer.parseInt(strs2[1]))){	
        		try {
        		    stat = Float.parseFloat(strs[3]);
        		} catch (NumberFormatException e){
        			stat = 0;
        		}
				synchronized (org){
					if (m2.keySet().contains(strs2[0])){
				    	for (String s: org.getInfoStatPrsList()) {
				    		if (s.startsWith(strs2[0])){
				    			tmp = s;
				    		}
				    	}
				    	if (!Constants.EMPTY.equals(tmp)){
			    		    org.getInfoStatPrsList().remove(tmp);
			    		}
				    	tmp = Constants.EMPTY;
				    } else {
				    	org.setInfoStatPrs(org.getInfoStatPrs() + 1);
						org.setInfoStat(org.getInfoStat() + stat);
				    }
					set3 = new HashSet();
				    set3.add(strs[1]);
				    org.setInfoStatPrsList(set3);
				}
				m2.put(strs2[0], strs2[1]);
        	} 
		} 
	}
	
	private void pct_info_stat() throws SQLException {
		set = dao.getPctInfoStat();
		Set<String> set2;
		Set<String> set3;
		Org org;
		String[] strs;
		for (String key: set){
			strs = key.split(Constants.COLON);
			if (strs.length < 4 || strs[0] == null || Constants.EMPTY.equals(strs[0]) || strs[1] == null || Constants.EMPTY.equals(strs[1]) || strs[3] == null || Constants.EMPTY.equals(strs[3])){
				continue;
			}
			if (tree.containsKey(strs[0])){
				org = tree.get(strs[0]);
			} else {
				if (tree.containsKey(strs[3])){
					org = tree.get(strs[3]);
				} else {
				    continue;
				}
			}
			set2 = new HashSet();
			set2.add(strs[1]);
			synchronized (org){
				if (strs[2] != null && !Constants.EMPTY.equals(strs[2])){
					org.setPctInfoStatPrsBList(set2);
				}
				org.setPctInfoStatPrsAList(set2);
			}
		} 		
	}
	
	private void verify_time() throws SQLException{       
        set = dao.getVerifyTime();
        Set<String> set1 = new HashSet();
		Set<String> set2;
		Org org;
		String[] strs;
		String pr = Constants.EMPTY;
		String tmp;
		float vt = 0;
		for (String key: set){
			strs = key.split(Constants.COLON);
			if (strs.length < 4 || strs[0] == null||Constants.EMPTY.equals(strs[0]) || strs[1] == null||Constants.EMPTY.equals(strs[1])
					|| strs[2] == null||Constants.EMPTY.equals(strs[2]) || strs[3] == null||Constants.EMPTY.equals(strs[3])){
					continue;
			}
			if (tree.containsKey(strs[0])){
				org = tree.get(strs[0]);
			} else {
				if (tree.containsKey(strs[3])){
					org = tree.get(strs[3]);
				} else {
				    continue;
				}
			}
			set2 = new HashSet();
			set2.add(strs[1]);
			tmp = strs[1].split(Constants.HYPHEN)[0];
			if (!set1.contains(tmp)){
				set1.add(tmp);
				if (strs[2] != null && !Constants.EMPTY.equals(strs[2])){
	        		try {
	        		    vt = Float.parseFloat(strs[2]);
	        		    if (vt < 0 ) {
	        		    	vt = 0;
	        		    }
	        		} catch (NumberFormatException e){
	        			vt = 0;
	        		}
	        	} 
				synchronized (org){
					org.setVerifyTimePrs(org.getVerifyTimePrs() + 1);
					org.setVerifyTime(org.getVerifyTime() + vt);
					org.setVerifyTimePrsList(set2);
			    }
			} else {
				synchronized (org){
					org.setVerifyTimePrsList(set2);
				}
			}
		}
	}
}
