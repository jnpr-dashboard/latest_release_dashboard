/**
 * allenyu@juniper.net, Dec 9, 2011
 *
 * Copyright (c) 2011, Juniper Networks, Inc.
 * All rights reserved.
 */
package net.juniper.dash.sla.proc;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.HashSet;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.io.FileInputStream;
import java.io.IOException;
import javax.naming.NamingException;


import net.juniper.dash.sla.conf.Constants;
import net.juniper.dash.sla.conf.SLAProperties;
import net.juniper.dash.sla.dao.LDAPDao;

public class LDAPOrgChartProcessor {
	
    private Set<String> allbus;
    private Map<String, List<String>> bgbus;
    private Map<String, ArrayList<String>> buOrgChart;
    private Map<String, Set<String>> buManagers;
    
    public LDAPOrgChartProcessor() throws NamingException {
    	buManagers = new HashMap();
    	buOrgChart = new HashMap();
    	allbus = new HashSet(); 
    	LDAPDao.initilize();
    }
    
    public Map<String, List<String>> getBGBUs(){
    	return bgbus;  	
    }
    
    public Map<String, ArrayList<String>> getBUOrgChart(){
    	return buOrgChart;  	
    }
    
    public Map<String, Set<String>> getBUManagers(){
    	return buManagers;  	
    }
    
    public void parseBGList() throws IOException {
    	String bglistfile = SLAProperties.getProperty("DASHBOARD_PATH") + Constants.SLASH + SLAProperties.getProperty("SPECIAL_DATA_PATH") + Constants.SLASH + SLAProperties.getProperty("BG_LIST");
    	Properties p = new Properties();
    	FileInputStream fis = new FileInputStream(bglistfile);
		p.load(fis);
		String bglist = p.getProperty("bg");
		String[] bgs = bglist.split(Constants.COMMA);
		String[] bgnbus;
		String[] bus;
		List<String> l;
		bgbus = new HashMap();
		for (String bg: bgs){
			bgnbus = bg.split(Constants.COLON);
			bus = bgnbus[1].split(Constants.SEMICOLON);
			l = new ArrayList();
			for (String s: bus){
				l.add(s);
				allbus.add(s);
			}
			bgbus.put(bgnbus[0], l);		
		}
    }
    
    public void fetchBUs() throws NamingException {
    	Set<String> members;
    	ExecutorService executor = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
    	for (String buname: allbus){
    	    members = LDAPDao.getBU(buname);
    	    executor.execute(new LDAPAdapter(this, buname, members));      		
    	}
    	executor.shutdown();
		while (!executor.isTerminated()) {}
    }
}
