/**
 * allenyu@juniper.net, Oct 28, 2011
 *
 * Copyright (c) 2011, Juniper Networks, Inc.
 * All rights reserved.
 */
package net.juniper.dash.sla.util;

import net.juniper.dash.sla.util.JunLogger;
import net.juniper.dash.sla.conf.SLAProperties;

public class SLALogger extends JunLogger {
	
	private static SLALogger slalogger;
	
	private SLALogger(){
		super(SLAProperties.getProperty("SLA_LOG_FILE"), "SLALogger");
	}
	
	public synchronized static SLALogger getInstance(){
		if (slalogger == null){
			slalogger = new SLALogger();
		}
		return slalogger;
	}
	

}
