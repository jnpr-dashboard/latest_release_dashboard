/**
 * allenyu@juniper.net, Oct 28, 2011
 *
 * Copyright (c) 2011, Juniper Networks, Inc.
 * All rights reserved.
 */
package net.juniper.dash.sla.util;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Timestamp;
import java.util.Date;
import java.util.logging.FileHandler;
import java.util.logging.Formatter;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class JunLogger {
	private Logger logger;
	private FileHandler handler;
	
	
	public void logMessage(String s){
	    logger.info(s);
	}
	
	public void logError(Error e){
		StringWriter trace = new StringWriter();
	    e.printStackTrace(new PrintWriter(trace));
	    logger.severe(trace.toString());
	}
	
	public void logException(Exception e){
	    StringWriter trace = new StringWriter();
	    e.printStackTrace(new PrintWriter(trace));
	    logger.severe(trace.toString());
	}
	
	protected JunLogger(String file, String clz){
		try {
		    // Create an appending file handler		       
		    DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");  
		    String s = df.format(new Date());  
		    file = file.replace(".", "_"+ s.substring(0, 10) + ".");  
		    handler = new FileHandler(file, true);
		    handler.setFormatter(new Formatter() {
		        public String format(LogRecord record) {
		            return  "[" + (new Timestamp(record.getMillis())) + "] "
		            		+ record.getLevel() + " : "
		                    + record.getMessage() + "\n";
		            }
		        });
		    logger = Logger.getLogger(clz);
		    logger.addHandler(handler);
		} catch (IOException e) {
			
		}
	}

}
