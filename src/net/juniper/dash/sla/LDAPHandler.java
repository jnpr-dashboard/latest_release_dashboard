/**
 * allenyu@juniper.net, Dec 9, 2011
 *
 * Copyright (c) 2011, Juniper Networks, Inc.
 * All rights reserved.
 */
package net.juniper.dash.sla;

import java.util.Date;
import java.util.Map;
import java.io.IOException;

import net.juniper.dash.sla.bean.Org;
import net.juniper.dash.sla.conf.SLAProperties;
import net.juniper.dash.sla.proc.LDAPOrgChartProcessor;
import net.juniper.dash.sla.util.SLALogger;
import net.juniper.dash.sla.dao.TreeDao;
import net.juniper.dash.sla.proc.TreeBuilder;


public class LDAPHandler {
    private static boolean doPersistence = true;
    private static Map <String, Org> tree;
	
	public static void main(String[] args){
		try {
		    SLAProperties.initialize();
		    SLALogger logger = SLALogger.getInstance();
		    logger.logMessage("LDAPHandler started work at " + new Date());
			try {
				LDAPOrgChartProcessor lop = new LDAPOrgChartProcessor();
				lop.parseBGList();
				lop.fetchBUs();
				TreeBuilder builder = new TreeBuilder();
				builder.setProduct(SLAProperties.getProperty("PRODUCT"));
				builder.setBGBUs(lop.getBGBUs());
				builder.setBUManagers(lop.getBUManagers());
				builder.setBUOrgChart(lop.getBUOrgChart());
				builder.buildTree(SLAProperties.getProperty("PRODUCT"));
				tree = builder.getTree();
			} catch (Exception e) {
				doPersistence = false;
				logger.logException(e);
			} catch (Error e)  {
				doPersistence = false;
				logger.logError(e);
	        }
			
			try {
				System.out.println("doPersistence = " + doPersistence);
				if (doPersistence){
				    TreeDao tdao = new TreeDao(tree);
			    	tdao.setup();
			    	tdao.saveTree();
					tdao.close(true);
				}
			} catch (Exception e) {
				logger.logException(e);
			} catch (Error e)  {
				logger.logError(e);
	        }

			logger.logMessage("LDAPHandler ended at " + new Date());
		} catch (IOException e){
			e.printStackTrace();
		}
	}
}
