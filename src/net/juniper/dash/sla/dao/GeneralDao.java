package net.juniper.dash.sla.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import net.juniper.dash.sla.conf.SLAProperties;

public abstract class GeneralDao {
	
	protected Connection connect = null;
	protected PreparedStatement preparedStatement = null;
	protected ResultSet resultSet = null;
	
	protected final String ORACLE_DASH_URL = SLAProperties.getProperty("ORACLE_DASH_URL") + "/" + SLAProperties.getProperty("ORACLE_DASH_SERVICE_NAME") ;
	protected final String ORACLE_DASH_USER = SLAProperties.getProperty("ORACLE_DASH_USER");
	protected final String ORACLE_DASH_PASSWD = SLAProperties.getProperty("ORACLE_DASH_PASSWD");
	
	protected final String ORACLE_GNATS_URL = SLAProperties.getProperty("ORACLE_GNATS_URL")+ "/" + SLAProperties.getProperty("ORACLE_GNATS_SERVICE_NAME") ;
	protected final String ORACLE_GNATS_USER = SLAProperties.getProperty("ORACLE_GNATS_USER");
	protected final String ORACLE_GNATS_PASSWD = SLAProperties.getProperty("ORACLE_GNATS_PASSWD");
	
    public void setup() throws ClassNotFoundException, SQLException {
    	Class.forName("oracle.jdbc.driver.OracleDriver");
    }
    
    protected void buildStatement(String sql) throws SQLException {
    	preparedStatement = connect.prepareStatement(sql);
    }
    
    protected void buildStatement(PreparedStatement pstmt, String sql) throws SQLException {
    	pstmt = connect.prepareStatement(sql);
    }
    
	public void close(boolean finish) throws SQLException {
		if (resultSet != null) {
			resultSet.close();
		}

		if (preparedStatement != null) {
			preparedStatement.close();
		}

		if (connect != null && finish) {
			connect.commit();
			connect.close();
		}
	}
}
