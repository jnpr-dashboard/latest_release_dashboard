package net.juniper.dash.sla.dao;

import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Map;

import net.juniper.dash.sla.bean.Org;

public class TreeDao extends GeneralDao {
	
	private final String POPULATE_ORGCHART = "insert into ORGCHART (ORG_ID, SUPER_ID, LEAF) values (?, ?, ?)";
	private final String POPULATE_SLA = "insert into SLA (ORG_ID, ACK_TIME, ACK_TIME_LINK, ASSIGN_TIME, ASSIGN_TIME_LINK, PCT_INFO_STAT, PCT_INFO_STAT_LINK, INFO_TIME, INFO_TIME_LINK, INFO_STAT, INFO_STAT_LINK, VERIFY_TIME, VERIFY_TIME_LINK) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
	
    private final String DELETE_SLA = "delete from SLA";
    private final String DELETE_ORGCHART = "delete from ORGCHART";
	
	private Map<String, Org> tree;
	
	public TreeDao(Map<String, Org> tree) {
		this.tree = tree;
	}
	
	public void removeAllFromSLA() throws SQLException {
	    buildStatement(DELETE_SLA);
	    writeDataBase(); 
		close(false);  	  	
    }
	
	public void removeAllFromOrgChart() throws SQLException {
	    buildStatement(DELETE_ORGCHART);
	    writeDataBase(); 
		close(false);  	  	
    }
	
	public void storeTree() throws SQLException {
		Org u;
		PreparedStatement pstmt = connect.prepareStatement(POPULATE_ORGCHART);
		PreparedStatement pstmt2 = connect.prepareStatement(POPULATE_SLA);
    	for (String k : tree.keySet()){
    		u = tree.get(k);
    		prepareOrgChart(pstmt, u.getOrgId(), u.getSuperId(), u.isLeaf() ? 1 : 0); 
    		if (u.getAverageAckTime() == 0 && u.getAckTimeLink().isEmpty() && 
    		    u.getAverageAssignTime() == 0 && u.getAssignTimeLink().isEmpty() &&
    		    u.getPctInfoStat() == 0 && u.getPctInfoStatLink().isEmpty() &&
    		    u.getAverageInfoTime() == 0 && u.getInfoTimeLink().isEmpty() &&
    		    u.getAverageInfoStat() == 0 && u.getInfoStatLink().isEmpty() &&
    		    u.getAverageVerifyTime() == 0 && u.getVerifyTimeLink().isEmpty()){
    			continue;
    	     } else {
    		    prepareSLA(pstmt2,u.getOrgId(),
    				     Double.toString(u.getAverageAckTime()), u.getAckTimeLink(), 
    				     Double.toString(u.getAverageAssignTime()), u.getAssignTimeLink(),
    				     Double.toString(u.getPctInfoStat()), u.getPctInfoStatLink(), 
    				     Double.toString(u.getAverageInfoTime()), u.getInfoTimeLink(),
    				     Double.toString(u.getAverageInfoStat()), u.getInfoStatLink(),
    				     Double.toString(u.getAverageVerifyTime()), u.getVerifyTimeLink());
    	     }
    	} 
    	pstmt.executeBatch();
    	pstmt.close();
    	pstmt2.executeBatch();
    	pstmt2.close();
    }
	
	
    private void prepareOrgChart(PreparedStatement pstmt, String s1, String s2, int leaf)  throws SQLException {
    	pstmt.setString(1, s1);	
    	pstmt.setString(2, s2);	
    	pstmt.setInt(3, leaf);
    	pstmt.addBatch();
	}
    
    private void prepareSLA(PreparedStatement pstmt, String s1, String s2, String s3, String s4, String s5, String s6, String s7, 
            String s8, String s9, String s10, String s11, String s12, String s13) throws SQLException {
    	pstmt.setString(1, s1);	
    	pstmt.setString(2, s2);	
    	pstmt.setString(3, s3); 
    	pstmt.setString(4, s4);	
    	pstmt.setString(5, s5);
    	pstmt.setString(6, s6);  
    	pstmt.setString(7, s7);
    	pstmt.setString(8, s8); 
    	pstmt.setString(9, s9);
    	pstmt.setString(10, s10);
    	pstmt.setString(11, s11); 
    	pstmt.setString(12, s12);
    	pstmt.setString(13, s13);
    	pstmt.addBatch();
    } 
    
    private void writeDataBase() throws SQLException {
		preparedStatement.executeUpdate();
		
    }
    
    @Override
    public void setup() throws ClassNotFoundException, SQLException {
    	super.setup();
    	connect = DriverManager.getConnection(ORACLE_DASH_URL, ORACLE_DASH_USER, ORACLE_DASH_PASSWD);
    	connect.setAutoCommit(false);
    }
    
    /*
     * Store LDAP data to an extra table ORGCHART1
     */
     
    private final String POPULATE_ORGCHART1 = "insert into ORGCHART1 (ORG_ID, SUPER_ID, LEAF) values (?, ?, ?)";
    
    public void saveTree() throws SQLException {
    	Org u;
		PreparedStatement pstmt = connect.prepareStatement(POPULATE_ORGCHART1);
    	for (String k : tree.keySet()){
    		u = tree.get(k);
    		prepareOrgChart(pstmt, u.getOrgId(), u.getSuperId(), u.isLeaf() ? 1 : 0);    
    	} 
    	pstmt.executeBatch();
    	pstmt.close();
    }
    

}
