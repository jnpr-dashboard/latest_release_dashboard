/**
 * allenyu@juniper.net, Dec 8, 2011
 *
 * Copyright (c) 2011, Juniper Networks, Inc.
 * All rights reserved.
 */
package net.juniper.dash.sla.dao;

import java.util.Hashtable;
import java.util.Set;
import java.util.HashSet;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.BasicAttribute;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;

import net.juniper.dash.sla.conf.Constants;
import net.juniper.dash.sla.conf.SLAProperties;

public class LDAPDao {

	private static Hashtable env;
    private static DirContext ctx;
    private static SearchControls controls;
    
    public static void initilize() throws RuntimeException, NamingException {
    	env = new Hashtable();
    	env.put(Context.INITIAL_CONTEXT_FACTORY,"com.sun.jndi.ldap.LdapCtxFactory");
        env.put(Context.PROVIDER_URL, SLAProperties.getProperty("LDAP_URL"));
        ctx = new InitialDirContext(env);
        controls = new SearchControls();
        controls.setSearchScope(SearchControls.SUBTREE_SCOPE);
    }
    
    public static Set<String> getBU(String buname) throws NamingException {
    	Set <String> set = new HashSet();
    	SearchResult searchResult;
    	Attributes attrs;
    	Attribute attr;
    	Object o;
    	String member;
    	NamingEnumeration results, enum0;
    	results = ctx.search("", SLAProperties.getProperty("LDAP_SEARCH_BU").replace("BUNAME", buname), controls);
        while (results.hasMore()) {
            searchResult = (SearchResult) results.next();
            attrs = searchResult.getAttributes();
            attr = attrs.get(SLAProperties.getProperty("LDAP_SEARCH_BU_ATTR"));
            enum0 = ((BasicAttribute)attr).getAll();
            while(enum0.hasMore()) {
            	o = enum0.next();
            	if (o != null ) {
            		member = o.toString().split(Constants.COMMA)[0];
            		if (member.length() >= 5){
            			member = member.substring(4).toLowerCase();
            			if (member.matches("[a-z]+")) {
            				set.add(member);
                    	        }
            		}
            	}
            }
        }
        return set;
    }
    
    public static Set<String> getReports(String manager) throws NamingException {
    	SearchResult searchResult;
    	Attributes attrs;
    	Attribute attr;
    	Object o;
    	Set<String> set = new HashSet();
    	String name;
    	NamingEnumeration results, enum0;
    	results = ctx.search("", SLAProperties.getProperty("LDAP_SEARCH_REPORTS").replace("MANAGER", manager), controls);
    	while (results.hasMore()) {
            searchResult = (SearchResult) results.next();
            attrs = searchResult.getAttributes();
            attr = attrs.get(SLAProperties.getProperty("LDAP_SEARCH_REPORTS_ATTR"));
            enum0 = ((BasicAttribute)attr).getAll();
            while(enum0.hasMore()) {
            	o = enum0.next();
            	if (o != null ) {
            	    name = o.toString().toLowerCase();
                    if (!manager.equals(name) && name.matches("[a-z]+")) {
                	    set.add(name);
                    }     
            	}
            }
    	}
    	return set;
    }
}

