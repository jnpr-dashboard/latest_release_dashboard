/**
 * allenyu@juniper.net, Oct 26, 2011
 *
 * Copyright (c) 2011, Juniper Networks, Inc.
 * All rights reserved.
 */
package net.juniper.dash.sla.dao;

import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Set;
import java.util.HashSet;


import net.juniper.dash.sla.conf.Constants;
import net.juniper.dash.sla.conf.SLAProperties;
import net.juniper.dash.sla.dao.GeneralDao;

public class GnatsDao extends GeneralDao {
	
	private String QUERY_ACK_TIME;
	private String QUERY_ASSIGN_TIME;
	private String QUERY_PCT_INFO_STAT;
	private String QUERY_INFO_TIME_STAT;
	private String QUERY_VERIFY_TIME;
	
	private Set<String> set;
	
	public GnatsDao(){
		set = new HashSet();
		QUERY_ACK_TIME = SLAProperties.getProperty("QUERY_ACK_TIME")
				.replace(Constants.START_DATE, SLAProperties.getProperty("SQL_START_DATE"))
				.replace(Constants.ARRIVAL_DATE, SLAProperties.getProperty("SQL_ARRIVAL_DATE"));
		QUERY_ASSIGN_TIME = SLAProperties.getProperty("QUERY_ASSIGN_TIME")
				.replace(Constants.START_DATE, SLAProperties.getProperty("SQL_START_DATE"))
				.replace(Constants.ARRIVAL_DATE, SLAProperties.getProperty("SQL_ARRIVAL_DATE"));
		QUERY_PCT_INFO_STAT = SLAProperties.getProperty("QUERY_PCT_INFO_STAT")
				.replace(Constants.START_DATE, SLAProperties.getProperty("SQL_START_DATE"))
				.replace(Constants.ARRIVAL_DATE, SLAProperties.getProperty("SQL_ARRIVAL_DATE"));
		QUERY_INFO_TIME_STAT = SLAProperties.getProperty("QUERY_INFO_TIME_STAT")
				.replace(Constants.START_DATE, SLAProperties.getProperty("SQL_START_DATE"))
				.replace(Constants.ARRIVAL_DATE, SLAProperties.getProperty("SQL_ARRIVAL_DATE"));
		QUERY_VERIFY_TIME = SLAProperties.getProperty("QUERY_VERIFY_TIME")
				.replace(Constants.START_DATE, SLAProperties.getProperty("SQL_START_DATE"))
				.replace(Constants.ARRIVAL_DATE, SLAProperties.getProperty("SQL_ARRIVAL_DATE"));
	}
	
	public Set<String> getAckTime() throws SQLException {
		buildStatement(QUERY_ACK_TIME);
		fetchResultSet();
		return set;
	}
	
	public Set<String> getAssignTime() throws SQLException {
		buildStatement(QUERY_ASSIGN_TIME);
		fetchResultSet();
		return set;
	}
	
	public Set<String> getPctInfoStat() throws SQLException {
		buildStatement(QUERY_PCT_INFO_STAT);
		fetchResultSet();
		return set;			
	}
	
	public Set<String> getInfoTimeStat() throws SQLException {
		buildStatement(QUERY_INFO_TIME_STAT);
		fetchResultSet();
		return set;			
	}
	
	public Set<String> getVerifyTime() throws SQLException {
		buildStatement(QUERY_VERIFY_TIME);
		fetchResultSet();
		return set;
	}
	
	private void fetchResultSet() throws SQLException {
		set.clear();
		preparedStatement.setQueryTimeout(Integer.parseInt(SLAProperties.getProperty("ORACLE_GNATS_QUERY_TIMEOUT")));
		preparedStatement.setFetchSize(Integer.parseInt(SLAProperties.getProperty("ORACLE_GNATS_RESULSET_FETCH_SIZE")));
		resultSet = preparedStatement.executeQuery();
		int NumOfCol=resultSet.getMetaData().getColumnCount();
		StringBuilder builder = new StringBuilder();
		String str;
		while (resultSet.next()){
			for (int i=1; i<=NumOfCol; i++){
				str = resultSet.getString(i);
				if (str == null || str.equals(Constants.NULL)){
					str = Constants.EMPTY;
				}
				builder.append(str);
			    builder.append(Constants.COLON);
			}
			str = builder.toString();
			set.add(str.substring(0, str.lastIndexOf(Constants.COLON)));
			builder.delete(0, builder.length());
		}
		close(false);
	}
	
    @Override
    public void setup() throws ClassNotFoundException, SQLException  {
        super.setup();
        connect = DriverManager.getConnection(ORACLE_GNATS_URL, ORACLE_GNATS_USER, ORACLE_GNATS_PASSWD);
		connect.setAutoCommit(false);
    }
}
