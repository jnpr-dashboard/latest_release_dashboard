package net.juniper.dash.sla.bean;

import java.util.Set;
import java.util.HashSet;

import net.juniper.dash.sla.conf.Constants;

public class Org {
	
	 private String orgId;
	 private String superId;
	 private boolean isLeaf;
	 
	 private double  ackTime;
	 private double  ackTimePrs;
	 private Set<String> ackTimePrsList;
	 
	 private double  assignTime;
	 private double  assignTimePrs;
	 private Set<String> assignTimePrsList;
	 
	 private double  pctInfoStatPrsA;
	 private double  pctInfoStatPrsB;
	 private Set<String> pctInfoStatPrsBList;
	 private Set<String> pctInfoStatPrsAList;
	 
	 private double  infoTime;
	 private double  infoTimePrs;
	 private Set<String> infoTimePrsList;
	 
	 private double  infoStat;
	 private double  infoStatPrs;
	 private Set<String> infoStatPrsList;
	 
	 private double  verifyTime;
	 private double  verifyTimePrs;
	 private Set<String> verifyTimePrsList;

     public Org(){
    	 ackTimePrsList = new HashSet<String>();
    	 assignTimePrsList = new HashSet<String>();
    	 pctInfoStatPrsBList = new HashSet<String>();
    	 pctInfoStatPrsAList = new HashSet<String>();
    	 infoTimePrsList = new HashSet<String>();
    	 infoStatPrsList = new HashSet<String>();
    	 verifyTimePrsList = new HashSet<String>();
     }
	 
	 public String getOrgId() {
	     return orgId;
	 }

	 public void setOrgId(String orgId) {
	     this.orgId = orgId;
	 }
	 
	 public String getSuperId() {
	     return superId;
	 }

	 public void setSuperId(String superId) {
	     this.superId = superId;
	 }
	 
	 public double getAckTime() {
	     return ackTime;
	 }

	 public void setAckTime(double ackTime) {
	     this.ackTime = ackTime;
	 }
	 
	 public double getAckTimePrs() {
	     return ackTimePrs;
	 }
	 
	 public void setAckTimePrs(double ackTimePrs) {
	     this.ackTimePrs = ackTimePrs;
	 }
	 
	 public double getAverageAckTime() {
	     return (ackTimePrs==0 ? 0 : (double)ackTime/ackTimePrs);
	 }
	 
	 public Set<String> getAckTimePrsList() {
	     return ackTimePrsList;
	 }
	 
	 public void setAckTimePrsList(Set set) {
	     this.ackTimePrsList.addAll(set);
	 }
	 
	 public String getAckTimeLink() {
		 return getLink(ackTimePrsList, "-1");	
	 }
	 
	 public double getAssignTime() {
	     return assignTime;
	 }

	 public void setAssignTime(double assignTime) {
	     this.assignTime = assignTime;
	 }
	 
	 public void setAssignTimePrs(double assignTimePrs) {
	     this.assignTimePrs = assignTimePrs;
	 }
	 
	 public double getAssignTimePrs() {
	     return assignTimePrs;
	 }
	 
	 public double getAverageAssignTime() {
	     return (assignTimePrs == 0 ? 0 : (double)assignTime/assignTimePrs);
	 }
	 
	 public Set<String> getAssignTimePrsList() {
	     return assignTimePrsList;
	 }
	 
	 public void setAssignTimePrsList(Set set) {
	     this.assignTimePrsList.addAll(set);
	 }
	 
	 public String getAssignTimeLink() {
		 return getLink(assignTimePrsList, "-1");	
	 }
	 
	 public void setPctInfoStatPrsA(double pctInfoStatPrsA) {
	     this.pctInfoStatPrsA = pctInfoStatPrsA;
	 }
	 
	 public double getPctInfoStatPrsA() {
	     return pctInfoStatPrsA;
	 }
	 
	 public void setPctInfoStatPrsB(double pctInfoStatPrsB) {
	     this.pctInfoStatPrsB = pctInfoStatPrsB;
	 }
	 
	 public double getPctInfoStatPrsB() {
	     return pctInfoStatPrsB;
	 }
	 
	 public double getPctInfoStat() {
		 int asize = pctInfoStatPrsAList.size();
	     return (asize == 0 ? 0 : (double)100 * pctInfoStatPrsBList.size()/asize);
	 }
	 
	 public void setPctInfoStatPrsBList(Set set) {
	     this.pctInfoStatPrsBList.addAll(set);
	 }
	 
	 public Set<String> getPctInfoStatPrsBList() {
	     return pctInfoStatPrsBList;
	 }
	 
	 public void setPctInfoStatPrsAList(Set set) {
	     this.pctInfoStatPrsAList.addAll(set);
	 }
	 
	 public Set<String> getPctInfoStatPrsAList() {
	     return pctInfoStatPrsAList;
	 }
	 
	 public String getPctInfoStatLink() {
		  return getLink(pctInfoStatPrsBList, null);	
	 }
	 
	 public double getInfoTime() {
	     return infoTime;
	 }

	 public double getInfoTimePrs() {
	     return infoTimePrs;
	 }
	 
	 public void setInfoTime(double infoTime) {
	     this.infoTime = infoTime;
	 }
	 
	 public void setInfoTimePrs(double infoTimePrs) {
	     this.infoTimePrs = infoTimePrs;
	 }
	 
	 public double getAverageInfoTime() {
	     return (infoTimePrs == 0 ? 0: (double)infoTime/infoTimePrs);
	 }
	 
	 public Set<String> getInfoTimePrsList() {
	     return infoTimePrsList;
	 }
	 
	 public void setInfoTimePrsList(Set set) {
	     this.infoTimePrsList.addAll(set);
	 }
	 
	 public String getInfoTimeLink() {
		 return getLink(infoTimePrsList, null);	
	 }
	 
	 public double getInfoStat() {
	     return infoStat;
	 }

	 public void setInfoStat(double infoStat) {
	     this.infoStat = infoStat;
	 }
	 
	 public void setInfoStatPrs(double infoStatPrs) {
	     this.infoStatPrs = infoStatPrs;
	 }
	 
	 public double getInfoStatPrs() {
	     return infoStatPrs;
	 }
	 
	 public double getAverageInfoStat() {
	     return (infoStatPrs == 0 ? 0 : (double)infoStat/infoStatPrs);
	 }
	 
	 public Set<String> getInfoStatPrsList() {
	     return infoStatPrsList;
	 }
	 
	 public void setInfoStatPrsList(Set set) {
	     this.infoStatPrsList.addAll(set);
	 }
	 
	 public String getInfoStatLink() {
		 return getLink(infoStatPrsList, null);	
	 }
	 
	 public double getVerifyTime() {
	     return verifyTime;
	 }

	 public void setVerifyTime(double verifyTime) {
	     this.verifyTime = verifyTime;
	 }
	 
	 public void setVerifyTimePrs(double verifyTimePrs) {
	     this.verifyTimePrs = verifyTimePrs;
	 }
	 
	 public double getVerifyTimePrs() {
	     return verifyTimePrs;
	 }
	 
	 public double getAverageVerifyTime() {
	     return (verifyTimePrs == 0 ? 0 : (double)verifyTime/verifyTimePrs);
	 }
	 
	 public Set<String> getVerifyTimePrsList() {
	     return verifyTimePrsList;
	 }
	 
	 public void setVerifyTimePrsList(Set set) {
	     this.verifyTimePrsList.addAll(set);
	 }
	 
	 public String getVerifyTimeLink() {
		 return getLink(verifyTimePrsList, null);	
	 }
	 
	 public String getLink(Set<String> set, String scp) {
		 StringBuilder bd = new StringBuilder();
		 if (scp != null){
			 for (String s: set){
				 bd.append(s);
				 bd.append(scp);
				 bd.append(Constants.COMMA);
			 }
		 } else {
			 for (String s: set){
				 bd.append(s);
				 bd.append(Constants.COMMA);
			 }
		 }
		 String prs = bd.toString();
		 String link = "";
		 if (prs != null && !Constants.EMPTY.equals(prs)){
	        link = prs.substring(0, prs.length()-1);
		 } 
		 return link;
	 }
	  
	 public boolean isLeaf() {
	     return isLeaf;
	 }

	 public void setLeaf(boolean isLeaf) {
	     this.isLeaf = isLeaf;
	 }
}

