/**
 * allenyu@juniper.net, Dec 9, 2011
 *
 * Copyright (c) 2011, Juniper Networks, Inc.
 * All rights reserved.
 */
package net.juniper.dash.sla.bean;

import java.util.Set;
import java.util.HashSet;

public class BU {
	
	private String name;
	private Set<String> members;
	
	public BU(){
		members = new HashSet();
	}
	
	public void setName(String name){
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	public void setMembers(Set<String> members){
		this.members.addAll(members);
	}
	
	public void setMembers(String member){
		this.members.add(member);
	}
	
	public Set<String> getMembers() {
		return members;
	}
}
