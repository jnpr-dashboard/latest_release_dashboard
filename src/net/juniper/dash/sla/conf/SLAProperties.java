/**
 * allenyu@juniper.net, Oct 18, 2011
 *
 * Copyright (c) 2011, Juniper Networks, Inc.
 * All rights reserved.
 */
package net.juniper.dash.sla.conf;

import java.util.Properties;
import java.io.FileInputStream;
import java.io.IOException;

import net.juniper.dash.sla.conf.Constants;

public class SLAProperties {
	
	private static Properties prop;
	private static Properties ldapprop;
	
	public static void initialize() throws IOException {
		FileInputStream fis = new FileInputStream(Constants.SLA_PROPS);
		prop = new Properties();
	    prop.load(fis);	    
	    FileInputStream fis2 = new FileInputStream(prop.getProperty("DASHBOARD_PATH") + Constants.SLASH + prop.getProperty("SPECIAL_DATA_PATH") + Constants.SLASH + prop.getProperty("LDAP_PROPS"));
	    ldapprop = new Properties();
	    ldapprop.load(fis2);
	    prop.setProperty("LDAP_URL", ldapprop.getProperty("url"));
	}
	
	public static String getProperty(String k){		
		return prop.getProperty(k);		
	}
}
