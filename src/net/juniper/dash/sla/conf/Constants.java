/**
 * allenyu@juniper.net, Oct 19, 2011
 *
 * Copyright (c) 2011, Juniper Networks, Inc.
 * All rights reserved.
 */
package net.juniper.dash.sla.conf;

public class Constants {

	public static final String COLON = ":";
	public static final String COMMA = ",";
	public static final String SEMICOLON = ";";
	public static final String SPACE = " ";
	public static final String HYPHEN = "-";
	public static final String SLASH = "/";
	public static final String EMPTY = "";
	public static final String NULL = "null";
	
	public static final String ROOT = "juniper";
	
	public static final String START_DATE = "$start_date";
	public static final String ARRIVAL_DATE = "$arrival_date";
	
	public static final String SLA_PROPS = "/opt/dashboard/special/sla.properties";
}
