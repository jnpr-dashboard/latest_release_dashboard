/**
 * allenyu@juniper.net, Oct 20, 2011
 *
 * Copyright (c) 2011, Juniper Networks, Inc.
 * All rights reserved.
 */
package net.juniper.dash.sla;

import java.util.Date;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.io.IOException;
import java.sql.SQLException;
import javax.naming.NamingException;

import net.juniper.dash.sla.conf.Constants;
import net.juniper.dash.sla.util.SLALogger;
import net.juniper.dash.sla.conf.SLAProperties;
import net.juniper.dash.sla.bean.Org;
import net.juniper.dash.sla.dao.GnatsDao;
import net.juniper.dash.sla.dao.TreeDao;
import net.juniper.dash.sla.proc.TreeBuilder;
import net.juniper.dash.sla.proc.GnatsSQLProcessor;
import net.juniper.dash.sla.proc.LDAPOrgChartProcessor;

public class SLARunner {
	
	private TreeBuilder tbuilder;
	
	private boolean doPersistence = true;
	
	public void setDoPersistence(boolean doPersistence){
		this.doPersistence = doPersistence;
	}
	
	private void buildTreeByLDAP() throws IOException, NamingException {
		LDAPOrgChartProcessor lop = new LDAPOrgChartProcessor();
		lop.parseBGList();
		lop.fetchBUs();
		tbuilder = new TreeBuilder();
		tbuilder.setProduct(SLAProperties.getProperty("PRODUCT"));
		tbuilder.setBGBUs(lop.getBGBUs());
		tbuilder.setBUManagers(lop.getBUManagers());
		tbuilder.setBUOrgChart(lop.getBUOrgChart());
		tbuilder.buildTree(SLAProperties.getProperty("PRODUCT"));
	}
	
	private void queryGnats(){
		
		String[] jobs = SLAProperties.getProperty("SLAJOBS").split(Constants.SPACE);
		ExecutorService executor = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
		for (String job : jobs) {
			executor.execute(new GnatsSQLProcessor(this, job, tbuilder.getTree(), new GnatsDao()));
		}
		executor.shutdown();
		while (!executor.isTerminated()) {};		
	}
	
	private void persistData() throws SQLException, ClassNotFoundException{
		TreeDao tdao = new TreeDao(tbuilder.getTree());
		tdao.setup();
		tdao.removeAllFromSLA();

		tdao.removeAllFromOrgChart();
		tdao.storeTree();
		tdao.close(true); 
	}
	
	public static void main(String[] args){
		try {
		    SLAProperties.initialize();
		    SLALogger logger = SLALogger.getInstance();
		    logger.logMessage("SLARunner started at " + new Date());
			SLARunner sr = new SLARunner();
			try {
				sr.buildTreeByLDAP();
				System.out.println("Tree building done");
				sr.queryGnats();
				System.out.println("Gnats querying done");
				sr.tbuilder.sumUp(sr.tbuilder.getTree().get(Constants.ROOT));
				Org o = sr.tbuilder.getTree().get(Constants.ROOT);
				System.out.println(o.getAverageAckTime());
				System.out.println(o.getAverageAssignTime());
				System.out.println(o.getPctInfoStat());
				System.out.println(o.getAverageInfoTime());
				System.out.println(o.getAverageInfoStat());
				System.out.println(o.getAverageVerifyTime());
			} catch (Exception e) {
				sr.doPersistence = false;
				logger.logException(e);
			} catch (Error e)  {
				sr.doPersistence = false;
				logger.logError(e);
	        }
			
			try {
				System.out.println("doPersistence = " + sr.doPersistence);
				if (sr.doPersistence){
				    sr.persistData();
				}
			} catch (Exception e) {
				logger.logException(e);
			} catch (Error e)  {
				logger.logError(e);
	        }
			logger.logMessage("SLARunner ended at " + new Date());
		} catch (IOException e){
			e.printStackTrace();
		}
	}
}
