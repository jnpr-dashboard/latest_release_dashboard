/**
 * dbergstr@juniper.net, Mar 5, 2009
 *
 * Copyright (c) 2009, Juniper Networks, Inc.
 * All rights reserved.
 */
package net.juniper.dash;


/**
 * Indicates that the object's records/members can be serialized as JSON.  
 *
 * @author dbergstr
 */
public interface JSONSerializable {
    
    public String serializeRecords();
}
