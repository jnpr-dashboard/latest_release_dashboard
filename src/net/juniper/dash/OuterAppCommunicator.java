/**
 * allenyu@juniper.net, Mar 9, 2012
 *
 * Copyright (c) 2012, Juniper Netgrouping_names, Inc.
 * All rights reserved.
 */
package net.juniper.dash;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.HashSet;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.lang.reflect.Method;

import java.net.URL;
import java.net.HttpURLConnection;

import org.apache.log4j.Logger;

import net.juniper.dash.Repository;

public class OuterAppCommunicator implements Runnable {
private Class cls;
private String clsname;
private String job;
private Map<String, Object> task;

static Logger logger;
private String errmsg = "Error " + clsname + " for job = ";

private String space = " ";

public OuterAppCommunicator(Map<String, Object> task){
cls = this.getClass();
logger = Logger.getLogger(cls.getName());
clsname = cls.getName();
this.task = task;
this.job = (String)this.task.get("job");
}

@Override
public void run() {
try {
Method method = cls.getDeclaredMethod(job);
method.invoke(this);
    } catch (Exception e) {
     errmsg += job + " " + e.getMessage();
     logger.error(errmsg);
            Repository.logError(errmsg);
    } catch (Error e)  {
     errmsg += job + " " + e.getMessage();
     logger.error(errmsg);
            Repository.logError(errmsg);
    } 
}

	private void getUsers() throws Exception {
		
		logger.debug(clsname + " for job " + job + " starts");
		Set<String> users = (Set<String>)task.get("users");
		
		try {	
			String ops = "query-pr -v dashboard --adm-field " + (String)task.get("search-field") + " --adm-subfield username";
			String line;
			String[] cmd = ops.split(space);
	        Process p = Runtime.getRuntime().exec(cmd);
	        BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));
	        while ((line = input.readLine()) != null) {
	        	users.add(line.trim());
	        }
	        input.close();			
	    } catch (Exception e){
		    logger.debug(clsname + " for job " + job + "  " + e.toString());
		    throw e;
	    }
		
		logger.debug(clsname + " for job " + job + " ends");
	}

    private void populateAliasMap() throws Exception {
        logger.debug(clsname + " for job " + job + " starts");
        Set<String> possibleAliases = (Set<String>)task.get("possibleAliases");
        String[] possibleAliasesArray = (String[])possibleAliases.toArray(new String[possibleAliases.size()]);
        Map<String, Set<String>> userAliasMap = (Map<String, Set<String>>)task.get("userAliasMap");
        Map<String, Set<String>> aliasUserMap = (Map<String, Set<String>>)task.get("aliasUserMap");

        Set<String> argLines = new HashSet<String>();      
        StringBuilder args = new StringBuilder();
        URL url;
        HttpURLConnection conn;
        BufferedReader rd;
        String line, header, alias, uidline, user;
        int idx;
        String[] uids;
        Set<String> lines;
        Set<String> aliasesSet, usersSet;

        for (int i = 0; i < possibleAliasesArray.length; i++){
            if (null==possibleAliasesArray[i] || "".equals(possibleAliasesArray[i]))
                continue;
            args.append("&operand=");
            args.append(possibleAliasesArray[i]);
            if (i != 0 && i % 99 == 0 || i == possibleAliasesArray.length - 1){
                argLines.add(args.toString());
                args = new StringBuilder();
            }
        }
        
        try {
            for (String arg : argLines){
                url = new URL((String)task.get("aliases_search_url") + arg);
                conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("GET");
                rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                lines = new HashSet<String>();
                while ((line = rd.readLine()) != null) {
                    lines.add(line);
                }
                rd.close();
        
                for (String outLine : lines){
                    if (outLine == null || "".equals(outLine) || outLine.trim().endsWith(" is") || outLine.startsWith("WARNING"))
                        continue;
                    idx = outLine.indexOf("  is");
                    header = outLine.substring(0, idx);
                    alias = header.split(space)[1].trim();
                    uidline = outLine.substring(idx + 4);
    
                    usersSet = aliasUserMap.get(alias);
                    if (usersSet == null){
                        usersSet = new HashSet<String>();
                        aliasUserMap.put(alias, usersSet);
                    }
    
                    uids = uidline.split(",");
                    for (String uid : uids){
                        user = uid.trim();
                        if (user.equals(alias)) {
                            aliasUserMap.remove(alias);
                            continue;
                        }
                        aliasesSet = userAliasMap.get(user);
                        if (aliasesSet == null){
                            aliasesSet = new HashSet<String>();
                            userAliasMap.put(user, aliasesSet);
                        }
                        aliasesSet.add(alias);
                        usersSet.add(user);
                   }
               }
           }
       } catch (Exception e){
           logger.debug(clsname + " for job " + job + "  " + e.toString());
           throw e;
       }
       logger.debug(clsname + " for job " + job + " ends");
    }
    
	private void getOutInvocationResult() throws Exception {
		
		logger.debug(clsname + " for job " + job + " starts");
		List<String[]> output = (List<String[]>)task.get("output");
		List<String> cmd = (List<String>)task.get("cmd");
		logger.debug(clsname + " for job " + job + " cmd= " + cmd.toString());
		try {	
			
			String line;
	        Process p = Runtime.getRuntime().exec(cmd.toArray(new String[cmd.size()]));
	        BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));
	        while ((line = input.readLine()) != null) {
	        	output.add(line.trim().split(":"));
	        }
	        input.close();			
	    } catch (Exception e){
		    logger.debug(clsname + " for job " + job + "  " + e.toString());
		    throw e;
	    }
		
		logger.debug(clsname + " for job " + job + " ends");
	}
}
