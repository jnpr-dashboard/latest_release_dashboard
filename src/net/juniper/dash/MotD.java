/**
 * dbergstr@juniper.net, Feb 2, 2008
 *
 * Copyright (c) 2008, Juniper Networks, Inc.
 * All rights reserved.
 */
package net.juniper.dash;

/**
 * Simple Message of the Day container.
 *
 * @author dbergstr
 */
public class MotD {
    public String text = "";

    public String body = "";

    public MotD() {
        super();
    }
}
