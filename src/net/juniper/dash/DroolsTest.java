package net.juniper.dash;

import java.io.InputStreamReader;
import java.io.Reader;
import java.lang.Thread.UncaughtExceptionHandler;
import java.util.Date;

import org.drools.RuleBase;
import org.drools.RuleBaseFactory;
import org.drools.compiler.PackageBuilder;
import org.drools.compiler.PackageBuilderConfiguration;
import org.drools.rule.Package;
import org.drools.rule.builder.dialect.java.JavaDialectConfiguration;

/**
 * This is a sample file to launch a rule package from a rule source file.
 */
public class DroolsTest {

    public static final void main(String[] args) {
        try {
            System.out.println("Starting at " + new Date());
            Repository.initialize("DroolsTest");
            System.out.println("\n\n>>> Repository initialized at " + new Date() +
                "\n                         ...main() now sleeping.\n\n");
            Thread.sleep(10000 * 600);
            Repository.shutdown();
            System.out.println(">>> Done at " + new Date());
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    private static class MyThread extends Thread {

        private boolean wakeup = false;

        public void setwake() {
            wakeup = true;
            interrupt();
        }

        public void run() {
            System.out.println("starting");
            try {
                Thread.sleep(2000);
//              System.out.println("napping");
//              Thread.sleep(1000);
            } catch (InterruptedException e) {
                if (wakeup) {
                    System.out.println("I'm awake!");
                } else {
                    System.out.println("Killed");
                    throw new RuntimeException("I was brutally murdered");
                }
            }
//            System.out.println("throwing");
//            throw new RuntimeException("Whee!");
        }
    }

    private static void threadTest() {
        MyThread t = new MyThread();
        t.setDaemon(false);
        UncaughtExceptionHandler eh = new Thread.UncaughtExceptionHandler() {
            public void uncaughtException(Thread t, Throwable e) {
                System.out.println("Caught ex " + e.getMessage() +
                    " in thread " + t.getName());
            }
        };
        t.setUncaughtExceptionHandler(eh);
        t.start();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {}
        System.out.println("interrupting.");
        t.setwake();
//        t.interrupt();
        System.out.println("done.");
    }

    /**
     * Please note that this is the "low level" rule assembly API.
     */
    private static RuleBase readRule() throws Exception {
        // Use package builder to build up a rule package.
        // An alternative lower level class called "DrlParser" can also be
        // used...
        PackageBuilderConfiguration pkgBuilderCfg =
            new PackageBuilderConfiguration();
        JavaDialectConfiguration javaConf = (JavaDialectConfiguration)
        pkgBuilderCfg.getDialectConfiguration("java");
    javaConf.setCompiler(JavaDialectConfiguration.JANINO);
    javaConf.setJavaLanguageLevel("1.5");
        PackageBuilder builder = new PackageBuilder(pkgBuilderCfg);

        // Read in the DSL.
        Reader dsl = new InputStreamReader(DroolsTest.class
            .getResourceAsStream("/rules/dashboard.dsl"));

        String[] drls = new String[] { "/rules/dashboard.drl" };
        for (String drlFile : drls) {
            Reader drlSource = new InputStreamReader(DroolsTest.class
                .getResourceAsStream(drlFile));
            builder.addPackageFromDrl(drlSource, dsl);
        }

        // Use the following instead of above if you are using a DSL:
        // builder.addPackageFromDrl(source, dsl);

        // get the compiled package (which is serializable)
        Package pkg = builder.getPackage();

        // add the package to a rulebase (deploy the rule package).
        RuleBase ruleBase = RuleBaseFactory.newRuleBase();
        ruleBase.addPackage(pkg);
        return ruleBase;
    }
}
