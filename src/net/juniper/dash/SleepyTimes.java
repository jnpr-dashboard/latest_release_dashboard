/**
 * dbergstr@juniper.net, Nov 10, 2006
 *
 * Copyright (c) 2006, Juniper Networks, Inc.
 * All rights reserved.
 */
package net.juniper.dash;

/**
 * Delays used by various threads, collected here to make tuning easy.
 *
 * @author dbergstr
 */
public enum SleepyTimes {

    // h = hours, m = minutes, s = seconds
    UPDATER_WATCHDOG_SLEEP(9, 'h'), // 9 hours
    WAIT_BEFORE_DATA_REFRESH(15, 'm'), // 15 minutes
    EXTERNAL_RELOADER_TIMEOUT(24, 'm'), // 24 minutes
    EXTERNAL_RELOADER_POLL_TIME(10, 's'), // 10 seconds
    BUFFER_FLUSH_WAIT(200, '\0'), // 200 milliseconds
    WAIT_FOR_THREADS_TO_DIE(10, 's'), // 10 seconds
    DEPENDENCY_WAIT(4, 's'), // 4 seconds
    DEPENDENCY_TIMEOUT(5, 'm'), // 5 minutes
    ;

    /**
     * This allows me to easily reduce delays for testing.
     */
    private static final long MILLIS_PER_SECOND = 1000L;

    private long time;

    /**
     * @param t Delay time in seconds.
     */
    private SleepyTimes(long t, char type) {
        switch (type) {
        case 'h':
            time = (long) t * 60L * 60L * MILLIS_PER_SECOND;
            break;
        case 'm':
            time = (long) t * 60L * MILLIS_PER_SECOND;
            break;
        case 's':
            time = t * MILLIS_PER_SECOND;
            break;
        default:
            time = t;
        }
    }

    public long t() {
        return time;
    }

    /**
     * Adjust the frequency of updates (generally used in testing).
     */
    static void setDataRefreshMinutes(int mins) {
        WAIT_BEFORE_DATA_REFRESH.time = mins * 60 * MILLIS_PER_SECOND;
//        UPDATER_WATCHDOG_SLEEP.time = 4 * WAIT_BEFORE_DATA_REFRESH.time;
    }
}
