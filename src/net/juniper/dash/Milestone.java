/**
 * dbergstr@juniper.net, Nov 30, 2006
 *
 * Copyright (c) 2006-2007, Juniper Networks, Inc.
 * All rights reserved.
 */
package net.juniper.dash;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;

import flexjson.JSONSerializer;

import net.juniper.dash.data.NpiPhased;
import net.juniper.dash.data.Record;
import net.juniper.dash.data.ReleaseRecord;

/**
 * The available Milestones for a Release.
 *
 * @author dbergstr
 */
public enum Milestone {
    // ENUM_NAME(prettyName, isABigWindow, isAnNpiPhase)
    P0_LL_EXIT("P0 LL Exit", false, true),
    IN_DEVELOPMENT("In Development (P0 - P4)", true, true),
    P0_EXIT("P0 Exit", false, true),
    P1_EXIT("P1 Exit", false, true),
    FUNC_SPEC_COMPLETE("Functional Spec Check-In", false, false),
    FUNC_SPEC_APPROVED("Functional Spec Approved", false, false),
    DEV_COMPLETE_ONE("Development Complete One", false, false),
    DEV_COMPLETE_TWO("Development Complete Two", false, false),
    DEV_COMPLETE_THREE("Development Complete Three", false, false),
    P2_EXIT("P2 Exit", false, true),
    THROTTLE_BRANCH("Throttle Branch", false, false),
    P3_EXIT("P3 Exit", false, true),
    NEXT_BUILD("Next Build", false, false),
    NEXT_DEPLOY("Next Deploy", false, false),
    BN_BUILD("Bn Build", false, false),
    BN_DEPLOY("Bn Deploy", false, false),
    B1_BUILD("B1 Build", false, false),
    B1_DEPLOY("B1 Deploy", false, false),
    B2_BUILD("B2 Build", false, false),
    B2_DEPLOY("B2 Deploy", false, false),
    B3_BUILD("B3 Build", false, false),
    B3_DEPLOY("B3 Deploy", false, false),
    P4_EXIT("P4 Exit", false, true),
    RN_BUILD("Rn Build", false, false),
    RN_DEPLOY("Rn Deploy", false, false),
    R1_BUILD("R1 Build", false, false),
    R1_DEPLOY("R1 Deploy", false, false),
    EXTERNAL_SHIP("External Ship", false, false),
    R2_BUILD("R2 Build", false, false),
    R2_DEPLOY("R2 Deploy", false, false),
    R3_BUILD("R3 Build", false, false),
    R3_DEPLOY("R3 Deploy", false, false),
    R4_BUILD("R4 Build", false, false),
    R4_DEPLOY("R4 Deploy", false, false),
    R5_BUILD("R5 Build", false, false),
    R5_DEPLOY("R5 Deploy", false, false),
    R6_BUILD("R6 Build", false, false),
    R6_DEPLOY("R6 Deploy", false, false),
    R7_BUILD("R7 Build", false, false),
    R7_DEPLOY("R7 Deploy", false, false),
    R8_BUILD("R8 Build", false, false),
    R8_DEPLOY("R8 Deploy", false, false),
    NO_MILESTONE("Always Active", true, false);

    private static float MILLIS_PER_DAY = 86400000.0f;

    String prettyName;
    boolean isABigWindow;
    boolean isAnNpiPhase;

    Milestone(String name, boolean window, boolean phased) {
        prettyName = name;
        isABigWindow = window;
        isAnNpiPhase = phased;
    }

    public String toString() {
        return prettyName;
    }

    public float daysUntil(Record grouping) {
        if (this == NO_MILESTONE) {
            // XXX??? What makes sense here?
            return -1;
        }
        long now = System.currentTimeMillis();
        if (this.isAnNpiPhase) {
            if (!(grouping instanceof NpiPhased)) {
                // grouping not applicable to this Milestone
                return 0;
            }
            NpiPhased phased = (NpiPhased) grouping;
            switch (this) {
            case P0_LL_EXIT:
                return (phased.getP0_ll_exit_epoch() - now) / MILLIS_PER_DAY;
            case P0_EXIT:
                return (phased.getP0_exit_epoch() - now) / MILLIS_PER_DAY;
            case P1_EXIT:
                return (phased.getP1_exit_epoch() - now) / MILLIS_PER_DAY;
            case P2_EXIT:
                return (phased.getP2_exit_epoch() - now) / MILLIS_PER_DAY;
            case P3_EXIT:
                return (phased.getP3_exit_epoch() - now) / MILLIS_PER_DAY;
            case P4_EXIT:
                return (phased.getP4_exit_epoch() - now) / MILLIS_PER_DAY;
            case IN_DEVELOPMENT:
                return (phased.getP4_exit_epoch() - now) / MILLIS_PER_DAY;
            }
            throw new AssertionError("Unknown NPI phased milestone: " + this);
        }
        if (grouping instanceof ReleaseRecord) {
            ReleaseRecord rel = (ReleaseRecord) grouping;
            switch (this) {
            case NEXT_BUILD:
                return (findRelevantDate(rel.getBuilds(), now) - now) / MILLIS_PER_DAY;
            case NEXT_DEPLOY:
                return (findRelevantDate(rel.getDeploys(), now) - now) / MILLIS_PER_DAY;
            case BN_DEPLOY:
                return (findRelevantDate(rel.getBetaDeploys(), now) - now) / MILLIS_PER_DAY;
            case B1_DEPLOY:
                return (rel.getB1_deploy_epoch() - now) / MILLIS_PER_DAY;
            case B2_DEPLOY:
                return (rel.getB2_deploy_epoch() - now) / MILLIS_PER_DAY;
            case B3_DEPLOY:
                return (rel.getB3_deploy_epoch() - now) / MILLIS_PER_DAY;
            case BN_BUILD:
                return (findRelevantDate(rel.getBetaBuilds(), now) - now) / MILLIS_PER_DAY;
            case B1_BUILD:
                return (rel.getB1_build_epoch() - now) / MILLIS_PER_DAY;
            case B2_BUILD:
                return (rel.getB2_build_epoch() - now) / MILLIS_PER_DAY;
            case B3_BUILD:
                return (rel.getB3_build_epoch() - now) / MILLIS_PER_DAY;
            case RN_DEPLOY:
                return (findRelevantDate(rel.getReleaseDeploys(), now) - now) / MILLIS_PER_DAY;
            case R1_DEPLOY:
                return (rel.getR1_deploy_epoch() - now) / MILLIS_PER_DAY;
            case R2_DEPLOY:
                return (rel.getR2_deploy_epoch() - now) / MILLIS_PER_DAY;
            case R3_DEPLOY:
                return (rel.getR3_deploy_epoch() - now) / MILLIS_PER_DAY;
            case R4_DEPLOY:
                return (rel.getR4_deploy_epoch() - now) / MILLIS_PER_DAY;
            case R5_DEPLOY:
                return (rel.getR5_deploy_epoch() - now) / MILLIS_PER_DAY;
            case R6_DEPLOY:
                return (rel.getR6_deploy_epoch() - now) / MILLIS_PER_DAY;
            case R7_DEPLOY:
                return (rel.getR7_deploy_epoch() - now) / MILLIS_PER_DAY;
            case R8_DEPLOY:
                return (rel.getR8_deploy_epoch() - now) / MILLIS_PER_DAY;
            case RN_BUILD:
                return (findRelevantDate(rel.getReleaseBuilds(), now) - now) / MILLIS_PER_DAY;
            case R1_BUILD:
                return (rel.getR1_build_epoch() - now) / MILLIS_PER_DAY;
            case R2_BUILD:
                return (rel.getR2_build_epoch() - now) / MILLIS_PER_DAY;
            case R3_BUILD:
                return (rel.getR3_build_epoch() - now) / MILLIS_PER_DAY;
            case R4_BUILD:
                return (rel.getR4_build_epoch() - now) / MILLIS_PER_DAY;
            case R5_BUILD:
                return (rel.getR5_build_epoch() - now) / MILLIS_PER_DAY;
            case R6_BUILD:
                return (rel.getR6_build_epoch() - now) / MILLIS_PER_DAY;
            case R7_BUILD:
                return (rel.getR7_build_epoch() - now) / MILLIS_PER_DAY;
            case R8_BUILD:
                return (rel.getR8_build_epoch() - now) / MILLIS_PER_DAY;
            case DEV_COMPLETE_ONE:
                return (rel.getDev_complete_one_epoch() - now) / MILLIS_PER_DAY;
            case DEV_COMPLETE_TWO:
                return (rel.getDev_complete_two_epoch() - now) / MILLIS_PER_DAY;
            case DEV_COMPLETE_THREE:
                return (rel.getDev_complete_three_epoch() - now) / MILLIS_PER_DAY;
            case EXTERNAL_SHIP:
                return (rel.getExternal_ship_epoch() - now) / MILLIS_PER_DAY;
            case FUNC_SPEC_COMPLETE:
                return (rel.getFunc_spec_complete_epoch() - now) / MILLIS_PER_DAY;
            case FUNC_SPEC_APPROVED:
                return (rel.getFunc_spec_approved_epoch() - now) / MILLIS_PER_DAY;
            case THROTTLE_BRANCH:
                return (rel.getThrottle_branch_epoch() - now) / MILLIS_PER_DAY;
            }
            throw new AssertionError("Unknown milestone: " + this);
        }
        /* At this point, we can't calculate how many days until, because
         * grouping doesn't have the appropriate method.  Punt. */
        return 0;
    }

    /**
     * @param dates Map whose keys are Longs representing epoch dates.
     * @param now
     * @return The latest date that's not already passed, or zero if there
     * are no dates in the Map.
     */
    private long findRelevantDate(SortedMap<String, Long> dates, long now) {
        long relevant = 0;
        for (Map.Entry<String, Long> me : dates.entrySet()) {
            relevant = me.getValue();
            if (relevant > now) {
                break;
            }
        }
        return relevant;
    }

    public boolean isActive(Record grouping) {
        switch(this) {
        case IN_DEVELOPMENT:
            if (!(grouping instanceof NpiPhased)) {
                // grouping not applicable to this Milestone
                return false;
            }
            NpiPhased phased = (NpiPhased) grouping;
            long now = System.currentTimeMillis();
            return (phased.getP0_exit_epoch() > 0 &&
                phased.getP0_exit_epoch() < now &&
                phased.getP4_exit_epoch() > now);
        case NO_MILESTONE:
            return true;
        default:
            return daysUntil(grouping) > 0;
        }
    }

    public boolean isExceptionCDM(Record grouping){
    	boolean retval = false;
        if (this.isAnNpiPhase) {
        	if (grouping instanceof ReleaseRecord){
        		ReleaseRecord rec = (ReleaseRecord)grouping;
        		if (rec.isCDMrelease()){
        			// NPI phases in CDM releases is not correctly associated
        			// so we remove it
        			retval = true;
        		}
        	}
        }
        
        if (grouping instanceof ReleaseRecord){
    		ReleaseRecord rec = (ReleaseRecord)grouping;
        	switch(this){
	    		case DEV_COMPLETE_ONE:
	    			if (rec.isCDMrelease()) { retval = true; }
	            case DEV_COMPLETE_TWO: 
	    			if (rec.isCDMrelease()) { retval = true; }
	            case DEV_COMPLETE_THREE:
	    			if (rec.isCDMrelease()) { retval = true; }
	            case EXTERNAL_SHIP: 
	    			if (rec.isCDMrelease()) { retval = true; }
	            case FUNC_SPEC_COMPLETE: 
	    			if (rec.isCDMrelease()) { retval = true; }
	            case FUNC_SPEC_APPROVED: 
	    			if (rec.isCDMrelease()) { retval = true; }
	            case THROTTLE_BRANCH:
	    			if (rec.isCDMrelease()) { retval = true; }
        	}
        }
    	return retval;
    }
    
    public boolean isABigWindow() {
        return isABigWindow;
    }
    
    public static String serializeAll() {
        List<Map<String, Object>> miles =
            new ArrayList<Map<String, Object>>(Milestone.values().length);
        int sort = 0;
        for (Milestone m : Milestone.values()) {
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("class", "net.juniper.dash.Milestone");
            map.put("id", m.name());
            map.put("name", m.prettyName);
            map.put("big_window", m.isABigWindow);
            map.put("npi_phase", m.isAnNpiPhase);
            map.put("sort_key", sort++);
            miles.add(map);
        }
        return new JSONSerializer().serialize(miles);
    }
}
