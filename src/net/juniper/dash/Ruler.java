/**
 * dbergstr@juniper.net, Nov 6, 2006
 *
 * Copyright (c) 2006-2007, Juniper Networks, Inc.
 * All rights reserved.
 */
package net.juniper.dash;

import java.io.IOException;
import java.io.StringReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import org.apache.log4j.Logger;
import org.drools.RuleBase;
import org.drools.RuleBaseFactory;
import org.drools.StatefulSession;
import org.drools.compiler.DroolsParserException;
import org.drools.compiler.PackageBuilder;
import org.drools.compiler.PackageBuilderConfiguration;
import org.drools.rule.Package;
import org.drools.rule.builder.dialect.java.JavaDialectConfiguration;

/**
 * Handles running the rules engine.
 *
 * @author dbergstr
 */
public class Ruler {

    protected static final Logger logger =
        Logger.getLogger(Ruler.class.getName());

    private static String DELETE_RULE = "DELETE FROM " + Repository.dbname + " WHERE identifier=?";

    private static String INSERT_RULE =
        "INSERT INTO " + Repository.dbname + "  (identifier, name, description, owner, milestone,\n" +
        "horizon, lhs, active, weight, counts, query_fields, objective,\n" +
        "grouping_variable, rhs, attributes, agenda_group, lede,\n" +
        "last_updated, sunset_milestone, sunset_horizon)\n" +
        "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, " +
        "CURRENT_TIMESTAMP, ?, ?)";

    private static String UPDATE_RULE =
        "UPDATE " + Repository.dbname + " SET identifier=?, name=?, description=?, owner=?,\n" +
        "milestone=?, horizon=?, lhs=?, active=?, weight=?, counts=?,\n" +
        "query_fields=?, objective=?, grouping_variable=?, rhs=?," +
        "attributes=?, agenda_group=?, lede=?, last_updated=CURRENT_TIMESTAMP,\n" +
        "sunset_milestone=?, sunset_horizon=? WHERE identifier=?";

    private static String FIND_RULES =
        "SELECT identifier, name, description, owner, milestone, horizon, " +
        "lhs, last_updated, active, weight, counts, query_fields, objective, " +
        "grouping_variable, rhs, attributes, agenda_group, lede, " +
        "sunset_milestone, sunset_horizon FROM " + Repository.dbname ;

    private RuleBase ruleBase;

    private PackageBuilderConfiguration pkgBuilderCfg;

//    private StringReader dslReader;

    public Ruler() throws DashboardException {
        pkgBuilderCfg = new PackageBuilderConfiguration();
        JavaDialectConfiguration javaConf = (JavaDialectConfiguration)
                                            pkgBuilderCfg.getDialectConfiguration("java");
        javaConf.setCompiler(JavaDialectConfiguration.JANINO);
        javaConf.setJavaLanguageLevel("1.5");

        ruleBase = RuleBaseFactory.newRuleBase();

        /* Load up the business rules */
        loadRules();
        short count_rule = 0;
        for (Rule rule : Repository.getRules()) {
            /**
             * the closedPR is need to be there although we are not making it active
             */
            boolean isAdded = (rule.isActive());
            if (Repository.testingMode) {
                //  Put the testing rules here for faster development
                isAdded = (
                              rule.getIdentifier().startsWith("hardening_")
                          ) & rule.isActive();
            }

            if (  isAdded )  {
                count_rule ++;
                try {
                    addRuleToRuleBase(rule);
                } catch (DashboardException e) {
                    String errmsg = "Failed to add rule to RuleBase: " +
                                    e.getMessage();
                    Repository.logError(errmsg);
                    logger.error(errmsg);
                }
            } else if (logger.isDebugEnabled()) {
                logger.debug("Skipping inactive rule '" + rule.getName()
                             + "'");
            }
        }
        logger.info("Rules from " + Repository.dbname + " successfully added " + count_rule);
    }

    /**
     * @return A new StatefulSession, which will be updated whenever rules are
     * added or removed.
     */
    StatefulSession getNewStatefulSession() {
        return ruleBase.newStatefulSession();
    }

    public String toString() {
        return "Ruler of the Universe!";
    }

    /**
     * Remove the rule from the ruleBase and set it to inactive.
     *
     * @param rule
     */
    public void removeRule(Rule rule) {
        synchronized (Repository.needToCullRules) {
            logger.info("Removing rule '" + rule.getName() + "' from RuleBase.");
            rule.setActive(false);
            try {
                ruleBase.removePackage(rule.getPackageName());
                Repository.needToCullRules = true;
            } catch (IllegalArgumentException e) {
                /* The rule isn't in the rule base (happens if you try to
                 * remove a rule that you hadn't yet activated. */
                logger.warn("Rule \"" + rule.getName() + "\" not found in " +
                            " RuleBase during removePackage().");
            }
        }
    }

    public void deleteRule(Rule rule) throws DashboardException {
        logger.info("Deleting rule '" + rule.getName() +
                    "' from Database and RuleBase.");
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            conn = Repository.getConnection();
            stmt = conn.prepareStatement(DELETE_RULE);
            stmt.setString(1, rule.getIdentifier());
            stmt.execute();
            conn.commit();
        } catch (SQLException e) {
            String msg = "Exception deleting rule: " + e.getMessage();
            Repository.logError(msg);
            logger.error(msg);
            throw new DashboardException(msg);
        } finally {
            Repository.closeConnStmtRset(conn, stmt, null);
        }
        removeRule(rule);
    }

    /**
     * Save a rule to the database.
     *
     * @param rule
     * @throws DashboardException
     */
    public void saveRule(Rule rule) throws DashboardException {
        logger.info("Saving rule '" + rule.getName() + "'.");
        Connection conn = null;
        PreparedStatement stmt = null;
        String sql;
        if (rule.isNewlyCreated()) {
            sql = INSERT_RULE;
        } else {
            sql = UPDATE_RULE;
        }
        try {
            conn = Repository.getConnection();
            stmt = conn.prepareStatement(sql);
            stmt.setString(1, rule.getIdentifier());
            stmt.setString(2, rule.getName());
            stmt.setString(3, rule.getDescription());
            stmt.setString(4, rule.getOwner());
            stmt.setString(5, rule.getMilestone().name());
            stmt.setInt(6, rule.getHorizon());
            stmt.setString(7, rule.getLhs());
            stmt.setBoolean(8, rule.isActive());
            stmt.setInt(9, rule.getWeight());
            stmt.setString(10, rule.getCounts().name());
            stmt.setString(11, rule.getColumnsDelimited());
            stmt.setString(12, rule.getObjective().name());
            stmt.setString(13, rule.getGroupingVariable());
            stmt.setString(14, rule.getRhs());
            stmt.setString(15, rule.getAttributes());
            stmt.setString(16, rule.getAgendaGroup());
            stmt.setString(17, rule.getLede());
            stmt.setString(18, rule.getSunsetMilestone().name());
            stmt.setInt(19, rule.getSunsetHorizon());
            if (! rule.isNewlyCreated()) {
                stmt.setString(20, rule.getIdentifier());
            }
            stmt.execute();
            rule.saved();
        } catch (SQLException e) {
            String msg = "Exception saving rule: " + e.getMessage();
            Repository.logError(msg);
            logger.error(msg);
            throw new DashboardException(msg);
        } finally {
            Repository.closeConnStmtRset(conn, stmt, null);
        }
    }

    /**
     * Load rules from the db and instantiate a Rule object for each one.
     *
     * @throws DashboardException
     */
    private void loadRules() throws DashboardException {
        if (logger.isDebugEnabled()) {
            logger.debug("Loading rule definitions from db.");
        }
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rset = null;
        try {
            conn = Repository.getConnection();
            stmt = conn.prepareStatement(FIND_RULES);
            rset = stmt.executeQuery();

            int count = 0;
            while (rset.next()) {
                String identifier = rset.getString(1);
                String name = rset.getString(2);
                String description = rset.getString(3);
                String owner = rset.getString(4);
                String milestone = rset.getString(5);
                int horizon = rset.getInt(6);
                String lhs = rset.getString(7);
                Date lastUpdated = rset.getDate(8);
                boolean active = rset.getBoolean(9);
                int weight = rset.getInt(10);
                String countsType = rset.getString(11);
                String queryFields = rset.getString(12);
                String objective = rset.getString(13);
                String groupingVariable = rset.getString(14);
                String rhs = rset.getString(15);
                String attributes = rset.getString(16);
                String agendaGroup = rset.getString(17);
                String lede = rset.getString(18);
                String sunsetMilestone = rset.getString(19);
                int sunsetHorizon = rset.getInt(20);
                if (logger.isTraceEnabled()) {
                    logger.trace("Read rule " + identifier);
                }
                Rule r;
                try {
                    r = new Rule(identifier, name, objective, owner,
                                 description, queryFields, lhs, rhs, milestone, horizon,
                                 sunsetMilestone, sunsetHorizon, active, weight,
                                 countsType, groupingVariable, attributes, agendaGroup,
                                 lede, lastUpdated);
                } catch (IllegalArgumentException iae) {
                    String errmsg = "IllegalArgumentException for rule '" + name +
                                    "': " + iae.getMessage();
                    Repository.logError(errmsg);
                    logger.error(errmsg);
                    continue;
                }
                Repository.addRule(r);
                count++;
            }
            if (count == 0) {
                String msg = "Unable to proceed: No rules found in db.";
                logger.fatal(msg);
                throw new DashboardException(msg);
            }
        } catch (SQLException e) {
            String msg = "Exception reading rules: " + e.getMessage();
            Repository.logError(msg);
            logger.error(msg);
            throw new DashboardException(msg);
        } finally {
            Repository.closeConnStmtRset(conn, stmt, rset);
        }
    }

    /**
     * Add a new Rule to the RuleBase.
     *
     * @param rule
     * @throws DashboardException
     */
    public void addRuleToRuleBase(Rule rule) throws DashboardException {
        if (logger.isDebugEnabled()) {
            logger.debug("Adding rule '" + rule.getName() + "' to RuleBase.");
        }
        if (logger.isTraceEnabled()) {
            logger.trace("Rule=" + rule.getFullRuleText());
        }
        try {
            ruleBase.addPackage(parseRule(rule));
            rule.setActive(true);
            logger.info("Rule '" + rule.getName() + "' added to RuleBase.");
        } catch (DashboardException e) {
            // Otherwise it gets caught below.
            throw e;
        } catch (Exception e) {
            /* Grumble.  RuleBase.addPackage() declares "Exception",
             * which is waaaay to broad. */
            e.printStackTrace();
            throw new DashboardException("Exception adding pkg to ruleBase " +
                                         "for rule'" + rule.getName() + "': " + e);
        }
    }

    /**
     * Parse a Rule.
     *
     * @param rule
     * @return A Package containing the rule.
     * @throws DashboardException If the Rule is invalid.
     */
    public Package parseRule(Rule rule) throws DashboardException {
        PackageBuilder rulePackageBuilder = new PackageBuilder(pkgBuilderCfg);
        try {
            rulePackageBuilder.addPackageFromDrl(
                new StringReader(rule.getFullRuleText())); // FIXME add dslReader
            if (rulePackageBuilder.hasErrors()) {
                throw new DashboardException("Errors parsing rule '" +
                                             rule.getName() + "': " +
                                             rulePackageBuilder.getErrors().toString());
            }
            return rulePackageBuilder.getPackage();
        } catch (DroolsParserException e) {
            throw new DashboardException("Exception parsing rule '" +
                                         rule.getName() + "': " + e.getMessage());
        } catch (IOException e) {
            // Shouldn't get here since we're using StringReader for "IO".
            throw new DashboardException("Surprising IOException during " +
                                         "processing of rule '" + rule.getName() + "': " +
                                         e.getMessage());
        }
    }
}
