/**
 * allenyu@juniper.net, Jul 16, 2012
 *
 * Copyright (c) 2012, Juniper Networks, Inc.
 * All rights reserved.
 */
package net.juniper.dash;

import java.lang.reflect.Method;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Arrays;
import java.util.Map;
import java.util.List;
import java.util.Set;

import javax.naming.Context;
import javax.naming.InitialContext;

import org.apache.log4j.Logger;

public class CRSDBHandler implements Runnable {
	private Class cls;
	private String clsname;
	private String job;
	private Map<String, Object> task;

	private Connection con;
	private PreparedStatement pstmt;

	static Logger logger;
	private String errmsg;

	public CRSDBHandler(Map<String, Object> task){
	    cls = this.getClass();
	    clsname = cls.getName();
	    logger = Logger.getLogger(clsname);
	    errmsg = "Error " + clsname + " for job = ";
	    this.task = task;
	    this.job = (String)this.task.get("job");
	    setup();
	}

	private void setup(){
		try {
			Context ctx = new InitialContext();
			DataSource pds = (DataSource)ctx.lookup("java:comp/env/jdbc/crs");
			con = pds.getConnection();
			logger.debug(clsname + " for job " + job + " : connection established " + con.toString());
	    } catch (Exception e){
			e.printStackTrace();
			errmsg += job + " " + e.getMessage();
			logger.error(errmsg);
			Repository.logError(errmsg);
		}
	}

	@Override
	public void run() {
		if (job == null || "".equals(job)){
			logger.debug(clsname + " for job " + job + " = null");
			return;
		}
	    try {
	    	Method method = cls.getDeclaredMethod(job);
	    	con.setAutoCommit(false);
	    	method.invoke(this);
	    	con.commit();
	    	con.close();
	    } catch (Exception e) {
	    	errmsg += job + " " + e.getMessage();
	    	logger.error(errmsg);
	    	Repository.logError(errmsg);
	    } catch (Error e)  {
	    	errmsg += job + " " + e.getMessage();
	    	logger.error(errmsg);
	    	Repository.logError(errmsg);
	    } 
	}

	private void getAll() throws Exception {

		logger.debug(clsname + " for job " + job + " starts");

		List<String[]> all = (List<String[]>)task.get("all");
		String all_fields = (String)task.get("all_fields");

		try {
                       /*
                        * The column names (all_fields):
                        *   throttle_request_id
                        *   user_username
                        *   throttle_request_state
                        *   throttle_request_type
                        *   approval_tier
                        *   issue_synopsis
                        *   last_modified_date
                        *   mgr_approval_state
                        *   release_name
                        *   approver_username
                        *   throttle_approval_jrm_state
                        *   jrm_username
                        *   date_part('day',cr_life)
                        *   default_approver_list
                        */
			pstmt = con.prepareStatement("select " + all_fields +" from mba.throttle_request_release_view where throttle_request_state in ('Open', 'Pending on Approval', 'Information Required') union select " + all_fields + " from mba.throttle_request_release_view where throttle_request_state in ('Expired') and date_part('day',current_date - last_modified_date) < 30;");
			ResultSet rslt = pstmt.executeQuery();
			int NumOfCol = rslt.getMetaData().getColumnCount();
			StringBuilder builder = new StringBuilder();
			String str;
			while (rslt.next()){
				for (int i=1; i<=NumOfCol; i++){
					str = rslt.getString(i);
					if (str == null){
						str = "";
					}
					str = str.trim();
					if (str.equals("not-set") || "n/a".equals(str) || "".equals(str) || "NULL".equals(str) || "null".equals(str)){
						str = "";
					}
					builder.append(str);
				    builder.append("#C#R#S#");
				}
				str = builder.toString();
				all.add(str.split("#C#R#S#"));
				builder = new StringBuilder();
			}
		    pstmt.close();
		    logger.debug(clsname + " for job " + job + " get " + all.size() + " rows.");
	    } catch (Exception e){
		    logger.debug(clsname + " for job " + job + "  " + e.toString());
		    throw e;
		}

		logger.debug(clsname + " for job " + job + " ends");
	}
	
	private void getDefaultApprovers() throws Exception {
		
		logger.debug(clsname + " for job " + job + " starts");
		
		Set<String> users = (Set<String>)task.get("users");
		
		try {	
			pstmt = con.prepareStatement("select distinct default_approver_list from mba.throttle_request_release_view;");
			ResultSet rslt = pstmt.executeQuery();
			String str;
			int count = 0;
			while (rslt.next()){
				str = rslt.getString(1);
				if (str == null || "n/a".equals(str) || str.equals("not-set") || "".equals(str) || "NULL".equals(str) || "null".equals(str)){
					continue;
				}
				str = str.trim();
				while (str.indexOf(" ") >= 0){
					str = str.replace(" ", "");
				}
				if (str.indexOf(",") >= 0){
					users.addAll(Arrays.asList(str.split(",")));
				} else {
				    users.add(str);
				}
				count++;
			}
		    pstmt.close();
		    logger.debug(clsname + " for job " + job + " get " + count + " rows.");		
	    } catch (Exception e){
		    logger.debug(clsname + " for job " + job + "  " + e.toString());
		    throw e;
	    }
		
		logger.debug(clsname + " for job " + job + " ends");
	}

}
