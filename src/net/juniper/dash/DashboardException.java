/**
 * dbergstr@juniper.net, Oct 24, 2006
 *
 * Copyright (c) 2006, Juniper Networks, Inc.
 * All rights reserved.
 */
package net.juniper.dash;


/**
 * Application exception class.  
 *
 * @author dbergstr
 */
public class DashboardException extends Exception {

    /**
     * Satisfies serializable interface.
     */
    private static final long serialVersionUID = 1L;

    /**
     * 
     */
    public DashboardException() {
        super();
    }

    /**
     * @param message
     */
    public DashboardException(String message) {
        super(message);
    }

    /**
     * @param cause
     */
    public DashboardException(Throwable cause) {
        super(cause);
    }

    /**
     * @param message
     * @param cause
     */
    public DashboardException(String message, Throwable cause) {
        super(message, cause);
    }
}
