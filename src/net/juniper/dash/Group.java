/**
 * dbergstr@juniper.net, Jan 16, 2007
 *
 * Copyright (c) 2007, Juniper Networks, Inc.
 * All rights reserved.
 */
package net.juniper.dash;

import java.util.SortedSet;
import java.util.TreeSet;

import org.apache.log4j.Logger;


/**
 * Internal representation of an LDAP group.
 *
 * FIXME Might make sense to turn this whole thing into an Enum, make it a
 * bit less generalized, but a lot simpler to code to. 
 *
 * @author dbergstr
 */
public class Group {
    
    protected static final Logger logger =
        Logger.getLogger(Group.class.getName());

    private String name;
    private String ldapGroupName;
    private long lastUpdateTime;
    private SortedSet<String> members;
    
    public Group(String name, String ldapGroupName) throws DashboardException {
        this.name = name;
        this.ldapGroupName = ldapGroupName;
        refresh();
    }
    
    public void refresh() throws DashboardException {
        members = new TreeSet<String>(Repository.expandLDAPGroup(ldapGroupName));
        lastUpdateTime = System.currentTimeMillis();
        if (logger.isDebugEnabled()) {
            logger.debug("Refreshed group '" + name +
                "' from LDAP group '" + ldapGroupName + "', found " +
                members.size() + " members.");
        }
    }
    
    /**
     * @return the members
     */
    public SortedSet<String> getMembers() {
        return members;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    
    /**
     * @return the ldapGroupName
     */
    public String getLdapGroupName() {
        return ldapGroupName;
    }

    /**
     * @return the time elapsed since the last unit of work was completed.
     */
    public float getMinutesSinceLastUpdate() {
        if (lastUpdateTime == 0) {
            return -1;
        } else {
            return Math.round(
                (System.currentTimeMillis() - lastUpdateTime) / 6000f)
                / 10f;
        }
    }
    
    public enum Groups {
        CORE_MANAGERS("core_manager_group"),
        MANAGERS("manager_group"),
        TOP_LEVEL("top_level_group"),
        ALL_USERS("all_users_group"),
        ADMIN("admin_group");

        public final String key;
        
        private Groups(String keyname) {
            key = keyname;
        }
    }
}
