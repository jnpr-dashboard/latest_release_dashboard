/**
 * allenyu@juniper.net, Mar 9, 2012
 *
 * Copyright (c) 2012, Juniper Netgrouping_names, Inc.
 * All rights reserved.
 */
package net.juniper.dash;

import java.util.Map;
import java.util.Set;
import java.util.HashSet;
import java.util.Properties;
import java.lang.reflect.Method;
import java.sql.PreparedStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import javax.naming.Context;
import javax.naming.InitialContext;

import oracle.jdbc.pool.OracleDataSource;

import org.apache.log4j.Logger;

import net.juniper.dash.Repository;

public class DashDBHandler implements Runnable {
private Class cls;
private String clsname;
private String job;
private Map<String, Object> task;

private Connection con;
private PreparedStatement pstmt;

static Logger logger;
private String errmsg = "Error " + clsname + " for job = ";

public DashDBHandler(Map<String, Object> task){
cls = this.getClass();
logger = Logger.getLogger(cls.getName());
clsname = cls.getName();
this.task = task;
this.job = (String)this.task.get("job");
setup();
}

private void setup(){
try {
Context initCtx = new InitialContext();
    Properties props = new Properties();
    props.put("oracle.jdbc.RetainV9LongBindBehavior", "true");
    OracleDataSource ods = (OracleDataSource)initCtx.lookup("java:comp/env/jdbc/dash");
    ods.setConnectionProperties(props);
con = ods.getConnection();
logger.debug(clsname + " for job " + job + " : connection established " + con.toString());
} catch (Exception e){
e.printStackTrace();
errmsg += job + " " + e.getMessage();
     logger.error(errmsg);
            Repository.logError(errmsg);
}
}

@Override
public void run() {
if (job == null || "".equals(job)){
logger.debug(clsname + " for job " + job + " = null");
return;
}
try {
Method method = cls.getDeclaredMethod(job);
method.invoke(this);
con.commit();
          con.close();
    } catch (Exception e) {
     errmsg += job + " " + e.getMessage();
     logger.error(errmsg);
            Repository.logError(errmsg);
    } catch (Error e)  {
     errmsg += job + " " + e.getMessage();
     logger.error(errmsg);
            Repository.logError(errmsg);
    } 
}

private void getPossibleAliases() throws Exception {

logger.debug(clsname + " for job " + job + " starts");

Set<String> possibleAliases = (Set<String>)task.get("possibleAliases");

try {
String sql = "select ORG_ID from ORGCHART";
pstmt = con.prepareStatement(sql);
            ResultSet rslt = pstmt.executeQuery();
            Set<String> orgIds = new HashSet<String>();
while (rslt.next()){
orgIds.add(rslt.getString(1));
}
pstmt.close();

possibleAliases.removeAll(orgIds);
    } catch (Exception e){
    logger.debug(clsname + " for job " + job + "  " + e.toString());
    throw e;
    }

logger.debug(clsname + " for job " + job + " ends");
}

private void getAliases() throws Exception {

logger.debug(clsname + " for job " + job + " starts");

Map<String, Set<String>> userAliasMap = (Map<String, Set<String>>)task.get("userAliasMap");
Map<String, Set<String>> aliasUserMap = (Map<String, Set<String>>)task.get("aliasUserMap");

try {
String sql = "select ALIAS, BROAD_USER from ALIASES";
pstmt = con.prepareStatement(sql);
            ResultSet rslt = pstmt.executeQuery();
            String alias, user;
            Set<String> aliases, users;
while (rslt.next()){
alias = rslt.getString(1);
user = rslt.getString(2);
users = aliasUserMap.get(alias);
if (users == null){
users = new HashSet<String>();
aliasUserMap.put(alias, users);
}
users.add(user);

aliases = userAliasMap.get(user);
if (aliases == null){
aliases = new HashSet<String>();
userAliasMap.put(user, aliases);
}
aliases.add(alias);
}
pstmt.close();
    } catch (Exception e){
    logger.debug(clsname + " for job " + job + "  " + e.toString());
    throw e;
    }

logger.debug(clsname + " for job " + job + " ends");
}

private void storeAliases() throws Exception {

logger.debug(clsname + " for job " + job + " starts");
Map<String, Set<String>> aliasUserMap = (Map<String, Set<String>>)task.get("aliasUserMap");

try {
con.setAutoCommit(false);
String sql = "delete from ALIASES";
pstmt = con.prepareStatement(sql);
pstmt.executeUpdate();
pstmt.close();

sql = "insert into ALIASES (ALIAS, BROAD_USER) values (?,?)";
pstmt = con.prepareStatement(sql);

Set<String> users;
for (String alias : aliasUserMap.keySet()){
users = aliasUserMap.get(alias);
for (String user:  users){
pstmt.setString(1, alias);
pstmt.setString(2, user);
pstmt.addBatch();
}
}
int[] num = pstmt.executeBatch();
pstmt.close();
logger.debug(clsname + " for job " + num.length + " alias/user in DB");
    } catch (Exception e){
    logger.debug(clsname + " for job " + job + "  " + e.toString());
    throw e;
    }

logger.debug(clsname + " for job " + job + " ends");
}
}
