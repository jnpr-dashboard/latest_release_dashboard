/**
 * dbergstr@juniper.net, Nov 20, 2006
 *
 * Copyright (c) 2006, Juniper Networks, Inc.
 * All rights reserved.
 */
package net.juniper.dash;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

public abstract class Refresher extends Thread implements Comparable<Refresher> {

    static Logger logger = Logger.getLogger(Refresher.class.getName());

    protected boolean jolted = false;
    protected boolean killed = false;
    protected boolean done = false;
    protected int unitsCompleted = 0;
    protected long lastUnitTime = 0;
    private String deathCertificate = "Not yet Started";

    /**
     * Number of RuntimeExceptions we've hit.
     */
    private int exceptionCount = 0;

    /**
     * How many RuntimeExceptions will we swallow before dying?
     */
    protected int maxRuntimeExceptionsToAccept = 3;

    /**
     * If this is non-zero, we sleep every time through the run() loop.
     */
    protected long napTime = 0;

    /**
     * If true, nap once at the beginning of run().
     */
    protected boolean napBeforeRunning = false;

    public Refresher(String name) {
        super(name);
        setDaemon(true);
        Repository.registerRefresher(this);
    }

    /*
     * All threads in dashboard come here
     * Watchdog is the first one, then the datasources will go here also
     */
    public void run() {
        if (logger.isTraceEnabled()) {
            logger.trace(getName() + " now run()ing.");
        }
        deathCertificate = "I think I'm alive.";
        if (napBeforeRunning) {
            nap(napTime);
        }
        jolted = false;
        done = false;
        // this wraps .work() to allow thread to die when finished and
        // to catch unexpected errors to run it again plus a few misc
        // admin things
        while (! (killed || done)) {
            try {
            	// wrap actual work class to catch errors and 
            	// let it continue again
                work();
                lastUnitTime = System.currentTimeMillis();
                unitsCompleted++;
                if (logger.isTraceEnabled()) {
                    logger.trace(getName() + " completed one unit of work().");
                }
                if (napTime > 0) {
                    nap(napTime);
                }
            }
            catch (RefresherCanceledException rce) {
                // We're done.
                logger.info("Refresher '" + getName() + "' shutting down.");
                deathCertificate = "Canceled intentionally.";
                return;
            }
            catch (RuntimeException rte) {
                handleException(rte);
                // stop everything if in testing mode, because somethign went wrong and we need to see it
                if (Repository.testingMode) {
                	logger.fatal("In testing mode and STOPPING because we hit an error");
                	done = true;          
                	Repository.shutdown();
                }
            }
        }
        logger.info("Refresher '" + getName() + "' shutting down.");
        deathCertificate = (done ? "Done." : "Canceled intentionally.");
    }

    /**
     * @return The number of times the work() method has run.
     */
    public int getUnitsCompleted() {
        return unitsCompleted;
    }

    /**
     * @return the exceptionCount
     */
    public int getExceptionCount() {
        return exceptionCount;
    }

    public String getDeathCertificate() {
        return deathCertificate ;
    }

    /**
     * This is where the actual work is done.  It's the core of run().
     */
    protected abstract void work();

    /**
     * If sleeping, wake up; if awake, skip next nap.
     */
    public synchronized void jolt() {
        if (logger.isTraceEnabled()) {
            logger.trace(getName() + " jolted.");
        }
        jolted = true;
        interrupt();
    }

    /**
     * Stop this thread as soon as practical.
     */
    public synchronized void kill() {
        jolted = false;
        killed  = true;
        interrupt();
    }

    /**
     * See if we need to stop.
     *
     * TODO It might be nice to have a "pause" method, so we could
     * temporarily stop when the db is down, etc.
     */
    public synchronized void checkIfKilled() {
        if (killed || (! jolted && interrupted())) {
            if (logger.isDebugEnabled()) {
                logger.debug(this.getName() + " killed (jolted=" + jolted + ")");
            }
            throw new RefresherCanceledException("Thread " +
                this.getName() + "Canceled");
        }
    }

    /**
     * Sleep for time millis, wake up if interrupted, and possibly shut down
     * the whole thread.
     */
    public void nap(long time) {
        checkIfKilled();
        if (jolted) {
            if (logger.isDebugEnabled()) {
                logger.debug(this.getName() + " jolted, skipping nap.");
            }
            jolted = false;
            return;
        }
        try {
            if (logger.isDebugEnabled()) {
                //logger.debug(getName() + " napping for " + time);
            }
            Thread.sleep(time);
        } catch (InterruptedException e) {
            if (jolted && ! killed) {
                if (logger.isDebugEnabled()) {
                    logger.debug(this.getName() + " jolted while napping.");
                }
                jolted = false;
                return;
            } else {
                /* We'll take this to mean that we should quit. */
                if (logger.isDebugEnabled()) {
                    logger.debug(this.getName() + " interrupted to death. " +
                            "(jolted=" + jolted + ")");
                }
                throw new RefresherCanceledException("Thread " +
                    this.getName() + "Canceled");
            }
        }
    }

    /**
     * Run an external process that returns delimited record data.
     *
     * @param proc Specification for the process.
     * @param timeout Kill the process after this many millis (minimum 5000).
     * @throws DashboardException If the process does not exit successfully.
     * @throws TimeoutException If the process does not exit before the timeout.
     */
    @SuppressWarnings("unchecked")
    public List<String[]> getDelimitedOutput(ProcessBuilder proc, long timeout)
            throws DashboardException, TimeoutException {
                return (List<String[]>) runExternal(proc, timeout, true);
            }

    /**
     * Run an external process that returns lines of output.
     *
     * @param proc Specification for the process.
     * @param timeout Kill the process after this many millis (minimum 5000).
     * @throws DashboardException If the process does not exit successfully.
     * @throws TimeoutException If the process does not exit before the timeout.
     */
    @SuppressWarnings("unchecked")
    public List<String> getOutput(ProcessBuilder proc, long timeout)
    throws DashboardException, TimeoutException {
        return (List<String>) runExternal(proc, timeout, false);
    }

    /**
     * Run an external process, polling every five seconds for completion.
     *
     * @param proc Specification for the process.
     * @param timeout Kill the process after this many millis (minimum 5000).
     * @param expectsDelimitedOutput If true, return List<String[]>, otherwise
     * returns List<String>.
     * @throws DashboardException If the process does not exit successfully.
     * @throws TimeoutException If the process does not exit before the timeout.
     */
    private List runExternal(ProcessBuilder proc, long timeout,
            boolean expectsDelimitedOutput)
            throws DashboardException, TimeoutException {
        if (logger.isDebugEnabled()) {
            logger.debug(getName() + " running external process '" +
                proc.command().get(0) + "', returning delimited data.");
        }
        if (timeout < SleepyTimes.EXTERNAL_RELOADER_POLL_TIME.t()) {
            timeout = SleepyTimes.EXTERNAL_RELOADER_POLL_TIME.t();
        }
        boolean exited = false;
        Process p = null;
        List stdout;
        if (expectsDelimitedOutput) {
            stdout = new ArrayList<String[]>(500);
        } else {
            stdout = new ArrayList<String>(50);
        }
        StringBuffer stderr = new StringBuffer();
        try {
            p = proc.start();
            StreamCollector outproc =
                new StreamCollector(p.getInputStream(), stdout,
                    expectsDelimitedOutput);
            StreamCollector errproc =
                new StreamCollector(p.getErrorStream(), stderr);
            outproc.start();
            errproc.start();
            long dropDead = System.currentTimeMillis() + timeout;
            /* XXX??? There is a race condition here.  If the JVM stops
             * processing this thread right here, the timeout could elapse
             * before the while check runs.  It's unlikely, but real... */
            while (System.currentTimeMillis() < dropDead) {
                nap(SleepyTimes.EXTERNAL_RELOADER_POLL_TIME.t()); // 5 seconds
                try {
                    /* exitValue() throws unless the process is finished.
                     * Polling is kinda clunky, but wait() blocks, and I didn't
                     * want to spawn yet another thread. */
                    int exitVal = p.exitValue();
                    exited = true;
                    if (exitVal != 0) {
                        // Failure.  Log STDERR and throw.
                        StringBuilder se = new StringBuilder();
                        se.append("STDERR for command '");
                        se.append(getCommandAsString(proc));
                        se.append("' run by ");
                        se.append(getName());
                        se.append(" :\n");
                        // Wait a bit for STDERR output buffers to flush
                        nap(SleepyTimes.BUFFER_FLUSH_WAIT.t());
                        se.append(stderr);
                        logger.warn(se);
                        throw new DashboardException("Command '"
                            + proc.command().get(0) +
                            "' exited with nonzero value " +  exitVal);
                    }

                    // Wait for STDOUT buffers to flush
                    while (outproc.isAlive()
                        && System.currentTimeMillis() < dropDead) {
                        nap(SleepyTimes.BUFFER_FLUSH_WAIT.t());
                    }
                    break;
                }
                catch (IllegalThreadStateException itse) {
                    // process not done yet, nap again.
                }
            }
        }
        catch (IOException ioe) {
            // Process put up a fuss
            throw new DashboardException("Command '" + getCommandAsString(proc) +
                "' threw IOException: " + ioe.getMessage());
        }
        finally {
            if ( ! exited && null != p) {
                // Process still not done, kill it
                p.destroy();
                throw new TimeoutException("Command '" +
                    getCommandAsString(proc) +
                    "' exceeded timeout and was destroy()ed");
            }
        }
        if (logger.isTraceEnabled()) {
            logger.trace(getName() + " runExternal finished.");
        }
        String cmd = proc.command().get(0);
        String str_stderr = stderr.toString();
        if (stderr.length() > 0 && !cmd.contains("releases.pl") && !str_stderr.contains("Duplicate")) {
            logger.warn("Spurious output on STDERR from successful run of '" + cmd + "': " + str_stderr);
        }
        return stdout;
    }

    @SuppressWarnings("serial")
    public class TimeoutException extends Exception {
        public TimeoutException(String message) {
            super(message);
        }
    }

    private String getCommandAsString(ProcessBuilder proc) {
        StringBuilder cmd = new StringBuilder();
        for (String arg: proc.command()) {
            cmd.append(arg);
            cmd.append(" ");
        }
        return cmd.toString();
    }

    private static class StreamCollector extends Thread {
        InputStream instream;
        List list;
        StringBuffer buf = null;
        private boolean expectsDelimitedOutput;

        StreamCollector(InputStream instream, List list,
                boolean expectsDelimitedOutput) {
            this.instream = instream;
            this.list = list;
            this.expectsDelimitedOutput = expectsDelimitedOutput;
            setDaemon(false);
        }

        StreamCollector(InputStream instream, StringBuffer buf) {
            this.instream = instream;
            this.buf = buf;
            setDaemon(false);
        }

        // "unchecked" suppressed because 'list' is of type Object.
        @SuppressWarnings("unchecked")
        public void run() {
            BufferedReader reader =
                new BufferedReader(new InputStreamReader(instream));
            String line;
            try {
                if (null == buf && expectsDelimitedOutput) {
                    // We're collecting delimited data
                    StringBuilder sb = new StringBuilder();
                    boolean multiLine = false;
                    while ((line = reader.readLine()) != null) {
                        if (line.endsWith(Repository.RECORD_SEPARATOR)) {
                            if (multiLine) {
                                // This is end of a multi-line record
                                multiLine = false;
                                sb.append(line);
                                line = sb.toString();
                            }  // else it's a whole record on one line...
                            /* String.split() drops trailing empty fields,
                             * so we leave the record separator on the end of
                             * the line, and let the consumer ignore the
                             * extra field.*/
                            list.add(line.split(Repository.FIELD_SEPARATOR));
                        } else {
                            if (multiLine) {
                                // Continuation of a multi-line record
                                sb.append(line);
                            } else {
                                // First line of a multi-line record
                                multiLine = true;
                                sb.setLength(0);
                                sb.append(line);
                            }
                            // Add back the newline
                            sb.append("\n");
                        }
                    }
                } else if (null == buf && ! expectsDelimitedOutput) {
                    // Collecting lines
                    while ((line = reader.readLine()) != null) {
                        list.add(line);
                    }
                } else {
                    // We want to collect input into a buffer.
                    while ((line = reader.readLine()) != null) {
                        buf.append(line);
                    }
                }
            }
            catch (IOException ioe) {
                logger.warn("IOException reading stream: " + ioe.getMessage());
            }
            if (logger.isTraceEnabled()) {
                logger.trace("StreamCollector done.");
            }
        }
    }

    /**
     * This should ideally be overridden, as it does basically nothing.
     *
     * @param e The Exception to handle
     */
    protected void handleException(Exception e) {
        logger.error("Ignoring exception: " + e.getMessage(), e);
    }

    @SuppressWarnings("serial")
    public static class RefresherCanceledException extends RuntimeException {

        public RefresherCanceledException() {
            super();
        }

        public RefresherCanceledException(String message) {
            super(message);
        }

        public RefresherCanceledException(Throwable cause) {
            super(cause);
        }

        public RefresherCanceledException(String message, Throwable cause) {
            super(message, cause);
        }
    }

    /**
     * Refreshers sort in reverse name order.
     */
    public int compareTo(Refresher o) {
        int retval = o.getName().compareTo(getName());
        if (0 == retval) {
            retval = this.hashCode() - o.hashCode();
        }
        return retval;
    }

    /**
     * @return the time elapsed since the last unit of work was completed.
     */
    public float getMinutesSinceLastUnit() {
        if (lastUnitTime == 0) {
            return -1;
        } else {
            return Math.round(
                (System.currentTimeMillis() - lastUnitTime) / 6000f)
                / 10f;
        }
    }
}
