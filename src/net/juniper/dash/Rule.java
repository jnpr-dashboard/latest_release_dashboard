/**
 * dbergstr@juniper.net, Nov 30, 2006
 *
 * Copyright (c) 2006, Juniper Networks, Inc.
 * All rights reserved.
 */
package net.juniper.dash;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.SortedSet;
import java.util.TreeSet;

import net.juniper.dash.data.Record;

import org.apache.log4j.Logger;

import com.thoughtworks.xstream.XStream;
/**
 * FileStreamStrategy and StreamStrategy is deprecated
 */
import com.thoughtworks.xstream.persistence.FilePersistenceStrategy;
import com.thoughtworks.xstream.persistence.PersistenceStrategy;
import com.thoughtworks.xstream.persistence.XmlMap;

import flexjson.JSONSerializer;

/**
 * A dashboard rule.
 *
 * @author dbergstr
 */
public class Rule implements Comparable<Rule> {

    static Logger logger = Logger.getLogger(Rule.class.getName());

    private static XStream xstream = null;

    private static XStream getStreamer() {
        if (null != xstream) {
            return xstream;
        }
        xstream = new XStream();
        xstream.omitField(Rule.class, "newlyCreated");
        xstream.omitField(Rule.class, "changed");
        return xstream;
    }

    public static Rule fromXML(String xml) {
        logger.info("Creating Rule from XML.");
        Rule r = (Rule) getStreamer().fromXML(xml);
        if (Repository.getRule(r.identifier) == null) {
            // new rule
            r.active = false;
            r.newlyCreated = true;
        }
        Repository.addRule(r);
        return r;
    }

    /**
     * Write XStream representations of all rules to a directory.
     *
     * @param dirname The directory to write to.
     * @return The number of Rules written out.
     */
    @SuppressWarnings("unchecked")
    public static int persistAllRules(String dirname) {
        logger.info("Persisting all Rules to XML in " + dirname +  ".");
        PersistenceStrategy strategy = new FilePersistenceStrategy(new File(dirname));
        Map rules = new XmlMap(strategy);
        for (Rule r : Repository.getRules()) {
            rules.put(r.identifier, r);
        }
        return rules.size();
    }

    /**
     * Loads rules from XML files in the given directory.
     *
     * @param dirname A directory containing XStream XML Rule representations.
     * @param skipExisting If true, don't overwrite existing rules, only
     * create new ones.
     * @return The number of Rules loaded.
     */
    @SuppressWarnings("unchecked")
    public static int loadRules(String dirname, boolean skipExisting) {
        logger.info("Reading Rules as XML from " + dirname +  ".");
        PersistenceStrategy strategy = new FilePersistenceStrategy(new File(dirname));
        Map rules = new XmlMap(strategy);
        int count = 0;
        for(Iterator it = rules.values().iterator(); it.hasNext(); ) {
            Rule r = (Rule) it.next();
            if (Repository.getRule(r.identifier) != null) {
                // Rule already exists
                if (skipExisting) {
                    logger.debug("Skipping duplicate rule '" + r.identifier + "'");
                    continue;
                }
            } else {
                // new rule
                r.active = false;
                r.newlyCreated = true;
            }
            Repository.addRule(r);
            count++;
        }
        return count;
    }

    /**
     * Called by XStream during de-serialization.
     */
    private Object readResolve() {
        changed = true;
        lastUpdated = new Date();
        generateRuleText();
        return this;
    }

    /**
     * Save all changed rules to the database.
     *
     * @return The number of rules saved.
     * @throws DashboardException
     */
    public static int saveAllChangedRules() throws DashboardException {
        Ruler ruler = Repository.getRuler();
        int count = 0;
        for (Rule r : Repository.getRules()) {
            if (r.changed) {
                ruler.saveRule(r);
                count++;
            }
        }
        return count;
    }

    private static JSONSerializer serializer =
        new JSONSerializer().include("columns").
            exclude("fullRuleText", "changed", "newlyCreated", "oldId",
                "columnsDelimited");

    /**
     * @return All Rules serialized as JSON.
     */
    public static String serializeAll() {
        return serializer.serialize(Repository.getRules());
    }

    /**
     * The package name for all the generated DRL rules.
     */
    public static final String PACKAGE_BASE = "net.juniper.dash.rules.";

    /**
     * Imports for the generated DRL rules.
     */
    private static final String RULE_IMPORTS =
        "import java.util.Date;\n" +
        "import net.juniper.dash.data.RecordSet;\n" +
        "import net.juniper.dash.data.DataSource;\n" +
        "import net.juniper.dash.data.NPIUser;\n" +
        "import net.juniper.dash.data.Record;\n";

    private static final String COL_SPLIT_REGEX = "[,\\s]+";

    private static List<String> imports = new ArrayList<String>();

    public static void addImport(String type) {
        imports.add("import " + type + ";\n");
    }

    /**
     * A list of the agenda group names in all the rules, in reverse order
     * (because Drools processes agendas in LIFO order).  Note that if the
     * only rule in a group is removed, the name will remain in the list.
     * This isn't really a problem, since Drools silently ignores empty agendas.
     */
    private static SortedSet<String> agendaGroups =
        new TreeSet<String>(Collections.reverseOrder());

    public static SortedSet<String> getAgendaGroups() {
        return Collections.unmodifiableSortedSet(agendaGroups);
    }

    public static int firstPercent = 50;
    public static int secondPercentMultiplier = 2;
    public static int secondPercent = 75;
    public static int thirdPercentMultiplier = 3;
    public static int thirdPercent = 90;
    public static int finalPercentMultiplier = 4;
    public static int daysAfterPeriod = 7;
    public static int secondPeriodMultiplier = 2;
    public static int thirdPeriodMultiplier = 3;
    public static int finalPeriodMultiplier = 4;
    
    public static Properties loaderProps = null;
    public static boolean absolutePropPath = true;
    public static String propFileName = Repository.createPath(Repository.dash_special_directory,"multipliers.properties",absolutePropPath);
    
    public static void loadMultipliers() throws DashboardException {
        logger.info("Loading multipliers");
        loaderProps = Repository.readProperties(propFileName,absolutePropPath);
        try {
            firstPercent = Integer.parseInt(loaderProps.getProperty("first-percent"));
            secondPercentMultiplier = Integer.parseInt(loaderProps.getProperty("second-percent-multiplier"));
            secondPercent = Integer.parseInt(loaderProps.getProperty("second-percent"));
            thirdPercentMultiplier = Integer.parseInt(loaderProps.getProperty("third-percent-multiplier"));
            thirdPercent = Integer.parseInt(loaderProps.getProperty("third-percent"));
            finalPercentMultiplier = Integer.parseInt(loaderProps.getProperty("final-percent-multiplier"));
            daysAfterPeriod = Integer.parseInt(loaderProps.getProperty("days-after-period"));
            secondPeriodMultiplier = Integer.parseInt(loaderProps.getProperty("second-period-multiplier"));
            thirdPeriodMultiplier = Integer.parseInt(loaderProps.getProperty("third-period-multiplier"));
            finalPeriodMultiplier = Integer.parseInt(loaderProps.getProperty("final-period-multiplier"));
        } catch (NumberFormatException e) {
            throw new DashboardException(
                "NumberFormatException parsing multipliers file: " +
                e.getMessage());
        }
    }

    private String packageName = "";

    private String identifier = "";

    private String name = "";

    private Objective objective = Objective.NO_OBJECTIVE;

    private String lhs = "";

    private Milestone milestone = Milestone.NO_MILESTONE;

    private int horizon = 0;

    private Milestone sunsetMilestone = Milestone.NO_MILESTONE;

    private int sunsetHorizon = 0;

    private int weight = 1;

    private String owner = "";

    private String description = "";

    private Date lastUpdated;

    private boolean active = false;

    private String fullRuleText = "";

    private Counts counts = Counts.NOTHING;

    private String[] columns = new String[0];

    private String groupingVariable = "";

    private boolean groupingBased = false;

    private boolean changed = false;

    private boolean newlyCreated = false;

    private String oldId = null;

    private String attributes = "";

    private String rhs = "";

    private String agendaGroup = "";

    private String lede = "";

    /**
     * A Rule that applies an increasing penalty during the run-up to a
     * Milestone.
     *
     * @param name
     * @param lhs
     * @param milestoneName
     * @param horizon
     * @param owner
     * @param description
     * @param lastUpdated
     */
    public Rule(String identfier, String name, String objectiveName, String owner,
             String description, String columns, String lhs, String rhs,
             String milestoneName, int horizon, String sunsetMilestoneName,
             int sunsetHorizon, boolean active, int weight,
             String countType, String groupingVariable, String attributes,
             String agendaGroup, String lede, Date lastUpdated) {
        super();
        this.identifier = identfier;
        this.name = (null == name ? "" : name);
        this.objective = Objective.valueOf(objectiveName);
        this.owner = (null == owner ? "" : owner);
        this.description = canonicalizeNewlines(description);
        this.lede = (null == lede ? "" : lede);
        if (null == columns || columns.length() == 0) {
            this.columns = new String[0];
        } else {
            this.columns = columns.split(COL_SPLIT_REGEX);
        }
        this.attributes = canonicalizeNewlines(attributes);
        this.agendaGroup = (null == agendaGroup ? "" : agendaGroup.trim());
        this.lhs = canonicalizeNewlines(lhs);
        this.rhs = canonicalizeNewlines(rhs);
        this.milestone = Milestone.valueOf(milestoneName);
        this.horizon = horizon;
        this.sunsetMilestone = Milestone.valueOf(sunsetMilestoneName);
        this.sunsetHorizon = sunsetHorizon;
        if (milestone.isABigWindow() && (horizon != 0 || sunsetHorizon != 0 ||
            sunsetMilestone != Milestone.NO_MILESTONE)) {
            logger.error("Rule '" + name + "' has a bigWindow Milestone, and " +
                "a non-zero horizon or sunsetHorizon, or a sunsetMilestone. " +
                "Milestone trumps, and other values overruled.");
            this.horizon = 0;
            this.sunsetHorizon = 0;
            this.sunsetMilestone = Milestone.NO_MILESTONE;
        }
        if (sunsetMilestone.isABigWindow &&
                sunsetMilestone != Milestone.NO_MILESTONE) {
            logger.error("Rule '" + name + "' has a bigWindow sunsetMilestone " +
                "other than NO_MILESTONE.");
            this.sunsetMilestone = Milestone.NO_MILESTONE;
        }
        this.active = active;
        this.weight = weight;
        this.counts = Counts.valueOf(countType);
        if (null != groupingVariable && groupingVariable.trim().length() > 0) {
            this.groupingBased = true;
            this.groupingVariable = groupingVariable.trim();
        } else {
            this.groupingVariable = "";
        }
        this.lastUpdated = lastUpdated;
        generateRuleText();
    }

    /**
     * Bean-style constructor for create/edit use.
     */
    public Rule() {
        logger.debug("Creating semi-empty Rule");
        lastUpdated = new Date();
        newlyCreated = true;
    }

    /**
     * Put together the various fields and generate a complete drools rule.
     */
    private void generateRuleText() {
        packageName = PACKAGE_BASE + identifier.replaceAll("\\W", "");
        StringBuilder sb = new StringBuilder(200);
        sb.append("package ");
        sb.append(packageName);
        sb.append(";\n\n");
        sb.append(RULE_IMPORTS);
        for (String imp : imports) {
            sb.append(imp);
        }
        sb.append("\nrule \"");
        sb.append(name);
        sb.append("\"\n");
        if (null != agendaGroup && agendaGroup.length() > 0) {
            /* A bookkeeping rule. */
            sb.append(" agenda-group \"");
            sb.append(agendaGroup);
            sb.append("\"\n");

            /* And while we're at it, make a note of this agendaGroup. */
            agendaGroups.add(agendaGroup);
        }
        if (null != attributes && attributes.length() > 0) {
            sb.append(" ");
            sb.append(attributes);
            sb.append("\n");
        }
        sb.append("when\n");
        sb.append(" ");
        sb.append(lhs);
        sb.append("\nthen\n");
        if (null != rhs && rhs.length() > 0) {
            // Rule comes with its own RHS
            sb.append(" ");
            sb.append(rhs);
        } else {
            // Rule needs the stock RHS
            sb.append(" $user.setScore(\"");
            sb.append(identifier);
            sb.append("\", \"default");
            sb.append("\", $record_list");
            if (groupingBased) {
                sb.append(", $");
                sb.append(groupingVariable);
            }
            sb.append(");");
        }
        sb.append("\nend");
        fullRuleText = sb.toString().trim();
        // print out full rule text if needed
        //logger.debug(sb);
        if (lede.length() == 0) {
            // No lede, generate one from description.
            lede = description.length() > 60 ?
                description.substring(0, 61) + "..." : description;
        }
    }

    /**
     * @return The number of days remaining before this rule's milestone for
     * the given release, rounded to the nearest int.
     */
    public int daysUntil(Record release) {
        return Math.round(milestone.daysUntil(release));
    }

    /**
     * @param grouping A release to match milestones against.
     * @return The score multiplier based on the number of days remaining until
     * the milestone is reached, and the sunset time.
     */
    public int multiplier(Record grouping) {
        if (milestone.isABigWindow()) {
            return milestone.isActive(grouping) ? 1 : 0;
        }
        float daysUntil = daysUntil(grouping);
        
        if (isExceptionGroup(grouping)){
        	return 0;
        }
        /* Removing null date milestones from score calculation
         * anything beyond/after 24 months(24*30 = 720 days) is
         * removed from score calculation. Negative sign is because
         * daysuntil is calulated as milestone date - now
         * So when milestone date is not filled then it become 0 - now
         * which is not a valid value for score calcualtion
         */
        if(daysUntil < -720) {
              return 0;
        }

        if (daysUntil > horizon) {
            // We haven't crossed the horizon yet.
            if (logger.isTraceEnabled()) {
                logger.trace("Haven't crossed the threshold.");
            }
            return 0;
        }
        // See if we're past the sunset time for this rule
        if (! sunsetMilestone.isABigWindow &&
                Math.round(sunsetMilestone.daysUntil(grouping)) < sunsetHorizon) {
            return 0;
        }

        /* We're past the horizon, and not yet to sunset.  Now we need to see
         * what kind of horizon it is...
         * For rules that fire *after* a milestone, we bump the multiplier
         * every week.  For rules that count down, we bump at 50%, 75% & 90% */
        if (horizon > 0) {
            float percent = ((horizon - daysUntil) / horizon) * 100;
            if (logger.isTraceEnabled()) {
                logger.trace("Over the horizon by " + percent + "%");
            }
            if (percent < firstPercent) {
                return 1;
            } else if (percent < secondPercent) {
                return secondPercentMultiplier;
            } else if (percent < thirdPercent) {
                return thirdPercentMultiplier;
            } else {
                return finalPercentMultiplier;
            }
        } else {
            // switch on number of weeks since we passed the horizon
            if (logger.isTraceEnabled()) {
                logger.trace("Past the horizon by " +
                    Math.abs(daysUntil - horizon) + " days");
            }
            switch ((int) Math.floor(Math.abs((daysUntil - horizon) /
                daysAfterPeriod))) {
            case 0:
                return 1;
            case 1:
                return secondPeriodMultiplier;
            case 2:
                return thirdPeriodMultiplier;
            default:
                return finalPeriodMultiplier;
            }
        }
    }

    /**
     * Some certain milestones is not applicable anymore in CDM, so until
     * there are new milestones confirmation we just aggresively make the
     * score to 0
     * 
     * @return True if the rules is one of the "wrong" milestones in CDM
     */
    private boolean isExceptionGroup(Record grouping){
    	return milestone.isExceptionCDM(grouping);
    }
    
    /**
     * @return the lede.
     */
    public String getLede() {
        return lede ;
    }

    /**
     * @param attributes the attributes to set
     */
    public void setLede(String newLede) {
        if (null != newLede && ! lede.equals(newLede)) {
            this.lede= newLede.trim();
            changed = true;
        }
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @return the fullRuleText
     */
    public String getFullRuleText() {
        return fullRuleText;
    }

    public String getPackageName() {
        return packageName;
    }

    /**
     * @return the horizon
     */
    public int getHorizon() {
        return horizon;
    }

    /**
     * @return the sunsetHorizon
     */
    public int getSunsetHorizon() {
        return sunsetHorizon;
    }

    /**
     * @return the lastUpdated
     */
    public Date getLastUpdated() {
        return lastUpdated;
    }

    /**
     * @return the lhs
     */
    public String getLhs() {
        return lhs;
    }

    /**
     * @return the milestone
     */
    public Milestone getMilestone() {
        return milestone;
    }

    /**
     * @return the sunsetMilestone
     */
    public Milestone getSunsetMilestone() {
        return sunsetMilestone;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @return the owner
     */
    public String getOwner() {
        return owner;
    }


    /**
     * @return the identifier
     */
    public String getIdentifier() {
        return identifier;
    }

    /**
     * @return the active
     */
    public boolean isActive() {
        return active;
    }

    /**
     * Rules sort by counts, objective, milestone, horizon and identifier.
     */
    public int compareTo(Rule o) {
        int retval = counts.compareTo(o.counts);
        if (0 == retval) {
            retval = o.groupingVariable.compareTo(groupingVariable);
        }
        if (0 == retval) {
            retval = objective.compareTo(o.objective);
        }
        if (0 == retval) {
            retval = milestone.compareTo(o.milestone);
        }
        if (0 == retval) {
            retval = horizon - o.horizon;
        }
        if (0 == retval) {
            retval = identifier.compareTo(o.identifier);
        }
        return retval;
    }

    /**
     * @return the weight
     */
    public int getWeight() {
        return weight;
    }

    /* FIXME These should be rationalized so it's easier to navigate from
     * a rule to the relevant DataSource (esp. in the python layer).  */
    public static enum Counts {
        RLIS,
        BTS,
        NPIS,
        PRS,
        NEWLYINPRS,
        REVIEWS,
        CRSS,
        NOTHING,
        CLOSEDPRS,
        HRCLOSEPRS,
        REQS,
        ;
    }

    /**
     * @return What this Rule counts
     */
    public Counts getCounts() {
        return counts;
    }


    /**
     * @return the columns
     */
    public String[] getColumns() {
        return columns;
    }

    /**
     * @return the columns as a comma-delimited string.
     */
    public String getColumnsDelimited() {
        StringBuilder sb = new StringBuilder();
        for (String col : columns) {
            sb.append(col);
            sb.append(", ");
        }
        return sb.toString();
    }


    /**
     * @return the objective
     */
    public Objective getObjective() {
        return objective;
    }


    public boolean isGroupingBased() {
        return groupingBased;
    }

    public String getGroupingVariable() {
        return groupingVariable;
    }

    /**
     * @param identifier the identifier to set
     */
    public void setIdentifier(String i) {
        if (null != i && ! identifier.equals(i)) {
            // Tell the Repository that we've changed keys.
            oldId = identifier;
            this.identifier = i.trim();
            changed = true;
        }
    }

    /**
     * @param name the name to set
     */
    public void setName(String n) {
        if (null != n && ! name.equals(n)) {
            this.name = n.trim();
            changed = true;
        }
    }

    /**
     * @param objective the objective to set
     */
    public void setObjective(Objective o) {
        if (objective != o) {
            this.objective = o;
            changed = true;
        }
    }

    /**
     * @param lhs the lhs to set
     */
    public void setLhs(String newLhs) {
        newLhs = canonicalizeNewlines(newLhs);
        if (! lhs.equals(newLhs)) {
            this.lhs = newLhs;
            changed = true;
        }
    }

    /**
     * @return the rhs
     */
    public String getRhs() {
        return rhs;
    }

    /**
     * @param rhs the rhs to set
     */
    public void setRhs(String newRhs) {
        newRhs = canonicalizeNewlines(newRhs);
        if (! rhs.equals(newRhs)) {
            this.rhs = newRhs;
            changed = true;
        }
    }

    /**
     * @return the attributes
     */
    public String getAttributes() {
        return attributes;
    }

    /**
     * @param attributes the attributes to set
     */
    public void setAttributes(String newAttr) {
        newAttr = canonicalizeNewlines(newAttr);
        if (! attributes.equals(newAttr)) {
            this.attributes = newAttr;
            changed = true;
        }
    }

    /**
     * @return the agendaGroup
     */
    public String getAgendaGroup() {
        return agendaGroup;
    }

    /**
     * @param agendaGroup the agendaGroup to set
     */
    public void setAgendaGroup(String newAg) {
        if (null != newAg && ! agendaGroup.equals(newAg)) {
            this.agendaGroup = newAg.trim();
            changed = true;
        }
    }

    /**
     * @param milestone the milestone to set
     */
    public void setMilestone(Milestone m) {
        if (milestone != m) {
            this.milestone = m;
            changed = true;
        }
    }

    /**
     * @param sunsetMilestone the sunsetMilestone to set
     * @throws DashboardException
     */
    public void setSunsetMilestone(Milestone m) throws DashboardException {
        if (m.isABigWindow && m != Milestone.NO_MILESTONE) {
            throw new DashboardException(
                "Invalid sunsetMilestone, try \"Always Active\" (NO_MILESTONE).");
        }
        if (sunsetMilestone != m) {
            this.sunsetMilestone = m;
            changed = true;
        }
    }

    /**
     * @param weight the weight to set
     * @throws DashboardException
     */
    public void setWeight(int w) throws DashboardException {
        if (w < 0 || w > 100) {
            throw new DashboardException("Weight must be between 0 and 100.");
        }
        if (weight != w) {
            this.weight = w;
            changed = true;
        }
    }

    /**
     * @param owner the owner to set
     *
     */
    public void setOwner(String o) {
        if (null != o && ! owner.equals(o)) {
            this.owner = o.trim();
            changed = true;
        }
    }

    /**
     * @param description the description to set
     * @throws DashboardException
     */
    public void setDescription(String d) throws DashboardException {
        d = canonicalizeNewlines(d);
        if (d.length() == 0) {
            throw new DashboardException("You must supply a description.");
        }
        if (! description.equals(d)) {
            this.description = d;
            changed = true;
        }
    }

    /**
     * @param active the active to set
     */
    public void setActive(boolean a) {
        if (active != a) {
            this.active = a;
            changed = true;
        }
    }

    /**
     * @param counts the counts to set
     */
    public void setCounts(Counts c) {
        if (counts != c) {
            this.counts = c;
            changed = true;
        }
    }

    /**
     * @param columns the columns to set
     */
    public void setColumnsDelimited(String cols) {
        if (! getColumnsDelimited().equals(cols)) {
            this.columns = cols.split(COL_SPLIT_REGEX);
            changed = true;
        }
    }

    /**
     * @param groupingVariable the groupingVariable to set
     */
    public void setGroupingVariable(String gv) {
        if (!groupingVariable.equals(gv)) {
            this.groupingVariable = gv;
            changed = true;
            groupingBased = (null != gv && gv.length() > 0);
        }
    }

    /**
     * @param horizon the horizon to set
     */
    public void setHorizon(int h) {
        if (horizon != h) {
            this.horizon = h;
            changed = true;
        }
    }

    /**
     * @param sunsetHorizon the sunsetHorizon to set
     */
    public void setSunsetHorizon(int h) {
        if (sunsetHorizon != h) {
            this.sunsetHorizon = h;
            changed = true;
        }
    }

    /**
     * @return true if this Rule has been changed but not persisted to the db.
     */
    public boolean getChanged() {
        return changed;
    }

    public void processEdit() {
        if (changed) {
            if (null != oldId) {
                Repository.newIdForRule(this, oldId);
                oldId = null;
            }
            generateRuleText();
        }
        lastUpdated = new Date();
    }

    public String toString() {
        return identifier;
    }

    /**
     * @return true if this rule was created this session.
     */
    public boolean isNewlyCreated() {
        return newlyCreated;
    }

    /**
     * Call this when the rule has been persisted to the DB.
     */
    public void saved() {
        changed = false;
        newlyCreated = false;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
            + ((identifier == null) ? 0 : identifier.hashCode());
        result = prime * result + ((lhs == null) ? 0 : lhs.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        final Rule other = (Rule) obj;
        if (identifier == null) {
            if (other.identifier != null) return false;
        } else if (!identifier.equals(other.identifier)) return false;
        if (lhs == null) {
            if (other.lhs != null) return false;
        } else if (!lhs.equals(other.lhs)) return false;
        return true;
    }

    /**
     * @return An XML representation of this Rule.
     */
    public String toXML() {
        return getStreamer().toXML(this);
    }

    /**
     * Change to The One True Newline.
     *
     * This is important, because hashCode() and equals() use lhs, and
     * round-tripping through the web UI tends to bork the newlines,
     * causing invisible changes, which result in two copies of the Rule
     * being "different" as far as HashMap is concerned.
     */
    private String canonicalizeNewlines(String in) {
        return (null == in ? "" :
            in.replaceAll("(\\n|\\r\\n|\\r)", "\n").trim());
    }
}
