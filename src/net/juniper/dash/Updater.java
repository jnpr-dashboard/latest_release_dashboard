/**
 * dbergstr@juniper.net, Jan 16, 2007
 *
 * Copyright (c) 2007, Juniper Networks, Inc.
 * All rights reserved.
 */
package net.juniper.dash;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.Calendar;
import java.util.GregorianCalendar;

import net.juniper.dash.data.DataSource;

import org.apache.log4j.Logger;
import org.drools.FactException;
import org.drools.FactHandle;
import org.drools.WorkingMemory;

/**
 * Handles boring repetetive tasks.
 *
 * @author dbergstr
 */
public final class Updater {

    protected static final Logger logger =
        Logger.getLogger(Updater.class.getName());

    private StatusProcessor statusProcessor;


    private FactHandle nowHandle;

    private List<DataSourceProcessor> dsProcessors;

    private CyclicBarrier barrier;
    private Watchdog watchdog;

    private Refresher sleeper;
    
    public Updater() {
        watchdog = new Watchdog();
        // rabbit goes into threads down this hole
        // Watchdog is a subclass of Refresher which is a subclass of 
        // Thread.  Go look at Refresher.run() to see what happens next
        // which will then call .work on Watchdog
        watchdog.start();
        Repository.resetStatusReportCycle();
        // at this point, the rabbit is running in threads
        // and the struts contextInitialized will complete
    }

    private void processDataSources() {
    	// this will run after all of the data processors have finished
        statusProcessor = new StatusProcessor("Status Processor");

        Collection<DataSource> sources = DataSource.sources();
        // will call statusProcessor when sources.size() (all of the 
        // data processing) threads have finished
        // each data processor will call barrier.await() when it's done
        // and every sources.size() times that is called, statusProcessor
        // will be called and then the datasources will continue to process again
        barrier = new CyclicBarrier(sources.size(), statusProcessor);
        dsProcessors = new ArrayList<DataSourceProcessor>();
        // start the threads for all of the data sources
        for (DataSource source : sources) {
            DataSourceProcessor dsp =
            	// send in the source and the barrier 
                new DataSourceProcessor(source, barrier);
            dsProcessors.add(dsp);
            dsp.start();
        }
    }

    /**
     * Wake up whatever thread is currently the StatusProcessor.
     *
     * FIXME this doesn't work, and I can't see why.
     */
    public void wakeUp() {
        if (null != sleeper) {
            sleeper.jolt();
        }
    }

    /**
     * Shut down all running workers.
     * @throws DashboardException
     */
    public void shutdown() {
        for (DataSourceProcessor dsp : dsProcessors) {
            dsp.kill();
        }
        watchdog.kill();
    }

    /**
     * Restart processing after errors.
     *
     * FIXME Need to implement handleException here.  Should prob. make it abstract...
     */
    private class Watchdog extends Refresher {

        public Watchdog() {
            super("Watchdog");
        }

        public void work() {
        	// all of the datasource threads are created here along with the
        	// Cyclic barrier stuff.
        	// the rabbit goes down here and each datasource thread is created        	
            processDataSources();            
            checkIfKilled();

            int numUpdateCycles = Repository.getUpdateCycles();
            boolean updatesStalled = false;

            /* Now loop until we see a problem.
             * The only other way out of here is for the thread to be killed. */
            // this is just checking for errors.  if there is an error, it will clean things up
            // here and then .start() from Rfresher will catch the error and run .work() again
            while (! barrier.isBroken()) {
                nap(SleepyTimes.UPDATER_WATCHDOG_SLEEP.t());
                /* This gets us an hourly timestamp in catalina.out, which
                 * helps us understand when things happened. */
                System.out.println(new Date());

                // Make sure that the updates haven't hung up in some way
                if (Repository.getUpdateCycles() == numUpdateCycles) {
                    // No updates since last go-round, could be bad...
                    if (updatesStalled) {
                        /* Two cycles without an update.  Break out of the loop,
                         * and restart the updaters. */
                        break;
                    }
                    /* Log an error, and note that we've gone one cycle without
                     * an update. */
                    Repository.logError("No updates since last watchdog check.");
                    updatesStalled = true;
                } else {
                    updatesStalled = false;
                    numUpdateCycles = Repository.getUpdateCycles();
                }

                /* Check to make sure that we're still connected to the DB. */
                if (! Repository.testDatabaseConnection()) {
                    try {
                        Repository.setupConnectionPool();
                    } catch (DashboardException e) {
                        String errmsg = "Database Connection died, exception " +
                        "recreating it: "+ e.getMessage();
                        Repository.logError(errmsg);
                        logger.error(errmsg);
                    }
                }
            }

            String errmsg;
            if (barrier.isBroken()) {
                errmsg = "Barrier broken, killing threads and attempting to " +
                    "restart processing.";
            } else {
                errmsg = "Updates stalled for two watchdog cycles, killing " +
                "threads and attempting to restart processing.";
            }
            Repository.logError(errmsg);
            logger.error(errmsg);

            /* Kill the processors and start over.
             * Some or all of them may already have quit on their own. */
            for (DataSourceProcessor dsp : dsProcessors) {
                dsp.kill();
            }
            statusProcessor.kill();
            nap(SleepyTimes.WAIT_FOR_THREADS_TO_DIE.t());
            for (DataSourceProcessor dsp : dsProcessors) {
                if (dsp.isAlive()) {
                    logger.warn(dsp.getName() + " did not honor kill() yet.");
                }
            }
            checkIfKilled();
        }
    }

    /**
     * Fire rules and process the results, take care of other periodic
     * maintenance tasks.
     */
    private class StatusProcessor extends Refresher {

        public StatusProcessor(String name) {
            super(name);
        }

        public void work() {
            /* Assert a new "now" into the WorkingMemory, to update any
             * time-based rules. */
            if (null != nowHandle) {
                Repository.workingMemory.retract(nowHandle);
            }
            nowHandle = Repository.workingMemory.insert(new Date());
            // while the data has been updated, drools will make some notes on what should
            // be updated, but it will not actually do any updates until here which will 
            // eventuallty call setScore on User
            fireRules(Repository.workingMemory);
            // this will always write everything but users
            // the first time through, it will only write out managers, every other time it will
            // write out all users
            Repository.writeJson(true);
            checkIfKilled();
            Repository.noteUpdateCycle();
            // this is done because the code tried to do just
            // managers first
            if(Repository.inStartUp) {
                /* Finish up the assertion of Users.
                 * We're only going to attempt this once.  If there's a problem
                 * in this method, the remaining users will still be in notAsserted,
                 * and they'll get dealt with in UserSource.populateRecords() later. */
                DataSource.USERSOURCE.finishStagedAssertion(this);
                Repository.inStartUp = false;
            } else if (Repository.updatePauseMinutes != 0) {
                long time = Repository.updatePauseMinutes * 60 * 1000;
                Repository.updatePauseMinutes = 0;
                // take note of which thread is sleeping.
                sleeper = (Refresher) currentThread();
                logger.info("Wait for next update on " + time + " ms");
                nap(time);
            } else {
                // take note of which thread is sleeping.
                sleeper = (Refresher) currentThread();
                logger.info("Wait for next update on " + SleepyTimes.WAIT_BEFORE_DATA_REFRESH.t() + " ms");
                // Nap before repeating the cycle
                nap(SleepyTimes.WAIT_BEFORE_DATA_REFRESH.t());
            }

            /* Refresh all the Groups */
            // update the LDAP information
            for (Group g : Repository.groups()) {
                try {
                    g.refresh();
                    checkIfKilled();
                } catch (DashboardException e) {
                    logger.error("Exception refreshing group '" + g.getName() +
                        "': " + e.getMessage());
                }
            }
            // And we're done.
            done = true;
        }

        @Override
        protected void handleException(Exception e) {
            logger.error(getName() + " caught exception: " +
                e.getMessage() + "; Skipping remaining processing.", e);
            done = true;
        }
    }

    /**
     * A thread for running DataSource updates.
     */
    private static class DataSourceProcessor extends Refresher {

        private DataSource dataSource;
        private CyclicBarrier dspBarrier;

        public DataSourceProcessor(DataSource dataSource,
                CyclicBarrier barrier) {
            super(dataSource.getName() + " Processor");
            this.dataSource = dataSource;
            this.dspBarrier = barrier;
        }

        public void work() {
            try {
    			dataSource.populateRecords(this);
            } catch (DashboardException e) {
                String errmsg = getName() + " giving up record population " +
                    " because of exception: " + e.getMessage();
                Repository.logError(errmsg);
                logger.error(errmsg);
            } catch (TimeoutException e) {
            	// consider this as error, so we can keep track of it in error-log.
                logger.error(getName() + " giving up record population " +
                    "because of data read timeout.");
            } 

            waitAtBarrier();
        }

        @Override
        protected void handleException(Exception e) {
            String errmsg = getName() + " giving up because of exception: " +
                e.getMessage();
            Repository.logError(errmsg);
            logger.error(errmsg, e);
            waitAtBarrier();
        }

        private void waitAtBarrier() {
            /* Wait for everyone else to finish, end thread if the barrier
             * breaks. */
            try {
                if (logger.isDebugEnabled()) {
                    logger.debug(getName() + " waiting at barrier.");
                }
                dspBarrier.await();
            } catch (InterruptedException e) {
                logger.error(getName() +
                    "Caught InterrupedException while waiting at barrier.");
                killed = true;
            } catch (BrokenBarrierException e) {
                String errmsg = getName() +
                    "Caught BrokenBarrierException while waiting at barrier: " +
                    e.getMessage();
                Repository.logError(errmsg);
                logger.error(errmsg);
                killed = true;
            }
        }
    }

    /**
     * Sets the agenda focus stack, fires all rules, and updates User status.
     */
    public static void fireRules(WorkingMemory wm) {
        logger.info("Firing all rules.");
        try {
            Repository.refreshCache();	
            /* Put all the agendas onto the stack, in reverse order. */
            for (String agenda : Rule.getAgendaGroups()) {
                wm.setFocus(agenda);
            }
        	wm.fireAllRules();
            synchronized (Repository.needToCullRules) {
                // Finalize User scores.
            	// this is where the scores are REALLY calculated
                DataSource.USERSOURCE.processUserStatuses(new Date());
                Repository.needToCullRules = Boolean.FALSE;
            }
        }
        catch (FactException fe) {
            String errmsg = "Exception processing rules: " + fe.getMessage();
            Repository.logError(errmsg);
            logger.error(errmsg);
        }
    }
}

