/**
 * dbergstr@juniper.net, Oct 19, 2006
 *
 * Copyright (c) 2006, Juniper Networks, Inc.
 * All rights reserved.
 */
package net.juniper.dash;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attributes;
import javax.naming.directory.BasicAttribute;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;

import net.juniper.dash.data.DataSource;
import net.juniper.dash.data.PRRecord;
import net.juniper.dash.data.User;
import oracle.jdbc.pool.OracleConnectionCacheManager;
import oracle.jdbc.pool.OracleDataSource;

import org.apache.log4j.Logger;
import org.drools.WorkingMemory;

import flexjson.JSONSerializer;

/**
 * Holds application-wide lists of RLIs, releases, etc.
 *
 * Applying Security Fix for hanging fork (so we dont needd kill-kids.sh, since
 * that won't work well on virtual environment)
 * http://wiki.hudson-ci.org/display/HUDSON/Solaris+Issue+6276483
 *
 * On Unix there are other Issues, since we are relying on unix script to get records
 * http://forums.sun.com/thread.jspa?threadID=5187941
 * http://www.mjmwired.net/kernel/Documentation/vm/overcommit-accounting
 *
 * @author dbergstr
 */
public class Repository {

    static Logger logger = Logger.getLogger(Repository.class.getName());

    public static final String VERSION = "5.6";
    public static final String RELEASE_DATE = "2010-11-1";

    public static final String BIN_DIR =
        Repository.class.getResource("/bin").getPath();
    public static final String FIELD_SEPARATOR = "\037";
    public static final String RECORD_SEPARATOR = "\036";
    public static Date lastErrorDate;
    public static WorkingMemory workingMemory;
    /**
     * If a Rule has been deactivated or deleted, set this to signal the
     * score finalization process to weed out the Rule.
     */
    public static Boolean needToCullRules = Boolean.FALSE;
    /**
     * If this is true, DataSources will run record queries that fetch a subset
     * of available records, which will greatly increase startup speed.
     */
    public static Boolean testingMode;
    /**
     * If true, JSON for Users will be pretty-printed.  Larger, but human-readable.
     */
    public static boolean prettyPrint;
    /**
     * The file holding database connection parameters.
     */
    public static String dbConnInfoFile ;
    public static boolean inStartUp = true;
    public static int updatePauseMinutes = 0;
    /**
     * The path that the main (Python) UI is running on.  Used to build URLs
     * in the admin pages.
     */
    public static String uiBasePath;
    /**
     * Configuration File
     */
    public static String dbname = "rules";
    // override DASH_SPECIAL_DIRECTORY in <approot>/tomcat/bin/setenv.sh
    // if you don't want it to be relitive.  This is the defualt if that
    // varibale isn't set for some reason.  see how this is changed in
    // initialize below
    public static String dash_special_directory = "/opt/dashboard/special/";
    public static String dash_configuration_files = "dash.properties";
    /**
     * PR Bug fix Configuration Settings
     */
    public static String PR_fix_releases;
    public static String PR_fix_rules_openPR;
    public static String PR_fix_rules_closedPR;


    private static OracleDataSource ods;
    private static OracleConnectionCacheManager occm;
    private static String cacheName;
    private static ConcurrentMap<String, Rule> rules =
        new ConcurrentHashMap<String, Rule>();
    private static SortedSet<Refresher> refreshers = new TreeSet<Refresher>();
    private static Properties ldapProps;
    private static String ldapFileName;
    private static Properties appProps;
    private static String appFileName;
    /**
     * Used to disambiguate between possible multiple instances of the
     * Dashboard in a single container (servlet engine).
     */
    private static String appIdentifier = "UNKNOWN";
    private static Map<Group.Groups, Group> groups =
        new HashMap<Group.Groups, Group>();
    private static Ruler ruler;
    private static Updater updater;
    private static int updateCycles;
    private static long lastUpdateTime = 0;
    private static long totalUpdateCycleTime = 0;
    private static long[] recentUpdateTimes = new long[10];
    private static MotD motd = new MotD();
    private static Map<String, Integer> errors = new HashMap<String, Integer>();
    private static Map<String, Integer> newErrors = new HashMap<String, Integer>();
    /**
     * If this is non-zero, it overrides the default value of
     * SleepyTimes.WAIT_BEFORE_DATA_REFRESH.
     */
    private static int dataRefreshMinutes;

    /**
     * The directory where JSON will be written.
     */
    private static String jsonPath;
    private static String jsonReadyUrl;
    private static int StatusReportCycle;
    /**
     * counter of separation File
     */
    private static int EveryoneElseCount = 0;
    private static int ManagerCount = 0;

    private static int aliasesMapRefreshDay;    
    public static Map<String, Set<String>> userAliasMap = new HashMap<String, Set<String>>();
    public static Map<String, Set<String>> aliasUserMap = new HashMap<String, Set<String>>();

    public static String createPath(String path1, String path2, boolean absolute) {
        if (absolute) {
            return new File(path1, path2).getAbsolutePath();
        } else {
            return new File(path1, path2).getPath();
        }
    }

    /**
     * Initialize the application.
     * @param identifier
     *
     * @throws DashboardException
     */
    public static void initialize(String identifier) throws DashboardException {
        //Logger.getRootLogger().setLevel(Level.toLevel("TRACE"));

        // initialize a bunch of stuff.  no real processing done here, just setup
        // set the special directory
        String special_dir;
        special_dir = System.getenv("DASH_SPECIAL_DIRECTORY");
        if (!special_dir.equals("") && !(special_dir == null)) {
            dash_special_directory = special_dir;
        }
        logger.info("using special directory " + dash_special_directory);
        appIdentifier = identifier;
        logger.info("Initializing repository with identifier '" +
                    appIdentifier + "'.");

        loadLDAPproperties();
        loadApplicationProperties();

        setupConnectionPool();

        // need to setup the correct datasource we need on this engine
        DataSource.init_DataSource();

        // Add each datasource to the Rule imports list.
        for (DataSource source : DataSource.sources()) {
            Rule.addImport("net.juniper.dash.data." + source.getRecordType());
        }

        /* initialize and load the Groups */
        initGroups();
        
        refreshCache(); 

        // rabbit goes down this hole
        startProcessing();

        lastUpdateTime = System.currentTimeMillis();

        logger.info("Repository initialized.");
    }

    /**
     * Also used in administration to reflect changes
     * @throws DashboardException
     */
    public static void loadLDAPproperties() throws DashboardException {
        ldapFileName = Repository.createPath(Repository.dash_special_directory, "ldap.properties", true);
        ldapProps = readProperties(ldapFileName, true);
    }

    /**
     * Also used in administration to reflect changes
     * @throws DashboardException
     */
    public static void loadApplicationProperties() throws DashboardException {
        appFileName = Repository.createPath(Repository.dash_special_directory, Repository.dash_configuration_files, true);
        appProps = readProperties( appFileName , true);
        testingMode =
            appProps.getProperty("testing-mode", "no").equalsIgnoreCase("yes");
        String drm = appProps.getProperty("data-refresh-minutes", "0");
        try {
            dataRefreshMinutes = Integer.parseInt(drm);
        } catch (NumberFormatException e) {
            // We don't care if it doesn't parse
        }
        if (dataRefreshMinutes > 0) {
            SleepyTimes.setDataRefreshMinutes(dataRefreshMinutes);
        }
        prettyPrint =
            appProps.getProperty("pretty-print", "no").equalsIgnoreCase("yes");
        jsonPath =
            appProps.getProperty("json-path", "/opt/dashboard/json");
        jsonReadyUrl = appProps.getProperty("json-ready-url",
                                            "http://localhost/dashboard/json-ready/");
        uiBasePath = appProps.getProperty("ui-base-path", "/dashboard");
        dbConnInfoFile = appProps.getProperty("db-conn-info-file",
                                              "/opt/dashboard/special/dash-db.properties");
        // Database we are using - agenda or Reporting
        Repository.dbname = appProps.getProperty("dbname", "rules");

        // PR bug fix related Configuration
        PR_fix_releases = appProps.getProperty("pr-fix-releases", "10.3,10.4,11.1,11.2");
        PR_fix_rules_openPR = appProps.getProperty("pr-fix-rules-openpr", "regression_prs_fix,major_prs_fix,critical_prs_fix");
        PR_fix_rules_closedPR = appProps.getProperty("pr-fix-rules-closedpr", "closed_regression_prs,major_closed_pr,closed_critical_prs");

    }

    // User methods

    public static User getUser(String name) {
        return (User) DataSource.USERSOURCE.getRecord(name);
    }

    public static SortedSet<User> getAllUsers() {
        return DataSource.USERSOURCE.allUsers();
    }

    // Database methods

    public static void setupConnectionPool() throws DashboardException {
        Properties dbProps = readProperties("/db.properties", false);
        Properties connProps = readProperties(dbConnInfoFile, true);
        logger.trace("db.properties read and parsed.");

        try {
            // create a DataSource
            ods = new OracleDataSource();
            // set DataSource properties
            ods.setTNSEntryName(connProps.getProperty("tns_entry"));
            ods.setDriverType(connProps.getProperty("driver_type"));
            ods.setUser(connProps.getProperty("user"));
            ods.setPassword(connProps.getProperty("password"));
            if ("yes".equals(dbProps.getProperty("debug"))) {
                // This produces a stupefying amount of output...
                logger.warn("Running with Oracle debugging to System.out.");
                ods.setLogWriter(new java.io.PrintWriter(System.out));
            }
            ods.setConnectionCachingEnabled(true);

            // set cache properties
            Properties odsProps = new Properties();
            odsProps.setProperty("MinLimit", dbProps.getProperty("MinLimit"));
            odsProps.setProperty("MaxLimit", dbProps.getProperty("MaxLimit"));
            odsProps.setProperty("InactivityTimeout",
                                 dbProps.getProperty("InactivityTimeout"));
            odsProps.setProperty("PropertyCheckInterval",
                                 dbProps.getProperty("PropertyCheckInterval"));
            ods.setConnectionCacheProperties(odsProps);

            /* Make sure we don't run into trouble with a stale connection
             * cache left over from a previous run. */
            occm =
                OracleConnectionCacheManager.getConnectionCacheManagerInstance();
            cacheName = "DashboardCache-" + appIdentifier;
            if (occm.existsCache(cacheName)) {
                logger.warn("Found existing cache under name '" + cacheName +
                            "', removing it.");
                occm.removeCache(cacheName, 0);
            }
            logger.debug("Creating ConnectionCache '" + cacheName + "'.");
            occm.createCache(cacheName, ods, dbProps);

            /* Statement caching will hopefully provide a performance boost */
            ods.setImplicitCachingEnabled(true);
            logger.debug("ods created and configured");

            /* We need to create a connection to create the cache, and in
             * the process we'll check to see that things are working. */
            if (! testDatabaseConnection()) {
                throw new DashboardException("Exception testing new DB connection pool.");
            }
            logger.debug("connection tested");
        } catch (SQLException sqle) {
            logger.fatal("Exception initializing DB connection pool.", sqle);
            throw new DashboardException("Can't setup DB connection pool.");
        }
    }

    /**
     * Returns true if the database connection is still good.
     */
    public static boolean testDatabaseConnection() {
        Connection conn = null;
        Statement stmt = null;
        try {
            conn = getConnection();
            stmt = conn.createStatement();
            /* Run a query to verify that we're live. */
            stmt.executeQuery("select user from dual");
            return true;
        } catch (SQLException e) {
            return false;
        } finally {
            closeConnStmtRset(conn, stmt, null);
        }
    }

    public static OracleDataSource getOds() {
        return ods;
    }

    public static Connection getConnection() throws SQLException {
        return ods.getConnection();
    }

    public static String getConnectionCacheStats() {
        try {
            StringBuilder stats = new StringBuilder();
            stats.append("Available: ");
            stats.append(occm.getNumberOfAvailableConnections(cacheName));
            stats.append("; Active: ");
            stats.append(occm.getNumberOfActiveConnections(cacheName));
            stats.append("; Viable: ");
            stats.append(testDatabaseConnection());
            return stats.toString();
        } catch (SQLException e) {
            return "Exception generating conncache stats:" + e.getMessage();
        }
    }

    /**
     * Close up shop after a database transaction.
     * Any of the supplied args can be null.
     *
     * @param conn
     * @param stmt
     * @param rset
     */
    public static void closeConnStmtRset(Connection conn, Statement stmt,
                                         ResultSet rset) {
        if (rset != null) {
            try {
                rset.close();
            } catch (SQLException e) {
                logger.warn("Failed to close result set:" + e.getMessage());
            }
        }
        if (stmt != null) {
            try {
                stmt.close();
            } catch (SQLException e) {
                logger.warn("Failed to close statement:" + e.getMessage());
            }
        }
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException e) {
                logger.warn("Failed to close connection:" + e.getMessage());
            }
        }
    }

    /**
     * Write a json file to disk.
     */
    private static void writeJsonFile(String fileName, String json) {
        String fullPath = jsonPath + File.separatorChar + fileName + ".json";
        try {
            BufferedWriter out = new BufferedWriter(new FileWriter(fullPath));
            out.write(json);
            out.close();
        } catch (IOException e) {
            String errmsg = "Error writing JSON file \"" + fileName + "\": " +
                            e.getMessage();
            logger.error(errmsg);
            logError(errmsg);
        }
    }

    /**
     * Write out JSON for all the info needed by the UI layer.
     *
     * @param informUI If true, inform the UI layer that new JSON is available.
     */
    public static void writeJson(boolean informUI) {
        logger.info("Writing updated JSON to disk");
        logger.debug("Writing JSON for records and summaries");
        writeJsonFile("release", DataSource.RELEASES.serializeRecords());
        writeJsonFile("npi", DataSource.NPI.serializeRecords());
        writeJsonFile("pr", DataSource.GNATS.getSummaries());
        writeJsonFile("rli", DataSource.RLI.getSummaries());
        writeJsonFile("newlyinpr", DataSource.NEWLYINPRFIX.getSummaries());
        writeJsonFile("bt", DataSource.BRANCHTRACKER.getSummaries());
        writeJsonFile("review", DataSource.REVIEWTRACKER.getSummaries());
        writeJsonFile("crs", DataSource.CRSTRACKER.getSummaries());
        writeJsonFile("history-pr", DataSource.HISTORYGNATS.getSummaries());
        writeJsonFile("hardening", DataSource.HARDENING.getSummaries());
        writeJsonFile("hardeningclosed", DataSource.HARDENINGCLOSED.getSummaries());
        // ==PPM==.... UNCOMMENT LINE BELOW FOR PPM PROJECT
//        writeJsonFile("req", DataSource.REQUIREMENTS.getSummaries());

        logger.debug("Writing static JSON");
        writeJsonFile("rules", Rule.serializeAll());
        writeJsonFile("milestones", Milestone.serializeAll());
        writeJsonFile("sources", DataSource.serializeAll());
        writeJsonFile("objectives", Objective.serializeAll());

        // Writing Manager User Object to JSON file
        writeManager();

        /* Managers are asserted in the initial startup of UserSource, the
         * rest of the Users are asserted by UserSource.finishStagedAssertion() */
        if (! inStartUp) {
            writePseudos();
            writeVirtualTeams();
            writeEveryoneElse();
        }
        /*
         * Some of EveryoneElse information will be written here
         */
        writeMisc();
        if (informUI) informUI();
    }

    public static void informUI() {
        logger.info("Informing UI layer of JSON updates");
        try {
            String data = URLEncoder.encode("time", "UTF-8") + "=" +
                          URLEncoder.encode(Long.toString(lastUpdateTime), "UTF-8");
            URL url = new URL(jsonReadyUrl);
            URLConnection conn = url.openConnection();
            conn.setDoOutput(true);
            OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
            wr.write(data);
            wr.flush();

            BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String line;
            while ((line = rd.readLine()) != null) {
                if (line.contains("error")) {
                    String errmsg = "UI had problems on JSON update: " + line;
                    logger.error(errmsg);
                    logError(errmsg);
                }
            }
            wr.close();
            rd.close();
        } catch (IOException e) {
            String errmsg = "Error informing UI of new JSON available: " +
                            e.getMessage();
            logger.error(errmsg);
            logError(errmsg);
        }
    }

    private static void writeManager() {
        // Growth of manager object will eventually leads to serialized object
        // too big to fit in JVM. So using the same static separation in
        // writeEveryoneElse, the problems can be minimized
        int manager_scored = 0;
        ManagerCount = 0; // instantiate / reset the counter
        int ManagerSize = 250;
        Set<String> mgrs = new HashSet<String>();
        for (User u: DataSource.USERSOURCE.allUsers()) {
            if (u.isAManager()) {
                mgrs.add(u.getId());
                manager_scored++;
            }
            if (manager_scored > ManagerSize) {
                logger.debug("Writing JSON for all-managers" + (ManagerCount > 0 ? ManagerCount : ""));
                writeJsonFile("all-managers" + (ManagerCount > 0 ? ManagerCount : ""), DataSource.USERSOURCE.serialize(mgrs));
                ManagerCount++;
                // clear out the object and start clean
                mgrs.clear();
                // clear the counter
                manager_scored = 0;
            }
        }
        // write the rest of the object that still left in the Set
        // OR the last one... JUNOS object
        mgrs.add(User.JUNOS.getId());
        logger.debug("Writing JSON for all-managers" + (ManagerCount > 0 ? ManagerCount : ""));
        writeJsonFile("all-managers" + (ManagerCount > 0 ? ManagerCount : ""), DataSource.USERSOURCE.serialize(mgrs));
        mgrs = null;
    }

    public static void writeEveryoneElse() {
        // On all procedure of writing JSON, if the serialize object is too big
        // heap memory in JVM will not be enough. Ideally, we need to calculate
        // the memory size of the objects and decide whether it needs separation.
        // To calculate the memory size of the objects, gc() is needed; that steps
        // will slow down the JVM even more. So use more static separation...
        Set<String> users = new HashSet<String>();
        int user_scored = 0;
        EveryoneElseCount = 0; // instantiate / reset the counter
        int USER_SIZE = 500; // we assume that this size is what our heap size can handle safely
        for (User u: DataSource.USERSOURCE.allUsers()) {
            if (! (u.isVirtualTeam() || u.isAManager() || u.isPseudoUser())) {
                if (u.isScored()) {
                    // if the user is not scored yet, then their object size is not
                    // too big
                    user_scored++;
                }
                users.add(u.getId());
                // check for memory usage , need to separate or divide
                if (user_scored > USER_SIZE) {
                    logger.debug("Writing JSON for everyone-else" + (EveryoneElseCount > 0 ? EveryoneElseCount : ""));
                    writeJsonFile("everyone-else" + (EveryoneElseCount > 0 ? EveryoneElseCount : "") , DataSource.USERSOURCE.serialize(users));
                    EveryoneElseCount++;
                    // clear out the object and start with clean object
                    users = null; users = new HashSet<String>();
                    // clear the counter... starts from 0 again
                    user_scored = 0;
                }
            }
        }
        // write the rest of the users left in Set
        logger.debug("Writing JSON for everyone-else" + (EveryoneElseCount > 0 ? EveryoneElseCount : ""));
        if (users.size() > 0)  writeJsonFile("everyone-else" + (EveryoneElseCount > 0 ? EveryoneElseCount : ""), DataSource.USERSOURCE.serialize(users));
        users = null;
    }

    public static void writeVirtualTeams() {
        logger.debug("Writing JSON for virtual-teams");
        Set<String> vts = new HashSet<String>();
        for (User u: DataSource.USERSOURCE.allUsers()) {
            if (u.isVirtualTeam()) {
                vts.add(u.getId());
            }
        }
        writeJsonFile("all-virtual-teams", DataSource.USERSOURCE.serialize(vts));
        vts = null;
    }

    public static void writePseudos() {
        logger.debug("Writing JSON for pseudos");
        Set<String> pseudos = new HashSet<String>();
        for (User u: DataSource.USERSOURCE.allUsers()) {
            if (u.isPseudoUser() && ! u.isJunos()) {
                pseudos.add(u.getId());
            }
        }
        writeJsonFile("all-pseudos", DataSource.USERSOURCE.serialize(pseudos));
        pseudos = null;
    }

    /**
     * public wrapper to write misc information
     */
    public static void writeMisc() {
        logger.debug("Writing JSON for misc information");
        writeJsonFile("misc", serializeMisc());
    }

    private static void startProcessing() throws DashboardException {
        try {
            // setup time based multipliers, not rule based ones
            Rule.loadMultipliers();
        } catch (DashboardException e) {
            String errmsg = "Exception reading multipliers file, using (some) defaults: " +
                            e.getMessage();
            logError(errmsg);
            logger.error(errmsg);
        }

        /* Prep rules engine  -- all rules are read from DB and created here */
        ruler = new Ruler();

        // workingMemory is what will hold all of the rules as well as
        // the data to go with it
        workingMemory = ruler.getNewStatefulSession();
        DataSource.initializeSources();
        /* Start the business (This fires off a thread and returns quickly) */

        // rabbit goes down this hole to create the Watchdog thread
        updater = new Updater();
    }

    /**
     * Kills the Updater and all the status processors, and spawns a new
     * working memory and updater.  Used when the rules engine gets wedged.
     *
     * @throws DashboardException
     */
    public static void restartProcessing() throws DashboardException {
        updater.shutdown();
        updater = null;
        // Throw away the old list of Refreshers
        refreshers = new TreeSet<Refresher>();
        needToCullRules = true;
        DataSource.resetAllSources();
        logger.warn("\n\n>> Restarting processing. <<\n\n");
        inStartUp = true;
        startProcessing();
    }

    /**
     * Wake the StatusProcessor from its nap.
     */
    public static void wakeUpStatusProcessor() {
        updater.wakeUp();
    }

    /**
     * Set needToCullRules and fire all rules in the rules engine.  Useful
     * after adding or changing a rule.
     */
    public static void fireAllRules() {
        needToCullRules = Boolean.TRUE;
        Updater.fireRules(workingMemory);
    }

    public static void shutdown() {
        // Tell all the Refreshers that they need to quit
        for (Refresher refr : refreshers) {
            refr.interrupt();
        }
        // Close the Oracle datasource and clean up the connection cache
        try {
            ods.close();
        } catch (SQLException e) {
            logger.error("Exception closing datasource: " + e.getMessage());
        }
    }

    /**
     * Remove the Rule with the given id from the system.
     *
     * @param ruleid
     * @throws DashboardException
     */
    public static void deleteRule(String ruleid) throws DashboardException {
        Rule rule = getRule(ruleid);
        if (null == rule) {
            throw new DashboardException("Rule '" + ruleid + "' not found.");
        }
        ruler.deleteRule(rule);
        rules.remove(ruleid);
    }

    /**
     * Add the Rule to our list.
     *
     * @param rule
     */
    public static void addRule(Rule rule) {
        rules.put(rule.getIdentifier(), rule);
    }

    /**
     * Register an old rule under a new id.
     *
     * @param rule
     */
    public static void newIdForRule(Rule rule, String oldId) {
        rules.remove(oldId);
        rules.put(rule.getIdentifier(), rule);
    }

    /**
     * Get a Rule given its identifier.
     */
    public static Rule getRule(String ruleIdentifier) {
        if (null == ruleIdentifier) {
            /* rules is a ConcurrentHashMap, and it throws an NPE if you try
             * to get() a null. */
            return null;
        }
        return rules.get(ruleIdentifier);
    }

    public static SortedSet<Rule> getRules() {
        return Collections.unmodifiableSortedSet
               (new TreeSet<Rule>(rules.values()));
    }

    public static Ruler getRuler() {
        return ruler;
    }

    public static void registerRefresher(Refresher refresher) {
        synchronized (refreshers) {
            refreshers.add(refresher);
        }
    }

    public static SortedSet listRefreshers() {
        return Collections.unmodifiableSortedSet(refreshers);
    }

    public static String getAppFileName() {
        return appFileName;
    }

    public static void setAppFileName(String appFileName) {
        Repository.appFileName = appFileName;
    }

    public static Properties getAppProps() {
        return appProps;
    }

    public static void setAppProps(Properties appProps) {
        Repository.appProps = appProps;
    }

    /**
     * LDAP Connection methods & properties
     * @return
     */
    public static Properties getLdapProps() {
        return ldapProps;
    }
    public static String getLdapFileName() {
        return ldapFileName;
    }


    private static void initGroups() {
        for (Group.Groups g : Group.Groups.values()) {
            try {
                groups.put(g,
                           new Group(g.toString(), ldapProps.getProperty(g.key)));
            } catch (DashboardException e) {
                String errmsg = "Exception loading Group '" + g.key + "':" +
                                e.getMessage();
                Repository.logError(errmsg);
                logger.error(errmsg);
            }
        }
    }

    public static Group getGroup(Group.Groups g) {
        return groups.get(g);
    }

    public static Collection<Group> groups() {
        return groups.values();
    }

    /**
     * Expand an LDAP group against the authzldap system.
     *
     * XXX?? Doesn't cache a connection, because we don't (currently) use this
     * frequently enough to deal with the overhead.
     *
     * @return Set of usernames in the group.
     * @throws DashboardException
     */
    public static Set<String> expandLDAPGroup(String groupname)
    throws DashboardException {
        Set<String> results = new HashSet<String>();
        NamingEnumeration<SearchResult> enumeration = null;
        try {
            String query = "authGroupName=" + groupname;
            enumeration = queryLDAP(query, true);

            /* We get back an enumeration which may contain any number of
             * results, so work through them one at a time. */
            while (enumeration.hasMore()) {
                SearchResult result = enumeration.next();
                // Extract the attributes (if any) from the current entry.
                Attributes attribs = result.getAttributes();
                BasicAttribute memAttr = (BasicAttribute) attribs.get("member");
                if (null == memAttr) {
                    Repository.logError("Missing member property on Repository.java::expandLDAPGroup " + groupname);
                    logger.error("LDAP member property for group " + groupname + " are missing!");
                    continue;
                }
                NamingEnumeration members = memAttr.getAll();
                while (members.hasMore()) {
                    String mgrDN = (String) members.next();
                    String uid = mgrDN.substring(4, mgrDN.indexOf(","));
                    if ("authldapplaceholder".equals(uid)) {
                        continue;
                    }
                    results.add(uid);
                }
            }
        } catch (NamingException ne) {
            throw new DashboardException("Error expanding group '" +
                                         groupname + "': " + ne.getMessage());
        } finally {
            if (null != enumeration) {
                try {
                    enumeration.close();
                } catch (NamingException ne) {
                    // Don't really care.
                }
            }
        }
        return results;
    }

    /**
     * @param query An LDAP query.
     * @param searchGroups If true, search for groups, otherwise for users.
     * @return A NamingEnumeration of query results.
     * @throws NamingException
     */
    public static NamingEnumeration<SearchResult> queryLDAP(String query,
            boolean searchGroups) throws NamingException {
        // Assemble a hash with data to use when connecting.
        Hashtable<String, String> env = new Hashtable<String, String>();
        env.put(Context.INITIAL_CONTEXT_FACTORY,
                "com.sun.jndi.ldap.LdapCtxFactory");
        env.put(Context.PROVIDER_URL, ldapProps.getProperty("url"));
        env.put("com.sun.jndi.ldap.connect.timeout",
                ldapProps.getProperty("connection-timeout"));
        // Make a directory context by connecting with the above details.
        DirContext context = new InitialDirContext(env);

        // Set up how we want to search - in this case the entire subtree
        // under the specified directory root.
        SearchControls ctrl = new SearchControls();
        ctrl.setSearchScope(SearchControls.SUBTREE_SCOPE);

        // And don't wait forever to get an answer back
        ctrl.setTimeLimit(Integer.
                          parseInt(ldapProps.getProperty("search-timelimit")));

        String name;
        if (searchGroups) {
            name = ldapProps.getProperty("groups_name");
        } else {
            name = ldapProps.getProperty("users_name");
        }

        // Now do the search, using the specified query and search controls.
        if (logger.isDebugEnabled()) {
            logger.debug("Querying LDAP for '" + query + "'.");
        }
        NamingEnumeration<SearchResult> enumeration =
            context.search(name, query, ctrl);
        return enumeration;
    }

    /**
     * Read a file into a StringBuilder.
     *
     * @param filePath
     * @throws FileNotFoundException
     * @throws IOException
     */
    public static StringBuilder readFile(String filePath)
    throws DashboardException {
        try {
            StringBuilder buffer = new StringBuilder();
            Reader instr = new FileReader(filePath);
            char[] b = new char[128];
            int n;
            while ((n = instr.read(b)) > 0) {
                buffer.append(b, 0, n);
            }
            instr.close();
            return buffer;
        } catch (FileNotFoundException e) {
            throw new DashboardException("File '" + filePath + "' not found.");
        } catch (IOException e) {
            throw new DashboardException("IOException reading file '" +
                                         filePath + "': " + e.getMessage());
        }
    }

    public static Properties readProperties(String filePath, boolean absolute)
    throws DashboardException {
        Properties p = new Properties();
        InputStream stream;
        try {
            if (absolute) {
                stream = new FileInputStream(filePath);
            } else {
                stream = Repository.class.getResourceAsStream(filePath);
            }
            p.load(stream);
        } catch (FileNotFoundException e) {
            throw new DashboardException("File '" + filePath + "' not found.");
        } catch (IOException e) {
            throw new DashboardException("IOException reading file '" +
                                         filePath + "': " + e.getMessage());
        }
        return p;
    }

    public static void noteUpdateCycle() {
        long now = System.currentTimeMillis();
        long time = now - lastUpdateTime;
        totalUpdateCycleTime = totalUpdateCycleTime + time;
        updateCycles++;
        recentUpdateTimes[updateCycles % 10] = time;
        lastUpdateTime = now;
    }

    public static int getUpdateCycles() {
        return updateCycles;
    }

    /**
     * @return The average time taken by all update cycles, in minutes.
     */
    public static float getAverageUpdateCycleTime() {
        return
            Math.round(totalUpdateCycleTime / (float) updateCycles / 6000f) / 10f;
    }

    /**
     * @return The average time taken by the last ten update cycles,
     * in minutes.
     */
    public static float getAverageRecentUpdateCycleTime() {
        long total = 0;
        int cycles = 0;
        for (long time : recentUpdateTimes) {
            if (time > 0) {
                total = total + time;
                cycles++;
            }
        }
        return Math.round(total / (float) cycles / 6000f) / 10f;
    }

    /**
     * @return the motd
     */
    public static MotD getMotd() {
        return motd;
    }

    /**
     * @param motd the motd to set
     */
    public static void setMotd(MotD motd) {
        Repository.motd = motd;
    }

    /**
     * Log an error message for later retrieval.
     */
    public static void logError(String errmsg) {
        lastErrorDate = new Date();
        Integer count = errors.get(errmsg);
        if (null == count) {
            count = 0;
        }
        count++;
        errors.put(errmsg, count);
        newErrors.put(errmsg, count);
    }

    /**
     * @return All the errors logged since the last time someone called this.
     */
    public static Map<String, Integer> getNewErrors() {
        Map<String, Integer> retval = new HashMap<String, Integer>(newErrors);
        newErrors.clear();
        return retval;
    }

    /**
     * @return All the errors logged since startup.
     */
    public static Map<String, Integer> getErrors() {
        return errors;
    }

    public static String serializeMisc() {
        // prepare the comparator
        compareString compare = new compareString();

        logger.debug("Writing Misc information");
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("class", "net.juniper.dash.Misc");
        map.put("minutes_per_cycle", getAverageRecentUpdateCycleTime());
        map.put("first_percent", Rule.firstPercent);
        map.put("second_percent", Rule.secondPercent);
        map.put("second_percent_multiplier", Rule.secondPercentMultiplier);
        map.put("third_percent", Rule.thirdPercent);
        map.put("third_percent_multiplier", Rule.thirdPercentMultiplier);
        map.put("final_percent_multiplier", Rule.finalPercentMultiplier);
        map.put("days_after_period", Rule.daysAfterPeriod);
        map.put("second_period_multiplier", Rule.secondPeriodMultiplier);
        map.put("third_period_multiplier", Rule.thirdPeriodMultiplier);
        map.put("final_period_multiplier", Rule.finalPeriodMultiplier);
        map.put("pr_query_expression", PRRecord.queryExpression);
        /**
         * Information on how many batch of everyone-else.json
         *   divided to reduce the number of memory limitation
         */
        map.put("everyone_else_count", getEveryoneElseCount() );
        map.put("manager_count", getManagerCount());
        /**
         * Additional Information on Backlog PRs calculation, we
         *   only serialize ReleaseRecord information so we
         *   need to include the information from Release here.
         */
        // we need the result in descending order
        compare.descending = true;
        map.put("current_backlog_releases", compare.sortIt(DataSource.RELEASES.getShippedReleaseIds()).toString());
        map.put("future_backlog_releases", DataSource.RELEASES.getFutureReleaseId());
        /**
         * PR bug fix Configuration
         */

        map.put("pr_fix_releases", Repository.PR_fix_releases);
        map.put("pr_fix_rules_openpr", Repository.PR_fix_rules_openPR);
        map.put("pr_fix_rules_closedpr", Repository.PR_fix_rules_closedPR);
        /* The reader system expects lists of objects, so fake it with
         * a one-element array. */
        return new JSONSerializer().serialize(new Map[] { map });
    }

    /**
     * setter and getter for Status Report Cycle
     *
     */
    public static int getStatusReportCycle() {
        return StatusReportCycle;
    }

    public static void noteStatusReportCycle() {
        StatusReportCycle++;
    }

    public static void resetStatusReportCycle() {
        StatusReportCycle = 0;
    }

    public static int getEveryoneElseCount() {
        return EveryoneElseCount;
    }

    public static int getManagerCount() {
        return ManagerCount;
    }

    public static Set<String> SetUnion(Set<String>... collections) {
        Set<String> retval = new HashSet<String>();
        for (Set<String>items : collections) {
            try {
                retval.addAll(items);
            } catch(NullPointerException ev) {}
        }
        return retval;
    }

    public static Set<String> SetDiff(Set<String> source, Set<String>... collections) {
        Set<String> retval = new HashSet<String>();
        if (source != null) {
            retval = new HashSet<String>(source);
            for (Set<String>items : collections) {
                try {
                    retval.removeAll(items);
                } catch (NullPointerException ev) {}
            }
        }
        return retval;
    }

    public static Map<String, String> getBugFixRulesPair() {
        Map<String, String> pairs = new HashMap<String, String>();

        Set<String> sourceOpen = new HashSet<String>(Arrays.asList(Repository.PR_fix_rules_openPR.split(",")));
        Set<String> sourceClosed = new HashSet<String>(Arrays.asList(Repository.PR_fix_rules_closedPR.split(",")));
        /**
         * Extra layer of checking to make sure the rules hasn't changed or still exists
         */
        if (sourceOpen.contains("major_prs_fix") &&
                sourceClosed.contains("major_closed_pr")) {
            pairs.put("major_prs_fix", "major_closed_pr");
        }
        if (sourceOpen.contains("regression_prs_fix") &&
                sourceClosed.contains("closed_regression_prs")) {
            pairs.put("regression_prs_fix", "closed_regression_prs");
        }
        return pairs;
    }
    
    public static void refreshCache() {
    	//Refresh aliases maps once a day
    	Calendar calendar = GregorianCalendar.getInstance(); 
    	calendar.setTime(new Date());
    	int day = calendar.get(Calendar.DAY_OF_YEAR);
        if (aliasesMapRefreshDay != day){
            aliasesMapRefreshDay = day;
    	    populateAliases();
        }
    }
	public static void populateAliases(){
	    Map<String, Object> m = new HashMap<String, Object>();
	    m.put("job", "getUsers");
	    Set<String> users = new HashSet<String>();
	    m.put("users", users);
	    m.put("search-field", "responsible");
	    ExecutorService executor = Executors.newFixedThreadPool(1);
	    executor.execute(new OuterAppCommunicator(m));
	    executor.shutdown();
	    while (!executor.isTerminated());

            m.put("job", "getDefaultApprovers");
	    executor = Executors.newFixedThreadPool(1);
	    executor.execute(new CRSDBHandler(m));
	    executor.shutdown();
	    while (!executor.isTerminated());
	    
	    m = new HashMap<String, Object>();
	    Set<String> possibleAliases;
	    m.put("job", "getPossibleAliases");
		m.put("possibleAliases", possibleAliases = users);
	    executor = Executors.newFixedThreadPool(1);
	    executor.execute(new DashDBHandler(m));
	    executor.shutdown();
	    while (!executor.isTerminated());
	        
	    if (!possibleAliases.isEmpty()) {
	    	m = new HashMap<String, Object>();
	    	m.put("job", "populateAliasMap");
	    	m.put("aliases_search_url", appProps.getProperty("aliases_search_url", "http://rotor.juniper.net/cgi-bin/webify?command=aka&style=text&opt_ldap=1"));
	    	m.put("possibleAliases", possibleAliases);
	    	m.put("userAliasMap", userAliasMap);
	    	m.put("aliasUserMap", aliasUserMap);
	    	executor = Executors.newFixedThreadPool(1);
	    	executor.execute(new OuterAppCommunicator(m));
		    executor.shutdown();
		    while (!executor.isTerminated());
		    
		    if (!aliasUserMap.isEmpty()){
			    executor = Executors.newFixedThreadPool(1);
		    	m = new HashMap<String, Object>();
		    	m.put("job", "storeAliases");
		    	m.put("aliasUserMap", aliasUserMap);
			    executor.execute(new DashDBHandler(m));
			    executor.shutdown();
			    while (!executor.isTerminated());
		    } 
	    }
	    
	    if (aliasUserMap.isEmpty() || userAliasMap.isEmpty()){
	    	m = new HashMap<String, Object>();
	    	m.put("job", "getAliases");
	    	m.put("userAliasMap", userAliasMap);
	    	m.put("aliasUserMap", aliasUserMap);
	    	executor = Executors.newFixedThreadPool(1);
		    executor.execute(new DashDBHandler(m));
		    executor.shutdown();
		    while (!executor.isTerminated());
	    }
	}
	
	public static Set<String> getAliases(String uid) {		
		return userAliasMap.get(uid);
	}
	
	public static Set<String> getUsers(String systest_owner) {		
		return aliasUserMap.get(systest_owner);
	}
}

/**
 * custom comparator class to compare list with release name
 *
 * Usage :
 *     List<String> releases; <-- our variable to hold releases or numeric string
 *     Set<String> unsortedSet;
 *     ...
 *
 *     compareString compare = compareString();
 *
 *     Collections.sort(releases,compare); // release will be sort ascending
 *
 *     compare.descending = true;
 *
 *     Collections.sort(releases,compare); // release will be sort descending
 *
 *     // to use sortIt method:
 *     List<String> sortedReleases = compare.sortIt(unsortedSet);
 *
 */
class compareString implements Comparator<String> {
    boolean descending = false;
    public compareString() {}

    // another utility to return list of ordered string from Set
    public List<String> sortIt(Set<String> unsortedSet) {
        List<String> sortedList = new ArrayList<String>(unsortedSet);
        Collections.sort(sortedList, this);
        return sortedList;
    }

    @Override
    // override compare method.
    public int compare (String rel1, String rel2) {
        if (rel1.equalsIgnoreCase(rel2)) {
            // if they are the same
            return 0;
        }
        String[] l1 = rel1.split("[.,-]");
        String[] l2 = rel2.split("[.,-]");
        // find which one as pivot
        int bound = l1.length;
        if (l2.length > bound) {
            bound = l2.length;
        }
        // iterate through
        for (int i = 0; i < bound; i++) {
            if (i >= l1.length) {
                if (this.descending) {
                    return 1;
                }
                return -1;
            } else if (i >= l2.length) {
                if (this.descending) {
                    return -1;
                }
                return 1;
            }
            String e1 = l1[i];
            String e2 = l2[i];
            Integer i1 = Integer.parseInt(l1[i]);
            Integer i2 = Integer.parseInt(l2[i]);
            if ((i1 == null) && (i2 == null)) {
                // if we can't convert to integer then fallback
                // with string comparison
                if (descending) {
                    return e2.compareTo(e1);
                }
                return  e1.compareTo(e2);
            } else if (i1 == null) {
                if (descending) {
                    return 1;
                }
                return -1;
            } else if (i2 == null) {
                if (descending) {
                    return -1;
                }
                return 1;
            }
            if (i1 != i2) {
                if (descending) {
                    return i2.compareTo(i1);
                }
                return i1.compareTo(i2);
            }
        }
        return 0;
    }
}

