<map version="0.9.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1202250639005" ID="Freemind_Link_643912604" MODIFIED="1202250659584" TEXT="Multistate PRs">
<node CREATED="1206489899147" FOLDED="true" ID="Freemind_Link_1296772913" MODIFIED="1244834585425" POSITION="left" TEXT="dashboard">
<node CREATED="1202251733626" HGAP="25" ID="Freemind_Link_106198979" MODIFIED="1202251786092" TEXT="Assert what?" VSHIFT="-18">
<node CREATED="1202251759961" ID="Freemind_Link_460586792" MODIFIED="1202251761984" TEXT="Record per scope"/>
<node CREATED="1202251762547" ID="Freemind_Link_704068145" MODIFIED="1202251771178" TEXT="Record per PR">
<node CREATED="1202251771163" ID="Freemind_Link_1269702870" MODIFIED="1202251781472" TEXT="how do we run rules on scoped fields?"/>
</node>
</node>
<node CREATED="1202250661862" ID="_" MODIFIED="1202250847619" TEXT="Associate PRs with Releases during bookkeeping"/>
<node CREATED="1202250850856" ID="Freemind_Link_1207311003" MODIFIED="1202250875283" TEXT="Run rules as &quot;...from $release.relatedPRs&quot;"/>
<node CREATED="1202250950254" ID="Freemind_Link_1301551417" MODIFIED="1202250966060" TEXT="Need to assert Record per scope"/>
</node>
<node CREATED="1206489920598" FOLDED="true" ID="Freemind_Link_1674032036" MODIFIED="1244835058137" POSITION="right" TEXT="proposal">
<node CREATED="1206489937461" ID="Freemind_Link_579458818" MODIFIED="1206490297486" TEXT="Pre multi-state">
<node CREATED="1206489943932" ID="Freemind_Link_116148717" MODIFIED="1206566398581" TEXT="release info in Release field - mix of &quot;found in&quot; and &quot;working on in&quot;"/>
<node CREATED="1206566340952" ID="Freemind_Link_51460310" MODIFIED="1206566353567" TEXT="Blockers indicated in Blocking-Release">
<node CREATED="1206566353413" ID="Freemind_Link_894590381" MODIFIED="1206566359194" TEXT="m.n"/>
<node CREATED="1206566359593" ID="Freemind_Link_1952332723" MODIFIED="1206566362737" TEXT="m.n-CAM"/>
<node CREATED="1206566363497" ID="Freemind_Link_873881128" MODIFIED="1206566369760" TEXT="m.n-testblocker"/>
<node CREATED="1206566370370" ID="Freemind_Link_796276876" MODIFIED="1206566372272" TEXT="etc."/>
</node>
<node CREATED="1206489966504" ID="Freemind_Link_1634808769" MODIFIED="1206566425408" TEXT="Dashboard uses Release / Blocking-Release, because it&apos;s all that&apos;s available">
<edge WIDTH="thin"/>
</node>
</node>
<node CREATED="1206490024888" ID="Freemind_Link_292630684" MODIFIED="1206490293934" TEXT="Post multi-state">
<node CREATED="1206490027756" ID="Freemind_Link_63951937" MODIFIED="1206490038333" TEXT="Reported-in and Release">
<node CREATED="1206490039274" ID="Freemind_Link_811066807" MODIFIED="1206490057341" TEXT="Reported-In for &quot;found in&quot;"/>
<node CREATED="1206490061658" ID="Freemind_Link_317277325" MODIFIED="1206490068149" TEXT="Release for &quot;working on in&quot;"/>
<node CREATED="1206490073806" ID="Freemind_Link_1725552484" MODIFIED="1206490076477" TEXT="Release scoped"/>
<node CREATED="1206566435521" ID="Freemind_Link_1569850132" MODIFIED="1206566455315" TEXT="Blocker???"/>
</node>
<node CREATED="1206490083200" ID="Freemind_Link_1795088106" MODIFIED="1206490140381" TEXT="A PR will have one scope for every release that is being actively worked"/>
<node CREATED="1206490141110" ID="Freemind_Link_911302221" MODIFIED="1206490631981" TEXT="Dashboard will only report opportunity when there is a scope with an identifiable value (/\d+\.\d+([RB].*)?/) in the Release field, and State not in (closed, suspended, monitored)"/>
<node CREATED="1206490318400" ID="Freemind_Link_348102713" MODIFIED="1206490343439" TEXT="Dashboard will *ignore* the Reported-In field">
<node CREATED="1206490345106" ID="Freemind_Link_1769965129" MODIFIED="1206490356278" TEXT="Dashboard only cares about things that are actionable."/>
<node CREATED="1206490356785" ID="Freemind_Link_1609346933" MODIFIED="1206490365126" TEXT="It&apos;s about what you need to do *now*"/>
</node>
</node>
<node CREATED="1206490648393" ID="Freemind_Link_959825623" MODIFIED="1206490654896" TEXT="How do we get from here to there?">
<node CREATED="1206490657056" ID="Freemind_Link_1959046141" MODIFIED="1206492096031" TEXT="At rollout, PRs will have one scope, with a blank release"/>
<node CREATED="1206492097879" ID="Freemind_Link_1353660715" MODIFIED="1206492115977" TEXT="Dashboard (and everyone else) will need to look at Reported-In"/>
<node CREATED="1206492116776" ID="Freemind_Link_1304159009" MODIFIED="1206492138752" TEXT="At some point, scopes (with releases) will be created"/>
<node CREATED="1206492139127" ID="Freemind_Link_439325993" MODIFIED="1206492169664" TEXT="How does someone know when to look at Release (scopes) and not Reported-In?"/>
<node CREATED="1206492170201" ID="Freemind_Link_712131084" MODIFIED="1206511468969" TEXT="We should ensure that there is an unambiguous answer to that question."/>
</node>
<node CREATED="1206506189037" ID="Freemind_Link_1688296132" MODIFIED="1206506194443" TEXT="Proposal">
<node CREATED="1206506194430" ID="Freemind_Link_1383899191" MODIFIED="1206554903052" TEXT="When a change is made to any scoped field in an existing (pre multi-state) PR, a scope should be created for each release that needs work"/>
<node CREATED="1206554958231" ID="Freemind_Link_35180987" MODIFIED="1206555000505" TEXT="This will allow users, and, more to the point, tools, to clearly understand where to look for release information about that PR.">
<node CREATED="1206555000492" ID="Freemind_Link_1434206524" MODIFIED="1206555038733" TEXT="One scope =&gt; look at Reported-In / Blocking-Release"/>
<node CREATED="1206555039390" ID="Freemind_Link_368883518" MODIFIED="1206555056061" TEXT="Multiple scopes =&gt; use Release"/>
</node>
</node>
</node>
<node CREATED="1244835058676" ID="ID_1133655744" MODIFIED="1244835065010" POSITION="right" TEXT="no-release">
<node CREATED="1244835140524" ID="ID_1817653421" MODIFIED="1244835145437" TEXT="record types">
<node CREATED="1244835065011" ID="ID_1768320849" MODIFIED="1244835068885" TEXT="PRs">
<node CREATED="1244835086811" ID="ID_1373979682" MODIFIED="1244835097709" TEXT="no value"/>
<node CREATED="1244835098141" ID="ID_549528603" MODIFIED="1244835102301" TEXT="inactive release"/>
</node>
<node CREATED="1244835069676" ID="ID_1501968302" MODIFIED="1244835073724" TEXT="RLIs">
<node CREATED="1244835076354" ID="ID_613060403" MODIFIED="1244835109213" TEXT="unscheduled"/>
<node CREATED="1244835109635" ID="ID_663114049" MODIFIED="1244835110797" TEXT="n/a"/>
</node>
</node>
<node CREATED="1244835151997" ID="ID_1839077980" MODIFIED="1244835215804" TEXT="scoring">
<node CREATED="1244835222874" ID="ID_1614954840" MODIFIED="1244835226930" TEXT="score as normal">
<node CREATED="1244835226932" ID="ID_1896005881" MODIFIED="1244835232949" TEXT="what are the dates then?"/>
<node CREATED="1244835234305" ID="ID_526817071" MODIFIED="1244835249309" TEXT="Always active or never active?"/>
<node CREATED="1244835250365" ID="ID_961655219" MODIFIED="1244835253653" TEXT="doesn&apos;t make sense"/>
</node>
<node CREATED="1244835215805" ID="ID_1723396972" MODIFIED="1244835222349" TEXT="zero score">
<node CREATED="1244835284623" ID="ID_473943693" MODIFIED="1244835291765" TEXT="easier to implement and explain"/>
</node>
</node>
<node CREATED="1244835294837" ID="ID_791826779" MODIFIED="1244835298867" TEXT="display">
<node CREATED="1244835298871" ID="ID_247914049" MODIFIED="1244835561654" TEXT="Make available in the &quot;more&quot; dropdown?"/>
<node CREATED="1244835536887" ID="ID_718870325" MODIFIED="1244835551662" TEXT="In the matrix?"/>
<node CREATED="1244835562997" ID="ID_1630719702" MODIFIED="1244835579255" TEXT="Only show implicitly in the totals box."/>
</node>
</node>
</node>
</map>
