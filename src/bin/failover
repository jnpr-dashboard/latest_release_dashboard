#!/bin/bash

# This script will check whether the host name passed as a parameter is in the
# list of valid Dashboard servers (see $SERVERS below), down/disable VIP on a
# currently active host and up/enable it on the specified host.
# If no parameters are passed, the status for each host in the $SERVERS list
# will be displayed.

# TODO: It would be a GOOD IDEA to configure VIP on dashboard-stage1 and
# -stage2 so they could be used in the rotation if neither dashboard-prod1
# nor dashboard-prod2 are operational for whatever reason.

# An list of ALL production servers
SERVERS=" \
    dashboard-prod1 \
    dashboard-prod2 \
    dashboard-stage1 \
    dashboard-stage2 \
"

# Print usage and exit 1
function usage() {
    echo "Usage: $0 [options] [standby-host]"
    echo ""
    echo "Fail over to the specified standby host. The host must have"
    echo "the dashboard.juniper.net VIP configured."
    echo ""
    echo "If no arguments are passed, status for each host is diplayed."
    echo ""
    echo "Options:"
    echo "    -h        print this help message"
    echo "    -v        provide verbose output"
    echo ""
    echo "Example:"
    echo "    failover -v dashboard-prod2"
    exit 1
}

while getopts "hv" opt
do
    case $opt in
        v ) VERBOSE=1
            ;;
        h ) usage
            ;;
        * ) usage
            ;;
    esac
done
shift $(($OPTIND - 1))
TARGET=$1

# Replace with empty strings for non-color terminal
RED="\033[1;31m"
GREEN="\033[1;32m"
CLEAR="\033[0m"

# Print error message and exit -1
function fatal() {
    printf "$0: ${RED}fatal error:${CLEAR} $@\n" >&2
    exit -1
}

function log() {
    if [ "${VERBOSE}" != "" ]; then
        printf "$0: $1\n"
    fi
}

# Return 0 if host $1 is up, -1 otherwise
function isUp() {
    UP=$(ssh $1.juniper.net 'ifconfig eth0:1 | egrep -c "inet addr:([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3})"')
    if [[ ${UP} -eq 1 ]]; then
        return 0
    fi
    return -1
}

# Return 0 if the specified host name is in the $SERVERS list, -1 otherwise 
function inList() {
    for host in ${SERVERS}; do
        if [ "${host}" == "$1" ]; then
            return 0
        fi
    done
    return -1
}

# Enable and ifup VIP on the specified host $1
function vipUp() {
    ssh $1.juniper.net 'sudo mv /etc/sysconfig/network-scripts/off-ifcfg-eth0\:1 /etc/sysconfig/network-scripts/ifcfg-eth0\:1'
    ssh $1.juniper.net 'sudo ifup eth0:1'
}

# ifdown and disable VIP on the specified host $2
function vipDown() {
    ssh $1.juniper.net 'sudo ifdown eth0:1'
    ssh $1.juniper.net 'sudo mv /etc/sysconfig/network-scripts/ifcfg-eth0\:1 /etc/sysconfig/network-scripts/off-ifcfg-eth0\:1'
}

# If target is not specified, display the status of each host
if [ -z "${TARGET}" ]; then
    for host in ${SERVERS}; do
        isUp ${host}
        if [ $? -eq 0 ]; then
            printf "${host}: ${GREEN}active${CLEAR}\n"
        else
            printf "${host}: ${RED}standby${CLEAR}\n"
        fi
    done
else
    # If the specified target is in the list...
    inList ${TARGET}
    if [ $? -eq 0 ] ; then
        # ... for all servers in the list ...
        for host in ${SERVERS}; do
            # ... if it is UP ...
            isUp ${host}
            if [ $? -eq 0 -a ${TARGET} != ${host} ]; then
                # ... turn it down
                log "${RED}Deactivating${CLEAR} ${host}..."
                vipDown ${host}
                if [ $? -ne 0 ]; then
                    fatal "Failed to deactivate ${host}, manual intervention is required!"
                fi
            fi
        done
        # If the specified target is not ALREADY up ...
        isUp ${TARGET}
        if [ $? -ne 0 ]; then
            # ... turn it up
            log "${GREEN}Activating${CLEAR} ${TARGET}..."
            vipUp ${TARGET}
        else
            log "${TARGET} is already ${GREEN}active${CLEAR}\n"
        fi
    else
        fatal "Unrecognized host name ${TARGET}\n(check the name, do not append the domain name)"
    fi
fi
