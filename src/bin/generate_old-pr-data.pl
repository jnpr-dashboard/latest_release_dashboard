#!/usr/bin/env perl
use strict;
use warnings;
use Getopt::Long;
use Data::Dumper;

sub usage {
    my $prog = "$0";
    print <<HELP;
Options:  help   display this help message
          output <arg> REQUIRED where results should be stored
          host   <arg> gnats port to use (default gnats.juniper.net)
          port   <arg> gnats port to use (default 1529)

NOTE: This script is designed to get a snapshot from one point in time from gnats.  To combine these, use
      combine_history.py to combine it with existing data and to prep it for inclusing to be used with
      OldPRRecord as a data source.
      
** WARNING: If running this against a database snapshot before around September 2010, the field planned-release
            didn't exist and the field release needs to be renamed to planned-release in the database for 
            this script to work correctly.

            Also, if is this being run against a database before the end of December 2010, the field problem-level
            didn't exist and for this script to work, that field needs to be added and the values calculated for
            it run and populate that field.
HELP
    exit 0;
}

my ($help, $output) = undef;
my $host = "gnats.juniper.net";
my $port = "1529";

GetOptions(
    'help'     => \$help,
    'host=s'   => \$host,
    'port=s'   => \$port,
    'output=s' => \$output,
);

usage() if $help;

if (not $output) {
    print "ERROR: output is required\n";
    usage()
}


# *****
# These should match what is in history.pl
# *****

my %fields = (
'last-modified' => 'Date',
'arrival-date' => 'Date',
'state' => 'PickList',
'synopsis' => 'Text',
'reported-in' => 'Text',
'planned-release' => 'Text',
'problem-level' => 'Text',
'committed-release' => 'Text',
'conf-committed-release' => 'Text',
'blocker' => 'Text',
'attributes' => 'Text',
'class' => 'PickList',
'functional-area' => 'PickList',
'product' => 'PickList',
'category' => 'PickList',
'responsible' => 'User',
'dev-owner' => 'User',
'systest-owner' => 'User',
'techpubs-owner' => 'User',
'originator' => 'User',
'customer-escalation' => 'Boolean',
'branch' => 'Text',
'target' => 'Text',
'keywords' => 'Text',
'updated' => 'Date',
'feedback-date' => 'Date',
'confidential' => 'Boolean',
'rli' => 'Text',
'fix-eta' => 'Date',
'submitter-id' => 'Text',
'cust-visible-behavior-changed' => 'Text',
);


# get all open PRs OR all DOC CVBC Open PRs
my $query = "(product[group]==\"junos\" & state!=\"closed\") | (product[group]==\"junos\" & (cust-visible-behavior-changed==\"yes\" | cust-visible-behavior-changed==\"yes-ready-for-review\") )";

my $separator = "\037";

my $format = '';
foreach ('number', sort keys %fields) {
    # if not first one, add separator
    $format .= ($format eq '') ? '"' : $separator;
    # add %s for each field
    # Adding dummy separator for non-existing GNATS database field to maintain
    # consistency with existing snapshot.
    if ($_ eq "blocker" or $_ eq "planned-release" or $_ eq "rli") {
        $format .= "%s$separator";
    }
    else {
        $format .= '%s';
    }
}
$format .= '" ' . join(' ', 'number', sort keys %fields);
#print $format."\n";

# need to take host and port from command line
my @cmd = ('query-pr','-H', $host,'-P', $port, '-v', 'dashboard', '-e', $query, '-f', $format);

print "running command...\n";
print join (" ", @cmd) , "\n";


open(Q, '-|') || exec(@cmd) || die "Could not execute query-pr";

print "processing results...\n";

open(CSVFILE, ">>$output");

my @results;
while (<Q>) {
    chomp;
    push @results, $_;
}

print CSVFILE "$_\n" foreach @results;
#print "$_\n" foreach @results;

close(CSVFILE);

# not going to do this here.. python is going to combine and add extract-date and line-number
#system('bzip2 /opt/dashboard/special/gnats-hist/backlog20100131.csv');

