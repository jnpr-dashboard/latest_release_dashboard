#!/usr/bin/env perl
#
# Pull all history PR from  Flat file 
#
# Sanny Mulyono, smulyono 2010 
#
# Copyright (c) 2009-2010, Juniper Networks, Inc.
# All rights reserved.
#
#####################
use strict;
#use warnings;
use Carp qw(croak);

use Getopt::Long;
use HTTP::Date;
use FindBin;
use lib $FindBin::Bin;
use utils;


# XXX debugging
use Data::Dumper;

#my $VERSION = sprintf('%d.%02d', q$Revision: 1.5 $ =~ /(\d+)\.(\d+)/);

my $help = undef;
my $filename = "/opt/dashboard/special/data/gnats.txt.bz2";

GetOptions(
    'help'         => \$help,
    'filename=s'   => \$filename,
);

usage() if $help;

sub usage {
    my $code = shift;
    print <<EOM;
usage: historypr.pl <input_filename>
    --help     show this message
    --filename <arg> filename to load, defaults to /opt/dashboard/special/data/gnats.txt.bz2
EOM
    exit $code;
}

# Get the list of fields that the consumer desires
my @db_columns = @ARGV;


# *****
# These should match what is in generate_old-pr-date.pl
# *****

my %fields = (
'last-modified' => 'Date',
'arrival-date' => 'Date',
'state' => 'PickList',
'synopsis' => 'Text',
'reported-in' => 'Text',
'planned-release' => 'Text',
'problem-level' => 'Text',
'committed-release' => 'Text',
'conf-committed-release' => 'Text',
'blocker' => 'Text',
'attributes' => 'Text',
'class' => 'PickList',
'functional-area' => 'PickList',
'product' => 'PickList',
'category' => 'PickList',
'severity' => 'PickList',
'priority' => 'PickList',
'responsible' => 'User',
'dev-owner' => 'User',
'systest-owner' => 'User',
'techpubs-owner' => 'User',
'originator' => 'User',
'customer-escalation' => 'Boolean',
'branch' => 'Text',
'target' => 'Text',
'keywords' => 'Text',
'updated' => 'Date',
'feedback-date' => 'Date',
'confidential' => 'Boolean',
'rli' => 'Text',
'fix-eta' => 'Date',
'submitter-id' => 'Text',
'blocking-release' => 'Text',
'cust-visible-behavior-changed' => 'Text',
);
 
do_start(); 
 
# Backlog PR 
#  - history , to construct the data point and point of reference
my @history_rec = `bunzip2 -c $filename`;

my @oldPR = ();
my $separator = "\037";
my $i=0;
my $row_num = 0;
for my $row (@history_rec){
	my %prdata = ();
	my @raw_data = split($separator,$row);
	$prdata{"line-number"} = $raw_data[0];
	$prdata{"extract-date"} = $raw_data[1];
	$prdata{"number"} = $raw_data[2];
	$i=3;
	for my $field (sort keys %fields){
		$prdata{$field} = $raw_data[$i];
		if ($prdata{$field} eq ""){
			$prdata{$field} = "-";
		}
		$i++;
	}
	push(@oldPR, \%prdata);
	$row_num ++;
}


# print out the number + extract date which will be our id... values[0]
for my $old_pr (@oldPR){
    print $old_pr->{"line-number"}, $UNIT_SEPARATOR,$old_pr->{"extract-date"}, $UNIT_SEPARATOR,$old_pr->{"number"};
    for my $colname (@db_columns) {
        no warnings;           
        print $UNIT_SEPARATOR, $old_pr->{$colname};
    }
    print "$RECORD_SEPARATOR\n";
}


