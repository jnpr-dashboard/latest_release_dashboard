#!/bin/sh
#
# Fetch all the JSON data from the java layer and save it to disk.
#
# Dirk Bergstrom, dirk@juniper.net, 2009-04-01
#
# Copyright (c) 2009, Juniper Networks, Inc.
# All rights reserved.
#
#########
function usage() {
    echo "Usage: get-json.sh https://server.name/path/to/app /path/for/output"
    exit
}

WGET='wget --no-check-certificate --output-document'

if [ "x$1" = "x-h" ]; then
    usage
fi

get_users=
if [ "x$1" = "x-u" ]; then
    get_users='yes'
    shift
fi

server_path=$1
out_path=$2
if [ "x$2" = "x" -o "x$1" == "x" ]; then
    usage
fi

if [ "x$get_users" != "x" ]; then
    for type in 'all-managers' 'all-virtual-teams' 'all-pseudos' 'everyone-else'; do
        echo "fetching $type"
        $WGET=${out_path}/${type}.json ${server_path}/json/${type}.do
    done
fi

for type in 'sources' 'milestones' 'rules' 'objectives' 'all-usernames'; do
    echo "fetching $type"
    $WGET=${out_path}/${type}.json ${server_path}/json/${type}.do
done

for type in 'release' 'npi'; do
    echo "fetching records of $type"
    $WGET=${out_path}/${type}.json ${server_path}/json/ds-records.do?ds=${type}
done

echo "done"