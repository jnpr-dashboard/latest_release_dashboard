#!/usr/bin/env perl 
#
# Pull all open JUNOS releases into the dashboard system.
#
# Dirk Bergstrom, dirk@juniper.net, 2006-10-10
#
# Copyright (c) 2006-2007, Juniper Networks, Inc.
# All rights reserved.
#
# What we do here?
# We are going query all R1-R4 and B1-B4 and merge them 
#  into TOT (M.n). From branch tracker, we will pickup the
#  IB branches for each (M.n) releases and put them as child
#  branch for M.nR1. 
#  We will use the M.nR1 as our pinpoint in dashboard.
#  We can figurely assume that Rn-Bn-Sn-IBn is all scopes for
#    M.n. 
#
# Preparing the fields to be collected, there are:
#  - (.*)_fields = fields which resides in milestones, Branch Tracker, NPI 
#        Fields kept here is also used to do query. 
#        
#  - (.*_)pra_fields = date fields which needs to examine between (actual, revised, planned)
#        and return to us the correct date. (actual > revised > planned). So we will
#        only keep one date here and one type.
#        Fields here will be used for processing the fields information with main_fields
#  
#####################
use strict;
#use warnings;
use Carp qw(croak);

use Getopt::Std;
use HTTP::Date;
use FindBin;
use lib $FindBin::Bin;
use utils;

my $VERSION = sprintf('%d.%02d', q$Revision: 1.28 $ =~ /(\d+)\.(\d+)/);

use vars qw($opt_h $opt_v $opt_V $opt_d $opt_u);
getopts('hvVdu:');

usage(0) if ($opt_h);

sub usage {
    my $code = shift;
    print <<EOM;
usage: releases.pl [-hvVd] [-u<username>] host
    -u Connect to deepthought with this username (incompatible with cert usage)
    -d Die on errors
    -v be verbose
    -V be very verbose
    -h show this message
This is version $VERSION.
EOM
    exit $code;
}

my $host = shift;

$opt_v = 1 if ($opt_V);

# Get the list of fields that the consumer desires
my @db_columns = @ARGV;

#### Start Customization ###
# Deepthought info
my $miles_table = 'Milestones';
my $npi_table = 'npi';
my $bt_table = 'branch';
#### End Customization ###
do_start();
# Get connected to the outside world.
setup_deepthought($host, $miles_table);

# The Milestone fields we pull in as-is
my @main_fields = ();
my @pra_fields = ();
my @release_fields = ();
for my $fld (@release_metadata) {
    my $fname = $fld->{'field'};
    next unless ($fld->{'data_source'} eq 'Milestones');
    if ($fld->{'data_type'} eq "Date" && 
    	# fixme later...., need find a way if date has no _planned, _revised,_actual
        ($fname ne 'last_modified' && $fname ne 'pre_ibrg_one' 
        && $fname ne 'pre_ibrg_two' 
        && $fname ne 'ibfrs_checkpoint_1' && $fname ne 'ibfrs_checkpoint_2' 
        && $fname ne 'ibfrs_checkpoint_3' && $fname ne 'ibfrs_checkpoint_4' 
        && $fname ne 'tprg' && $fname ne 'pre_rrg' && $fname ne 'respin')) {
            push(@release_fields, $fname.'_planned', $fname.'_revised', $fname.'_actual');
            push(@pra_fields, $fname);
    } elsif ($fld->{'data_type'} eq "Date" && $fname eq 'respin' ) {
        push(@release_fields, $fname.'_planned',  $fname.'_actual');
        push(@pra_fields, $fname);
    } else {
        push(@release_fields, $fname);
        push(@main_fields, $fname);
    }
}

my @bt_fields = ();
my @bt_pra_fields = ();
for my $fld (@release_metadata) {
    my $fname = $fld->{'field'};
    next unless (($fld->{'data_source'} eq 'BT') && $fname ne 'record_number');
    if ($fld->{'data_type'} eq "Date" && $fname ne 'last_modified' && $fname ne     'teg') {
        push(@bt_fields, $fname.'_planned', $fname.'_revised', $fname.'_actual');
        push(@bt_pra_fields, $fname);
    } else {
        push(@bt_fields, $fname);
    }
}

my @release_params = (
    'skipClosed' , 'yes',
    'release_type_pick' , 'R1',
    'sortField1' , 'release',
    'sortDir1'   , 'asc',
    'sortField2' , 'release_type',
    'sortDir2'   , 'asc',
    );

# Fields we get for Bn and Rn records
# and also IB / PR Fields
my @bn_rn_fields = ();
my @bn_rn_pra_fields = ();
for my $fld (@release_metadata) {
    my $fname = $fld->{'field'};
    next unless (($fld->{'data_source'} eq 'Bn_Rn'  )  
                 && $fname ne 'record_number'  );
    if ($fld->{'data_type'} eq "Date" && $fname ne 'last_modified' && $fname ne 'respin') {
        push(@bn_rn_fields, $fname.'_planned', $fname.'_revised', $fname.'_actual');
        push(@bn_rn_pra_fields, $fname);
     # we dont need revised for these fields, 
    } elsif ($fld->{'data_type'} eq "Date" && $fname eq 'respin' ) {
        push(@bn_rn_fields, $fname.'_planned', $fname.'_actual');
        push(@bn_rn_pra_fields, $fname);
    } else {
        push(@bn_rn_fields, $fname);
    }
}

my @bn_rn_params = (
    'skipClosed', 'yes',
    'state_pick', 'P6',
    'not_state', 'not',
    'release_type_pick', 'B1',
    'release_type_pick', 'B2',
    'release_type_pick', 'B3',
#    'release_type_pick', 'B4',
#    'release_type_pick', 'B5',
#    'release_type_pick', 'B6',
#    'release_type_pick', 'B7',
#    'release_type_pick', 'B8',
#    'release_type_pick', 'B9',
    'release_type_pick', 'R2',
    'release_type_pick', 'R3',
    'release_type_pick', 'R4',
    'release_type_pick', 'R5',
    'release_type_pick', 'R6',
    'release_type_pick', 'R7',
    'release_type_pick', 'R8',
#    'release_type_pick', 'R9',
#   We also query Sn releases now, but RLI API didn't provide us good way to do regex on their query (as far as i know of)
#      there will be only 30 Service releases, but they usually less than 15. 
    'release_type_pick', 'S1', 'release_type_pick', 'S2','release_type_pick', 'S3','release_type_pick', 'S4','release_type_pick', 'S5',
    'release_type_pick', 'S6', 'release_type_pick', 'S7','release_type_pick', 'S8','release_type_pick', 'S9','release_type_pick', 'S10',
    'release_type_pick', 'S11', 'release_type_pick', 'S12','release_type_pick', 'S13','release_type_pick', 'S14','release_type_pick', 'S15',
#    'release_type_pick', 'S16', 'release_type_pick', 'S17','release_type_pick', 'S18','release_type_pick', 'S19','release_type_pick', 'S20',
#    'release_type_pick', 'S21', 'release_type_pick', 'S22','release_type_pick', 'S23','release_type_pick', 'S24','release_type_pick', 'S25',
#    'release_type_pick', 'S26', 'release_type_pick', 'S27','release_type_pick', 'S28','release_type_pick', 'S29','release_type_pick', 'S30',
    'sortField1', 'release',
    'sortDir1', 'asc',
    'sortField2', 'release_type',
    'sortDir2', 'asc',
    );

my @npi_fields = ();
for my $fld (@release_metadata) {
    next unless ($fld->{'data_source'} eq 'npi' &&
                 $fld->{'field'} ne 'record_number');
    push(@npi_fields, $fld->{'field'});
}

# FIXME This is something of a hack, but there's no field that says "This is
# the NPI record for a JUNOS release"...
my %npi_params = (
    'skipClosed' => 'yes',
    'synopsis' => 'junos npi',
    'npi_email_alias' => 'junos-npi',
    );

# Fetch the R1 records
my $query_results = do_query($r, \@release_params, \@release_fields);

my $count = 0;
my %seen = ();
my %releases = ();
my %eoled_releases = ();
my %cdm_release = ();
# With CDM releases, we process the M.nIBx as the child branch of R1
warn "Processing R1 Milestones records\n" if ($opt_v);
for my $rec (@$query_results) {
    $count++;
    my $num = $$rec{'record_number'};
    if ($opt_V) {
    	warn $num, ' ';
    	warn "\n" if ($count % 10 == 0);
    }

	my $relname = $$rec{'release'};
	my $reltype = $$rec{'release_type'};
	if ($seen{$relname} && $seen{$relname}->{'R1'} && $reltype eq "R1") {
	    # We've already processed this release.  That's bad...
	    warn("Duplicate R1 records for release '$relname'
First occurence was record ".$seen{$relname}->{'R1'}.", skipping $num.\n");
        next;
	}
	$seen{$relname} = {$reltype=> $num};

	if ($$rec{'state'} eq 'P6') {
	    # This release has reached EOL
	    $eoled_releases{$relname} = $num;
	}

	my %release = ();
	$release{'id'} = $num;
	$release{'r1_record_num'} = $num;
    $release{'r1_color'} = $rec->{'color'};
    $release{'r1_major_issues'} = $rec->{'major_issues'};
        $release{'r1_blocker_lockdown'} = $rec->{'blocker_lockdown'};
	$release{'release'} = $relname ;
	$release{'branch_name'} = $relname;
	$release{'relname'} = $relname ;
	$release{'last_modified'} = $$rec{'last_modified'};
	for my $field (@main_fields) {
	    $release{$field} = $$rec{$field};
	}
	for my $field (@pra_fields) {
	    my $fname = $field;
            #print "A1: Field is $field\n";
	    my $res = coalesce($fname,
	                       $$rec{$field.'_planned'},
	                       $$rec{$field.'_revised'},
	                       $$rec{$field.'_actual'});
	    if ($field eq 'build' || $field eq 'deploy' || $field eq 'respin' || $field eq 'release_branch') {
	       $fname = 'r1_'.$field;
	    }
        $release{$fname} = $res->[0];
        $release{$fname.'_type'} = $res->[1];
    }
   	$releases{$relname} = \%release;
   	# check if this release part of the CDM release
   	if ((1*$relname) >=  10.3 ) {
   		$cdm_release{$relname} = "BUZZ";
   	}
}

# Fetch the Bn & Rn Milestone records, also added the IBn and PR
$query_results = do_query($r, \@bn_rn_params, \@bn_rn_fields);

warn "Processing Bn & Rn records\n" if ($opt_v);
for my $rec (@$query_results) {
    $count++;
    my $num = $$rec{'record_number'};
    if ($opt_V) {
    	warn $num, ' ';
    	warn "\n" if ($count % 10 == 0);
    }
    my $relname = $$rec{'release'};
	my $reltype = lc($$rec{'release_type'});
	if ( ! $seen{$relname}) {
	    warn("Found $reltype info for a release w/o R1 info." .
	         "  Skipping record $num.\n");
        next;
	}
	if ($seen{$relname}->{$reltype}) {
	    # We've already processed this release.  That's bad...
	    warn("Duplicate $reltype records for release '$relname'
First occurence was record ".$seen{$relname}->{$reltype}.", skipping $num.\n");
        next;
	}
	$seen{$relname}{$reltype} = $num;
	my $release = $releases{$relname};

	my $lastmod = $$rec{'last_modified'};
	if ($lastmod gt $$release{'last_modified'}) {
	   $$release{'last_modified'} = $lastmod;
	}

    $$release{$reltype.'_color'} = $rec->{'color'};
    $$release{$reltype.'_major_issues'} = $rec->{'major_issues'};
    $$release{$reltype.'_record_num'} = $num;
    $$release{$reltype.'_blocker_lockdown'} = $rec->{'blocker_lockdown'};
    # PROCESS THE Bn Rn
    for my $ms (@bn_rn_pra_fields) {
        #print "A2: Field is $ms\n";
    	my $res = coalesce($ms,
    	                   $$rec{$ms.'_planned'},
                           $$rec{$ms.'_revised'},
                           $$rec{$ms.'_actual'});
        $$release{$reltype.'_'.$ms} = $res->[0];
        $$release{$reltype.'_'.$ms.'_type'} = $res->[1];
    }
}

# Fetch the NPI records
$r->table('npi');
$query_results = do_query($r, \%npi_params, \@npi_fields);

warn "Processing NPI records\n" if ($opt_v);
for my $rec (@$query_results) {
    $count++;
    my $num = $$rec{'record_number'};
    if ($opt_V) {
    	warn $num, ' ';
    	warn "\n" if ($count % 10 == 0);
    }
    my $relname = $$rec{'release'};
    my $phase = $$rec{'phase'};
	if ( ! $seen{$relname}) {
	    warn("Found npi info for a release w/o R1 info." .
	         "  Skipping NPI record $num.\n");
        next;
	}
	if ($seen{$relname}->{'npi'}) {
	    # We've already processed this release.  That's bad...
	    warn("Duplicate NPI records for release '$relname'
First occurence was record ".$seen{$relname}->{'npi'}.", skipping $num.\n");
        next;
	}
	$seen{$relname}{'npi'} = $num;
	if (exists($eoled_releases{$relname}) && $phase ne 'P6') {
        warn("Milestones thinks '$relname' is EOL, but NPI doesn't ".
            "(Milestones wins).") if ($opt_v);
        $phase = 'P6';
    } elsif ( ! exists($eoled_releases{$relname}) && $phase eq 'P6') {
	    warn("Release '$relname' is live in Milestones, but EOLed in NPI ".
	         "(NPI wins)");
	    $eoled_releases{$relname} = $seen{$relname}->{'R1'};
	    $releases{$relname}->{'state'} = 'P6';
	}
	my $release = $releases{$relname};

	my $lastmod = $$rec{'last_modified'};
	if ($lastmod gt $$release{'last_modified'}) {
	   $$release{'last_modified'} = $lastmod;
	}

	$$release{'phase'} = $phase;
    $$release{'npi_record_num'} = $num;
	# if the release is in the cdm release,
	# we need to also include :
	#     - phase
	# 
	if (exists($cdm_release{$relname})){
		$cdm_release{$relname} = $phase;
	}

    for my $ph qw(0_ll 0 1 2 3 4 5) {
        my $name = 'p'.$ph.'_exit';
        $$release{$name} = $$rec{$name};
    }
}

# Fetch the records from BT
# for development uncomment this
#setup_deepthought($dev_host, $bt_table);
$r->table($bt_table);

my @bt_params = (
	'branch_type_pick','ib',
	'branch_type_pick','dev',
	'childBranch','yes',
        'state_pick','closed-abandoned',
        'not_state','not',
	'skipClosed','no'
    );
# construct to get the branch for releases
my @cdm_rel = sort(keys(%cdm_release));
for my $cdmr (@cdm_rel){
	push @bt_params,'release_target_pick';
	push @bt_params, $cdmr."R1";
}

# adding this IB branch as release

$query_results = do_query($r, \@bt_params, \@bt_fields);
my @orphan_branch = ();
for my $rec (@$query_results){
	my $b_type = $$rec{"branch_type"};
	my $rel_target = $$rec{"release_target"};
	my $b_name = $$rec{"branch_name"};
	if ($b_name =~ ($ib_branch_regex)){
		my $rec_name = $2.".".$3;
		my %desc_release = ();
		if (exists($cdm_release{$rec_name})){
		    # construct the release name
			$desc_release{"release"} = $rec_name;
			$desc_release{"relname"} = $b_name;
			# only IF this is branch/dev then we update this field
			$desc_release{"branch_name"} = $b_name;
			$desc_release{"record_num"} = $$rec{"record_number"};
			$desc_release{"id"} = $$rec{"id"};
			$desc_release{"release_type"} = $1;
			my $parent_rel = $releases{$rec_name};
			for my $field (@bt_fields) {
				if ($field eq "last_modified"){
					my $lastmod = $$rec{'last_modified'};
					if ($lastmod gt $$parent_rel{'last_modified'}) {
					   $desc_release{'last_modified'} = $lastmod;
					}
				} else {
		    		$desc_release{$field} = $$rec{$field};
				}
			}
			# to make sure the parents has the same eol time
			$desc_release{"phase"} = $$parent_rel{"phase"};
			$desc_release{"ibrg"} = $$parent_rel{"ibrg"};
			$desc_release{"ibrg_type"} = $$parent_rel{"ibrg_type"};
			$desc_release{"pre_ibrg_one"} = $$parent_rel{"pre_ibrg_one"};
			# Adding ibfrs and r1_deploy date to BT record
			$desc_release{"ibfrs"} = $$parent_rel{"ibfrs"};
			$desc_release{"r1_deploy"} = $$parent_rel{"r1_deploy"};

                        for my $field (@bt_pra_fields) {
                            my $fname = $field;
                            my $res = coalesce($fname,
                                               $$rec{$field.'_planned'},
                                               $$rec{$field.'_revised'},
                                               $$rec{$field.'_actual'});
                            $desc_release{$fname} = $res->[0];
                            $desc_release{$fname.'_type'} = $res->[1];
                        }
			$releases{$b_name} = \%desc_release;
		}		
	}
	# we will revisit it later.......
	push(@orphan_branch, $rec);
}

my @dev_branch=();
# revisit the orphan, for all of the other processed release 
for my $rec (@orphan_branch){
	my $rel_target = $$rec{"release_target"};
	my $b_name = $$rec{"branch_name"};
	# identified if this record a child branch of some"IB-branch"
	if ($rel_target =~ ($ib_branch_regex)){
		add_child($rel_target, $b_name);
	} elsif ($rel_target =~ ($tot_regex)){
		#identified for TOT	
		my $rec_name = $1.".".$2;
		my $branch_name = $b_name;
		add_child($rec_name, $b_name);
	} else {
		# this should ONLY contains the DEV release target
		push(@dev_branch, $rec);
	}
}

sub add_child {
	# insert the new $b_name as the child of $rec_name
	my $rec_name = shift;
	my $b_name = shift;
	if (exists($releases{$rec_name})){
		my $relhash = $releases{$rec_name};
		if (exists($relhash->{"child_branch"})){
			$relhash->{"child_branch"} = $relhash->{"child_branch"}.",".$b_name;
		} else {
			$relhash->{"child_branch"} = $b_name;
		}
		$releases{$rec_name} = $relhash;
	}
}

# injection for testing
#######################
sub inject_new_branch {
  my $base_release = shift;
  my $r_name = shift;
  my $b_name = shift; 
  my $c_branch = shift;
  my $rec = $releases{$base_release};
  my %desc_release = ();
  # construct the release name
  $desc_release{"release"} = $r_name;
  $desc_release{"relname"} = $b_name;
  # only IF this is branch/dev then we update this field
  $desc_release{"branch_name"} = $b_name;
  $desc_release{"record_num"} = $$rec{"record_number"};
  $desc_release{"release_type"} = "IB";
  for my $field (@bt_fields) {
      $desc_release{$field} = $$rec{$field};
  }
  # to make sure the parents has the same eol time
  $desc_release{"phase"} = $$rec{"phase"};
  $desc_release{"ibrg"} = $$rec{"ibrg"};
  $desc_release{"ibrg_type"} = $$rec{"ibrg_type"};
  $desc_release{"pre_ibrg_one"} = $$rec{"pre_ibrg_one"};
  $releases{$b_name} = \%desc_release;
  add_child($r_name, $b_name);
  my @temp_child = split(',', $c_branch);
  for my $t_child (@temp_child){
    add_child($b_name, $t_child);
  }
}

if ($opt_V) {
    # Some releases will have full info, some (future ones) will have only
    # an R1 record.  We don't really care about this, but a user might want
    # to see what's up.
    warn "\nChecking release info.\n";
    for my $rel (sort(keys(%releases))) {
        warn ">> $rel: R1, ";
        my $saw = $seen{$rel};
        my @missing = ();
        for my $type qw(B1 B2 B3 B4 B5 B6 R2 R3 R4 R5 R6 R7 R8 R9 npi IB1 IB2 IB3 IB4 IB5 PR) {
            if ($$saw{$type}) {
                warn "$type, ";
            } else {
                push(@missing, $type);
            }
        }
        if (scalar(@missing) > 0) {
            warn "\n        Missing ", join(', ', @missing), "\n";
        }
    }
    #use Data::Dump qw(dump);
    #warn dump($releases{'8.2'});
}

# JUST FOR TESTING
#inject_new_branch('11.1','11.3','IB_11_3_FT_BRANCH','DEV_11_3_FT_BRANCH,DEV_11_3_FT2_BRANCH');
#inject_new_branch('11.1','11.3','IB3_11_3_SLT_BRANCH','DEV_11_3_SLT_BRANCH');
#inject_new_branch('11.1','11.2','IB1_11_2_BRANCH','DEV_EABU_Q4_BRANCH');
#inject_new_branch('11.1','11.3','IB_11_3_STATENISLAND_BRANCH','');
#inject_new_branch('11.1','11.3','IB_11_3_SLT_ROUTE66_BRANCH','DEV_SLT_ROUTE66_BRANCH');

#@db_columns =('release','relname','child_branch');
#$UNIT_SEPARATOR = " | ";
#Print out the releases
# During the print-out, we also tracking the childBranch
# do it in this big loop so we don't need to make another big loop
my @rels_order = sort(keys(%releases));
for my $relname (@rels_order) {
    my $relhash = $releases{$relname};
    # last phase for child branch, it should take care of everything that has DEV
    if (exists($relhash->{"child_branch"})){
	    for my $brec (@dev_branch){
			my $rel_target = $$brec{"release_target"};
			my $b_name = $$brec{"branch_name"};
			my $found =index($relhash->{"child_branch"},$rel_target); 
			if ($found >= 0 ){
				# dev is there
				add_child($relname, $b_name);
			} 
	    }
    }
    print $relname ;
    for my $colname (@db_columns) {
        no warnings;
        print $UNIT_SEPARATOR, $relhash->{$colname};
    }
    print "$RECORD_SEPARATOR\n";
}

do_exit();

# End of main
#############

sub coalesce {
    my ($fname, $p, $r, $a) = @_;
    if ($fname =~ /respin/) {
        if ($a gt $p) {
            return [$a, 'actual'];
        } else {
            return [$p, 'planned'];
        }
    } else {
        if ($a) {
            return [$a, 'actual'];
        } elsif ($r) {
            return [$r, 'revised'];
        } else {
            return [$p, 'planned'];
        }
    }
}
