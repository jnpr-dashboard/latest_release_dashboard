#!/usr/bin/env perl
#
# Partially translate a struts JSP page to a Django template
#
# Dirk Bergstrom, dirk@juniper.net, 2009-03-31
#
# Copyright (c) 2009, Juniper Networks, Inc.
# All rights reserved.
#
#####################
use strict;

#$/ = undef;
my %not = ('!' => 'not ', '' => '');

my @lines= <STDIN>;
my @no_cc_lines = ();

for my $line (@lines) {
    # find lines that look like they have a camelCaseWord in a struts tag
    if ($line =~ /="[^"]*[a-z][A-Z]/) {
        # camelCaseWords => camel_case_words
        $line =~ s/(?<=[a-z])([A-Z])/'_'.lc($1)/ge;
    }
    push(@no_cc_lines, $line);
}

my $file = join('', @no_cc_lines);

# if/else statements
$file =~ s#<s:if\s+test="(\!)?\s*(.*?)"\s*>#{% if $not{$1||''}$2 %}#gsm; 
$file =~ s#</s:if>\s*<s:else>#{% else %}#gsm; 
$file =~ s#</s:(else|if)>#{% endif %}#gsm; 

# iterator
$file =~ s#<s:iterator\s+value="(.*?)"\s*(?:status=".*?")?\s*>#{% for XXX in $1 %}#gsm; 
$file =~ s/#itstat/forloop/g; 
$file =~ s#</s:iterator\s*>#{% endfor %}#gsm; 

# variable interpolation
$file =~ s#<s:property\s+value="(.*?)"\s*/\s*>#{{ $1 }}#gsm; 

# URL tag (half-assed translation)
$file =~ s#<s:url[^>]+?action="(.*?)".*?/>#{% url $1 %}#gsm; 

# includes
$file =~ s#<%@\s*include\s+file="(.*?)"\s*%>#{% include '$1' %}#gsm; 

# comments
$file =~ s#<%--#{% comment %}#gsm;
$file =~ s#--%>#{% endcomment %}#gsm;

print $file;