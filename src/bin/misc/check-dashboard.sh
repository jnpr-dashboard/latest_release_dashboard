#!/bin/sh
#
# Check status of Dashboard, quick and dirty.
#   in the new dashboard server, we have file auth for
#   all user free access. So instead of passing cert
#   we are using free access. To reduce overhead of adding
#   another Vhost which accept cert authorization only for
#   monitoring.
#   
# In the future, we probably
#   need to pass cert and provide additional Vhost to serve it.
#   It will make more sense if that Vhost is not only serving
#   this monitor script, API probably?!?
# --certificate
# --private-key
#
# MAKE SURE TO PUT THIS SCRIPT ON OUTSIDE OF SERVER BEING MONITORED
# CURRENTLY is in chronos.juniper.net
#########

# /usr/local/bin/wget on chronos is ancient and has no SSL support
WGET=/usr/home/wget/bin/wget
# For development, this is the most standard place
#WGET=/usr/bin/wget

# Production server which is behind the proxy
SERVER=imdash.juniper.net
# Not using certificate for now.....
#CERT_DIR=/c/eclipse-workspace/dashboard/src/bin/misc/monitor-certs
#CERT_DIR=/opt/dashboard-smulyono2/application/build/WEB-INF/classes/bin/misc/monitor-certs
#CERT=rli-monitors.pem
#KEY=rli-monitors-npk.pem

# How long to wait before trying again
SLEEP=1

# --quiet \                 No diagnostic output
# --output-document=- \     Output to stdout
# --tries=2 \               Make two tries (one second apart)
# --retry-connrefused \     Retry on connection refused
# --timeout=30 \            30 second connection/DNS timeout
# --no-check-certificate \  Don't complain about invalid server cert
CMD="${WGET} --timeout=30 \
--quiet \
--output-document=- \
--tries=2 \
--retry-connrefused \
--no-check-certificate \
--user=dashboard \
--password=dashboard \
https://${SERVER}/dashjava/status.do"

# Check the status
out=`$CMD`

if [ $? -ne 0 ]; then
    # Can't connect, denied, or some other difficulty.
    # Wait three minutes, and try again.
    sleep $SLEEP
    out=`$CMD`
fi

if [ $? -ne 0 ]; then
    echo "Failed to connect to Dashboard on $SERVER"
elif [ "a$out" != "aOK" ]; then
    echo "Dashboard error:"
    echo $out
fi
