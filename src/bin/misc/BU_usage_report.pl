#!/usr/bin/perl

#use strict;
use Time::Local;
use Data::Dumper;
use Getopt::Long;
use Switch;

my $awstat_dir = "";
my $dir = "/tmp";
my $month_and_year = "";
my $now = localtime();
my @dates = split(/\s+/,$now);
my $year = $dates[4];
my $month = &getMonth($dates[1]);
my %responsible;
my %bu_usage;
my %bu_unique_users;

GetOptions (
            "help" => \$help,
            "month=s" => \$month_and_year,
            "email=s" => \$email,
            "application=s" => \$application
           );

if ( defined $help )
{
    print "Please provide correct input\n";
    print "Example : If you want output for the month of April 2012\n";
    print "\n          perl $0 -month 042012 -application dashboard -email <email>\@juniper.net\n";
    exit 2;
}
switch ( $application ) {
    case "dashboard"	{ $awstat_dir = "/volume/recluse-httpd/awstats-data/database/dashboard_stage1_2"; }
    case "howdy"        { $awstat_dir = "/volume/recluse-httpd/awstats-data/database/howdy"; }
    else                { print "Please provide the application name from the list below \n 1. dashboard\n 2. howdy\n"; exit 3; }
}

my $attachment = "$dir/Users_$month_and_year.csv";
my $attachment1 = "Users_$month_and_year.csv";
my $attachment2 = "$dir/Bu_Usage_$month_and_year.csv";
my $attachment3 = "Bu_Usage_$month_and_year.csv";
my $attachment4 = "$dir/Bu_Unique_Users_$month_and_year.csv";
my $attachment5 = "Bu_Unique_Users_$month_and_year.csv";

`query-pr --adm-field responsible > $dir/responsible.txt`;

open(FH, "$dir/responsible.txt") or die "Unable to open file";
while(<FH> ) {
    my $line = $_;
    chomp $line;
    my @info = split(':', $line);
    $responsible{$info[0]}{'BU'} = $info[3];
}
close(FH);

opendir(FD,"$awstat_dir") or die "Unable to open dir $awstat_dir";
my @files = readdir(FD);
closedir(FD);

open(FD, ">$dir/Users_$month_and_year.csv") or die 'Unable to open file';
print FD "User, BG, BU, Hits\n";
foreach my $filename (@files) {
    next unless($filename =~ /$month_and_year/);
    open(FH, "$awstat_dir/$filename") || die 'Unable to open file';
    my $login_found = 0;
    while (my $line = <FH> ) {
        chomp $line;
        if($line =~ /^BEGIN_LOGIN/) {
            $login_found = 1;
            next;
        }
        next if($line !~ /^BEGIN_LOGIN/ and $login_found == 0); 
        if($line =~ /^END_LOGIN/) {
            $login_found = 0;
            last;
        }
        my @data = split(/\s+/, $line);
        my $bu = $responsible{$data[0]}{'BU'};
        print FD "$data[0],,$bu,$data[2]\n";
        $bu = "BLANK" unless($bu);
        $bu_usage{$bu} += $data[2];
        $bu_unique_users{$bu}++;
    }
    close(FH);
}
close(FD);

open (FD, ">$dir/Bu_Usage_$month_and_year.csv") or die 'Unable to open file';
foreach my $bu ( reverse sort { $bu_usage{$a} <=> $bu_usage{$b} } keys %bu_usage ) {
    print FD "$bu,$bu_usage{$bu}\n";
}
close(FD);

open (FD, ">$dir/Bu_Unique_Users_$month_and_year.csv") or die 'Unable to open file';
foreach my $bu ( reverse sort { $bu_unique_users{$a} <=> $bu_unique_users{$b} } keys %bu_unique_users ) {
    print FD "$bu,$bu_unique_users{$bu}\n";
}
close(FD);

open (MAIL, "|/usr/sbin/sendmail -t");
print MAIL "To: $email\n";
print MAIL "From: $email\n";
print MAIL "Subject: Dashboard Usage details\n\n";
print MAIL "Hi\nThis is auto-generated mail.";
print MAIL "\n\n";

my $attachment = "$dir/Users_$month_and_year.csv";
my $attachment1 = "Users_$month_and_year.csv";
my $attachment2 = "$dir/Bu_Usage_$month_and_year.csv";
my $attachment3 = "Bu_Usage_$month_and_year.csv";
my $attachment4 = "$dir/Bu_Unique_Users_$month_and_year.csv";
my $attachment5 = "Bu_Unique_Users_$month_and_year.csv";

open(FILE, "uuencode $attachment $attachment1; uuencode $attachment2 $attachment3; uuencode $attachment4 $attachment5 |");
while( <FILE> ) { print MAIL; };
close(FILE);
close(MAIL);

sub getMonth {
    my $mm = shift;
    my %months = ( 'Jan' => '01', 'Feb' => '02', 'Mar' => '03', 'Apr' => '04', 'May' => '05', 'Jun' => '06', 'Jul' => '07', 'Aug' => '08', 'Sep' => '09', 'Oct' => '10', 'Nov' => '11', 'Dec' => '12' );
    return $months{$mm};
}
