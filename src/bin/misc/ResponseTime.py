#!/usr/bin/env python

# This is a script to get response time for each URL of dashboard application

import MySQLdb
import os
import re
import urllib2, base64
from datetime import datetime

if __name__ == '__main__':
    # Take some default URLs to calculate Response Time
    URL	 =  {'dashboard': ['http://deepthought.juniper.net/dashboard/rli-report/junos', 
                           'http://deepthought.juniper.net/dashboard/backlogprs/junos', 
                           'http://deepthought.juniper.net/dashboard/mttr/junos',
                           'http://deepthought.juniper.net/dashboard/sla/junos',
                           'http://deepthought.juniper.net/dashboard/rli-report/junos',
                           'http://deepthought.juniper.net/dashboard/agenda/junos',
                           'http://deepthought.juniper.net/dashboard/npis'],
             'howdy'    : ['http://deepthought.juniper.net/howdy/metrics',
                           'http://deepthought.juniper.net/howdy/how-it-works/',
                           'http://deepthought.juniper.net/howdy']
            }
    cursor = MySQLdb.connect(host='metroid.juniper.net', user='scraper', passwd='scrap3r', db='dashboard_stats').cursor()
    for key, value in URL.items():
        instance = key
        USERNAME = 'howdytest' if instance == 'howdy' else 'dashboard'
        PASSWORD = 'testme' if instance == 'howdy' else 'dashboard'
        for urls in value:
            auth_encoded = base64.encodestring('%s:%s' % (USERNAME, PASSWORD))[:-1]
            req = urllib2.Request(urls)
            req.add_header('Authorization', 'Basic %s' % auth_encoded)
            time1 = time2 = datetime.now()
            try:
                urllib2.urlopen(req)
                time2 = datetime.now()
            except urllib2.HTTPError, http_e:
                print("Failed to retrieve %s with error code %s" % (urls, http_e))
            diff = time2 - time1
            response_time = ( diff.seconds * 1000000 ) + diff.microseconds
            # Add these response times into database to access by tableau
            cursor.execute("insert into ResponseTime (instance, url, ResponseTime) values(%s, %s, %s)", (instance, urls, response_time))
