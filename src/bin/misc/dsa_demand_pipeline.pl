#!/usr/bin/perl
############################
## Demand_Supply Details  ##
############################i

require 5.6.0;
use strict;
use Getopt::Std;
use RLI::api;
use Data::Dumper;  #for debugging

my $host = 'deepthought.juniper.net';
my $table = 'deep_rli';
my $cert = '/opt/dashboard/special/dashboard.crt';
my $key = '/opt/dashboard/special/dashboard.key';
my $printtext;
my $printresults;
use POSIX 'strftime';
my $datestring = strftime "%a %b %e %H:%M:%S %Y", localtime;

my $r = new RLI::api($table, $host);
unless (defined $r) {
    print STDERR "Can't create RLI::api handle for ",
      "table '$table' on host '$host'\n";
    exit 1;
}
$r->add_cert($cert, $key);
unless ($r->check_connection) { 
   die "Problem connecting: ", $r->errstr(), "\n";
}

my @interesting_fields = ( 'synopsis', 'multi_platform',
                           'i2o_status', 'parent_rli', 'rli_class', 'value_to_customer', 'planned_rel_date','feature_priority',
                       );

my %interesting_fields;
for my $f (@interesting_fields) {
    $interesting_fields{$f}=$f;
}

my $debug = 1;

my %record;


my @interesting_platforms = ('dashboard-info','tableau-eng','howdy','ic-info');

my @params = ( 'skipClosed', 'yes', );
for my $p ( @interesting_platforms ) {
    push @params, 'multi_platform_pick', $p;
}

my $field_name;
my $results;
my ($field_names, $results) = $r->query_hash(\@params, \@interesting_fields);
my $str = $r->errstr();
if ($str & $debug) {
    print STDERR "Problem with query: $str\n";
    return;
}

print_table ( "header" );

for my $d ("now") {
    for my $p1 (@interesting_platforms) {
        print_table ( $d, $p1, $results );
    }
}

my $dt = strftime "%m/%d", localtime;
open(MAIL, "|/usr/sbin/sendmail -t");
print MAIL "To: ndivya\@juniper.net \n";
print MAIL "Mime-Version: 1.0\n";
print MAIL "Content-type: text/html; charset=\"iso-8859-1\"\n";
print MAIL "Subject: Demand/Pipeline numbers for $dt\n\n";
## Mail Body
print MAIL "(This is an auto generated email)<br/><br/> $printtext</table></html>";
close(MAIL);

exit;


sub print_table {
    my $date = shift;
    my $platform = shift;
    my $results = shift;

    my $format = "%8s %23s %8d %8d %8d %8d | %8d %8d \n";

    #Resources is a fib
    my $resources = 1;

   if ( $date eq "header" ) {
    # "\nDEFINATION:\n";
    # "----------\n";
    # "Incoming requests : open usr-feature-req RLIs not on deploy roadmap \n";
    # "Backlog : open usr-feature-req RLIs, i2o_status in busval_inadequate or ready_for_dac_review \n";
    # "Support/Maint, not Inflight : not usr-feature-req, not on deployment roadmap, planned release date is null \n";
    # "Support/Maint, not Inflight, high : not usr-feature-req, not on deployment roadmap, planned release date is null,priority is 'Showstopper' or 'High'  \n";
    # "Support/Maint, not Inflight, low : not usr-feature-req, not on deployment roadmap, planned release date is null, priotity other than 'Showstopper' and 'High'\n";
    # "Inflight : usr-feature-req on deployment roadmap and planned release date not null  \n";
    # "Support/Maint, Inflight : not usr-feature-req and planned release date not null \n\n";

        $printtext .=  "<html><table border=1><tr><td>date</td><td>platform</td><td>incoming</td><td>backlog</td><td>s/m high</td><td>s/m low</td><td>inflight</td><td>s/m inflight</td></tr>";
        return;
    }


    my $titlebar_printed;
    my ($incoming, $inflight, $backlog,
        $support_inflight,
        $support_not_inflight, $support_not_inflight_high, $support_not_inflight_low);

    $incoming = $inflight = $backlog = $support_inflight = $support_not_inflight = $support_not_inflight_high = $support_not_inflight_low = 0;

    for my $hashRef (sort status_sort @{$results}) {

        my %tmp = %{$hashRef};
        my $record = \%tmp;

        #is this the right platform
        my $found_platform;
        if ( ref $hashRef->{multi_platform} eq 'ARRAY' ) {
            foreach my $p (@{$hashRef->{multi_platform}}) {
                $found_platform = 1 if $p eq $platform;
            }
        }
        else {
            foreach my $p ( split ("|", $hashRef->{multi_platform})) {
                $found_platform = 1 if $p eq $platform;
            }
        }

        next if not $found_platform;

        $incoming++ if $hashRef->{i2o_status} ne "on_deploy_rdmap" and
                 $hashRef->{rli_class} eq 'usr-feature-req';
        $inflight++ if $hashRef->{i2o_status} eq 'on_deploy_rdmap'  and
                 $hashRef->{'planned_rel_date'} ne '';

        if ( $hashRef->{rli_class} eq "usr-feature-req" ) {
            $backlog++ if $hashRef->{i2o_status} eq 'busval_inadequate';
            $backlog++ if $hashRef->{i2o_status} eq 'ready_for_dac_review';
        };

        # support/maint
        if ( $hashRef->{i2o_status} ne 'on_deploy_rdmap' and
                 $hashRef->{rli_class} ne 'usr-feature-req' and
			 $hashRef->{planned_rel_date} eq '') {
            $support_not_inflight++;
            if ( $hashRef->{feature_priority} eq '1-Showstopper' or
                 $hashRef->{feature_priority} eq '2-High') {
                $support_not_inflight_high++;
            }
            else {
                $support_not_inflight_low++;
            }
        }
        if ( 
             $hashRef->{rli_class} ne 'usr-feature-req' and
              $hashRef->{planned_rel_date} ne ''
           ) {
            $support_inflight++;
        }

    }
    $printtext .=  "<tr><td>$datestring</td><td>$platform</td><td>$incoming</td><td>$backlog</td><td>$support_not_inflight_high</td><td>$support_not_inflight_low</td><td>$inflight</td><td>$support_inflight</td></tr>"; 

}

sub status_sort {
    my %valuelist;
    my @list = ( 'duplicate',
                 'business_value_inadequate',
                 'rejected',
                 'suspended',
                 'not_applicable',
                 'initial_request',
                 'business_value_assessment',
                 'technical_value_assessment',
                 'technical_solution_proposal',
                 'ready_for_dac_review',
                 'prioritized',
                 'on_deploy_rdmap',
                 'deployed',
             );
    my $num = 1;
    for my $i (@list) {
        $valuelist{$i} = $num++;
    }
    return $valuelist{$a->{i2o_status}} <=> $valuelist{$b->{i2o_status}}; }



