#!/usr/bin/env perl
#
# Standard sanity test for JUNOS MBO backlog PR counts
#
# smulyono@juniper.net , 2010-11-10
# Copyright (c) 2010 , Juniper Networks, Inc.
#  ---
#

# !!!! Change this as needed !!!!!
my $release_regex="(".
                  "(11\\.4)|(11\\.3)|(11\\.2)|(11\\.1)".
				  "|(10\\.4)|(10\\.3)|(10\\.1)|(10\\.2)|(9\\.5)|(9\\.6)|(6\\.2)|(6\\.3)|(6\\.0)|(6\\.1)|(10\\.0)|(6\\.4)".
                  "|(7\\.5)|(7\\.6)|(7\\.3)|(7\\.4)|(7\\.1)|(7\\.2)|(7\\.0)|(5\\.7)|(8\\.3)|(8\\.2)|(5\\.5)|(8\\.5)".
                  "|(4\\.1)|(5\\.4)|(8\\.4)|(5\\.6)|(5\\.3)|(5\\.1)|(8\\.1)|(5\\.2)|(8\\.0)|(4\\.4)|(5\\.0)|(4\\.2)".
                  "|(4\\.3)|(9\\.0)|(9\\.2)|(9\\.1)|(4\\.0)|(9\\.4)|(9\\.3)".
				  ")";
#                  "(([RrBbSs])[1-9][0-9]?)?";
#my $release_regex = "10\\.4";
#my $release_regex = '(([3-9]\.[0-9])|(10\.[0-3]))(([rbs])[1-9][0-9]?)?';
# ===============================

my $separator = "\037";
foreach ('number', 'functional-area','planned-release','reported-in') {
    $format .= ($format eq '') ? '"' : $separator;
     $format .= '%s ,';
}
$format .= '" ' . join(' ', 'number', 'functional-area','planned-release','reported-in', 'last-modified');

$format_unique = "";
foreach ('number', 'functional-area','planned-release','reported-in') {
    $format_unique .= ($format_unique eq '') ? '"' : $separator;
    $format_unique .= '%s ,';
}
$format_unique .= '" ' . join(' ', 'number', 'functional-area');

my $global_conf = "-H gnats.juniper.net -P 1529";

my $doc_backlog_pr_query="(problem-level == \"1-CL1\" | problem-level == \"2-CL2\" | problem-level == \"4-CL3\" | problem-level == \"3-IL1\" | problem-level == \"4-IL2\" |  problem-level == \"5-IL3\") &". 
                         "(class==\"bug\" | class==\"unreproducible\") & functional-area==\"documentation\" & ".
                         "((planned-release==\"\" & reported-in ~ \"(^|[ \[-])$release_regex\") | planned-release = \"$release_regex\") & ".
                         "product[group]==\"junos\" & state!=\"closed\"";
my $cvbc_backlog_pr_query="((planned-release==\"\" & reported-in ~ \"(^|[ \[-])$release_regex\") | planned-release =\"$release_regex\") & ".
						  "product[group]==\"junos\" & ".
						  "(cust-visible-behavior-changed==\"yes\" | cust-visible-behavior-changed==\"yes-ready-for-review\")";
my $backlog_pr_query="(problem-level == \"1-CL1\" | problem-level == \"2-CL2\" | problem-level == \"4-CL3\" | problem-level == \"3-IL1\" | problem-level == \"4-IL2\" | problem-level == \"5-CL4\" | problem-level == \"5-IL3\") & ".
                     "(class==\"bug\" | class==\"unreproducible\") & functional-area==\"software\" & ".
                     "((planned-release==\"\" & reported-in ~ \"(^|[ \[-])$release_regex\") | planned-release = \"$release_regex\") & ".
                     "product[group]==\"junos\" & state!=\"closed\"";

# Printing the raw Query if necessary!!!@#@$!
#print "query-pr -v dashboard --expr='$doc_backlog_pr_query' --format='$format \n";
#print "query-pr -v dashboard --expr='$cvbc_backlog_pr_query' --format='$format";

#print "Current Backlog PR Query = ". `query-pr -v dashboard $global_conf --expr='$backlog_pr_query' --format='$format' | wc -l` ;
#print "Current Backlog Unique PR Query = ". `query-pr -v dashboard $global_conf --expr='$backlog_pr_query' --format='$format_unique' | wc -l` ;
print "Current Doc Backlog PR Query = ". `query-pr -v dashboard $global_conf --expr='$doc_backlog_pr_query' --format='$format' | wc -l`;
print "Current Doc Backlog Unique PR Query = ". `query-pr -v dashboard $global_conf --expr='$doc_backlog_pr_query' --format='$format_unique' | wc -l`;
print "Current CVBC Backlog PR Query = ". `query-pr -v dashboard $global_conf --expr='$cvbc_backlog_pr_query' --format='$format' | wc -l`;
print "Current CVBC Backlog Unique PR Query = ". `query-pr -v dashboard $global_conf --expr='$cvbc_backlog_pr_query' --format='$format_unique' | wc -l`;
