#!/usr/bin/env python
# -*- coding: utf-8 -*-

# make adjustments to the trend files for the 2011-01 rollout
# change uid on all old backlog files from backlog_pr to backlog_pr_2010
# this needs to be run after the rename of the directories

import csv
import os
import sys

from datetime import datetime, date
from copy import deepcopy
from pprint import pformat


# need to tweak path to get in SnapshotManager
snapshot_path = os.path.normpath(os.path.join(os.path.dirname(__file__),'..','..','dash','ui'))
sys.path.insert(0, snapshot_path)
from snapshot import SnapshotManager, SnapshotJsonFile

for old_type, new_type in [
                            ['backlog_pr', 'backlogprs'],
                            ['backlog_pr-2010', 'backlogprs-2010'],
                            ['doc_cvbc_backlog_pr', 'docbacklog']
                          ]:
    print 'running %s -> %s' % (old_type, new_type)
    sm = SnapshotManager(new_type)
        
    for jfile in sm.get_files():
        if jfile.data['data'][0].has_key(old_type):
            jfile.data['data'][0][new_type] = jfile.data['data'][0][old_type]
            del jfile.data['data'][0][old_type]
            jfile.save()
        elif not jfile.data['data'][0].has_key(new_type):
            print 'could not find %s or %s, did find %s' % (old_type, new_type, jfile.data['data'][0].keys())
        
