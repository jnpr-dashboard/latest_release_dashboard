#!/usr/bin/env python

import csv
import MySQLdb
import smtplib
import string
import os
import re
import time
import urllib2
from datetime import datetime
from optparse import OptionParser

def sendemail(delta, instance):
    SUBJECT = "<Attention>: Delay in dashboard updates"
    BODY = string.join ((
                        "Subject: %s" % SUBJECT ,
                        "",
                        "(This is an auto generated email)",
                        "Last updated timestamp exceeds %s minutes on %s server" % (delta, instance),
                        "Please take an immediate action item and do the needful."
                         ), "\r\n")
    server = smtplib.SMTP ('smtp.juniper.net')
    server.sendmail('dashboard-dev@juniper.net', 'dashboard-dev@juniper.net', BODY)
    server.quit()

if __name__ == '__main__':
    DEFAULT_URL      = 'https://127.0.0.1/dashboard/last-updated'
    DEFAULT_FILENAME = '/opt/log/update-delta-scrapings.csv'
    DEFAULT_INSTANCE = None

    argv_parse = OptionParser()
    argv_parse.add_option('--user-name', dest='username', default='dashboard',
                      help='specify correct username for AUTHLDAP authentication')
    # As this is a password we shouldn't give a default value.
    argv_parse.add_option('--password', dest='password', default=None,
                      help='specify correct password for AUTHLDAP authentication *** MANDATORY ARGUMENT ***')
    argv_parse.add_option('-o', '--output-file', dest='output_file', default=DEFAULT_FILENAME,
                      help='specify an alternate output file (default: ' + DEFAULT_FILENAME + ')')
    argv_parse.add_option('-u', '--url', dest='url', default=DEFAULT_URL,
                      help='specify an alternate URL (default: ' + DEFAULT_URL + ')')
    argv_parse.add_option('-d', '--database', dest='database', action='store_true', default=False,
                      help='output to the database (metroid::dashboard_stats::delta_scrapings) (default: FALSE)')
    (options, args) = argv_parse.parse_args()

    # do a little checking for proper parameters
    if options.password == None:
        raise Exception("Error: you must specify a password for URL authentication ('--password')")

    # This is the place to check which is the production server at any given point of time.
    # At Present use a common dashboard URL (which usually gives less amount of HTML output and grep for the production server name.
    URL_for_HOSTNAME = 'https://deepthought.juniper.net/dashboard/hosting-server'
    username = options.username
    password = options.password
    passman = urllib2.HTTPPasswordMgrWithDefaultRealm()
    passman.add_password(None, URL_for_HOSTNAME, username, password)

    # create the AuthHandler
    authhandler = urllib2.HTTPBasicAuthHandler(passman)
    opener = urllib2.build_opener(authhandler)
    urllib2.install_opener(opener)
    # ...and install it globally so it can be used with urlopen.
    host_name = urllib2.urlopen(URL_for_HOSTNAME).read()
    prod_ser_name = re.sub(".juniper.net", "", host_name)

    # This needs to addressed in later stages whenever we'll start using 
    # Virtual IP for Dashboard servers, so they all have the same hostname 
    # specified on that page. At which point this method will stop working

    prod_status = 1
    if os.uname()[1] != prod_ser_name:
         prod_status = 0
         prod_ser_name = os.uname()[1]

    # find the data in question
    content = urllib2.urlopen(options.url).read().strip()

    fields = ['timestamp', 'last_update_date', 'last_update_time', 'delta', 'instance']
    raw_data = content.split( )
    last_update_date = content.split( )
    raw_data[1] = datetime.strptime(content.strip(),'%Y-%m-%d %H:%M:%S')
    
    delta = int((time.mktime(datetime.now().timetuple()) - time.mktime(raw_data[1].timetuple())) / 60)
    raw_data[1] = last_update_date[1]
    raw_data.append(delta)

    d = datetime.today()
    raw_data.insert(0, d.isoformat())
    data = dict(zip(fields, raw_data))

    # Send an email alert when the delta is more than 5 hours on a 
    # production machine.
    # This script is supposed to run every minute from crontab,
    # considering only a 2 minute window to avoid spam.
    diff = int (data['delta'])
    if (diff >= 300 and diff < 302 and prod_status):
        sendemail (diff, prod_ser_name)

    # time for some outputtin...
    if options.output_file and not options.database:
        # if this file already exists, put in the headers
        if not os.path.isfile(options.output_file):
            csv_writer = csv.DictWriter(open(options.output_file, 'wb'), fields, delimiter=',', dialect='excel')
            csv_writer.writerow(dict(zip(fields, fields)))
            csv_writer.writerow(data)
        else:
            csv_writer = csv.DictWriter(open(options.output_file, 'ab'), fields, delimiter=',', dialect='excel')
            csv_writer.writerow(data)
    else:
        cursor = MySQLdb.connect(host='metroid.juniper.net', user='scraper', passwd='scrap3r', db='dashboard_stats').cursor()
        cursor.execute("insert into delta_scrapings (timestamp, last_update_date, last_update_time, delta, instance, status) values(%s, %s, %s, %s, %s, %s)", (data['timestamp'], data['last_update_date'], data['last_update_time'], data['delta'], prod_ser_name, prod_status))
