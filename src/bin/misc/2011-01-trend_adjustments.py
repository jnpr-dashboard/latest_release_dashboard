#!/usr/bin/env python
# -*- coding: utf-8 -*-

# make adjustments to the trend files for the 2011-01 rollout
# change uid on all old backlog files from backlog_pr to backlog_pr_2010
# this needs to be run after the rename of the directories

import csv
import os
import sys

from datetime import datetime, date
from copy import deepcopy
from pprint import pformat


# need to tweak path to get in SnapshotManager
snapshot_path = os.path.normpath(os.path.join(os.path.dirname(__file__),'..','..','dash','ui'))
sys.path.insert(0, snapshot_path)
from snapshot import SnapshotManager, SnapshotJsonFile, STORE_PATH, TYPE_TO_PATH

def find_user_index(data, uid):
    for count, line in enumerate(data['data']):
        if line['uid'] == uid:
            return count
    raise ValueError('uid %s not found' % uid)

for file_type in ['backlog_pr','mttr']:
    new_file_type = '%s-2010' % file_type
    print 'running %s -> %s' % (file_type, new_file_type)
    sm = SnapshotManager(new_file_type)
        
    for jfile in sm.get_files():
        if jfile.data['data'][0].has_key(file_type):
            jfile.data['data'][0][new_file_type] = jfile.data['data'][0][file_type]
            del jfile.data['data'][0][file_type]
            jfile.save()
        else:
            if not jfile.data['data'][0].has_key(new_file_type):
                print 'could not find %s or %s, did find %s' % (file_type, new_file_type, jfile.data['data'][0].keys())
        
