# make adjustments to the trend files for the 2010-07 rollout
# it's safe to run this many times if something fails

import sys
import os

from pprint import pformat

snapshot_path = os.path.normpath(os.path.join(os.path.dirname(__file__),'..','..','dash','ui'))

sys.path.insert(0, snapshot_path)

from snapshot import SnapshotManager

# insert column headers and reference row for all in past for mttr

sm = SnapshotManager('mttr')

print 'working on mttr files',
count = 0
# get all files
for jfile in sm.get_files():
    count += 1
    if count % 10000 == 0:
        print '.',
    matrix = jfile.data['matrix'][0]
    # add trend field info if not there
    matrix['trend_fields'] = {'Last Year MTTR (days)':0,
                              'Reference #CFD fixed':1,
                              'YTD MTTR (days)':2,
                              'YTD #CFD fixed':3,
                              'Current MTO (days)':4,
                              'Current #CFD open':5,
                              'Target MTTR (days)':6
                             }
    for line in jfile.data['data']:
        # add the target mttr if not already there
        if len(line['mttr']) == 6:
            line['mttr'].append({'value':'30.0'})
    jfile.save()
            
# insert column headers and clear next release for all in past for backlog_pr

sm = SnapshotManager('backlog_pr')

print '\nworking on backlog_pr files',
count = 0
# get all files
for jfile in sm.get_files():
    count += 1
    if count % 10000 == 0:
        print '.',
    #print pformat(jfile.data)
    matrix = jfile.data['matrix'][0]
    # add trend field info if not there
    matrix['trend_fields'] = {'Reference':0,
                              'Current':1,
                              'Delta':2
                             }
    for line in jfile.data['data']:
        # clear out what was Next Release and is now Delta
        line['backlog_pr'][2]['value']=''
        line['backlog_pr'][2]['prs']=[]
    jfile.save()
    