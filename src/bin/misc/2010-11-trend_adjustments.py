#!/usr/bin/env python
# -*- coding: utf-8 -*-

# make adjustments to the trend files for the 2010-11 rollout
# adding additional trending data as well as fixing up the mttr fields
# which didn't match the web page.

# it's safe to run this many times if something fails

#fst-dcg-bu merges into fst-dc
#fst-epg-bu merges into fst-ep 
#slt-op merges into slt-oe 

#ft-jt goes away
#ft-nm goes away
#slt-junicomm goes away
#ipg-partner-integration goes away


import csv
import os
import sys

from datetime import datetime, date
from copy import deepcopy
from pprint import pformat


# need to tweak path to get in SnapshotManager
snapshot_path = os.path.normpath(os.path.join(os.path.dirname(__file__),'..','..','dash','ui'))
sys.path.insert(0, snapshot_path)
from snapshot import SnapshotManager, SnapshotJsonFile, STORE_PATH, TYPE_TO_PATH

# totally delete these items
DELETE = ['ft-jt','ft-nm','slt-junicomm','junicomm','partner-integration','mfg','cussrvc-mfg']

# take data from key and put it in value, deleting the key data
MERGE = {'fst-dcg-bu':'fst-dc',
         'dcg-bu'    :'fst-dc',
         'fst-epg-bu':'fst-ep',
         'epg-bu'    :'fst-ep',
         'slt-op':'slt-oe'} 

# these are the only dates that matter
DATES = [date(2010,3,24), date(2010,4,6)]

# copy the delete base
to_delete = set(DELETE[:])

def find_user_index(data, uid):
    for count, line in enumerate(data['data']):
        if line['uid'] == uid:
            return count
    raise ValueError('uid %s not found' % uid)

for file_type in ['mttr','backlog_pr']:
    print 'running %s' % file_type
    sm = SnapshotManager(file_type)
    
        
    # for merge, get values from source and put them in destination and do the same in the junos file, then add source to to_delte 
    for dt in DATES:
        junos_file = sm.get_files('junos', dt, dt).next()
        #print junos.data
        for source, destination in MERGE.items():
            print '%s -> %s' % (source, destination)
            try:
                jfile = sm.get_files(source, dt, dt).next()
                print jfile.user, jfile.date
                #print pformat(jfile.data)
                jsource = jfile.data['data'][find_user_index(jfile.data, source)]
            except StopIteration:
                jfile = None
                jsource = None
            try:
                junos_source = junos_file.data['data'][find_user_index(junos_file.data, source)]
            except ValueError:
                junos_source = None
            # ensure that the sources match.. 
            if jsource != junos_source:
                print 'jsource=%s' % pformat(jsource)
                print 'junos_source=%s' %  pformat(junos_source)
                raise ValueError("sources didn't match")
            if jfile:
                # make new file for destination
                new = SnapshotJsonFile(load_now=False, base_path=os.path.join(STORE_PATH, TYPE_TO_PATH[file_type]), user=destination, date=dt)
                new.create()
                new.data = jfile.data 
                # adjust name parts of the file
                # get the first file found of the destination to get the name info
                dest_file = sm.get_files(destination).next()
                dest_row = dest_file.data['data'][find_user_index(dest_file.data, destination)]
                source_row = new.data['data'][find_user_index(new.data, source)] 
                source_row['displayName'] = dest_row['displayName']            
                source_row['linkDisplayName'] = dest_row['linkDisplayName']
                source_row['uid'] = dest_row['uid']
                new.data['displayName'] = dest_file.data['displayName']
                new.data['feed_url'] = dest_file.data.get('feed_url','')
                new.data['uid'] = dest_file.data['uid']
                new.save()
                # add destination row in junos file
                junos_file.data['data'].append(source_row)
                junos_file.save()    
                # add source to things to be deleted
            # we always add the source to be deleted
            to_delete.add(source)
            
            
#for deletes, need to remove actual user files + from junos for date range
print 'deletes'
for file_type in ['mttr','backlog_pr']:
    print 'running %s' % file_type
    sm = SnapshotManager(file_type)
    
        
    # remove things that are no longer needed along with things that were merged 
    for dt in DATES:
        junos_file = sm.get_files('junos', dt, dt).next()
        print dt
        for delete in to_delete:
            # delete the file
            print 'going to delete %s' % delete
            try:
                jfile = sm.get_files(delete, dt, dt).next()
            except StopIteration:
                jfile = None
            if jfile:
                if jfile.user != delete:
                    raise ValueError('jfile.user (%s) != delete (%s)' % (jfile.user, delete))
                print 'got file, doing delete'
                jfile.delete()
            # delete the row in junos
            try:
                index = find_user_index(junos_file.data, delete)
                junos_file.data['data'].pop(index)
            except ValueError:
                pass
        junos_file.save()
