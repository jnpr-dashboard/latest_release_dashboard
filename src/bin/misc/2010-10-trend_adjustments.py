#!/usr/bin/env python
# -*- coding: utf-8 -*-

# make adjustments to the trend files for the 2010-10 rollout
# adding additional trending data as well as fixing up the mttr fields
# which didn't match the web page.

# it's safe to run this many times if something fails

# Venkat's Mapping
# Reference -> Last Year MTTR (days)
# #PR Reference -> Last Year #CFD fixed
# Current -> YTD MTTR (days)
# #PR Current -> YTD #CFD fixed

# My Mapping
# Reference -> Last Year MTTR (days)
# #PR Reference -> Last Year #CFD fixed (Reference #CFD fixed in file)
# Current -> Current MTO (days) -> YTD MTTR (days)
# #PR Current -> Current #CFD open -> YTD #CFD fixed

# Spencer Says
# junos - Current MTO (days) []-> junos - YTD MTTR (days)
# junos - Current #CFD open [] -> junos - YTD #CFD fixed



import csv
import os
import sys

from datetime import datetime
from copy import deepcopy
from pprint import pformat

# need to tweak path to get in SnapshotManager
snapshot_path = os.path.normpath(os.path.join(os.path.dirname(__file__),'..','..','dash','ui'))
sys.path.insert(0, snapshot_path)
from snapshot import SnapshotManager, SnapshotJsonFile


MTTR_PAGE = {'count_type': 'all',
 'count_unique': 'yes',
 'data': [],
 'displayName': 'SLT-AABU / tapan', # change
 'graph_type': 'bar',
 'matrix': [{'MTTRlabel': {'PRC': 'YTD #CFD fixed',
                           'PRP': 'Current #CFD open',
                           'PRR': 'Last Year #CFD fixed',
                           'denominator': 'Last Year MTTR',
                           'numerator': 'YTD MTTR',
                           'projection': 'Current MTO'},
             'condition': '',
             'displayName': 'MTTR',
             'label': {'PRC': 'YTD #CFD fixed',
                       'PRP': 'Current #CFD open',
                       'PRR': 'Last Year #CFD fixed',
                       'denominator': 'Last Year MTTR (days)',
                       'numerator': 'YTD MTTR (days)',
                       'projection': 'Current MTO (days)',
                       'target': 'Target MTTR (days)'},
             'rules': {'denominator': [{'rule_id': 'history_backlog_prs'}],
                       'forward': [{'rule_id': 'forward_view_backlog_prs'}],
                       'numerator': [{'rule_id': 'current_backlog_prs'}]},
             'trend_fields': {'Last Year MTTR (days)': 0,
                              'Last Year #CFD fixed': 1,
                              'YTD MTTR (days)': 2,
                              'YTD #CFD fixed': 3,
                              'Current MTO (days)': 4,
                              'Current #CFD open': 5,
                              'Target MTTR (days)': 6
                              },
             'uid': 'mttr'}],
 'mttr_url': '/dashboard/mttr/tapan', # change
 'page_type': [{'count_type': 'all',
                'displayName': 'Include monitored,suspended and feedback',
                'uid': 'all'},
               {'count_type': 'yes',
                'displayName': 'Include feedback only',
                'uid': 'yes'},
               {'count_type': 'only',
                'displayName': 'Feedback PRs only',
                'uid': 'only'},
               {'count_type': 'monitored',
                'displayName': 'Monitored PRs only',
                'uid': 'monitored'},
               {'count_type': 'suspended',
                'displayName': 'Suspended PRs only',
                'uid': 'suspended'},
               {'count_type': 'no',
                'displayName': 'Exclude Feedback',
                'uid': 'no'}],
 'page_version': '1.0',
 'title': 'MTTR Report ',
 'uid': 'tapan', # change
 'usr': '<dash.User "tapan", score: 2>'} #change            

MTTR_ROW = {'displayName': 'SLT-AABU / tremahl', # change
           'displayed': True,
           'linkDisplayName': '/dashboard/mttr/tremahl', #change
           'mttr': [],
           'uid': 'tapan', # change
           'user': '<dash.User "tapan", score: 2>'} #change 

MTTR_COLUMN = {'background-color': 'transparent',
                     'bgcolor': 'transparent',
                     'color_text': '#000000',
                     'link': '35.7', # change
                     'value': '35.7'} # change

# insert column headers and clear next release for all in past for backlog_pr

sm = SnapshotManager('mttr')

print 'adjusting fields on on mttr files',
sys.stdout.flush()
count = 0
# get all files
for jfile in sm.get_files():
    count += 1
    if count % 10000 == 0:
        print '.',
        sys.stdout.flush()

    matrix = jfile.data['matrix'][0]
    # adjust the trend fields
    # old - leaving here for reference
    #    matrix['trend_fields'] = {'Last Year MTTR (days)':0,
    #                              'Reference #CFD fixed':1,
    #                              'YTD MTTR (days)':2,
    #                              'YTD #CFD fixed':3,
    #                              'Current MTO (days)':4,
    #                              'Current #CFD open':5,
    #                              'Target MTTR (days)':6
    #                             }
    # new
    matrix['trend_fields'] = {'Last Year MTTR (days)':0,
                              'Last Year #CFD fixed':1,
                              'YTD MTTR (days)':2,
                              'YTD #CFD fixed':3,
                              'Current MTO (days)':4,
                              'Current #CFD open':5,
                              'Target MTTR (days)':6
                             }
    jfile.save()
print

def read_data(filename):    
    datafile = open(filename,'r')
    reader = csv.reader(datafile)
    header = reader.next()
    data = {}
    for line in reader:
        row = dict(zip(header, line))
        user = row['User/BG/BU']
        counter = row['Counter']
        if not data.has_key(user):
            data[user] = {}
        for date in header[3:]:
            if not data[user].has_key(date):
                data[user][date] = {}
            data[user][date][counter] = row[date]
    return data

print 'adding new mttr data'
# do individual users, then do junos which will have everything with junos at the top
src = read_data('2010-10-mttr_trend_data.csv')
junos_data = {}
#print pformat(data)        
must_exist = ['Current #CFD open', 'Current MTO (days)', 'Last Year MTTR (days)', 'Last Year MTTR (days)']
for display_user, dates in src.items():
    display_user = display_user.strip()
    user = display_user.strip()
    if '/' in user:
        user = user.split('/')[1].strip()
    if user.lower() == display_user.lower():
        display_user = display_user.strip().lower()
        user = user.strip().lower()
    user = user.lower()
    for date, counters in dates.items():
        make_file = True
        for field in must_exist:
            if not counters.get(field):
                make_file = False
                break
        if make_file:
            d_object = datetime.strptime(date, '%m/%d/%y').date()
            data = deepcopy(MTTR_PAGE)
            data['displayName'] = display_user
            data['mttr_url'] = '/dashboard/mttr/%s' % user
            data['uid'] = user
            data['usr'] = '<dash.User "%s", score: -1>' % user
            row = deepcopy(MTTR_ROW)
            row['displayName'] = display_user
            row['linkDisplayName'] = '/dashboard/mttr/%s' % user
            row['uid'] = user
            row['user'] = '<dash.User "%s", score: -1>' % user
            row_data = []
            #print user
            # need to remap the field names from what was used in the source
            field_remap = {'Current MTO (days)' : 'YTD MTTR (days)',
                           'Current #CFD open'  : 'YTD #CFD fixed',
                           'YTD MTTR (days)'    : 'Current MTO (days)',
                           'YTD #CFD fixed'     : 'Current #CFD open',
                           
                          } 
            for field in ['Last Year MTTR (days)','Reference #CFD fixed','YTD MTTR (days)','YTD #CFD fixed','Current MTO (days)','Current #CFD open','Target MTTR (days)']:
                column_data = deepcopy(MTTR_COLUMN)
                field = field_remap.get(field, field)
                column_data['link'] = column_data['value'] = counters.get(field,'')
                row_data.append(column_data)
            row['mttr'] = row_data
            # don't add junos because it will already exist
            if not user == 'junos':
                if not junos_data.has_key(date):
                    junos_data[date] = {}
                junos_data[date][user] = row
            data['data'] = [row]
            f = SnapshotJsonFile(load_now=False, base_path='/opt/dashboard/snapshot/mttr', user=user, date=d_object)
            f.create()
            f.data = data
            #print pformat(f.data)
            f.save()
        else:
            pass
            #print 'not making file for %s %s %s' % (display_user, date, counters)

for date, users in junos_data.items():
    sorted_user_names = users.keys()
    sorted_user_names.sort()
    d_object = datetime.strptime(date, '%m/%d/%y').date()
    f = SnapshotJsonFile(base_path='/opt/dashboard/snapshot/mttr', user='junos', date=d_object)
    for user_name in sorted_user_names:
        user = users[user_name]
        display_name = user['displayName']
        #print display_name
        if not '/' in display_name:
            user['displayName'] = display_name.upper()
        f.data['data'].append(user)
    f.save()

# insert column headers and clear next release for all in past for backlog_pr


PR_PAGE = {'count_type': 'all',
 'count_unique': 'yes',
 'data': [],
 'displayName': 'SLT-AABU / tapan', # change
 'graph_type': 'bar',
 'matrix': [{'PRlabel': {'bbi': 'BBI',
                         'current': 'Current Backlog PR',
                         'diff': 'Backlog Delta (Current  - Forward)',
                         'forward': 'Forward View',
                         'reduction': ' % ',
                         'reference': 'Reference Backlog PR'},
             'condition': '',
             'displayName': 'Backlog PRs',
             'label': {'bbi': 'BBI',
                       'current': 'Current Backlog',
                       'forward': 'Pending (+&Delta) 10.3R1 PRs ',
                       'reduction': ' Reduction to date ',
                       'reference': 'Reference Backlog',
                       'target': 'Target Backlog (85% of Reference)'},
             'rules': {'current': [{'rule_id': 'current_backlog_prs'}],
                       'forward': [{'rule_id': 'diff_view_backlog_prs'}],
                       'reference': [{'rule_id': 'history_backlog_prs'}]},
             'trend_fields': {'BBI': 4,
                              'Current': 1,
                              'Delta': 2,
                              'Reference': 0,
                              'Target': 3},
             'uid': 'backlog_pr'}],
 'feed_url': '/dashboard/backlogprs/ravin', #change
 'future_release': 'unknown',
 'page_type': [{'count_type': 'all',
                'displayName': 'Include monitored,suspended and feedback',
                'uid': 'all'},
               {'count_type': 'yes',
                'displayName': 'Include feedback only',
                'uid': 'yes'},
               {'count_type': 'only',
                'displayName': 'Feedback PRs only',
                'uid': 'only'},
               {'count_type': 'monitored',
                'displayName': 'Monitored PRs only',
                'uid': 'monitored'},
               {'count_type': 'suspended',
                'displayName': 'Suspended PRs only',
                'uid': 'suspended'},
               {'count_type': 'no',
                'displayName': 'Exclude Feedback',
                'uid': 'no'}],
 'page_version': '1.2',
 'title': 'Backlog PRs Report ',
 'uid': 'ravin', # change
 'usr': '<dash.User "ravin", score: 2>'} #change            

PR_ROW = {'displayName': 'SLT-AABU / ravin', # change
           'displayed': True,
           'linkDisplayName': '/dashboard/mttr/ravin', #change
           'mttr': [],
           'uid': 'ravin', # change
           'user': '<dash.User "ravin", score: 2>'} #change 

PR_COLUMN = {'background-color': 'transparent',
                     'bgcolor': 'transparent',
                     'color_text': '#000000',
                     'link': '', # change
                     'value': ''} # change


sm = SnapshotManager('backlog_pr')

print 'adding new backlog pr data'
# do individual users, then do junos which will have everything with junos at the top
src = read_data('2010-10-backlog_trend_data.csv')
junos_data = {}
must_exist = ['Reference', 'Current']
for display_user, dates in src.items():
    display_user = display_user.strip()
    user = display_user.strip()
    if '/' in user:
        user = user.split('/')[1].strip()
    if user.lower() == display_user.lower():
        display_user = display_user.strip().lower()
        user = user.strip().lower()
    user = user.lower()
    for date, counters in dates.items():
        make_file = True
        for field in must_exist:
            if not counters.get(field):
                make_file = False
                break
        if make_file:
            d_object = datetime.strptime(date, '%m/%d/%y').date()
            data = deepcopy(PR_PAGE)
            data['displayName'] = display_user
            data['feed_url'] = '/dashboard/backlogprs/%s' % user
            data['uid'] = user
            data['usr'] = '<dash.User "%s", score: -1>' % user
            row = deepcopy(PR_ROW)
            row['displayName'] = display_user
            row['linkDisplayName'] = '/dashboard/backlogprs/%s' % user
            row['uid'] = user
            row['user'] = '<dash.User "%s", score: -1>' % user
            row_data = []
                              
            for count, field in enumerate(['Reference','Current','Delta','Target','BBI']):
                column_data = deepcopy(PR_COLUMN)
                column_data['link'] = column_data['value'] = counters.get(field,'')
                if count in [0,1] and column_data['value'] :
                    column_data['value'] = int(column_data['value'])
                row_data.append(column_data)
            row['backlog_pr'] = row_data
            # don't add junos because it will already exist
            if not user == 'junos':
                if not junos_data.has_key(date):
                    junos_data[date] = {}
                junos_data[date][user] = row
            data['data'] = [row]
            f = SnapshotJsonFile(load_now=False, base_path='/opt/dashboard/snapshot/backlogprs', user=user, date=d_object)
            f.create()
            f.data = data
            #print pformat(f.data)
            f.save()

for date, users in junos_data.items():
    sorted_user_names = users.keys()
    sorted_user_names.sort()
    d_object = datetime.strptime(date, '%m/%d/%y').date()
    f = SnapshotJsonFile(base_path='/opt/dashboard/snapshot/backlogprs', user='junos', date=d_object)
    for user_name in sorted_user_names:
        user = users[user_name]
        display_name = user['displayName']
        #print display_name
        if not '/' in display_name:
            user['displayName'] = display_name.upper()
        f.data['data'].append(user)
    f.save()
