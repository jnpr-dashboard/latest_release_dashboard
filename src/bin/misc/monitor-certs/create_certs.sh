#!/bin/sh

# creating certification for monitoring, dev-server
# 

OUT_FILE=$1

echo 'creating private key'
openssl genrsa -des3 -out ${OUT_FILE}.key 1024

echo 'creating CSR file'
openssl req -new -key ${OUT_FILE}.key -out ${OUT_FILE}.csr

# uncomment this if you don't want any passphrase.. UNSAFE!!!
#echo 'removing passphrase from key'
#cp ${OUT_FILE}.key ${OUT_FILE}.key.org
#openssl rsa -in ${OUT_FILE}.key.org -out ${OUT_FILE}.key

echo 'generate self-signed CA'
openssl x509 -req -days 365 -in ${OUT_FILE}.csr -signkey ${OUT_FILE}.key -out ${OUT_FILE}.crt

echo 'DONE....'
