#!/usr/bin/env python
# -*- coding: utf-8 -*-

# make adjustments to the trend files for the 2010-07 rollout
# it's safe to run this many times if something fails

import sys
import os

# redefine standard out to be unbuffered
#sys.stdout = os.fdopen(sys.stdout.fileno(), 'w', 0)

# joel says i'll need something like this for unbuffered utf-8 output:
#   io.open(sys.stdout.fileno(), 'w', 0, 'utf-8', os.linesep, False)

# XXX for debugging
#from pprint import pformat

snapshot_path = os.path.normpath(os.path.join(os.path.dirname(__file__),'..','..','dash','ui'))

sys.path.insert(0, snapshot_path)

from snapshot import SnapshotManager
            
# insert column headers and clear next release for all in past for backlog_pr

sm = SnapshotManager('backlog_pr')

pinwheel = ('\\', '|', '/', '-')
initial_message = 'working on backlog_pr files: [-]'

print initial_message,

count = 0

# get all files
for jfile in sm.get_files():
    count += 1
    print '\b\b\b' + pinwheel[count % len(pinwheel)] + ']',
    
    # XXX debugging   
    #print pformat(jfile.data)

    matrix = jfile.data['matrix'][0]

    # fix the trend field data
    matrix['trend_fields'] = {
        'Reference': 0,
        'Current': 1,
        'Delta': 2,
        'Target': 3,
        'BBI': 4,
    }
    
    # save our changes
    jfile.save()

print "\b\b\bdone]"