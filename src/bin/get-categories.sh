#!/bin/sh
#
# Fetch the desired subfields for all GNATS categories, and return them
# in an easily parseable format.
#
# Dirk Bergstrom, dirk@juniper.net, 2008-09-19
#
# Copyright (c) 2008, Juniper Networks, Inc.
# All rights reserved.
#
#########
if [ $1 = "-h" ]; then
    echo "Usage: get-categories.sh <query-pr path> <host> <port> <database> <subfield,subfield,...>"
    exit
fi

query_pr=$1
host=$2
port=$3
database=$4
subfields=$5

# Turn a comma-separated list of numbers into an awk printf format.
# 7,3,5 ==> '"%s|%s:%s:%s\n", $1, $7, $3, $5'
fmt=`echo $subfields | /usr/local/bin/sed -e 's/,/:/g' -e 's/[0-9]/%s/g'`
nums=`echo $subfields | /usr/local/bin/sed -e 's/,/, $/g'`
awkfmt="\"%s|${fmt}\\n\", \$1, \$${nums}"

${query_pr} --database=${database} --host=${host} --port=${port} \
    --adm-field category | /usr/bin/awk -F: "{printf $awkfmt}"
