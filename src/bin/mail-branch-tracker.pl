#!/usr/bin/env perl
#
# Retrieve all branch tracker record that has "created" as their 
#   tracker record
#
# Sanny Mulyono, smulyono@juniper.net
#
# Script will read the intended mail from
# /opt/dashboard/special/tracker-branch-mail-list.txt
#   * one line for each mail/alias
#
#
# Copyright (c) 2009-2010, Juniper Networks, Inc.
# All rights reserved.
#
#####################
use strict;
use warnings;

use Getopt::Std;
use vars qw($opt_h $opt_v $opt_V $opt_d $opt_t $opt_u $opt_C $opt_B $opt_D);

use FindBin;
use lib $FindBin::Bin;
use utils;

my $VERSION = sprintf('%d.%02d', q$Revision: 1.5 $ =~ /(\d+)\.(\d+)/);


getopts('hvdu:');
usage(0) if ($opt_h);

sub usage {
    my $code = shift;
    print <<EOM;
usage: releases.pl [-hvVd] [-u<username>] host
    -u Connect to deepthought with this username (incompatible with cert usage)
    -d Die on errors
    -v be verbose
    -V be very verbose
    -h show this message
This is version $VERSION.
EOM
    exit $code;
}

my $host = shift;

$opt_v = 1 if ($opt_V);

# Get the list of fields that the consumer desires
my @db_columns = @ARGV;

#### Start Customization ###
# Deepthought info
my $branch_table = 'branch';
#### End Customization ###

do_start();

# Get connected to the outside world.
setup_deepthought($host, $branch_table);

# set up the param
my @closed_tracker_params = (
	'skipClosed','yes',
	'state_pick', 'branch-created',
	'state','closed-%',
	'not_state','not'
);
# setup initial value
my %email_param = (
	From => 'dashboard-admin@juniper.net',
	Subject => "[Branch Tracker] Daily Summary",
);

# grab the list of recipient from file
open FHANDLE, "<", "/opt/dashboard/special/tracker-branch-mail-list.txt" or die $!;
my @recipients = <FHANDLE>;
my @recipient = ();
for my $rcpt (@recipients){
	$rcpt =~ s/\n//g;
	push(@recipient, $rcpt);
}
$email_param{"To"} =  join(',',@recipient);

# Fetch the tracker records
my @fields_to_query = ();
my @state_picklist = ();
$r->fetch_metadata();
for my $fld (@branch_tracker_metadata){
	my $fname = $fld->{'field'};
	if ($fname eq "state"){
		my %meta = $r->field_metadata($fname);
		# Collecting the state based on the metadata
		for my $state (@{$meta{'Picklist_Order'}}){
			# Array for determining the state order
			# need MANUAL checking on the branch state
			# make it the same as the query params
			if ($state ne "branch-created" 
				&&	$state !~ /closed-*/
				){
					push (@state_picklist, $state);
			}
		}
	}
    if ($fld->{'data_type'} eq "Date" && $fld->{'field'} ne "teg" && $fld->{'field'} ne "created_date" ) {
        push(@fields_to_query, $fname.'_planned', $fname.'_revised', $fname.'_actual');
    } else {
        push(@fields_to_query, $fname);
    }
}

my $query_results = do_query($r, \@closed_tracker_params, \@fields_to_query);

# Iterating them
#  --> IF NEED MORE FIELD, THEN ADD HERE
my @trackers_set = ();
my %trackers_record = ();
for my $rec (@$query_results){
	my %trackers = ();
	my $_state = $$rec{'state'};
	$trackers{'id'} = $$rec{'record_number'};;
	$trackers{'record_num'} = $$rec{'record_number'};;
	$trackers{'branch_type'} = $$rec{'branch_type'};
	$trackers{'branch_name'} =  $$rec{'branch_name'};
	$trackers{'responsible'} = $$rec{'responsible'};
	$trackers{'state'} = $$rec{'state'};
	$trackers{'created_by'} = $$rec{'created_by'};
	# create/add record to  
	#    trackers_record{"<state>}":@trackers_set}
	my @temp = ();
	if ($trackers_record{$_state}) {
		## ADD
		@temp = @{$trackers_record{$_state}};
		push(@temp, \%trackers);
	} else {
		## CREATE, push 
		push (@temp , \%trackers);
	}
	$trackers_record{$_state} = \@temp;
}


my $txt_message = "Branch Record Summary : \n";
my $html_message = "";
# construct the header
$html_message =$html_message . "<h3>Branch Tracker Daily Summary </h3>";
my @html_messages=();
for my $state (@state_picklist){
	$html_message =$html_message . "<table style='width:600px' border=1>";
	$html_message =$html_message . "<tr style='background:#CCCCCC'><td colspan=3>Branch state : <strong>$state</strong></td></tr>";
	# CONSTRUCT THE MESSAGE
	# disperse them and send email... or other stuff
	if ($trackers_record{$state}){
		$html_message = $html_message ."<tr><th style='width:20%'>Record No.</th>".
						"<th style='width:30%'>Responsible</th>".
						"<th style='width:50%'>Branch Name</th></tr> \n";
		$txt_message = $txt_message . "Branch record '".$state."' record summary : \n";
		for my $fld (@{$trackers_record{$state}}){
			my $result = construct_result($fld);
			my $html_res = $$result{"html"};
			$txt_message = $txt_message . $$result{"txt"}."\n";
			$html_message = $html_message . "<tr>" . $html_res."</tr>";
		}
	} else {
		$txt_message = $txt_message . "There is no record associated to state \'$state\' \n";
    	$html_message =$html_message . "<tr><td colspan=3 ><span style='margin-left:5px'><em>There are no record found in '$state' state</em></span></td></tr>";
	}
	$html_message = $html_message."</table><br/>\n";
}
send_email(\%email_param, $txt_message, $html_message);

# Construct the result in TXT and HTML
# adding '\n' on the end of HTML result for handling 
# exclamation error 
# more information about 'sendmail exclamation mark'
# http://www.tinyfrogsoftware.com/blog/the-story-of-the-rogue-exclamation-mark-or-how-i-lost-4-hours-of-my-life
sub construct_result{
	my $field = shift;
	my $id = $$field{'id'};
	my $responsible = $$field{'responsible'};
	my $branch_name = $$field{'branch_name'};
	my $txt= $$field{'id'}. " | ". $$field{'responsible'}. " | ".$$field{'branch_name'}." | ".$$field{'state'}." \n";
	my $html = "<td><a href='https://deepthought.juniper.net/app/do/showView?tableName=branch&taskCode=all&record_number=$id'>$id</a>".
			   " &nbsp;<a href='https://deepthought.juniper.net/app/do/showEdit?tableName=branch&taskCode=all&record_number=$id'>edit</a>".
			   "</td> ".
			   "<td>$responsible</td><td>$branch_name</td>";
	my %result=(
		txt => $txt,
		html => $html. "\n"
	);
	return \%result;	
}
 
