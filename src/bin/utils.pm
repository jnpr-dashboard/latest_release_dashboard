#!/usr/bin/env perl
#
# Utility routines for managing RLI info
#
# Dirk Bergstrom, dirk@juniper.net, 2006-10-17
#
# Copyright (c) 2006, Juniper Networks, Inc.
# All rights reserved.
#
#####################
use strict;
use warnings;

use RLI::api;


our($opt_D, $opt_v, $opt_u, $opt_d, $r, $dbh, %type_to_format,
    $RECORD_SEPARATOR, $UNIT_SEPARATOR, @release_metadata, @branch_tracker_metadata, 
    @requirements_metadata, @hardening_metadata, $dev_mode, $ib_branch_regex, $tot_regex);
package utils;
use vars qw(@ISA @EXPORT @EXPORT_OK);
use Carp;
use MIME::Lite::TT;
use URI::Escape;

require Exporter;
require AutoLoader;

@ISA = qw(Exporter);

# Items to export into callers namespace by default. Note: do not export
# names by default without a very good reason. Use EXPORT_OK instead.
# Do not simply export all your public functions/methods/constants.
@EXPORT    = qw(do_start do_exit do_query @release_metadata @branch_tracker_metadata $dev_mode
                @requirements_metadata @hardening_metadata $ib_branch_regex $tot_regex setup_deepthought boolean_populator
                date_populator float_populator text_populator
                escape_string_for_java handle_data_type emit_field_dump send_email 
                $r %type_to_format $RECORD_SEPARATOR $UNIT_SEPARATOR);
@EXPORT_OK = qw();

my $VERSION = sprintf('%d.%02d', q$Revision: 1.44 $ =~ /(\d+)\.(\d+)/);

# unbuffer outputs
select(STDERR); $| = 1;
select(STDOUT); $| = 1;

#### Start Customization ###

# Deepthought info
my $deepthought_default_host = 'deepthought.juniper.net';
my $default_table = 'Milestones';
# Path to deepthought connection certs
my $cert = '/opt/dashboard/special/dashboard.crt';
my $key = '/opt/dashboard/special/dashboard.key';

my $err_count = 0;

my $start_time = 0;
sub do_start {
    warn "Starting at ", scalar(gmtime(time)), "\n" if ($opt_v);
    $start_time = time();

    # separates records and audit trail entries
    $RECORD_SEPARATOR      = "\036";
    # separates record fields and audit trail entry fields
    $UNIT_SEPARATOR        = "\037";

    %type_to_format = (
        'int' => '?',
        'number' => '?',
        'varchar2' => '?',
        'timestamp(6) with time zone' =>
            "TO_TIMESTAMP_TZ(?, 'YYYY-MM-DD HH24:MI:SS TZHTZM')",
    );

    @release_metadata = (
        {field => "last_modified", data_type => "Date",
         data_source => "Milestones", use_in_bean => 1},
        {field => "last_modified", data_type => "Date",
         data_source => "Bn_Rn", use_in_bean => 1},
        {field => "release_branch", data_type => "Date",
         data_source => "Bn_Rn", use_in_bean => 1},
        {field => "release_branch", data_type => "Date",
         data_source => "Milestones", use_in_bean => 0},
        {field => "last_modified", data_type => "Date",
         data_source => "npi", use_in_bean => 1},
        {field => "dev_complete_two", data_type => "Date",
         data_source => "Milestones", use_in_bean => 1},
        {field => "dev_complete_one", data_type => "Date",
         data_source => "Milestones", use_in_bean => 1},
        {field => "func_spec_approved", data_type => "Date",
         data_source => "Milestones", use_in_bean => 1},
        {field => "release", data_type => "Text",
         data_source => "Milestones", use_in_bean => 1},
        {field => "release_type", data_type => "PickList",
         data_source => "Milestones", use_in_bean => 1},
        {field => "deploy_status", data_type => "PickList",
         data_source => "Milestones", use_in_bean => 1},
        {field => "responsible", data_type => "User",
         data_source => "Milestones", use_in_bean => 1},
        {field => "st_responsible", data_type => "User",
         data_source => "Milestones", use_in_bean => 1},
        {field => "blocker_threshold", data_type => "Number",
         data_source => "Milestones", use_in_bean => 1},
        {field => "beta_mid_term", data_type => "Text",
         data_source => "Milestones", use_in_bean => 1},
        {field => "state", data_type => "PickList",
         data_source => "Milestones", use_in_bean => 0},
        {field => "dev_complete_three", data_type => "Date",
         data_source => "Milestones", use_in_bean => 1},
        {field => "func_spec_complete", data_type => "Date",
         data_source => "Milestones", use_in_bean => 1},
        {field => "beta_start", data_type => "Date",
         data_source => "Milestones", use_in_bean => 1},
        {field => "systest_start", data_type => "Date",
         data_source => "Milestones", use_in_bean => 1},
        {field => "throttle_branch", data_type => "Date",
         data_source => "Milestones", use_in_bean => 1},
# changed on June 10th, 2011 by mfletcher
# FIXME remove so as to avoid clutter, maintained for backup purposes for the time being.
# For CDMv2 Purposes eco_sign is being replaced by first_factory_ship
        {field => "first_factory_ship", data_type => "Date",
         data_source => "Milestones", use_in_bean => 1},
        {field => "external_ship", data_type => "Date",
         data_source => "Milestones", use_in_bean => 1},
        {field => "build", data_type => "Date",
         data_source => "Milestones", use_in_bean => 0},
        {field => "respin", data_type => "Date",
         data_source => "Milestones", use_in_bean => 0},
        {field => "deploy", data_type => "Date",
         data_source => "Milestones", use_in_bean => 0},
        {field => "record_number", data_type => "Text",
         data_source => "Bn_Rn", use_in_bean => 1},
        {field => "release", data_type => "Text",
         data_source => "Bn_Rn", use_in_bean => 0},
        {field => "release_type", data_type => "PickList",
         data_source => "Bn_Rn", use_in_bean => 0},
        {field => "build", data_type => "Date",
         data_source => "Bn_Rn", use_in_bean => 1},
        {field => "respin", data_type => "Date",
         data_source => "Bn_Rn", use_in_bean => 1},
        {field => "deploy", data_type => "Date",
         data_source => "Bn_Rn", use_in_bean => 1},
        {field => "record_number", data_type => "Text",
         data_source => "npi", use_in_bean => 1},
        {field => "release", data_type => "Text",
         data_source => "npi", use_in_bean => 0},
        {field => "phase", data_type => "PickList",
         data_source => "npi", use_in_bean => 1},
        {field => "p0_ll_exit", data_type => "Date",
         data_source => "npi", use_in_bean => 1},
        {field => "p0_exit", data_type => "Date",
         data_source => "npi", use_in_bean => 1},
        {field => "p1_exit", data_type => "Date",
         data_source => "npi", use_in_bean => 1},
        {field => "p2_exit", data_type => "Date",
         data_source => "npi", use_in_bean => 1},
        {field => "p3_exit", data_type => "Date",
         data_source => "npi", use_in_bean => 1},
        {field => "p4_exit", data_type => "Date",
         data_source => "npi", use_in_bean => 1},
        {field => "p5_exit", data_type => "Date",
         data_source => "npi", use_in_bean => 1},
        {field => "eol", data_type => "Date",
         data_source => "Milestones", use_in_bean => 1},
        {field => "external_eol", data_type => "Date",
         data_source => "Milestones", use_in_bean => 1},
        {field => "eeol", data_type => "Date",
         data_source => "Milestones", use_in_bean => 1},
		# 	added fields for CDM
		{field => "pre_ibrg_one", data_type => "Date",
         data_source => "Milestones", use_in_bean => 1},
		{field => "pre_ibrg_two", data_type => "Date",
         data_source => "Milestones", use_in_bean => 1},
		{field => "ibrg", data_type => "Date",
         data_source => "Milestones", use_in_bean => 1},
		{field => "tprg", data_type => "Date",
         data_source => "Milestones", use_in_bean => 1},
		{field => "pre_rrg", data_type => "Date",
         data_source => "Milestones", use_in_bean => 1},
		{field => "rrg", data_type => "Date",
         data_source => "Milestones", use_in_bean => 1},
                # added fields for CDMV2
                {field => "ibfrs_checkpoint_1", data_type => "Date",
         data_source => "Milestones", use_in_bean => 1},
                {field => "ibfrs_checkpoint_2", data_type => "Date",
         data_source => "Milestones", use_in_bean => 1},
                {field => "ibfrs_checkpoint_3", data_type => "Date",
         data_source => "Milestones", use_in_bean => 1},
                {field => "ibfrs_checkpoint_4", data_type => "Date",
         data_source => "Milestones", use_in_bean => 1},
                {field => "ibfrs", data_type => "Date",
         data_source => "Milestones", use_in_bean => 1},
        # added fields from BranchTracker (branch)  
        {field => "record_number", data_type => "Text",
         data_source => "BT", use_in_bean => 1},
        {field => "branch_type", data_type => "PickList",
         data_source => "BT", use_in_bean => 0},
        {field => "branch_name", data_type => "Text",
         data_source => "BT", use_in_bean => 0},
        {field => "release_target", data_type => "Text",
         data_source => "BT", use_in_bean => 0},
        {field => "branch_manager", data_type => "User",
         data_source => "BT", use_in_bean => 1},
        {field => "branch_steward", data_type => "User",
         data_source => "BT", use_in_bean => 1},
        {field => "steward_bg", data_type => "Picklist",
         data_source => "BT", use_in_bean => 1},
        {field => "steward_bu", data_type => "Picklist",
         data_source => "BT", use_in_bean => 1},
        {field => "teg", data_type => "Date",
         data_source => "BT", use_in_bean => 1},
        {field => "last_modified", data_type => "Date",
         data_source => "BT", use_in_bean => 0},
        {field => "state", data_type => "PickList",
         data_source => "BT", use_in_bean => 1},
                # added fields for CDMV2
        {field => "collapse_start", data_type => "Date",
         data_source => "BT", use_in_bean => 1},
        {field => "collapse_end", data_type => "Date",
         data_source => "BT", use_in_bean => 1},
        {field => "build_ib_beta_1", data_type => "Date",
         data_source => "BT", use_in_bean => 1},
        {field => "deploy_ib_beta_1", data_type => "Date",
         data_source => "BT", use_in_bean => 1},
                # Added fields for junos indicate color status
        {field => "color", data_type => "PickList",
         data_source => "Bn_Rn", use_in_bean => 1},
        {field => "major_issues", data_type => "Text",
         data_source => "Bn_Rn", use_in_bean => 1},
        {field => "color", data_type => "PickList",
         data_source => "Milestones", use_in_bean => 1},
        {field => "major_issues", data_type => "Text",
         data_source => "Milestones", use_in_bean => 1},
        {field => "blocker_lockdown", data_type => "PickList",
         data_source => "Milestones", use_in_bean => 1},
        {field => "blocker_lockdown", data_type => "PickList",
         data_source => "Bn_Rn", use_in_bean => 1},
        );
 
 	@branch_tracker_metadata = (
        {field => "branch_name", data_type => "Text",
         use_in_bean => 1},
        {field => "branch_type", data_type => "PickList",
         use_in_bean => 1},
        {field => "state", data_type => "PickList",
         use_in_bean => 1},
        {field => "release_target", data_type => "Relation",
         use_in_bean => 1},
        {field => "responsible", data_type => "User",
         use_in_bean => 1},
        {field => "branch_manager", data_type => "User",
         use_in_bean => 1},
        {field => "branch_steward", data_type => "User",
         use_in_bean => 1},
        {field => "keywords", data_type => "Text",
         use_in_bean => 1},
        {field => "magic_pr", data_type => "Number",
         use_in_bean => 1},
        {field => "prs", data_type => "Text",
         use_in_bean => 1},
        {field => "platforms", data_type => "Text",
         use_in_bean => 1},
        {field => "description", data_type => "LongText",
         use_in_bean => 1},
        {field => "gatekeepers", data_type => "Text",
         use_in_bean => 1},
        {field => "branch_integrator", data_type => "User",
         use_in_bean => 1},
        {field => "sisyphus_support", data_type => "Boolean",
         use_in_bean => 1},
        {field => "tot_lock", data_type => "Text",
         use_in_bean => 1},
        {field => "lock_status", data_type => "PickList",
         use_in_bean => 1},
        {field => "commit_allowed_for", data_type => "Text",
         use_in_bean => 1},
        {field => "notify_list", data_type => "Text",
         use_in_bean => 0},
        {field => "teg", data_type => "Date",
         use_in_bean => 0},
        {field => "steward_bg", data_type => "PickList",
         use_in_bean => 1},
        {field => "steward_bu", data_type => "PickList",
         use_in_bean => 1},
        {field => "stakeholder_bg", data_type => "PickList",
         use_in_bean => 0},
        {field => "stakeholder_bu", data_type => "PickList",
         use_in_bean => 0},
        {field => "created_date", data_type => "Date",
         use_in_bean => 1},
        {field => "created_by", data_type => "User",
         use_in_bean => 1},
        {field => "collapse_start", data_type => "Date",
         use_in_bean => 1},
        {field => "collapse_end", data_type => "Date",
         use_in_bean => 1},
        {field => "build_ib_beta_1", data_type => "Date",
         use_in_bean => 1},
        {field => "deploy_ib_beta_1", data_type => "Date",
         use_in_bean => 1},
 	);
 
 	@requirements_metadata = (
        {field => "description", data_type => "LongText",
         use_in_bean => 1},
        {field => "plm_driver", data_type => "PickList",
         use_in_bean => 1},
        {field => "status", data_type => "PickList",
         use_in_bean => 1},
        {field => "release_target", data_type => "Relation",
         use_in_bean => 1},
 	);

    @hardening_metadata = (
        {field => "pr_scope", data_type => "Text",
         use_in_bean => 1},
        {field => "planned_rel", data_type => "Text",
         use_in_bean => 1},
        {field => "program_release", data_type => "PickList",
         use_in_bean => 1},
        {field => "hardng_cust", data_type => "MultiPick",
         use_in_bean => 1},
        {field => "hardng_tech", data_type => "MultiPick",
         use_in_bean => 1},
        {field => "hardening_source", data_type => "MultiPick",
         use_in_bean => 1},
    );
    _set_globals_var();
}

sub _set_globals_var{
    # simple global variable to make development easier on deepthought
    # change it here to 1 for global mode of development or on their appropriate class
    $dev_mode = 0;
    $ib_branch_regex = qr/(IB[1-5]?)\_((?#major)[1-9][0-9]?)\_((?#minor)[1-4])(\_((?#alphanumeric)\w+))?\_BRANCH$/;
    $tot_regex = qr/([1-9][0-9]?)\.([1-9][0-9]?)R1$/;
}

sub do_test {
   _set_globals_var();
   my $test_string = "IB_10_1_SLT_ROUTE_66_BRANCH";
   my $result = "";
   if ($test_string =~ ($ib_branch_regex)){
       $result = $2.".".$3.$1;
       $result .= $5||"";
   }
   warn $result;
};

#do_test();

sub do_exit {
    my $elapsed = time() - $start_time;
    warn "\nFinished at ", scalar(gmtime(time)), ", ($elapsed seconds)\n" if ($opt_v);
    if ($err_count != 0) {
        die("Finished with $err_count errors.\n");
    } else {
        exit(0);
    }
}

sub setup_deepthought {
 	my $development_deepthought_host = "deepthought.juniper.net";
    my $host_name = shift || $deepthought_default_host;
    my $table_name = shift || $default_table;
    if ($dev_mode){
    	$host_name = $development_deepthought_host;
    }
    
    # initialize the connection to Deepthought
    $r = new RLI::api($table_name, $host_name);
    unless (defined $r) {
       $err_count++; # In case we're in an eval...
       croak("Unable to initialize RLI::api");
    }
    if ($opt_u) {
        $r->username($opt_u);
    } else {
        # add credentials to access the host
        $r->add_cert($cert, $key);
    }
    # increase the timeout, since we'll be requesting large amounts of data
    $r->timeout(1200);
    if ($r->check_connection) {
        warn "Connected to Deepthought on $host_name\n" if ($opt_v);
    } else {
        $err_count++; # In case we're in an eval...
        croak("Problem connecting: ", $r->errstr(), "\n");
    }
}

sub do_query {
    my ($r, $params, $fields) = @_;

    my ($junk, $query_results) = $r->query_hash($params, $fields);
    unless ($query_results) {
        $err_count++; # In case we're in an eval...
        croak("Error: ", $r->errstr());
    }
    warn "\nRetrieved " . scalar(@$query_results) . " records.\n" if ($opt_v);

    return $query_results;
}

sub handle_data_type {
    my $col             = shift;
    my $type            = shift;
    my $i               = shift;
    my $getter_methods  = shift;
    my $populators      = shift;
    my $bean_fields     = shift;

    $col = 'clazz' if ($col eq 'class');

    $col =~ s/-/_/g;
    my $colcap = ucfirst($col);

    if ($type eq 'Date') {
        push(@$bean_fields, <<EOM);
    private Date $col;
    private String ${col}_string;
    private long ${col}_epoch = -1;
EOM
        push(@$getter_methods, <<EOM);
    public Date get${colcap}() {
        return $col;
    }
    public String get${colcap}_string() {
        return ${col}_string;
    }
    public long get${colcap}_epoch() {
        return ${col}_epoch;
    }
EOM
        push(@$populators, <<EOM);
        ${col} = dataSource.parseDate(values[$i]);
        if (${col}_epoch != ${col}.getTime()) {
            ${col}_epoch = ${col}.getTime();
            // Display TBD string for the tbd values.
            ${col}_string = (values[$i].equalsIgnoreCase("tbd")) ? 
                              "TBD" : dataSource.yyyymmdd($col);
            changed = true;
        }
EOM

    } elsif ($type eq 'Boolean') {
        push(@$bean_fields, "    private boolean $col;");
        push(@$getter_methods, <<EOM);
    public boolean get${colcap}() {
        return $col;
    }
EOM
        push(@$populators, <<EOM);
        if ($col ^ values[$i].length() == 3) {
            $col = values[$i].length() == 3;
            changed = true;
        }
EOM

    } elsif ($type eq 'Number') {
        push(@$bean_fields, "    private float $col;");
        push(@$getter_methods, <<EOM);
    public float get${colcap}() {
        return $col;
    }
EOM
        push(@$populators, <<EOM);
        float ${col}_old = $col;
        ${col} = dataSource.parseNumber(values[$i]);
        changed |= ${col}_old != $col;
EOM

    } elsif ($type =~ /PickList|Relation/) {
        push(@$bean_fields, "    private String $col;");
        push(@$bean_fields, "    private Integer ${col}_ordinal;");
        push(@$getter_methods, <<EOM);
    public String get${colcap}() {
        return $col;
    }
    public Integer get${colcap}_ordinal() {
        return ${col}_ordinal;
    }
EOM
        push(@$populators, <<EOM);
        if (! values[$i].equals($col)) {
            $col = values[$i];
            ${col}_ordinal = dataSource.picklistOrdinal("$col", $col);
            changed = true;
        }
EOM

    } elsif ($type eq 'MultiPick') {
        push(@$bean_fields, "    private String[] $col;");
        push(@$bean_fields, "    private String ${col}_mp;");
        push(@$bean_fields, "    private String ${col}_string;");
        push(@$getter_methods, <<EOM);
    public String[] get${colcap}() {
        return $col;
    }
    public String get${colcap}_string() {
        return ${col}_string;
    }
EOM
        push(@$populators, <<EOM);
        if (! values[$i].equals(${col}_mp)) {
            ${col}_mp = values[$i];
            $col = splitMultiPick(values[$i]);
            ${col}_string = mungeMultiPick(${col}_mp);
            changed = true;
        }
EOM

    } else {
        # Text, User
        push(@$bean_fields, "    private String $col;");
        push(@$getter_methods, <<EOM);
    public String get${colcap}() {
        return $col;
    }
EOM
        if ($col eq 'originator') {
            # Need to parse a username out of the email addr in the field.
            push(@$populators, <<EOM);
        String origtmp = "";
        Matcher origMatch = originatorPat.matcher(values[$i]);
        if (origMatch.find()) {
            origtmp = origMatch.group(1);
        }
        if (! origtmp.equals(originator)) {
            originator = origtmp;
            changed = true;
        }
EOM

        } elsif ($type eq 'User') {
            push(@$populators, <<EOM);
        if (! values[$i].toLowerCase().equals($col)) {
            $col = values[$i].toLowerCase();
            changed = true;
        }
EOM

        } else {
            push(@$populators, <<EOM);
        if (! values[$i].equals($col)) {
            $col = values[$i];
            changed = true;
        }
EOM
        }
    }
}

sub emit_field_dump {
    my $field_list = shift;
    my $field_types = shift;
    my $OUT = shift;
    
    for my $fld (@$field_list) {
        my $type = $field_types->{$fld};
        $fld = 'clazz' if ($fld eq 'class');
        $fld =~ s/-/_/g;
        if ($type eq 'Date') {
            print $OUT <<EOM;
        sb.append("$fld => \\"");
        if (${fld}_epoch == 0) {
            sb.append("\\"\\n");
        } else {
            sb.append($fld);
            sb.append("\\"");
            sb.append(" (");
            sb.append(${fld}_epoch);
            sb.append(")\\n");
        }
EOM
        } elsif ($type eq 'MultiPick') {
            print $OUT <<EOM;
        sb.append("$fld => \\"");
        for (String val : $fld) {
            sb.append(val);
            sb.append(" | ");
        }
        sb.append("\\"\\n");
EOM
        } else {
            print $OUT <<EOM;
        sb.append("$fld => \\"");
        sb.append($fld);
        sb.append("\\"");
EOM
            if ($type =~ /PickList|Relation/) {
                print $OUT <<EOM;
        sb.append(" (");
        sb.append(${fld}_ordinal);
        sb.append(")");
EOM
            }
        print $OUT <<EOM;
        sb.append("\\n");
EOM
    }
}

}

sub escape_string_for_java {
    my $txt = shift;
    $txt =~ s/\n/ /g;
    $txt =~ s/"/\"/g;
    return $txt;
}

sub send_email {
	my $email_param = shift;
	my $text_msg = shift;
	my $html_msg = shift;
	my %params = (
		messages => $html_msg
	);
	my %email_options = (
		EVAL_PERL => 1,
		INCLUDE_PATH => "mail_template"
	);

	my $msg = MIME::Lite::TT->new(
		From => $$email_param{"From"},
		To => $$email_param{"To"},
		Subject => $$email_param{"Subject"},
		Type => 'text/html',
		Template => 'email.html.tt',
		TmplParams => \%params,
		TmplOptions => \%email_options 		
	);
	$msg->attr("content-type.charset" => "UTF-8");
	$msg->send();
}

# Return truth
1;
