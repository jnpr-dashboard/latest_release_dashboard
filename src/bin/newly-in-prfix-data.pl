#!/usr/bin/perl

#
# Author: ravikp@juniper.net, July 02, 2013
# Copyright (c) 2013, Juniper Networks, Inc. All rights reserved.
# 

use strict;
use Data::Dumper;
BEGIN {
    push @INC, '/volume/sw-tools/lib/perl';
    $ENV{REMOTE_USER} = 'dashboard'; 
}
use collapsed_branches qw(release2query get_branch_data);
$collapsed_branches::deepthought_cert = "/opt/dashboard/special/dashboard";

# folder to create IB level files and log file in debug mode
my $prBugFix_path = "/opt/dashboard/prBugFix/"; # traling / is needed
# properties file
my $confile = '/opt/dashboard/special/dash.properties';
my $gnats_prop_file = '/opt/dashboard/special/gnats.properties';
# separates records and audit trail entries
my $RECORD_SEPARATOR      = "\036";
my $FIELD_SEPARATOR        = "\037";
my $MAX_LEVEL;
my @rels =  ();
my $prev_rel;

# Junos M.n old releases
my @OLD_RELEASES = (2,3,4,5,6,7,8,9,10,11,12);
my @G_HIERARCHY = ("R", "B"); # for scope picking order is important
my $DEBUG = 0;

# pull the PR fix releases from the /opt/dashboard/special/dash.properties file
&getReleases();
# extract query-pr path from the gnats properties file
my $query_pr_path = get_query_pr_path();
# this flag indicates magic query to pull the branches of a release apart from IB's
my $pln = 1;
# this flag indicates magic query to pull PR's only based on Planned-Release field
my $app = 1;

# log the debugging statements in debug mode
if ($DEBUG) {
    mkdir $prBugFix_path unless(-d $prBugFix_path);
    my $gfile = $prBugFix_path.'newlyinprfix.log';
	# GFH is file handle to store the select PRs to csv files in the debug mode
    open GFH, "> $gfile" or die "Couldnt write to file $gfile $! \n";
    print GFH "Release: \t @rels\n";
    print GFH "Pre Release: \t $prev_rel\n";
}

# fetch immediate previous release branches
my $imed_prev_rel_hash = get_branch_data($prev_rel, $pln, $app);
my @immediate_prev_rel_branches = keys %{$imed_prev_rel_hash};

my $rel_branch_data = {}; # stores the IB level hierarchies and queries
my $query_pr_expr = $query_pr_path.' -v dashboard -H gnats -P 1529 -f \'"%s" Number \' -e \'( product[group]=="junos" '
        .' & functional-area=="software" & (class == "bug" | class == "pre-commit-bug") & '
        .'((problem-level != "6-IL4" & problem-level !=  "5-CL4" & Blocker == "") | (Blocker != "")) ';
# query-pr to fetch all the scopes of a PR
my $query_all_scopes = $query_pr_path.' -v dashboard -H gnats -P 1529 -f \'"%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s" '
        .' Number Planned-Release State last-modified arrival-date dev-owner originator responsible submitter-id class category\' ';

# prepares the list of branches to exclude for a release
# for 13.1 you would need to explude the PR's whose scopes are child brnaches of 12.3
my $exclude_rel_branches = {};
for (my $i = 0; $i <= $#rels; $i++){
        $exclude_rel_branches->{$rels[$i]} = [];
        if ($i == 0) {
            $exclude_rel_branches->{ $rels[$i] } = \@immediate_prev_rel_branches;
        }else{
            my $branch_hash = get_branch_data($rels[$i-1], $pln, $app);
            my @branches;
            push( @branches, keys %{$branch_hash} );
            if (exists $exclude_rel_branches->{ $rels[$i-1] }) {
                push(@branches, @{$exclude_rel_branches->{$rels[$i-1]}});
            }
            $exclude_rel_branches->{ $rels[$i] } = \@branches;
        }
}

if ( $DEBUG ) {
    print GFH "Exclude Rel Branches \n";
    print GFH Dumper($exclude_rel_branches);
}

# Prepare IB level branch hierarchy and gnats queries to pull PR's
foreach my $release ( @rels ){
    # pulls the IB info
    my $branch_hash = get_branch_data($release);
    my $IB_BRANCH_LIST;
    foreach my $branchname (keys %$branch_hash){
        if ($branchname =~ /IB/){
            my $ib_magic_query = "";
            if ( $DEBUG ) {
                print GFH "\n Generating branch lists for $branchname\n";
            }
            $IB_BRANCH_LIST = get_branch_data($branchname, $pln, $app);
            if ( $DEBUG ) {
                print GFH Dumper($IB_BRANCH_LIST);
            }
            $MAX_LEVEL = getMaxLevel($IB_BRANCH_LIST);

            my $count = keys %$IB_BRANCH_LIST;
            foreach my $l (1 .. $MAX_LEVEL) {
                foreach my $ib_branch (keys %$IB_BRANCH_LIST) {
                    if ($IB_BRANCH_LIST->{$ib_branch}->{'level'} == $l) {
                        push @{$rel_branch_data->{$release}->{$branchname}->{"hierarchy"}{$l}}, $ib_branch;
                        $ib_magic_query .= " planned-release == \"$ib_branch\" |";
                    }
                }
            }
            $ib_magic_query =~ s/\|$/)\'/;
            if ($ib_magic_query ne "") {
                $ib_magic_query = $query_pr_expr.' & '.$ib_magic_query;
                $rel_branch_data->{$release}->{$branchname}->{"query"} = $ib_magic_query;
            }
        }
    }
}

# process the PR's foreach release and its branches
foreach my $release ( keys %{$rel_branch_data} ) {
    foreach my $ib ( keys %{$rel_branch_data->{$release}}) {
        if ( $DEBUG ) {
            print GFH "Start Time for $ib : \t ".localtime()."\n";
            print GFH "Query: ".$rel_branch_data->{$release}->{$ib}{"query"}."\n";
        }
        my $ib_prs = &get_gnats_pr($rel_branch_data->{$release}->{$ib}{"query"});
        #extract uniques out of returned Pr's
        my %unique_prs_hash = ();
        foreach (@$ib_prs) {
            $unique_prs_hash{$_} =1;
        }
        $ib_prs = [keys %unique_prs_hash];
        my $ret = &process_records($release, $ib, $ib_prs);
        if ( $ret && $DEBUG ) {
            print GFH "Processed records for : $ib \n";
        }
    }
}
close(GFH);
exit;

sub process_records{
    my $release = shift;
    my $ib_name = shift;
    my $ib_prs = shift;

    my $ib_prs_count = scalar @{$ib_prs};
    if ( $ib_prs_count  <= 0 ) {
        # no records in the IB no further processing is needed
        return 1;
    }
    if ( $DEBUG ) {
        print GFH "Total IB Prs: $ib_prs_count \n";
    }
    my $group = 200;
    # split the array into 200 elements each and extract all the scopes
    my $n = int($ib_prs_count/$group); # this give "n" iterations to be done
    my $previous_releases = &getPreviousReleases($release);
    my $prev_rel_regex = &getPreRegex($previous_releases);
    if ( $DEBUG ) {
        print GFH "Previuos Rel for - $release: $prev_rel_regex \n";
    }
    my $hierarchy_tmp = $rel_branch_data->{$release}->{$ib_name}->{"hierarchy"};
    my $length = keys %{ $hierarchy_tmp };
    my @hierarchy = ();
    for (my $i=1; $i<=$length; $i++) {
        push (@hierarchy, $hierarchy_tmp->{$i});
    }
    my $index_start = 0;
    my $include_prs = {};
    my $exclude_prs = {};
    for (my $i=1; $i<=$n+1; $i++) {
        my $index_end;
        if ($i>$n) {
            $index_end = $#{$ib_prs};
            print GFH "last batch $index_start to $index_end \n" if ( $DEBUG );
        }else{
            $index_end =  ($n-($n-$i))*$group;
        }
        if ($index_start > $#{$ib_prs}) {
            last;
        }
        my $all_scopes =  &featch_all_scopes(@{$ib_prs}[$index_start..$index_end]);
        if ($DEBUG) {
            print GFH "Fetching Prs from: $index_start To: $index_end \n";
            print GFH "Scopes Count: ", scalar @{$all_scopes}, "\n";
        }
        # add TOT to the regex Ex: 13.3
        my $reg_ex = '^'.$release.'$|'.$prev_rel_regex;
        my $prev_rel_branches = $exclude_rel_branches->{$release};
        foreach my $pr (@{$ib_prs}[$index_start..$index_end]) {
            # For each PR extract all the scopes
            my @pr_group = grep {/^$pr\-[\d]*/} @{$all_scopes};
            my $exclude_pr_flag = 0;
            my $record;
            ($exclude_pr_flag, $record) = &can_exclude(\@pr_group, $reg_ex, $prev_rel_branches);
            if ($exclude_pr_flag) {
                push (@{$exclude_prs->{$release}->{$ib_name}},$record);
            }else{
                #this PR is considered lets pick the right scope
                $record = &pick_scope(\@pr_group, \@hierarchy);
                push (@{$include_prs->{$release}->{$ib_name}},$record);
            }
        }
        $index_start = $index_end+1;
    }
    my $include_prs_count = scalar @{$include_prs->{$release}->{$ib_name}};
    my $exclude_prs_count = scalar @{$exclude_prs->{$release}->{$ib_name}};
    if ($DEBUG) {
        print GFH "Included PRs Count: $include_prs_count \n";
        print GFH "Excluded PRs Count: $exclude_prs_count \n";
        print GFH "End Time for $ib_name : \t ".localtime()."\n";
        write_to_file($release, $ib_name, $include_prs, 1);
    }
    write_to_console($release, $ib_name, $include_prs);
    return 1;
}

# retunrs all scopes of the PR's inputed to this method
sub featch_all_scopes{
    my @prs = @_;
    my $prs_string = join(" ", @prs);
    my $query = $query_all_scopes." ".$prs_string;
    my $all_scopes = &get_gnats_pr($query);
    return $all_scopes;
}

# prepares the previous releases list for for a release
sub getPreviousReleases{
    my $release = shift;
    my ($m, $n);
    if ($release =~ /(\d+)\.(\d+)/) {
        $m = $1;
        $n = $2;
    }
    my @all_earlier_releases = @OLD_RELEASES;
    my $diff = $m - $OLD_RELEASES[$#OLD_RELEASES];
    if ($diff>1) {
        for (my $i=1; $i<$diff; $i++) {
            push(@all_earlier_releases, $m-$i);
        }
    }
    for(my $i=$n-1; $i>0; $i--){
        push(@all_earlier_releases, "$m.$i");
    }
    return \@all_earlier_releases;
}

# Pr Group is all the scopes of a PR and this method checks planned-release
# fields of each scope and compares with earlier release to see if it can be ignored
sub can_exclude{
    my $pr_group = shift;
    my $prev_rel_regex = shift;
    my $pre_branches = shift;
    my $exists_in_prev_release = 0;
    my $ret_record;

    foreach my $record ( @{$pr_group} ) {
        my ($pr_num, $planned_rel, $state) = split(",", $record);
        #check if any of the planned-release matches immidiate previous braches
        if (grep {$_ eq $planned_rel } @{$pre_branches}) {
            $ret_record = $record;
            $exists_in_prev_release = 1;
            last;
        }
        # check if any of the planned-release matches previous releases
        if ( $planned_rel =~ /^($prev_rel_regex)/ ) {
            $ret_record = $record;
            $exists_in_prev_release = 1;
            last;
        }
    }
    return ($exists_in_prev_release, $ret_record);
}

sub getPreRegex{
    my $prev_release = shift;
    my @regex_str;
    foreach my $rel ( reverse @{$prev_release} ) {
        if ( $rel =~ /([\d]+)\.([\d]+)/ ) {
            push (@regex_str, "$1\\.$2.*");         
        }else{
            push (@regex_str, "$rel\\.[\\d]*"); 
        }
    }
    return join("\|", @regex_str);
}

# extracts the PrFix releases from the properties file
sub getReleases {
    open PROPERTIES, "<", $confile or die "Error: $!\n";
    my $rels = '';
    while (<PROPERTIES>){
        if ($_ =~ /^pr-fix-releases\s*=\s*(.*)/){
            push @rels,  split(/,/, $1);
        }
        if ($_ =~ /^pr-fix-previous-release\s*=\s*(.*)/){
            $prev_rel = $1;
        }
    }
    close PROPERTIES;
}

sub getMaxLevel {
    my $IB_BRANCH_LIST = shift;
    return -1 unless $IB_BRANCH_LIST;
    my @levels = ();
    foreach my $ib_branch (keys %$IB_BRANCH_LIST) {
        push @levels, $IB_BRANCH_LIST->{$ib_branch}->{'level'};
    }
    @levels = sort {$b <=> $a} @levels;
    if (scalar @levels > 0) {
        return shift @levels;
    }else{
        return -1;
    }
}

# sub routine to pick the correct scope of a PR
sub pick_scope{
    my $pr_group = shift;
    my $ib_level_hirarchy = shift;
    my $matched = 0;
    foreach my $g_rel (@G_HIERARCHY) {
        #matches M.nR1, M.nB1
        my $pattern = '^[\d]+\.[\d]+'.$g_rel.'([\d]+)';
        # pick the planned-release of every scope and check if it matches M.n + pattern
        my @pr_scopes = grep { $_ if( (split(",", $_))[1] =~ /$pattern/) } @{$pr_group};
        if (scalar @pr_scopes == 1) {
            $matched = 1;
            return $pr_scopes[0];
        }elsif( scalar @pr_scopes > 1 ) {
                # pick the highest R or B scope
                #picks the max scopes means in 13.3R1, 13.3R2 it picks 13.3R2
                #if multiple scopes has same R/B type ex: 13.3R1-S1, 13.3R1S2 function returns
                # $m_count as positive values it means it requires further processing
                my ($tmp_scope, $m_count) = &pick_max_rel_scope(\@pr_scopes, $pattern);
                if ( $m_count ) {
                    #further extract all the highest(equal) R/B scopes
                    my $eq_pattern = '^[\d]+\.[\d]+'.$g_rel.$m_count;
                    my @eq_scopes = grep { $_ if( (split(",", $_))[1] =~ /$eq_pattern/i) } @pr_scopes;
                    # matches the scopes like 13.3R1-S1, 13.3R1S2
                    my $add_pattern = '\-\w(\d+)';
                    my ($tmp_scope, $m_count) = &pick_max_rel_scope(\@eq_scopes, $pattern, $add_pattern);
                    $matched = 1;
                    return $tmp_scope;
                }
                $matched = 1;
                return $tmp_scope;
            } # elsif end
    } # foreach end
    # match the IB and DEV branch scopes to pick
    unless ($matched) {
        foreach my $branch_set (@{$ib_level_hirarchy}) {
            foreach my $branch ( @{$branch_set} ) {
                # pick the planned-release of every sope and check if it matches the branches
                my @matched_scopes = grep {$_ if( (split(",", $_))[1] eq $branch)} @{$pr_group};
                if (scalar @matched_scopes >= 1) {
                    $matched = 1;
                    return $matched_scopes[0];
                }
            }
        }
    }
    unless ($matched) {
        return undef;
    }
}

sub pick_max_rel_scope{
    my $pr_scopes = shift;
    my $pattern = shift;
    my $add_pattern = shift || undef;
    my @scopes = @{$pr_scopes};
    my $tmp_scope;
    $tmp_scope = $scopes[0];
    my $number;
    #extract number from scopes ex: For 13.3R4 it picks "4"
    if (defined $add_pattern) {
        $pattern = $pattern.$add_pattern;
    }
    if((split(",", $scopes[0]))[1] =~ /$pattern/i){
        if (defined $add_pattern) {
            $number = $2;
        }else{
            $number = $1;
        }
    }
    # m_count varialble to see if a same release is repeated more than once
    my $m_count = 0;
    for (my $i=1; $i<=$#scopes; $i++){
        #extract number from scopes ex: For 13.3R4 it picks "4"
        my @temp = split(",", $scopes[$i]);
        if ( $temp[1] =~ /$pattern/i) {
            if (defined $add_pattern) {
            #extract number from scopes ex: For 13.3R4-S2 it picks "2"
                if ($number < $2) {
                    $number = $2;
                    $tmp_scope = $scopes[$i];
                }elsif($number == $2){
                    $m_count++;
                }
            }else{
            #extract number from scopes ex: For 13.3R4 it picks "4"
                if ($number < $1) {
                    $number = $1;
                    $tmp_scope = $scopes[$i];
                }elsif($number == $1){
                    $m_count++;
                }
            }
        }
    }
    if ($m_count >= 1 ) {
        return ($tmp_scope, $number);
    }else{
        return ($tmp_scope, undef);
    }
}

# runs the gnats query and returns PR records
sub get_gnats_pr{
    my $query = shift;
    open QFH, "$query 2>/dev/null |" or die "could not open the query-pr $! \n";
    my @data = ();
    while (<QFH>) {
        chomp();
        push (@data, $_);
    }
    close(QFH);
    return \@data;
}

# stores processed records included/excluded foreach IB
sub write_to_file{
    my $release = shift;
    my $ib_name = shift;
    my $pr_data = shift;
    my $include_flag = shift;

    my $file_name;
    if ( $include_flag ) {
        $file_name = $ib_name.'.csv';
    }else{
        $file_name = $ib_name.'_Excluded.csv';
    }
    $file_name = $prBugFix_path.$file_name;
    open FH, "> $file_name" or die "Couldnt write to file $file_name $! \n";
    foreach my $record ( @{ $pr_data->{$release}->{$ib_name} } ) {
        if ($record) {
            print FH "$record\n";
        }
    }
    close(FH);
}

# writes processed records to console foreach IB
sub write_to_console{
    my $release = shift;
    my $ib_name = shift;
    my $pr_data = shift;
    foreach my $record ( @{ $pr_data->{$release}->{$ib_name} } ) {
        if ($record) {
            $record = $record.",$ib_name,$ib_name";
            $record =~ s/,/$FIELD_SEPARATOR/g;
            print $record . $FIELD_SEPARATOR . $RECORD_SEPARATOR ."\n";
        }
    }
}

# extracts the query-pr path from the gnats.properties file
sub get_query_pr_path {
    open FH, "<", $gnats_prop_file or die "Error: $!\n";
    my $query_pr_path = '';
    while (<FH>){
        if ($_ =~ /^query_pr\s*=\s*(.*)/){
            $query_pr_path = $1;
            last;
        }
    }
    close FH;
    return $query_pr_path;
}
