#!/bin/sh -x
#
# Regenerate all record beans.
#
# Dirk Bergstrom, dirk@juniper.net, 2008-02-01
#
# Copyright (c) 2008, Juniper Networks, Inc.
# All rights reserved.
#
#########

echo PRRecord.java
./gen-pr-bean.pl -v

echo ClosedPRRecord.java
./gen-pr-bean.pl -vC

echo HardeningClosedPRRecord.java
./gen-pr-bean.pl -vR

echo ReleaseRecord.java
./gen-release-beans.pl -v

echo RLIRecord.java
./gen-rli-bean.pl -v

echo OldPRRecord.java
./gen-old-pr-bean.pl -v

echo BTRecord.java
./gen-bt-bean.pl -v

echo NPIRecord.java
./gen-npi-bean.pl -v

echo ReleaseHardeningRecord.java
./gen-hardening-bean.pl -v

#==PPM==.... UNCOMMENT LINE BELOW FOR PPM PROJECT
#echo RequirementsRecord.java
#./gen-requirements-bean.pl -v

echo Finished
