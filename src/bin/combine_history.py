#!/bin/env python
# used to process multiple history files created by generate_old-pr-data.pl
# this will add line numbers and an extract date to the information already gathered
# and ensure that it can be loaded by OldPRRecord correctly

import os
import sys

from datetime import datetime
from pprint import pformat

# This is the number of expected fields which are all of the fields in the extract including the prepended
# number and extract date
# so line number + extract date + 35 extracted fields 
NUM_FIELDS = 37

# get output, input/extract pairs

def handle_error(message):
    print 'ERROR: %s' % message
    print 'combine_history.py <input_output_file> <input_file> <extract_date> [<input_file> <extract_date ... ]'
    print 'extract_date must be in the form YYYY-MM-DD'
    sys.exit()
    
if len(sys.argv) <= 3:
    handle_error('input_output_file, the first input_file/extract_date are required')

if len(sys.argv) % 2 == 1:
    handle_error('input files and extract dated must be in pairs')

input_output_filename = sys.argv[1]
input_pairs = []
for x in range(1,((len(sys.argv)-2)/2) + 1):
    filename, date =  sys.argv[2 * x], sys.argv[2 * x + 1] 
    try:
        datetime.strptime(date, '%Y-%m-%d')
    except ValueError:
        handle_error('%s is not a valid date in the format of YYYY-MM-DD' % date)  
    input_pairs.append([filename, date])
print input_output_filename
print input_pairs

# format of line will be line-number, extract-date, <rest of fields>
# <rest of fields> starts with (pr) number and then the rest of the fields
# in alpha order 

if not os.path.exists(input_output_filename):
    #create it
    open(input_output_filename,'w+').close()
    
input_output = open(input_output_filename, 'r')

# work through the existing file and find the highest line number and ensure it's a valid file
max_line_number = 0
for line_number, line in enumerate(input_output):
    row = line[:-1].split('\x1f')
    if len(row) != NUM_FIELDS:
        raise ValueError('line number %s does not have the expected %s fields, found %s\nline: %s' % (line_number, NUM_FIELDS, len(row), row))
    try:
        line_no = int(row[0])
    except ValueError:
        raise ValueError('the first field on line number %s is not a valid integer\nline: %s' % (line_number, row))
    if line_no > max_line_number:
        max_line_number = line_no
        
input_output = open(input_output_filename, 'a')

# for each of the input /extract pairs, add to input_outptu

for input_filename, extract_date in input_pairs:
    for line in open(input_filename, 'r'):
        max_line_number += 1
        #print repr(line)
        row = line[:-1].split('\x1f')
        input_output.write('%s\x1f%s\x1f%s\n' % (max_line_number, extract_date, '\x1f'.join(row)))


    
