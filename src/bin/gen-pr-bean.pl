#!/usr/bin/env perl
#
# Generate PRRecord bean.
#   + ClosedPRRecord
#
# Dirk Bergstrom, dirk@juniper.net, 2007-03-19
#
# Copyright (c) 2007, Juniper Networks, Inc.
# All rights reserved.
#####################
use strict;
use warnings;

use Getopt::Std;
use vars qw($opt_h $opt_v $opt_V $opt_d $opt_D $opt_C $opt_R);
getopts('hvVdDCR');

use FindBin;
use lib $FindBin::Bin;
use utils;

my $VERSION = sprintf('%d.%02d', q$Revision: 1.63 $ =~ /(\d+)\.(\d+)/);

my $DEFAULT_BEAN_DIR = '../net/juniper/dash/data/';

my $CLASSNAME = 'PRRecord';

my $outdir = shift || $DEFAULT_BEAN_DIR;

usage(0) if ($opt_h);

sub usage {
    my $code = shift;
    print <<EOM;
usage: gen-pr-bean.pl [-hvVdDC]  [<bean file>]
    -d Die on errors
    -v be verbose
    -V be very verbose
    -D connect to dev database
    -h show this message
    -C apply for closed PR
    -R apply for Hardening closed PR
This is version $VERSION.

<bean file> defaults to $DEFAULT_BEAN_DIR.
EOM
    exit $code;
}

$opt_v = 1 if ($opt_V);

my %fields = (
'state' => 'PickList',
'synopsis' => 'Text',
'reported-in' => 'Text',
'planned-release' => 'Text',
'committed-release' => 'Text',
'conf-committed-release' => 'Text',
'blocker' => 'Text',
'attributes' => 'Text',
'class' => 'PickList',
'functional-area' => 'PickList',
'product' => 'PickList',
'category' => 'PickList',
'problem-level' => 'Text',
'found-during' => 'Text',
'responsible' => 'User',
'systest-owner' => 'User',
'dev-owner' => 'User',
'techpubs-owner' => 'User',
'originator' => 'User',
'customer-escalation' => 'Boolean',
'branch' => 'Text',
'target' => 'Text',
'keywords' => 'Text',
'updated' => 'Date',
'feedback-date' => 'Date',
'confidential' => 'Boolean',
'rli' => 'Text',
'fix-eta' => 'Date',
'submitter-id' => 'Text',
'cust-visible-behavior-changed' => 'Text',
'hardening-program' => 'Text',
'resolution' => 'PickList',
'test-gap-analysis-status' => 'Text',
);

my %reduced_fields = (
'state' => 'PickList',
'reported-in' => 'Text',
'planned-release' => 'Text',
'problem-level' => 'Text',
'branch' => 'Text',
'committed-release' => 'Text',
'conf-committed-release' => 'Text',
'blocker' => 'Text',
'attributes' => 'Text',
'class' => 'PickList',
'category' => 'PickList',
'product' => 'PickList',
'responsible' => 'User',
'systest-owner' => 'User',
'dev-owner' => 'User',
'techpubs-owner' => 'User',
'originator' => 'User',
'updated' => 'Date',
'submitter-id' => 'Text',
'hardening-program' => 'Text',
'resolution' => 'PickList',
);

do_start();

my @getter_methods = ();
my @populators = ();
my @bean_fields = ();
my @field_list = ();
my $i = 3;
my %fields_list = ($opt_C or $opt_R) ? %reduced_fields : %fields;
for my $col (sort(keys(%fields_list))) {
    # These are hardcoded as the first fields; they're needed for processing.
    next if ($col =~ /^(last-modified|number|arrival-date)$/);

    push(@field_list, $col);
    handle_data_type($col, $fields{$col}, $i, \@getter_methods,
                     \@populators, \@bean_fields);
    if ($col eq 'responsible') {
        # Populator for responsible is special
        $populators[$#populators] =~ s/responsible/real_responsible/g;
    }
    $i++;
}

my $today = scalar(gmtime(time));
my $this_year = (gmtime(time))[5] + 1900;

#set up the query for closedPR or not
my $DEFAULT_PR_QUERY = 'product[group]==\\"junos\\" & state!=\\"closed\\" ';
my $CLOSED_PR_QUERY =  'product[group]==\\"junos\\" & state==\\"closed\\" & class==\\"bug\\" & resolution=\\"fixed\\"' .
                       # Additional criteria used by PR Bug Fix
                       ' & (attributes~\\".*regression-pr.*\\" | problem-level=\\"2-CL2|4-CL3|4-IL2|5-IL3\\")'.
                       ' & category!~\\".*hw-.*\\" & category!~\\".*build.*\\"';

my $HARDENING_CLOSED_PR_QUERY =  'product[group]==\\"junos\\" & state==\\"closed\\" & class==\\"bug\\" & resolution=\\"fixed\\"' .
                       ' & hardening-program!=\\"\\"'.' & category!~\\".*hw-.*\\" & category!~\\".*build.*\\"';

# debug queries for faster development (NB! These are APPENDED to the ones above!)
my $DEBUG_PR_QUERY =   '& ( planned-release ~ \\"11\\\.4\\" )';
my $DEBUG_CLOSED_PR_QUERY =   '& ( planned-release ~ \\"11\\\.4\\" )';
my $HARDENING_DEBUG_CLOSED_PR_QUERY =   '& ( planned-release ~ \\"11\\\.4\\" )';

# determining either HardeningClosedPRRecord, ClosedPRRecord or PRRecord
$CLASSNAME = $opt_R ? "HardeningClosed".$CLASSNAME : $opt_C ? "Closed".$CLASSNAME : $CLASSNAME; 
my $PR_QUERY = $opt_R ?  $HARDENING_CLOSED_PR_QUERY.$HARDENING_DEBUG_CLOSED_PR_QUERY : 
               $opt_C ? $CLOSED_PR_QUERY : $DEFAULT_PR_QUERY;
my $DEBUG_QUERY = $opt_R ? $HARDENING_CLOSED_PR_QUERY.$HARDENING_DEBUG_CLOSED_PR_QUERY :
                  $opt_C ? $CLOSED_PR_QUERY.$DEBUG_CLOSED_PR_QUERY : $PR_QUERY.$DEBUG_PR_QUERY;
# just to make the title of the class more obvious
my $class_title = $opt_R ? "HardeningClosed" : $opt_C ? "CLOSED" : "";


my$OUT;
open($OUT, '>', $outdir."/$CLASSNAME.java") ||
    die("Can't write to $outdir/$CLASSNAME.java.");

print $OUT <<EOM;
/**
 * Autogenerated self-populating value bean for a JUNOS RLI.
 *
 * Copyright (c) $this_year, Juniper Networks, Inc.
 * All rights reserved.
 */
package net.juniper.dash.data;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.Collections;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net.juniper.dash.DashboardException;
EOM

if ($opt_C){
   print $OUT <<EOM;
import net.juniper.dash.Repository;
EOM
}

print $OUT <<EOM;
/**
 *  A JUNOS $class_title PR
 */
public class $CLASSNAME extends Record implements ScopedRecord, SharedPRRecord {

    public static final String queryExpression = "$PR_QUERY";

    public static final String debugQueryExpression = "$DEBUG_QUERY";

    private static Pattern releasePat = Pattern.compile(RELEASE_REGEX,
        Pattern.CASE_INSENSITIVE | Pattern.COMMENTS);

    // Regex to pull usernames out of email addresses
    private static final String ORIGINATOR_REGEX =
        "(?:\\\\s*<.+?>)?\\\\s*([\\\\w\\\\-]+)(\@|\\\\s*\$)";
    private static Pattern originatorPat = Pattern.compile(ORIGINATOR_REGEX);

    // Find the m and n in "_mn_(T|B|R1)", where m might be 1 or 2 digits
    private static Pattern branchReleasePat =
        Pattern.compile("_([1-9]?[0-9])([0-9])_(T|B|R1)", Pattern.CASE_INSENSITIVE);
// end BSC

    private static final String[] fieldList = new String[] {
        "number",
        "last-modified",
        "arrival-date",
EOM
for my $fld (@field_list) {
    print $OUT "        \"$fld\",\n";
}
print $OUT <<EOM;
    };

    public static String[] getFieldList() {
        return fieldList;
    }

// Backlog PR stuff
    private boolean possibleBacklog=false;
    private boolean possibleForwardViewBacklog=false;
    private String backlog_pr_dev_owner;
    private String backlog_pr_responsible;
    private String backlog_pr_techpub_owner;
// End Backlog PR stuff
   
// Check if the PR has No Milestones Release   
    private boolean NotMilestonesRelease;
    
    private boolean BlankDevowner;
   
    private Set<String> releases;
    private String release_chosen;

    private Set<String> blocker_types;
    private String blocked_release;
    
    // Identify the dev/pvt branches in release or branch
    private String branchReleaseType = "";
    private String branchRelease = "";

    // Which field are we using to determine release?
    private String release_field;

    private String real_responsible;
    private String category_spoc;
    private boolean non_junos_responsible = false;

    private Date last_modified;
    private String last_modified_string;
    private long last_modified_epoch;
    
    private Date arrival_date;
    private String arrival_date_string;
    private long arrival_date_epoch;
        
    private String containingRecordId;
    

    // FIXME remove this when the field is added to gnats
    private String npi = "";

EOM

if (!$opt_C and !$opt_R){
print $OUT <<EOM;
    private Set<String> rli_nums;
EOM
} 

print $OUT join("\n", @bean_fields);
print $OUT <<EOM;

    public $CLASSNAME(String[] values, DataSource dataSource)
            throws DashboardException {
        super(values, dataSource);
        populate(values);
    }

	/**
	 * \@return if this PR is possible backlog PR
	 */
	public boolean isPossible_backlog(){
		return possibleBacklog;
	}

    /**
     * \@return if this PR is possible Future backlog PR
     */
    public boolean isPossible_forward_view_backlog(){
        return possibleForwardViewBacklog;
    }

	/**
	 * 
	 * \@return  whether the release associated to PR
	 *          is non-milestones Release
	 */
	public boolean isNot_milestones_release(){
		return NotMilestonesRelease;
	}


	public String getBacklog_pr_responsible(){
		return backlog_pr_responsible;
	}


    public String getBacklog_pr_dev_owner(){
        return backlog_pr_dev_owner;
    }

    public String getBacklog_pr_techpub_owner() {
        return backlog_pr_techpub_owner;
    }
     
    /**
     * 
     * \@return  True if dev_owner is empty
     */
    public boolean isBlank_devowner(){
        return BlankDevowner;
    }



    /**
     * \@return the real_responsible
     */
    public String getReal_responsible() {
        return real_responsible;
    }

    /**
     * \@return the category_owner
     */
    public String getCategory_spoc() {
        return category_spoc;
    }

    /**
     * \@return the non_junos_responsible
     */
    public boolean isNon_junos_responsible() {
        return non_junos_responsible;
    }

    public Date getLast_modified() {
        return last_modified;
    }
    public String getLast_modified_string() {
        return last_modified_string;
    }
    public long getLast_modified_epoch() {
        return last_modified_epoch;
    }

    public Date getArrival_date() {
        return arrival_date;
    }
    public String getArrival_date_string() {
        return arrival_date_string;
    }
    public long getArrival_date_epoch() {
        return arrival_date_epoch;
    }

    public String getContainingRecordId() {
        return containingRecordId;
    }

    // FIXME remove this when the field is added to gnats
    public String getNpi() {
        return npi;
    }

    // Keep this even after the npi field is added.
    public void setNpi(String newval) {
        npi = (null == newval ? "" : newval);
    }

EOM
print $OUT join("\n", @getter_methods);

print $OUT <<EOM;

    public boolean populate(String[] values) throws DashboardException {
        if (null == values) {
            throw new DashboardException("Null input.");
        } else if (values.length != fieldList.length + 1) {
            // values always comes with a throwaway on the end
            throw new DashboardException("Wrong number of elements: Got " +
                values.length + ", expected " + (fieldList.length + 1));
        }
        boolean changed = false;
        lastUpdated = new Date();
        String lastModifiedString = values[1];
        // going to need arrival date for some pre-processing
        String ArrivalDateString = values[2];
        
        if (lastModifiedString.trim().length() == 0) {
            // Never been modified, use arrival-date
            lastModifiedString = values[2];
        }
        Date newLastModified = dataSource.parseDate(lastModifiedString);
        Date newArrivalDate = dataSource.parseDate(ArrivalDateString);
        if (! newLastModified.equals(lastModified)) {
            lastModified = newLastModified;
            changed = true;
        }
        
        /**
         * This should never happened... if it does there are
         * some discrepany in GNATS system
         */
        if (! newArrivalDate.equals(arrival_date)){
            arrival_date = newArrivalDate;
            changed = true;
        }

        last_modified = lastModified;
        last_modified_epoch = last_modified.getTime();
        last_modified_string = dataSource.yyyymmdd(last_modified);
        
        arrival_date_epoch = arrival_date.getTime();
        arrival_date_string= dataSource.yyyymmdd(arrival_date);

        containingRecordId = findPRNum(id);
EOM
print $OUT join('', @populators);

print $OUT <<EOM;
        String category_spoc_old = category_spoc;
        category_spoc = DataSource.CATEGORY_CONTACTS.getContacts().get(category);
        if (null == category_spoc ) {
            category_spoc = "deprecated_category";
        } else if (!DataSource.USERSOURCE.getRealUsernames().contains(category_spoc) &&
	                !DataSource.USERSOURCE.getUserNames().contains(category_spoc)) {
        	// user is non_user --> leave company or not found in ldap
            category_spoc = "non_user";
        }
        changed |= ! category_spoc.equals(category_spoc_old);

		/**
		 * for DOC backlog PR, we will need to identify the category techpub owner 
		 */
		String categoryTechpubOwner = DataSource.CATEGORY_CONTACTS.getContacts().get(category);
		if (null == categoryTechpubOwner){
			categoryTechpubOwner = "deprecated_category";
		} else if (!DataSource.USERSOURCE.getRealUsernames().contains(categoryTechpubOwner) &&
	                !DataSource.USERSOURCE.getUserNames().contains(categoryTechpubOwner)) {
        	// techpubOwner is non_user --> leave company or not found in ldap, so use category SPOC
            categoryTechpubOwner = category_spoc;
        }
        if (category_spoc.length() > 0 &&
            ! DataSource.USERSOURCE.getRealUsernames().contains(real_responsible)) {
            /* The responsible party isn't a real person.  We'll  pretend that
             * it's the category contact. */
            changed |= ! category_spoc.equals(responsible);
            responsible = category_spoc;
        } else {
        	changed |= ! real_responsible.equals(responsible);
            responsible = real_responsible;
        }

        boolean old_non_junos_responsible = non_junos_responsible;
        if (DataSource.USERSOURCE.getRealUsernames().contains(real_responsible) && 
            ! DataSource.USERSOURCE.getUserNames().contains(real_responsible)) {
            /* responsible is a person, but not in JUNOS engineering.
             * We list these PRs separately for the category owner to keep
             * track of.  
             * */
            non_junos_responsible = true;
        } 
        changed |= ! (non_junos_responsible == old_non_junos_responsible);

		/**********************************************************************************************************************
		 * PR Backlog Block Start
		 * This block is copied into OldPRRecord and if this is modified, the changes also need to be made there
		 * OldPRRecord needed to make some adjustments based on the release, so a straight copy and paste will not work
		 * look for the comments about where there are differences in OldPRRecord
		 */

        /**
         * Backlog PR checks, to accomodate changing on Release Records 
         * 
         * determine who own backlog PR, so only 1 BU/BG/User will own it
         * unlike other rules which
         * 
         * IF non_junos_responsible Look for SPOC 
         * IF dev-owner empty/invalid/non-junos Look for SPOC
         */
        String old_backlog_pr_dev_owner = backlog_pr_dev_owner;
        backlog_pr_dev_owner = dev_owner;
        if (DataSource.USERSOURCE.getRealUsernames().contains(dev_owner) &&
                ! DataSource.USERSOURCE.getUserNames().contains(dev_owner)) {
        	backlog_pr_dev_owner = category_spoc;
        } else {
	        if ((dev_owner.trim().length() > 0) & (!dev_owner.equals("-"))) {
	        	// dev_owner value never been checked before, so check if dev_owner is valid
	            if (!DataSource.USERSOURCE.getRealUsernames().contains(dev_owner) &&
	                    !DataSource.USERSOURCE.getUserNames().contains(dev_owner)) {
	            	// dev_owner is non_junos responsible
	            	backlog_pr_dev_owner = category_spoc;
	            } else {
	            	// dev_owner is part of the user_names in USERSOURCE
	            	backlog_pr_dev_owner = dev_owner;
	            }
	        } else {
	        	// dev_owner is empty or non value
	        	backlog_pr_dev_owner = category_spoc;
	        }
        }
        if (! backlog_pr_dev_owner.equals(old_backlog_pr_dev_owner)){
        	changed = true;
        }

        /**
         * Backlog PR checks, to accomodate changing on Release Records 
         * 
         * determine who own backlog PR, so only 1 BU/BG/User will own it
         * unlike other rules which
         * 
         * IF non_junos_responsible Look for SPOC 
         * IF responsible empty/invalid/non-junos Look for SPOC
         */
        String old_backlog_pr_responsible = backlog_pr_responsible;
        backlog_pr_responsible = responsible;
        if (DataSource.USERSOURCE.getRealUsernames().contains(responsible) &&
                ! DataSource.USERSOURCE.getUserNames().contains(responsible)) {
            backlog_pr_responsible = category_spoc;
        } else {
            if ((responsible.trim().length() > 0) & (!responsible.equals("-"))) {
                if (non_junos_responsible) {
                    backlog_pr_responsible = category_spoc;                 
                } else {
                    backlog_pr_responsible = responsible;
                }
            } else {
                // responsible is empty or non value
                backlog_pr_responsible = category_spoc;
            }
        }
        if (! backlog_pr_responsible.equals(old_backlog_pr_responsible)){
            changed = true;
        }  
 
         /**
         * Doc & CVBC Backlog PR checks
         * Determine the owner of DOC / CVBC Backlog PR by using this logic:
         * - techpub-owner
         * - IF techpub-owner is EMPTY/INVALID/NON_JUNOS_RESPONSIBLE THEN look to backlog_pr_dev_owner
         */
        String old_backlog_pr_techpubs_owner = backlog_pr_techpub_owner;
        backlog_pr_techpub_owner = techpubs_owner;
        if (DataSource.USERSOURCE.getRealUsernames().contains(techpubs_owner) && 
                ! DataSource.USERSOURCE.getUserNames().contains(techpubs_owner)) {
            backlog_pr_techpub_owner = category_spoc;
        } else {
            if ((techpubs_owner.trim().length() > 0) & (! techpubs_owner.equals("-"))){
                if (!DataSource.USERSOURCE.getRealUsernames().contains(techpubs_owner) &&
                        !DataSource.USERSOURCE.getUserNames().contains(techpubs_owner)) {
                    backlog_pr_techpub_owner = category_spoc;
                } else {
                    backlog_pr_techpub_owner = techpubs_owner;
                }
            } else {
                backlog_pr_techpub_owner = category_spoc;
            }
        }
        if (! backlog_pr_techpub_owner.equals(old_backlog_pr_techpubs_owner)){
            changed = true;
        }
              
        /*
         * Determine if the PR is possible a backlog PR
         * the flag try to mimic this expression
         * (release=="" & reported-in~"(^| )('active_shipped_releases')") | release="'active_shipped_releases'")'
         * 
         * We run this everytime since the changed field might not affect the active shipped release
         */
        String backlog_release_value = "";
        if ((planned_release.trim().length() > 0) & (!planned_release.equals("-"))) {
            backlog_release_value = planned_release;
        }else {
            backlog_release_value = reported_in;
        }
        Pattern pat = releasePat;
        boolean old_possibleBacklog = possibleBacklog;
        boolean old_possibleForwardViewBacklog = possibleForwardViewBacklog;
        possibleBacklog = false;
        possibleForwardViewBacklog = false;
        for (String rel : backlog_release_value.split("\\\\s+")) {
            Matcher mat_backlog = pat.matcher(rel);
            if (mat_backlog.lookingAt()) {
                String mDotN = mat_backlog.group(1);
                String type = "";
                type = mat_backlog.group(2); // need the type for cdm releases
                if (type != null ){
                	type = type.toUpperCase();
                	if (type.startsWith("IB")){
						//mDotN += type;
                	}
                } 
                if (DataSource.RELEASES.getShippedReleaseIds().contains(mDotN)) {
                	possibleBacklog = true;
                } else {
                	possibleBacklog = false;
                }
                if (DataSource.RELEASES.getFutureReleaseId().equalsIgnoreCase(mDotN)) {
                    possibleForwardViewBacklog = true;
                } else {
                    possibleForwardViewBacklog = false;
                }
            }
        }
        if (!(old_possibleBacklog == possibleBacklog) ||
        	!(old_possibleForwardViewBacklog == possibleForwardViewBacklog)){
        	changed = true;
        }

        /**********************************************************************************************************************
		 * PR Backlog Block End
		 **********************************************************************************************************************/

        /** 
         * Changes in Branch Tracker and Release Target need to somehow trigger the PR changing
         *    group (RLI1664)
         * 
         * which field shows pvt/dev branch value? 
         */
        
        String branchRelease_value = "";
        String old_branchReleaseType = branchReleaseType;
        String old_branchRelease = branchRelease;
        if (branch.length()>0){
            branchRelease_value = branch;
        } else if (planned_release.length() > 0){
            branchRelease_value = planned_release;
        } 
        if (branchRelease_value.length() > 0) {
            branchReleaseType = "";
            if (branchRelease_value.matches("DEV_.*")){
                branchReleaseType = "DEV";
                branchRelease = DataSource.RELEASES.getParentRecord(branchRelease_value);
            } else if (branchRelease_value.matches("PVT_.*")){
                branchReleaseType = "PVT";
            }
        }
        
        if (!old_branchReleaseType.equalsIgnoreCase(branchReleaseType) || 
        	!old_branchRelease.equalsIgnoreCase(branchRelease)){
        	changed = true;
        }

        /* This block of stuff really only needs to happen if one of the
         * associated fields has changed, but that's hard to keep track of, so
         * we'll just look at the "changed" flag. */
        if (changed) {
EOM

if (!$opt_C and !$opt_R){
	print $OUT <<EOM;
            rli_nums = new HashSet<String>();
            for (String num : rli.split("[,\\\\s]+")) {
                rli_nums.add(num);
            }
EOM
}

print $OUT <<EOM;
            /* Which field has our release value? */
            String release_value = "";
            if (conf_committed_release.length() > 0) {
                release_field = "conf_committed_release";
                release_value = conf_committed_release;
            } else if (committed_release.length() > 0) {
                release_field = "committed_release";
                release_value = committed_release;
            } else if (planned_release.length() > 0) {
                release_field = "planned-release";
                release_value = planned_release;
            }
            release_chosen = release_value.trim();
            releases = findReleases(release_value);
            NotMilestonesRelease = is_NotMilestoneRelease(release_value);

            /**
             * Determine if dev-owner is blank, we need this
             * since identifying blank dev-owner is needed in rules
             */
            BlankDevowner = false;
            if ((getDev_owner().length() <= 0) || (getDev_owner().equals("-"))){
                BlankDevowner = true;
            }   
           /* This is an exception requested per RLI 2466 */ 
            if (blocker.length() > 0) {
                /* It's some sort of blocker. */
                blocker_types = new HashSet<String>();
                String lcBlocker = blocker.toLowerCase();
                /* First, see if it's one of the special blocker types. */
                blocker_types.add(lcBlocker);
                if (lcBlocker.indexOf("beta") > -1) {
                    blocker_types.add("beta");
                } else if (lcBlocker.indexOf("frs") > -1) {
                    blocker_types.add("frs");
                }
                /* Then see if we can glean anything from Release */
                String type;
                Matcher mat = releasePat.matcher(planned_release);
                if (mat.lookingAt()) {
                    blocked_release = mat.group(1);
                    type = mat.group(2);
                    if (null != type) {
                        type = type.toUpperCase();
                    } else {
                        type = "";
                    }
                } else {
                    /* Hmmm, it's a blocker, but without a valid release.
                     * We're quite unlikely to get here, but we need to do
                     * something.  Grab a value out of releases... */
                    for (String rel : releases) {
                        blocked_release = rel;
                        break;
                    }
                    if (null == blocked_release) {
                        blocked_release = "";
                    }
                    type = "";
                }
                if (type.startsWith("R")) {
                    blocker_types.add("release");
                    if (type.startsWith("R1")) {
                        blocker_types.add("r1");
                    } else if (type.startsWith("R2")) {
                        blocker_types.add("r2");
                    } else if (type.startsWith("R3")) {
                        blocker_types.add("r3");
                    } else if (type.startsWith("R4")) {
                        blocker_types.add("r4");
                    } else if (type.startsWith("R5")) {
                        blocker_types.add("r5");
                    } else if (type.startsWith("R6")) {
                        blocker_types.add("r6");
                    } else if (type.startsWith("R7")) {
                        blocker_types.add("r7");
                    } else if (type.startsWith("R8")) {
                        blocker_types.add("r8");
                    }
                } else if (type.startsWith("B")) {
                    blocker_types.add("beta");
                    if (type.startsWith("B1")) {
                        blocker_types.add("b1");
                    } else if (type.startsWith("B2")) {
                        blocker_types.add("b2");
                    } else if (type.startsWith("B3")) {
                        blocker_types.add("b3");
                    }
                } else { // if not Rn Bn then it should be IBn
                    blocked_release = blocked_release + type;
                    
                    if (DataSource.RELEASE_PORTAL.getReleases().containsKey(blocked_release)
                       && type.toLowerCase().startsWith("ib")){
                        blocked_release = DataSource.RELEASE_PORTAL.getReleases().get(blocked_release);
                    }
                }
            } else {
                blocker_types = Collections.emptySet();
                blocked_release = "";
            }

EOM

if (!$opt_C and !$opt_R){
	print $OUT <<EOM;
	            summary = new String[] {
	                trimString(synopsis, maxSummarySynopsisLength),
	                last_modified_string
	            };
EOM
}

print $OUT <<EOM;
            calculateHashCode();
        }
        valid_dev_owners.add(dev_owner);
        valid_responsibles.add(responsible);
        valid_techpub_owners.add(techpubs_owner);
        valid_category_spocs.add(category_spoc);
        valid_real_responsibles.add(real_responsible);
        valid_systest_owners.add(systest_owner);
        
        valid_backlog_pr_dev_owners.add(backlog_pr_dev_owner);
        valid_backlog_pr_responsibles.add(backlog_pr_responsible);
        valid_backlog_pr_techpub_owners.add(backlog_pr_techpub_owner);
        return changed;
    }

    /**
     * Function to determine if the releases is no Milestones
	 *
     * \@param fieldvalue
     * \@return
     */
    private boolean is_NotMilestoneRelease(String fieldValue){
    	boolean retval = false;
        Pattern pat = releasePat;
        for (String rel : fieldValue.split("\\\\s+")) {
            Matcher mat = pat.matcher(rel);
            if (mat.lookingAt()) {
                String type = "";
                type = mat.group(2); // need the type for cdm releases
                if (type == null ){
                	retval = true;
                } 
            }
        }
        return retval;
    }


    /**
     * Find the release values in the supplied field.
     *
     * \@param fieldValue The field to search.
     * \@return A Set of major.minor release values.
     */
    private Set<String> findReleases(String fieldValue) {
        Set<String> rels = null;
        String tmpRel = null;
        Pattern pat;
        pat = releasePat;
        for (String rel : fieldValue.split("\\\\s+")) {
            Matcher mat = pat.matcher(rel);
            if (mat.lookingAt()) {
                String mDotN = mat.group(1);
                String type = "";
                type = mat.group(2); // need the type for cdm releases
                if (null == rels) {
                    rels = new HashSet<String>();
                }
                if (type != null ){
                	type = type.toUpperCase();
                	if (type.startsWith("IB")){
						mDotN += type;
                	}
                } 
                tmpRel = mDotN;
                rels.add(mDotN);
                if (DataSource.RELEASE_PORTAL.getReleases().containsKey(mDotN)){
                	/*
                	 * adding the branch value of specific releases from JUNOS Release
                	 * Portal. 
                	 */
                	rels.add(DataSource.RELEASE_PORTAL.getReleases().get(mDotN));
                }
                if (! DataSource.RELEASES.getActiveReleaseIds().contains(mDotN)) {
                    /* This PR contains a release that is not in the list
                     * of active releases.  Note that fact by adding the
                     * "fake release" value.
                     * ==> If the release becomes active before the PR changes,
                     * this PR will continue to be listed against the fake release,
                     * so don't do anything important with that data... */
                    rels.add(DataSource.RELEASES.NO_RELEASE.getId());
                }
            }
        }
        if (null == rels) {
            /* If the PR has no release (rare, but it happens), count it as
             * belonging to the fake release, so that it will get collected
             * by the "totals" rules. */
            rels = Collections.singleton(DataSource.RELEASES.NO_RELEASE.getId());
        } else if (rels.size() == 1) {
            rels = Collections.singleton(tmpRel);
        }
        return rels;
    }

EOM

if (!$opt_C and !$opt_R){
	print $OUT <<EOM;
    public Set<String> getRli_nums() {
        return rli_nums;
    }
EOM
} elsif ($opt_C) {
    print $OUT <<EOM;
    public static String getExpressions(){
        StringBuffer release_expr = new StringBuffer();
        for (String rel : Repository.PR_fix_releases.split(",")){
            if (release_expr.length() > 0){
                release_expr.append("|");
            }
            release_expr.append("planned-release ~ \\"" + rel.replace(".","\\\\.") + "\\"");
            release_expr.append("|conf-committed-release ~ \\"" + rel.replace(".","\\\\.") + "\\"");
            release_expr.append("|committed-release ~ \\"" + rel.replace(".","\\\\.") + "\\"");
        }
        // DEV Branches is something standard for all closed query
        release_expr.append("|");
        release_expr.append("planned-release ~ \\"DEV.*\\"");
        StringBuffer result = new StringBuffer();
        result.append(queryExpression);
        result.append("&(");
        result.append(release_expr.toString());
        result.append(")");
        return result.toString();
    }
EOM
}

	print $OUT <<EOM;
    public Set<String> getReleases() {
        return releases;
    }

    public String getBlocked_release() {
        return blocked_release;
    }

    public Set<String> getBlocker_types() {
        return blocker_types;
    }

    public String getBranchReleaseType() {
        return branchReleaseType;
    }

    public String getBranchRelease() {
        return branchRelease;
    }

    \@Override
    public String dump() {
        StringBuilder sb = dumpCommon();
        sb.append("last_modified => \\"");
        sb.append(last_modified);
        sb.append("\\"\\nlast_modified_string => \\"");
        sb.append(last_modified_string);
        sb.append("\\"\\nlast_modified_epoch => \\"");
        sb.append(last_modified_epoch);
        sb.append("\\"\\nnpi => \\"");
        sb.append(npi);
        sb.append("\\"\\ncategory_spoc => \\"");
        sb.append(category_spoc);
        sb.append("\\"\\nnon_junos_responsible => \\"");
        sb.append(non_junos_responsible);
        sb.append("\\"\\nreal_responsible => \\"");
        sb.append(real_responsible);
        sb.append("\\"\\nrelease_field => \\"");
        sb.append(release_field);
        sb.append("\\"\\nblocked_release => \\"");
        sb.append(blocked_release);
        sb.append("\\"\\n");

EOM

emit_field_dump(\@field_list, \%fields, $OUT);

print $OUT <<EOM;
        sb.append("releases => \\"");
        for (String s : releases) {
            sb.append(s);
            sb.append(", ");
        }
        sb.append("\\"\\n");
        sb.append("blocker_types => \\"");
        for (String s : blocker_types) {
            sb.append(s);
            sb.append(", ");
        }
        sb.append("\\"\\n");

        return sb.toString();
    }
    //Performance tuning for user filters in rules
    private static Set<String> valid_dev_owners = new HashSet<String>();
    private static Set<String> valid_responsibles = new HashSet<String>();
    private static Set<String> valid_techpub_owners = new HashSet<String>();
    private static Set<String> valid_category_spocs = new HashSet<String>();
    private static Set<String> valid_real_responsibles = new HashSet<String>();
    private static Set<String> valid_systest_owners = new HashSet<String>();
    
    private static Set<String> valid_backlog_pr_dev_owners = new HashSet<String>();
    private static Set<String> valid_backlog_pr_responsibles = new HashSet<String>();
    private static Set<String> valid_backlog_pr_techpub_owners = new HashSet<String>();
    
    public static void initValidUserList() {
    	valid_dev_owners.clear();
    	valid_responsibles.clear();
    	valid_techpub_owners.clear();
    	valid_category_spocs.clear();
    	valid_real_responsibles.clear();
    	valid_systest_owners.clear();
    	
    	valid_backlog_pr_dev_owners.clear();
    	valid_backlog_pr_responsibles.clear();
    	valid_backlog_pr_techpub_owners.clear();
    }
    
    public static Set<String> getValidUsers(String user) {
        Set<String> s =  new HashSet<String>();
        if (user.contains(":dev_owners")) {
    		s.addAll(valid_dev_owners);
    	} 
    	if (user.contains(":responsibles")) {
    		s.addAll(valid_responsibles);
    	} 
    	if (user.contains(":techpub_owners")) {
    		s.addAll(valid_techpub_owners);
    	}
    	if (user.contains(":category_spocs")) {
    	    s.addAll(valid_category_spocs);
    	} 
        if (user.contains(":real_responsibles")) {
    	    s.addAll(valid_real_responsibles);
    	}  
    	if (user.contains(":systest_owners")) {
    	    s.addAll(valid_systest_owners);
    	} 
    	if (user.contains(":backlog_pr_dev_owners")) {
    		s.addAll(valid_backlog_pr_dev_owners);
    	} 
        if (user.contains(":backlog_pr_responsibles")) {
    		s.addAll(valid_backlog_pr_responsibles);
    	} 
        if (user.contains(":backlog_pr_techpub_owners")) {
    		s.addAll(valid_backlog_pr_techpub_owners);
    	}   
        return s;	  
    }

    public boolean isWithoutRelease() {
        if (null == release_chosen || "".equals(release_chosen) || "unknown".equals(release_chosen)) {
            return true;
        }
        if (release_chosen.matches("^\\\\d+[\\\\.]\\\\d+([BRSWX]\\\\d+.*)?")) {
            return false;
        }
    	return Releases.isWithoutRelease(release_chosen);
    } 

    public long getAge() {
    	return ((new Date()).getTime() - last_modified.getTime());
    } 

}
EOM

close($OUT);

do_exit();
