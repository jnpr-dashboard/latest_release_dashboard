#!/usr/bin/env perl
#
# Sync up dashboard with table metadata in Deepthought
#
# Dirk Bergstrom, dirk@juniper.net, 2007-06-05
#
# Copyright (c) 2007-2008, Juniper Networks, Inc.
# All rights reserved.
#
#####################
use strict;
use warnings;

use Getopt::Std;
use vars qw($opt_h $opt_v $opt_V $opt_d $opt_t $opt_u $opt_C $opt_B $opt_D);

use FindBin;
use lib $FindBin::Bin;
use utils;

my $VERSION = sprintf('%d.%02d', q$Revision: 1.5 $ =~ /(\d+)\.(\d+)/);

getopts('hvdu:');

usage(0) if ($opt_h);

sub usage {
    my $code = shift;
    print <<EOM;
usage: deep-metadata.pl [-hvd] [-u<username>] host_name table_name fieldname fieldname...
    -u Connect to Deepthought with this username
    -h show this message

Fetch PickList and RelationField Picklist_Order data for table_name.
This is version $VERSION.
EOM
    exit $code;
}

do_start();

my $host = shift;
my $table = shift;

my @fields = @ARGV;

#my $dev_host = "kfoster-lnx.jnpr.net";
# Get connected to the outside world.
setup_deepthought($host, $table);

for my $fld (@fields) {
    my %fm = $r->field_metadata($fld);
    next unless (exists($fm{'Field_Type'}) &&
                 $fm{'Field_Type'} =~ /Relation|Pick/);
    my $i = 0;
    for my $code (@{$fm{'Picklist_Order'}}) {
        print $fld, $UNIT_SEPARATOR, $code, $UNIT_SEPARATOR, $i, $UNIT_SEPARATOR, $RECORD_SEPARATOR, "\n";
        $i++;
    }
}

do_exit();
