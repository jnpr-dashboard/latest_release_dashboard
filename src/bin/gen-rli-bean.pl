#!/usr/bin/env perl
#
# Generate RLI record bean.
#
# Dirk Bergstrom, dirk@juniper.net, 2006-12-05
#
# Copyright (c) 2006-2007, Juniper Networks, Inc.
# All rights reserved.
#
#####################
use strict;
use warnings;

use Getopt::Std;
use vars qw($opt_h $opt_v $opt_d $opt_u);
getopts('hvdu:');

use FindBin;
use lib $FindBin::Bin;
use utils;

my $VERSION = sprintf('%d.%02d', q$Revision: 1.27 $ =~ /(\d+)\.(\d+)/);

my $DEFAULT_BEAN_DIR = '../net/juniper/dash/data/';

my $CLASSNAME = 'RLIRecord';

my $outdir = shift || $DEFAULT_BEAN_DIR;

usage(0) if ($opt_h);

sub usage {
    my $code = shift;
    print <<EOM;
usage: gen-rli-bean.pl [-hvd] [-u<username>] [<bean file>]
    -u Connect to Deepthought with this username
    -d Die on errors
    -v be verbose
    -h show this message
This is version $VERSION.

<bean file> defaults to $DEFAULT_BEAN_DIR.
EOM
    exit $code;
}

# Fields we need to write rules about
my %fields_to_use = (
        "last_modified" => 1,
        "synopsis" => 1,
        "release_target" => 1,
        "state" => 1,
        "project_status" => 1,
        "toi_status" => 1,
        "at_risk" => 1,
        "applicability" => 1,
        "confidential" => 1,
        "category" => 1,
        "platform" => 1,
        "responsible" => 1,
        "plm_responsible" => 1,
        "sw_mgr_responsible" => 1,
        "sw_responsible" => 1,
        "eng_commitment" => 1, # added for CDM
        "hw_responsible" => 1,
        "st_mgr_responsible" => 1,
        "st_responsible" => 1,
        "st_commitment" => 1, # added for CDM 
        "tp_mgr_responsible" => 1,
        "tp_lead_responsible" => 1,
        "tp_responsible" => 1,
        "tp_commitment" => 1, # added for CDM 
        "toi_presenter" => 1,
        "sw_dev_complete_date" => 1,
        "sw_proj_len" => 1,
        "npi_program" => 1,
        "tracking_pr" => 1,
        "associated_prs" => 0,
        "functional_specification" => 1,
        "functional_spec_status" => 1,
        "unit_test_plan" => 1,
        "unit_test_plan_status" => 1,
        "tp_func_spec_approver" => 0,
        "tp_approval_status" => 0,
        "trd_status" => 1,
        "primary_tp_book" => 0,
        "tp_size" => 1,
        "tp_edit_status" => 0,
        "tp_release_status" => 1,
        "change_locator" => 1,
        "tp_feature_description" => 1,
        "er_numbers" => 1,
        "related_rlis" => 1,
        # added these fields to suppport Reporting criterion
        "product_test_plan_status" => 1,
        "cam_status" => 1,
	    "tests_executed_percentage" => 1,
	    "tests_passed_percentage" => 1,
	    "tests_script_percentage" => 1,
        # added these fields for support CDM rules on release target vs P1 release target
        "p1_release_target" => 1,
        "rli_class" => 1
);

# LongText fields that we do want to include
my %longtext_fields = (
'change_locator' => 1,
'tp_feature_description' => 1,
);

do_start();

# Get connected to the outside world, using the default Deepthought host
setup_deepthought(undef, 'RLI');

# see what's up in Deepthought
my %fields = ();
my @picklists = ();
my @ordered_fields = ();
for my $fld ($r->fields()) {
    my %fm = $r->field_metadata($fld);
    next unless (exists ($longtext_fields{$fld}) ||
        (exists ($fm{'Field_Type'}) && $fm{'Field_Type'} =~
        /^(Text|Relation|PickList|Boolean|MultiPick|User|Number|Date)$/));
    $fields{$fld} = $fm{'Field_Type'};
    if ($fm{'Field_Type'} =~ /PickList|Relation/) {
        push(@picklists, $fld);
    }
    push(@ordered_fields, $fld);
}
# Add last_modified to the list
$fields{'last_modified'} = 'Date';

my @getter_methods = ();
my @populators = ();
my @bean_fields = ();
my @field_list = ();
my @field_list_dump = ();
my $i = 2;
for my $col (@ordered_fields) {
    next unless ($fields_to_use{$col});

    # This is hardcoded to be the first field, as it's needed for processing.
    next if ($col eq 'last_modified');

    push(@field_list, $col);
    push(@field_list_dump, $col);
    handle_data_type($col, $fields{$col}, $i, \@getter_methods,
                     \@populators, \@bean_fields);

    if ($col =~ /tp_.*responsible/) {
        # FIXME Cheap hack to pull only the first name out of the
        # tp_*_responsible fields.
        pop(@populators);
        push(@populators, <<EOM);
        String ${col}_old = $col;
        $col = Record.getFirstWord(values[$i]);
        changed |= ! $col.equals(${col}_old);
EOM
    }
    
    if ($col =~ /tests_.*_percentage/){
    	# we will do the convertion to here
    	# change the bean fields declaration
    	pop(@bean_fields);
    	pop(@getter_methods);
    	pop(@populators);
    	pop(@field_list_dump);
    	if ($col eq 'tests_script_percentage'){
	    	push(@bean_fields, <<EOM);
	private byte tests_script;
EOM
			push(@getter_methods, <<EOM);
	public byte getTests_script(){
		return tests_script;
	}
EOM
			push(@populators, <<EOM);
	num_mat = numberPat.matcher(values[$i]);
	if (num_mat.lookingAt()){
		tests_script = Byte.parseByte(num_mat.group());
	} else {
		// not a number
		tests_script = -1;
	}
EOM
    	}
    	if ($col eq 'tests_passed_percentage'){
	    	push(@bean_fields, <<EOM);
	private byte tests_passed;
EOM
			push(@getter_methods, <<EOM);
	public byte getTests_passed(){
		return tests_passed;
	}
EOM
			push(@populators, <<EOM);
	num_mat = numberPat.matcher(values[$i]);
	if (num_mat.lookingAt()){
		tests_passed = Byte.parseByte(num_mat.group());
	} else {
		// not a number
		tests_passed = -1;
	}
EOM
    	}
    	if ($col eq 'tests_executed_percentage'){
	    	push(@bean_fields, <<EOM);
	private byte tests_executed;
EOM
			push(@getter_methods, <<EOM);
	public byte getTests_executed(){
		return tests_executed;
	}
EOM
			push(@populators, <<EOM);
	num_mat = numberPat.matcher(values[$i]);
	if (num_mat.lookingAt()){
		tests_executed = Byte.parseByte(num_mat.group());
	} else {
		// not a number
		tests_executed = -1;
	}
EOM
    	}
    }
    
    $i++;
}

my $today = scalar(gmtime(time));
my $this_year = (gmtime(time))[5] + 1900;

# Only look for Branch RLI
#my $debug_query = "public static final String debugQueryParams = \"skipClosed=yes\";";
# my $debug_query = "public static final String debugQueryParams = \"skipClosed=yes&release_target=10\_%\";";
# yesterday
my $debug_query = "public static final String debugQueryParams = \"skipClosed=yes&dateFld1=last_modified&dateOp1=>&dateTyp1=days_ago&dateVal1=10\";";	

# RLI RECORD BEAN part
my $OUT;
open($OUT, '>', $outdir."/$CLASSNAME.java") ||
    die("Can't write to $outdir/$CLASSNAME.java.");

print $OUT <<EOM;
/**
 * Autogenerated value bean for a JUNOS RLI.
 *
 * Copyright (c) $this_year, Juniper Networks, Inc.
 * All rights reserved.
 */
package net.juniper.dash.data;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net.juniper.dash.DashboardException;

/**
 *  A JUNOS RLI
 */
public class $CLASSNAME extends Record {

    public static final String queryParams = "skipClosed=yes";
	$debug_query
	
    private static Pattern releasePat = Pattern.compile(RELEASE_REGEX,
        Pattern.CASE_INSENSITIVE | Pattern.COMMENTS);
    private static Pattern branchPat = Pattern.compile(BRANCH_REGEX,
            Pattern.CASE_INSENSITIVE | Pattern.COMMENTS);
    private static Pattern numberPat = Pattern.compile("(\\\\d+(\\\\.\\\\d+)?)",
            Pattern.CASE_INSENSITIVE | Pattern.COMMENTS);
    private static Pattern rliPat = Pattern.compile("(\\\\d+)(\\\\w+|[,])?",
            Pattern.CASE_INSENSITIVE | Pattern.COMMENTS);

    private String relname ="";
    private String p1relname ="";

	// used to identify if the associated RLI has test effort
	private boolean have_associated_test_effort;
	
	private Matcher num_mat;

    public String getRelname() {
        return relname;
    }
    public String getP1relname() {
        return p1relname;
    }
EOM
print $OUT '    // last_modified ';
for my $fld (@field_list) {
    print $OUT $fld, ' ';
}
print $OUT <<EOM;

    private static final String[] fieldList = new String[] {
        "last_modified",
EOM
for my $fld (@field_list) {
    print $OUT "        \"$fld\",\n";
}
print $OUT <<EOM;
    };

    public static String[] getFieldList() {
        return fieldList;
    }

    private Date last_modified;
    private String last_modified_string;
    private long last_modified_epoch;
EOM
print $OUT join("\n", @bean_fields);
print $OUT <<EOM;
    public $CLASSNAME(String[] values, DataSource dataSource)
            throws DashboardException {
        super(values, dataSource);
        populate(values);
    }

    public Date getLast_modified() {
        return last_modified;
    }
    public String getLast_modified_string() {
        return last_modified_string;
    }
    public long getLast_modified_epoch() {
        return last_modified_epoch;
    }
    
    public boolean isHave_associated_test_effort(){
    	return have_associated_test_effort;
    }

EOM
print $OUT join("\n", @getter_methods);

print $OUT <<EOM;

    public boolean populate(String[] values) throws DashboardException {
        if (null == values) {
            throw new DashboardException("Null input.");
        } else if (values.length != fieldList.length + 2) {
            // values always comes with a throwaway on the end, and the
            // record number at the beginning.
            throw new DashboardException("Wrong number of elements: Got " +
                values.length + ", expected " + (fieldList.length + 2));
        }
        boolean changed = false;
        lastUpdated = new Date();
        Date newLastModified = dataSource.parseDate(values[1]);
        if (! newLastModified.equals(
        lastModified)) {
            lastModified = newLastModified;
            changed = true;
        }

        last_modified = lastModified;
        last_modified_epoch = last_modified.getTime();
        last_modified_string = dataSource.yyyymmdd(last_modified);
EOM
print $OUT join('', @populators);

print $OUT <<EOM;

        if (changed) {
            summary = new String[] {
                trimString(synopsis, maxSummarySynopsisLength),
                last_modified_string
            };

            if (release_target.length() == 0) {
                relname = "unscheduled";
            } else if (release_target.equals("unscheduled") ||
                release_target.equals("n/a")) {
                relname = release_target;
            } else {    
            	// Fall to this if the Release target is 
            	// - Major.Minor
            	// - IB Branch
            	// - DEV Branch (not really checked correctly yet)
		        Matcher mat = releasePat.matcher(release_target);
		        if (mat.lookingAt()){
		        	// Major.Minor
		        	relname = mat.group(1);
		        } else {
		        	// IB or DEV Branch
		        	relname = release_target;
		        	/*
                     * REMOVE THIS AFTER RELEASE 5.4 
                    mat = branchPat.matcher(release_target);
		        	if (mat.lookingAt()){
		        		// branch format = Major.Minor<IB><Character>?
		        		String branch = mat.group(2)+"."+mat.group(3)+mat.group(1);
		        		if (mat.groupCount() >= 6){
		        		    branch = branch + mat.group(6);
		        		}
		        		relname = branch;
		        	} else {
		        		// DEV Branch..... still no process
		        		relname = release_target;
		        	}
		        	*/
		        }
            }
            
            // Identifying the P1 Release Target value
            if (p1_release_target.length() == 0) {
                p1relname = "unscheduled";
            } else if (p1_release_target.equals("unscheduled") ||
                p1_release_target.equals("n/a")) {
                p1relname = p1_release_target;
            } else {    
            	// Fall to this if the Release target is 
            	// - Major.Minor
            	// - IB Branch
            	// - DEV Branch (not really checked correctly yet)
		        Matcher mat = releasePat.matcher(p1_release_target);
		        if (mat.lookingAt() & p1_release_target.contains("R1")){
		        	// Major.Minor , we don't take the Rn, Bn
		        	p1relname = mat.group(1);
		        } else {
                    // IB or DEV Branch
                    p1relname = release_target;
                    /*
                     * REMOVE THIS AFTER RELEASE 5.4 
                    mat = branchPat.matcher(release_target);
                    if (mat.lookingAt()){
                        // branch format = Major.Minor<IB><Character>?
                        String branch = mat.group(2)+"."+mat.group(3)+mat.group(1);
                        if (mat.groupCount() >= 6){
                            branch = branch + mat.group(6);
                        }
                        relname = branch;
                    } else {
                        // DEV Branch..... still no process
                        relname = release_target;
                    }
                    */
		        }
            }
			have_associated_test_effort = false;
//            process_relatedRLI();
            calculateHashCode();
        }
        valid_responsibles.add(responsible);
    	valid_sw_responsibles.add(sw_responsible);
    	valid_st_responsibles.add(st_responsible);
    	valid_sw_mgr_responsibles.add(sw_mgr_responsible);
    	valid_st_mgr_responsibles.add(st_mgr_responsible); 
    	valid_tp_responsibles.add(tp_responsible);
    	valid_tp_lead_responsibles.add(tp_lead_responsible);
    	valid_tp_mgr_responsibles.add(tp_mgr_responsible);
    	valid_plm_responsibles.add(plm_responsible);    	

        return changed;
    }

/*
    public void process_relatedRLI(){
		Matcher mat = rliPat.matcher(related_rlis);
		Set<String> related = new HashSet<String>();
		boolean all_related_processed = true;
		while (mat.find()){
			if (!DataSource.RLI.hasRecord(mat.group(1))){
				// Either RLI is not exist (unlikely) OR
				// RLI is not processed yet
				DataSource.RLI.addUnprocessed_RLI(getId());
				all_related_processed = false;
			} else {
				related.add(mat.group(1));
			}
		}

		// IF all related RLI has been captured in this phase otherwise it will be handle on 
		// DataSource.RLI.additionalPreProcessing
		
		/**
		 *  For checking on have_associated_test_effort rule
		 *   
		 */
/*		if (all_related_processed){
			have_associated_test_effort = identify_test_effort(related);
		}
		
    }*/
    
    public boolean identify_test_effort(Set<String> related){
    	// check if there is test effor on this RLI
    	boolean return_val = false;
    	Iterator it = related.iterator();
    	while (it.hasNext()){
    		// ensure the systest_commitment <st_commitment> is "committed"
    		// and has same release target as relname
    		RLIRecord _rli = (RLIRecord)DataSource.RLI.getRecord((String)it.next());
    		if (_rli.st_commitment.equalsIgnoreCase("committed") && _rli.relname.equalsIgnoreCase(getRelname())){
    			return_val = true;
    		}
    	}
    	return return_val;
    }

    \@Override
    public String dump() {
        StringBuilder sb = dumpCommon();
        sb.append("last_modified => \\"");
        sb.append(last_modified);
        sb.append("\\"\\nlast_modified_string => \\"");
        sb.append(last_modified_string);
        sb.append("\\"\\nlast_modified_epoch => \\"");
        sb.append(last_modified_epoch);
        sb.append("\\"\\nrelname => \\"");
        sb.append(relname);
        sb.append("\\"\\n");
EOM

emit_field_dump(\@field_list_dump, \%fields, $OUT);

print $OUT <<EOM;

        return sb.toString();
    }
    
    //Performance tuning for user filters in rules
    private static Set<String> valid_responsibles = new HashSet<String>();
    private static Set<String> valid_sw_responsibles = new HashSet<String>();
    private static Set<String> valid_st_responsibles = new HashSet<String>();
    private static Set<String> valid_sw_mgr_responsibles = new HashSet<String>();
    private static Set<String> valid_st_mgr_responsibles = new HashSet<String>();
    private static Set<String> valid_tp_responsibles = new HashSet<String>();
    private static Set<String> valid_tp_lead_responsibles = new HashSet<String>();
    private static Set<String> valid_tp_mgr_responsibles = new HashSet<String>();
    private static Set<String> valid_plm_responsibles = new HashSet<String>();
    
    public static void initValidUserList() {
    	valid_responsibles.clear();
    	valid_sw_responsibles.clear();
    	valid_st_responsibles.clear();
    	valid_sw_mgr_responsibles.clear();
    	valid_st_mgr_responsibles.clear();
    	valid_tp_responsibles.clear();
    	valid_tp_lead_responsibles.clear();
    	valid_tp_mgr_responsibles.clear();
    	valid_plm_responsibles.clear();  	  	
    }
    
    public static Set<String> getValidUsers(String user) {
    	Set<String> s = new HashSet<String>();
    	if (user.contains(":responsibles")) {
    		s.addAll(valid_responsibles);
    	}  
    	if (user.contains(":sw_responsibles")) {
    		s.addAll(valid_sw_responsibles);
    	} 
        if (user.contains(":st_responsibles")) {
    		s.addAll(valid_st_responsibles);
    	} 
        if (user.contains(":sw_mgr_responsibles")) {
    		s.addAll(valid_sw_mgr_responsibles);
    	} 
        if (user.contains(":st_mgr_responsibles")) {
    		s.addAll(valid_st_mgr_responsibles);
    	}
        if (user.contains(":tp_responsibles")) {
    		s.addAll(valid_tp_responsibles);
    	} 
        if (user.contains(":tp_lead_responsibles")) {
    		s.addAll(valid_tp_lead_responsibles);
    	} 
        if (user.contains(":tp_mgr_responsibles")) {
    		s.addAll(valid_tp_mgr_responsibles);
    	} 
        if (user.contains(":plm_responsibles")) {
    		s.addAll(valid_plm_responsibles);
    	}
        return s;
    }    

    public boolean isWithoutRelease() {  
        String unsch = "unscheduled";
        String na = "n/a";
    	if ((null == release_target || unsch.equals(release_target) || na.equals(release_target)) && (null == p1_release_target || unsch.equals(p1_release_target) || na.equals(p1_release_target))) {
    	    return true;
    	}
    	if (release_target.matches("^\\\\d+[\\\\.]\\\\d+([BRSWX]\\\\d+.*)?") && p1_release_target.matches("^\\\\d+[\\\\.]\\\\d+([BRSWX]\\\\d+.*)?")) {
            return false;
        } 	
    	return Releases.isWithoutRelease(release_target) && Releases.isWithoutRelease(p1_release_target); 
    }  
}
EOM

close($OUT);

# RLI bean part
$CLASSNAME = "RLI";
open($OUT, '>', $outdir."/$CLASSNAME.java") ||
    die("Can't write to $outdir/$CLASSNAME.java.");

print $OUT <<EOM;
/**
 * Copyright (c) $this_year, Juniper Networks, Inc.
 * All rights reserved.
 */
package net.juniper.dash.data;

import java.util.HashMap;
import java.util.Map;
import java.util.List;

import net.juniper.dash.DashboardException;
import net.juniper.dash.Refresher.TimeoutException;
import net.juniper.dash.Repository;
import net.juniper.dash.Refresher;

/**
 * Abstraction of a Deepthought table.
 * starting dashboard version 4.8_2 this is auto-generated bean
 * modified by smulyono\@juniper.net  \@2010
 */

public class $CLASSNAME extends Deepthought {

    private Map<String, RLIRecord> records = new HashMap<String, RLIRecord>();

	public RLI(String name) {
        super(name, "rli", "RLIRecord", "RLI", "RLIs");
        tableName = "RLI";
    }

    private static final String[] rlipickLists = new String[] {
EOM
for my $fld (@picklists) {
    print $OUT "        \"$fld\",\n";
}
print $OUT <<EOM;
    };

    \@Override
    protected String[] getPickLists() {
    	return rlipickLists;
    }

    \@Override
    protected void additionalPreProcessing(){
    }
    
    \@Override
    protected Record constructRecord(String[] fields) throws DashboardException {
        return new RLIRecord(fields, this);
    }

    \@Override
    public Record getRecord(String id) {
        return records.get(id);
    }

    \@Override
    public Map<String, Record> getRecords() {
        return new HashMap<String, Record>(records);
    }

    \@Override
    protected void removeRecord(String number) {
        records.remove(number);
    }

    \@Override
    protected void addRecord(Record record) {
        records.put(record.getId(), (RLIRecord) record);
    }

    \@Override
    protected boolean hasRecord(String id) {
        return records.containsKey(id);
    }

    \@Override
    protected String getQueryParams() {
        return Repository.testingMode ?
            RLIRecord.debugQueryParams : RLIRecord.queryParams;
    }

    \@Override
    protected String[] getFieldList() {
        return RLIRecord.getFieldList();
    }
    
    //Performance tuning
    protected List<String[]> getRawRecordData(Refresher refresher) throws DashboardException, TimeoutException {    	
    	RLIRecord.initValidUserList();  	
        return super.getRawRecordData(refresher);
    }   
}    
EOM
close($OUT);

do_exit();


