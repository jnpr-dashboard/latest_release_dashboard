#!/usr/bin/env perl
#
# Retrieve all open records from a Deepthought table and dump them to STDOUT.
#
# Dirk Bergstrom, dirk@juniper.net, 2007-02-27
#
# Copyright (c) 2007-2008, Juniper Networks, Inc.
# All rights reserved.
#
#####################
use strict;
use warnings;

use Getopt::Std;

use FindBin;
use lib $FindBin::Bin;
use utils;

my $VERSION = sprintf('%d.%02d', q$Revision: 1.5 $ =~ /(\d+)\.(\d+)/);

use vars qw($opt_h $opt_v $opt_d $opt_u);
getopts('hvdu:D');

usage(0) if ($opt_h);

sub usage {
    my $code = shift;
    print <<EOM;
usage: get-deep-records.pl [-hvd] [-u<username>] host table_name params field1 field2 ... fieldn
    -u Connect to deepthought with this username (incompatible with cert usage)
    -d Die on errors
    -v be verbose
    -h show this message
Return all records in <table_name> matching <params>, where params is a
list of query params in URL format (with no url escaping).
This is version $VERSION.
EOM
    exit $code;
}

do_start();

my $host = shift;
my $table = shift;

my $param_kvps = shift;

my @fields = @ARGV;

my @params = split(/[&=]/, $param_kvps);
#my $dev_host = "kfoster-lnx.jnpr.net";
# Get connected to the outside world.
setup_deepthought($host, $table);

# Set raw mode, so we can dump the output w/o postprocessing
$r->raw_mode(1);

# Print records to STDOUT
print $r->query_array(\@params, \@fields);

if ($r->errstr()) {
    die("Error reading records from $table: ", $r->errstr(), "\n");
} else {
    do_exit();
}

