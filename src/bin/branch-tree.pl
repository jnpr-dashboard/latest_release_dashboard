#!/usr/bin/env perl 

use strict;
use warnings;

use FindBin;
use lib $FindBin::Bin;
use utils;

do_start();

my $host = shift;
my $bt_table = 'branch';

setup_deepthought($host, $bt_table);

my @bt_fields = qw(branch_name release_target);
my @bt_params = (
	'branch_type_pick','ib',
	'branch_type_pick','dev',
	'branch_type_pick','pr',
	'childBranch','yes',
    'not_state','not',
	'skipClosed','no'
    );

my @query_results = do_query($r, \@bt_params, \@bt_fields);

for my $rec (@query_results) {
    for my $col (@$rec) {
        print $$col{'branch_name'}.":".$$col{'release_target'}, "\n";
    }
}