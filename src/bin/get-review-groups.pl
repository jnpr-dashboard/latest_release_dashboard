#!/usr/bin/env perl 
#
# Get moderator for each alias on the command line.
#
# Dirk Bergstrom, dirk@juniper.net, 2007-04-04
#
# Copyright (c) 2007, Juniper Networks, Inc.
# All rights reserved.
#
#####################
use strict;
use warnings;

use Getopt::Std;
use Net::SMTP;

use FindBin;
use lib $FindBin::Bin;
use utils;

my $VERSION = sprintf('%d.%02d', q$Revision: 1.5 $ =~ /(\d+)\.(\d+)/);

use vars qw($opt_h $opt_v $opt_d);
getopts('hvd');

usage(0) if ($opt_h);

sub usage {
    my $code = shift;
    print <<EOM;
usage: get-review-groups.pl [-hvd] alias1 alias2 ... aliasn
    -d Die on errors
    -v be verbose
    -h show this message
This is version $VERSION.
EOM
    exit $code;
}

do_start();

my @groups = @ARGV;

my $MAX_EXPANSIONS_BEFORE_RECONNECT = 5;
my $SECONDS_BETWEEN_EXPANDS = 0.10;
my $server = "mail.juniper.net";

# Connect to the SMTP server
my $smtp;
connect_to_smtp();

my $i = 0;
my @output = ();
for my $group (@groups) {
    my $moderator = expand($group . '-review-moderator');

# FIXME Need some error handling code...

    push(@output, $group . $UNIT_SEPARATOR . $moderator . $UNIT_SEPARATOR .
        $RECORD_SEPARATOR . "\n");
    if (++$i % $MAX_EXPANSIONS_BEFORE_RECONNECT == 0) {
        # Looks like the mail server throttles down the response speed
        # a *lot* after expanding a few aliases.  Hack around this by
        # reconnecting periodically.
        $smtp->quit;
        connect_to_smtp();
    }

    # Wait a bit
    select(undef, undef, undef, $SECONDS_BETWEEN_EXPANDS);
}

for my $line (@output) {
    print $line;
}

$smtp->quit;

sub connect_to_smtp {
    if ($opt_v) {
        warn("(re)connecting to $server.\n");
    }
    $smtp = Net::SMTP->new($server) or
        die("FATAL: couldn't connect to SMTP server $server: $!");
}

sub expand {
    # takes the name of an alias
    # returns the first entry that looks like a username (as opposed to a
    # newsgroup or another mailing list)

    # get the search string
    my $listname = shift(@_);
    if (! $smtp) {
        connect_to_smtp();
    }
    my @users = $smtp->expand($listname);

    if ($opt_v) {
        warn "$listname => ", join(', ', @users), "\n";
    }

    my @valid_users = ();
    foreach my $entry (@users) {
    	# entries come like so:
    	# <address@host.domain>\n
    	# though the name may appear also: joe blo <blo@juniper.net>\n
    	$entry =~ m/<(.*?)(@.*)?>/;
    	my $username = $1;
    	my $domain = $2 || '';

        # Many aliases are gatewayed to an internal usenet server via
        # newsgate.juniper.net
        next if ($domain =~ /newsgate/i);
        # dot and underbar not present in real usernames
        # TODO This may not be a good assumption, but I need some way to
        # sort out the many aliases and such.
        next if ($username =~ /[\._]/);

        push(@valid_users, $username);
    }
    return lc(join(',', @valid_users));
}
