#!/bin/sh
#
# Fetch the desired subfields for all GNATS fields, and return them
# in an easily parseable format.
#
# Sanny Mulyono, smulyono@juniper.net
# based on get-categories.sh, make more generic format for all subfields
#  and make it works for double digit parameter
#
# Copyright (c) 2008, Juniper Networks, Inc.
# All rights reserved.
#
#########
if [ $1 = "-h" ]; then
    echo "Usage: get-subfields.sh <query-pr path> <host> <port> <database> <adm-field> <subfield,subfield,...>"
    exit
fi

query_pr=$1
extra_opts=$2
host=$3
port=$4
database=$5
adm_field=$6
subfields=$7

# Turn a comma-separated list of numbers into an awk printf format.
# 7,11 ==> '"%s|%s:%s\n", rec[1], rec[7], rec[11]'
fmt=`echo $subfields | /usr/local/bin/sed -e 's/,/:/g' -e 's/[0-9]*/%s/g'`
nums=`echo 1,$subfields | /usr/local/bin/sed -e 's/[0-9]*/rec[&]/g'`
awkfmt="\"%s|${fmt}\\n\", ${nums}"

${query_pr} --database=${database} --host=${host} --port=${port} ${extra_opts}\
    --adm-field ${adm_field} | /usr/bin/awk -F: "{split(\$0,rec,\":\");printf $awkfmt}"

