# Settings appropriate for a development system.

DEBUG = True

DATABASE_HOST = 'univac-dev.juniper.net'

if DEBUG:
    LOG_FILE = '-'

    TEMPLATE_DEBUG = True

def adjust_settings(outer):
    if DEBUG:
        _tmp_mc = list(outer['MIDDLEWARE_CLASSES'])
        _tmp_mc.remove('django.contrib.auth.middleware.RemoteUserMiddleware')
        outer['MIDDLEWARE_CLASSES'] = tuple(_tmp_mc)

        _tmp_ab = list(outer['AUTHENTICATION_BACKENDS'])
        _tmp_ab.remove('django.contrib.auth.backends.RemoteUserBackend')
        _tmp_ab.append('django.contrib.auth.backends.ModelBackend')
        outer['AUTHENTICATION_BACKENDS'] = tuple(_tmp_ab)
