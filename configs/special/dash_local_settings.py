"""
Local server customizations for the Dashboard UI.

Place this file in /opt/dashboard/special

Dirk Bergstrom, dirk@juniper.net, 2009-05-01

Copyright (c) 2009, Juniper Networks, Inc.
All rights reserved.
"""
DEBUG = False

DATABASE_HOST = 'univac.juniper.net'

if DEBUG:
    LOG_FILE = '-'
    TEMPLATE_DEBUG = True

def adjust_settings(outer):
    if DEBUG:
        pass
