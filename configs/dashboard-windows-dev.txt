﻿====================================================
Building Dashboard Environment in Windows - Cygwin
====================================================

Cygwin + MinGW32
	* rysnc, openssh, openssl, openldap (optional)

Eclipse
	* 3.5+
	* PyDev - http://pydev.org/updates
	* EPIC -  http://e-p-i-c.sf.net/updates
	* Tomcat Plugin -- usefull for admin page development and simple testing
	* Web Development Tools 

	To make sure you have NO line-ending problems :
	ECLIPSE CONFIGURATION:
		* Window->Preferences->CVS [Files and Folders]
			uncheck "Convert text files to use platform line ending"
		* Window->General->Workspace
			Text file Encoding -> UTF-8
			New text file line delimiter -> Unix

MySQL - 5.x	
	* get the binary files from http://www.mysql.com/downloads/
	  [choose community server]
	  
MinGW 5.1.4+
	* need to build python library
	
Python
	* Install Python2.6
	  [ using python26 as run command ]
		- on /Lib/distutils create "distutils.cfg"
			[build_ext]
			compiler=mingw32

	* MySQL-Python
		- download source from : http://pypi.python.org/packages/source/M/MySQL-python/MySQL-python-1.2.3c1.tar.gz#md5=310dd856e439d070b59ece6dd7a0734d
		  or binary http://www.thescotties.com/mysql-python/test/
		- Extract and edit site.cfg (change the MySQL version to meet your version)
		- on Cygwin:
			$ python26 setup.py build 
			$ python26 setup.py install
	* cjson
		- download source from http://pypi.python.org/pypi/python-cjson/1.0.5
		- edit cjson.c 
			* change MODULE_VERSION to "1.0.5"
		- python26 setup.py build
		- python26 setup.py install
		
	* Django
		- easy_install django
	* django-compress
		- svn co http://django-compress.googlecode.com/svn/trunk/ django-compress
		- python26 setup.py build
		- python26 setup.py install
	
Perl 5.8.8
	* Using strawbery perl 5.8.8 http://strawberryperl.com/releases.html
	* Install openssl Library Dev from Shining Light  http://www.slproweb.com/products/Win32OpenSSL.html
    $ perl -MCPAN -e shell
      cpan> install MIME::Lite::TT
      cpan> install HTTP::Date
      cpan> install Date::Calc
      cpan> install Graph
	  cpan> install Crypt::SSLeay (OR install from sources perl Makefile.PL | dmake | dmake install)
	  
Java SDK 1.6
	* Install Java SDK 1.6
	
Apache - Tomcat
	* Struts Library
	* Drools Library
	
Apache - Ant 1.7
	* Download from http://ant.apache.org/bindownload.cgi

Apache - httpd
	* Download from http://httpd.apache.org/download.cgi

========================================================
Dashboard Application
========================================================
	* check out from CVS cvs/juniper/ sw-tools/src/dashboard
	* make USER_LIBRARY called "tomcat-lib" which points to your tomcat/lib 
	  and Oracle lib
	* Compile the project 
   
	* For the perl script able to use "/usr/bin/env perl -w", in Cygwin needs this adjustment
	  - Make sure env.exe is available in /usr/bin
	  - create wrapper script called env, contents like below:
	    #!/usr/bin/sh
	    env.exe $@
	    
TIPS:
	* Use Eclipse-ANT plugin to run the ant scripts so we don't need to point it as external resources



** http://forums.sun.com/thread.jspa?threadID=5187941
** http://www.mjmwired.net/kernel/Documentation/vm/overcommit-accounting
** http://stackoverflow.com/questions/1124771/how-to-solve-java-io-ioexception-error12-cannot-allocate-memory-calling-runt