ORACLE_HOME=/opt/instantclient
export ORACLE_HOME
LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:$ORACLE_HOME
export LD_LIBRARY_PATH

JAVA_HOME=/opt/java/jre
export JAVA_HOME

#Debugging Purposes
# set these to have a debugging port available from eclipese
#JPDA_TRANSPORT=dt_socket
#export JPDA_TRANSPORT

#JPDA_ADDRESS=9002
#export JPDA_ADDRESS

# Increase min heap size and total memory available.
MEMORY_OPTS="-Xms8000m -Xmx34000m"
AGGRESSIVE_GC_OPTS="-XX:+UseConcMarkSweepGC -XX:+UseParNewGC -XX:+CMSClassUnloadingEnabled -XX:+UseLargePages "
AGGRESSIVE_GC_OPTS="${AGGRESSIVE_GC_OPTS} -XX:NewRatio=3 "
AGGRESSIVE_GC_OPTS="${AGGRESSIVE_GC_OPTS} -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=/opt/dashboard/tomcat/log/heapdump.log"
AGGRESSIVE_GC_OPTS="${AGGRESSIVE_GC_OPTS} -Dcom.sun.management.jmxremote -Dcom.sun.management.jmxremote.port=8115 -Dcom.sun.management.jmxremote.authenticate=false -Dcom.sun.management.jmxremote.ssl=false"

CATALINA_OPTS="$MEMORY_OPTS $AGGRESSIVE_GC_OPTS"
JAVA_OPTS="-server -d64 "
echo "Catalina Parameter = ${CATALINA_OPTS}"

# set the special directory that java will use to be relative to where this script lives by default
# if you really want to use a different directory, set DASH_SPECIAL_DIRECTORY to a hard coded path below
# Absolute path to this script. /home/user/bin/foo.sh
SCRIPT=`readlink -f $0`
# Absolute path this script is in. /home/user/bin
SCRIPTPATH=`dirname $SCRIPT`
SPECIAL_PATH="${SCRIPTPATH}/../.." 

DASH_SPECIAL_DIRECTORY=`cd $SPECIAL_PATH;pwd`
DASH_SPECIAL_DIRECTORY="${DASH_SPECIAL_DIRECTORY}/special/"

if [ "$DASH_SPECIAL_DIRECTORY" = "/opt/special/" ]; then    
# in production, tomcat is installed under /opt, so if we get that, 
    # force it to the real production location
    DASH_SPECIAL_DIRECTORY="/opt/dashboard/special/"
fi
# redefine DASH_SPECIAL_DIRECTORY before the export
# if you really don't want the default

export DASH_SPECIAL_DIRECTORY

echo "DASH_SPECIAL_DIRECTORY = ${DASH_SPECIAL_DIRECTORY}"



