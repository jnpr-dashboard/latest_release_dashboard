#!/bin/sh

ORACLE_HOME=/opt/instantclient
export ORACLE_HOME
LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:$ORACLE_HOME
export LD_LIBRARY_PATH
${ORACLE_HOME}/sqlplus $@
