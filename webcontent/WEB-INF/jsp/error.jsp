<%@ taglib prefix="s" uri="/struts-tags" %><%@ include file="admin/header.jsp" %>

<h1>Dashboard Error</h1>

<p>While attempting to produce the page "<s:property value="title"/>", I encountered 
the error:<p>

<s:if test="null != errorMessage">
<h3><s:property value="errorMessage"/></h3>
</s:if>
<s:if test="null != exception">
<pre>
Exception:
<s:property value="exception"/>

Stacktrace:
<s:property value="exceptionStack"/>
</pre>
</s:if>

<p>
If you don't understand this error, please email the contents of this page to
<a href="mailto:dashboard-admin@juniper.net">dashboard-admin@juniper.net</a>.
</p>

<ul class="note">
<li>Time: <% out.println(new java.util.Date()); %></li>
<li>User: <s:property value="remoteUser"/></li>
<li>Browser: <s:property value="userAgent"/></li>
</ul>

<%@ include file="admin/footer.jsp" %>
