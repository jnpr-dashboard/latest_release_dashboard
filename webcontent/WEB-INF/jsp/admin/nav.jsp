<%@ taglib prefix="s" uri="/struts-tags" %>
<div id="menu_link">
	<h2><a href="#">Dashboard</a></h2>
	<div>
		<ul class="tree-admin-left-menu">
			<li><a href="main">Overview Stats</li>
		</ul>
	</div>
	
	<h2><a href="#">Configuration</a></h2>
	<div>
		<ul class="tree-admin-left-menu">
			<li><a href="configuration#application">Application</a></li>
			<li><a href="configuration#database">Database</a></li>
			<li><a href="configuration#ldap">LDAP</a></li>
			<li><a href="configuration#logging">Logging</a></li>
			<li><a href="configuration#multiplier">Multiplier</a></li>
			<li><a href="configuration#datasources">DataSources</a></li>
		</ul>
	</div>

	<h2><a href="#">DataSources</a></h2>
	<div>
		<ul class="tree-admin-left-menu">
			<li>
			<a href="<s:url namespace="/admin" action="datasource">
			</s:url>?dsName=Users">Summary Stats</a>
			</li>
		</ul>
	</div>
	
	<h2><a href="#">Rules</a></h2>
	<div>
		<ul class="tree-admin-left-menu">
			<li><a href="<s:url namespace="/admin" action="show-create-rule" />">Manage Rule</a> </li>
		</ul>
	</div>

	<h2><a href="#">Results(JSON)</a></h2>
	<div>
		<ul class="tree-admin-left-menu">
			<li><a href="javascript:window.open('<s:url namespace="/json" action="index"/>')">Access Output</a></li>
		</ul>
	</div>

	<h2><a href="#">Statistics</a></h2>
	<div>
		<ul class="tree-admin-left-menu">
			<li><a href="stats#summary">Overview (Summary)</a></li>
			<li><a href="stats#log">Error Log </a></li>
		</ul>
	</div>
	
	<h2><a href="#">Documentation</a></h2>
	<div>
		<ul class="tree-admin-left-menu">
			<li><a href="docs"> Admin documentation</a>
                <ul>
                    <li><a href="docs#infra"> Infrastructure</a> </li>
                    <li><a href="docs#cvs"> CVS </a> </li>
                    <li><a href="docs#newds"> Adding a new DataSource</a> </li>
                    <li><a href="docs#howitworks"> How it Works</a> </li>
                    <li><a href="docs#vteams"> Virtual Teams</a> </li>
                    <li><a href="docs#release"> Rollout Procedure </a> </li>
                </ul>
            </li>
		</ul>
	</div>
</div>
