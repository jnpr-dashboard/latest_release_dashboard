<%@ taglib prefix="s" uri="/struts-tags" %><%@ include file="header-admin.jsp" %>
<h1>View "<s:property value="dsName"/>" Record "<s:property value="recId"/>"</h1>

<div id="misc" class="boxed">
<s:if test="null == record">
<span class="bad">Error:</span>
No record with id=<s:property value="recId"/> in DataSource <s:property value="dsName"/>
</s:if>
<s:else>
<pre>
<s:property value="record.dump()"/>
</pre>
</s:else>
</div>

<%@ include file="ds-list.jsp" %>

<%@ include file="footer.jsp" %>
