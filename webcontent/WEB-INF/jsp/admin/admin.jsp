<!--  Old admin file, still kept for reference  -->
<%@ taglib prefix="s" uri="/struts-tags" %><%@ include file="header.jsp" %>
<h1><s:property value="title"/></h1>

<div class="boxed">
<ul>
<%@ include file="nav.jsp" %>
</ul>
</div>

<div id="misc" class="boxed">
<h3>Misc</h3>
<table border="1" cellspacing="0" cellpadding="1">
<thead>
<tr>
<th>Name</th>
<th>Value</th>
</tr>
</thead>
<tbody>

<tr>
<td class="rclabel">Update Cycles</td>
<td class="rcvalue">
<s:property value="@net.juniper.dash.Repository@getUpdateCycles()"/>
</td>
</tr>

<tr>
<td class="rclabel">Average of Last 10 Update Times</td>
<td class="rcvalue">
<s:property value="@net.juniper.dash.Repository@getAverageRecentUpdateCycleTime()"/>
</td>
</tr>

<tr>
<td class="rclabel">Average Update Time</td>
<td class="rcvalue">
<s:property value="@net.juniper.dash.Repository@getAverageUpdateCycleTime()"/>
</td>
</tr>

<td class="rclabel">Connection Cache</td>
<td class="rcvalue">
<s:property value="connCacheStats"/>
</td>
</tr>

<tr>
<td class="rclabel">Number of Errors</td>
<td class="rcvalue">
<s:property value="@net.juniper.dash.Repository@getErrors().size()"/>
</td>
</tr>

<tr>
<td class="rclabel">Last Error at</td>
<td class="rcvalue">
<s:property value="@net.juniper.dash.Repository@lastErrorDate"/>
</td>
</tr>

<tr>
<td class="rclabel">Number of Users</td>
<td class="rcvalue">
<s:property value="allUsers.size()"/>
</td>
</tr>
</tbody>
</table>
</div>

<div id="editrule" class="boxed">
<h3>Edit Rule</h3>
<s:form method="GET" namespace="/admin" action="show-rule-edit">
<s:select name="ruleid" label="Identifier" size="1"
          list="rules" listKey="identifier" listValue="%{ counts + ': ' + name}"/>
<s:submit value="Go" />
</s:form>
</div>

<div id="dumprules" class="boxed">
<h3>Persist Rules</h3>
<s:form method="POST" namespace="/admin" action="persist-rules">
<s:textfield name="dirname" value="%{dirname}" label="Directory" size="60"/>
<s:submit value="Go" />
</s:form>
</div>

<div id="loadrules" class="boxed">
<h3>Load Rules</h3>
<s:form method="POST" namespace="/admin" action="load-rules">
<s:textfield name="dirname" value="%{dirname}" label="Directory" size="60"/>
<s:checkbox name="skipExisting" value="%{skipExisting}"
            fieldValue="true" label="Skip Existing"/>
<s:submit value="Go" />
</s:form>
</div>

<div id="saverules" class="boxed">
<h3>Save Changed Rules</h3>
<s:form method="POST" namespace="/admin" action="save-rules">
<p>Save all changed rules to the database.</p>
<s:submit value="Go" />
</s:form>
</div>

<div id="pause" class="boxed">
<h3>Pause Updates</h3>
<s:form method="POST" namespace="/admin" action="pause-updates">
<p>Pause updates for the next 
<select name="pauseMinutes">
<option value="60">hour</option>
<option value="120">2 hours</option>
<option value="180">3 hours</option>
<option value="240">4 hours</option>
<option value="720">12 hours</option>
<option value="144">day</option>
<option value="1008">week</option>
<option value="525600">10 years</option>
<option value="0">(zero)</option>
</select>
<small>(currently set at <s:property value="pauseMinutes" /> minutes).</small></p>
<s:submit value="Go" />
</s:form>
</div>

<div id="motd" class="boxed">
<h3>Message of the Day</h3>
<p>If body is non-blank, headline is rendered as a link to motd.jsp, which
will display body (which should be html), otherwise, headline is rendered
as is (and should perhaps contain hrefs).</p>
<s:form method="POST" namespace="/admin" action="motd">
<s:textarea name="motd.text" value="%{motd.text}" label="Headline"
            rows="2" cols="80"/>
<s:textarea name="motd.body" value="%{motd.body}" label="Body"
            rows="4" cols="80"/>
<s:submit value="Update" />
</s:form>
</div>

<div id="restart" class="boxed">
<h3>Soft Restart</h3>
<s:form method="POST" namespace="/admin" action="restartProcessing">
<p>Throw away the RuleBase, WorkingMemory, and all data & scores.  Read
the rules from the db, and start over.</p>
<s:submit value="Go" />
</s:form>
</div>

<div id="multipliers" class="boxed">
<h3>Time Based Multipliers</h3>
<s:form method="POST" namespace="/admin" action="reloadMultipliers">

<table border="0" cellspacing="0" cellpadding="2">
<tr><td>
<h4>Positive Horizon</h4>
<table border="1" cellspacing="0" cellpadding="2">
<thead>
<tr>
<th>Time Consumed Between Horizon and Milestone</th>
<th>Multiplier</th>
</tr>
</thead>
<tbody>
<tr>
<td class="rclabel">
Up to <s:property value="@net.juniper.dash.Rule@firstPercent"/>%
</td>
<td class="rcvalue">
1
</td>
</tr><tr>
<td class="rclabel">
Up to <s:property value="@net.juniper.dash.Rule@secondPercent"/>%
</td>
<td class="rcvalue">
<s:property value="@net.juniper.dash.Rule@secondPercentMultiplier"/>
</td>
</tr><tr>
<td class="rclabel">
Up to <s:property value="@net.juniper.dash.Rule@thirdPercent"/>%
</td>
<td class="rcvalue">
<s:property value="@net.juniper.dash.Rule@thirdPercentMultiplier"/>
</td>
</tr><tr>
<td class="rclabel">
Thereafter
</td>
<td class="rcvalue">
<s:property value="@net.juniper.dash.Rule@finalPercentMultiplier"/>
</td></tr>
</tbody>
</table>

</td><td>&nbsp;&nbsp;</td><td>

<h4>Negative Horizon</h4>
<table border="1" cellspacing="0" cellpadding="2">
<thead>
<tr>
<th>Days Since Effective Date</th>
<th>Multiplier</th>
</tr>
</thead>
<tbody>
<tr>
<td class="rclabel">
First <s:property value="@net.juniper.dash.Rule@daysAfterPeriod"/> days
</td>
<td class="rcvalue">
1
</td>
</tr><tr>
<td class="rclabel">
Second <s:property value="@net.juniper.dash.Rule@daysAfterPeriod"/> days
</td>
<td class="rcvalue">
<s:property value="@net.juniper.dash.Rule@secondPeriodMultiplier"/>
</td>
</tr><tr>
<td class="rclabel">
Third <s:property value="@net.juniper.dash.Rule@daysAfterPeriod"/> days
</td>
<td class="rcvalue">
<s:property value="@net.juniper.dash.Rule@thirdPeriodMultiplier"/>
</td>
</tr><tr>
<td class="rclabel">
Thereafter
</td>
<td class="rcvalue">
<s:property value="@net.juniper.dash.Rule@finalPeriodMultiplier"/>
</td>
</tr><tr>
</tbody>
</table>

</td></tr></table>

<s:submit value="Reload Multipliers" />
</s:form>
</div>

<%@ include file="ds-list.jsp" %>

<div class="boxed">
<h3>Groups</h3>
<table dojoType="filteringTable" id="groupstable"
       multiple="false" alternateRows="false" maxSortable="1"
       border="1" cellspacing="0" cellpadding="2">
<thead>
<tr>
<th dataType="String">Group</th>
<th dataType="String">LDAP Group</th>
<th dataType="Number">Size</th>
<th dataType="Number">Last Upd</th>
</tr>
</thead>
<tbody>
<s:iterator value="allGroups" status="itstat">
<tr value="<s:property value="#itstat.index"/>">

<td class="rclabel">
<s:property value="name"/>
</td>

<td class="rcvalue">
<a href="https://authldap-primary.juniper.net/cgi-bin/authz_interface.pl?action=get_group&select_authzgroup=<s:property value="ldapGroupName"/>"><s:property value="ldapGroupName"/></a>
</td>

<td class="rcvalue">
<s:property value="members.size()"/>
</td>

<td class="rcvalue">
<s:property value="minutesSinceLastUpdate"/>
</td>

</tr>
</s:iterator>
</tbody>
</table>
</div>

<div class="boxed">
<h3>Errors</h3>
<table border="1" cellspacing="0" cellpadding="2">
<thead>
<tr>
<th>Count</th>
<th>Message</th>
</tr>
</thead>
<tbody>
<s:iterator value="@net.juniper.dash.Repository@getErrors()">
<tr><td class="rcvalue">
<s:property value="value"/>
</td>
<td class="rcvalue">
<s:property value="key"/>
</td></tr>
</s:iterator>
</tbody>
</table>
</div>

<%@ include file="footer.jsp" %>