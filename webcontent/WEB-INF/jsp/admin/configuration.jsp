<%@ taglib prefix="s" uri="/struts-tags" %>

<%@ include file="header-admin.jsp" %>
<a name=#top></a>
<div class="boxed">
Note: All of the reloaded configuration will takes effect on the next update process.<br />
<ul>
	<li>Currently, editing the properties have to be done manually to the Configuration file. 
		The next version of admin panel, should give on-site edit feature.
	</li>
	<li>
		All of configuration changes is done independent. So changes in Application properties will
		not trigger the 'reconnect database' 
	</li> 
</ul>
</div>

<h2 id="application">Application Configuration</h2>
<div class="boxed">
<ul>
	<li>Application Properties
	<div class="nav-details">
		<table border="1" cellspacing="0" cellpadding="2">
		<thead>
			<tr>
				<th>Properties Key</th>
				<th>Properties Value</th>
			</tr>
		</thead>
		<tbody>
			<s:iterator value="appConfig">
				<tr>
					<td><s:property value="key" /></td>
					<td><s:property value="value" /></td>
				</tr>
			</s:iterator>
		</tbody>
		</table>
       	<s:form method="POST" namespace="/admin" action="reloadAppConfig">
    		<s:submit value="Reload Application Properties" />
    	</s:form>
    	<small>Reloading the application configuration, will takes effect on the next updates and it is <b>NOT</b> temporary change</small>
	</div>
	</li>
</ul>
</div>

<h2 id="database">Database Configuration</h2>
<div class="boxed">
<ul>
    <li>Connection Properties
    <div class="nav-details">
        <table border="1" cellspacing="0" cellpadding="2">
        <thead>
            <tr>
                <th>Properties Key</th>
                <th>Properties Value</th>
            </tr>
        </thead>
        <tbody>
        <s:iterator value="dbConfig">
            <tr>
                <td><s:property value="key" /></td>
                <td><s:property value="value" /></td>
            </tr>
        </s:iterator>
        </tbody>
        </table>
       	<s:form method="POST" namespace="/admin" action="reloadDB">
    		<s:submit value="Reload Database Properties" />
    	</s:form>
    </div>
    </li>
    <li><a href="<s:url namespace="/admin" includeParams="none"
			       action="conf_reconnectDB"/>" >Reconnect Database</a>
			<div class="navdetails">
			Throw away old connection pool, and build a new one.
			</div>
	</li>
</ul>
<a href="#top">Go to TOP</a>
</div>

<h2 id="ldap">LDAP Configuration</h2>
<div class="boxed">
<ul>
    <li>Property Configuration<br />
    <div class="nav-details">
        <table border="1" cellspacing="0" cellpadding="2">
        <thead>
            <tr>
                <th>Properties Key</th>
                <th>Properties Value</th>
            </tr>
        </thead>
        <tbody>
        <s:iterator value="ldapConfig">
            <tr>
                <td><s:property value="key" /></td>
                <td><s:property value="value" /></td>
            </tr>
        </s:iterator>
        </tbody>
        </table>
    	<s:form method="POST" action="reloadLDAP" namespace="/admin">
    		<s:submit value="Reload LDAP"/>
    	</s:form>
    </div>
    </li>
</ul>
<a href="#top">Go to TOP</a>
</div>

<h2 id="Loggin">Logging Configuration</h2>
<div class="boxed">

<a href="#top">Go to TOP</a>
</div>

<h2 id="multiplier">Multiplier Configuration</h2>
<div class="boxed">
<ul>
    <li>Multiplier Configuration
    <div class="nav-details">
        <table border="1" cellspacing="0" cellpadding="2">
        <thead>
            <tr>
                <th>Properties Key</th>
                <th>Properties Value</th>
            </tr>
        </thead>
        <tbody>
        <s:iterator value="multiplierConfig">
            <tr>
                <td><s:property value="key" /></td>
                <td><s:property value="value" /></td>
            </tr>
        </s:iterator>
        </tbody>
        </table>
       	<s:form method="POST" namespace="/admin" action="reloadMultipliers">
    	<s:submit value="Reload Multipliers" />
    	</s:form>
    </div>
    </li>
</ul>
<a href="#top">Go to TOP</a>
</div>

<h2 id="datasources">DataSources Configuration</h2>
<div class="boxed">
<ul>
    <s:iterator value="dataConfig">
    <li>
    <s:property value="key" /> Property
    <div class="nav-details">
        <table border="1" cellspacing="0" cellpadding="2">
        <thead>
            <tr>
                <th>Properties Key</th>
                <th>Properties Value</th>
            </tr>
        </thead>
        <tbody>
        <s:iterator value="value">
            <tr>
                <td><s:property value="key"/> </td>
                <td><s:property value="value"/> </td>
            </tr>
        </s:iterator>
        </tbody>
        </table>
        <a href="<s:url namespace="/admin" action="reloadDS" > 
                        <s:param name="dsName" value="key" />
                 </s:url>"> Reload <s:property value="key"/></a>
    </div>
    </li><br />
    
    </s:iterator>
    <s:form method="POST" namespace="/admin" action="reloadAllDS">
    <s:submit value="Reload All Datasources Configuration" />
    </s:form>
</ul>
<a href="#top">Go to TOP</a>
</div>

<%@ include file="footer.jsp" %>