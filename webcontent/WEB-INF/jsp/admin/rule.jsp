<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ include file="header-admin.jsp" %>
<div>

<p style="float: right">
(<a href="#inst">Instructions</a>)
</p>

<s:if test="rule.changed">
<span class="warning">This Rule has unsaved changes.</span>
</s:if>
<s:if test="cloning || (rule.newlyCreated  && ! rule.changed)">
<s:set name="dest" value="%{'rule-create'}" />
<s:set name="submit_label" value="%{'Create Rule'}" />
</s:if>
<s:else>
<s:set name="dest" value="%{'rule-edit'}" />
<s:set name="submit_label" value="%{'Submit Changes'}" />
<s:set name="ident" value="rule.identifier" />
</s:else>

<s:form method="POST" namespace="/admin" action="%{#dest}">
<s:hidden name="ruleid" value="%{#ident}"/>
<tr><td class="label">Active</td>
<td class="<s:if test="rule.active">good</s:if><s:else>bad</s:else>">
<s:property value="rule.active"/></td></tr>
<s:textfield  name="rule.identifier" label="Identifier" size="40"
             value="%{rule.identifier}"/>
<s:textfield name="rule.name" value="%{rule.name}" label="Name" size="60"/>
<s:textfield name="rule.lede" value="%{rule.lede}" label="Lede" size="80"/>
<s:textarea name="rule.description" label="Description" rows="5" cols="80"
             value="%{rule.description}" />
<s:textfield name="rule.owner" label="Owner" size="12"
             value="%{rule.owner}"/>
<s:select name="rule.objective" label="Objective"
          list="allObjectives" listKey="name()" listValue="toString()"
          value="%{rule.objective.name()}"/>
<s:select name="rule.weight" label="Weight"
          list="#{0:0, 1:1, 2:2, 3:3, 4:4, 5:5, 6:6, 7:7, 8:8, 9:9, 10:10, 15:15, 20:20, 25:25, 30:30, 40:40, 50:50, 60:60, 70:70, 80:80, 90:90, 100:100}"
          value="%{rule.weight}"/>
<s:select name="rule.milestone" label="Milestone"
          list="allMilestones" listKey="name()" listValue="toString()"
          value="%{rule.milestone.name()}"/>
<s:textfield name="rule.horizon" label="Horizon"
             value="%{rule.horizon}" size="4"/>
<s:select name="rule.sunsetMilestone" label="Sunset Milestone"
          list="allMilestones" listKey="name()" listValue="toString()"
          value="%{rule.sunsetMilestone.name()}"/>
<s:textfield name="rule.sunsetHorizon" label="Sunset Horizon"
             value="%{rule.sunsetHorizon}" size="4"/>
<s:select name="rule.counts" label="Counts" size="1"
          list="allCounts" listKey="name()" listValue="toString()"
          value="%{rule.counts.name()}"/>
<s:textarea name="rule.columnsDelimited" label="Query Link Columns"
            rows="2" cols="90"
            value="%{rule.columnsDelimited}"/>
<s:textfield name="rule.agendaGroup" label="Agenda Group"
             value="%{rule.agendaGroup}"  size="40"/>
<s:textarea name="rule.attributes" label="Attributes"
             value="%{rule.attributes}"  rows="2" cols="90"/>
<s:textarea name="rule.lhs" label="LHS" rows="15" cols="90"
             value="%{rule.lhs}"/>
<s:textarea name="rule.rhs" label="RHS" rows="2" cols="90"
             value="%{rule.rhs}"/>
<s:textfield name="rule.groupingVariable" value="%{rule.groupingVariable}"
             label="Grouping Variable" size="20"/>
<s:if test="null != rule">
<tr><td class="label">Last Updated</td>
<td><s:property value="rule.lastUpdated"/></td></tr>
</s:if>

<s:submit value="%{#submit_label}" />
</s:form>
<s:if test="! (cloning || (rule.newlyCreated  && ! rule.changed))">
<div id="rule-buttons">
<s:form method="GET" namespace="/admin" action="rule-check">
<s:hidden name="ruleid" value="%{rule.identifier}"/>
<s:submit value="Validate" />
</s:form>

<s:if test="rule.active">
<s:form method="POST" namespace="/admin" action="rule-reload">
<s:hidden name="ruleid" value="%{rule.identifier}"/>
<s:submit value="Reload in RuleBase" />
</s:form>
</s:if>

<s:form method="POST" namespace="/admin" action="rule-activity">
<s:hidden name="ruleid" value="%{rule.identifier}"/>
<s:if test="rule.active">
<s:submit value="De-Activate Rule" />
</s:if><s:else>
<s:submit value="Activate Rule" />
</s:else>
</s:form>

<s:form method="POST" namespace="/admin" action="rule-save">
<s:hidden name="ruleid" value="%{rule.identifier}"/>
<s:submit value="Save to Database" />
</s:form>

<s:form method="GET" namespace="/admin" action="show-clone-rule">
<s:hidden name="ruleid" value="%{rule.identifier}"/>
<s:submit value="Clone Rule" />
</s:form>

<s:form method="POST" namespace="/admin" action="rule-delete">
<s:hidden name="ruleid" value="%{rule.identifier}"/>
<s:submit value="Delete Rule" />
</s:form>
</div>
<br />
</div>
<a id="adminToggle">Show full text</a>
<div id="adminInfo">
<pre><s:property value="rule.fullRuleText"/></pre></div>
<br />
</s:if>
<s:else></div></s:else>

<%@ include file="rule-list.jsp" %>

<h2>Create Rule from XML</h2>
<s:form method="POST" namespace="/admin" action="rule-from-xml">
<s:textarea name="ruleAsXML" label="XML"
            rows="8" cols="90"
            value="%{ruleAsXML}"/>
<s:submit value="Create Rule" />
</s:form>

<h2 id="inst">Instructions</h2>

<p>
First thing, read the top level <a href="<s:url namespace="/"
 includeParams="none" action="help"/>">help</a> page.
</p>

<h3>Working with Rules</h3>

<ul>
<li> Create - You can create a rule from scratch, by cloning an existing rule
(either with the clone button, or the <code>(x)</code> link in the rule list),
or by pasting in xml in the form.</li>

<li> Workflow - After making changes, click the <code>Submit Changes</code> button
to submit them.  Assuming there are no errors, next hit <code>Validate</code>
to ensure that the rule parses.  Then <code>Save to Database</code>, so that
your work isn't lost if activation barfs.  Now, finally, you may <code>Activate
Rule</code>.  And then save it again.<br />
Yeah, this is a pain, live with it.</li>

<li>Deleting Rules - When you delete a rule, it is removed from the db, the
rule list, and the Drools RuleBase.  It will continue to appear in agendas
until the next read-assert-fire cycle is complete, at which time it will
be purged from the User scores.<br />
If you delete a Rule by mistake, the form comes up with all the values filled
in, so you can quickly recreate it.</li>

<li>Click the "Reload in RuleBase" button to remove the rule from the RuleBase
and immediately add it back.  Useful when the Drools portion of a rule has
been changed.</li>

<li>Click on "Full Rule Text" below the buttons to see the generated rule
as fed to Drools.</li>

</ul>

<p>Note that if you for some reason change a rule grouping-based to not, or vice
versa, totals in the matrix will be wrong until the system is restarted.</p>

<h3>Rule Fields</h3>

<table class="wwFormTable">
<tr>
    <td class="tdLabel">Identifier:</td>
    <td>Short, machine-readable name for this rule.  Used in URLs and the like.</td>
</tr>

<tr>
    <td class="tdLabel">Name:</td>
    <td>Human readable name.  Used in the DRL for the rule, and as the display
    name in the agenda.  No double quotes.</td>
</tr>

<tr>
    <td class="tdLabel">Lede:</td>
    <td>Short description, displayed in the rule popup and the list of rules.
    </td>
</tr>

<tr>
    <td class="tdLabel">Description:</td>
    <td>Textual description.  A few sentences to a paragraph.  HTML allowed.
    If lede is blank, the first 60 characters of the description are used,
    so don't put any html at the beginning unless you've got a lede.
    </td>
</tr>

<tr>
    <td class="tdLabel">Owner:</td>
    <td>Username of the person responsible for this rule.</td>
</tr>

<tr>
    <td class="tdLabel">Objective:</td>
    <td>What aspect of the JUNOS process is this Rule aimed at?</td>
</tr>

<tr>
    <td class="tdLabel">Weight:</td>
    <td>The amount of opportunity per offending record.  Set to zero to create
    a rule that will not show up on the agenda, but can be used in a matrix.</td>
</tr>

<tr>
    <td class="tdLabel">Milestone:</td>
    <td>The milestone at which this rule starts accruing opportunity.</td>
</tr>

<tr>
    <td class="tdLabel">Horizon:</td>
    <td>How many days before (positive) or after (negative) the milestone
    that the rule starts accruing.</td>
</tr>

<tr>
    <td class="tdLabel">Sunset Milestone:</td>
    <td>The milestone at which opportunity for this rule goes to zero.</td>
</tr>

<tr>
    <td class="tdLabel">Sunset Horizon:</td>
    <td>How many days before (positive) or after (negative) the sunset milestone
    that opportunity is no longer assigned.</td>
</tr>

<tr>
    <td class="tdLabel">Counts:</td>
    <td>What sort of Records does this Rule count?  Rules counting NOTHING
    are considered "bookkeeping" rules, and are not visible to users.</td>
</tr>

<tr>
    <td class="tdLabel">Query Link Columns:</td>
    <td>The columns that should be displayed in the data source query results.
    These should be in the form expected by gnatsweb or Deepthought.</td>
</tr>

<tr>
    <td class="tdLabel">Agenda Group:</td>
    <td>Used for bookkeeping rules that need to be executed in a particular
    order.  Agenda groups are run in ascending alphanumeric order.</td>
</tr>

<tr>
    <td class="tdLabel">Attributes:</td>
    <td>Other Drools attributes for the Rule (salience, lock-on-active, etc).
    Generally used only for bookkeeping rules.</td>
</tr>

<tr>
    <td class="tdLabel">LHS:</td>
    <td>The Left Hand Side of the rule.  The meat of the matter.</td>
</tr>

<tr>
    <td class="tdLabel">RHS:</td>
    <td>The Right Hand Side of the rule, the "then" part.  Generally used
    only for bookkeeping rules.</td>
</tr>

<tr>
    <td class="tdLabel">Grouping Variable:</td>
    <td>The variable referenced in the auto-generated RHS for standard
    scoring rules.  Don't include the $ sigil.</td>
</tr>
</table>

<%@ include file="footer.jsp" %>
