<%@ taglib prefix="s" uri="/struts-tags" %>

<%@ include file="header-admin.jsp" %>

<h2 id='summary'>Overall Stats</h2>
<div class="boxed">
	<table border="1" cellspacing="0" cellpadding="1">
	<thead>
		<tr>
		<th>Name</th>
		<th>Value</th>
		</tr>
	</thead>
	<tbody>
		<tr>
		<td class="rclabel">Update Cycles</td>
		<td class="rcvalue">
		<s:property value="@net.juniper.dash.Repository@getUpdateCycles()"/>
		</td>
		</tr>
		
		<tr>
		<td class="rclabel">Average of Last 10 Update Times</td>
		<td class="rcvalue">
		<s:property value="@net.juniper.dash.Repository@getAverageRecentUpdateCycleTime()"/>
		</td>
		</tr>
		
		<tr>
		<td class="rclabel">Average Update Time</td>
		<td class="rcvalue">
		<s:property value="@net.juniper.dash.Repository@getAverageUpdateCycleTime()"/>
		</td>
		</tr>
		
		<tr>
		<td class="rclabel">Connection Cache</td>
		<td class="rcvalue">
		<s:property value="connCacheStats"/>
		</td>
		</tr>
		
		<tr>
		<td class="rclabel">Number of Errors</td>
		<td class="rcvalue">
		<s:property value="@net.juniper.dash.Repository@getErrors().size()"/>
		</td>
		</tr>
		
		<tr>
		<td class="rclabel">Last Error at</td>
		<td class="rcvalue">
		<s:property value="@net.juniper.dash.Repository@lastErrorDate"/>
		</td>
		</tr>
		
		<tr>
		<td class="rclabel">Number of Users</td>
		<td class="rcvalue">
		<s:property value="allUsers.size()"/>
		</td>
		</tr>
	</tbody>
	</table>
</div>

<h2 id='logs'>Errors</h2>
<div class="boxed">
	<table border="1" cellspacing="0" cellpadding="2">
	<thead>
		<tr>
		<th>Count</th>
		<th>Message</th>
		</tr>
	</thead>
	<tbody>
		<s:iterator value="@net.juniper.dash.Repository@getErrors()">
			<tr><td class="rcvalue">
			<s:property value="value"/>
			</td>
			<td class="rcvalue">
			<s:property value="key"/>
			</td></tr>
		</s:iterator>
		</tbody>
	</table>
</div>

<%@ include file="footer.jsp" %>