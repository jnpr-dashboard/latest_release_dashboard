<%@ taglib prefix="s" uri="/struts-tags" %><%@ include file="header-admin.jsp" %>

<div class="boxed">
<table dojoType="filteringTable" id="releasetable"
       multiple="false" alternateRows="false" maxSortable="1"
       border="1" cellspacing="0" cellpadding="2">
<thead>
<tr>
<th dataType="String">Release</th>
<th dataType="html">Opprtnty</th>
<th dataType="Number">RLIs</th>
<th dataType="Number">Release<br />Units</th>
<th dataType="Number">Release<br />Last Upd</th>
<th dataType="Number">Status<br />Units</th>
<th dataType="Number">Status<br />Last Upd</th>
<th dataType="Number">Core Mgr<br />Units</th>
<th dataType="Number">Core Mgr<br />Last Upd</th>
<th dataType="Number">Other Mgr<br />Units</th>
<th dataType="Number">Other Mgr<br />Last Upd</th>
</tr>
</thead>
<tbody>
<s:iterator value="activeReleases" status="itstat">
<s:set name="refreshers" value="refreshersForRelease(relname)"/>
<tr value="<s:property value="#itstat.index"/>">

<td class="rclabel">
<a href="<s:url namespace="/admin" action="detail-release">
<s:param name="relname" value="relname" />
</s:url>"><s:property value="relname"/></a>
</td>

<td class="rcvalue">
<a href="<s:url namespace="/" action="release">
<s:param name="relname" value="relname" />
<s:param name="mgr" value="noUser.name" />
</s:url>"><s:property value="scoreForRelease(relname)"/></a>
</td>

<td class="rcvalue">
<s:property value="rliCountForRelease(relname)"/>
</td>

<td class="rcvalue"><s:property value="#refreshers.get(3).unitsCompleted"/>
<s:if test="! #refreshers.get(3).alive">
<span class="bad">Dead: <s:property value="#refreshers.get(3).deathCertificate"/></span>
</s:if>
</td>
<td class="rcvalue"><s:property value="#refreshers.get(3).minutesSinceLastUnit"/></td>

<td class="rcvalue"><s:property value="#refreshers.get(2).unitsCompleted"/>
<s:if test="! #refreshers.get(2).alive">
<span class="bad">Dead: <s:property value="#refreshers.get(2).deathCertificate"/></span>
</s:if>
</td>
<td class="rcvalue"><s:property value="#refreshers.get(2).minutesSinceLastUnit"/></td>

<td class="rcvalue"><s:property value="#refreshers.get(1).unitsCompleted"/>
<s:if test="! #refreshers.get(1).alive">
<span class="bad">Dead: <s:property value="#refreshers.get(1).deathCertificate"/></span>
</s:if>
</td>
<td class="rcvalue"><s:property value="#refreshers.get(1).minutesSinceLastUnit"/></td>

<td class="rcvalue"><s:property value="#refreshers.get(0).unitsCompleted"/>
<s:if test="! #refreshers.get(0).alive">
<span class="bad">Dead: <s:property value="#refreshers.get(0).deathCertificate"/></span>
</s:if>
</td>
<td class="rcvalue"><s:property value="#refreshers.get(0).minutesSinceLastUnit"/></td>

</tr>
</s:iterator>
</tbody>
</table>
</div>

<%@ include file="footer.jsp" %>
