</div>
<div id="footer">
 <div class="noprint" id="footer-links">
  <a href="http://www-int.juniper.net/">Intranet Home</a> |
  <a href="https://wintermute.juniper.net/junos/">Wintermute</a> |
  <a href="http://freebase.juniper.net/">Freebase</a> |
  <a href="http://eng-in.juniper.net/cgi-bin/throttle/tp.cgi">Throttle Req</a> |
  <a href="https://deepthought.juniper.net/app/index.jsp?tableName=RLI">Deepthought</a> |
  <a href="https://gnats.juniper.net/web/default">GNATS</a> |
  <a href="https://gnats.juniper.net/cgi-bin/gnats.review?database=review">Review Tracker</a>
 </div>
 <div class="footer-left">Copyright &copy; 2009. Juniper Networks, Inc.</div>
 <div class="footer-right noprint">Contact: <a href="mailto:dashboard-admin@juniper.net">dashboard-admin@juniper.net</a></div>
 <div class="footer-right print-only">Juniper Confidential</div>
</div>

<script>
	// Collection of Javascript in Admin Page
    $(document).ready(function(){
        $(".tree-admin-left-menu").treeview();
		// Easy Multiple Accordion from http://paste.pocoo.org/show/105888/
        $("#menu_link").addClass("ui-accordion ui-widget ui-helper-reset").find("h2")
		.addClass("ui-accordion-header ui-helper-reset ui-state-default ui-corner-top ui-corner-bottom")
		.prepend('<span class="ui-icon ui-icon-triangle-1-e"/>')
		.click(function() {
			$(this).toggleClass("ui-accordion-header-active").toggleClass("ui-state-active")
				.toggleClass("ui-state-default").toggleClass("ui-corner-bottom")
			.find("> .ui-icon").toggleClass("ui-icon-triangle-1-e").toggleClass("ui-icon-triangle-1-s")
			.end().next().toggleClass("ui-accordion-content-active").slideToggle();
			return false;
		})
		.next().addClass("ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom").show();
    });
</script>

</body>
</html>
