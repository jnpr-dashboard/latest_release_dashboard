<%@ taglib prefix="s" uri="/struts-tags" %>
<h3>Rules</h3>
<div class="note">
(+) = Clone Rule; (x) = Dump as XML; Rule Name = Edit Rule
</div>
<table border="1" cellpadding="1" cellspacing="0" parseWidgets="false"><%-- Dojo widget parsing TURNED OFF in this ul --%>
<s:iterator value="rules">
<tr><td><s:if test="! active"><span class="greyed">(inactive)</s:if>
<s:else><span></s:else>
<s:url id="cloneurl" includeParams="none" action="show-clone-rule">
<s:param name="ruleid" value="identifier"/>
</s:url><s:a href="%{cloneurl}">(+)</s:a>
<s:url id="xmlurl" includeParams="none" action="stream-rule">
<s:param name="ruleid" value="identifier"/>
</s:url><s:a href="%{xmlurl}">(x)</s:a></td><td>
<s:property value="counts"/>
</td><td>
<s:property value="identifier"/>
</td><td>
<s:url id="ruleurl" includeParams="none" action="show-rule-edit">
<s:param name="ruleid" value="identifier"/>
</s:url><s:a href="%{ruleurl}"><s:property value="name"/></s:a>
<s:if test="agendaGroup.length() > 0"> &nbsp;
[Agenda Group: <s:property value="agendaGroup"/>]
</s:if>
<s:if test="changed"> &nbsp; <span class="bad">(changed)</span></s:if>
</span></td></tr>
</s:iterator>
</table>
