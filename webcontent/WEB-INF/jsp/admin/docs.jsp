<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ include file="header-admin.jsp" %>
<a name="#top"></a>
<ul>
<li> <a href="#infra">Infrastructure</a></li>
<li> <a href="#cvs">CVS</a></li>
<li> <a href="#newds">Adding a new DataSource</a></li>
<li> <a href="#howitworks">How it Works</a></li>
<li> <a href="#vteams">Virtual Teams</a></li>
<li> <a href="#release">Rollout Procedure</a></li>
</ul>

<h2 id="infra">Infrastructure</h2>
<p>
Currently dashboard runs on 64bit Solaris machine on vaxdash.juniper.net. Bowker still
acts as the front-end proxy. Our failover machine which is also the development machine
is imdash.juniper.net. <br /> 
The application and its infrastructure is (mostly) contained in /opt/dashboard.
It uses the same apache front-end as Deepthought, and depends on Oracle
10g libraries and perl 5.8 in /usr/local/bin.
</p>

<p>
The install directory (root in /opt) contains the following:
<table border="1" cellspacing="1" cellpadding="2">
<thead>
<tr>
<th>Directory</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr>
<td>ant -> apache-ant-1.8.1/</td>
<td>The 'ant' build tool</td>
</tr>
<tr>
<td>tomcat -> apache-tomcat-6.0.24/</td>
<td>A minimally modified install of the Tomcat servlet engine.
See below for details.</td>
</tr>
<tr>
<td>dashboard/application/</td></td>
<td>The Dashboard code.  The actual running code is in the <code>build</code>
subdirectory.</td>
</tr>
<tr>
<td>java -> jdk1.6.0_18/</td>
<td>Captive Java install</td>
</tr>
<tr>
<td>src/</td>
<td>Upstream tarballs of tomcat, ant and Java. Also contains the solaris package to rebuild the server</td>
</tr>
<tr>
<td>dashboard/special/</td>
<td>Server-specific config files:
<pre>
dash-db.properties - Oracle server, username &amp; password
dashboard.crt      - Deepthought cert for RLI and Milestone data scraping
dashboard.csr      - Deepthough cert request (just in case)
dashboard.key      - Deepthought cert key.
debug-value.xml    - Included in struts.xml to set debug status
gnats.properties   - GNATS host/port/db connection info, and path to query-pr
[datasource].properties - Datasource connection properties
</pre>
</td>
</tr>
<tr>
	<td>dashboard/snapshot</td>
	<td>Snapshot for Backlog and MTTR</td>
</tr>
</tbody>
</table><br />
</p>

<h3>Tomcat Customizations</h3>

<p>
The Tomcat install has the following customizations:

<ul>
<li> The various example webapps have been removed from $CATALINA_HOME/webapps
so that they don't use up system resources.
</li>

<li> Several config files have been modified.  All the changed configs have
been committed to CVS, and can be found in the Dashboard directory, or in
CVS <a
href="http://cvs.juniper.net/cgi-bin/viewcvs.cgi/sw-tools/src/dashboard/configs/tomcat/?sortby=date">here</a></li>

<li> The Oracle JDBC library, <code>10g-ojdbc14.jar</code>, has been copied to
$CATALINA_HOME/lib.
</li>

<li> The jar file for tomcat-specific ant tasks has been copied from
$CATALINA_HOME/lib/catalina-ant.jar to /opt/dashboard/ant/lib.
(Yeah, this is an ant customization...)
</li>

<li> When setting up a new tomcat, install like so:
<pre>
sudo tar -xzf /path/to/apache-tomcat-6.XX.tar.gz

sudo chgrp -R sw-tools conf logs temp webapps work
sudo chmod -R g+w conf logs temp webapps work
sudo chown nobody work temp logs
rm -r webapps/*
</pre>
</li>

</ul>
</p>


<h3>Apache Customizations</h3>

<p>
Tomcat is integrated with apache via a standard mod_jk setup, with the addition
of a clause that lets apache serve up static content directly.  Without this,
tomcat will fail to properly serve *.html files, which breaks the dojo
javascript library.  The apache configs (with Deepthought and Dashboard info
muddled together) can be found <a
href="http://cvs.juniper.net/cgi-bin/viewcvs.cgi/sw-tools/src/dashboard/configs/apache/?sortby=date">here</a>.
</p>
<a href="#top">Go to Top</a>


<h2 id="cvs">CVS</h2>
<p>
The source is in cvs.juniper.net at
<a href="http://cvs.juniper.net/cgi-bin/viewcvs.cgi/sw-tools/src/dashboard/?sortby=date">sw-tools/src/dashboard</a>.
This document is <a href="http://cvs.juniper.net/cgi-bin/viewcvs.cgi/*checkout*/sw-tools/src/dashboard/webcontent/WEB-INF/jsp/admin/docs.jsp">here</a>.
</p>
<a href="#top">Go to Top</a><br />


<h2 id="newds">Adding a new DataSource</h2>
<ol>
<li> Copy whichever existing DataSource subclass most closely resembles the
new one.  This will save you some typing.</li>

<li> Add the new DS to the list at the top of DataSource:
<pre>
    public static final NPI NPI = new NPI("NPI Programs");
    <strong>public static final NewDs NEWDS = new NewDs("A Source");</strong>
</pre>
and:
<pre>
    static {
        sources.put(RLI.type, RLI);
        ...
        <strong>sources.put(NEWDS.type, NEWDS);</strong>
    };
</pre></li>

<li> Decide if you're going to need to generate the Record bean subclass
dynamically (by querying Deepthought or GNATS), or if you can code a simple
static class for it.
<ul>
<li> If it needs to be dynamic, use one of the existing gen-XXX-bean.pl scripts
in src/bin as a template.  Don't forget to add your new generator to
<code>regen-beans.sh</code> in src/bin.</li>
<li> Otherwise, copy an appropriate existing Record subclass.</li>
</ul></li>

<li> Add a type to Rule.Counts for your datasource (assuming you want to
write rules about its records).</li>

<li> Add an appropriate if-block to broken-rule.jsp based on the Counts
type.</li>

<li> If it's not a GNATS or Deepthought source, you'll need to create a
new XXX-link.jsp to generate links to collected records in the source.
Copy one of the existing XXX-link.jsp files, and <strong>understand</strong> the
link-vs-form calculation.</li>

<li> If your type is a grouping type (like a release or an NPI program,
something that groups other records like RLIs and PRs):
<ul>

<li> You need to add an XXX-info.jsp fragment.  Have a look at the Release
and NPI ones.</li>

<li> Add appropriate mappings to struts.xml</li>

<li> Muck around in <code>net.juniper.dash.struts.DashSupport.java</code>
as necessary.</li>
</ul>
</li>
<li> Heck, you'll probably have to do a million other things, but this
will at least get you started...</li>
</ol>
<a href="#top">Go to Top</a>

<h2 id="howitworks">How it Works</h2>
<p>
First, read the <a href="<s:property value="uiBasePath"/>/help">user docs</a> for an overview of
the application.
</p>

<p>
Information is read in from multiple data sources, generally by running an
external program (perl/RLI::api for Deepthought, query-pr for GNATS) which
writes to stdout.  Delimited records are read, parsed, and turned into
Record objects.  New or changed Records are asserted into the working memory
of the Drools rules engine.
</p>

<p>
The rules collect matching records, and "attach" them to the appropriate
Records (usually a User).  These collections of records are associated with
the offending rule, and a score is calculated for each.
</p>


<h3>Application flow</h3>
<ul>

<li> Tomcat starts, initializes Struts, which in turn instantiates
<code>net.juniper.dash.struts.ApplicationListener</code>, and calls the
<code>contextInitialized</code> method.  This, finally, calls the real
Dashboard code in the static method
<code>net.juniper.dash.Repository.initialize()</code>.
</li>

<li> The Repository sets up the Oracle connection pool, reads the LDAP params,
creates a <code>net.juniper.dash.Ruler</code> (which preps the rules engine),
and a <code>net.juniper.dash.Updater</code>, which does most of the work.
</li>

<li> The <code>Updater</code> fires off several threads that:
<ul>
<li> Refresh the various groups from the LDAP server.</li>
<li> Read in new and changed Records from each DataSource.</li>
<li> Fire the rules and handle some accounting.</li>
</ul>
It also spawns a watchdog thread to (hopefully) restart things if the
process borks.
</li>

<li> And that's it.  Now the big heap of threads just keeps spinning out their
updates.  When you shut down the application, all the threads are told to stop.
</li>

</ul>

<p>
All of the worker threads are instances of <code>net.juniper.dash.Refresher</code>.
This class is mostly a thick wrapper around Thread that provides methods for
controlling and querying thread state, and a framework for throttling update
rate.
</p>
<a href="#top">Go to Top</a><br />

<h2 id="vteams">Virtual Teams</h2>
<p>
See the
<a href="<s:url namespace="/" action="help" includeParams="none"/>#vteams">user docs</a>
to get started.
</p>

<p>
A team is defined by an AuthLDAP group.  Until the namespace feature is rolled
out, we fake a namespace by prepending "vt-" to the name of the team in the
authgroup name.  When setting up a group, don't put any managers' names in the
"Imported Groups" section, since the Dashboard handles reporting structure
expansion.  Just put them in the "Individual users" section.  After you create
the group, wait until the next update cycle has finished (cycles currently
take <s:property value="@net.juniper.dash.Repository@getAverageRecentUpdateCycleTime()"/>
minutes) , make sure that the VT has status, and send the following email
template to the owner:  
</p>
<blockquote><pre>
Your Dashboard Virtual Team has been created.  You can view it here:

https://deepthought.juniper.net/dashboard/agenda/<strong>NAME-OF-VT</strong>

You may adjust the membership of the team via the AuthLDAP system
by clicking on the "[Edit virtual team]" link at the bottom of
the right column of the team's Agenda page.  Please see the
AuthLDAP documentation (linked from the top of each page) for
details on using the system.  Don't put any managers' names in
the "Imported Groups" section, since the Dashboard handles
reporting structure expansion.

If you don't view status for the virtual team for an extended period,
we reserve the right to delete it from the system to keep things tidy.

Thank you,
Your pals in dashboard-admin@juniper.net
</pre></blockquote>

<p>
There's a list of all VTs on the <a href="<s:url namespace="/admin" action="datasource">
<s:param name="dsName" value="%{'Users'}"/></s:url>">User datasource page</a>.</li>
Each User keeps track of the number of times its status has been viewed
since the last time the application was started, and the time of the last
viewing.  Use this to prune moribund teams from the system.
</p>
<a href="#top">Go to Top</a><br />


<h2 id="rollout">Rollout Procedure</h2>

<p>
How to roll out a new version of the code.  Dashboard releases on the same
schedule as Deepthought, so both apps will be down for maintenance at the same
time. 
<pre>
The document can be found <a href="http://cvs.juniper.net/cgi-bin/viewcvs.cgi/*checkout*/sw-tools/src/dashboard/RolloutSteps.txt">here</a>.  
</pre>
</p>
<a href="#top">Go to Top</a><br />

<%@ include file="footer.jsp" %>
