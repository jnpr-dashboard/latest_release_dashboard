<%@ taglib prefix="s" uri="/struts-tags" %>

<%@ include file="header-admin.jsp" %>
<h2>View "<s:property value="dsName"/>" Properties</h2>

	<s:if test="null == dataSource">
	<div class="boxed">	
	<span class="bad">Error:</span>
	No DataSource called "<s:property value="dsName"/>"
	</div>
	</s:if>
	<s:else>
<div class="boxed">
	<table border="1" cellspacing="0" cellpadding="1">
	<thead>
	<tr>
	<th>Name</th>
	<th>Value</th>
	</tr>
	</thead>
	<tbody>
	
	<tr>
	<td class="rclabel">Name</td>
	<td class="rcvalue">
	<s:property value="dataSource.name"/>
	</td>
	</tr>
	
	<tr>
	<td class="rclabel">dependsOn</td>
	<td class="rcvalue">
	<a href="<s:url namespace="/admin" action="datasource">
	<s:param name="dsName" value="dataSource.dependsOn.name"/>
	</s:url>"><s:property value="dataSource.dependsOn.name"/></a>
	</td>
	</tr>
	
	<tr>
	<td class="rclabel">recordsUpdated</td>
	<td class="rcvalue">
	<s:property value="dataSource.recordsUpdated"/>
	</td>
	</tr>
	
	<tr>
	<td class="rclabel">records.size()</td>
	<td class="rcvalue">
	<s:property value="dataSource.records.size()"/>
	</td>
	</tr>
	
	<tr>
	<td class="rclabel">notAsserted.size()</td>
	<td class="rcvalue">
	<s:property value="dataSource.notAsserted.size()"/>
	</td>
	</tr>
	
	</tbody>
	</table>
</div>
	</s:else>

<s:if test="dsName == 'Users'">
<h2>Virtual Teams</h2>
<div id="vteams" class="boxed">
	<table border="1" cellspacing="0" cellpadding="1">
	<thead>
	<tr>
	<th>Name</th>
	<th>Score</th>
	<th>Owner</th>
	<th>Views</th>
	<th>Last Viewed</th>
	</tr>
	</thead>
	<tbody>
	<s:iterator value="allVirtualTeams" id="vt" status="itstat">
	<tr>
	<td><a href="<s:url namespace="/admin" action="record">
	<s:param name="dsName" value="%{'Users'}"/>
	<s:param name="recId" value="id"/>
	</s:url>"><s:property value="id"/></a></td>
	<td><a href="<s:url namespace="/" action="agenda">
	<s:param name="uid" value="id"/>
	</s:url>"><s:property value="totalScore()"/></a></td>
	<td><s:property value="manager"/></td>
	<td><s:property value="numViews"/></td>
	<td><s:property value="lastViewed"/></td>
	</tr>
	</s:iterator>
	</tbody>
	</table>
</div>
</s:if>

<s:if test="dataSource.picklists.size() > 0">
<h2>Picklist Ordering</h2>
<div id="picklists" class="boxed">
	<ul>
	<s:iterator value="dataSource.picklists.keySet()" id="field" status="itstat">
	<li><strong><s:property value="#field"/></strong>
	<table border="1" cellspacing="0" cellpadding="1">
	<thead>
	<tr>
	<th>Value</th>
	<th>Ordinal</th>
	</tr>
	</thead>
	<tbody>
	<s:iterator value="dataSource.picklists.get(#field).entrySet()"
	            id="fMapEntry" status="itstat">
	<tr><td class="rclabel"><s:property value="#fMapEntry.key"/></td>
	<td class="rcvalue"><s:property value="#fMapEntry.value"/></td></tr>
	</s:iterator>
	</tbody>
	</table><br />
	</li>
	
	</s:iterator>
	</ul>
	
</div>
	</s:if>

<%@ include file="ds-list.jsp" %>

<%@ include file="footer.jsp" %>
