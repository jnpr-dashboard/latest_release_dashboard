<%@ taglib prefix="s" uri="/struts-tags" %>

<%@ include file="header-admin.jsp" %>
<!--  Overview Admin Dashboard -->
		<h2>Usefull Administration Commands</h2>    	
    	<div class="boxed">
        	<ul>
			<li><a href="<s:url namespace="/admin" includeParams="none"
			       action="show-create-rule"/>" >Create Rule</a>
			<div class="navdetails">
			Create a new rule.
			</div></li>

			<li><a href="<s:url namespace="/admin" includeParams="none"
			       action="fireAllRules"/>">Fire All Rules</a>
			<div class="navdetails">
			Call WorkingMemory.fireAllRules() and Repository.writeJson(true).  Don't do this
			unless the system is sleeping.
			</div></li>
			
			<li><a href="<s:url namespace="/admin" includeParams="none"
			       action="reconnectDB"/>">Reconnect to Database</a>
			<div class="navdetails">
			Throw away old connection pool, and build a new one.
			</div></li>
			
			<li><a href="<s:url namespace="/admin" includeParams="none"
			       action="wakeUpdater"/>">Wake Updater</a>
			<div class="navdetails">
			Start a new read-assert-fire cycle.  Don't do this unless the system is sleeping.
			</div></li>
			
			<li>Pause updates for the next <small>(currently set at <s:property value="pauseMinutes" /> minutes).</small>
				<s:form method="POST" namespace="/admin" action="pause-updates">
					<s:select name="pauseMinutes" label="Pause updates for the next"
						list="#{'60':'hour',
								'120':'2 hours',
								'180':'3 hours',
								'240':'4 hours',
								'720':'12 hours',
								'144':'day',
								'1008':'week',
								'525600':'10 years',
								'0' : '(zero)'
								}" />
					<s:submit value="Go"/>
				</s:form>
			</li>
			
			<li>Soft Restart 
				<s:form method="POST" namespace="/admin" action="restartProcessing">
				<s:submit value="Go" />
				</s:form>
				Throw away the RuleBase, WorkingMemory, and all data & scores.  Read
				the rules from the db, and start over.
			</li>
			</ul>
    	</div>

		<h2>DataSources list</h2>
		<%@ include file="ds-list.jsp" %>

		<h2>Edit Rule</h2>
		<div class="boxed">
			<s:form method="GET" namespace="/admin" action="show-rule-edit">
			<s:select name="ruleid" label="Identifier" size="1"
			          list="rules" listKey="identifier" listValue="%{ counts + ': ' + name}"/>
			<s:submit value="Go" />
			</s:form>
		</div>

<%@ include file="footer.jsp" %>