<%@ page contentType="text/html; charset=UTF-8" %><%@
 taglib uri="/struts-tags" prefix="s" %><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
<head>
  <title><s:property value="title" default="JUNOS Dashboard"/></title>
 <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
 <link href="<s:property value="uiBasePath"/>/inc/styles/dash.css" rel="stylesheet" type="text/css"/>
 <link href="<s:property value="uiBasePath"/>/inc/styles/ui.base.css" rel="stylesheet" type="text/css"/>
 <link href="<s:property value="uiBasePath"/>/inc/styles/ui.theme.css" rel="stylesheet" type="text/css"/>
 <link href="<s:property value="uiBasePath"/>/inc/styles/ui.accordion.css" rel="stylesheet" type="text/css"/>
 <link href="<s:property value="uiBasePath"/>/inc/styles/superfish.css" rel="stylesheet" type="text/css"/>
 <link href="<s:property value="uiBasePath"/>/inc/styles/jquery.autocomplete.css" rel="stylesheet" type="text/css"/>
 <link href="<s:property value="uiBasePath"/>/inc/styles/jquery.treeview.css" rel="stylesheet" type="text/css"/>
 <link href="<s:property value="uiBasePath"/>/inc/styles/print.css"
       rel="stylesheet" type="text/css" media="print"/>
<!--[if lt IE 7]>
  <link href="<s:property value="uiBasePath"/>/inc/ie.css" rel="stylesheet" type="text/css"/>
<![endif]-->
<script type="text/javascript">
 var dash_base_url = "dashboard";
 var page = "admin";
</script>
<script src="<s:property value="uiBasePath"/>/inc/jquery/jquery.js" type="text/javascript"></script>
<script src="<s:property value="uiBasePath"/>/inc/jquery/ui.js" type="text/javascript"></script>
<script src="<s:property value="uiBasePath"/>/inc/jquery/jquery.qtip.js" type="text/javascript"></script>
<script src="<s:property value="uiBasePath"/>/inc/jquery/jquery.autocomplete.js" type="text/javascript"></script>
<script src="<s:property value="uiBasePath"/>/inc/jquery/jquery.hoverIntent.js" type="text/javascript"></script>
<script src="<s:property value="uiBasePath"/>/inc/jquery/superfish.js" type="text/javascript"></script>
<script src="<s:property value="uiBasePath"/>/inc/jquery/jquery.cookie.js" type="text/javascript"></script>
<script src="<s:property value="uiBasePath"/>/inc/jquery/jquery.bgiframe.js" type="text/javascript"></script>
<script src="<s:property value="uiBasePath"/>/inc/jquery/jquery.treeview.js" type="text/javascript"></script>
<script src="<s:property value="uiBasePath"/>/inc/dash.js" type="text/javascript"></script>
<link rel="shortcut icon" href="/favicon.ico" />
</head>
<body>
<h1 class="print-only"><span style="color:blue">JUNOS</span> Dashboard</h1>
<div id="header">
 <ul class="sf-menu main-menu">
  <li><a name="#">JUNOS Dashboard</a></li>
  <li><a href="<s:property value="uiBasePath"/>/agenda/junos">JUNOS &raquo;</a>
   <ul>
    <li><a href="<s:property value="uiBasePath"/>/prs/junos">PRs</a></li>
    <li><a href="<s:property value="uiBasePath"/>/agenda/junos">Agenda</a></li>
   </ul>
  </li>
  <li><a href="<s:property value="uiBasePath"/>/npis">NPIs</a></li>
  <li><a href="<s:property value="uiBasePath"/>/matrix-form">Matrix</a></li>
  <li><a>Report  &raquo;</a>
    <ul>
        <li><a href="<s:property value="uiBasePath"/>/matrix-form">Matrix</a></li>
        <li><a href="<s:property value="uiBasePath"/>/rli-report/junos">RLI Report</a></li>
        <li><a href="<s:property value="uiBasePath"/>/backlogprs/junos">Backlog PRs</a> </li>
        <li><a href="<s:property value="uiBasePath"/>/mttr/junos">MTTR</a> </li>
    </ul>
  </li>
  <li><a>Utilities &raquo;</a>
   <ul>
    <li><a href="<s:property value="uiBasePath"/>/pr_date">PR Deploy Date</a>
   </ul></li>
  <li><a href="<s:property value="uiBasePath"/>/help">Help &raquo;</a>
   <ul>
    <li><a href="<s:property value="uiBasePath"/>/help">Help</a>
    <li><a href="<s:property value="uiBasePath"/>/rules">List Rules</a></li>
    <li><a href="<s:property value="uiBasePath"/>/browser-reqs">Requirements</a></li>
    <li><a href="<s:property value="uiBasePath"/>/bglist">BU/BG List</a>
   </ul></li>
  <li><a name="#">
   <form id="dwim-form" method="get" action="<s:property value="uiBasePath"/>/dwim" style="margin: 0px; display: inline;">
   <input id="dwim" name="s" type="text" size="30" value=""/>
   <input type="submit" value="dwim" class="sf-menu-button" style="height: 1.8em;"/></form></a>
  </li>
   <li><a href="/dashjava/admin/main">Admin &raquo; </a>
        <ul>
            <li><a href="<s:property value="uiBasePath"/>/admin">Django-admin</a></li>
        </ul>
   </li>
 </ul><br clear="both" />
 <div class="tooltip" id="tt_dwim">
  <table>
  <tr><th>If you want</th><th>Enter</th></tr>
  <tr><td>Agenda for user</td><td><code>username</code></td></tr>
  <tr><td>Release agenda for you or user</td><td><code>[username] m.n</code></td></tr>
  <tr><td>PR matrix for you or user</td><td><code>[username] prs</code></td></tr>
  <tr><td>View PR(s)</td><td><code>pr nnnn[ nnnn...]</code></td></tr>
  <tr><td>View rli(s)</td><td><code>rli nnnn[ nnnn...]</code></td></tr>
  </table>
 </div>
 <div id="loading">thinking...</div>
<s:if test="infoMessage.length() > 0">
<div id="messagebox"><s:property value="infoMessage"/></div></s:if>
</div>

