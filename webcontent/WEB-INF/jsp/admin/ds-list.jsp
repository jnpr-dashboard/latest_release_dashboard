<%@ taglib prefix="s" uri="/struts-tags" %>
<div id="misc" class="boxed">
<h3>DataSources</h3>
<table border="1" cellspacing="0" cellpadding="2">
<thead>
<tr>
<th>Name</th>
<th>Records</th>
<th>View</th>
</tr>
</thead>
<tbody>
<s:iterator value="dataSources" status="itstat">
<tr value="<s:property value="#itstat.index"/>">
<td class="rclabel"><a href="<s:url namespace="/admin" action="datasource">
<s:param name="dsName" value="name"/>
</s:url>"><s:property value="name"/></a></td>
<td class="rcvalue">
<s:property value="records.size()"/>
</td>
<td class="rcvalue">
<s:form id="record_form" method="get" namespace="/admin" action="record"
   theme="simple" name="record_form">
 <input type="hidden" name="dsName" value="<s:property value="name"/>"/>
 <input name="recId" size="10"/>
 <s:submit type="input" value="%{'view'}" />
</s:form>
</td>
</tr>

</s:iterator>
</tbody>
</table>
</div>
