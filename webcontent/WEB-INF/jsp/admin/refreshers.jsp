<%@ taglib prefix="s" uri="/struts-tags" %><%@ include file="header-admin.jsp" %>

<div id="refreshers" class="boxed">
<table dojoType="filteringTable" id="refreshertable"
       multiple="false" alternateRows="false" maxSortable="1"
       border="1" cellspacing="0" cellpadding="2">
<thead>
<tr>
<th field="Name" dataType="String">Name</th>
<th field="Alive" dataType="String">Alive</th>
<th field="State" dataType="String">State</th>
<th field="RTEs" dataType="html">RTEs</th>
<th field="Units" dataType="Number">Units</th>
<th field="Time" dataType="Number">Last Upd</th>
</tr>
</thead>
<tbody>
<s:iterator value="refreshers" status="itstat">
<tr value="<s:property value="#itstat.index"/>">
<td class="rclabel"><s:property value="name"/></td>
<td class="rcvalue">
<s:if test="alive"><span class="good">Yes</span></s:if>
<s:else><span class="bad">No:<br />
<s:property value="deathCertificate"/></span>
</s:else>
</td>
<td class="rcvalue"><s:property value="state"/></td>
<td class="rcvalue">
<s:if test="exceptionCount > 0"><span class="bad"><s:property
 value="exceptionCount"/></span></s:if><s:else>0</s:else>
</td>
<td class="rcvalue"><s:property value="unitsCompleted"/></td>
<td class="rcvalue"><s:property value="minutesSinceLastUnit"/></td>
</tr>
</s:iterator>
</tbody>
</table>
</div>

<%@ include file="footer.jsp" %>
