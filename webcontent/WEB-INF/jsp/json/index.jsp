<html>
<head><title>JSON Data</title>
</head>
<body>
<h1>JSON Data</h1>

<p>Static stuff</p>
<ul>
<li><a href="milestones.do">Milestones</a></li>
<li><a href="rules.do">Rules</a></li>
<li><a href="objectives.do">Objectives</a></li>
<li><a href="sources.do">DataSources</a></li>
<li><a href="misc.do">Misc</a></li>
<li><a href="exception.do">Exception</a></li>
</ul>

<p>Data Sources</p>
<ul>
<li><a href="ds-records.do?ds=release">Releases</a></li>
<li><a href="ds-records.do?ds=npi">NPIs</a></li>
</ul>

<p>Users</p>
<ul>
<li><a href="all-managers.do">All managers</a></li>
<li><a href="all-virtual-teams.do">All Virtual Teams</a></li>
<li><a href="everyone-else.do">Everyone Else</a></li>
<li><a href="all-usernames.do">All Usernames</a></li>
<li><form method="GET" action="usernames-since.do">
Names of users Modified Since: <input name="since" size="10" />
</form></li>
<li><form method="GET" action="users-since.do">
Users Modified Since: <input name="since" size="10" />
</form></li>
<li><form method="GET" action="users.do">
User Names: <input name="usernames" size="60" />
</form></li>
</ul>

</body></html>