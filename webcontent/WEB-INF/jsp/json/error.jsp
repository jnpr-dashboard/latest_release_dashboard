<%@ page contentType="application/json; charset=UTF-8" %><%@
 page session="false"%><%@ taglib prefix="s" uri="/struts-tags"
 %><s:if test="null != errorMessage">
<s:property value="jsonify('error', errorMessage)" escape="false"/>
</s:if><s:else>
// No error message supplied
{"error": "Unspecified error"}
</s:else><s:if test="null != exception">
<s:property value="jsonify('exception', exception)" escape="false"/>
<s:property value="jsonify('stacktrace', exceptionStack)" escape="false"/>
</s:if>
